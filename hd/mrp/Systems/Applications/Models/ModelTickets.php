<?php
/*
* Model for Ticketdesk
*/
class Tickets extends Model
{
	public $tableName = "ticket_main";
    public $lastInsertID;
	
	function createTicket($form = array(), $log = array()){
		$this->beginTransaction();
		try{
			$this->insertQuery('ticket_main',$form);
			$log['ticket_id'] = $this->lastInsertId();
			$this->lastInsertID = $this->lastInsertId();
			$this->insertQuery('ticket_log',$log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
    function editTicket($form = array(), $log = array()){
        $table_ticket = 'ticket_main';
        $table_log = 'ticket_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table_ticket, $form['input'], $form['where']);
			$this->insertQuery('ticket_log', $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
	
    function showAllTicket($query_option)
    {
        $query_string = "SELECT 
        ticket_id, 
        ticket_title, 
        ticket_type, 
        ticket_type.type_name, 
        ticket_category.category_name, 
        spare_part, 
        ticket_category.category_id, 
        ticket_category.category_parent_id, 
        PC.category_name AS parent, 
        ticket_status.status_name, 
        ticket_priority.priority_name, 
        impact, content, 
        status, 
        st.user_name, 
        st.display_name, 
        store_name,
        st.store_id AS store_id, 
        assigned_staff, 
        astaff.user_name AS staff, 
        astaff.display_name AS staff_name, 
        assign_time, 
        ticket_user, 
        cname.display_name AS creator_name, 
        submit_date,
        last_update, 
        problem_source, 
        resolved_date, 
        resolved_solution ";
		$query_string .= "FROM ticket_main LEFT JOIN ticket_type ON ticket_main.ticket_type=ticket_type.type_id ";
		$query_string .= "LEFT JOIN ticket_status ON ticket_main.status=ticket_status.status_id ";
		$query_string .= "LEFT JOIN ticket_category ON ticket_main.category=ticket_category.category_id ";
        $query_string .= "LEFT JOIN ticket_category PC ON ticket_category.category_parent_id=PC.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON ticket_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON ticket_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ticket_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ticket_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON ticket_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON ticket_main.ticket_user=st.user_id ";
        $count_query = "SELECT COUNT(*) AS row_total FROM ticket_main";
        #echo $query_string;
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
    function showAllTicketFiltered($filter, $query_option)
    {
        
        $query_string = "SELECT 
        ticket_id, 
        ticket_title, 
        ticket_type, 
        ticket_type.type_name, 
        ticket_category.category_name, 
        spare_part, 
        ticket_category.category_id, 
        ticket_category.category_parent_id, 
        ticket_status.status_name, 
        PC.category_name AS parent, 
        ticket_priority.priority_name, 
        impact, 
        content, 
        status, 
        st.user_name, 
        st.display_name, 
        store_name,
        st.store_id, 
        assigned_staff, 
        astaff.user_name AS staff, 
        astaff.display_name AS staff_name, 
        assign_time, 
        ticket_user, 
        cname.display_name AS creator_name, 
        submit_date,
        last_update,
        engineer_name,
        problem_source, 
        resolved_date, 
        resolved_solution ";
		$query_string .= "FROM ticket_main LEFT JOIN ticket_type ON ticket_main.ticket_type=ticket_type.type_id ";

		$query_string .= "LEFT JOIN ticket_status ON ticket_main.status=ticket_status.status_id ";

		$query_string .= "LEFT JOIN ticket_category ON ticket_main.category=ticket_category.category_id ";

        $query_string .= "LEFT JOIN ticket_category PC ON ticket_category.category_parent_id=PC.category_id ";

		$query_string .= "LEFT JOIN ticket_priority ON ticket_main.priority=ticket_priority.priority_id ";

		$query_string .= "LEFT JOIN (SELECT user_id,user_name,display_name FROM sushitei_portal.portal_user) astaff ON ticket_main.assigned_staff=astaff.user_id ";

        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ticket_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ticket_main.ticket_id=atime.tid ";

        $query_string .= "LEFT JOIN (SELECT user_id,display_name FROM sushitei_portal.portal_user) cname ON ticket_main.ticket_creator=cname.user_id ";
        
        $query_string .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON ticket_main.ticket_user=st.user_id ";
        $num = 0;
        $count_query = "SELECT COUNT(*) AS row_total FROM ticket_main ";
        $count_query .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON ticket_main.ticket_user=st.user_id ";
        if(count($filter) > 0){
            $query_string .= "WHERE ";
            $count_query .= "WHERE ";
        }
        foreach($filter as $cat => $arr){
            if($cat != 'date_end' && !empty($arr['value'])){
                if(!empty($cat) && $num > 0){
                    $query_string .= "AND ";
                    $count_query .= "AND ";
                }
                if($cat == 'submit_date' && !empty($filter['date_end']['value'])){
                    $query_string .= "submit_date BETWEEN '{$arr['value']} 00:00:00' AND '{$filter['date_end']['value']} 23:59:00' ";
                    $count_query .= "submit_date BETWEEN '{$arr['value']} 00:00:00' AND '{$filter['date_end']['value']} 23:59:00' ";                    
                }elseif($cat == 'get_all'){
                    $query_string .= "category IN (SELECT category_id FROM ticket_category WHERE category_parent_id='{$arr['value']}') ";
                    $count_query .= "category IN (SELECT category_id FROM ticket_category WHERE category_parent_id='{$arr['value']}') ";
                }else{
                    $query_string .=  "{$cat} {$arr['type']} '{$arr['value']}' ";
                    $count_query .= "{$cat} {$arr['type']} '{$arr['value']}' ";
                }
            }
            $num++;
        }
        #exit($query_string);
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
	function showMyActiveTicket($my_id, $query_option)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, ticket_type.type_name, ticket_category.category_name, spare_part, category_id, category_parent_id, ticket_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution ";
		$query_string .= "FROM ticket_main LEFT JOIN ticket_type ON ticket_main.ticket_type=ticket_type.type_id ";
        $query_string .= "LEFT JOIN ticket_status ON ticket_main.status=ticket_status.status_id ";
		$query_string .= "LEFT JOIN ticket_category ON ticket_main.category=ticket_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON ticket_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON ticket_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ticket_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ticket_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON ticket_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name, email FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON ticket_main.ticket_user=st.user_id ";
		$query_string .= "WHERE ticket_user='{$my_id}'";
        $count_query = "SELECT COUNT(*) AS row_total FROM ticket_main WHERE ticket_user='{$my_id}'";
		
		
        $result = $this->pagingQuery($query_string,$count_query,$query_option);
        return $result;
    }
    
    function showTicketBy($input)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, ticket_type.type_name, ticket_category.category_name, category_id, category_parent_id, ticket_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, st.email, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, spare_part ";
		$query_string .= "FROM ticket_main LEFT JOIN ticket_type ON ticket_main.ticket_type=ticket_type.type_id ";
        $query_string .= "LEFT JOIN ticket_status ON ticket_main.status=ticket_status.status_id ";
		$query_string .= "LEFT JOIN ticket_category ON ticket_main.category=ticket_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON ticket_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON ticket_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ticket_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ticket_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON ticket_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name, email FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON ticket_main.ticket_user=st.user_id ";
        if(isset($input['my_id'])){
            $query_string .= "WHERE ticket_user=:my_id AND ticket_id=:ticket_id";
        } else {
            $query_string .= "WHERE ticket_id=:ticket_id";
        }

        $result = $this->fetchSingleQuery($query_string, $input);
        
		return $result;
    }
    
    function getLog($input){
        $query_string = "SELECT ticketlog_title, ticket_log.user_id, display_name, ticketlog_type, ticketlog_content, ticketlog_time FROM ticket_log ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user ON ticket_log.user_id=sushitei_portal.portal_user.user_id ";
        $query_string .= "WHERE ticket_id = :ticket_id AND ticketlog_show = 1 ORDER BY ticketlog_time DESC";
        $result = $this->fetchAllQuery($query_string, $input);
		return $result;
    }
    
	function getTicketType()
    {
        $query_string = 'SELECT * FROM ticket_type';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM ticket_category WHERE category_parent_id=1';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM ticket_category WHERE category_parent_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getUploadedFile($query_value)
	{
		$query_string = 'SELECT * FROM ticket_upload WHERE ticket_id=:ticket_id';
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getAllActiveStore()
    {
        $query_string = "SELECT * FROM sushitei_portal.portal_store WHERE store_status=1 AND store_location IN (1,12,14,15) ORDER BY store_name ASC"; //store_type,store_location,store_name
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getMailRecipient()
    {
        $query_string = 'SELECT pu.email,pu.display_name FROM sushitei_portal.portal_user pu ';
        $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
        $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
        $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="receiveEmail" AND pp.permission_site='.getConfig('site_number').' GROUP BY pu.email';
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function myOutlet($input){
        $query_string = "SELECT store_name FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id WHERE pu.user_id=:user_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
}