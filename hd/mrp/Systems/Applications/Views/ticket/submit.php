                    <div class="container-fluid">
                        <h3 class="well" style="background: rgb(51,122, 183); color: #fff;">Submit new Ticket</h3>
                        <?=(isset($error) ? '<div class="alert alert-danger">Please check your Highlighted input below</div>' : '')?>
                        <div class="col-lg-12 well">
                            <div class="row">
                                <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
                                    <div class="col-lg-12">
                                        <div class="form-group <?=(isset($error) && isset($error['title']) ? 'has-error' : '')?>">
                                            <label>Ticket Title</label>
                                            <input name="title" type="text" placeholder="Type here.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['title']) ? "value='{$input['title']}'" : '')?> required>
                                            <?=(isset($error) && isset($error['title']) ? "<div class='alert alert-danger'>{$error['title']}</div>" : '')?>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 form-group">
                                                <label>Ticket Type</label>
                                                <select name="type" class="form-control" required>
                                                    <option value="4" selected>Work Request</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 form-group">
                                                <label>Category</label>
                                                <select name="main_category" id="mainCategory" class="form-control" required>
                                                    <option value="" default></option>
                                                    <?php
                                                        foreach($main_category as $category_form){
                                                            echo "\n<option value=\"{$category_form['category_id']}\">{$category_form['category_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 form-group">
                                                <label>Sub-Category</label>
                                                <select name="sub_category" class="form-control" id="subCategory" disabled>
                                                    <option value="" default></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" name="customer_impact">Impact Customer</input>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>Current Condition</label>
                                            <textarea id="ticket-content" name="detail" placeholder="Type here.." class="form-control" rows="3"><?=(isset($input) && isset($input['detail']) ? $input['detail'] : '')?></textarea>
                                        </div>
                                        <div class="form-group" id="addFile">
                                            <label>Upload Image (JPEG)</label>
                                            <input name="image_upload[]" type="file" class="form-control" accept="image/jpeg">
                                        </div>
                                        <div class="form-group">
                                            <button id="addFileButton" type="button" name="addfile" class="btn btn-sm btn-success">Add File</button>
                                        </div>
                                        <?php
                                            if($admin){
                                        ?>
                                        <div class="form-group">
                                            <label for="onbehalf">Create on Behalf other users</label>
                                            <input type="checkbox" value="1" id="onbehalf" name="behalf"></input>
                                            <select id="userlist" name="user" class="form-control" disabled>
                                            </select>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                        <div class="form-group" id="addFile" style="margin-top:30px;">
                                            <div class="g-recaptcha <?=(isset($error) && isset($error['recaptcha']) ? 'alert alert-danger' : '')?>" data-sitekey="6LemwyETAAAAACZCN9RkUJ1D4j_bxeBvu1lgmAZP"></div>
                                            
                                        </div>
                                        <button type="submit" name="submit" value="kirim" class="btn btn-lg btn-primary">Submit Ticket</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    