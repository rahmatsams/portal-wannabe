<?php
class Admin extends Controller
{
    public function accessRules()
	{
		return array(
			array('Deny', 
				'actions'=>array('index'),
				'groups'=>array('Guest'),
			),
            array('Allow', 
				'actions'=>array('index','exportbyuser', 'store_index', 'store_insert', 'store_edit'),
				'groups'=>array('Super Admin','Administrator'),
			),
            array('Allow', 
				'actions'=>array('exportbyuser', 'phpExcel'),
				'groups'=>array('Ticket Users', 'Admin MRP', 'Admin View'),
			),
            array('Allow', 
				'actions'=>array('index'),
				'groups'=>array('Admin MRP'),
			),
		);
	}
	
    function index()
    {
        $data = array(
			'menu'=>'adminmenu',
            'admin'=>$this->isAdmin(),
			'_mySession' => $this->_mySession,
		);
        $this->load->template('admin/index',$data);
    }
	
	function exportbyuser()
	{
		$data = $this->load->model('Tickets');
		$option = array(
			'file_name' => 'Ticketexportuser',
			'context_title' => 'Report Ticket Desk MRP Oleh Client'
		);
		$query_option = array(
            'order_by' => 'submit_date',
            'order' => 'DESC',
            'result' => 1000
        );
        if(empty($this->_mySession['last_query']) || !is_array($this->_mySession['last_query'])){
            if($this->_mySession['group'] > 5){
                $query = array();
            }else{
                $query = array(
                    'ticket_user' => $this->_mySession['user_id']
                );
            }
            
        }else{
            $query = $this->_mySession['last_query'];
        }
		$this->phpExcel($data->showAllTicketFiltered($query, $query_option), $option);
        #print_r($data->showAllTicketFiltered($query, $query_option));
	}
	
    function phpExcel($ticket_result, $option)
    {
		$objPHPExcel = $this->load->lib('PHPExcel');
		$objPHPExcel->getProperties()->setCreator("Sushitei IT")
							 ->setLastModifiedBy("Sushitei IT")
							 ->setTitle("Ticketdesk MRP")
							 ->setSubject("Ticketdesk MRP")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1')
					->setCellValue('A1', $option['context_title'])
                    ->mergeCells('A3:A4')
					->setCellValue('A3', 'No.')
                    ->mergeCells('B3:C3')
                    ->setCellValue('B3', 'Pemohon')
					->setCellValue('B4', 'Nama')
					->setCellValue('C4', 'Outlet')
                    ->mergeCells('D3:G3')
                    ->setCellValue('D3', 'Registrasi Tiket')
					->setCellValue('D4', 'No Tiket')
					->setCellValue('E4', 'Tanggal Registrasi')
                    ->setCellValue('F4', 'Tanggal Respon')
					->setCellValue('G4', 'Waktu Respon')
                    ->mergeCells('H3:I3')
                    ->setCellValue('H3', 'Peralatan Rusak')
					->setCellValue('H4', 'Kategori')
					->setCellValue('I4', 'Nama Peralatan')
                    ->mergeCells('J3:K3')
                    ->setCellValue('J3', 'Analisa Permasalahan')
					->setCellValue('J4', 'Masalah/Kondisi saat ini')
                    ->setCellValue('K4', 'Penyebab Masalah')
                    ->mergeCells('L3:N3')
                    ->setCellValue('L3', 'Penanganan Masalah')
					->setCellValue('L4', 'Solusi')
                    ->setCellValue('M4', 'Estimasi Biaya')
                    ->setCellValue('N4', 'PIC')
                    ->setCellValue('O4', 'Teknisi')
                    ->mergeCells('P3:P4')
					->setCellValue('P3', 'Status Penanganan')
                    ->mergeCells('Q3:R3')
                    ->setCellValue('Q3', 'Waktu')
					->setCellValue('Q4', 'Aktual')
                    ->setCellValue('R4', 'Durasi')
                    ->mergeCells('S3:T3')
                    ->setCellValue('S3', 'Sparepart')
					->setCellValue('S4', 'Butuh Sparepart')
                    ->setCellValue('T4', 'Nama Sparepart');
                    

		// Miscellaneous glyphs, UTF-8
		if(is_array($ticket_result) && !empty($ticket_result)){
			$no = 1;
			$row = 5;
			$status = array('Unknown','Open','On Progress','Pending','Need Vendor','Solved','Closed');
			$priority = array('Unknown','ASAP','High','Medium','Low');
			foreach($ticket_result as $result){
                $resolve_date = date('m/d/Y H:i:s', strtotime($result['resolved_date']));
                $submit_date = date('m/d/Y H:i:s', strtotime($result['submit_date']));
                $last_update = date('m/d/Y H:i:s', strtotime($result['last_update']));
                $assign_date = date('m/d/Y H:i:s', strtotime($result['assign_time']));
				$objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A{$row}", $no)
                            ->setCellValue("B{$row}", $result['display_name'])
                            ->setCellValue("C{$row}", $result['store_name'])
                            ->setCellValue("D{$row}", $result['ticket_id'])
                            ->setCellValue("E{$row}", $submit_date)
                            ->setCellValue("F{$row}", $result['assign_time'])
                            ->setCellValue("G{$row}", "=F{$row}-E{$row}")
                            ->setCellValue("H{$row}", $result['parent'])
                            ->setCellValue("I{$row}", $result['category_name'])
                            ->setCellValue("J{$row}", $result['content'])
                            ->setCellValue("K{$row}", $result['problem_source'])
                            ->setCellValue("L{$row}", $result['resolved_solution'])
                            ->setCellValue("N{$row}", $result['staff_name'])
                            ->setCellValue("O{$row}", $result['engineer_name'])
                            ->setCellValue("P{$row}", $result['status_name'])
                            ->setCellValue("Q{$row}", ($result['resolved_date'] != '0000-00-00 00:00:00' ? $result['resolved_date'] : ''))
							->setCellValue("R{$row}", ($result['resolved_date'] != '0000-00-00 00:00:00' ? floor(abs(strtotime($result['resolved_date']) - strtotime($result['submit_date']))/(60*60*24)) : ''))
                            ->setCellValue("S{$row}", ($result['spare_part'] == '' ? 'Tidak' : 'Ya'))
                            ->setCellValue("T{$row}", $result['spare_part']);
				$no++;
				$row++;
			}
		} else {
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:J4')
					->setCellValue('A4', "Tiket kosong");
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Category');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename={$option['file_name']}.xlsx");
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
    }
    
        
    
}
?>