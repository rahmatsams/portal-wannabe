<?php
/*
* Model for Ticketdesk
*/
class TicketPriority extends Model
{
	
	function createTicket($form = array(), $log = array()){
		$this->beginTransaction();
		try{
			$this->insertQuery('ticket_main',$form);
			$log['ticket_id'] = $this->lastInsertId();
			
			$this->insertQuery('ticket_log',$log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
	
    function getPriority($query_value)
    {
        $query_string = "SELECT * from ticket_priority WHERE priority_id=:pid";
		
		$result = $this->fetchSingleQuery($query_string, $query_value);
        return $result;
    }
	
}