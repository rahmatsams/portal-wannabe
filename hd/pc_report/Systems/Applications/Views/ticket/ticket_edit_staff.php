<?php  
    $title = date("d-m-Y", strtotime($ticket['ticket_title']));
?>
<div class="container-fluid">    
<div class="row">
<div class="col-lg-7">
    <h4 class="well" style="background: rgb(128, 176, 93); color: white; padding: 5px;">Ticket ID: <?=$ticket['ticket_id']?></h4>
</div>
<div class="col-lg-5">
    <h4 class="well" style="background: rgb(128, 176, 93); color: white; padding: 5px;">Title: <?=$title?></h4>
</div>
<div class="col-lg-7">
    <div class="col-lg-12 well" style="min-height: 393px;">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group hidden" id="tickettabledesc">
                    <label>Detail</label>
                    <textarea id="ticketcontent" name="detail" placeholder="Type ticket detail.." class="form-control" rows="5" readonly=""><?=$ticket['content']?></textarea>
                </div>
                <div id="tickettableraw">
                    <?=nl2br(htmlspecialchars_decode($ticket['content']))?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-5">
    <div class="col-lg-12 well" style="min-height: 360px;">
        <form id="detailedit">
            <div id="tickettabledetail">
                <table class="table table-striped" id="listTable">
                    <tbody>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td><?=$ticket['status_name']?></td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>:</td>
                            <td><?=$ticket['type_name']?></td>
                        </tr>
                        <tr>
                            <td>Main Category</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['main_category']) ? $ticket['main_category'] : '-')?></td>
                        </tr>
                        <tr>
                            <td>Sub Category</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['sub_category']) ? $ticket['sub_category'] : '-')?></td>
                        </tr>   
                        <tr>
                            <td>Owner</td>
                            <td>:</td>
                            <td><?=$ticket['display_name']?></td>
                        </tr>
                        <tr>
                            <td>PIC</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['staff_name']) ? $ticket['staff_name'] : '-')?></td>
                        </tr>
                        <tr>
                            <td>Submit Date</td>
                            <td>:</td>
                            <td><?=date("d-M-Y H:i", strtotime($ticket['submit_date']))?></td>
                        </tr>
                        <tr>
                            <td>Last Update</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['last_update']) ? date("d-M-Y H:i", strtotime($ticket['last_update'])) : '-')?></td>
                        </tr>
                        <tr>
                            <td>Set Date</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['start_time']) ? date("d-M-Y", strtotime($ticket['start_time'])) : '-')?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row hidden" id="tickettableedit">
                <div class="form-group">
                    <label>Ticket Type</label>
                    <select name="type" class="form-control" required>
                        <option value=""></option>
                        <?php
                            foreach($ticket_type as $type_input){
                                echo "\n
                                <option value=\"{$type_input['type_id']}\"" . ($ticket['ticket_type'] == $type_input['type_id'] ? ' Selected' : '') . ">{$type_input['type_name']}</option>\n";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="main_category" id="mainCategory" class="form-control" <?=(isset($main_category) && is_array($main_category) ? '' : 'disabled')?>>
                        <option value=""></option>
                        <?php
                            foreach($main_category as $category_form){
                                echo "\n
                                <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] || $ticket['category_parent_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Sub Category</label>
                    <select name="sub_category" class="form-control" id="subCategory" <?=(isset($sub_category) && is_array($sub_category) ? '' : 'disabled')?>>
                        <?php
                            if(isset($sub_category) && is_array($sub_category)){
                                foreach($sub_category as $category_form){
                                    echo "\n
                                    <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                                }
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="hidden" name="ticket_id" value="<?=$ticket['ticket_id']?>"/>
                        <input type="checkbox" value="1" name="customer_impact" <?=($ticket['impact'] == 1 ? 'checked' : '')?> hidden> </input>
                    </label>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
    if(is_array($uploaded) && count($uploaded) > 0){
        echo "
<div class=\"col-lg-12\">
    <h4 class=\"well\" style=\"background: rgb(128, 176, 93); color: white; padding: 5px;\" id=\"tickettitle\">Uploaded File</h4>
</div>
<div class=\"col-lg-12 upload-thumbnail\">
        ";
        foreach($uploaded as $img_file){
            echo "
    <a href=\"{$img_file['upload_location']}{$img_file['upload_name']}\" target=\"_blank\"><img src=\"{$img_file['upload_location']}{$img_file['upload_name']}\" class=\"img-thumbnail\" style=\"width:100px; height:100px;\"/></a>
            ";
        }
        
        echo "
</div>
        ";
    }
?>
<div class="col-lg-12">
    <h4 class="well" style="background: rgb(128, 176, 93); color: white; padding: 5px;" id="tickettitle">Ticket command</h4>
</div>
<div class="col-lg-12" style="margin-top: 0px; margin-bottom: 5px;" id="activitybutton">
    <div class="col-lg-12 well">
        <div class="row">
            <?php
                $menu = array();
                if($_mySession['user_id'] == $ticket['ticket_user']){
                    if($ticket['status'] < 4 || $ticket['status'] == 5){
                        $menu['editbutton'] = 'Edit';
                        $menu['commentbutton'] = 'Comment';
                        $menu['closebutton'] = 'Set Date';
                    }
                    if($ticket['status'] == 4){ #Status=Closed
                        $menu['reopenbutton'] = 'Re-open';
                    }
                }

                if($admin){
                    if($ticket['status'] < 4 || $ticket['status'] == 5){
                        $menu['editbutton'] = 'Edit';
                        $menu['commentbutton'] = 'Comment';
                        $menu['closebutton'] = 'Set Date';
                        $menu['takeoverbutton'] = 'Close';  
                    }
                    /*if($ticket['status'] < 4 || $ticket['status'] == 5 && $_mySession['user_id'] != $ticket['assigned_staff']){
                        $menu['takeoverbutton'] = 'Close';
                    }*/
                    if($ticket['status'] == 4){
                        $menu['reopenbutton'] = 'Re-open';
                    }
                }

                foreach($menu as $key => $value){
                    echo "
            <div class=\"col-sm-2 form-group\">
                <button type=\"button\" value=\"edit\" class=\"btn btn-sm btn-success btn-block\" id=\"{$key}\">{$value}</button>
            </div>
                    ";
                }
            ?>
        </div>
    </div>
</div>
<div class="col-lg-12 hidden" id="tickettablecomment">
    <form id="updateticketstatus">
        <div class="col-lg-12 well">
            <div class="row">
                <div class="col-sm-6 form-group hidden" id="setDate">
                    <label>Resolve Date</label>
                    <input type="date" class="form-control" id="dateEditMinToday" name="start_date">
                </div>

                <div class="col-sm-6 form-group hidden" id="coreProblem" hidden="">
                    <label>Problem Cause :</label>
                    <textarea id="coreproblem" name="cause" placeholder="Type problem cause.." class="form-control" rows="3"></textarea>
                    </select>
                </div>
                <div class="col-sm-6 form-group hidden" id="spareParts">
                    <label>Sparepart Name :</label>
                    <input type="text" name="spare_part" id="sparePartName" class="form-control" placeholder="Type sparepart name.."/>
                </div>
                <div class="col-sm-12 form-group">
                    <label id="labelcomment">Komentar</label>
                    <textarea id="updatereason" name="detail" placeholder="Type a comment.." class="form-control" rows="3"></textarea>
                </div>
                <div class="col-sm-6 form-group hidden" id="assignform">
                    <label>Assign ticket to :</label>
                    <select id="userlist" name="selectedpic" class="form-control">
                        <?php
                            foreach($staff as $pic){
                                echo "\n
                                <option value=\"{$pic['user_id']}\" data-id=\"{$pic['display_name']}\">{$pic['display_name']}</option>\n";
                            }
                        ?>
                    </select>
                </div>
                <div class="col-lg-12">
                    <div class="col-sm-3 form-group">
                        <button type="submit" name="submit" class="btn btn-sm btn-success btn-block" id="submitstatus">Update</button>
                    </div>
                    <div class="col-sm-3 form-group">
                        <button type="button" name="cancel" class="btn btn-sm btn-danger btn-block" id="cancelstatus">Batal</button>
                    </div>
                </div>
            </div>
        </form>   
    </div>
</div>
<div class="col-lg-12">
    <h4 class="well" style="background: rgb(128, 176, 93); color: white; padding: 5px;">Activity Log</h4>
</div>
<div class="col-lg-12">
    <div class="col-lg-12 well">
        <?php
            foreach($log as $ticket_log){
                
                if($ticket_log['user_id'] == $ticket['ticket_user']){
                    $style = "bg-warning";
                } else {
                    $style = "bg-success";
                }
                
                $date = date('D - d M Y, H:i', strtotime($ticket_log['ticketlog_time']));
                echo "
                <table class='table table-bordered'>
                    <thead>
                        <tr class='{$style}'>
                            <td class='{$style}' style='width: 15%; font-weight:bold;'>{$ticket_log['ticketlog_type']}</td>
                            <td class='{$style}' style='width: 60%; font-weight:bold;'>{$ticket_log['ticketlog_title']}</td>
                            <td class='{$style}' style='width: 25%; font-weight:bold;'>{$date}</td>
                        </tr>
                    </thead>".
                    (!empty($ticket_log['ticketlog_content']) ? "<tbody>
                        <tr>
                            <td colspan='3' class='bg-white' style='white-space: pre-wrap;'>". nl2br(htmlspecialchars_decode($ticket_log['ticketlog_content'])) ."</td>
                        </tr>
                    </tbody>" : '') ."
                </table>
                ";
            }
        ?>
    </div>
</div>
</div>
</div>        