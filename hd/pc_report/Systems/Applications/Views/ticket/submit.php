                    <div class="container-fluid">
                        <h3 class="well" style="background: rgb(128, 176, 93); color: #fff;">Submit new Ticket</h3>
                        <?=(isset($error) ? '<div class="alert alert-danger">Please check your Highlighted input below</div>' : '')?>
                        <div class="col-lg-12 well">
                            <div class="row">
                                <form action="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
                                    <div class="col-lg-12">
                                        <div class="form-group <?=(isset($error) && isset($error['title']) ? 'has-error' : '')?>">
                                            <label>Tanggal Pest Control</label>
                                            <input type="date" class="form-control" name="title" required>
                                            <small>(Waktu pengerjaan pest control)</small>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 form-group">
                                                <label>Ticket Type</label>
                                                <select name="type" id="typeCategory" class="form-control" required>
                                                    <option value="1">Service Reguler</option>
                                                    <option value="2">Red Alert</option>
                                                    <option value="3">Red Alert Tikus</option>
                                                    <option value="4">Red Alert Kecoa</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 form-group">
                                                <label>Category</label>
                                                <select name="main_category" id="mainCategory" class="form-control" required disabled="">
                                                    <option value="" default></option>
                                                    <?php
                                                        foreach($main_category as $category_form){
                                                            echo "\n<option value=\"{$category_form['category_id']}\">{$category_form['category_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 form-group">
                                                <label>Sub-Category</label>
                                                <select name="sub_category" class="form-control" id="subCategory" disabled>
                                                    <option value="" default></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" hidden="">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" name="customer_impact">Impact Customer</input>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>Current Condition</label>
                                            <textarea id="ticket-content" name="detail" class="form-control" rows="7"><?=(isset($input) && isset($input['detail']) ? $input['detail'] : 'Jumlah Tangkapan (Tikus/Kecoa) :
Lokasi Tangkapan:
Hasil service: 
Rekomendasi: 
Nomor ticket desk MRP: 
Nama pendamping outlet:     
Nama pendamping teknisi:')?>
                                            </textarea>
                                        </div>
                                        <div class="form-group" id="addFile">
                                            <label>Upload Image (JPEG)</label>
                                            <input name="image_upload[]" type="file" class="form-control" accept="image/jpeg">
                                        </div>
                                        <div class="form-group">
                                            <button id="addFileButton" type="button" name="addfile" class="btn btn-sm btn-success">Add File</button>
                                        </div>
                                        <?php
                                            if($admin){
                                        ?>
                                        <div class="form-group" hidden="">
                                            <label for="onbehalf">Create on Behalf other users</label>
                                            <input type="checkbox" value="1" id="onbehalf" name="behalf"></input>
                                            <select id="userlist" name="user" class="form-control" disabled>
                                            </select>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                        <div class="form-group" id="addFile" style="margin-top:30px;">
                                            <div class="g-recaptcha <?=(isset($error) && isset($error['recaptcha']) ? 'alert alert-danger' : '')?>" data-sitekey="6LemwyETAAAAACZCN9RkUJ1D4j_bxeBvu1lgmAZP"></div>
                                        </div>
                                        <button type="submit" name="submit" value="kirim" class="btn btn-lg btn-success">Submit Ticket</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    