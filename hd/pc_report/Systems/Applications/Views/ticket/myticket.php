<div class="container-fluid">    
    <h2 class="sub-header">My Submitted Ticket</h2>
	<div class="col-md-12">
        <div class="row form-inline" style="margin-bottom: 10px;">
            <div class="col-sm-2 form-group">
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Advanced Search
                </a>
            </div>
            <div class="col-sm-4 col-lg-offset-6 form-group">
                <label>Sort</label>
                <select id="sortResult" name="sort_result" class="form-control">
                    <option value=""></option>
                    <option value="title_asc">Title ▲</option>
                    <option value="title_desc">Title ▼</option>
                    <option value="submit_asc">Submit Date ▲</option>
                    <option value="submit_desc">Submit Date ▼</option>
                    <option value="category_asc">Category ▲</option>
                    <option value="category_desc">Category ▼</option>
                    <option value="pic_asc">PIC ▲</option>
                    <option value="pic_desc">PIC ▼</option>
                    <option value="status_asc">Status ▲</option>
                    <option value="status_desc">Status ▼</option>
                </select>
            </div>
            
        </div>
        <div class="collapse collapse-well clearleftright" id="collapseExample">
                <form id="advanceSearch">
                    <div class="col-md-12 well">
                        <div class="row">
                            <div class="col-sm-3 form-group">
                                <label>Title</label>
                                <input id="ticketPage" name="page" type="hidden" value="1">
                                <input id="ticketTitle" name="title" type="text" placeholder="Masukkan judul.." class="form-control" maxlength="25">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Status</label>
                                <select id="ticketStatus" name="status" class="form-control">
                                    <option value="" default></option>
                                    <option value="1">Open</option>
                                    <option value="2" hidden="">On-Progress</option>
                                    <option value="3" hidden="">Solved</option>
                                    <option value="4">Closed</option>
                                    <option value="5">Re-Open</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Category</label>
                                <select name="main_category" id="mainCategory" class="form-control">
                                    <option value="" default></option>
                                    <?php
                                        foreach($main_category as $category_form){
                                            echo "\n
                                            <option value=\"{$category_form['category_id']}\">{$category_form['category_name']}</option>\n";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Sub Category</label>
                                <select name="sub_category" class="form-control" id="subCategory" disabled>
                                    <option value="" default></option>
                                </select>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-sm-3 form-group">
                                <label>Submit date from</label>
                                <input id="ticketDateFirst" name="date_first" type="date" class="form-control" maxlength="10">
                            </div>                            
							<div class="col-sm-3 form-group">
								<label>to date</label>
                                <input id="ticketDateLast" name="date_last" type="date" class="form-control" maxlength="10">
                            </div>
                        </div>
                        <button type="submit" name="submit" value="kirim" class="btn btn-sm">Search</button>
                    </div>
                </form> 
        </div>
    </div>
    <div class="table-responsive clearleftright">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Ticket</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Main Category</th>
                    <th>Sub Category</th>
                    <th>PIC</th>
                    <th>Status</th>
                    <th>Submit Date</th>
                    <th>Last Update</th>
                </tr>
            </thead>
            <tbody id="listTable">
                <?php
                    if(is_array($ticket_list) && !empty($ticket_list)){
                        $num = 1+(((isset($_GET['page']) ? $_GET['page'] : 1)-1)*10);
                        foreach($ticket_list as $result){
                            $date_check = '';
							$diff_seconds  = ($result['resolved_date'] != '0000-00-00 00:00:00' ? strtotime($result['resolved_date']) - strtotime($result['submit_date']) : strtotime(date("Y-m-d H:i:s")) - strtotime($result['submit_date']));
							$stat = floor($diff_seconds/3600);
							
							if(($stat >= 24 && $stat < 48) && $result['status'] < 3){
								$date_check = "warning";
							}elseif($stat >= 48 && $result['status'] < 3){
								$date_check = "danger";
							}elseif($result['status'] >= 3){
								$date_check = "success";
							}else {
								$date_check = 'active';
							}
                            
                            $submit_date = date("d-m-Y", strtotime($result['submit_date']));
                            $last_update = (!empty($result['last_update']) ? date("d-m-Y", strtotime($result['last_update'])) : '-');
                            $title = date("d-m-Y", strtotime($result['ticket_title']));
                                    
                            echo "
                <tr class=\"{$date_check}\">
                    <td>{$num} </td>
                    <td><a href=\"edit_ticket_{$result['ticket_id']}.html\">{$result['ticket_id']}</a></td>
                    <td><a href=\"edit_ticket_{$result['ticket_id']}.html\">{$title}</a></td>
                    <td>{$result['type_name']}</td>
                    <td>{$result['parent']}</td>
                    <td>{$result['category_name']}</td>
                    <td>{$result['staff_name']}</td>
                    <td>{$result['status_name']}</td>
                    <td>{$submit_date}</td>
                    <td>{$last_update}</td>
                </tr>";
                            $num++;
                        }
                    } else {
                ?>
                <tr>
                    <td colspan="10">There's no ticket found</td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
        <?php
            $page = ceil($total/$max_result);
            if($page > 1){
                echo '
            <nav>
                <ul class="pagination pagination-sm">
                    <li>
                        <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
                    </li>';
                for($i=1;$i <= $page;$i++){
                    echo "<li><a href=\"index_page_{$i}.html\">{$i}</a></li>";
                }
                echo '
                <li>
                    <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                </li>
                </ul>
            </nav>';
            }
        ?>
<a class="btn btn-primary" role="button" href="export_data.html" aria-expanded="false" aria-controls="collapseExample">
        Export Data
    </a>
    </div>
</div>