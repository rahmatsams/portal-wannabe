
                    <div id="main-office">
                        <h3>Pt. Control Systems Headquarters</h3>
                        <p><img src="Resources/images/home2.gif"/>Beltway Office Park<br/>
                        Building A, 2nd Floor<br/>
                        Jln Ampera Raya No.9-10<br/>
                        Ragunan, Pasar Minggu<br/>
                        Phone: 021-780-7881<br/>
                        Fax: 021-780-7879<br/>
                        </p>
                    </div>
					<h3 class="bottom-border">Branch Office</h3>
					<div class="branch-office">
						<h3>Pekanbaru</h3>
						<p>Graha Ventura Lt.1<br/>
						Jln. Jendral Sudirman No.1 Tangkerang Utara<br/>
						Pekanbaru 28282<br/>
						Phone: (0761) 855-071<br/>
                        Phone: (0761) 855-072<br/>
						Fax: (0761) 27-266<br/>
						</p>
					</div>
					<div class="branch-office">
						<h3>Balikpapan</h3>
						<p>Gedung Gasono Travel<br/>
						Jln. Ahmad Yani No.40<br/>
						Balikpapan 76113<br/>
						Phone: (0542) 750-363<br/>
                        Phone: (0542) 750-362<br/>
						Fax: (0542) 750-365<br/>
						</p>
					</div>
					<div class="branch-office">
						<h3>Surabaya</h3>
						<p>Ruko Juanda Business Center<br/>
						Jln. Ir Juanda Blok B-16<br/>
						Surabaya<br/>
						Phone: (031) 855-6080<br/>
						</p>
					</div>
					<div class="branch-office">
						<h3>Duri</h3>
						<p>
						Jln I. Hang Tuah No.22<br/>
						Duri Riau, 28884<br/>
						Phone: (0765) 569-858<br/>
						Fax: (0765) 596-859<br/>
						</p>
					</div>