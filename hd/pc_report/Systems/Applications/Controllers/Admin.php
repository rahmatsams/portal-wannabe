<?php
class Admin extends Controller
{
    public function accessRules()
	{
		return array(
			array('Deny', 
				'actions'=>array('index'),
				'groups'=>array('Guest'),
			),
            array('Allow', 
				'actions'=>array('index','exportbyuser', 'store_index', 'store_insert', 'store_edit'),
				'groups'=>array('Super Admin','Administrator'),
			),
            array('Allow', 
				'actions'=>array('exportbyuser', 'phpExcel'),
				'groups'=>array('Ticket Users', 'Admin QA'),
			),
            array('Allow', 
				'actions'=>array('index'),
				'groups'=>array('Admin QA'),
			),
		);
	}
	
    function index()
    {
        $data = array(
			'menu'=>'adminmenu',
			'_mySession' => $this->_mySession,
            //'admin'=>$this->isAdmin(),
		);
        $this->load->template('admin/index',$data);
    }
	
	function exportbyuser()
    {
        $data = $this->load->model('Tickets');
        $option = array(
            'file_name' => 'PC_Report',
            'context_title' => 'Pest Control Report'
        );
        $query_option = array(
            'order_by' => 'submit_date',
            'order' => 'DESC'
        );
        if(empty($this->_mySession['last_query']) || !is_array($this->_mySession['last_query'])){
            if($this->isAdmin()){
                $query = array();
            }else{
                $query = array(
                    'ticket_user' => $this->_mySession['user_id']
                );
            }
            
        }else{
            $query = $this->_mySession['last_query'];
        }
        $this->phpExcel($data->showAllTicketFilteredExport($query, $query_option), $option);
        //print_r($data->showAllTicketFilteredExport($query, $query_option));
        //exit();
    }
	
    function phpExcel($ticket_result, $option)
    {
		$objPHPExcel = $this->load->lib('PHPExcel');
		$objPHPExcel->getProperties()->setCreator("Sushitei IT")
							 ->setLastModifiedBy("Sushitei IT")
							 ->setTitle("Pest Control Report")
							 ->setSubject("Pest Control Report")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1')
					->setCellValue('A1', $option['context_title'])
                    ->mergeCells('A3:A4')
					->setCellValue('A3', 'No.')

                    ->mergeCells('B3:C3')
                    ->setCellValue('B3', 'Pemohon')
					->setCellValue('B4', 'Nama')
					->setCellValue('C4', 'Outlet')

                    ->mergeCells('D3:F3')
                    ->setCellValue('D3', 'Registrasi Tiket')
					->setCellValue('D4', 'No Tiket')
                    ->setCellValue('E4', 'Title')
					->setCellValue('F4', 'Tanggal Registrasi')

                    ->mergeCells('G3:I3')
                    ->setCellValue('G3', 'Kategori')
					->setCellValue('G4', 'Type')
					->setCellValue('H4', 'Main')
                    ->setCellValue('I4', 'Sub')

                    ->mergeCells('J3:J4')
                    ->setCellValue('J3', 'Desc')

                    ->mergeCells('K3:K4')
                    ->setCellValue('K3', 'Status')

                    ->mergeCells('L3:L4')
                    ->setCellValue('L3', 'PIC')

                    ->mergeCells('M3:N3')
                    ->setCellValue('M3', 'Waktu')
					->setCellValue('M4', 'Resolved')
                    ->setCellValue('N4', 'Closed');
                    
		// Miscellaneous glyphs, UTF-8
		if(is_array($ticket_result) && !empty($ticket_result)){
			$no = 1;
			$row = 5;
			//$status = array('Unknown','Open','On Progress','Pending','Need Vendor','Solved','Closed');
			//$priority = array('Unknown','ASAP','High','Medium','Low');
			foreach($ticket_result as $result){
                $resolve_date = ($result['start_time'] == '0000-00-00 00:00:00' ? '-' : date("d/m/Y", strtotime($result['start_time'])));

                $close_date = (is_null($result['close_time']) ? '-' : date("d/m/Y", strtotime($result['close_time'])));

                $submit_date = date('m/d/Y H:i:s', strtotime($result['submit_date']));
                
                $last_update = date('m/d/Y H:i:s', strtotime($result['last_update']));

                $title = date('m-d-Y', strtotime($result['ticket_title']));
                
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A{$row}", $no)
                            ->setCellValue("B{$row}", $result['display_name'])
                            ->setCellValue("C{$row}", $result['store_name'])
                            ->setCellValue("D{$row}", $result['ticket_id'])
                            ->setCellValue("E{$row}", $title)
                            ->setCellValue("F{$row}", $submit_date)
                            ->setCellValue("G{$row}", $result['type_name'])
                            ->setCellValue("H{$row}", $result['parent'])
                            ->setCellValue("I{$row}", $result['category_name'])
                            ->setCellValue("J{$row}", $result['content'])
                            ->setCellValue("K{$row}", $result['status_name'])
                            ->setCellValue("L{$row}", $result['staff_name'])
                            ->setCellValue("M{$row}", $resolve_date)
                            ->setCellValue("N{$row}", $close_date);

				$no++;
				$row++;
			}
		} else {
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:J4')
					->setCellValue('A4', "Tiket kosong");
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Category');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename={$option['file_name']}.xlsx");
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
    }
    
        
    
}
?>