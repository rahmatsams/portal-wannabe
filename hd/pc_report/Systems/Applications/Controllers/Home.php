<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Home extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('index', 'employee_logout'),
                'groups'=>array('Guest'),
            ),
            array('Allow', 
                'actions'=>array('index', 'employee_login', 'employee_logout', 'new_user'),
                'groups'=>array('*'),
            ),
        );
    }
    
    public function index()
    {
        $category = $this->load->model('TicketCategory');
		
        $option = array(
            'scriptload' => '
            <script type="text/javascript" src="Resources/js/home_pc.js"></script>
            ',
            'admin' => $this->isAdmin()
        );
        $ticket = $this->load->model('Tickets');
        $data = array(
            '_mySession' => $this->_mySession,
			'main_category' => $ticket->getMainCategory(),
            'outlet_list' => $ticket->getAllActiveStore(),
        );
        
        $query_option = array(
            'page' => (isset($_GET['page']) ? $_GET['page'] : 1),
            'result' => 10,
            'order_by' => (isset($this->_mySession['option']) && isset($this->_mySession['option']['order_by']) ? $this->_mySession['option']['order_by'] : 'submit_date'),
            'order' => (isset($this->_mySession['option']) && isset($this->_mySession['option']['order']) ? $this->_mySession['option']['order'] : 'DESC')
        );
         
        if($this->isAdmin()) {
            $data['ticket_list'] = $ticket->showAllTicket($query_option);
            $data['current_page'] = isset($_GET['page']) ? $_GET['page'] : 1;
            $data['total'] = $ticket->getCountResult();
            $data['max_result'] = $query_option['result'];
            $this->load->template('home/home', $data, $option);
        } else {
            $data['max_result'] = $query_option['result'];
            $data['current_page'] = $query_option['page'];
            $data['ticket_list'] = $ticket->showMyActiveTicket($this->_mySession['user_id'], $query_option);
            $data['total'] = $ticket->getCountResult();
            $this->load->template('ticket/myticket',$data, $option);
        }
        
    }
    
    public function employee_login()
    {
        if($this->isGuest()){
            $store = $this->load->model('Store');
            $data = array(
                '_mySession' => $this->_mySession,
                'store' => $store->getAllActiveStore()
            );
            $option = array(
                'personal_js' => 1,
                'scriptload' => '
                <script>
                    $(document).ready(function(){
                        $("#login").submit(function(event){
                            // cancels the form submission
                            event.preventDefault();
                            var me = $(this);
                            loginForm(me);
                        });
                    });
                </script>
                ',
            );
            $this->load->Template('home/login_member', $data, $option);
        }else{
            header("Location: index.html");
        }
        
        
    }
    
    public function employee_logout()
    {
        if($this->isGuest() < 1) {
            $this->unsetSession('user_id');
            $this->setSession('username','Guest');
            $this->setSession('group','Guest');
            $this->setSession('role',1);
            header("Location: index.html");
        } else {
            $this->showError(1);
        }
    }
    
    public function new_user()
    {
        if(isset($_POST['register']) && $_POST['register'] == 'Daftar'){
            $m_users = $this->load->model('Users');
            $ticket = $this->load->model('Tickets');
            $data = array(
                '_mySession' => $this->_mySession,
                'main_category' => $ticket->getMainCategory(),
                'outlet_list' => $ticket->getAllActiveStore(),
            );
            $input = $this->load->lib('input');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Periksa kembali input anda');
            $input->addValidation('user_name',$_POST['user_name'],'min=3', 'User name minimal 3 karakter');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Periksa kembali input anda');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Display Name wajib diisi');
            $input->addValidation('password', $_POST['password'],'min=6', 'Password minimal 6 karakter');
            $input->addValidation('password_confirm', $_POST['password_confirm'],'like='.$_POST['password'], 'Password konfirmasi berbeda');
            $input->addValidation('email',$_POST['email'],'email', 'Periksa kembali input anda');
            $input->addValidation('sec_quest', $_POST['sec_quest'],'min=1', 'Pertanyaan wajib diisi');
            $input->addValidation('sec_quest', $_POST['sec_quest'],'numeric', 'Periksa kembali input anda');
            $input->addValidation('answer', $_POST['sec_quest'],'min=1', 'Jawaban pertanyaan wajib diisi');
            $input->addValidation('outlet', $_POST['outlet'],'min=1', 'Nama Outlet wajib diisi');
            $input->addValidation('outlet', $_POST['outlet'],'numeric', 'Periksa kembali input anda');
            if($input->validate()){
                $insert_value = array(
                    'user_name' => $_POST['user_name'],
                    'user_password' => MD5($_POST['password']),
                    'display_name' => $_POST['display_name'],
                    'email' => $_POST['email'],
                    'store_id' => $_POST['outlet'],
                    'department_id' => '0',
                    'group_id' => '4',
                    'security_question' => $_POST['sec_quest'],
                    'security_answer' => $_POST['answer'],
                    'user_status' => '1'
                );
                if($m_users->checkUserName($_POST['user_name'])){
                    if($m_users->newUser($insert_value)){
                        $mail_option = array(
                            'title' => 'Pendaftaran user baru MRP Ticketdesk Sushitei Indonesia',
                            'content' => "
                            <div class=\"content-right\">
                            <h1 class=\"well\">Registrasi Berhasil</h1>
                            <div class=\"well\">
                                <div class=\"row\">
                                    Selamat ID MRP Ticketdesk anda sudah dapat digunakan.<br>
                                    Nama user : {$insert_value['user_name']}<br/>
                                    Password  : {$_POST['password']}							</div>
                            </div>
                        </div>",
                            'recipient' => $insert_value['email'],
                            'recipient_name' => $insert_value['display_name'],
                        );
                        $this->send_email($mail_option);
                        header("Location: ".siteUrl().'register_ok.html');
                    } else {
                        header("Location: ".siteUrl().'login.html');
                    }
                }else{
                    $data['error'] = $input->_error;
                    $data['input'] = $_POST;
                    $this->load->template('users/registration_form', $data);
                }
            } else {
                $data['error'] = $input->_error;
                $data['input'] = $_POST;
                $this->setFlashData($validation_data);
                $this->load->template('users/registration_form', $data);
            }
            
        }else{
            $this->showError(1);
        }
    }
    
    private function send_email($option){
        $mail             = $this->load->lib('PHPMailer');
        $smtp             = $this->load->lib('SMTP');
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "mail.sushitei.co.id"; // SMTP server
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $mail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "it1.jkt@sushitei.co.id"; // SMTP account username
        $mail->Password   = "Fangblade_99";        // SMTP account password

        $mail->SetFrom('it1.jkt@sushitei.co.id', 'Rahmat Samsudin');

        $mail->AddReplyTo('it1.jkt@sushitei.co.id', 'Rahmat Samsudin');
        
        $mail->isHTML('true');

        $mail->Subject    = $option['title'];
        
        $mail->MsgHTML($option['content']);

        $mail->AddAddress($option['recipient'], $option['recipient_name']);

        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
          echo "Message sent!";
        }
            
        
    }
    function tests(){
        $data = array(
            '_mySession' => $this->_mySession,
            'flash_data' => $this->getFlashData(),
            'js_folder' => './'.getConfig('resource_dir').'/'.getConfig('js_folder'),
            'css_folder' => './'.getConfig('resource_dir').'/'.getConfig('css_folder'),
        );
        $content = file_get_contents('http://localhost/mrpticket/?s=email&i=register_success&');
        $content = preg_replace("/\\\\/",'',$content);
        echo $content;
    }
}
/*
* End Home Class
*/