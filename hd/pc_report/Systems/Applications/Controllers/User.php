<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* User Controller
*/
class User extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('index', 'insert_category', 'edit_user', 'new_user','my_account'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin QA'),
            ),
            array('Allow', 
                'actions'=>array('my_account'),
                'groups'=>array('Ticket User'),
            ),
            array('Deny', 
                'actions'=>array('index', 'my_account', 'edit_user', 'edit_user', 'new_user'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
	public function load_model(){
		$model = $this->load->model('Users');
		return $model;
	}
	
    
    public function register_ok()
    {
        $data = array(
            '_mySession' => $this->_mySession,
        );
        $this->load->template('users/reg_ok', $data);
    }
    
    public function my_account(){
        $data = array(
            '_mySession' => $this->_mySession,
            'admin' => $this->isAdmin(),
        );
        $account = $this->load->model('Users');
        $store = $this->load->model('Store');
        $data['store'] = $store->getAllActiveStore();
        if(isset($_POST['register']) && $_POST['register'] == 'Daftar' ){
            $input = $this->load->lib('Input');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc','Cek kembali input anda');
            $input->addValidation('email',$_POST['email'],'email','Cek kembali input anda');
            if(!empty($_POST['password'])){
                $input->addValidation('password', $_POST['password'], 'min=6', 'Password minimal 6 huruf');
                $input->addValidation('password_confirm', $_POST['password_confirm'], 'like=' . $_POST['password'], 'Password konfirmasi harus sama dengan password awal');
                $_POST['user_password'] = md5($_POST['password']);
            }
            if($input->validate()){
                $_POST['store_id'] = $_POST['outlet'];
                unset($_POST['outlet']);
                unset($_POST['register']);
                unset($_POST['password']);
                unset($_POST['password_confirm']);
                $where = array('user_id' => $this->_mySession['userid']);
                if($account->editUser($_POST, $where)){
                    $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['userid']));
                    $data['success'] = 1;
                    $this->load->template('users/my_account', $data);
                }else{
                    $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['userid']));
                    $data['success'] = 0;
                    $this->load->template('users/my_account', $data);
                }
            }else{
                $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['userid']));
                $data['success'] = 0;
                $data['error'] = $input->_error;
                $this->load->template('users/my_account', $data);
            }
            
        }else{
            $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['user_id']));
            if(is_array($data['account']) && count($data['account']) > 1){
                $this->load->template('users/my_account', $data);
            }else{
                $this->showError(2);
            }
        }
    }
    
}

/*
End User Controller
*/