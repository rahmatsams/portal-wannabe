$(document).ready(function(){
    $("#mainCategory").change(function(event){
        event.preventDefault();
        changeValue();
    });
    var detail_height = $("#tickettabledetail").height();
    $("#tickettabledesc").css("min-height", detail_height);
    $("#commentbutton").click(function(event){
        var text = "Comment";
        var text2 = "Comment on ticket as additional note.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#assignbutton").click(function(event){
        var text = "Assign";
        var text2 = "Assign the ticket to the listed staff.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleAssign();
    });
    $("#takeoverbutton").click(function(event){
        var text = "Take Over and Close";
        var text2 = "Take over this ticket to assign it to yourself and Close ticket.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#needsparebutton").click(function(event){
        var text = "Need Spare-part";
        var text2 = "Mark this ticket, need to order sparepart.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleSparePart();
    });
    $("#cancelsparebutton").click(function(event){
        var text = "Cancel Spare-part";
        var text2 = "Cancel order sparepart.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#donesparebutton").click(function(event){
        var text = "Sparepart Done";
        var text2 = "Mark this ticket, the ordered sparepart has arrived.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#resolvebutton").click(function(event){
        var text = "Solve Ticket";
        var text2 = "Close your ticket.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleResolve();
    });
    $("#closebutton").click(function(event){
        var text = "Solved ticket";
        var text2 = "Pest Control has done.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleDate();
    });
    
    $("#forcebutton").click(function(event){
        var text = "Force Close";
        var text2 = "Force close the ticket";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#vendorbutton").click(function(event){
        var text = "Vendor";
        var text2 = "Note the ticket need a vendor to solve it";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#reopenbutton").click(function(event){
        var text = "Re-Open Ticket";
        var text2 = "Re-open closed ticket";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#cancelstatus").click(function(event){
        showActivity();
        if($("#submitstatus").html() == "Edit"){
            toggleEdit();
        }
        if($("#submitstatus").html() == "Assign"){
            toggleAssign();
        }
        if($("#submitstatus").html() == "Resolve"){
            toggleResolve();
        }
        $("#tickettitle").html("Ticket Command");
    });
    $("#editbutton").click(function(event){
        var text = "Edit Ticket";
        var text2 = "Edit the ticket detail";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleEdit();
    });
    $("#updateticketstatus").submit(function(event){
        event.preventDefault();
        var me = $(this);
        editTicket(me);
        
    });

    function changeValueType(){
        type=$("#typeCategory").val();
        if(type > 1){ /*Red Alert if(type == 2)*/
            $("#mainCategory").attr("disabled", false);
        }else{
            $("#mainCategory").attr("disabled", true);
        }
        return false;
    }
                        
});