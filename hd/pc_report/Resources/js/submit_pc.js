$(document).ready(function(){
    $("#mainCategory").change(function(event){
        event.preventDefault();
        changeValue();
    });
    $("#onbehalf").change(function(event){
        event.preventDefault();
        if($("#onbehalf").prop("checked") == true){
            changeBehalf();
        } else {
            $("#userlist").attr("disabled", true);
        }
        
    });
    
    $("#addFileButton").click(function(event){
        event.preventDefault();
        num_files = $("#addFile input:file").length;
        if(num_files < 5){
            $("#addFile").append("<input name=\"image_upload[]\" type=\"file\" class=\"form-control\" accept=\"image/jpeg\">");
        }
    });

    $("#typeCategory").change(function(event){
        event.preventDefault();
        changeValueType();
    });

    function changeValueType(){
       /* type=$("#typeCategory").val();
        if(type == 2){*/ /*Red Alert */
        var type = $("#typeCategory").val();
        if(type > 1){
            $("#mainCategory").attr("disabled", false);
        }else{
            $("#mainCategory").attr("disabled", true);
        }
        return false;
    }
    
});