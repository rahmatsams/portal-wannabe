<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Report extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewExportReport', 'viewExportReportOutlet'),
                'groups'=>array('*'),
            ),
        );
    }
    
    public function site()
    {
        $site = array(
            'root' => 'mrp'
        );
        return $site;
    }
    
    /* Other function */
    public function view2ExportReport()
    {
        $ticket = $this->load->model('Ticket');
        $gd = $ticket->getTicketGroup(array('group' => $_GET['id']));
        $tr = $ticket->getTicketByGroup(array('group' => $_GET['id']));
        $location = 'Resources/images/prp/';
        
        header('Content-Type: application/vnd-ms-excel');
        header("Content-Disposition: attachment;filename='{$gd['store_name']}_".date("F Y",strtotime($gd['group_submit_date'])).".xls'");
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        
        echo "<table>
                <tr>
                    <td>Test</td>
                    <td><img src='http://".siteUrl()."{$location}{$tr[0]['finding_images']}'>
                </tr>
              </table>";
    }
    
    //REPORT PER OUTLET
    public function viewExportReportOutlet()
    {
        $ticket = $this->load->model('Ticket');
        $tg = $this->load->model('TicketGroup');
        $input = $this->load->lib('Input');
        if(isset($_GET['n']) && isset($_GET['groupYear']) && isset($_GET['groupMonth'])){
            $input->addValidation('idformat', $_GET['n'], 'numeric', 'Unknown Error');
            $input->addValidation('idlen', $_GET['n'], 'min=1', 'Unknown Error');
            $input->addValidation('idmax', $_GET['n'], 'max=6', 'Unknown Error');
            $input->addValidation('yearformat', $_GET['groupYear'], 'numeric', 'Unknown Error');
            $input->addValidation('yearlen', $_GET['groupYear'], 'min=1', 'Unknown Error');
            $input->addValidation('yearmax', $_GET['groupYear'], 'max=4', 'Unknown Error');
            $input->addValidation('monthformat', $_GET['groupMonth'], 'numeric', 'Unknown Error');
            $input->addValidation('monthlen', $_GET['groupMonth'], 'min=1', 'Unknown Error');
            $input->addValidation('monthmax', $_GET['groupMonth'], 'max=2', 'Unknown Error');
            if($input->validate()){
                $persen = $tg->getTicketPercent(array('store_id' => $_GET['n'], 'group_year' => $_GET['groupYear'],'group_month' => $_GET['groupMonth']));
                $new_data = array();
                foreach ($persen as $key) {
                    $hitung = 100-(($key['rusak']/$key['total'])*100);
                    $hasil = number_format($hitung, 0);
                    array_push($key, $hasil);
                    array_push($new_data, $key);
                }
                $datapercent = $new_data;
                //var_dump($datapercent);exit();

                $gd = $tg->getTicketGroup(array('store_id' => $_GET['n'], 'group_year' => $_GET['groupYear'],'group_month' => $_GET['groupMonth']));
                if(is_array($gd)){
                    $objPHPExcel = $this->load->lib('PHPExcel');

                    #$objPHPExcel = new Spreadsheet();
                    $objPHPExcel->getProperties()->setCreator("Sushitei IT")
                                         ->setLastModifiedBy("Sushitei IT")
                                         ->setTitle("Audit MRP Outlet")
                                         ->setSubject("Audit MRP Outlet")
                                         ->setDescription("In America.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("MRP");

                    $objPHPExcel->setActiveSheetIndex(0)
                                ->mergeCells('A1:E3')
                                ->setCellValue('A1', "FORM PEMERIKSAAN PERAWATAN ".strtoupper($gd[0]['store'])." / ".strtoupper(date("F Y",strtotime($gd[0]['group_submit_date'])))." " )
                                
                                
                                //TABLE
                                ->mergeCells('A5:A6')
                                ->setCellValue('A5', 'NO')
                                ->mergeCells('B5:B6')
                                ->setCellValue('B5', 'Nama Alat')
                                ->mergeCells('C5:C6')
                                ->setCellValue('C5', 'Code')
                                ->mergeCells('D5:D6')
                                ->setCellValue('D5', 'Kondisi')
                                ->mergeCells('E5:E6')
                                ->setCellValue('E5', 'Catatan');
                                
                    $sheet = $objPHPExcel->getActiveSheet();
                    $sheet->setShowGridlines(false);
                    $styleA1 = array(
                            'font'  => array(
                            'size'  => 14,
                            'name'  => 'Trebuchet MS'
                        ));

                        $colA = $sheet->getStyle('A1');
                        $colA->applyFromArray($styleA1);
                    $A1 = $sheet->getStyle('A1');
                    $A1->getAlignment()->applyFromArray(
                        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                        )
                    );
                    $A1->getFont()->setBold( true );
                    $A11 = $sheet->getStyle('A5:E6');
                    $A11->getAlignment()->applyFromArray(
                        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    );
                    $A11->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
                    $A11->getFont()->setBold( true );
                    $first = 5;
                    $last = 7;
                    $num = 1;
                    $current_week = '';
                    if(is_array($gd) && count($gd) > 0){ 
                        foreach($gd as $data){
                            $monthly = $data['group_week'];
                            if($current_week != $data['group_week']){
                                foreach ($datapercent as $value) {
                                    if ($value['group_week'] == $data['group_week']) {
                                        $retVal = $value[0];
                                    }
                                }
                                
                                $sheet->mergeCells("A{$last}:E{$last}")->setCellValue("A{$last}", $data['group_week'] == 0 ? 'MRP Periode Bulanan' : 'MRP Periode Minggu '.$monthly);

                                $last++;
                                $current_week = $data['group_week'];
                            }
                            
                            $sheet->setCellValue("B{$last}", $data['area'])
                                  ->setCellValue("A{$last}", $num)
                                  ->setCellValue("C{$last}", $data['equipment_code'] )
                                  ->setCellValue("D{$last}", $data['group_status'] )
                                  ->setCellValue("E{$last}", $data['group_notes']);

                            $last++;
                            $num++;
                            
                        }
                        //KOLOM PERSENTASE
                        $awal = $last;
                        $awal2 = $awal+1;
                        $akhir = $awal2+5;
                        $awal3 = $akhir+1;
                        $sheet->mergeCells("A{$awal}:B{$awal}")
                            ->setCellValue("A{$awal}", 'Equipment Availability');
                            
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                    'wrap'            => TRUE
                                )
                            );
                            

                        $sheet->mergeCells("C{$awal}:E{$awal}")
                            ->setCellValue("C{$awal}", $retVal. '%');
                            
                        $styleA = array(
                            'borders' => array(
                                'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        );
                        $colA = $sheet->getStyle('A'.$awal.':B'.$awal); 
                        $colA->applyFromArray($styleA);
                        $colC = $sheet->getStyle('C'.$awal.':E'.$awal);
                        $colC->applyFromArray($styleA);

                    }else{
                        $sheet->mergeCells("A{$last}:E{$last}")
                              ->setCellValue("A{$last}", 'No Data Found');
                    }

                    $styleBorder = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );

                    $b = $last-1;
                    $tc = $sheet->getStyle("A{$first}:E{$b}");
                    $tc->applyFromArray($styleBorder);
                    // Rename worksheet
                    $objPHPExcel->getActiveSheet()->setTitle(" Outlet ");

                    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                    $objPHPExcel->setActiveSheetIndex(0);
                    
                    // Redirect output to a client’s web browser (Excel2007)
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header("Content-Disposition: attachment;filename='{$gd['0']['store']}_".date("F Y",strtotime($gd[0]['group_submit_date'])).".xlsx'");
                    header('Cache-Control: max-age=0');
                    // If you're serving to IE 9, then the following may be needed
                    header('Cache-Control: max-age=1');

                    // If you're serving to IE over SSL, then the following may be needed
                    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                    header ('Pragma: public'); // HTTP/1.0
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save('php://output');
                }
            }
    
        }
    }

    //REPORT PER EQUIPMENT
    public function viewExportReport()
    {
        $ticket = $this->load->model('Ticket');
        $tg = $this->load->model('TicketGroup');
        $input = $this->load->lib('Input');
        if(isset($_GET['id'])){
            $input->addValidation('group_id', $_GET['id'], 'numeric', 'Must be number');
            $input->addValidation('group_id', $_GET['id'], 'min=1', 'Must be filled');
            $input->addValidation('group_id', $_GET['id'], 'max=11', 'Limit reached');
            if($input->validate()){
                
                $tr = $ticket->getTicketByGroup(array('group' => $_GET['id']));
                $gd = $tg->getTicketGroupDetail(array('group' => $_GET['id']));
                if(is_array($gd)){
                    $objPHPExcel = $this->load->lib('PHPExcel');
                    $monthly = $gd['group_week'];
                    $eq = $gd['equipment'];
                    $ty = $gd['group_notes'];
                    $ts = $gd['group_status'];
                    #$objPHPExcel = new Spreadsheet();
                    $objPHPExcel->getProperties()->setCreator("Sushitei IT")
                                         ->setLastModifiedBy("Sushitei IT")
                                         ->setTitle("Audit MRP Outlet")
                                         ->setSubject("Audit MRP Outlet")
                                         ->setDescription("In America.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("MRP");

                    $objPHPExcel->setActiveSheetIndex(0)
                                ->mergeCells('A1:E3')
                                ->setCellValue('A1', "FORM PEMERIKSAAN PERAWATAN ".strtoupper($gd['equipment'])." " )
                                
                                ->setCellValue('A5', 'Nama Outlet')
                                ->setCellValue('B5', ": {$gd['store_name']}")
                                ->setCellValue('A6', 'Tanggal')
                                ->setCellValue('B6', ": ".date("d F Y",strtotime($gd['group_submit_date']))." ")
                                ->setCellValue('A7', 'Waktu / jam mulai pekerjaan')
                                ->setCellValue('B7', ": ".date("H:i ",strtotime($gd['group_submit_date']))." WIB             ")
                                ->setCellValue('A8', 'Waktu / jam selesai pekerjaan')
                                ->setCellValue('B8', ": ………………………… WIB            ")
                                
                                ->setCellValue('C5', 'Jenis Kunjungan Perawatan')
                                ->setCellValue('D5', ": Perawatan ".($gd['group_week'] == 0 ? 'Bulanan' : 'Mingguan ')." ")
                                #->setCellValue('E5', " ".($gd['group_week'] == 0 ? 'Bulanan' : 'Minggu '.$monthly)." ")
                                ->setCellValue('C6', 'Kode Alat')
                                ->setCellValue('D6', ":  {$gd['equipment_code']}") 
                                #->setCellValue('D9', 'NAMA AUDITEE')
                                #->setCellValue('E9', ": {$gd['group_auditee']}")
                                
                                //TABLE
                                ->mergeCells('A10:A11')
                                ->setCellValue('A10', 'NO')
                                ->mergeCells('B10:B11')
                                ->setCellValue('B10', 'ITEM PEMERIKSAAN / PERAWATAN')
                                ->mergeCells('C10:C11')
                                ->setCellValue('C10', 'HASIL MAINTENANCE')
                                ->mergeCells('D10:D11')
                                ->setCellValue('D10', 'FOTO HASIL MAINTENANCE')
                                ->mergeCells('E10:E11')
                                ->setCellValue('E10', 'KETERANGAN / KONDISI');
                                
                    $sheet = $objPHPExcel->getActiveSheet();
                    $sheet->setShowGridlines(false);
                    $styleA1 = array(
                            'font'  => array(
                            'size'  => 16,
                            'name'  => 'Trebuchet MS'
                        ));

                        $colA = $sheet->getStyle('A1');
                        $colA->applyFromArray($styleA1);
                    
                    $C6 = array(
                            'font'  => array(
                            'color' => array('rgb' => 'FF5733'),
                            'italic' => true,
                            'bold' => true
                        ));

                        $colA = $sheet->getStyle('C6');
                        $colA->applyFromArray($C6);
                    $D6 = array(
                            'font'  => array(
                            'color' => array('rgb' => 'FF5733'),
                            'italic' => true,
                            'bold' => true
                        ));

                        $colA = $sheet->getStyle('D6');
                        $colA->applyFromArray($D6);

                    $A1 = $sheet->getStyle('A1');
                    $A1->getAlignment()->applyFromArray(
                        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                        )
                    );
                    $A1->getFont()->setBold( true );
                    $A11 = $sheet->getStyle('A10:E11');
                    $A11->getAlignment()->applyFromArray(
                        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    );
                    $A11->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
                    $A11->getFont()->setBold( true );
                    $first = 10;
                    $last = 12;
                    $num = 1;
                    $last_cat = '';
                    if(count($tr) > 0){ 
                        foreach($tr as $data){
                            if($last_cat != $data['category_name']){
                                $sheet->mergeCells("A{$last}:E{$last}")->setCellValue("A{$last}", $data['category_name']);
                                $last++;
                                $last_cat = $data['category_name'];
                            }
                            $sheet->setCellValue("B{$last}", $data['condition_name'])
                                    ->setCellValue("A{$last}", $num)
                                    ->setCellValue("C{$last}", $data['status'] == 1 ? 'Ya' : 'Tidak' )
                                    ->setCellValue("E{$last}", $data['notes']);

                            $location = 'Resources/images/prp/';
                            if(!empty($data['finding_images']) && file_exists($location.$data['finding_images'])){
                                #$gdImage = imagecreatefromjpeg($location.$data['finding_images']);
                                // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Finding Image');
                                $objDrawing->setDescription('Finding Image');
                                $objDrawing->setPath($location.$data['finding_images']);
                                $objDrawing->setHeight(150);
                                $objDrawing->setCoordinates("D{$last}");
                                $objDrawing->setOffsetY(10);
                                $objDrawing->setOffsetX(10);
                                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                                $sheet->getRowDimension($last)->setRowHeight(150);
                                $sheet->getColumnDimension('D')->setWidth(30);
                            }
                            
                            $last++;
                            $num++;
                            
                        }
                        //kolom ttd teknisi
                        $awal = $last;
                        $awal2 = $awal+1;
                        $akhir = $awal2+5;
                        $awal3 = $akhir+1;
                        $sheet->mergeCells("A{$awal}:B{$awal}")
                            ->setCellValue("A{$awal}", 'Pelaksana')
                            ->mergeCells("A{$awal2}:B{$akhir}")
                            ->setCellValue("A{$awal2}", "( ……………………………………………… )")
                            ->mergeCells("A{$awal3}:B{$awal3}")
                            ->setCellValue("A{$awal3}", "Teknisi"); 
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                    'wrap'            => TRUE
                                )
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal2}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                                )
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal3}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                    'wrap'            => TRUE
                                )
                            );

                        $sheet->mergeCells("C{$awal}:E{$awal}")
                            ->setCellValue("C{$awal}", 'Kondisi '.$eq. ': '.$ts)
                            ->mergeCells("C{$awal2}:E{$awal3}")
                            ->setCellValue("C{$awal2}", "Catatan:\n\n\n\n\n");
                            $objPHPExcel->getActiveSheet()->getStyle("C{$awal2}")->getAlignment()->applyFromArray(
                                array(
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
                                    'wrap'            => TRUE
                                )
                            );
                        
                        $styleA = array(
                            'borders' => array(
                                'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        );
                        $colA = $sheet->getStyle('A'.$awal.':B'.$awal3); 
                        $colA->applyFromArray($styleA);
                        $colC = $sheet->getStyle('C'.$awal.':E'.$awal3);
                        $colC->applyFromArray($styleA);

                        //next kolom ttd mod/cod
                        $awal = $awal3+1;
                        $awal2 = $awal+1;
                        $akhir = $awal2+5;
                        $awal3 = $akhir+1;
                        $sheet->mergeCells("A{$awal}:B{$awal}")
                            ->setCellValue("A{$awal}", 'Diperiksa')
                            ->mergeCells("A{$awal2}:B{$akhir}")
                            ->setCellValue("A{$awal2}", "( ……………………………………………… )")
                            ->mergeCells("A{$awal3}:B{$awal3}")
                            ->setCellValue("A{$awal3}", "MOD/COD");
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                    'wrap'            => TRUE
                                )
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal2}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                                )
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal3}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                    'wrap'            => TRUE
                                )
                            );

                        $sheet->mergeCells("C{$awal}:E{$awal}")
                            ->setCellValue("C{$awal}", 'Catatan: ')
                            ->mergeCells("C{$awal2}:E{$awal3}")
                            ->setCellValue("C{$awal2}", "");
                            $objPHPExcel->getActiveSheet()->getStyle("C{$awal2}")->getAlignment()->applyFromArray(
                                array(
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
                                    'wrap'            => TRUE
                                )
                            );                        
                        $styleA = array(
                            'borders' => array(
                                'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        );
                        $colA = $sheet->getStyle('A'.$awal.':B'.$awal3);
                        $colA->applyFromArray($styleA);
                        $colC = $sheet->getStyle('C'.$awal.':E'.$awal3);
                        $colC->applyFromArray($styleA);

                        //kolom ttd mrp supervisor
                        $awal = $awal3+1;
                        $awal2 = $awal+1;
                        $akhir = $awal2+5;
                        $awal3 = $akhir+1;
                        $sheet->mergeCells("A{$awal}:B{$awal}")
                            ->setCellValue("A{$awal}", 'Mengetahui')
                            ->mergeCells("A{$awal2}:B{$akhir}")
                            ->setCellValue("A{$awal2}", "( ……………………………………………… )")
                            ->mergeCells("A{$awal3}:B{$awal3}")
                            ->setCellValue("A{$awal3}", "MRP Supervisor");
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                    'wrap'       => TRUE
                                )
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal2}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                                )
                            );
                            $objPHPExcel->getActiveSheet()->getStyle("A{$awal3}")->getAlignment()->applyFromArray(
                                array(
                                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                    'wrap'            => TRUE
                                )
                            );

                        $sheet->mergeCells("C{$awal}:E{$awal}")
                            ->setCellValue("C{$awal}", 'Catatan: ')
                            ->mergeCells("C{$awal2}:E{$awal3}")
                            ->setCellValue("C{$awal2}", "");
                            $objPHPExcel->getActiveSheet()->getStyle("C{$awal2}")->getAlignment()->applyFromArray(
                                array(
                                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
                                    'wrap'            => TRUE
                                )
                            );

                        $styleA = array(
                            'borders' => array(
                                'left' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'right' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'bottom' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                ),
                                'top' => array(
                                    'style' => PHPExcel_Style_Border::BORDER_THIN
                                )
                            )
                        );

                        $colA = $sheet->getStyle('A'.$awal.':B'.$awal3);
                        $colA->applyFromArray($styleA);
                        $colC = $sheet->getStyle('C'.$awal.':E'.$awal3);
                        $colC->applyFromArray($styleA);

                        //CATATAN
                        $awal = $awal3+1;
                         $sheet->mergeCells("A{$awal}:E{$awal}")
                            ->setCellValue("A{$awal}", 'Catatan: Mohon disertai nama jelas dan tanggal pada kolom tanda tangan');
                        $styleA = array(
                            'font'  => array(
                            'italic'  => true,
                            'size'  => 10,
                        ));

                        $colA = $sheet->getStyle('A'.$awal.':E'.$awal);
                        $colA->applyFromArray($styleA);

                    }else{
                        $sheet->mergeCells("A{$last}:E{$last}")
                              ->setCellValue("A{$last}", 'No Data Found');
                    }
                    $styleBorder = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );
                    $b = $last-1;
                    $tc = $sheet->getStyle("A{$first}:E{$b}");
                    $tc->applyFromArray($styleBorder);
                    // Rename worksheet
                    $objPHPExcel->getActiveSheet()->setTitle('MRP Sushitei');

                    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                    $objPHPExcel->setActiveSheetIndex(0);
                    
                    // Redirect output to a client’s web browser (Excel2007)
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header("Content-Disposition: attachment;filename='{$gd['store_name']}_{$gd['equipment']}_".date("F Y",strtotime($gd['group_submit_date']))."_".($gd['group_week'] == 0 ? 'Bulanan' : 'Minggu '.$monthly).".xlsx'");
                    header('Cache-Control: max-age=0');
                    // If you're serving to IE 9, then the following may be needed
                    header('Cache-Control: max-age=1');

                    // If you're serving to IE over SSL, then the following may be needed
                    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                    header ('Pragma: public'); // HTTP/1.0
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    $objWriter->save('php://output');
                }
            }
    
        }
    }
}
/*
* End Home Class
*/