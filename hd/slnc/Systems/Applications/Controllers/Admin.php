<?php
class Admin extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Guest'),
            ),
            array('Allow', 
                'actions'=>array('viewIndex','viewExport', 'store_index', 'store_insert', 'store_edit'),
                'groups'=>array('Super Admin','Administrator'),
            ),
            array('Allow', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Admin SLNC'),
            ),
            array('Allow', 
                'actions'=>array('viewExport', 'phpExcel'),
                'groups'=>array('Ticket Users', 'Admin SLNC', 'Admin View', 'PIC SLNC', 'Admin PQNC SLNC', 'Warehouse', 'Purchasing', 'Central Kithen'),
            ),
        );
    }
    
    function viewIndex()
    {
        $data = array(
            'menu'=>'adminmenu',
            'session' => $this->_mySession,
        );
        $option = array(
            'admin' => $this->isAdmin(),
            'menu'=>'adminmenu',
            'session' => $this->_mySession,
        );
        $this->load->template('admin/index',$data,$option);
    }
    
    function viewExport()
    {
        $data = $this->load->model('Tickets');
        $option = array(
            'file_name' => 'SLNC_Report',
            'context_title' => 'Report SLNC'
        );
        $query_option = array(
            'order_by' =>'submit_date','po_number',
            'order' => 'DESC',
            'result' => 50000
        );
        if(empty($this->_mySession['last_query']) || !is_array($this->_mySession['last_query'])){
            if($this->isAdmin()){
                $query = array();
            }else{
                $query['user'] = $this->_mySession['user_id'];
            }
        }else{
            $query = $this->_mySession['last_query'];
        }
        $dataTicket = $data->showAllTicketFiltered($query, $query_option);
        $this->phpExcel($dataTicket, $option);
        #print_r($dataTicket);
        #exit();
    }
    
    function phpExcel($ticket_result, $option)
    {
        $objPHPExcel = $this->load->lib('PHPExcel');
        try{
            $objPHPExcel->getProperties()->setCreator("Sushitei IT")
                                 ->setLastModifiedBy("Sushitei IT")
                                 ->setTitle("SLNC")
                                 ->setSubject("Report")
                                 ->setDescription("Hasil export SLNC.")
                                 ->setKeywords("SLNC Export")
                                 ->setCategory("Export SLNC");

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1')
                        ->setCellValue('A1', $option['context_title'])
                        ->mergeCells('A3:A4')
                        ->setCellValue('A3', 'No.')
                        #->mergeCells('B3')
                        ->setCellValue('B3', 'PO/DO')
                        ->setCellValue('B4', 'Nomor PO/DO')
                        ->mergeCells('C3:D3')
                        ->setCellValue('C3', 'Pemohon')
                        ->setCellValue('C4', 'Nama')
                        ->setCellValue('D4', 'Outlet')
                        ->mergeCells('E3:H3')
                        ->setCellValue('E3', 'Registrasi Tiket')
                        ->setCellValue('E4', 'No Tiket')
                        ->setCellValue('F4', 'Tanggal Registrasi')
                        ->setCellValue('G4', 'Tanggal Respon')
                        ->setCellValue('H4', 'Waktu Respon')
                        ->mergeCells('I3:M3')
                        ->setCellValue('I3', 'Barang')
                        ->setCellValue('I4', 'Nama Barang')
                        ->setCellValue('J4', 'Kode Barang')
                        ->setCellValue('K4', 'Kategori Supplier')
                        ->setCellValue('L4', 'Supplier')
                        ->setCellValue('M4', 'Penyimpangan')
                        ->mergeCells('N3:O3')
                        ->setCellValue('N3', 'PO/DO')
                        ->setCellValue('N4', 'Tanggal Order')
                        ->setCellValue('O4', 'Tanggal Terima')
                        ->mergeCells('P3:S3')
                        ->setCellValue('P3', 'Detail Barang')
                        ->setCellValue('P4', 'UOM')
                        ->setCellValue('Q4', 'Jumlah Pesan')
                        ->setCellValue('R4', 'Jumlah Diterima')
                        ->setCellValue('S4', 'Selisih')
                        ->mergeCells('T3:U3')
                        ->setCellValue('T3', 'Outlet')
                        ->setCellValue('T4', 'Supply PIC')
                        ->setCellValue('U4', 'Note PIC')
                        ->mergeCells('V3:W3')
                        ->setCellValue('V3', 'Status')
                        ->setCellValue('V4', 'Closed')
                        ->setCellValue('W4', 'Tanggal Close');

            // Miscellaneous glyphs, UTF-8
            if(is_array($ticket_result) && !empty($ticket_result)){
                $no = 1;
                $row = 5;
                $temporary = "";
                $nothing = "";
                $last_warna = "D9D9D9";
                $status = array('Unknown','Open','On Progress','Pending','Need Vendor','Solved','Closed');
                $priority = array('Unknown','ASAP','High','Medium','Low');
                $sheet = $objPHPExcel->getActiveSheet(); #inisiasi active sheet dari object excel
                #$deteksi_data_ke = 0;
                foreach($ticket_result as $result){
                    #Warna
                    $warnai = $sheet->getStyle("A{$row}:W{$row}");
                    #Data
                    if ($temporary == $result['po_number']) {
                        $warnai->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($last_warna);
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue("A{$row}", $no)
                                ->setCellValue("B{$row}", $nothing)
                                ->setCellValue("C{$row}", $result['user_name'])
                                ->setCellValue("D{$row}", $result['store_name'])
                                ->setCellValue("E{$row}", $result['ticket_id'])
                                ->setCellValue("F{$row}", date('m/d/Y', strtotime($result['submit_date'])))
                                ->setCellValue("G{$row}", (!empty($result['resolved_date']) ? date('m/d/Y', strtotime($result['resolved_date'])) : ''))
                                ->setCellValue("H{$row}", "=IF(ISBLANK(F{$row})=FALSE,G{$row}-F{$row},0)")
                                ->setCellValue("I{$row}", $result['item_name'])
                                ->setCellValue("J{$row}", $result['item_code'])
                                ->setCellValue("K{$row}", $result['category_name'])
                                ->setCellValue("L{$row}", $result['supplier'])
                                ->setCellValue("M{$row}", $result['type_name'])
                                //PO No was here
                                ->setCellValue("N{$row}", $result['order_date'])
                                ->setCellValue("O{$row}", $result['rcv_date'])
                                ->setCellValue("P{$row}", $result['item_uom'])
                                ->setCellValue("Q{$row}", $result['order_qty'])
                                ->setCellValue("R{$row}", $result['rcv_qty'])
                                ->setCellValue("S{$row}", "=Q{$row}-R{$row}")
                                ->setCellValue("T{$row}", $result['staff_name'])
                                ->setCellValue("U{$row}", $result['resolved_solution'])
                                ->setCellValue("V{$row}", $result['close_status'] == 1 ? 'Yes' : ($result['close_status'] == 2 ? 'Forced' : 'No'))
                                ->setCellValue("W{$row}", (!empty($result['close_time']) ? date('m/d/Y', strtotime($result['close_time'])) : ''));

                                $no++;
                                $row++;
                    }else{
                        #$deteksi_data_ke = $deteksi_data_ke + 1;
                        if($last_warna == "D9D9D9"){
                            $last_warna = 'FFFFFF';
                        }else{
                            $last_warna = 'D9D9D9';
                        }
                        $warnai->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($last_warna);
                        /*if ($deteksi_data_ke % 2 == 1) { #row 0+1 mod 2 akan dwarnai
                            #$warnai = $sheet->getStyle("A{$row}:W{$row}"); #diwarna sheet A sampai W sesuai dengan row nya #GREY
                            $last_warna = "D9D9D9";
                            $warnai->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9'); #digunakan untuk mewarnai background sheet
                        }else{
                            $last_warna = "FFFFFF"; #warnai putih
                            $warnai->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFFFF');
                        }*/
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue("A{$row}", $no)
                                ->setCellValue("B{$row}", $result['po_number'])
                                ->setCellValue("C{$row}", $result['user_name'])
                                ->setCellValue("D{$row}", $result['store_name'])
                                ->setCellValue("E{$row}", $result['ticket_id'])
                                ->setCellValue("F{$row}", date('m/d/Y', strtotime($result['submit_date'])))
                                ->setCellValue("G{$row}", (!empty($result['resolved_date']) ? date('m/d/Y', strtotime($result['resolved_date'])) : ''))
                                ->setCellValue("H{$row}", "=IF(ISBLANK(F{$row})=FALSE,G{$row}-F{$row},0)")
                                ->setCellValue("I{$row}", $result['item_name'])
                                ->setCellValue("J{$row}", $result['item_code'])
                                ->setCellValue("K{$row}", $result['category_name'])
                                ->setCellValue("L{$row}", $result['supplier'])
                                ->setCellValue("M{$row}", $result['type_name'])
                                //PO No was here
                                ->setCellValue("N{$row}", $result['order_date'])
                                ->setCellValue("O{$row}", $result['rcv_date'])
                                ->setCellValue("P{$row}", $result['item_uom'])
                                ->setCellValue("Q{$row}", $result['order_qty'])
                                ->setCellValue("R{$row}", $result['rcv_qty'])
                                ->setCellValue("S{$row}", "=Q{$row}-R{$row}")
                                ->setCellValue("T{$row}", $result['staff_name'])
                                ->setCellValue("U{$row}", $result['resolved_solution'])
                                ->setCellValue("V{$row}", '')
                                ->setCellValue("W{$row}", '');

                                $no++;
                                $row++;
                                $temporary = $result['po_number'];
                    }
                    
                }
            } else {
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:J4')
                        ->setCellValue('A4', "Tiket kosong");
            }
            
            // Rename worksheet
            $objPHPExcel->getActiveSheet()->setTitle('SLNC Report');


            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename={$option['file_name']}.xlsx");
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        }catch(Exception $e){
            echo $e->getTraceAsTring();
        }
    }
    
    function store_index()
    {
        
        $model_store = $this->load->model('Store');
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'store_name',
            'order' => 'ASC'
        );
        $option = array(
            'admin' => $this->isAdmin()
        );
        $data = array(
            '_mySession' => $this->_mySession,
            'store' => $model_store->getAllStore($query_option),
            'total' => $model_store->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
        );
        $this->load->template('admin/index_store', $data, $option);
    }
    
    function store_insert(){
        $model_store = $this->load->model('Store');
        $data = array(
            '_mySession' => $this->_mySession,
            'location_list' => $model_store->getAllActiveLocation()
        );
        $option = array(
            'admin' => $this->isAdmin()
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'create_store'){
            $input = $this->load->lib('Input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->newStore($store)){
                    header("Location: admin_store.html");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/add_store', $data, $option);
            }
        }else{
            $this->load->template('admin/add_store', $data, $option);
        }
        
    }
    
    function store_edit(){
        $model_store = $this->load->model('Store');
        $input = $this->load->lib('Input');
        $data = array(
            '_mySession' => $this->_mySession
        );
        $option = array(
            'admin' => $this->isAdmin()
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_store'){
            $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->editStore($store, array('store_id' => $_GET['id']))){
                    header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/edit_store', $data);
            }
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                if($input->validate()){
                    $data['store'] = $model_store->getStoreBy(array('store_id' => $_GET['id']));
                    if(is_array($data['store']) && count($data['store']) > 1){
                        $data['location_list'] = $model_store->getAllActiveLocation();
                        $this->load->template('admin/edit_store', $data);
                    }
                }
                
            }
        }
        
    }
}
?>