<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ticket extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewEditTicket', 'viewSubmit'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewEditTicket','viewSubmit'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
    public function viewSubmit()
    {
        $ticket = $this->load->model('Tickets');
        $data = array(
            'session' => $this->_mySession,
            'main_category' => $ticket->getMainCategory(),
            'type' => $ticket->getTicketType()
        );
        
        $option = array(
            'admin' => $this->isAdmin(),
            'personal_js' => 1,
            'scriptload' => '
		<script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
        ');
        
        $this->load->template('ticket/submit', $data, $option);
        
    }
    
    function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
    
    public function viewEditTicket()
    {
        $input = $this->load->lib('Input');
        $input->addValidation('ticket_id', $_GET['n'], 'numeric', 'Terjadi kesalahan');
        if($input->validate()){
            $ticket = $this->load->model('Tickets');
            $m_users = $this->load->model('Users');
            $query = array(
                'my_id' => (isset($this->_mySession['user_id']) ? $this->_mySession['user_id'] : 0),
                'ticket_id' => $_GET['n']
            );
            $option = array(
                'admin' => $this->isAdmin(),
                'personal_js' => 1,
                'scriptload' => '
                    <script type="text/javascript">
                    $(document).ready(function(){
                        $("#mainCategory").change(function(event){
                            event.preventDefault();
                            changeValue();
                        });
                        var detail_height = $("#tickettabledetail").height();
                        $("#tickettabledesc").css("min-height", detail_height);
                        $("#commentbutton").click(function(event){
                            var text = "Comment";
                            var text2 = "Comment on ticket as additional note.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#takeoverbutton").click(function(event){
                            var text = "Take Over";
                            var text2 = "Take Over ticket to you.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#assignbutton").click(function(event){
                            var text = "Assign";
                            var text2 = "Assign ticket to the listed staff.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                            toggleAssign();
                        });
                        $("#releasebutton").click(function(event){
                            var text = "Release Item";
                            var text2 = "Release Item after closing it.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#returnbutton").click(function(event){
                            var text = "Return Item";
                            var text2 = "Return item after closing it.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#rejectbutton").click(function(event){
                            var text = "Reject Item";
                            var text2 = "Reject Item.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#closebutton").click(function(event){
                            var text = "Close";
                            var text2 = "Mark the ticket as Closed";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        
                        $("#forcebutton").click(function(event){
                            var text = "Force Close";
                            var text2 = "Force close the ticket";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#vendorbutton").click(function(event){
                            var text = "Vendor";
                            var text2 = "Note the ticket need a vendor to solve it";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#reopenbutton").click(function(event){
                            var text = "Re-Open Ticket";
                            var text2 = "Re-open closed ticket";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#cancelstatus").click(function(event){
                            showActivity();
                            if($("#submitstatus").html() == "Edit"){
                                toggleEdit();
                            }
                            if($("#submitstatus").html() == "Assign"){
                                toggleAssign();
                            }
							if($("#submitstatus").html() == "Resolve"){
                                toggleResolve();
                            }
                            $("#tickettitle").html("Ticket Command");
                        });
                        $("#editbutton").click(function(event){
                            var text = "Edit Ticket";
                            var text2 = "Edit the ticket detail";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                            toggleEdit();
                        });
                        $("#updateticketstatus").submit(function(event){
                            event.preventDefault();
                            editTicket();
                        });
                        
                    });
                    </script>'
            );
            if($this->isAdmin()){
                unset($query['my_id']);
            }
            $data = array(
                'admin' => $this->isAdmin(),
                'session' => $this->_mySession,
                'ticket' => $ticket->showTicketBy($query),
                'main_category' => $ticket->getMainCategory(),
                'ticket_type' => $ticket->getTicketType(),
                'uploaded' => $ticket->getUploadedFile(array('ticket_id'=> $_GET['n'])),
                'log' => $ticket->getLog(array('ticket_id'=> $_GET['n'])),
                'staff' => $ticket->getAllSLNC()
            );
            $this->load->template('ticket/edit', $data, $option);
        }
    }
    
    private function send_email($option, $data){
        $mail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail = $this->load->lib('STMail');
        $model_log = $this->load->model('Tickets');
        $log_data = $model_log->getLog(array('ticket_id' => $data['ticket_id']));
        $stmail->IsSMTP(); // telling the class to use SMTP
        $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
        $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $stmail->SMTPAuth   = true;                  // enable SMTP authentication
        $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $stmail->Username   = "qa.helpdesk@sushitei.co.id"; // SMTP account username
        $stmail->Password   = "St123456";        // SMTP account password

        $stmail->SetFrom('qa.helpdesk@sushitei.co.id', 'QA Helpdesk');

        $stmail->AddReplyTo('qa.helpdesk@sushitei.co.id', 'QA Helpdesk');
        
        $stmail->isHTML('true');

        $stmail->Subject    = $option['title'];
        
        $stmail->MsgHTML($stmail->mailContent($data, $log_data));
        
        $stmail->AddAddress($option['recipient'], $option['recipient_name']);
        
        foreach($model_log->getMailRecipient() as $recipient){
            $stmail->AddBCC($recipient['email'], $recipient['display_name']);
        }

        #$stmail->AddBCC('purchteam.jkt@sushitei.co.id', 'Team Purchasing');
        #$stmail->AddBCC('oktavianus@sushitei.co.id', 'Oktavianus');
        #$stmail->AddBCC('m.chandra@sushitei.co.id', 'M. Chandra');
        #$stmail->AddBCC('dadang@sushi-kiosk.com', 'Dadang');
        #$stmail->AddBCC('arief@sushi-kiosk.com', 'Arief');
        
        return ($stmail->Send() ? 1 : 0);
            
        
    }
}
/*
* End Home Class
*/