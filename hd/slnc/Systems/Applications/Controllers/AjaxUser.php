<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxUser extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewLoginData', 'viewRegister', 'viewForgot'),
                'role'=>array('*'),
            ),
            array('Allow', 
                'actions'=>array('viewNewRoom','viewEditRoom','viewRoomData','viewProviderData','viewNewProvider','viewEditProvider','viewAdminEventOption','viewEditEvent'),
                'role'=>array('Administrator', 'Super Admin'),
            ),
            array('Deny', 
                'actions'=>array('viewEventList', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
    protected $message;
    
    public function viewLoginData()
    {
		if($this->isGuest()){
            $current_attempt = $this->_mySession['attempt'];
            $recaptcha = 1;
            if($current_attempt > 5){
                #
                # Verify captcha
                $post_data = http_build_query(
                    array(
                        'secret' => '6LemwyETAAAAABp7aR2EScUPsLoLwCSHPvGMQTpH',
                        'response' => $_POST['g-recaptcha-response'],
                        'remoteip' => $_SERVER['REMOTE_ADDR']
                    )
                );
                $opts = array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $post_data
                    )
                );
                $context  = stream_context_create($opts);
                $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
                $result = json_decode($response);
                if (!$result->success) {
                    $recaptcha = 0;
                }
			}
            if(isset($_POST['ticket_email']) && isset($_POST['ticket_password']) && $recaptcha == 1){
                $input = $this->load->lib('Input');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'min=1', 'Email Address must be filled');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'username', 'Email not recognized');
                $input->addValidation('ticket_password', $_POST['ticket_password'], 'min=1', 'Password must be filled');
                if ($input->validate()) {
                    $login_data = array(
                        'user_name' => $_POST['ticket_email'],
                        'user_password' => MD5($_POST['ticket_password'].getConfig('salt')),
                    );
                    $model_users = $this->load->model('Users');
                    $result = $model_users->doLogin($login_data);
                    if(is_array($result) && count($result) > 0){
                        $this->setSession('userid',$result['user_id']);
                        $this->setSession('username',$result['display_name']);
                        $this->setSession('outlet',$result['store_id']);
                        $this->setSession('group',$result['group_name']);
                        $this->setSession('role',$result['level']);
                        $this->setSession('email',$result['email']);
                        $this->setSession('attempt', 0);
                        $return = array(
                            'success' => 1,
                            'last_url' => $_POST['last_url']
                        );
                    } else {
                        $current_attempt++;
                        $this->setSession('attempt', $current_attempt);
                        $return = array(
                            'login_attempt' => $current_attempt,
                            'message' => "Username or Password does not match.",
                            'success' => 0
                        );
                        
                    }
                } else {
                    $current_attempt++;
                    $this->setSession('attempt', $current_attempt);
                    $return = array(
                        'login_attempt' => $current_attempt,
                        'error' => $input->_error,
                        'success' => 0
                    );
                }
            
            } else {
                $current_attempt++;
                $return = array(
                    'login_attempt' => $current_attempt,
                    'success' => 0,
                    'data' => $_POST
                );
                
            }
        
            echo json_encode($return);
		}else{
            $return = array(
                'success' => 1
            );
             echo json_encode($return);
        }
	}
	
    
    
    public function getUserDetail()
    {
        if(isset($_POST['uid'])){
            $input = $this->load->lib('Input');
            $input->addValidation('uid', $_POST['uid'] ,'numeric','Terjadi kesalahan');
            if($input->validate()){
                $data = array(
                    'user_id' => $_POST['uid']
                );
                $m_users = $this->load->model('Users');
                $result = $m_users->getUserBy($data);
                if(is_array($result) && !empty($result)){
                    echo json_encode($result);
                } else {
                    echo 'false';
                }
            }
    
        
        } else {
            echo 'false';
        }
    }
    
    public function getUserList()
    {
        if(isset($_POST['user'])){
            $input = $this->load->lib('Input');
            $input->addValidation('user', $_POST['user'] ,'numeric','Terjadi kesalahan');
            if($input->validate()){
                $model_ticket = $this->load->model('Users');
                $result = $model_ticket->getAllNotAdmin();
                if(is_array($result) && !empty($result)){
                    echo json_encode($result);
                } else {
                    echo 'false';
                }
                
            }
        } else {
            echo 'false';
        }
    }
    
    public function viewRegister()
    {
        if(isset($_POST)){

            $input = $this->load->lib('input');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Periksa kembali input anda');
            $input->addValidation('user_name',$_POST['user_name'],'min=3', 'User name minimal 3 karakter');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Periksa kembali input anda');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Display Name wajib diisi');
            $input->addValidation('password', $_POST['password'],'min=6', 'Password minimal 6 karakter');
            $input->addValidation('password_confirm', $_POST['password_confirm'],'like='.$_POST['password'], 'Password konfirmasi berbeda');
            $input->addValidation('email',$_POST['email'],'email', 'Periksa kembali input anda');
            $input->addValidation('sec_quest', $_POST['sec_quest'],'min=1', 'Pertanyaan wajib diisi');
            $input->addValidation('sec_quest', $_POST['sec_quest'],'numeric', 'Periksa kembali input anda');
            $input->addValidation('answer', $_POST['sec_quest'],'min=1', 'Jawaban pertanyaan wajib diisi');
            $input->addValidation('outlet', $_POST['outlet'],'min=1', 'Nama Outlet wajib diisi');
            $input->addValidation('outlet', $_POST['outlet'],'numeric', 'Periksa kembali input anda');
            if($input->validate()){
                $m_users = $this->load->model('Users');
                $insert_value = array(
                    'user_name' => $_POST['user_name'],
                    'user_password' => MD5($_POST['password'].getConfig('salt')),
                    'display_name' => $_POST['display_name'],
                    'email' => $_POST['email'],
                    'store_id' => $_POST['outlet'],
                    'department_id' => '0',
                    'group_id' => '7',
                    'security_question' => $_POST['sec_quest'],
                    'security_answer' => $_POST['answer'],
                    'user_status' => '1'
                );
                if($m_users->checkUserName($_POST['user_name'])){
                    if($m_users->newUser($insert_value)){
                        $user = $m_users->getUserBy(array('user_id' => $m_users->lastInsertID()));
                        $user['user_password'] = $_POST['password'];
                        $mail_data = array(
                            'title' => "Booking Room User Registration",
                            'view' => 'email/new_user',
                            'data' => array(
                                'detail' => $user
                            ),
                            'recipient' => array(
                                'address' => $user['email'],
                                'name' => $user['display_name'],
                            ),
                            
                        );
                      
                        $result = array(
                            'success' => 1,
                            'mail' => 0
                        );
                        
                        if($this->send_email($mail_data)) $result['mail'] = 1;
                        
                        echo json_encode($result);
                    } else {
                        $result = array(
                            'success' => 0,
                            'err_code' => 2,
                            'message' => 'Failed to Register.'
                        );
                        echo json_encode($result);
                    }
                }else{
                    $result = array(
                        'success' => 0,
                        'err_code' => 1,
                        'message' => 'User Name is already taken.'
                    );
                    echo json_encode($result);
                }
            } else {
                $result = array(
                    'success' => 0,
                    'err_code' => 3,
                    'error' => $input->_error
                );
                echo json_encode($result);
            }
            
        }else{
            $result = array(
                'success' => 0,
                'err_code' => 4,
                'message' => 'Unknown Error'
            );
            echo json_encode($result);
        }
    }
    
    public function viewForgot()
    {
        if(isset($_POST)){

            $input = $this->load->lib('input');
            $input->addValidation('user_format',$_POST['user_name'],'username', 'Periksa kembali input anda');
            $input->addValidation('user_min',$_POST['user_name'],'min=3', 'User name minimal 3 karakter');
            $input->addValidation('security_required', $_POST['security_question'],'min=1', 'Pertanyaan wajib diisi');
            $input->addValidation('security_format', $_POST['security_question'],'numeric', 'Periksa kembali input anda');
            $input->addValidation('answer_required', $_POST['security_answer'],'min=1', 'Jawaban pertanyaan wajib diisi');
            if($input->validate()){
                $m_users = $this->load->model('Users');
                #$_POST['security_answer'] = md5($_POST['security_answer']);
                $clean = $input->cleanInputArray($_POST);
                $user = $m_users->checkForgotRequirement($clean);
                if(is_array($user) && count($user) > 0){
                    $new_password = substr(MD5($clean['user_name'].date("Y-m-d h:i:s")), 0, 6);
                    $new = array(
                        'user_password' => MD5($new_password.getConfig('salt'))
                    );
                    if($m_users->editUser($new, $clean)){
                        $mail_data = array(
                            'title' => "Booking Room User Forgot Password",
                            'view' => 'email/forgot_password',
                            'data' => array(
                                'data' => $user,
                                'password' => $new_password
                            ),
                            'recipient' => array(
                                'address' => $user['email'],
                                'name' => $user['display_name'],
                            ),
                            
                        );
                      
                        $result = array(
                            'success' => 1,
                            'mail' => 0
                        );
                        
                        if($this->send_email($mail_data)) $result['mail'] = 1;
                    }
                    echo json_encode($result);
                } else {
                    $result = array(
                        'success' => 0,
                        'err_code' => 2,
                        'message' => 'User info not found.'
                    );
                    echo json_encode($result);
                }
                
            } else {
                $result = array(
                    'success' => 0,
                    'err_code' => 3,
                    'error' => $input->_error
                );
                echo json_encode($result);
            }
            
        }else{
            $result = array(
                'success' => 0,
                'err_code' => 4,
                'message' => 'Unknown Error'
            );
            echo json_encode($result);
        }
    }
    /* Mailing */
        
    private function send_email($option){
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        #$stmail = $this->load->lib('STMail');
		try
		{
			$stmail->IsSMTP(); // telling the class to use SMTP
			$stmail->Host       = "mail.sushitei.co.id"; // SMTP server
			$stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
													   // 1 = errors and messages
													   // 2 = messages only
			$stmail->SMTPAuth   = true;                  // enable SMTP authentication
			$stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
			$stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
			$stmail->Username   = "it1.jkt@sushitei.co.id"; // SMTP account username
			$stmail->Password   = "Fangblade_99";        // SMTP account password

			$stmail->SetFrom('it1.jkt@sushitei.co.id', 'Room Booking');

			$stmail->AddReplyTo('it1.jkt@sushitei.co.id', 'Room Booking');
			
			$stmail->isHTML('true');

			$stmail->Subject    = $option['title'];
			
            ob_start();
            $this->load->view($option['view'], $option['data']);
            $returned = ob_get_contents();
            ob_end_clean();
            
			$stmail->MsgHTML($returned);
			
			$stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
			
			
			return ($stmail->Send() ? 1 : 0);
		} catch (phpmailerException $e) {
		    $this->message = $e->errorMessage();
			return 0;
		    
		} catch (Exception $e) {
			$this->message = $e->getMessage();
			return 0;
			
		}
        
        
    }
    
}
/*
* End Home Class
*/