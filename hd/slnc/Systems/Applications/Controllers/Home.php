<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Home extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewIndex', 'viewUserLogin', 'employee_logout', 'new_user'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewIndex', 'employee_logout'),
                'groups'=>array('Guest'),
            ),
            
        );
    }
    
    public function viewIndex()
    {
        $category = $this->load->model('TicketCategory');
		
        $option = array(
            'admin' => $this->isAdmin(),
            'bdatepick_js' => 1,
            'bdatepick_css' => 1,
            'scriptload' => '
            <script type="text/javascript" src="Resources/js/script.js"></script>'
        );
        $ticket = $this->load->model('Tickets');
        $data = array(
            'session' => $this->_mySession,
			'mc' => $ticket->getMainCategory(),
            'ol' => $ticket->getAllActiveStore(),
        );
        
        
            
        $query = array(
            'page' => (isset($_GET['page']) ? $_GET['page'] : 1),
            'result' => 10,
            'order_by' => (isset($this->_mySession['option']) && isset($this->_mySession['option']['order_by']) ? $this->_mySession['option']['order_by'] : 'submit_date'),
            'order' => (isset($this->_mySession['option']) && isset($this->_mySession['option']['order']) ? $this->_mySession['option']['order'] : 'DESC')
        );
         
        if($this->isAdmin()) {
            $data['ticket_list'] = $ticket->showAllTicket($query);
            $data['total'] = $ticket->getCountResult();
            $data['max_result'] = $query['result'];
			$data['current_page'] = $query['page'];
            $this->load->template('ticket/home', $data, $option);
        } else {
            $data['max_result'] = $query['result'];
			$data['current_page'] = $query['page'];
            $data['ticket_list'] = $ticket->showMyActiveTicket($this->_mySession['user_id'], $query);
            $data['total'] = $ticket->getCountResult();
            $this->load->template('ticket/myticket',$data, $option);
        }
            
    }

}
/*
* End Home Class
*/