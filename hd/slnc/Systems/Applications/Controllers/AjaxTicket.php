<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxTicket extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewEditTicket', 'viewSubmit', 'viewGetTicketData', 'viewGetTotalPage','viewGetCategoryEdit','viewGetSubCategory'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewEditTicket','viewSubmit', 'viewGetTicketData'),
                'groups'=>array('Guest'),
            ),
        );
    }
	
	public function viewGetCategoryEdit()
    {
		if(!$this->isGuest()){
			if(isset($_POST['cid'])){
			$input = $this->load->lib('Input');
            $input->addValidation('cid', $_POST['cid'], 'numeric', 'Periksa kembali input anda');
            
				if ($input->validate()) {
					$m_category = $this->load->model('TicketCategory');
					$result = $m_category->getCategoryId($_POST);
                    $sub_category = $m_category->getSubCategory($_POST['cid']);
					if(is_array($result)){
                        echo json_encode($result);
					} else {
						echo 'false';
					}
				} else {
					echo 'false';
				}
			
			} else {
				echo 'false';
			}
		
		} else {
            echo 'false';
        }
	}
	
	function getSubCategory(){
		if(isset($_POST['category'])){
			$input = $this->load->lib('Input');
			$input->addValidation('category', $_POST['category'] ,'numeric','Terjadi kesalahan');
			if($input->validate()){
				$model_ticket = $this->load->model('TicketCategory');
				$result = $model_ticket->getSubCategory($_POST['category']);
				if(is_array($result) && !empty($result)){
					echo json_encode($result);
				} else {
					echo 'false';
				}
				
			}
		} else {
			echo 'false';
		}
	}
	
    public function viewGetTotalPage(){
        if(!empty($this->_mySession['last_page']) && $this->_mySession['last_page'] > 1){
            
            if(($this->_mySession['current_page'] - 5) > 0){
                echo"
                <li>
                    <a href=\"#\" class='page-navigate' data-id='1'>1</a>
                </li>
                <li>
                    <a href=\"#\">...</a>
                </li>";
            }
            $page_start = 1;
            $page_end = $this->_mySession['last_page'];
            if($this->_mySession['last_page'] > 9 && $this->_mySession['current_page'] > 9)
            {
                $page_start = (($this->_mySession['current_page']-4) < 1 ? 1 : $this->_mySession['current_page']-4);
                $page_end = (($this->_mySession['current_page']+4) > $this->_mySession['last_page'] ? $this->_mySession['last_page'] : ($this->_mySession['current_page']+4));
            }
            elseif($this->_mySession['last_page'] > 9 && $this->_mySession['current_page'] <= 9)
            {
                $page_start = (($this->_mySession['current_page']-4) < 1 ? 1 : $this->_mySession['current_page']-4);
                $page_end = (($this->_mySession['last_page']) > 9 ? 9+4 : 9);
            }elseif($this->_mySession['last_page'] < 9 && $this->_mySession['current_page'] <= 9)
            {
                $page_start = (($this->_mySession['current_page']-4) < 1 ? 1 : $this->_mySession['current_page']-4);
                $page_end = $this->_mySession['last_page'];
            }
            
            for($i=$page_start;$i < $page_end;$i++){
                echo "<li ".($this->_mySession['current_page'] == $i ? 'class="active"' : '')."><a class='page-navigate' data-id='{$i}'>{$i}</a></li>";
            }
            if($this->_mySession['current_page'] < $this->_mySession['last_page']){
                echo "
                        <li>
                            <a href=\"#\">...</span></a>
                        </li>";
            }
            echo "
                <li>
                    <a class='page-navigate' data-id='{$this->_mySession['last_page']}'>{$this->_mySession['last_page']}</a>
                </li>";
        }else{
            echo '';
        }
    }
	
    public function viewSubmit()
    {
        $data = array();
        if(isset($_POST)){
            $recaptcha = 0;
            if(getConfig('recaptcha_submit') == 1){
                $captcha = $_POST['g-recaptcha-response'];
                $secretKey = '6LemwyETAAAAABp7aR2EScUPsLoLwCSHPvGMQTpH';
                $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$captcha}&remoteip={$_SERVER['REMOTE_ADDR']}");
                $obj = json_decode($response);
                if($obj->success == 1) {
                    $recaptcha = 1;
                }
            }
            
            if((getConfig('recaptcha_submit') == 1 && $recaptcha == 1) || (getConfig('recaptcha_submit') == 0 && $recaptcha == 0)){
                $input = $this->load->lib('Input');
                $input->addValidation('ic_req', $_POST['item_code'], 'min=1', ' is Required');
                $input->addValidation('ic_format', $_POST['item_code'], 'item_code', ' Check your input format');
                $input->addValidation('in_req', $_POST['item_name'], 'min=1', ' is Required');
                $input->addValidation('in_format', $_POST['item_name'], 'item_name', ' Check your input format');
                $input->addValidation('ns_req', $_POST['supplier'], 'min=1', ' is Required');
                $input->addValidation('ns_format', $_POST['supplier'], 'item_name', ' Check your input format');
                $input->addValidation('mc_req', $_POST['main_category'], 'min=1', ' is Required');
                $input->addValidation('mc_format', $_POST['main_category'], 'numeric', ' Check your input format');
                $input->addValidation('t_req', $_POST['type'], 'min=1', ' is Required');
                $input->addValidation('t_format', $_POST['type'], 'numeric', ' Check your input format');
                $input->addValidation('pn_req', $_POST['po_number'], 'min=1', ' is Required');
                $input->addValidation('pn_format', $_POST['po_number'], 'item_name', ' Check your input format');
                $input->addValidation('od_format', $_POST['order_date'], 'date', ' Check your input format');
                $input->addValidation('rd_format', $_POST['received_date'], 'date', ' Check your input format');
                $input->addValidation('oq_req', $_POST['order_quantity'], 'min=1', ' is Required');
                $input->addValidation('oq_format', $_POST['order_quantity'], 'numeric', ' Check your input format');
                $input->addValidation('rq_req', $_POST['received_quantity'], 'min=1', ' is Required');
                $input->addValidation('rq_format', $_POST['received_quantity'], 'numeric', ' Check your input format');
                $input->addValidation('uom_req', $_POST['uom'], 'min=1', ' is Required');
                $input->addValidation('uom_format', $_POST['uom'], 'alpha_numeric', ' Check your input format');
                if(!empty($_POST['etc_note'])){
                    $input->addValidation('enote_format', $_POST['etc_note'], 'alpha_numeric_sc', ' Check your input format');
                }
                if ($input->validate()){
                    
                    $ticket = $this->load->model('Tickets');
                    $user = $this->load->model('Users');
                    
                    $_POST = $input->cleanInputArray($_POST);
                    $user_detail = $user->getUserBy(array('user_id' => isset($_POST['behalf']) ? $_POST['user'] : $this->_mySession['user_id']));
                    $real_field = array(
                        'ticket_title' => "{$_POST['item_code']} - {$_POST['item_name']}",
                        'item_code' => $_POST['item_code'],
                        'item_name' => $_POST['item_name'],
                        'uom' => $_POST['uom'],
                        'supplier' => $_POST['supplier'],
                        'ticket_type' => $_POST['type'],
                        'category' => $_POST['main_category'],
                        'content' => htmlspecialchars($_POST['etc_note'], ENT_QUOTES),
                        'status' => 1,
                        'po_number' => $_POST['po_number'],
                        'rcv_qty' => $_POST['received_quantity'],
                        'rcv_date' => $_POST['received_date'],
                        'order_qty' => $_POST['order_quantity'],
                        'order_date' => $_POST['order_date'],
                        'ticket_user' => $user_detail['user_id'],
                        'ticket_creator' => $this->_mySession['user_id'],
                        'submit_date' => date("Y-m-d H:i:s"),
                        'store' => $user_detail['store_id']
                    );
                    
                    $log = array(
                        'log_type' =>  'Submit',
                        'user' => $this->_mySession['user_id'],
                        'log_title' => $this->_mySession['user_name']. " Created a new Ticket",
                        'log_content' => $real_field['content'],
                        'visible' => '1'
                    );
                    
                    if($ticket->createTicket($real_field, $log)){
                        $inserted_data = $ticket->showTicketBy(array('ticket_id' => $ticket->lastInsertID));
                        $mud = $this->load->model('UserData');
                        $mud->increaseDataValue(array('data_user'=>$this->_mySession['user_id'], 'data_site' => $this->site, 'data_type' => 1));
                        $mail_option = array(
                            'title' => "SLNC #{$ticket->lastInsertID} (NEW): {$inserted_data['ticket_title']}",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                        
                        $data = array(
                            'success' => 1,
                            'message' => 'Success.',
                            'email' => $this->send_email($mail_option, $inserted_data),
                            'url' => getConfig('base_url')."edit_ticket_{$inserted_data['ticket_id']}.html"
                        );
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'error_type' => 'Validation',
                        'message' => $input->_error
                    );
                }
            }elseif(getConfig('recaptcha_submit') == 1 && $recaptcha == 0){
                $data = array(
                    'success' => 0,
                    'error_type' => 'recaptcha',
                    'message' => 'Recaptcha error, please check your input'
                );
            }
            
        } else {
            $data = array(
                'success' => 0,
                'error_type' => 'Input',
                'message' => 'No input specified.'
            );
        }
        echo json_encode($data);
    }
    
    public function viewEditTicket()
    {
        $data = array(
            'success' => 0
        );
        if(isset($_POST['submit_type'])){
            
            $result = 'failed';
            $m_ticket = $this->load->model('Tickets');
            $input = $this->load->lib('input');
            $edit_status = 0;
            $time =  date("Y-m-d H:i:s");
            if($_POST['submit_type'] == 'Assign'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input->addValidation('assigned_staff', $_POST['assigned_staff'], 'numeric', 'Unknown Staff');
                $input->addValidation('a_name', $_POST['a_name'], 'alpha_numeric_sc', 'Unknown Target');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'assigned_staff' => $_POST['assigned_staff'],        
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Assign',
                        'log_title' => $this->_mySession['user_name']. " Assigned the item to ". $_POST['a_name'],
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 2;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (ASSIGN): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Ticket Assigned successfully'
                        );
                    }else{
                        $data = array(
                            'success' => 0,
                            'message' => 'Failed.',
                            'error' => $submitted
                        );  
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Take Over'){
                
                if(!empty($_POST['comment'])){
                    $input_filtered = array(
                        'comment' => htmlspecialchars($_POST['comment'], ENT_QUOTES)
                    );
                    $input->addValidation('comment', $input_filtered['comment'], 'textarea_no_html', 'Wrong input format');
                }
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'assigned_staff' => $this->_mySession['user_id'],
                            'status' => 2,
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Take Over',
                        'log_title' => $this->_mySession['user_name']. " is Taking-over the ticket",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (Take Over): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Ticket Taken Over'
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Comment'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Comment',
                        'log_title' => $this->_mySession['user_name']. " Commenting this ticket.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (Comment): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Comment Sent Successfully'
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Force-Close'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'close_status' => 2,
							'close_time' => $time,
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Force-Close',
                        'log_title' => $this->_mySession['user_name']. " Force-Closing the ticket.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (FORCE CLOSE): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Ticket Force Closed'
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Close'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'close_status' => 1,
                            'close_time' => $time,
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Close',
                        'log_title' => $this->_mySession['user_name']. " Closing the ticket.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (CLOSE): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Ticket Closed'
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Re-Open'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'close_status' => 0,
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Re-Open',
                        'log_title' => $this->_mySession['user_name']. " Re-opening the ticket.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (RE-OPEN): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Ticket Reopened'
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Release'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'status' => 3,
                            'resolved_date' => $time,
                            'resolved_solution' => $input_filtered['comment'],
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Release',
                        'log_title' => $this->_mySession['user_name']. " has Released the item.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (Release): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Item Successfully Released'
                        );
                    }
                }else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.'
                        ,
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Return'){
                
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'status' => 3,
                            'resolved_date' => $time,
                            'resolved_solution' => $input_filtered['comment'],
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Return',
                        'log_title' => $this->_mySession['user_name']. " has Returned the item.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (RETURN): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Item Successfully Returned'
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Reject'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $submitted = array(
                        'input' => array(
                            'status' => 4,
                            'resolved_date' => $time,
                            'resolved_solution' => $input_filtered['comment'],
                            'last_update' => $time
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Reject',
                        'log_title' => $this->_mySession['user_name']. " has Rejected the item.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $time,
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($submitted, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (REJECT): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                            'action' => 'edit'
                        );
                        $data = array(
                            'success' => 1,
                            'message' => 'Item Rejected'
                        );
                    
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }
            
            if($edit_status == 1){
                $inserted_data = $m_ticket->showTicketBy(array('ticket_id' => $_POST['t_id']));
                
                $data = array(
                    'success' => 1,
                    'message' => 'Success.',
                    'email' => $this->send_email($mail_option, $inserted_data)
                );
                #$ticket_data = $m_ticket->showTicketBy(array('ticket_id' => $_POST['t_id']));
                #$mail_option['recipient'] = $ticket_data['email'];
                #$mail_option['recipient_name'] = $ticket_data['display_name'];
                #$mail_option['title'] .= $ticket_data['ticket_title'];
                #if($this->send_email($mail_option, $ticket_data, $m_ticket))
                #echo 'sukses';
            }
        }else{
            $data['input'] = 'Not Included';
        }
        echo json_encode($data);
    
    }
    
    
    public function viewGetTicketData(){
        if(!$this->isGuest()){
            $input = $this->load->lib('Input');

            $input->addValidation('page',$_POST['page'],'numeric', 'Periksa kembali input anda');
            if(!empty($_POST['main_category'])){
                $input->addValidation('main_category_char',$_POST['main_category'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('main_category_max',$_POST['main_category'], 'max=20', 'Periksa kembali input anda');
            }
            if(!empty($_POST['ticket_type'])){
                $input->addValidation('status_char',$_POST['status'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('status_max',$_POST['status'], 'max=2', 'Periksa kembali input anda');
            }
            if(!empty($_POST['store'])){
                $input->addValidation('store',$_POST['store'], 'alpha_numeric_sp', 'Please check your input');
                $input->addValidation('store_max',$_POST['store'], 'max=25', '25');
            }
            if(!empty($_POST['date_start'])){
                $input->addValidation('date_start',$_POST['date_start'], 'date', 'Periksa kembali input anda');
            }
            if(!empty($_POST['date_end'])){
                $input->addValidation('date_end',$_POST['date_end'], 'date', 'Periksa kembali input anda');
            }
            if(!empty($_POST['page'])){
                $input->addValidation('page_format',$_POST['page'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('page_max',$_POST['page'], 'max=4', 'Periksa kembali input anda');
            }
            $option = array(
                'page' => (!empty($_POST['page']) ? $_POST['page'] : 1),
                'result' => 10,
                'order_by' => 'submit_date',
                'order' => 'DESC'
            );

            if ($input->validate()){
                $model_ticket = $this->load->model('Tickets');
                
                $query = array();
                
                if($_POST['sort'] == 'title_asc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'title_desc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'outlet_asc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'outlet_desc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'submit_asc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'submit_desc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'category_asc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'category_desc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'pic_asc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'pic_desc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'status_asc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'status_desc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'DESC';
                }
                
                
                if(isset($_POST['ticket_title']) && !empty($_POST['ticket_title'])){
                    $query['ticket_title']['value'] = "%{$_POST['ticket_title']}%";
                    $query['ticket_title']['type'] = 'LIKE';
                }
                
                if(!empty($_POST['status'])){
                    $query['status']['value'] = $_POST['status'];
                    $query['status']['type'] = '=';
                }
                
                if(!empty($_POST['store'])){
                    $query['store']['value'] = $_POST['store'];
                    $query['store']['type'] = '=';
                }
                
                if(!empty($_POST['date_start'])) {
                    $query['submit_date']['value'] = $_POST['date_start'];
                    $query['submit_date']['type'] = '=';
                }
                
                if(!empty($_POST['date_end'])){
                    $query['date_end']['value'] = $_POST['date_end'];
                    $query['date_end']['type'] = '=';
                }
                
                if($_POST['sub_category'] == '999'){
                    unset($query['category']);
                    $query['get_all']['value'] = $_POST['main_category'];
                    $query['get_all']['type'] = '=';
                }

                if(!empty($_POST['main_category'])){
                    $query['category']['value'] = $_POST['main_category'];
                    $query['category']['type'] = '=';
                }
                
                
                
                if(!$this->isAdmin()){
                    $query['ticket_user']['value'] = $this->_mySession['user_id'];
                    $query['ticket_user']['type'] = '=';

                    $query['store']['value'] = $this->_mySession['outlet'];
                    $query['store']['type'] = '=';
                }
               
                $ticket_list = $model_ticket->showAllTicketFiltered($query, $option);
                $this->setSession('last_query', $query);
                $this->setSession('option', $option);
                if(count($ticket_list) > 0){
                    $this->setSession('last_page', ceil($model_ticket->getCountResult()/$option['result']));
                    $this->setSession('current_page', $option['page']);
                    $num = (($option['page']-1)*10) + 1;
                    foreach($ticket_list as $result){
                        $date_check = '';
                        $diff_seconds  = (!empty($result['close_time']) ? strtotime($result['close_time']) - strtotime($result['submit_date']) : strtotime(date("Y-m-d H:i:s")) - strtotime($result['submit_date']));
                        $stat = floor($diff_seconds/3600);
                        
                        if(($stat >= 24 && $stat < 48) && $result['close_status'] == 0){
                            $date_check = "warning";
                        }elseif($stat > 48 && $result['close_status'] == 0){
                            $date_check = "danger";
                        }

                        if($result['close_status'] > 0){
                            $date_check = "success";
                        }
                        $closed = $result['close_status'] == 1 ? 'Yes' : ($result['close_status'] == 2 ? 'Forced' : 'No');
                        $submit_date = date("d-m-Y", strtotime($result['submit_date']));
                        $last_update = (!empty($result['last_update']) ? date("d-m-Y", strtotime($result['last_update'])) : '-');
                            
                        if(!$this->isAdmin()){
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$num} </td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                        <td>{$result['supplier']}</td>
                        <td>{$result['category_name']}</td>
                        <td>{$result['staff_name']}</td>
                        <td>{$result['status_name']}</td>
                        <td>{$submit_date}</td>
                        <td>{$last_update}</td>
                        <td>{$closed}</td>
                    </tr>";    
                        }else{
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$num} </td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                        <td>{$result['store_name']}</td>
                        <td>{$result['supplier']}</td>
                        <td>{$result['category_name']}</td>
                        <td>{$result['staff_name']}</td>
                        <td>{$result['status_name']}</a></td>
                        <td>{$submit_date}</td>
                        <td>{$last_update}</td>
                        <td>{$closed}</td>
                    </tr>";
                        }                
                        $num++;
                    }
                } else {
                    echo "
                    <tr>
                        <td colspan=\"9\">Tiket Kosong</td>
                    </tr>";
                }           
            }else{
                print_r($input->_error);
            }
		} else {
            echo 'nope';
        }
        
    }
    
    
    function reArrayFiles(&$file_post) 
    {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
    
    private function send_email($option, $data)
    {
        $mail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail = $this->load->lib('STMail');
        $model = $this->load->model('Tickets');
        $log_data = $model->getLog(array('ticket_id' => $data['ticket_id']));
        $stmail->IsSMTP(); // telling the class to use SMTP
        $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
        $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $stmail->SMTPAuth   = true;                  // enable SMTP authentication
        $stmail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $stmail->Username   = "fnb.jkt@sushitei.co.id"; // SMTP account user_name
        $stmail->Password   = "St@555fnb";        // SMTP account password

        $stmail->SetFrom('fnb.jkt@sushitei.co.id', 'SLNC Center');

        $stmail->AddReplyTo('fnb.jkt@sushitei.co.id', 'SLNC Center');
        
        $stmail->isHTML('true');

        $stmail->Subject    = $option['title'];
        
        $stmail->MsgHTML($stmail->mailContent($data, $log_data));
        
        $stmail->AddAddress($option['recipient'], $option['recipient_name']);
        
        if(isset($option['action']) && $option['action'] == 'edit'){
            $stmail->AddAddress($data['user_email'], $data['user_name']);
        }
        
        $category = $model->getCategoryDetail(array('input' => $data['category_id']));
        $recipient = explode(";", $category['description']);
        
        foreach($recipient as $address){
            #echo "addressnya adalah {$address} <br/>";
            $stmail->AddBCC($address, $data['category_name']);
        }
        
        return ($stmail->Send() ? 1 : 0);
            
        
    }
}
/*
* End Home Class
*/