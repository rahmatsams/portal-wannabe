<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class ModelPage extends Model{
		public $_result = array();
		
		public function getPageList(){
            $query = 'SELECT * FROM page ORDER BY page_name,page_controller ASC';
            $result = $this->fetchAllQuery($query);
            
            return $result;
        }
        
        public function newPage($input)
        {
            return $this->insertQuery('page', $input);
        }
	}
?>