<?php
/*
* Model for Ticketdesk
*/
class Tickets extends Model
{
	public $tableName = "slnc_main";
    public $lastInsertID;
	
	function createTicket($form = array(), $log = array()){
		$this->beginTransaction();
		try{
			$this->insertQuery('slnc_main',$form);
			$log['ticket_id'] = $this->lastInsertId();
			$this->lastInsertID = $this->lastInsertId();
			$this->insertQuery('slnc_log',$log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
    function editTicket($form = array(), $log = array()){
        $table_ticket = 'slnc_main';
        $table_log = 'slnc_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table_ticket, $form['input'], $form['where']);
			$this->insertQuery($table_log, $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
	
    function showAllTicket($query_option)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, slnc_type.type_name, slnc_category.category_name, spare_part, category_id, category_parent_id, slnc_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, sid.display_name AS resolver_name ";
		$query_string .= "FROM slnc_main LEFT JOIN slnc_type ON slnc_main.ticket_type=slnc_type.type_id ";
		$query_string .= "LEFT JOIN slnc_status ON slnc_main.status=slnc_status.status_id ";
		$query_string .= "LEFT JOIN slnc_category ON slnc_main.category=slnc_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON slnc_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN users astaff ON slnc_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM slnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON slnc_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN (SELECT slnc_log.ticket_id AS aidee, slnc_log.user_id, users.display_name FROM slnc_log LEFT JOIN users ON slnc_log.user_id=users.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return') sid ON slnc_main.ticket_id=sid.aidee ";
        $query_string .= "LEFT JOIN users cname ON slnc_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM users LEFT JOIN store ON users.store_id=store.store_id) AS st ON slnc_main.ticket_user=st.user_id ";
        $count_query = "SELECT COUNT(*) AS row_total FROM slnc_main";
        
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
    function showAllTicketFiltered($filter, $query_option)
    {
        
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, slnc_type.type_name, slnc_category.category_name, spare_part, category_id, category_parent_id, slnc_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, sid.display_name AS resolver_name ";
		$query_string .= "FROM slnc_main LEFT JOIN slnc_type ON slnc_main.ticket_type=slnc_type.type_id ";
		$query_string .= "LEFT JOIN slnc_status ON slnc_main.status=slnc_status.status_id ";
		$query_string .= "LEFT JOIN slnc_category ON slnc_main.category=slnc_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON slnc_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN users astaff ON slnc_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM slnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON slnc_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN (SELECT slnc_log.ticket_id AS aidee, slnc_log.user_id, users.display_name FROM slnc_log LEFT JOIN users ON slnc_log.user_id=users.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return') sid ON slnc_main.ticket_id=sid.aidee ";
        $query_string .= "LEFT JOIN users cname ON slnc_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM users LEFT JOIN store ON users.store_id=store.store_id) AS st ON slnc_main.ticket_user=st.user_id ";
        $num = 1;
        $count_query = "SELECT COUNT(*) AS row_total FROM slnc_main ";
        $count_query .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM users LEFT JOIN store ON users.store_id=store.store_id) AS st ON slnc_main.ticket_user=st.user_id ";
        foreach($filter as $key => $value){
            if($num > 1){
                if($key != 'date_end' && !empty($value)){
                    if($key == 'submit_date' && !empty($filter['date_end'])){
                        $query_string .= "AND submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                        $count_query .= "AND submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                    }else{
                        $query_string .= "AND {$key} LIKE '%{$value}%' ";
                        $count_query .= "AND {$key} LIKE '%{$value}%' ";
                    }
                }
            } else {
                if($key != 'date_end' && !empty($value)){
                    if($key == 'submit_date' && !empty($filter['date_end'])){
                        $query_string .= "WHERE submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                        $count_query .= "WHERE submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";                    
                    }else{
                        $query_string .=  "WHERE {$key} LIKE '%{$value}%' ";
                        $count_query .= "WHERE {$key} LIKE '%{$value}%' ";
                    }
                }
            }
            $num++;
        }
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
	function showMyActiveTicket($my_id, $query_option)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, slnc_type.type_name, slnc_category.category_name, spare_part, category_id, category_parent_id, slnc_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, sid.display_name AS resolver_name ";
		$query_string .= "FROM slnc_main LEFT JOIN slnc_type ON slnc_main.ticket_type=slnc_type.type_id ";
        $query_string .= "LEFT JOIN slnc_status ON slnc_main.status=slnc_status.status_id ";
		$query_string .= "LEFT JOIN slnc_category ON slnc_main.category=slnc_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON slnc_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN users astaff ON slnc_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM slnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON slnc_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN (SELECT slnc_log.ticket_id AS aidee, slnc_log.user_id, users.display_name FROM slnc_log LEFT JOIN users ON slnc_log.user_id=users.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return') sid ON slnc_main.ticket_id=sid.aidee ";
        $query_string .= "LEFT JOIN users cname ON slnc_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM users LEFT JOIN store ON users.store_id=store.store_id) AS st ON slnc_main.ticket_user=st.user_id ";
		$query_string .= "WHERE ticket_user='{$my_id}'";
        $count_query = "SELECT COUNT(*) AS row_total FROM slnc_main WHERE ticket_user='{$my_id}'";
		
		
        $result = $this->pagingQuery($query_string,$count_query,$query_option);
        return $result;
    }
    
    function showTicketBy($input)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, slnc_type.type_name, slnc_category.category_name, spare_part, category_id, category_parent_id, slnc_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, sid.display_name AS resolver_name, st.email ";
		$query_string .= "FROM slnc_main LEFT JOIN slnc_type ON slnc_main.ticket_type=slnc_type.type_id ";
        $query_string .= "LEFT JOIN slnc_status ON slnc_main.status=slnc_status.status_id ";
		$query_string .= "LEFT JOIN slnc_category ON slnc_main.category=slnc_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON slnc_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN users astaff ON slnc_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM slnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON slnc_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN (SELECT slnc_log.ticket_id AS aidee, slnc_log.user_id, users.display_name FROM slnc_log LEFT JOIN users ON slnc_log.user_id=users.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return') sid ON slnc_main.ticket_id=sid.aidee ";
        $query_string .= "LEFT JOIN users cname ON slnc_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name, email FROM users LEFT JOIN store ON users.store_id=store.store_id) AS st ON slnc_main.ticket_user=st.user_id ";
        if(isset($input['my_id'])){
            $query_string .= "WHERE ticket_user=:my_id AND ticket_id=:ticket_id";
        } else {
            $query_string .= "WHERE ticket_id=:ticket_id";
        }
        $result = $this->fetchSingleQuery($query_string, $input);
        
		return $result;
    }
    
    function getLog($input){
        $query_string = "SELECT ticketlog_title, slnc_log.user_id, display_name, ticketlog_type, ticketlog_content, ticketlog_time FROM slnc_log ";
        $query_string .= "LEFT JOIN users ON slnc_log.user_id=users.user_id ";
        $query_string .= "WHERE ticket_id = :ticket_id AND ticketlog_show = 1 ORDER BY ticketlog_time DESC";
        $result = $this->fetchAllQuery($query_string, $input);
		return $result;
    }
    
	function getTicketType()
    {
        $query_string = 'SELECT * FROM slnc_type';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM slnc_category WHERE category_parent_id=1';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM slnc_category WHERE category_parent_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getUploadedFile($query_value)
	{
		$query_string = 'SELECT * FROM ticket_upload WHERE ticket_id=:ticket_id';
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getAllActiveStore()
    {
        $query_string = "SELECT * FROM store WHERE store_status=1";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getMailRecipient(){
        $query_string = "SELECT DISTINCT email,display_name FROM users WHERE group_id IN (1,8)";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function myOutlet($input){
        $query_string = "SELECT store_name FROM users LEFT JOIN store ON users.store_id=store.store_id WHERE user_id=:user_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    function getAllPQNC(){
        $query_string = "SELECT user_id,display_name FROM users WHERE group_id=9";
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
}