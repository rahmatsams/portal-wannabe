<?php
/*
* Model for Ticketdesk
*/
class AnnounceMail extends Model
{

    function getDocByUpdateTime($value){
        $query_string = "SELECT `docs`.`id`, `docs`.`number`, `docs`.`name`, `rev`, `upload_date`, `docs`.`status`, `filename` ";
        $query_string .= "FROM `docs` ";
        if(isset($value['update_last'])){
            $query_string .= "WHERE upload_date BETWEEN '{$value['update_first']} 00:00:00' AND '{$value['update_last']} 00:00:00'";
        }else{
            $query_string .= "WHERE upload_date BETWEEN '{$value['update_first']} 00:00:00' AND '{$value['update_first']} 23:59:59'";
        }
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getMailRecipient($value){
        $query_string = 'SELECT doc_groups_id, users_id, email FROM doc_group_link ';
        $query_string .= 'LEFT JOIN (SELECT doc_groups_id AS group_id, users_id, email FROM user_doc_link LEFT JOIN users ON user_doc_link.users_id=users.user_id GROUP BY email) AS doc_user ON doc_user.group_id=doc_group_link.doc_groups_id ';
        $query_string .= 'WHERE docs_id = :doc_id';
        $result = $this->fetchAllQuery($query_string, $value);
        return $result;
    }
    function getMailRecipient2($value){
        $query_string = 'SELECT `docs`.`id`, `docs`.`number`, `docs`.`name`, `rev`, `upload_date`, `users`.`user_name`, `users`.`email` ';
        $query_string .= 'FROM `docs` ';
        $query_string .= "JOIN `doc_group_link` on `doc_group_link`.`docs_id`=`docs`.`id` and `docs`.`id` IN $value ";
        $query_string .= 'JOIN `user_doc_link` on `user_doc_link`.`doc_groups_id`=`doc_group_link`.`doc_groups_id` ';
        $query_string .= 'JOIN `users` on `users`.`user_id`=`user_doc_link`.`users_id` ';
        $query_string .= 'order by `users`.`email`,`docs`.`name`';
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getMailAddress($value){

        $query_string = 'SELECT DISTINCT(`users`.`email`),`users`.`display_name` ';
        $query_string .= 'FROM `docs` ';
        if(isset($value['end_date'])){
            $query_string .= "JOIN `doc_group_link` on `doc_group_link`.`docs_id`=`docs`.`id` AND `docs`.`upload_date` BETWEEN '{$value['start_date']} 00:00:00' AND '{$value['end_date']} 23:59:59' ";
        }else{
            $query_string .="JOIN `doc_group_link` on `doc_group_link`.`docs_id`=`docs`.`id` AND `docs`.`upload_date` BETWEEN '{$value['start_date']} 00:00:00' AND '{$value['start_date']} 23:59:59' ";
        }
        $query_string .= 'JOIN `user_doc_link` on `user_doc_link`.`doc_groups_id`=`doc_group_link`.`doc_groups_id` ';
        $query_string .= 'JOIN `users` on `users`.`user_id`=`user_doc_link`.`users_id` ';
        $query_string .= 'order by `users`.`email`,`docs`.`name`';
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    function getDocInfoByMail($value){

        $query_string = 'SELECT `docs`.`id`, `docs`.`number`, `docs`.`name`, `rev`, `upload_date`, `dept`.`name` AS `dept_name`, `docs`.`status` ';
        $query_string .= 'FROM `docs` ';
        if(isset($value['end_date'])){
            $query_string .= "JOIN `doc_group_link` on `doc_group_link`.`docs_id`=`docs`.`id` AND `docs`.`upload_date` BETWEEN '{$value['start_date']} 00:00:00' AND '{$value['end_date']} 23:59:59' ";
        }else{
            $query_string .="JOIN `doc_group_link` on `doc_group_link`.`docs_id`=`docs`.`id` AND `docs`.`upload_date` BETWEEN '{$value['start_date']} 00:00:00' AND '{$value['start_date']} 23:59:59' ";
        }
        $query_string .= 'JOIN `user_doc_link` on `user_doc_link`.`doc_groups_id`=`doc_group_link`.`doc_groups_id` ';
        $query_string .= 'JOIN `dept` on `docs`.`dept_id`=`dept`.`id` ';
        $query_string .= "JOIN `users` on `users`.`user_id`=`user_doc_link`.`users_id` AND `users`.`email` = '{$value['email']}' ";
        $query_string .= 'order by `users`.`email`,`docs`.`name`';
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
}