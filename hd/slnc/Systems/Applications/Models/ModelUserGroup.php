<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelUserGroup extends Model
    {
		public $_result = array();
		
        function getAllGroups()
        {
			$query_string = "SELECT group_id, group_name FROM user_groups";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
	}
?>