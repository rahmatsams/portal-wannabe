<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Model for Ticketdesk
*/
class ModelTicketGroup extends Model
{
	public $tableName = "ticket_group";
    public $lastID;
	
    function getTicketGroup($input)
    {
        $query_string = "SELECT 
        store_email,
        group_year,
        group_month,
        group_submit_date,
        group_user,
        a.group_week AS group_week,
        group_auditee,
        group_id,
        group_point,
        group_finding,
        b.default_point,
        group_status,
        group_notes,
        b.store_id,
        b.store_name AS store,
        equipment_code,
        c.area_name AS area
        FROM ticket_group a 
        LEFT JOIN store b ON a.group_store=b.store_id
        LEFT JOIN store_area c ON a.group_equipment=c.area_id
        WHERE store_id=:store_id 
        AND group_year=:group_year 
        AND group_month=:group_month
        ORDER BY a.group_week ASC
        ";

        $result = $this->fetchAllQuery($query_string, $input);
        
        return $result;
        
    }

    function getTicketPercent($input)
    {
        $query_string = "SELECT 
                    a.group_store,
                    a.group_week,
                    a.group_month,
                    a.group_year,
                    (SELECT COUNT(group_status) FROM ticket_group b WHERE b.group_week = a.group_week AND b.group_store = a.group_store AND b.group_status = 'Rusak') AS rusak,
                    (SELECT COUNT(group_status) FROM ticket_group b WHERE b.group_week = a.group_week AND b.group_store = a.group_store ) AS total
                FROM ticket_group a
                WHERE group_store=:store_id 
        AND group_year=:group_year 
        AND group_month=:group_month
                GROUP BY a.group_week, a.group_store
                ORDER BY a.group_store, a.group_week";

        $result = $this->fetchAllQuery($query_string, $input);
        
        return $result;
        
    }

    function getGroupDetail($input)
    {
        $query_string = "SELECT 
        store_email,
        group_year,
        group_month,
        group_submit_date,
        group_user,
        a.group_week AS group_week,
        group_auditee,
        group_id,
        group_point,
        group_finding,
        b.default_point,
        group_status,
        group_notes,
        b.store_id,
        b.store_name AS store,
        equipment_code,
        c.area_name AS area
        FROM ticket_group a 
        LEFT JOIN store b ON a.group_store=b.store_id
        LEFT JOIN store_area c ON a.group_equipment=c.area_id
        WHERE store_id=:store_id 
        AND group_year=:group_year 
        AND group_month=:group_month
        AND group_id=:group_id
        ORDER BY a.group_week ASC
        ";

        $result = $this->fetchAllQuery($query_string, $input);
        
        return $result;
        
    }

    function getGroupDetailPeriode($input)
    {
        $query_string = "SELECT 
        store_email,
        group_year,
        group_month,
        group_submit_date,
        group_user,
        a.group_week AS group_week,
        group_auditee,
        group_id,
        group_point,
        group_finding,
        b.default_point,
        group_status,
        group_notes,
        b.store_id,
        b.store_name AS store,
        equipment_code,
        c.area_name AS area
        FROM ticket_group a 
        LEFT JOIN store b ON a.group_store=b.store_id
        LEFT JOIN store_area c ON a.group_equipment=c.area_id
        WHERE store_id=:store_id 
        AND group_year=:group_year 
        AND group_month=:group_month
        AND group_week=:group_week
        ORDER BY a.group_week ASC
        ";

        $result = $this->fetchAllQuery($query_string, $input);
        
        return $result;
        
    }

    function getTicketGroupReport($input)
    {
        $query_string = "SELECT 
        store_name FROM ticket_group a 
        LEFT JOIN store b ON a.group_store=b.store_id 
        WHERE group_id=:group";

        $result = $this->fetchSingleQuery($query_string, $input);
        
        return $result;
    }

    function getTicketGroupBy($input)
    {
        /*$query_string = "SELECT 
        store_name,
        group_id FROM ticket_group a 
        LEFT JOIN store b ON a.group_store=b.store_id 
        WHERE group_id=:group";*/
        $query_string = "SELECT
        group_id,        
        group_year,
        group_month,
        b.store_name AS store,
        group_submit_date,
        group_user,
        a.group_week,
        group_auditee,
        group_point,
        group_finding,
        b.default_point,
        group_status,
        group_notes,
        b.store_id,
        store_email,
        equipment_code,
        c.area_name AS AREA
        FROM ticket_group a 
        LEFT JOIN store b ON a.group_store=b.store_id
        LEFT JOIN store_area c ON a.group_equipment=c.area_id
        WHERE group_id =:ticket_group
        AND store_id=:group_store
        AND group_year=:group_year
        AND group_month=:group_month
        ";

        $result = $this->fetchSingleQuery($query_string, $input);
        
        return $result;
        
    }

    function getTicketGroupDetail($input)
    {
        $query_string = "SELECT 
        store_name,
        store_email,
        group_submit_date,
        group_user,
        group_auditee,
        group_id,
        group_point,
        group_finding,
        b.default_point,
        group_status, 
        group_week, 
        group_equipment,
        group_notes,
        equipment_code,
        c.area_name AS equipment
        FROM ticket_group a 
        LEFT JOIN store b ON a.group_store=b.store_id 
        LEFT JOIN store_area c ON a.group_equipment=c.area_id
        WHERE group_id=:group";

        $result = $this->fetchSingleQuery($query_string, $input);
        
        return $result;
        
    }

    function getTicketGroupWeek($input)
    {
        $query_string = "SELECT * FROM ticket_group WHERE group_id=:group AND group_week=:week";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        
        return $result;
    }
    
    function getTicketGroupByStore($input)
    {
        $query_string = "SELECT store_name,group_year, group_month, group_week,group_submit_date,group_user,group_auditee,group_id,group_point,group_finding,b.default_point,group_status FROM ticket_group a LEFT JOIN store b ON a.group_store=b.store_id WHERE group_store=:group AND group_year = :year AND group_month = :month ORDER BY group_month ASC";

        $result = $this->fetchAllQuery($query_string, $input);
        
        return $result;
    }

    function createRecommendation($form){
        try{
            $result = $this->insertQuery('ticket_group', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
        } catch(Exception $e){
            if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
        }
    }
    
	public function updateGroupByTicket($input)
    {
        $insert = array(
            'group_id' => $input
        );
        $q = "
                UPDATE ticket_group d,
                (SELECT a.*,
                COUNT(ticket_id) AS total_finding 
                FROM ticket_main a 
                LEFT JOIN ticket_condition b ON b.condition_id=a.ticket_condition 
                WHERE a.ticket_group=:group_id) c 
                SET 
                d.group_finding=c.total_finding,
                d.group_status=d.group_status,
                d.group_notes=d.group_notes
                WHERE d.group_id=:group_id
        ";
       
        return $this->doQuery($q, $insert);
    }
    
    function editGroup($form = array())
    {
        $table = 'ticket_group';
        $table_log = 'ticket_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table, $form['value'], $form['where']);
			#$this->insertQuery($table_log, $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}

    function deleteMRP($id)
    {
        $query_string = $this->doQuery("DELETE FROM ticket_group WHERE group_id=$id");
    }
    
}