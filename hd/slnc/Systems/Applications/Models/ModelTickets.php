<?php
/*
* Model for Ticketdesk
*/
class ModelTickets extends Model
{
	public $tableName = "slnc_main";
    public $lastInsertID;
	
	function createTicket($form = array(), $log = array()){
		$this->beginTransaction();
		try{
			$this->insertQuery('slnc_main',$form);
			$log['ticket'] = $this->lastInsertId();
			$this->lastInsertID = $this->lastInsertId();
			$this->insertQuery('slnc_log',$log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
    function editTicket($form = array(), $log = array()){
        $table_ticket = 'slnc_main';
        $table_log = 'slnc_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table_ticket, $form['input'], $form['where']);
			$this->insertQuery($table_log, $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
	
    function showAllTicket($query_option)
    {
        $query_string = "SELECT * FROM view_slnc ";
		$count_query = "SELECT COUNT(*) AS row_total FROM slnc_main ";
        
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
    function showAllTicketFiltered($filter, $query_option)
    {
        
        $query_string = "
        SELECT
              `slnc_main`.`ticket_id`         AS `ticket_id`,
              `slnc_main`.`ticket_title`      AS `ticket_title`,
              `slnc_type`.`type_name`         AS `type_name`,
              `slnc_main`.`category`          AS `category_id`,
              `slnc_category`.`category_name` AS `category_name`,
              `slnc_main`.`po_number`         AS `po_number`,
              `slnc_status`.`status_name`     AS `status_name`,
              `slnc_main`.`close_status`      AS `close_status`,
              `slnc_main`.`close_time`        AS `close_time`,
              `slnc_main`.`item_code`         AS `item_code`,
              `slnc_main`.`item_name`         AS `item_name`,
              `slnc_main`.`uom`               AS `item_uom`,
              `slnc_main`.`order_qty`         AS `order_qty`,
              `slnc_main`.`rcv_qty`           AS `rcv_qty`,
              `slnc_main`.`supplier`          AS `supplier`,
              `slnc_main`.`content`           AS `content`,
              `users`.`user_id`                AS `user`,
              `users`.`display_name`           AS `user_name`,
              `users`.`email`                  AS `user_email`,
              `slnc_main`.`store`             AS `store`,
              `store`.`store_name`            AS `store_name`,
              `assigned`.`user_id`            AS `staff`,
              `assigned`.`display_name`       AS `staff_name`,
              `creator`.`user_id`             AS `creator`,
              `creator`.`display_name`        AS `creator_name`,
              `slnc_status`.`status_name`     AS `status`,
              `slnc_status`.`status_id`       AS `status_id`,
              `slnc_main`.`submit_date`       AS `submit_date`,
              `slnc_main`.`last_update`       AS `last_update`,
              `slnc_main`.`resolved_date`     AS `resolved_date`,
              `slnc_main`.`resolved_solution` AS `resolved_solution`,
              `slnc_main`.`rcv_date`          AS `rcv_date`,
              `slnc_main`.`order_date`        AS `order_date`
            FROM `slnc_main`
                LEFT JOIN `slnc_type` ON (`slnc_main`.`ticket_type` = `slnc_type`.`type_id`)
                LEFT JOIN `slnc_status` ON (`slnc_main`.`status` = `slnc_status`.`status_id`)
                LEFT JOIN `slnc_category` ON (`slnc_main`.`category` = `slnc_category`.`category_id`)
                LEFT JOIN sushitei_portal.portal_user assigned ON (`slnc_main`.`assigned_staff` = `assigned`.`user_id`)
                LEFT JOIN sushitei_portal.portal_user users ON (`slnc_main`.`ticket_user` = users.`user_id`)
                LEFT JOIN sushitei_portal.portal_user creator ON (`slnc_main`.`ticket_creator` = `creator`.`user_id`)
                LEFT JOIN sushitei_portal.portal_store store ON (`slnc_main`.`store` = store.`store_id`)
            ";
        $num = 0;
        $count_query = "SELECT COUNT(*) AS row_total FROM slnc_main ";
        if(count($filter) > 0){
            $query_string .= "WHERE ";
            $count_query .= "WHERE ";
            
            foreach($filter as $cat => $arr){
                if($cat != 'date_end' && !empty($arr['value'])){
                    if(!empty($cat) && $num > 0){
                        $query_string .= "AND ";
                        $count_query .= "AND ";
                    } 
                    if($cat == 'submit_date' && !empty($filter['date_end']['value'])){
                        $query_string .= "submit_date BETWEEN '{$arr['value']} 00:00:00' AND '{$filter['date_end']['value']} 23:59:00' ";
                        $count_query .= "submit_date BETWEEN '{$arr['value']} 00:00:00' AND '{$filter['date_end']['value']} 23:59:00' ";                    
                    }elseif($cat == 'get_all'){
                        $query_string .= "category IN (SELECT category_id FROM it_category WHERE category_parent_id='{$arr['value']}') ";
                        $count_query .= "category IN (SELECT category_id FROM it_category WHERE category_parent_id='{$arr['value']}') ";
                    }else{
                        $query_string .=  "{$cat} {$arr['type']} '{$arr['value']}' ";
                        $count_query .= "{$cat} {$arr['type']} '{$arr['value']}' ";
                    }
                }
                $num++;
            }
        }
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
	function showMyActiveTicket($my_id, $query_option)
    {
        $query_string = "SELECT * FROM view_slnc ";
		$query_string .= "WHERE user='{$my_id}'";
        $count_query = "SELECT COUNT(*) AS row_total FROM view_slnc WHERE user='{$my_id}'";
		
        $result = $this->pagingQuery($query_string,$count_query,$query_option);
        return $result;
    }
    
    function showTicketBy($input)
    {
        $query_string = "SELECT * FROM view_slnc ";
        if(isset($input['my_id'])){
            $query_string .= "WHERE user=:my_id AND ticket_id=:ticket_id";
        } else {
            $query_string .= "WHERE ticket_id=:ticket_id";
        }
        $result = $this->fetchSingleQuery($query_string, $input);
        
		return $result;
    }
    
    function getLog($input){
        $query_string = "SELECT log_title, user, display_name, log_type, log_content, log_time FROM slnc_log ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user sppu ON slnc_log.user = sppu.user_id ";
        $query_string .= "WHERE ticket = :ticket_id AND visible = 1 ORDER BY log_time DESC";
        $result = $this->fetchAllQuery($query_string, $input);
		return $result;
    }
    
	function getTicketType()
    {
        $query_string = 'SELECT * FROM slnc_type';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM slnc_category WHERE category_parent_id=1';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM slnc_category WHERE category_parent_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getUploadedFile($query_value)
	{
		$query_string = 'SELECT * FROM slnc_upload WHERE ticket_id=:ticket_id';
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getAllActiveStore()
    {
        $query_string = "SELECT * FROM sushitei_portal.portal_store WHERE store_status=1";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getMailRecipient(){
        $query_string = 'SELECT pu.email,pu.display_name FROM sushitei_portal.portal_user pu ';
        $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
        $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
        $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="receiveEmail" AND pp.permission_site='.getConfig('site_number').' GROUP BY pu.email';
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function myOutlet($input){
        $query_string = "SELECT sppu.store_id,spps.store_name FROM sushitei_portal.portal_user sppu LEFT JOIN sushitei_portal.portal_store spps ON sppu.store_id=spps.store_id WHERE sppu.user_id=:user_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    function getAllSLNC(){
        $query_string = 'SELECT pu.user_id,pu.display_name FROM sushitei_portal.portal_user pu ';
        $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
        $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
        $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="SLNCEngineer" AND pp.permission_site='.getConfig('site_number').' GROUP BY pu.email';
        
        #$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE role_id=9";
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getCategoryDetail($input){
        $query_string = "SELECT * FROM slnc_category WHERE category_id=:input";
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
}