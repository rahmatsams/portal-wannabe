<?php 
/*
* Class Model Categories for Category Controller
*/
class ModelPromiseBK extends Model
{
    function __construct(){
        $config = array('db' => 'MSSQL','host' => '172.16.2.210','db_name' => 'Attendance-Sushi','user_name' => 'useratt','password' => 'attST2017new');
        parent::__construct($config);
    }
	
    function getMyData($input)
    {
        $query_string = 'SELECT * FROM USERINFO WHERE ';
        /*
        if(is_array($excluded) && count($excluded) > 0){
            $query_string .= 'nomor NOT IN (';
            foreach($excluded as $item){
                $query_string .= "'{$item['exc_item_code']}',";
            }
            $query_string = rtrim($query_string,',');
            $query_string .= ') AND ';
        }
        */
        $query_string .= 'BADGENUMBER=:userid';
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    function getMyList($f, $qo)
    {
        $query_string = 'SELECT * FROM CHECKINOUT ';
        $count_query = "SELECT COUNT(*) AS row_total FROM CHECKINOUT ";
        $num = 0;
        foreach($f as $key => $value){
            if($num > 1){
                if($key != 'date_end' && is_array($value)){
                    if($key == 'date_start' && !empty($f['date_end'])){
                        $query_string .= "AND CHECKTIME BETWEEN '{$value} 00:00:00' AND '{$f['date_end']} 23:59:00' ";
                    }else{
                        $query_string .= "AND {$key} {$value['type']} '{$value['value']}' ";
                    }
                }
            } else {
                if($key != 'date_end' && is_array($value)){
                    if($key == 'date_start' && !empty($f['date_end'])){
                        $query_string .= "WHERE CHECKTIME BETWEEN '{$value} 00:00:00' AND '{$f['date_end']} 23:59:00' ";
                    }else{
                        $query_string .=  "WHERE {$key} {$value['type']} '{$value['value']}' ";
                    }
                }
            }
            
            $num++;
        }
        $query_string .=  "ORDER BY CHECKTIME ASC";
		$result = $this->fetchallQuery($query_string);
        return $result;
    }
}
