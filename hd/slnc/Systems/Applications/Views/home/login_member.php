
        <div class="container-fluid">
                <div class="row">
                    <div id="loginForm" style="margin-top: 20px;">
                        <h1 class="form-signin add-margin-bot well" style="background: rgb(51,122, 183); color: #fff;">Login</h1>
                        <form class="form-signin well" id="login">
                            
                            <div class="form-group" id="userName">
                                <label class="control-label sr-only" for="inputName">Terjadi kesalahan, coba cek ulang informasi login anda.</label>
                                <label for="inputName">Nama user</label>
                                <input name="last_url" type="hidden" id="lastUrl" class="form-control" value="<?=$_mySession['last_url']?>">
                                <input name="ticket_email" type="text" id="inputName" class="form-control" placeholder="Nama User" maxlength="25" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="sr-only">Password</label>
                                <input name="ticket_password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" name="login">Sign in</button>
                            </div>
                            Not registered yet? Register <a href="#" data-toggle="modal" data-target="#registermodal">here</a> !
                        </form>
                    </div>
                    <div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Registration Form</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12 well">
                                            <div class="row">
                                                <form id="register" method="POST" action="register.html">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label <?=isset($flash_data['error']['user_name']) ? '' : 'sr-only'?>" for="userName"><?=isset($flash_data['error']['user_name']) ? $flash_data['error']['user_name'] : ''?></label>
                                                            <label for="userName">Nama User</label>
                                                            <input id="userName" name="user_name" type="text" placeholder="Cth: stpim (tidak ada spasi dan Karakter Spesial)" class="form-control" maxlength="25" required>
                                                        </div>					
                                                        <div class="form-group">
                                                            <label class="control-label <?=isset($flash_data['error']['display_name']) ? '' : 'sr-only'?>" for="displayName"><?=isset($flash_data['error']['display_name']) ? $flash_data['error']['display_name'] : ''?></label>
                                                            <label for="displayName">Nama tampilan</label>
                                                            <input id="displayName" name="display_name" type="text" placeholder="Cth: Leonardo Da Vinci" class="form-control" maxlength="25" required>
                                                        </div>	
                                                        <div class="row">
                                                            <div class="col-sm-6 form-group" id="passwordForm">
                                                                <label class="control-label <?=isset($flash_data['error']['password']) ? '' : 'sr-only'?>" for="passwordInput"><?=isset($flash_data['error']['password']) ? $flash_data['error']['password'] : ''?></label>
                                                                <label>Password</label>
                                                                <input id="passwordInput" name="password" type="password" placeholder="Masukkan password.." class="form-control" required>
                                                            </div>	
                                                            <div class="col-sm-6 form-group" id="passwordConfirmForm">
                                                                <label class="control-label <?=isset($flash_data['error']['password_confirm']) ? '' : 'sr-only'?>" for="passwordConfirm"><?=isset($flash_data['error']['password_confirm']) ? $flash_data['error']['password_confirm'] : ''?></label>
                                                                <label>Konfirmasi password</label>
                                                                <input id="passwordConfirm" name="password_confirm" type="password" placeholder="Masukkan konfirmasi password.." class="form-control" required>
                                                            </div>	
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label <?=isset($flash_data['error']['email']) ? '' : 'sr-only'?>" for="eMail"><?=isset($flash_data['email']) ? $flash_data['error']['email'] : ''?></label>
                                                            <label>Alamat Email</label>
                                                            <input id="eMail" name="email" type="email" placeholder="Masukkan alamat email.." class="form-control" required>
                                                        </div>		
                                                        <div class="form-group">
                                                            <label>Outlet</label>
                                                            <select name="outlet" class="form-control" required>
                                                            <?php
                                                                if(is_array($store)){
                                                                    foreach($store as $outlet){
                                                                        echo " 
                                                                        <option value='{$outlet['store_id']}'>{$outlet['store_name']}</option>";
                                                                    }
                                                                    
                                                                }
                                                            ?>
                                                            </select>
                                                        </div>	
                                                        <div class="row">
                                                            <div class="col-sm-6 form-group">
                                                                <label>Pertanyaan keamanan</label>
                                                                <select name="sec_quest" class="form-control" required>
                                                                    <option value="" default></option>
                                                                    <option value="1">Dimanakah outlet pertama anda?</option>
                                                                    <option value="2">Siapakah nama panggilan kecil anda?</option>
                                                                    <option value="3">Siapakah nama ibu anda?</option>
                                                                    <option value="4">Apakah makanan kesukaan anda?</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-6 form-group">
                                                                <label>Jawaban</label>
                                                                <input name="answer" type="text" placeholder="Masukkan jawaban pertanyaan keamanan.." class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-lg btn-primary" type="submit" class="btn btn-lg btn-info" name="register" value="Daftar">Daftar</button>
                                                    </div>
                                                </form> 
                                            </div>    
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
 <!-- Bootstrap core JavaScript
