            <div class="card-deck">
            <div class="card">
              <div class="card-header">
                  <form id="searchMRP" class="forms-sample">
                    <div class="form-group">
                      <label for="selectMonth"><strong class="card-title">MRP Detail</strong></label>
                      <input type="month" name="date" class="form-control p-input" id="selectMonth" aria-describedby="emailHelp" placeholder="Select Month" value="<?=date("Y-m")?>">
                    </div>
                </form>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="indexContent" class="table center-aligned-table table-striped">
                    <thead>
                      <tr class="text-primary">
                        <th>No</th>
                        <th style="min-width: 180px;">Outlet</th>
                        <th style="min-width: 180px;">Equipment Availability</th>
                      </tr>
                    </thead>
                    <tbody>
						<?php
							if(is_array($result) && count($result) > 0){
								$i=1;
								foreach($result as $d){
									echo "
                                <tr class=''>
                                    <td>{$i}</td>
                                    <td>".(isset($d['store_id']) && isset($d['group_year']) && isset($d['group_month']) ? "<a href=\"view_mrp_{$d['store_id']}_{$d['group_year']}_{$d['group_month']}.html\" class='btn btn-primary d-block'>{$d['store_name']}</a>" : $d['store_name'])."</td>
                                  <td></td>
						        </tr>";
								$i++;
								}
							}
						?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>