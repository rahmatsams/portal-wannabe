<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="admin_category.html">Manage Equipment</a></li>
        <li class="breadcrumb-item active" aria-current="page"><b><?=$result['area_name']?></b></li>
    </ol>
</nav>
<?php
    if(is_array($fdata) && count($fdata) > 0)
    {
        if($fdata['success'] == 1){
            echo '<div class="p-3 mb-2 bg-success text-white">Updated Successfully !!</div>';
        }else{
            echo '<div class="p-3 mb-2 bg-danger text-white">Please check your input !!</div>';
            }
    }
?>
<div class="card-deck">
    <div class="card col-lg-12 px-0 mb-4"> 
        <!-- <div class="card-header">
            <strong class="card-title">Manage Equipment Pemeriksaan / Perawatan</strong>
        </div> -->
    <div class="card-body">
<div class="card-deck">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Edit Equipment</strong>
        </div>
    <div class="card-body p-0 pt-3 pb-3">
      <form id="editcategory" method="POST" action="admin_edit_category_<?=$result['area_id']?>.html">
        <div class="col-md-12 form-group">
            <label>Name</label>
            <input id="catid" name="area_id" type="hidden" value="<?=$result['area_id']?>" class="form-control" maxlength="6" required>
            <textarea name="area_name" id="insertCatName" class="form-control" cols="70" rows="2" required><?=$result['area_name']?></textarea>
        </div>
        <div class="col-md-12 form-group">
            <input type="submit" name="action" value="Update" class="btn btn-md btn-success waves-effect">
        </div>
      </form>
    </div>
    </div>
<div class="card">
    <div class="card-header">
        <strong class="card-title">Add Item</strong>
    </div>
<div class="card-body p-0 pt-3 pb-3">
    <form id="addsubcategory" method="POST" action="admin_edit_subcategory.html">
        <div class="col-md-12 form-group">
            <label for="catid">Item Name</label>
            <input id="catid" name="area_id" type="hidden" value="<?=$result['area_id']?>" class="form-control" maxlength="6" required>
            <textarea name="category_name" id="insertCatName" class="form-control" cols="70" rows="2" required></textarea>
        </div>
        <div class="col-md-12 form-group">
            Bulanan&nbsp; &nbsp;<input type="checkbox" name="category_monthly" value="1">
        </div>
        <div class="col-md-12 form-group">
            <input type='submit' class='btn btn-md btn-primary waves-effect' name="action" value="Add Item">
        </div>
    </form>
</div>
</div>
</div>
</div>
<div class="card-header">
    <strong class="card-title">Item Pemeriksaan / Perawatan</strong>
</div>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
    <thead>
        <tr>
            <th width="5%">ID</th>
            <th width="70%">Item</th>
            <th width="50%">Bulanan</th>
            <th colspan="2">Perintah</th>
        </tr>
    </thead>
    <tbody>
    <?php
        if(count($list) > 0){
            foreach($list as $result){
        echo "
            <form method='POST' action='admin_edit_subcategory_{$result['category_id']}.html'>
                <tr class='form-group' id='formEdit{$result['category_id']}'>
                    <td>
                        <input type='hidden' name='category_id' value='{$result['category_id']}' required>
                        <input type='hidden' name='area_id' value='{$result['area_id']}' readonly>{$result['category_id']}
                    </td>
                    <td>
                        <textarea name='category_name' cols='70' rows='2' readonly class='form-control-plaintext' required>{$result['category_name']}</textarea>
                    </td>
                    <td>
                        ".($result['category_monthly'] == 1 ? 'Ya' : 'Tidak')."
                    </td>
                    <td>
                        <a href=\"admin_edit_subcategory_{$result['category_id']}.html\" class='manage-button1' data-id='{$result['category_id']}'><button type=\"button\" class=\"btn btn-md btn-success waves-effect btn-block\">Edit</button></a><input type='submit' name='action' class='btn btn-sm btn-primary d-none' value='Edit'>
                    </td>
                    <td>
                        <a href=\"#\" class=\"delete_sub deleteconfirmationmodal\" data-id=\"{$result['category_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"btn btn-md btn-danger waves-effect btn-block\">Hapus</button></a>
                    </td>
                </tr>
            </form>";
    }
        }else{
        echo '
    <tr>
        <td colspan="6" align="center">No Record</td>
    </tr>';
        }
?>
    </tbody>
        </table>
            </div>
        </div>
    </div>
</div>
<!-- DELETE SUB CATEGORY -->
<div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel">Hapus Sub Category</h5>
                </div>
                <div class="modal-body">
                    Apa kamu yakin ingin menghapus Sub Category ini?
                </div>
                <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <!-- id="admin_recom_delete" untuk jquery-->    
                <a id="admin_subcategory_delete" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                              
                </div>
            </div>
        </div>
    </div>
</div>
                   
<!-- JQUERY DELETE SUB CATEGORY -->
<script type="text/javascript">
    $(document).ready(function(){
        $(".delete_sub").click(function() {
            var id = $(this).attr('data-id'); /*data-id ada di button Hapus*/
            var cat_id = $("#catid").val();
            $("#admin_subcategory_delete").attr("href", "admin_subcategory_delete_"+id+"_"+cat_id+".html"); 
        });
    });
</script>
                                            
        