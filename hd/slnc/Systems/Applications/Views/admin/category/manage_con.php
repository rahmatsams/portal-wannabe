<?php 
    $cat = $result['condition_name'];
?>
<?php 
    $dw = $result['category_name'];
?>
<?= substr($cat, 0, 10), ".."?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="admin_edit_storearea_<?=$result['area_id']?>.html">Manage <?=$result['area_name']?></a></li>
        <li class="breadcrumb-item"><a href="admin_edit_subcategory_<?=$result['category_id']?>.html">Item <?= substr($dw, 0, 10), ".." ?></a></li>
        <li class="breadcrumb-item active" aria-current="page">Kondisi <?= substr($cat, 0, 7), ".." ?> </li>
    </ol>
</nav>

<?php
    if(is_array($fdata) && count($fdata) > 0){
        if($fdata['success'] == 1){
        echo '<div class="p-3 mb-2 bg-success text-white">Updated Successfully !!</div>';
    }else{
        echo '<div class="p-3 mb-2 bg-danger text-white">Please check your input !!</div>';
        }
    }
?>

<div class="card-deck">
    <div class="card col-lg-12 px-0 mb-4"> 
        <div class="card-header">
        <strong class="card-title">Manage Item Pemeriksaan / Perawatan</strong>
    </div>
<div class="card-body">
    <div class="card-deck">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Edit Kondisi</strong>
            </div>
        <div class="card-body p-0 pt-3 pb-3">
            <form id="editcategory" method="POST" action="admin_edit_condition_<?=$result['condition_id']?>.html">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Kondisi</label>
                    <input id="subid" name="category_id" type="hidden" value="<?=$result['category_id']?>" class="form-control" maxlength="6" required>
                    <input id="catid" name="condition_id" type="hidden" value="<?=$result['condition_id']?>" class="form-control" maxlength="6" required>
                    <textarea id="catname" name="condition_name" cols="50" maxlength="100" rows="3" class="form-control" required><?=$result['condition_name']?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" name="action" value="Edit" class="btn btn-md btn-success waves-effect">
                </div>
            </div>
            </form>
        </div>
        </div>
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Tambah Tindakan</strong>
            </div>
        <div class="card-body p-0 pt-3 pb-3">
            <form id="addsubcategory" method="POST" action="admin_add_action.html">
                <div class="col-md-12 form-group">
                    <label for="insertCatName">Tindakan</label>
                    <input id="subids" name="condition_id" type="hidden" value="<?=$result['condition_id']?>" class="form-control" maxlength="6" required>
                    <textarea name="action_name" id="insertCatName" class="form-control" cols="70" rows="3" required></textarea>
                </div>
                <div class="col-md-12 form-group">
                    <input type='submit' class='btn btn-md btn-primary' name="action" value="Tambah">
                </div>
            </form>
        </div>
        </div>
        </div>
        </div>
<div class="card-header">
    <strong class="card-title">Manage Point</strong>
</div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tindakan</th> 
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
            if(count($list) > 0){
                foreach($list as $result){
                echo "
                <tr class='form-group' id='formEdit{$result['action_id']}'>
                <form method='POST' action='admin_edit_action_{$result['condition_id']}.html'>
                    <td>
                        <input type='hidden' name='condition_id' value='{$result['condition_id']}' readonly>
                        <input type='hidden' name='action_id' value='{$result['action_id']}' readonly>{$result['action_id']}
                    </td>
                    <td>
                        <textarea name='action_name' cols='70' rows='3' readonly class='form-control-plaintext' required>{$result['action_name']}</textarea>
                    </td>                
                    <td>
                        <a href=\"admin_edit_action_{$result['action_id']}.html\" class='manage-button' data-id='{$result['action_id']}'><button type=\"button\" class=\"btn btn-success btn-md btn-block\">Edit</button></a><input type='submit' name='action' class='btn btn-warning btn-md btn-block d-none' value='Edit'>
                    </td>
                    <td>
                        <a href=\"#\" class=\"delete_con deleteconfirmationmodal\" data-id=\"{$result['action_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"btn btn-danger btn-md btn-block\">Hapus</button></a>
                    </td>
                </form>
                </tr>";
                    }
                        }else{
                echo '
                    <tr>
                        <td colspan="6" align="center">No Record</td>
                    </tr>';
                        }
        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            <!-- DELETE CONDITION -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">Hapus Tindakan</h5>
                                    </div>
                                    <div class="modal-body">
                                        Apa kamu yakin ingin menghapus Data Tindakan ini?
                                    </div>
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <!-- id="admin_recom_delete" untuk jquery-->    
                                        <a id="admin_action_delete" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
            </div>
        </div>
        <!-- JQUERY DELETE SUB CATEGORY -->
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".delete_con").click(function() {
                            var id = $(this).attr('data-id'); /*data-id ada di button Hapus*/
                            var sub_id = $("#subids").val();
                            $("#admin_action_delete").attr("href", "admin_action_delete_"+id+"_"+sub_id+".html"); 
                        });
                    });
                </script>