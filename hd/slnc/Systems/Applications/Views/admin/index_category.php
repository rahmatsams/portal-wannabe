	<div class="container-fluid">
        <h2 class="sub-header">Kelola Kategori</h2>
        <ol class="breadcrumb">
            <li><a href="ticket_admin.html">Halaman Admin</a></li>
            <li class="active"><a href="#">Kelola Kategori</a></li>
        </ol>
		<div class="table-responsive">
            <h3>Kategori Utama</h3>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Kategori</th>
                        <th>Deskripsi</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($category) && !empty($category)){
                            $num = 1;
                            foreach($category as $result){
                                        
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['category_name']}</td>
                        <td>{$result['description']}</td>
                        <td><button type='button' id=\"{$result['category_id']}\" class='edit-category btn btn-primary btn-sm' data-toggle='modal' data-target='#myModal'>Edit</button></td>
                        <td><a class=\"delete_category deleteconfirmationmodal\" data-id=\"{$result['category_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\">Delete</a></td>
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="3">Kategori Kosong</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2">Tambah Kategori Baru</button>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12 well">
                                <div class="row">
                                    <form id="editcategory">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Kategori</label>
                                                <input id="catid" name="category_id" type="hidden" value="" class="form-control" maxlength="25" required>
                                                <input id="catname" name="category_name" type="text" value="" class="form-control" maxlength="25" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Deskripsi Kategori</label>
                                                <textarea id="catdesc" name="description" cols="20" rows="4" class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" name="submit" value="kirim" class="btn btn-sm btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                 </div>
                                 
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Tambah Kategori</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12 well">
                                <div class="row">
                                    <form id="addcategory">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Kategori</label>
                                                <input name="category_name" type="text" value="" class="form-control" maxlength="25" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Deskripsi Kategori</label>
                                                <textarea name="category_description" cols="20" rows="4" class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" name="submit" value="kirim" class="btn btn-sm btn-primary">Tambah</button>
                                            </div>
                                        </div>
                                    </form>
                                 </div>
                                 
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Hapus Kategori</h4>
                        </div>
                        <div class="modal-body">
                            <p class="content-padding bg-danger">Apa kamu yakin ingin menghapus kategori ini?</p>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>