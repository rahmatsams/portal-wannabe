<div class="content-right">
    
    <div class="card-body col-lg-6">
        <div class="card-deck">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">New Email</strong>
                </div>    
	<div class="card-body p-0 pt-3 pb-3">
        <form id="addEmail" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['pic_name']) ? "<div class=\"alert alert-danger\"><strong>PIC Name </strong>{$error['pic_name']}</div>" : '<label>PIC Name</label>')?>
                    <input name="pic_name" type="text" placeholder="Name" class="form-control" maxlength="50" required>
                </div>
            <!-- Email -->
                <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['email']) ? "<div class=\"alert alert-danger\"><strong>Store Email </strong>{$error['email']}</div>" : '<label>Email</label>')?>
                    <input name="email" type="email" placeholder="Email" class="form-control" maxlength="50" required>
                </div>
                <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['email_status']) ? "<div class=\"alert alert-danger\"><strong>Status </strong>{$error['email_status']}</div>" : '<label>Status </label>')?>
                    <select name="email_status" class="form-control">
                    <option value="0">Disabled</option>
                    <option value="1">Enabled</option></select>
                </div>
                <div class="col-md-12 form-group">
            <button type="submit" name="submit" value="create_email" class="btn btn-primary ">Create</button></div>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>


					