<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    
    <title>Login - <?=getConfig('site_name')?></title>
    
    
    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="Resources/assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="Resources/assets/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">


    <!-- Custom styles for this template -->
    <link href="Templates/st_ticketdesk/css/dashboard.css" rel="stylesheet">
    <link href="Templates/st_ticketdesk/css/styles.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="Resources/images/Ico/favicon.png">
</head>

<body style="background: url('Resources/images/bg.jpg'); background-size: cover;">
    <div class="container">
		<div class="row">
			<div class="container-fluid">
				<div class="main" style="margin-top: 20px;">
					<h1 class="form-signin add-margin-bot well">SLNC</h1>
					<form class="form-signin well" id="loginForm">
						
						<div class="form-group" id="userName">
							<label class="control-label sr-only" for="inputName">Please check your input.</label>
							<label for="inputName">Nama user</label>
							<input name="last_url" type="hidden" id="lastUrl" class="form-control" value="<?=$_mySession['last_url']?>">
							<input name="ticket_email" type="text" id="inputName" class="form-control" placeholder="Nama User" maxlength="15" required autofocus>
						</div>
						<div class="form-group">
							<label for="inputPassword" class="sr-only">Password</label>
							<input name="ticket_password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
						</div>
                        <div class="form-group <?=$_mySession['attempt'] > 5 ? '' : 'hidden'?>" id="recaptchaDiv">
						</div>
                        
						<div class="form-group">
							<button class="btn btn-lg btn-primary btn-block" name="login" <?=$_mySession['attempt'] > 5 ? 'disabled' : ''?>>Sign In</button>
						</div>
                        <p>Not registered yet? Register <a href="#" data-toggle="modal" data-target="#registermodal">here</a> !</p>
                        <p>Forgot your password? <a href="#" data-toggle="modal" data-target="#forgotpassmodal">click here</a> !</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
            <div class="modal-content" id="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Registration Form</h4>
                </div>
                <div class="modal-body well">
                    <div class="col-md-12">
                        <div class="row">
                            <form id="registrationForm">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="userName">User Name</label>
                                        <input id="userName" name="user_name" type="text" placeholder="Ex: rahmat.s (No space and Special Character except dot)" class="form-control" maxlength="25" required>
                                    </div>					
                                    <div class="form-group">
                                        <label for="displayName">Display Name</label>
                                        <input id="displayName" name="display_name" type="text" placeholder="Ex: Rahmat Samsudin" class="form-control" maxlength="25" required>
                                    </div>	
                                    <div class="row">
                                        <div class="col-sm-6 form-group" id="passwordForm">
                                            <label class="control-label" for="passwordInput"></label>
                                            <label>Password</label>
                                            <input id="passwordInput" name="password" type="password" placeholder="Min 6 character ..." class="form-control" required>
                                        </div>	
                                        <div class="col-sm-6 form-group" id="passwordConfirmForm">
                                            <label class="control-label" for="passwordConfirm"></label>
                                            <label>Confirm Password</label>
                                            <input id="passwordConfirm" name="password_confirm" type="password" placeholder="Min 6 character ..." class="form-control" required>
                                        </div>	
                                    </div>
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input id="eMail" name="email" type="email" placeholder="Type email address..." class="form-control" required>
                                    </div>		
                                    <div class="form-group">
                                        <label>Outlet</label>
                                        <select name="outlet" class="form-control" required>
                                            <option></option>
                                <?php
                                    if(is_array($store)){
                                        foreach($store as $outlet){
                                            echo " 
                                            <option value='{$outlet['store_id']}'>{$outlet['store_name']}</option>";
                                        }
                                        
                                    }
                                ?>
                                        </select>
                                    </div>	
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>Security Question</label>
                                            <select name="sec_quest" class="form-control" required>
                                                <option value="" default></option>
                                                <option value="1">Dimanakah outlet pertama anda?</option>
                                                <option value="2">Siapakah nama panggilan kecil anda?</option>
                                                <option value="3">Siapakah nama ibu anda?</option>
                                                <option value="4">Apakah makanan kesukaan anda?</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label>Answer</label>
                                            <input name="answer" type="text" placeholder="Type the answer.." class="form-control" required>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-md btn-primary" name="register" value="Daftar">Register</button>
                                </div>
                            </form> 
                        </div>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
		</div>
	</div>
    <div class="modal fade" id="forgotpassmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
            <div class="modal-content" id="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Forget Password Form</h4>
                </div>
                <div class="modal-body well">
                    <div class="col-md-12">
                        <div class="row">
                            <form id="forgotForm">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="userName">User Name</label>
                                        <input id="userName" name="user_name" type="text" placeholder="Ex: rahmat.s (No space and Special Character except dot)" class="form-control" maxlength="25" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>Security Question</label>
                                            <select name="security_question" class="form-control" required>
                                                <option value="" default></option>
                                                <option value="1">Dimanakah outlet pertama anda?</option>
                                                <option value="2">Siapakah nama panggilan kecil anda?</option>
                                                <option value="3">Siapakah nama ibu anda?</option>
                                                <option value="4">Apakah makanan kesukaan anda?</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label>Answer</label>
                                            <input name="security_answer" type="text" placeholder="Type the answer.." class="form-control" required>
                                        </div>
                                    </div>                                    
                                    <button type="submit" class="btn btn-md btn-primary" name="register" value="Daftar">Send Recovery Email</button>
                                </div>
                            </form> 
                        </div>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
		</div>
	</div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="Resources/assets/jquery/jquery-1.12.3.min.js"></script>
    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
    <script src="Resources/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script src="Resources/js/main.js"></script>
    <script>
        var onloadCallback = function() {
            grecaptcha.render('recaptchaDiv', {
                'sitekey': '6LemwyETAAAAACZCN9RkUJ1D4j_bxeBvu1lgmAZP',
                'callback' : verifyCallback
            });
        };
        var verifyCallback = function (response) {
            if (response.length == 0) { 
            
            }//reCaptcha not verified
            else {
                $("#loginForm button[name='login']").removeAttr("disabled");
            }//reCaptch verified      
        };
        $(document).ready(function(){
            $("#loginForm").submit(function(event){
                // cancels the form submission
                event.preventDefault();
                var me = $(this);
                var forms = $("#loginForm");
                loginForm(me, forms);
            });
            $("#registrationForm").submit(function(event){
                // cancels the form submission
                event.preventDefault();
                var me = $(this);
                var form = $("#registrationForm");
                doRegister(me, form);
            });
            $("#forgotForm").submit(function(event){
                // cancels the form submission
                event.preventDefault();
                var me = $(this);
                var form = $("#forgotForm");
                doForgot(me, form);
            });            
        });
    </script>
</body>
</html>
