            <div class="container-fluid">    
                <div class="row" style="background: rgb(22, 105, 173); color: white; padding: 5px;">
                    <div class="col-lg-6">
                        <h4><?=$ticket['ticket_title']?></h4>
                    </div>
                    <div class="col-lg-6">
                        <h4>SLNC ID: <?=$ticket['ticket_id']?></h4>
                    </div>
                </div>
                
                <div class="col-lg-12" style="min-height: 360px;">
                    <form id="detailedit">
                        <div class="row" id="tickettabledetail">
                            <div class="col-lg-6">
                                <table class="table table-striped" id="listTable">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold;">Supplier</td>
                                            <td>:</td>
                                            <td><?=$ticket['supplier']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Kategori Supplier</td>
                                            <td>:</td>
                                            <td><?=$ticket['category_name']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Penyimpangan</td>
                                            <td>:</td>
                                            <td><?=$ticket['type_name']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Pemilik</td>
                                            <td>:</td>
                                            <td><?=$ticket['creator_name']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">PIC</td>
                                            <td>:</td>
                                            <td><?=$ticket['staff_name']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Submit Time</td>
                                            <td>:</td>
                                            <td><?=$ticket['submit_date']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Last Update</td>
                                            <td>:</td>
                                            <td><?=$ticket['last_update']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Status</td>
                                            <td>:</td>
                                            <td><?=$ticket['status_name']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Closed</td>
                                            <td>:</td>
                                            <td><?=$ticket['close_status'] == 1 ? 'Yes' : ($ticket['close_status'] == 2 ? 'Forced' : 'No')?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Lain-lain</td>
                                            <td>:</td>
                                            <td><?=htmlspecialchars_decode($ticket['content'], ENT_QUOTES)?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-striped col-lg-6" id="listTable">
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: bold; width: 20%;">Kode Barang</td>
                                            <td style="width: 2%;">:</td>
                                            <td><?=$ticket['item_code']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Nama Barang</td>
                                            <td>:</td>
                                            <td><?=$ticket['item_name']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; width: 20%;">Nomor PO</td>
                                            <td style="width: 2%;">:</td>
                                            <td><?=$ticket['po_number']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Jumlah Order</td>
                                            <td>:</td>
                                            <td><?="{$ticket['order_qty']} {$ticket['item_uom']}"?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Jumlah Terima</td>
                                            <td>:</td>
                                            <td><?="{$ticket['rcv_qty']} {$ticket['item_uom']}"?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Tanggal Order</td>
                                            <td>:</td>
                                            <td><?=$ticket['order_date']?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Tanggal Kedatangan</td>
                                            <td>:</td>
                                            <td><?=$ticket['rcv_date']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row hidden" id="tickettableedit">
                            <div class="form-group">
                                <label>Ticket Type</label>
                                <select name="type" class="form-control" required>
                                    <option value=""></option>
                                    <?php
                                        foreach($ticket_type as $type_input){
                                            echo "\n
                                            <option value=\"{$type_input['type_id']}\"" . ($ticket['ticket_type'] == $type_input['type_id'] ? ' Selected' : '') . ">{$type_input['type_name']}</option>\n";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <select name="main_category" id="mainCategory" class="form-control" required>
                                    <option value=""></option>
                                    <?php
                                        foreach($main_category as $category_form){
                                            echo "\n
                                            <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] || $ticket['category_parent_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Sub Category</label>
                                <select name="sub_category" class="form-control" id="subCategory" <?=(isset($sub_category) && is_array($sub_category) ? '' : 'disabled')?>>
                                    <?php
                                        if(isset($sub_category) && is_array($sub_category)){
                                            foreach($sub_category as $category_form){
                                                echo "\n
                                                <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="checkbox-inline">
                                    <input type="hidden" name="ticket_id" value="<?=$ticket['ticket_id']?>"/>
                                    <input type="checkbox" value="1" name="customer_impact" <?=($ticket['impact'] == 1 ? 'checked' : '')?>>Impact Customer?</input>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
                    <?php
                        if(is_array($uploaded) && count($uploaded) > 0){
                            echo "
                    <div class=\"col-lg-12\">
                        <h4 class=\"well\" style=\"background: rgb(22, 105, 173); color: white; padding: 5px;\" id=\"tickettitle\">Uploaded File</h4>
                    </div>
                    <div class=\"col-lg-12 upload-thumbnail\">
                            ";
                            foreach($uploaded as $img_file){
                                echo "
                        <a href=\"{$img_file['upload_location']}{$img_file['upload_name']}\" target=\"_blank\"><img src=\"{$img_file['upload_location']}{$img_file['upload_name']}\" class=\"img-thumbnail\" style=\"width:100px; height:100px;\"/></a>
                                ";
                            }
                            
                            echo "
                    </div>
                            ";
                        }
                    ?>
                    <div class="col-lg-12">
                        <h4 class="well" style="background: rgb(22, 105, 173); color: white; padding: 5px;" id="tickettitle">Ticket command</h4>
                    </div>
                    <div class="col-lg-12" style="margin-top: 0px; margin-bottom: 5px;" id="activitybutton">
                        <div class="col-lg-12 well">
                            <div class="row">
                                <?php
									#print_r($ticket);
									$menu = array();
									if($ticket['close_status'] == 0 && $admin){
										if($ticket['status'] == 'Open' || $ticket['status'] == 'Assigned'){
                                            $menu['assignbutton'] = 'Assign';
                                        }elseif($ticket['status'] == 'Released' || $ticket['status'] == 'Rejected'){
                                            $menu['closebutton'] = 'Close';
                                        }
									}
									if($ticket['close_status'] == 0 && ($session['group'] == 'Warehouse' || $session['group'] == 'Purchasing')){

                                        if($ticket['status'] == 'Open'){
                                            #$menu['commentbutton'] = 'Comment';
                                            $menu['takeoverbutton'] = 'Take Over';
                                        }
                                        
									}
									
									if($ticket['close_status'] == 0 && $session['user_id'] == $ticket['staff']){

                                        if($ticket['status'] == 'Assigned'){
                                            #$menu['commentbutton'] = 'Comment';
                                            $menu['releasebutton'] = 'Release';
                                            $menu['rejectbutton'] = 'Reject';
                                        }elseif($ticket['status'] == 'Released'){
                                            $menu['forcebutton'] = 'Force-Close';
                                            #$menu['reopenbutton'] = 'Re-Open';
                                        }
                                        
									}

                                    if($ticket['status'] == 'Closed'){
                                        $menu['reopenbutton'] = 'Re-Open';
                                    }
                                    
									foreach($menu as $key => $value){
										echo "
								<div class=\"col-sm-2 form-group\">
									<button type=\"button\" value=\"edit\" class=\"btn btn-sm btn-primary btn-block\" id=\"{$key}\">{$value}</button>
								</div>
										";
									}
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 hidden" id="tickettablecomment">
                        <form id="updateticketstatus">
                            <div class="col-lg-12 well">
                                <div class="row">
                                    <div class="col-sm-6 form-group hidden" id="assignform">
                                        <label>Take Over to :</label>
                                        <select id="userlist" name="selectedpic" class="form-control">
                                            <?php
                                                foreach($staff as $pic){
                                                    echo "\n
                                                    <option value=\"{$pic['user_id']}\" data-id=\"{$pic['display_name']}\">{$pic['display_name']}</option>\n";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 form-group hidden" id="coreProblem">
                                        <label>Problem Cause :</label>
                                        <textarea id="coreproblem" name="cause" placeholder="Type problem cause.." class="form-control" rows="3"></textarea>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <label id="labelcomment">Komentar</label>
                                        <textarea id="updatereason" name="detail" placeholder="Type a comment.." class="form-control" rows="3"></textarea>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-sm-3 form-group">
                                            <button type="submit" name="submit" class="btn btn-sm btn-primary btn-block" id="submitstatus">Update</button>
                                        </div>
                                        <div class="col-sm-3 form-group">
                                            <button type="button" name="cancel" class="btn btn-sm btn-primary btn-block" id="cancelstatus">Batal</button>
                                        </div>
                                    </div>
                                </div>
                            </form>   
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <h4 class="well" style="background: rgb(22, 105, 173); color: white; padding: 5px;">Activity Log</h4>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-12 well">
                            <?php
                            
                                foreach($log as $ticket_log){
                                    
                                    if($ticket_log['user'] == $ticket['user']){
                                        $style = "bg-warning";
                                    } else {
                                        $style = "bg-success";
                                    }
                                    
                                    $date = date('D - d M Y, H:i', strtotime($ticket_log['log_time']));
                                    echo "
                                    <table class='table table-bordered'>
                                        <thead>
                                            <tr class='{$style}'>
                                                <td class='{$style}' style='width: 15%; font-weight:bold;'>{$ticket_log['log_type']}</td>
                                                <td class='{$style}' style='width: 60%; font-weight:bold;'>{$ticket_log['log_title']}</td>
                                                <td class='{$style}' style='width: 25%; font-weight:bold;'>{$date}</td>
                                            </tr>
                                        </thead>".
                                        (!empty($ticket_log['log_content']) ? "<tbody>
                                            <tr>
                                                <td colspan='3' class='bg-white' style='white-space: pre-wrap;'>". nl2br(htmlspecialchars_decode($ticket_log['log_content'])) ."</td>
                                            </tr>
                                        </tbody>" : '') ."
                                    </table>
                                    ";
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>        