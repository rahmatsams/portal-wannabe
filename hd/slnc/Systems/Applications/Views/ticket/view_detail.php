            <?php 
                $monthly = $prp['group_week'];
                $gstatus = $prp['group_status'];
            ?>
            <div class="card-deck">
                <div class="card">
                     <div class="card-body"> 
                        <div class="card-deck">
                            <div class="card">
                                <div class="card-header bg-secondary text-white"><strong class="card-title"><?=$prp['store_name']?> - <?=strftime("%B %Y", strtotime($prp['group_submit_date']))?></strong></div>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="indexContent" class="table">
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Equipment</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['equipment']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Code</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['equipment_code']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Periode</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?= $prp['group_week'] == 0 ? 'Bulanan' : 'Minggu '.$monthly ?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Auditor</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['group_user']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Tim Outlet</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['group_auditee']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Tanggal</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=date("d-m-Y", strtotime($prp['group_submit_date']))?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Kondisi</td>      
                                                 <td class="pt-1 pb-1 pl-3 pr-3">
                                                    <?= $gstatus == 'Normal' ? 'Normal' : '' ?>
                                                    <?= $gstatus == 'Kurang' ? 'Kurang Baik' : '' ?>
                                                    <?= $gstatus == 'Rusak' ? 'Rusak' : '' ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Catatan</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['group_notes']?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"><strong class="card-title">Perintah</strong></div>
                                <div class="card-body">

                                    <div class="col-sm-12 mt-1">
                                        <a href="report_mrp_<?=$prp['group_id']?>.html"><button class="btn btn-small btn-info btn-block">Unduh Report</button></a>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        <br>
                        <?php
                            $item_last='';
                            $i=1;
                            $first = 1;
                            $total = 0;
                            $baik = 0;
                            $rusak = 0;
                            if(is_array($sub) && count($sub) > 0){
                            $total = count($sub);
                            foreach($sub as $d){
                                
                                if ($item_last != $d['category_name']) {
                                    if($first == 0){
                                    echo '</tbody>
                                        </table>
                                    </div>';
                                    $first = 1;
                                    }
                                echo '
                                    <div class="card-header p-3 mb-2 bg-primary text-white">
                                    <tr class="" data-area="'.$d['category_name'].'">
                                        <td colspan="5"><strong>'.$d['category_name'].'</strong></td>
                                    </tr>
                                    </div>                 
                                    
                                    <div class="table-responsive">
                                          <table id="indexContent" class="table center-aligned-table table-striped">
                                            <thead>
                                              <tr class="text-primary">
                                                <th>No</th>
                                                <th>Kondisi</th> 
                                                <th>Gambar</th>
                                                <th>Status</th> 
                                                <th>Keterangan</th>
                                              </tr>
                                            </thead>
                                            <tbody> ';
                                }
                                    $first = 0;
                                    $item_last = $d['category_name'];
                                echo "
                                    <tr class='table-area control-row'>
                                        <td>{$i}</td> 
                                        <td>{$d['condition_name']}</td>
                                        <td>".(!empty($d['finding_images']) ? "<a href=\"./Resources/images/prp/{$d['finding_images']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$d['finding_images']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/{$d['finding_images']}\"></a>" : " ")."</td>
                                        <td>".($d['status'] == 1 ? 'Ya' : 'Tidak')."</td>
                                        <td>{$d['notes']}</td>
                                    </tr>";
                                    if($d['status'] == 1){
                                        $baik = $baik+ 1;
                                    }else{
                                        $rusak = $rusak+1;
                                    }

                                    $i++;
                            }
                            echo "
                                <tr class='table-area control-row bg-secondary text-white'>
                                    <td><b>Persentase</b></td> 
                                    <td colspan='4'><b>".number_format((100 - (($rusak / $total) * 100)), 0)."%</b></td>
                                </tr>
                            ";
                        ?>
                                            </tbody>
                                          </table>
                                        </div>
                        <?php
                            }else{
                                echo '<div class="p-3 mb-2 bg-success text-white">Tidak ditemukan data MRP periode ini</div>';
                            }
                        ?>
                    </div>
                    
                    <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-warning text-white" style="position: fixed; bottom: 40px; right: 30px; z-index: 99; border-radius: 4px;">Top</button>  