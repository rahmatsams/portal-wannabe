<style>
    .modal-mrp 
    {
    width: 850px;
    position: relative;
    }
    
    .modal-admin 
    {
    width: 900px;
    position: relative;
    }

    #rcorners1 {
      border-radius: 15px;
      background: #ABB2B9;
      padding-top: 5px;
      padding-bottom: 30px; 
      width: 200px;
      height: 15px;  
    }

    #rcorners2 {
      border-radius: 15px;
      border: 2px solid #ABB2B9;
      padding-top: 5px;
      padding-bottom: 15px; 
      width: 200px;
      height: 15px;  
    }

</style>

<div class="card">
  <div class="card-header">
    SUMMARY
  </div>
  <div class="card-body">
    <h5 class="card-title"><?= $prp[0]['store'] ?> - <?= date('F Y',strtotime($prp[0]['group_submit_date'])) ?></h5>
  </div>
</div>
<div class="card-deck">
    <div class="card">
        <div class="card-body"> 
            <div class="container">
                <?php
                    $current_week = '';
                    $first = 1;
                    if(is_array($prp) && count($prp) > 0){                    
                        echo "<div class='table-responsive'>
                                    <table id='indexContent' class='table center-aligned-table table-striped'>
                                    <thead>
                                        <tr class='text-primary text-center'>
                                            <th>Nama Alat</th>
                                            <th>Code</th>
                                            <th>Kondisi</th>
                                            <th>Catatan</th>
                                            <th colspan='4'>Perintah</th>
                                        </tr>
                                    </thead>
                                    <tbody>";
                        foreach($prp as $d){
                            if($current_week != $d['group_week']){
                                foreach ($datapercent as $value) {
                                    if ($value['group_week'] == $d['group_week']) {
                                        $retVal = $value[0];
                                    }                                     
                                }
                                $monthly = $d['group_week'];
                                echo "<tr class='bg-info text-white'>
                                            <td colspan='7' class='font-weight-bold'>MRP Periode ".($d['group_week'] == 0 ? 'Bulanan' : 'Minggu '.$monthly)." &nbsp;(".$retVal."%)</td>
                                            <td colspan='1' class='text-center'><a href=\"view_detail_{$d['store_id']}_{$d['group_year']}_{$d['group_month']}_{$d['group_week']}.html\" class='btn btn-small btn-secondary'>View</a></td>
                                        </tr>
                                ";
                                $first = 0;
                                $current_week = $d['group_week'];
                            }
                                echo "
                                    <tr class=''>
                                        <td align=\"center\">{$d['area']}</td>
                                        <td align=\"center\">{$d['equipment_code']}</td>
                                        <td align=\"center\">
                                        ".($d['group_status'] == 'Normal' ? 'Normal' : '' )." 
                                        ".($d['group_status'] == 'Kurang' ? 'Kurang Baik' : '' )."
                                        ".($d['group_status'] == 'Rusak' ? 'Rusak' : '' )."

                                        </td>
                                        <td align=\"center\">{$d['group_notes']}</td>
                                        <td align=\"center\">
                                            <a href=\"view_mrp_detail_{$d['group_id']}.html\" class='btn btn-info'>Detail</a>
                                        </td>
                                        <td align=\"center\">
                                            <a href=\"report_mrp_{$d['group_id']}.html\"><button class=\"btn btn-small btn-secondary\">Unduh</button></a>
                                        </td>
                                        <td align=\"center\">
                                            <a href=\"edit_mrp_{$d['group_id']}.html\" class='btn btn-success'>Edit</a>
                                        </td>
                                        <td>
                                            <input id=\"group_store\" value=\"{$d['store_id']}\" type=\"hidden\" class=\"form-control\" maxlength=\"6\" required>
                                            <input id=\"group_year\" value=\"{$d['group_year']}\" type=\"hidden\" class=\"form-control\" maxlength=\"6\" required>
                                            <input id=\"group_month\" value=\"{$d['group_month']}\" type=\"hidden\" class=\"form-control\" maxlength=\"6\" required>
                                            <a class=\"delete_group deleteconfirmationmodal btn btn-danger\" data-id=\"{$d['group_id']}\" data-toggle=\"modal\" data-target=\"#confirmDeleteMRP\" href=\"#\">Hapus</a>
                                        </td>
                                    </tr>";
                        }
                        echo "<tfoot>
                                <tr>
                                    <td colspan='8'>
                                        <a href='report_outlet_{$d['store_id']}_{$d['group_year']}_{$d['group_month']}.html'><button class='btn btn-small btn-success btn-block rounded'>Unduh Report Outlet</button></a>
                                    </td>
                                </tr>
                            </tfoot>";

                    }else{
                        echo '<div class="p-3 mb-2 bg-success text-white">Tidak ditemukan data bulan ini</div>';
                    }
                ?>
            </div>
        </div>                      
    </div>
</div>          
        <!-- MODAL EMAIL-->
        
        <div id="successModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title">Success</h4></div>
                        <div class="modal-body">Email has been sent to Outlet.</div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="failedModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title">Failed</h4></div>
                        <div class="modal-body">Send Email Failed.</div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        </div>
                    
        <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-warning text-white" style="position: fixed; bottom: 40px; right: 30px; z-index: 99;">Top</button>
        
        <!-- DELETE MRP -->
        <div class="modal fade" id="confirmDeleteMRP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title" id="myModalLabel">Hapus MRP</h4></div>
                        <div class="modal-body">Apa kamu yakin ingin menghapus Ticket di periode ini?</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <a id="admin_delete_group" href="<?= "delete_mrp_".$d['group_id']."_".$d['group_store']."_".$d['group_year']."_".$d['group_month'].".html"; ?>"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                              
                        </div>
                      </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="viewMRP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title" id="myModalLabel">Detail MRP</h4></div>
                        <div class="modal-body"><?= $d['area']; ?></div>
                        
                      </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $(".delete_group").click(function() {
                    var id = $(this).attr('data-id');
                    var idstore = $("#group_store").val();
                    var idyear = $("#group_year").val();
                    var idmonth = $("#group_month").val();
                    $("#admin_delete_group").attr("href", "delete_mrp_"+id+"_"+idstore+"_"+idyear+"_"+idmonth+".html"); 
                });
            });
        </script> 