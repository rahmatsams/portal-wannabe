                    <div class="container-fluid">
                        <h3 class="well" style="background: rgb(51,122, 183); color: #fff;">Buat SLNC</h3>
                        <?=(isset($error) ? '<div class="alert alert-danger">Please check your Highlighted input below</div>' : '')?>
                        <div class="col-lg-12 well">
                            <div class="row">
                                <form id="formSubmit" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-4 form-group">
                                                <label>Kode Barang</label>
                                                <input id="itemCode" name="item_code" type="text" placeholder="Hanya menerima huruf, angka" class="form-control" maxlength="20" <?=(isset($input) && isset($input['item_code']) ? "value='{$input['item_code']}'" : '')?> required>
                                            </div>
                                            <div class="col-lg-8 form-group">
                                                <label>Nama Barang</label>
                                                <input id="itemName" name="item_name" type="text" placeholder="Hanya menerima huruf, angka, koma, dan spasi.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['item_name']) ? "value='{$input['item_name']}'" : '')?> required>
                                            </div>
                                        </div>
										<div class="row">
											<div class="col-lg-4 form-group">
												<label>Nama Supplier</label>
												<input id="supplierName" name="supplier" type="text" placeholder="Hanya menerima huruf, angka, dan spasi.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['supplier']) ? "value='{$input['problem_source']}'" : '')?> required>
											</div>
                                            <div class="col-lg-4 form-group">
                                                <label>Kategori Supplier</label>
                                                <select name="main_category" id="mainCategory" class="form-control" required>
                                                    <option value="" default></option>
                                                    <?php
                                                        foreach($main_category as $category_form){
                                                            echo "\n<option value=\"{$category_form['category_id']}\" ".(isset($input['main_category']) && $input['main_category'] == $category_form['category_id'] ? 'selected' : '').">{$category_form['category_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
											<div class="col-lg-4 form-group">
                                                <label>Penyimpangan</label>
                                                <select id="typeID" name="type" class="form-control" required>
                                                    <?php
                                                        foreach($type as $type_list){
                                                            echo "\n<option value=\"{$type_list['type_id']}\">{$type_list['type_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
										<div class="row">
											<div class="col-lg-4 form-group">
                                                <label>Nomor PO/DO</label>
												<input id="poNumber" name="po_number" type="text" placeholder="Hanya menerima huruf, angka" class="form-control" maxlength="50" <?=(isset($input) && isset($input['po_number']) ? "value='{$input['po_number']}'" : '')?> required>
                                            </div>
                                            <div class="col-lg-4 form-group">
                                                <label>Tanggal Order</label>
                                                    <input id="orderDate" type="date" name="order_date" class="form-control" required></select>
                                            </div>
											<div class="col-lg-4 form-group">
                                                <label>Tanggal Kedatangan</label>
                                                <input id="receivedDate" type="date" name="received_date" class="form-control" required></select>
                                            </div>
											
										</div>
                                        <div class="row">
                                            <div class="col-lg-3 form-group">
                                                <label>Jumlah Order</label>
												<input name="order_quantity" id="orderQty" type="number" placeholder="Angka.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['order_quantity']) ? "value='{$input['order_quantity']}'" : '')?> required>
                                            </div>
                                            <div class="col-lg-3 form-group">
                                                <label>Jumlah Diterima</label>
												<input name="received_quantity" id="receiveQty" type="number" placeholder="Angka.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['received_quantity']) ? "value='{$input['received_quantity']}'" : '')?> required>
                                            </div>
                                            <div class="col-lg-3 form-group">
                                                <label>Selisih</label>
												<input id="diffQty" type="text" class="form-control" maxlength="50" disabled>
                                            </div>
                                            <div class="col-lg-3 form-group">
                                                <label>UOM</label>
												<input id="uomName" name="uom" type="text" placeholder="Hanya menerima huruf, angka" class="form-control" maxlength="50" <?=(isset($input) && isset($input['uom']) ? "value='{$input['uom']}'" : '')?> required>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <label>Lain-lain</label>
                                            <textarea name="etc_note" placeholder="Type here.." class="form-control" rows="3"></textarea>
                                        </div>
                                        
                                        <div class="form-group" id="addFile" style="margin-top:30px;">
                                            <div class="g-recaptcha <?=(isset($error) && isset($error['recaptcha']) ? 'alert alert-danger' : '')?>" data-sitekey="6LemwyETAAAAACZCN9RkUJ1D4j_bxeBvu1lgmAZP"></div>
                                            
                                        </div>
                                        <button id="submitButton" type="submit" name="submit" value="kirim" class="btn btn-md btn-primary">Submit SLNC</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    