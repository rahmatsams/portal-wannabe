		  <h3 class="page-heading mb-4">Submit MRP</h3>

          <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4">
              <div class="card-body">
                <form id="submitPRP" method="POST" enctype="multipart/form-data" action="submit_mrp.html">
                    <div class="upload-form">
                        <div class="form-row">
                            <div id="fileForm" class="col-lg-6">
                                <div class="form-group">
                                    <label>Upload Image (JPEG)</label>
                                    <div class="input-group">
                                        <input name="image_upload[]" type="file" class="form-control form-control-file" accept="image/*">
                                        <button class="btn btn-md btn-info" id="addFile">[+] Add</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
					<div class="col-lg-6">
						<input type="submit" id="submitButton" class="btn btn-md btn-primary btn-block" name="submit" value="Upload">
					</div>
                </form>
              </div>
            </div>
          </div>