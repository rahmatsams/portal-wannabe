            <div class="card-deck">
                <div class="card">
                     <div class="card-body"> 
                        <div class="card-deck">
                        </div>
                        <br>
                        <?php
                    $current_week = '';
                    $first = 1;
                    if(is_array($prp) && count($prp) > 0){                    
                        echo "<div class='table-responsive'>
                                    <table id='indexContent' class='table center-aligned-table table-striped'>
                                    <thead>
                                        <tr class='text-primary text-center'>
                                            <th>Nama Alat</th>
                                            <th>Code</th>
                                            <th>Kondisi</th>
                                            <th>Catatan</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>";
                        foreach($prp as $d){
                            if($current_week != $d['group_week']){
                                foreach ($datapercent as $value) {
                                    if ($value['group_week'] == $d['group_week']) {
                                        $retVal = $value[0];
                                    }                                     
                                }
                                $monthly = $d['group_week'];
                                #$total = $d['group_finding'];
                                echo "<tr class='bg-info text-white'>
                                            <td colspan='7' class='font-weight-bold'>MRP Periode ".($d['group_week'] == 0 ? 'Bulanan' : 'Minggu '.$monthly)."</td>
                                            
                                        </tr>
                                ";
                                $first = 0;
                                $current_week = $d['group_week'];
                            }
                                echo "
                                    <tr class=''>
                                        <td align=\"center\">{$d['area']}</td>
                                        <td align=\"center\">{$d['equipment_code']}</td>
                                        <td align=\"center\">
                                        ".($d['group_status'] == 'Normal' ? 'Normal' : '' )." 
                                        ".($d['group_status'] == 'Kurang' ? 'Kurang Baik' : '' )."
                                        ".($d['group_status'] == 'Rusak' ? 'Rusak' : '' )."

                                        </td>
                                        <td align=\"center\">{$d['group_notes']}</td>
                                        
                                    </tr>";
                        }
                            echo "
                                <tr class='table-area control-row bg-success text-white'>
                                    <td><b>Equipment Availability</b></td> 
                                    <td colspan='4'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$retVal."%</b></td>
                                </tr>
                            ";
                    
                ?>
                                </tbody>
                            </table>
                        </div>
                <?php 
                        }else{
                        echo '<div class="p-3 mb-2 bg-success text-white">Tidak ditemukan data MRP periode ini</div>';
                    }
                 ?>
                    </div>
                    
                    <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-warning text-white" style="position: fixed; bottom: 40px; right: 30px; z-index: 99; border-radius: 4px;">Top</button>  