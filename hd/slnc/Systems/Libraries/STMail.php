<?php

class STMail extends PHPMailer
{
    
    public function mailContent($data, $log_data){
        $body = "
        <html><head></head><body>
<STYLE>
body{font-family:Verdana,Helvetica,Arial,Sans-Serif;font-size:9pt;}td{font-size:9pt;}.Block{border:solid1px#A0A0A0;margin:3px;}.BlockHeader{background-color:#CDF2B3;padding:3px;font-size:10pt;}.BlockSubHeader{border-top:solid1px#A0A0A0;background-color:#FFFFDD;padding:3px;font-size:10pt;}.BlockBody{padding:3px;border-top:solid1px#A0A0A0;}.CommentBoxTable{border-spacing:0px;border-collapse:collapse;empty-cells:show;margin:10px;}.CommentBox{}.NewCommentArea{font-size:9pt;font-style:italic;color:#CC3300;text-align:center;}.UserCommentHead{font-size:8pt;padding:5px;color:#416523;border:1pxsolid#808080;background-color:#DAF5C5;vertical-align:top;}.CommentHead{font-size:8pt;padding:5px;color:#416523;border:1pxsolid#808080;background-color:#FFFFDD;vertical-align:top;}.CommentTitleArea{padding:5px;font-size:9pt;border-top:1pxsolid#808080;border-right:1pxsolid#808080;border-bottom:solid1px#C0C0C0;vertical-align:top;background-color:#ECF2ED;}.CommentSeperator{color:#CDCDCD;height:1px;}.CommentText{min-height:55px;border-right:1pxsolid#808080;border-bottom:1pxsolid#808080;padding:10px10px10px20px;}.MultiFieldEditContainer{color:#555555;}.MultiFieldEditFactsContainer{margin-left:15px;}.MultiFieldEditOldValue{}.MultiFieldEditOldValuelabel{width:50px;float:left;text-align:right;}.MultiFieldEditNewValue{}.MultiFieldEditNewValuelabel{width:50px;float:left;text-align:right;}.MultiFieldEditNewValuehr{height:1px;margin-top:3px;margin-bottom:3px;}
</STYLE>
<TABLE style=\"WIDTH: 100%\" border=\"1\">
    <TBODY>
        <TR>
            <TD style=\"VERTICAL-ALIGN: top; WIDTH: 75%\">
                <DIV class=Block>
                    <DIV class=BlockHeader>
                        <A id=TicketType href=\"".getConfig('base_url')."edit_ticket_{$data['ticket_id']}.html\">{$data['type_name']}</A> : <A id=TicketTitle href=\"".getConfig('base_url')."edit_ticket_{$data['ticket_id']}.html\">{$data['ticket_title']}</A> 
                    </DIV>
                    <DIV class=BlockBody style=\"HEIGHT: 180px\">
                    {$data['content']}
                    </DIV>
                </DIV>
            </TD>
    
            <TD style=\"VERTICAL-ALIGN: top; WIDTH: 25%\">
                <DIV class=Block>
                    <DIV class=BlockHeader>Ticket ID: <A id=TicketId href=\"".getConfig('base_url')."edit_ticket_{$data['ticket_id']}\">{$data['ticket_id']}</A> </DIV>
                    <DIV class=BlockBody style=\"WHITE-SPACE: nowrap; HEIGHT: 203px\">
                        <TABLE>
                            <TBODY>
                                <TR>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; TEXT-ALIGN: right\">Status: </TD>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap\">
                                        <SPAN id=CurrentStatus>{$data['status_name']}</SPAN> 
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; TEXT-ALIGN: right\">Category: </TD>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap\">
                                        <SPAN id=Category>{$data['category_name']}</SPAN> 
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; TEXT-ALIGN: right\">Owner: </TD>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap\">
                                        <SPAN id=Owner>{$data['user_name']}</SPAN> 
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; TEXT-ALIGN: right\">Assigned Engineer: </TD>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap\">
                                        <A id=AssignedTo>{$data['staff_name']}</A> 
                                    </TD>
                                </TR>
                                <TR>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; TEXT-ALIGN: right\">Affects Customer(s): </TD>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap\">
                                        <SPAN id=AffectsCustomer>Yes</SPAN> 
                                    </TD>
                                </TR>
                                
                                <TR>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap; TEXT-ALIGN: right\">Created by: </TD>
                                    <TD style=\"VERTICAL-ALIGN: top; WHITE-SPACE: nowrap\">
                                        <SPAN id=CreatedBy>{$data['creator_name']}</SPAN> on: <SPAN id=CreatedDate>{$data['submit_date']}</SPAN> 
                                    </TD>
                                </TR>
                            </TBODY>
                        </TABLE>
                    </DIV>
                </DIV>
            </TD>
        </TR>";
        $num = 1;
        foreach($log_data as $log){
            $content = nl2br(htmlspecialchars_decode($log['log_content']));
            if($num == 1){
                $body .= "
        <TR>
            <TD colSpan=2>
                <DIV class=Block>
                    <DIV class=BlockHeader>Activity Log: </DIV>
                    <DIV class=BlockBody>
                    <table class=CommentBoxTable>

                         <tbody class='CommentBox'>
                            <tr>
                                <td rowspan='2' runat='server' class='".($log['user'] == $data['user'] ? 'CommentHead' : 'UserCommentHead')."'>
                                    <div class='NewCommentArea'>New</div>
                                    {$log['log_type']} - {$log['log_time']}<br />
                                    <br />
                                    {$log['display_name']}
                                </td>
                                
                                <td class='CommentTitleArea'>
                                {$log['log_title']}
                                </td>
                            </tr>
                            <tr>
                                <td class='CommentText'>
                                
                                {$content}
                                </td>
                            </tr>
                         </tbody>";
            } else {
                $body .= "
                <tbody><tr><td colspan='2' style='height:10px;'></td></tr></tbody>
                     <tbody class='CommentBox'>
                        <tr>
                            <td rowspan='2' runat='server' class='".($log['user'] == $data['user'] ? 'CommentHead' : 'UserCommentHead')."'>
                                
                                {$log['log_type']} - {$log['log_time']}<br />
                                <br />
                                {$log['display_name']}
                            </td>
                            <td class='CommentTitleArea'>
                                {$log['log_time']}<br />
                                {$log['log_title']}
                            </td>
                        </tr>
                        <tr>
                            <td class='CommentText'>
                                {$content}
                            </td>
                        </tr>
                     </tbody>";
            }
            $num++;
        }
        
        
    $body .= "
                        </table>
                     </DIV>
                </DIV>
            </td>
        </tr>        
    </TBODY>
</TABLE>
</body></html>
        ";
        
        
        return $body;
    }
    
}
