<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    
    <title>SLNC</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?="$_resource_css/bootstrap.min.css"?>" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?="$_resource_css/ie10-viewport-bug-workaround.css"?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?="$_resource_css/dashboard.css"?>" rel="stylesheet">
    <link href="<?="$_template_css/style.css"?>" rel="stylesheet">
    <?=(isset($_option['bdatepick_css']) && $_option['bdatepick_css'] == 1) ? "<link href=\"{$_resource_css}/bootstrap-datepicker3.min.css\" rel=\"stylesheet\">" : ''?>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?="$resource_css/ie8-responsive-file-warning.js"?>"></script><![endif]-->
    <script src="<?="$_resource_css/ie-emulation-modes-warning.js"?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="Resources/images/Ico/favicon.png">
</head>

<body>
    <div class="container-fluid">
        <nav class="navbar navbar-inverse navbar-fixed-top">
          
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              
              <a class="navbar-brand" href="http://helpdesk.sushitei.co.id/">Sushi Tei - SLNC</a>
            </div>
            <?php if($session['user_name'] != 'Guest' && $session['group'] != 'Guest'):?>
            
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="my_account.html"><?=$session['user_name']?></a></li>
                <li><a href="create_ticket.html">Submit Ticket</a></li>
                <li><a href="<?=getConfig('base_domain').'user/logout'?>">Logout</a></li>
              </ul>
            </div>
            <?php endif; ?>
        </nav>
        <div class="col-sm-12 col-xs-12" style="background-color: #f5f5f5;">
            <div class="row row-down">
            <?php
                $menu_list[0] = array(
                    'name' => 'Ticket Admin',
                    'url' => getConfig('base_url').'ticket_admin.html', 
                    'icon' => '<i class="fa fa-cog"></i>'
                );
                $menu_list[1] = array(
                    'name' => 'SLNC Center',
                    'url' => getConfig('base_url').'index.html',
                    'icon' => '<i class="fa fa-list-alt"></i>'
                );
                $menu_list[2] = array(
                    'name' => 'Submit SLNC',
                    'url' => getConfig('base_url').'create_ticket.html',
                    'icon' => '<i class="fa fa-pencil-square-o"></i>'
                );
                $menu_list[3] = array(
                    'name' => 'Logout User',
                    'url' => getConfig('base_domain').'user/logout',
                    'icon' => '<i class="fa fa-sign-out"></i>'
                );
                if(!$_option['admin']){
                    unset($menu_list[0]);
                }
            
                if(!empty($menu_list)):?>
                    
                <div class="col-sm-2 col-xs-12 sidebar">
                    <ul class="nav nav-sidebar">
                        <?php foreach($menu_list as $l): ?>
                            <?php if($l['url'] == "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"): ?>
                            
                                <li class="active"><a href="<?=$l['url']?>"><?=$l['icon']?> <?=$l['name']?><span class="sr-only"></span></a></li>
                                
                            <?php else: ?>
                            
                                <li style="padding-left: 1.2em;"><a href="<?=$l['url']?>"><?=$l['icon']?>  <?=$l['name']?></a></li>
                            
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-sm-10 col-xs-12 main" style="padding-left: 1.2em; padding-right: 1.2em; background-color: #fff;">
                    <?=$this->view($_action,$_passed_var)?>
                </div>
                <?php else: ?>
                    
                <div class="main">
                    <?=$this->view($_action,$_passed_var)?>
                </div>
                    
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>    
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="Resources/assets/jquery/jquery-1.12.3.min.js"></script>
    <?=(isset($_option['tinymce']) && $_option['tinymce'] == 1) ? "<script src=\"http://cdn.tinymce.com/4/tinymce.min.js\"></script>\n" : ''?>
    <?=(isset($_option['personal_js']) && $_option['personal_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/script.js\"></script>\n" : ''?>
    <?=(isset($_option['bdatepick_js']) && $_option['bdatepick_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/bootstrap-datepicker.min.js\"></script>\n" : ''?>
    <?=(isset($_option['scriptload'])  ? $_option['scriptload'] : '')?>
    
    <script>window.jQuery || document.write('<script src="<?="{$_resource_js}/vendor/jquery.min.js"?>"><\/script>')</script>
    <script src="Resources/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"?>"></script>
</body>
</html>
