function editCategory(classid){
    $.ajax({
        type: "POST",
        url: "ajax_editcategory.html",
        data: "cid="+classid,
        success: function(html){
            if(html=="false")    {
                $("#modal-content").html("No Data Available");
            } else {
                var data = $.parseJSON(html);
                
                $(data).each(function() {
                    $("#catid").attr("value", this.category_id);
                    $("#subcategoryid").attr("value", this.category_id);
                    $("#catname").attr("value", this.category_name);
                    $("#catdesc").html(this.description);
                });
            }
        },
    });
    return false;
}



function submitEdit(){
    username=$("#inputName").val();
    password=$("#inputPassword").val();
    $.ajax({
        type: "POST",
        url: "ajax_login.html",
        data: "ticket_email="+username+"&ticket_password="+password,
        success: function(html){
            if(html=="true")    {
                window.location.assign("index.html");
            } else {
                $("#userName").addClass("has-error");
                $("#userName .control-label").removeClass("sr-only");
            }
        },
    });
    return false;
}


function getSubCategory(classid){
    $.ajax({
        type: "POST",
        url: "ajax_subcategory.html",
        data: "category="+classid,
        success: function(html){
            if(html=="false")    {
                $("#tablesub").html("Tidak ada sub categori");
                $("#tablesub").addClass("sr-only");
            } else {
                var data = $.parseJSON(html);
                $("#tablesub").removeClass("sr-only");
                $("#tablesub").html("");
                $(data).each(function() {
                    var row_data = "<div class=\"row\">"+
                    "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-name form-control\" name=\"category_name\" value=\""+this.category_name+"\" data-id=\""+this.category_id+"\" disabled/></div>"+
                    "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-desc form-control\" name=\"description\" value=\""+this.description+"\" data-id=\""+this.category_id+"\" disabled/></div>"+
                    "<div class=\"col-sm-3 form-group\"><button type='button' class='edit-sub-category btn btn-primary btn-sm' data-id=\""+this.category_id+"\">Edit</button> <button type='button' class='delete-sub-category btn btn-danger btn-sm' data-id=\""+this.category_id+"\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\">Delete</button> <button type='button' class='update-sub-category btn btn-primary btn-sm hidden' data-id=\""+this.category_id+"\">Edit</button> <button type='button' class='cancel-sub-category btn btn-sm hidden' data-id=\""+this.category_id+"\">Batal</button></div>"+
                    "</div>";
                    $("#tablesub").append(row_data);
                });
            }
        },
    });
    return false;
}

function editFormSubCategory(dataid){
    if($(".update-sub-category[data-id='"+dataid+"']").hasClass("hidden")){
        $(".sub-category-name[data-id='"+dataid+"']").attr("disabled", false);
        $(".sub-category-desc[data-id='"+dataid+"']").attr("disabled", false);
        $(".update-sub-category[data-id='"+dataid+"']").removeClass("hidden");
        $(".cancel-sub-category[data-id='"+dataid+"']").removeClass("hidden");
        $(".edit-sub-category[data-id='"+dataid+"']").addClass("hidden");
        $(".delete-sub-category[data-id='"+dataid+"']").addClass("hidden");
    }
}

function cancelFormSubCategory(dataid){
    if($(".edit-sub-category[data-id='"+dataid+"']").hasClass("hidden")){
        $(".sub-category-name[data-id='"+dataid+"']").attr("disabled", true);
        $(".sub-category-desc[data-id='"+dataid+"']").attr("disabled", true);
        $(".update-sub-category[data-id='"+dataid+"']").addClass("hidden");
        $(".cancel-sub-category[data-id='"+dataid+"']").addClass("hidden");
        $(".edit-sub-category[data-id='"+dataid+"']").removeClass("hidden");
        $(".delete-sub-category[data-id='"+dataid+"']").removeClass("hidden");
    }
}

function submitSubCategory(me){
    cid = $("#subcategoryid").val();
    name = $("#categoryname").val();
    desc = $("#categorydesc").val();
    if ( me.data('requestRunning') ) {
        return;
    }

    me.data('requestRunning', true);

    $.ajax({
        type: "POST",
        url: "ajax_insert_category.html",
        data: "category_parent_id="+cid+"&category_name="+name+"&description="+desc+"&category=1",
        success: function(html){
            if(html!="failed"){
                $("#categoryname").val('');
                $("#categorydesc").val('');
                $("#categorytime").val('');
                var row_data = "<div class=\"row\">"+
                "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-name form-control\" name=\"category_name\" value=\""+name+"\" data-id=\""+html+"\" disabled/></div>"+
                "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-desc form-control\" name=\"description\" value=\""+desc+"\" data-id=\""+html+"\" disabled/></div>"+
                "<div class=\"col-sm-3 form-group\"><button type='button' class='edit-sub-category btn btn-primary btn-sm' data-id=\""+html+"\">Edit</button> <button type='button' class='delete-sub-category btn btn-danger btn-sm' data-id=\""+html+"\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\">Delete</button> <button type='button' class='update-sub-category btn btn-primary btn-sm sr-only' data-id=\""+html+"\">Edit</button> <button type='button' class='cancel-sub-category btn btn-sm sr-only' data-id=\""+html+"\">Batal</button></div>"+
                "</div>";
                $("#tablesub").append(row_data);
              
            } else {
                alert("Terjadi kesalahan");
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}

function submitCategory(me){
    name = $("form#addcategory input[name=category_name]").val();
    desc = $("form#addcategory textarea[name=category_description]").val();
    if ( me.data('requestRunning') ) {
        return;
    }

    me.data('requestRunning', true);

    $.ajax({
        type: "POST",
        url: "ajax_insert_category.html",
        data: "category_name="+name+"&description="+desc+"&category=1",
        success: function(html){
            if(html!="failed"){
		var rowCount = parseInt($('#listTable tr').length)+1;
                $("form#addcategory input[name=category_name]").val('');
                $("form#addcategory textarea[name=category_description]").val('');
                $("form#addcategory input[name=category_treshold]").val('');
                var row_data = "<tr>"+
                "<td>"+ rowCount +"</td>"+
                "<td>"+ name +"</td>"+
                "<td>"+ desc +"</td>"+
                "<td><button type='button' id='"+ html +"' class='edit-category btn btn-primary btn-sm' data-toggle='modal' data-target='#myModal'>Edit</button></td>"+
                "<td><a class='delete_category' id='"+ html +"' href=\"#\">Delete</a></td>"+
                "</tr>";
                $("#listTable").append(row_data);
                alert('New Category added');
                
            } else {
                alert('Category failed to add');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}


function submitEditCategory(me){
    id = $("form#editcategory input[name=category_id]").val();
    name = $("form#editcategory input[name=category_name]").val();
    desc = $("form#editcategory textarea[name=description]").val();
    if ( me.data('requestRunning') ) {
        return;
    }

    me.data('requestRunning', true);

    $.ajax({
        type: "POST",
        url: "ajax_edit_category.html",
        data: "category_id="+id+"&category_name="+name+"&description="+desc+"&category=1",
        success: function(html){
            if(html!="failed"){
                alert('Edited Successfully');
                location.reload();
                
            } else {
                alert('Failed to edit');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}

function toogleEditor(){
    $("#ticket-content").tinymce({
        // Location of TinyMCE script
        script_url : "./Resources/js/tiny_mce/tiny_mce.js",
        entity_encoding : "raw",
    });
}

function showComment(text_title, text_label, button_name){
    $("#tickettablecomment").toggleClass("hidden");
    $("#labelcomment").html(text_label);
    $("#tickettitle").html(text_title);
    $("#submitstatus").html(button_name);
    $("#submitstatus").val(button_name);
    $("#activitybutton").toggleClass("hidden");
}

function showActivity(){
    $("#tickettablecomment").toggleClass("hidden");
    $("#activitybutton").toggleClass("hidden");
}

function toggleEdit(){
    $("#tiketttabledetail").toggleClass("hidden");
    $("#tickettableedit").toggleClass("hidden");
    $("#tickettabledetail").toggleClass("hidden");
    $("#tickettabledesc").toggleClass("hidden");
    $("#tickettableraw").toggleClass("hidden");
}

function toggleAssign(){
    $("#assignform").toggleClass("hidden");
}

function toggleResolve(){
    $("#coreProblem").toggleClass("hidden");
}
function toggleSparePart(){
    $("#spareParts").toggleClass("hidden");
}

function editTicket(){
    var reasons = $('#updatereason').val();
    var tid = $("form#detailedit input[name=ticket_id]").val();
    var data_list = "submit_type="+$('#submitstatus').val()+"&comment="+reasons+"&t_id="+tid;
    if(!$('#assignform').hasClass("hidden")){
        pic=$("#userlist").val();
        pic_name=$("#userlist").find(':selected').data('id');
        data_list += "&assigned_staff="+pic+"&a_name="+pic_name;
    }
    if(!$('#coreProblem').hasClass("hidden")){
        cause=$("#coreproblem").val();
        data_list += "&problem_source="+cause;
    }
    if(!$('#sparePartName').hasClass("hidden")){
        cause=$("#sparePartName").val();
        data_list += "&spare_part="+cause;
    }
    if($('#submitstatus').val() == 'Edit'){
        var desc = $('#ticketcontent').val();
        var type = $("form#detailedit select[name=type]").val();
        var main = $("form#detailedit select[name=main_category]").val();
        var sub = $("form#detailedit select[name=sub_category]").val();
        var impact = $("form#detailedit input[name=customer_impact]").val();
        data_list += "&description="+desc+"&ticket_type="+type+"&main_category="+main+"&sub_category="+sub+"&impact="+impact;
    }
    
    
    $.ajax({
        type: "POST",
        url: "ajax_edit_ticket.html",
        data: data_list,
        dataType: "json", 
        success: function(data){
            if(data.success == 1){
                alert(data.message);
                location.reload();
            }else{
                alert(data.success);
            }
        },
    });
    return false;
}

function getUser(classid){
    $.ajax({
        type: "POST",
        url: "ajax_user_detail.html",
        data: "uid="+classid,
        success: function(html){
            if(html=="false")    {
                $("#modal-content").html("No Data Available");
            } else {
                var data = $.parseJSON(html);
                
                $(data).each(function() {
                    $("#userid").attr('value', this.user_id);
                    $("#username").attr('value', this.user_name);
                    $("#displayname").attr('value', this.display_name);
                    $("#email").attr('value', this.email);
                    $("#groupid").val(this.group_id);
                    $("#userstatus").val(this.user_status);
                });
            }
        },
    });
    return false;
}

function loginForm(me){
    username=$("#inputName").val();
    password=$("#inputPassword").val();
    last_url=$("#lastUrl").val();
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_login.html",
        data: "ticket_email="+username+"&ticket_password="+password,
        success: function(html){
            if(html=="true")    {
                if(last_url != ''){
                    window.location.assign(last_url);
                }else{
                    window.location.assign("index.html");
                }
                
            } else {
                $("#userName").addClass("has-error");
                $("#userName .control-label").removeClass("sr-only");
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}

$(document).on("click", ".page-navigate", function(event){
    event.preventDefault();
    data_id = $(this).attr("data-id");
    $("#ticketPage").val(data_id);
    var me = $(this);
    searchForm(me);
    
});
$("#advanceSearch").submit(function(event){
    // cancels the form submission
    $("#ticketPage").val("1");
    event.preventDefault();
    var me = $(this);
    searchForm(me);
});

$("#sortResult").change(function(event){
    // cancels the form submission
    $("#ticketPage").val("1");
    event.preventDefault();
    var me = $(this);
    searchForm(me);
});

function searchForm(me){
    title=$("#ticketTitle").val();
    status=$("#ticketStatus").val();
    main=$("#mainCategory").val();
    sub=$("#subCategory").val();
    date_start=$("#ticketDateFirst").val();
    date_end=$("#ticketDateLast").val();
    store=$("#storeInput").val();
    page=$("#ticketPage").val();
    sort=$("#sortResult").val();
    if ( me.data("requestRunning") ) {
        $("#advanceSearch").attr("disabled", "disabled");
        return;
    }

    me.data("requestRunning", true);
    $.ajax({
        type: "POST",
        url: "ajax_search.html",
        data: "sort="+sort+"&page="+page+"&ticket_title="+title+"&status="+status+"&main_category="+main+"&sub_category="+sub+"&date_start="+date_start+"&date_end="+date_end+(store == null ? '' : '&store='+store),
        success: function(html){
            $("#listTable").html(html);
        },
        complete: function() {
            $.ajax({
                type: "POST",
                url: "ajax_search_page.html",
                success: function(html){
                    $("#pagingBottom").html(html);
                },
                complete: function() {
                    me.data("requestRunning", false);
                    $("#advanceSearch").removeAttr("disabled");
                },
            });
        },
    });
}
$(document).ready(function(){
    $("#onbehalf").change(function(event){
        event.preventDefault();
        if($("#onbehalf").prop("checked") == true){
            changeBehalf();
        } else {
            $("#userlist").attr("disabled", true);
        }
        
    });
    
    $("#formSubmit").submit(function(event){
		event.preventDefault();
        var me = $(this);
		var form = $("#formSubmit");
        
        if ( me.data('requestRunning') ) {
            return;
        }
        me.data('requestRunning', true);
        $("#submitButton").prop('disabled', true);
        $.ajax({
            type: "POST",
            url: "ajax_submit.html",
            dataType: "json",
            data: form.serialize(),
            success: function(resp){
                if(resp.success == 1)    {
                    alert('You\'ve successfully submit SLNC.');
                    location.replace(resp.url);
                } else if(resp.success == 0)    {
                    $("#formSubmit").find("input").parent().addClass("has-success");
                    $("#formSubmit").find("select").parent().addClass("has-success");
                    $("#formSubmit").find("textarea").parent().addClass("has-success");
                    if(typeof(resp.message.ic_format) !== 'undefined' || typeof(resp.message.ic_req) !== 'undefined'){
                        $("#itemCode").parent().removeClass('has-success');
                        $("#itemCode").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.in_format) !== 'undefined' || typeof(resp.message.in_req) !== 'undefined'){
                        $("#itemName").parent().removeClass('has-success');
                        $("#itemName").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.ns_format) !== 'undefined' || typeof(resp.message.ns_req) !== 'undefined'){
                        $("#supplierName").parent().removeClass('has-success');
                        $("#supplierName").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.mc_format) !== 'undefined' || typeof(resp.message.mc_req) !== 'undefined'){
                        $("#mainCategory").parent().removeClass('has-success');
                        $("#mainCategory").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.t_format) !== 'undefined' || typeof(resp.message.t_req) !== 'undefined'){
                        $("#typeID").parent().removeClass('has-success');
                        $("#typeID").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.pn_format) !== 'undefined' || typeof(resp.message.pn_req) !== 'undefined'){
                        $("#poNumber").parent().removeClass('has-success');
                        $("#poNumber").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.od_format) !== 'undefined'){
                        $("#orderDate").parent().removeClass('has-success');
                        $("#orderDate").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.rd_format) !== 'undefined'){
                        $("#receivedDate").parent().removeClass('has-success');
                        $("#receivedDate").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.oq_format) !== 'undefined' || typeof(resp.message.oq_req) !== 'undefined'){
                        $("#orderQty").parent().removeClass('has-success');
                        $("#orderQty").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.rq_format) !== 'undefined' || typeof(resp.message.rq_req) !== 'undefined'){
                        $("#receivedQty").parent().removeClass('has-success');
                        $("#receivedQty").parent().addClass('has-error');
                    }
                    if(typeof(resp.message.uom_format) !== 'undefined' || typeof(resp.message.uom_req) !== 'undefined'){
                        $("#uomName").parent().removeClass('has-success');
                        $("#uomName").parent().addClass('has-error');
                    }
                    alert('Failed to submit, check your input');
                }else{
                    alert('Unknown Error');
                }
            },
            complete: function() {
                $("#submitButton").prop('disabled', false);
                grecaptcha.reset();
                me.data('requestRunning', false);
            },
        });
        return;
		
	});
    $("#addFileButton").click(function(event){
        event.preventDefault();
        num_files = $("#addFile input:file").length;
        if(num_files < 5){
            $("#addFile").append("<input name=\"image_upload[]\" type=\"file\" class=\"form-control\" accept=\"image/jpeg\">");
        }
    });
    $("#orderQty").change(function(event){
        var a = $("#orderQty").val();
        var b = $("#receiveQty").val();
        var c;
        if(a > b){
            c = a - b;
        }else if(b > a){
            c = b - a;
        }
        $("#diffQty").val(c);
        
    });
    $("#receiveQty").change(function(event){
        var a = $("#orderQty").val();
        var b = $("#receiveQty").val();
        var c;
        if(a > b){
            c = a - b;
        }else if(b > a){
            c = b - a;
        }
        $("#diffQty").val(c);
        
    });
});
