<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Core of SISTER
* Important files that must be included in library
*/
$_required = array('Functions','Model','Controller','Load');

/*
* Load all necessary files
*/
for($i=0; $i < count($_required); $i++) {
    if (file_exists(__SYSTEM_DIR . __LIBS_DIR . $_required[$i].'.php')) {
        require_once(__SYSTEM_DIR . __LIBS_DIR . $_required[$i].'.php');
    } else {
        exit ("Library file {$_required[$i]}.php does not exist<br/>");
    }
}
/* 
*   [start] Check url command error
*   Check controller input error
*/
if(!isset($_GET[__CONTROLLER_COMMAND])
    || !preg_match("/\w+/i",__CONTROLLER_COMMAND) 
    || !file_exists(__SYSTEM_DIR . __APPLICATIONS_DIR . 'Controllers' . __DS__ .
    ucwords($_GET[__CONTROLLER_COMMAND]).'.php')
) {
    $_GET[__CONTROLLER_COMMAND] = getConfig('default_controller');
}
/* Check action input error */

if (!isset($_GET[__ACTION_COMMAND]) || !preg_match("/\w+/i",__ACTION_COMMAND)) {
    $_GET[__ACTION_COMMAND] = getConfig('default_action');
}
/* 
*   [end] Check url command error
*   Session Start
*/

/* SISTER Class Start */
class SISTER 
{
	protected static $_test;
	protected $_controller;
	protected $_view;
	
	public function __construct($controller, $view){
		$controllers = ucwords($controller);
        if(file_exists(__SYSTEM_DIR . __APPLICATIONS_DIR . 'Controllers' . __DS__ . $controllers . '.php')){
            include_once ( __SYSTEM_DIR . __APPLICATIONS_DIR . 'Controllers' . __DS__ . $controllers . '.php');
            $sister = new $controller ($controllers, $view);
            $this->_controller = $controllers;
            $this->_view = $view;
        } else {
            exit('Error on controller');
        }
		
	}	
	
	
}
$sister = new SISTER($_GET[__CONTROLLER_COMMAND], $_GET[__ACTION_COMMAND]);
// End Core
