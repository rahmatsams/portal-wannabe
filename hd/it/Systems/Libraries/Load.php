<?php
/*
*   Load Class for load Model, View, Template, Library
*/
class Load
{
    protected $_input = array();
    protected $_controller = null;
    protected $_models = array();
    protected $_libs = array();
    
    // Construct class using name of loaded controller
    public function __construct($controller)
    {
        $this->_controller = $controller;
    }

    public function setHtmlHeader($option)
    {
        if(isset($option['js'])){
            $this->_header .= "<script type=\"text/javascript\">{$option['js']}</script>\n";
        }elseif(isset($option['jsfile'])){
            $this->_header .= "<script type=\"text/javascript\" src=\"{$option['jsfile']}\"></script>\n";
        }
    }
    
    public function template($_action, $_passed_var = null, $_option = null)
    {
        $_device = $this->lib('Mobile_Detect');
        $_template_path = getConfig('templates_dir') .  '/' . getConfig('default_template');
        if (file_exists($_template_path . __DS__ . 'template.php')) {
			$_template_css = $_template_path. '/css';
			$_template_js = $_template_path. '/js';
			$_template_image = $_template_path. '/images';
            $_resource_css = getConfig('resource_dir') . '/' . getConfig('css_folder');
            $_resource_image = getConfig('resource_dir') . '/' . getConfig('image_folder');
            $_resource_js = getConfig('resource_dir') . '/' . getConfig('js_folder');
            $_resource_media = getConfig('resource_dir') . '/' . getConfig('media_folder');
            if(is_array($_passed_var) && count($_passed_var) > 0){
                extract($_passed_var, EXTR_PREFIX_SAME, 'pre');
            }
            include_once($_template_path . __DS__ . 'template.php');
        } else {
            exit ("Template Not Found");
        }
    }
    
    // Load view page
    public function view($view, $passed_var=array(), $controller=null)
    {
        if(file_exists(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __APPLICATIONS_DIR . 'Views' . __DS__ .
            ($controller != '' ? $controller . __DS__  : ''). $view .'.php')) {
            if(is_array($passed_var) && count($passed_var) > 0){
                extract($passed_var, EXTR_PREFIX_SAME, 'pre');
            }
            include_once (__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __APPLICATIONS_DIR . 'Views' . __DS__ .
                        ($controller != '' ? $controller . __DS__  : ''). $view .'.php');
        } else {
            exit ('Cannot find View File' . $view);
        }
    }
    
    // Functions to  Load Model
    public function model($name)
    {
        if(file_exists(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __APPLICATIONS_DIR . 'Models' .
            __DS__ . 'Model' .$name.'.php')
        ) {
            require_once(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __APPLICATIONS_DIR . 'Models' .
            __DS__ . 'Model' . $name.'.php');
            if(isset($this->_models[$name])){
                return $this->_models[$name];
            }
            $name = ucwords($name);
            $this->_models[$name] = new $name();
            return $this->_models[$name];
        }else{
            exit ("Model {$name} doesn't found");
        }
    }
    
    // Functions to get all Loaded Model
    public function getLoadedModels()
    {
        $models = array();
        foreach($this->_models as $key => $value){
            array_push($models,$key);
        }
        return $models;
    }
    
    // Functions to Load Library
    public function lib($name, $arguments=array())
    {
        $name = ucwords($name);
        if(file_exists(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __LIBS_DIR . $name .'.php')){   
            require_once(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __LIBS_DIR . $name .'.php');

            if(isset($this->_libs[$name])) {
                return $this->_libs[$name];
            }
            switch(count($arguments)){
                case 1:
                    $this->_libs[$name] = new $name($arguments[0]);
                    break;
                case 2:
                    $this->_libs[$name] = new $name($arguments[0],$arguments[1]);
                    break;
                default:
                    $this->_libs[$name] = new $name();
            }
            require_once(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __LIBS_DIR . $name .'.php');
            return $this->_libs[$name];
        }else{
            exit ("Library {$name} doesn't found");
        }
    }
    
    // Functions to get all Loaded Library
    public function getLoadedLibs()
    {
        $libs = array();
        foreach($this->_libs as $key => $value){
            array_push($libs,$key);
        }
        return $libs;
    }
}
// End Class Load