<?php
// Class To Manage File, Directory, Upload
//
class FileManager
{
    private $_fileInfo;
    private $_fileLocation;
    private $_error;
    private $_direct;
      
    function __construct($dir)
    {
        $this->_direct = $dir;
        if(!is_dir($this->_direct)){
            die('Supplied directory is not valid: '.$this->_direct);	
        }
    }
    function getFileInfo()
    {
        return $this->_fileInfo;
    }
    
    function checkExist($filename){
        $exist = file_exists($this->_direct . $filename);
        return $exist;
    }
    
    function upload($file, $file_name)
    {
        $this->_fileInfo = $file;
        $this->_fileLocation = $this->_direct . ($file_name != 0 ? $file_name : $this->_fileInfo['name']);
        if(!file_exists($this->_fileLocation)){
            if(move_uploaded_file($this->_fileInfo['tmp_name'], $this->_fileLocation)){
                return 1;
            } else {
                return 'File could not be uploaded';
                $this->_error = "Error: File could not be uploaded.\n";
                $this->_error .= 'Here is some more debugging info:';
                $this->_error .= print_r($_FILES);	
            }
        } else {
            return 'File by this name already exists';	
        }
    }
    function overwrite($file, $file_name = 0)
    {
        $this->_fileInfo = $file;
        $this->_fileLocation = $this->_direct . ($file_name != 0 ? $file_name : $this->_fileInfo['name']);
        if(file_exists($this->_fileLocation)){
            $this->delete($this->_fileInfo['name']);
        }
        return $this->upload($this->_fileInfo);
    }
    function location()
    {
        return $this->_fileLocation;	
    }
    function fileName()
    {
        return $this->_fileInfo['name'];
    }
    function delete($fileName)
    {
        $this->_fileLocation = $this->_direct . $fileName;
        if(is_file($this->_fileLocation)) {
            unlink($this->_fileLocation);
            return 'Your file was successfully deleted';
        } else {
            return 'No such file exists: '.$this->_fileLocation;	
        }
    }
    function reportError()
    {
        return $this->_error;	
    }
}
