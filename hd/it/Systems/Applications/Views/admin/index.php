	<div class="container-fluid">
        <h2 class="sub-header">Admin Page</h2>
        <ol class="breadcrumb">
            <li class="active"><a href="#">Admin</a></li>
        </ol>
		<div class="image-menu">
			<a href="category_admin.html">Manage Category</a>
		</div>
        <div class="image-menu" hidden>
			<a href="user_admin.html">Manage User</a>
		</div>
        <?php
            if($_mySession['group'] == 'Administrator' || $_mySession['group'] == 'Super Admin'){
        ?>
       
        <div class="image-menu" hidden>
			<a href="admin_store.html">Manage Store</a>
		</div>
        <?php
            }
        ?>
	</div>