					<div class="content-right">
						<h3 class="well">Edit Store</h3>
						<div class="row">
                            
                            <form id="editStore" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['store_name']) ? "<div class=\"alert alert-danger\"><strong>Store Name </strong>{$error['store_name']}</div>" : '<label>Store Name</label>')?>
                                            <input name="store_name" type="text" placeholder="Type store name.." value="<?=$store['store_name']?>" class="form-control" maxlength="25" required>
                                        </div>
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['store_location']) ? "<div class=\"alert alert-danger\"><strong>Store Location </strong>{$error['store_location']}</div>" : '<label>Store Location</label>')?>
                                            <select name="store_location" class="form-control" required>
                                                <?php
                                                    if(is_array($location_list) && count($location_list) > 0){
                                                        foreach($location_list as $location ){
                                                            echo "
                                                <option value=\"{$location['location_id']}\" ".($store['location_id'] == $location['location_id'] ? 'selected' : '').">{$location['location_name']}</option>
                                                            ";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['store_status']) ? "<div class=\"alert alert-danger\"><strong>Status </strong>{$error['store_status']}</div>" : '<label>Status</label>')?>
                                            <select name="store_status" class="form-control">
                                                <option value="0" <?=($store['store_status'] == 0 ? 'selected' : '')?>>Disabled</option>
                                                <option value="1" <?=($store['store_status'] == 1 ? 'selected' : '')?>>Enabled</option>
                                            </select>
                                        </div>
                                        <button type="submit" name="submit" value="edit_store" class="btn btn-primary">Edit</button>
                                    </div>
                            </form>
                         </div>
					</div>
					