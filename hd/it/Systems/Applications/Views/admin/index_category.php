	<div class="container-fluid">
        <h2 class="sub-header">Manage Category</h2>
        <ol class="breadcrumb">
            <li><a href="ticket_admin.html">Admin Page</a></li>
            <li class="active"><a href="#">Manage Category</a></li>
        </ol>
		<div class="table-responsive">
            <h3>Main Category</h3>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($category) && !empty($category)){
                            $num = 1;
                            foreach($category as $result){
                                        
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['category_name']}</td>
                        <td>{$result['description']}</td>
                        <td><a href='manage_category_{$result['category_id']}.html'>Edit</a></td>
                        <td><a class=\"delete_category deleteconfirmationmodal\" data-id=\"{$result['category_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\">Delete</a></td>
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="3">Data not found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2">New Category</button>
            
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Category</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12 well">
                                <div class="row">
                                    <form id="addcategory">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Category (*)</label>
                                                <input name="category_name" type="text" value="" placeholder="type here.." class="form-control" maxlength="25" autocomplete="off" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Description (*)</label>
                                                <textarea name="category_description" cols="20" rows="4" placeholder="you cannot leave this blank.." class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" name="submit" value="kirim" class="btn btn-sm btn-primary">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                 </div>
                                 
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Hapus Kategori</h4>
                        </div>
                        <div class="modal-body">
                            <p class="content-padding bg-danger">Apa kamu yakin ingin menghapus kategori ini?</p>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>