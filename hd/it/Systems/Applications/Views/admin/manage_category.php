	<div class="container-fluid">
        <h2 class="sub-header">Manage Category</h2>
        <ol class="breadcrumb">
            <li><a href="ticket_admin.html">Admin Page</a></li>
            <li><a href="category_admin.html">Manage Category</a></li>
            <li class="active"><?=$category['category_name']?></li>
        </ol>
		<div class="table-responsive">
            <div class="col-md-12 well">
                <div class="row">
                    <form id="editcategory" method="POST" action="manage_category_<?=$category['category_id']?>.html">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Category</label>
                                <input id="catid" name="category_id" type="hidden" value="<?=$category['category_id']?>" class="form-control" maxlength="6" required>
                                <input id="catname" name="category_name" type="text" value="<?=$category['category_name']?>" class="form-control" maxlength="25" required>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea id="catdesc" name="description" cols="20" rows="4" class="form-control" required><?=$category['description']?></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="action" value="Update" class="btn btn-sm btn-primary">
                            </div>
                        </div>
                    </form>
                 </div>
                 
            </div>
            <div class="col-md-12 well">
                
                <p class="content-padding bg-primary">Sub Category</p>
                
                <div class="row">
                    <div class="col-md-12">
                        <form id="addsubcategory" method="POST" action="manage_category_<?=$category['category_id']?>.html">
                            <table class="table table-responsive table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(count($list_category) > 0){
                                        foreach($list_category as $result){
                                            echo "
                                    <tr>
                                        <td>{$result['category_name']}</td>
                                        <td>{$result['description']}</td>
                                        <td><a href=\"manage_sub_category_{$result['category_id']}.html\">Manage</a></td>
                                        <td><a href=\"delete_sub_category_{$result['category_id']}.html\">Delete</a></td>
                                    </tr>";
                                        }
                                    }else{
                                        echo '
                                    <tr>
                                        <td colspan="4" align="center">No data</td>
                                    </tr>';
                                    }
                                ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="category_parent_id" value="<?=$category['category_id']?>"/>
                                            <input name="category_name" type="text" class="sub-category-name form-control" required/>
                                        </td>
                                        <td><input name="description" type="text" class="sub-category-desc form-control" required/></td>
                                        <td colspan="2"><input type='submit' class='btn btn-sm btn-primary' name="action" value="Add Category"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>