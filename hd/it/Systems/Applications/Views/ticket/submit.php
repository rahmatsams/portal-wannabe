                    <div class="container-fluid">
                        <h3 class="well" style="background: rgb(217, 83, 79); color: #fff;">Submit New Ticket</h3>
                        <?=(isset($error) ? '<div class="alert alert-danger">Please check your Highlighted input below</div>' : '')?>
                        <div class="col-lg-12 well">
                            <div class="row">
                                <form id="submitForm" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
                                    <div class="col-lg-12">
                                        <div class="form-group <?=((isset($error) && (isset($error['title']) || isset($error['title_req']) || isset($error['title_max'])) ) ? 'has-error' : '')?>">
                                            <label>Ticket Title (*)</label>
                                            <input name="title" type="text" placeholder="Type here.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['title']) ? "value='{$input['title']}'" : '')?> required>
                                            <input type="hidden" name="type" value="3">
                                            
                                            <?=(isset($error) && isset($error['title_req']) ? "<div class='alert alert-danger'>{$error['title_req']}</div>" : '')?>
                                            <?=(isset($error) && isset($error['title_max']) ? "<div class='alert alert-danger'>{$error['title_max']}</div>" : '')?>
                                            <?=(isset($error) && isset($error['title']) ? "<div class='alert alert-danger'>{$error['title']}</div>" : '')?>
										</div>
										<div class="form-group">
											<label class="checkbox-inline">
												<input type="checkbox" value="1" name="customer_impact">Impact Customer</input>
											</label>
										</div>
                                        <div class="form-group">
                                            <label>Effective Date (*)</label>
                                            <input type="date" class="form-control" name="start_date" id="dateMinToday" required=""><!-- using js for set input date min today -->
                                            <small>(Waktu berlakunya ticketdesk)</small>
                                        </div>
                                        <div class="form-group <?=((isset($error) && (isset($error['title']) || isset($error['title_req']) || isset($error['title_max'])) ) ? 'has-error' : '')?>">
                                            <label>Current Condition</label>
                                            <textarea id="ticket-content" name="detail" placeholder="Type here.." class="form-control" rows="15"><?=(isset($input) && isset($input['detail']) ? $input['detail'] : '')?></textarea>
                                        </div>
                                        <div class="form-group" id="addFile">
                                            <label>Upload Image (JPEG)</label>
                                            <input name="image_upload[]" type="file" class="form-control" accept="image/jpeg">
                                        </div>
                                        <div class="form-group">
                                            <button id="addFileButton" type="button" name="addfile" class="btn btn-sm btn-success">Add File</button>
                                        </div>
                                        <?php
                                            if($admin){
                                        ?>
                                        <div class="form-group" id="selectOnBehalf">
                                            <label for="checkOnBehalf">Create on Behalf other users</label>
                                            <input type="checkbox" value="1" id="checkOnBehalf" name="behalf"></input>
                                            <select id="userlist" name="user" class="form-control selectUserList" autocomplete="off" disabled>
                                                <option> </option>
                                                <?php if (is_array($user_behalf) && count($user_behalf) > 0): ?>
                                                <?php foreach ($user_behalf as $val): ?>
                                                <option><?=$val['display_name']?></option>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="onbehalf">Create on Behalf other users</label>
                                            <input type="checkbox" value="1" id="onbehalf" name="behalf"></input>
                                            <select id="userlist" name="user" class="form-control js-example-basic-single" disabled>
                                            </select>
                                        </div> -->
                                        <?php
                                        }
                                        ?>
                                        
                                        <div class="form-group" id="addFile" style="margin-top:30px;">
                                            <div class="g-recaptcha <?=(isset($error) && isset($error['recaptcha']) ? 'alert alert-danger' : '')?>" data-sitekey="6LemwyETAAAAACZCN9RkUJ1D4j_bxeBvu1lgmAZP"></div>
                                            
                                        </div>
                                        
                                        <button type="submit" name="submit" value="kirim" class="btn btn-lg btn-primary">Submit Ticket</button>
                                        
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
