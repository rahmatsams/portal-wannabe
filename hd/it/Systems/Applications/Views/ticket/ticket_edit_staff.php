<?php
    $menu = array();
    if($_mySession['user_id'] == $ticket['ticket_user']){
        if($ticket['status'] < 5){
            $menu['editbutton'] = 'Edit';
            $menu['commentbutton'] = 'Comment';
            $menu['setdatebutton'] = 'Set Date';
        }
        if($ticket['status'] == 5){
            $menu['closebutton'] = 'Close';
        }
        if($ticket['status'] > 4){
            $menu['reopenbutton'] = 'Re-open';
        }
    }

    if($admin){
        if($ticket['status'] < 5){
            $menu['editbutton'] = 'Edit';
            $menu['commentbutton'] = 'Comment';
            $menu['assignbutton'] = 'Assign';
            $menu['setdatebutton'] = 'Set Date';
        }
        if ($ticket['status'] < 3 && $ticket['status'] > 1 && $ticket['vendor_name'] == '') {
            $menu['needvendorbutton'] = 'Need Vendor';
        }
        if ($ticket['status'] == 3) {
            $menu['cancelvendorbutton'] = 'Cancel Vendor';
            $menu['donevendorbutton']   = 'Vendor Done';
        }
        /*if(!empty($ticket['vendor_name'])){
            $menu['cancelvendorbutton'] = 'Cancel Vendor';
        }*/
        if($ticket['status'] < 5 && $_mySession['user_id'] != $ticket['assigned_staff']){
            $menu['takeoverbutton'] = 'Take Over';
        }
        if($ticket['status'] == 2){
            $menu['resolvebutton'] = 'Resolve';
        }
        if($ticket['status'] == 5 && $_mySession['user_id'] == $ticket['assigned_staff']){
            $menu['forcebutton'] = 'Force Close';
        }
        if($ticket['status'] > 4){
            $menu['reopenbutton'] = 'Re-open';
        }
    }
?>
<div class="container-fluid" style="background: #fff; padding: 0;">
    
    <div class="col-md-12" style="background: rgb(217, 83, 79); color: white;">
        <h4>Ticket <?=$ticket['ticket_id']?> : <?=$ticket['ticket_title']?></h4>
    </div>
    <div class="col-md-12">
        <form id="detailedit">
            <div id="tickettabledetail" class="table-responsive">
                <table class="table table-striped" id="listTable">
                    <tbody>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td><?=$ticket['status_name']?></td>
                            <td>Affect Customer</td>
                            <td>:</td>
                            <td><?=($ticket['impact'] == 1 ? 'Yes' : 'No')?></td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>:</td>
                            <td><?=$ticket['type_name']?></td>
                            <td>Submit Date</td>
                            <td>:</td>
                            <td><?=date("d-M-Y H:i", strtotime($ticket['submit_date']))?></td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['main_category']) ? $ticket['main_category'] : '-')?></td>
                            <td>Effective Date</td>
                            <td>:</td>
                            <td><?=($ticket['start_time'] == '0000-00-00 00:00:00' ? '-' : date("d-M-Y", strtotime($ticket['start_time'])))?></td>
                        </tr>
                        <tr>
                            <td>Sub Category</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['sub_category']) ? $ticket['sub_category'] : '-')?></td>
                            <td>Last Update</td>
                            <td>:</td>
                            <td><?=(!empty($ticket['last_update']) ? date("d-M-Y H:i", strtotime($ticket['last_update'])) : '-')?></td>
                        </tr>
                        <tr>
                            <td>PIC</td>
                            <td>:</td>
                            <td><?=(isset($ticket['staff_name']) ? $ticket['staff_name'] : '-')?></td>
                            <td>Owner</td>
                            <td>:</td>
                            <td><?=$ticket['display_name']?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row hidden" id="tickettableedit">
                <div class="form-group">
                    <label>Ticket Type</label>
                    <select name="type" class="form-control" required>
                        <option value=""></option>
                        <?php
                            foreach($ticket_type as $type_input){
                                echo "\n
                                <option value=\"{$type_input['type_id']}\"" . ($ticket['ticket_type'] == $type_input['type_id'] ? ' Selected' : '') . ">{$type_input['type_name']}</option>\n";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="main_category" id="mainCategory" class="form-control" required>
                        <option value=""></option>
                        <?php
                            foreach($main_category as $category_form){
                                echo "\n
                                <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] || $ticket['category_parent_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Sub Category</label>
                    <select name="sub_category" class="form-control" id="subCategory" <?=(isset($sub_category) && is_array($sub_category) ? '' : 'disabled')?>>
                        <?php
                            if(isset($sub_category) && is_array($sub_category)){
                                foreach($sub_category as $category_form){
                                    echo "\n
                                    <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                                }
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="hidden" name="ticket_id" value="<?=$ticket['ticket_id']?>"/>
                        <input type="checkbox" value="1" name="customer_impact" <?=($ticket['impact'] == 1 ? 'checked' : '')?>>Impact Customer?</input>
                    </label>
                </div>
            </div><!-- end row hidden -->
        </form>
    </div>
    <div class="col-md-12">
        <div class="col-md-12" style="min-height: 350px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group hidden" id="tickettabledesc">
                        <label>Detail</label>
                        <textarea id="ticketcontent" class="form-control" rows="8" readonly=""><?=$ticket['content']?></textarea>
                    </div>
                    <div id="tickettableraw">
                        <?=htmlspecialchars_decode($ticket['content'])?>
                        <!-- <textarea id="viewContent" class="form-control" rows="8" readonly=""><?=$ticket['content']?></textarea> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if(is_array($uploaded) && count($uploaded) > 0):?>
    <div class='col-md-12'>
        <h4 style='background: rgb(22, 105, 173); color: white; padding: 5px;' id='tickettitle'>Uploaded File</h4>
    </div>
    <div class='col-md-12 upload-thumbnail'>
    
    <?php foreach($uploaded as $img_file): ?>
        <a href="<?=$img_file['upload_location'].$img_file['upload_name']?>" target="_blank"><img src="<?=$img_file['upload_location'].$img_file['upload_name']?>" class="img-thumbnail" style="width:100px; height:100px;"></a>
    <?php endforeach; ?>
    
    </div>
    
<?php endif; ?>
    <div class="col-lg-12">
        <h4 style="background: rgb(217, 83, 79); color: white; padding: 5px;" id="tickettitle">Ticket command</h4>
    </div>
    <div class="col-lg-12" style="margin-top: 0px; margin-bottom: 5px;" id="activitybutton">
        <div class="col-lg-12">
            <div class="row">
            
                <?php foreach($menu as $key => $value): ?>
                <div class='col-sm-2 form-group'>
                    <button type='button' value='edit' class='btn btn-sm btn-primary btn-block' id='<?=$key?>'><?=$value?></button>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<div class="col-lg-12 hidden" id="tickettablecomment">
    <form id="updateticketstatus">
        <div class="col-lg-12">
            <div class="row">
                <div class="row hidden" id="categoryInput">
                    <div class="col-sm-4 form-group">
                        <label>Type</label>
                        <select name="ticket_type" id="ticketType" class="form-control">
                        <?php
                            foreach($ticket_type as $type_input){
                                echo "\n
                        <option value=\"{$type_input['type_id']}\"" . ($ticket['ticket_type'] == $type_input['type_id'] ? ' Selected' : '') . ">{$type_input['type_name']}</option>\n";
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>Category</label>
                        <select name="main_category" id="resolveMainCat" class="form-control">
                        <option></option>
                        <?php
                            foreach($main_category as $category_form){
                                echo "\n
                                <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] || $ticket['category_parent_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                            }
                        ?>
                    </select>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Sub Category</label>
                    <select name="sub_category" class="form-control" id="resolveMainSub" <?=(isset($sub_category) && is_array($sub_category) ? '' : 'disabled')?>>
                        <option></option>
                        <?php
                            if(isset($sub_category) && is_array($sub_category)){
                                foreach($sub_category as $category_form){
                                    echo "\n
                                    <option value=\"{$category_form['category_id']}\"" . ($ticket['category_id'] == $category_form['category_id'] ? ' Selected' : '') . ">{$category_form['category_name']}</option>\n";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 form-group hidden" id="setDate">
                    <label>Effective Date</label>
                    <input type="date" class="form-control" id="dateEditMinToday" name="start_date"><!-- using js for set input date min today -->
                <div class="col-sm-6 form-group" hidden="">
                    <label>End Date</label>
                    <input type="date" class="form-control" name="end_date">
                </div>
            </div>
            
            <div class="col-sm-6 form-group hidden" id="coreProblem">
                <label>Problem Cause :</label>
                <textarea id="coreproblem" name="cause" placeholder="Type problem cause.." class="form-control" rows="3"></textarea>
                </select>
            </div>
            <div class="col-sm-6 form-group hidden" id="needVendor">
                    <label>Vendor Name :</label>
                    <input type="text" name="vendor_name" id="vendorName" class="form-control" placeholder="Type vendor name.."/>
                </div>
            <div class="col-sm-12 form-group">
                <label id="labelcomment">Komentar</label>
                <textarea id="updatereason" name="detail" placeholder="Type a comment.." class="form-control" rows="3"></textarea>
            </div>
            <div class="col-sm-6 form-group hidden" id="assignform">
                <label>Assign ticket to :</label>
                <select id="userlist" name="selectedpic" class="form-control">
                    <?php
                        foreach($staff as $pic){
                            echo "\n
                            <option value=\"{$pic['user_id']}\" data-id=\"{$pic['display_name']}\">{$pic['display_name']}</option>\n";
                        }
                    ?>
                </select>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-3 form-group">
                    <button type="submit" name="submit" class="btn btn-sm btn-primary btn-block" id="submitstatus">Update</button>
                </div>
                <div class="col-sm-3 form-group">
                    <button type="button" name="cancel" class="btn btn-sm btn-primary btn-block" id="cancelstatus">Batal</button>
                </div>
            </div>
        </div>
    </form>   
</div>
</div>
<div class="col-lg-12">
<h4 style="background: rgb(217, 83, 79); color: white; padding: 5px;">Activity Log</h4>
</div>
<div class="col-lg-12">
<div class="col-lg-12">
    <?php
        foreach($log as $ticket_log){
            
            if($ticket_log['user_id'] == $ticket['ticket_user']){
                $style = "bg-warning";
            } else {
                $style = "bg-success";
            }
            
            $date = date('D - d M Y, H:i', strtotime($ticket_log['ticketlog_time']));
            echo "
            <table class='table table-bordered'>
                <thead>
                    <tr class='{$style}'>
                        <td class='{$style}' style='width: 15%; font-weight:bold;'>{$ticket_log['ticketlog_type']}</td>
                        <td class='{$style}' style='width: 60%; font-weight:bold;'>{$ticket_log['ticketlog_title']}</td>
                        <td class='{$style}' style='width: 25%; font-weight:bold;'>{$date}</td>
                    </tr>
                </thead>".
                (!empty($ticket_log['ticketlog_content']) && $ticket_log['ticketlog_type'] != 'Submit' ? "<tbody>
                    <tr>
                        <td colspan='3' class='bg-white' style='white-space: pre-wrap;'>". nl2br(htmlspecialchars_decode($ticket_log['ticketlog_content'])) ."</td>
                    </tr>
                </tbody>" : '') ."
            </table>
            ";
        }
    ?>
</div>
</div>
</div>
</div>