<?php
/*
* Model for Ticketdesk
*/
class Store extends Model
{
	
    function getAllActiveStore()
    {
        $query_string = "SELECT * FROM sushitei_portal.portal_store WHERE store_status=1";
		
		$result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function newStore($input)
    {
        $result = $this->insertQuery('sushitei_portal.portal_store', $input);
        return $result;
    }
    
    function getAllStore($query_option)
    {
        $query_string = "SELECT store_id, store_name, store_status, location_name  FROM sushitei_portal.portal_store ps";
        $query_string .= "LEFT JOIN sushitei_portal.store_location sl ON ps.location_id=sl.location_id";
        $count_query = "SELECT COUNT(*) AS row_total FROM sushitei_portal.portal_store";
        
        $result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
    function getAllLocation()
    {
        $query_string = "SELECT * FROM sushitei_portal.store_location";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getAllActiveLocation()
    {
        $query_string = "SELECT * FROM sushitei_portal.store_location WHERE location_status=1";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getStoreBy($input)
    {
        $query_string = "SELECT * from sushitei_portal.portal_store ";
        $query_string .= "WHERE store_id=:store_id";            
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    
    function editStore($form = array(), $where = array()){
        $table = 'store';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }
    
    function deleteStore($id){
        
        $result = $this->deleteQuery('store' ,$id);
        return $result;
        
    }
    
}