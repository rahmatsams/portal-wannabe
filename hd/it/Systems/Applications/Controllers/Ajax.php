<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ajax extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('getLoginData','getSubCategory', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('getSubCategory', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
    public function getLoginData()
    {
        if($this->isGuest() > 0){
            if(isset($_POST['ticket_email']) && isset($_POST['ticket_password'])){
                $input = $this->load->lib('Input');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'min=1', 'Alamat email harus diisi');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'user_name', 'Format email salah');
                $input->addValidation('ticket_password', $_POST['ticket_password'], 'min=1', 'Password harus diisi');
        
                if ($input->validate()) {
                    $model_users = $this->load->model('Users');
                    $result = $model_users->doLogin($_POST);
                    if($result > 0){
                        $this->setSession('user_id',$result['user_id']);
                        $this->setSession('user_name',$result['display_name']);
                        $this->setSession('email',$result['email']);
                        $this->setSession('group',$result['group_name']);
                        $this->setSession('role',$result['level']);
                        echo 'true';
                    } else {
                        echo 'not found';
                    }
                } else {
                    print_r($input->_error);
                }
            
            } else {
                echo 'false';
            }
        
        }
    }
    
    function getSubCategory(){
        if(isset($_POST['category'])){
            $input = $this->load->lib('Input');
            $input->addValidation('category', $_POST['category'] ,'numeric','Terjadi kesalahan');
            if($input->validate()){
                $model_ticket = $this->load->model('TicketCategory');
                $result = $model_ticket->getSubCategory($_POST['category']);
                if(is_array($result) && !empty($result)){
                    echo json_encode($result);
                } else {
                    echo 'false';
                }
                
            }
        } else {
            echo 'false';
        }
    }
    
    function getUserDetail(){
        if(isset($_POST['uid'])){
            $input = $this->load->lib('Input');
            $input->addValidation('uid', $_POST['uid'] ,'numeric','Terjadi kesalahan');
            if($input->validate()){
                $data = array(
                    'user_id' => $_POST['uid']
                );
                $m_users = $this->load->model('Users');
                $result = $m_users->getUserBy($data);
                if(is_array($result) && !empty($result)){
                    echo json_encode($result);
                } else {
                    echo 'false';
                }
            }
    
        
        } else {
            echo 'false';
        }
    }
    
    function getUserList(){
        if(isset($_POST['user'])){
            $input = $this->load->lib('Input');
            $input->addValidation('user', $_POST['user'] ,'numeric','Terjadi kesalahan');
            if($input->validate()){
                $model_ticket = $this->load->model('Users');
                $result = $model_ticket->getUserITTicket();
                if(is_array($result) && !empty($result)){
                    echo json_encode($result);
                } else {
                    echo 'false';
                }
                
            }
        } else {
            echo 'false';
        }
    }
    
    public function getTicketData(){
        if(!$this->isGuest()){
            $input = $this->load->lib('Input');
            $input->addValidation('page',$_POST['page'],'numeric', 'Periksa kembali input anda');
            if(isset($_POST['main_category']) && !empty($_POST['main_category'])){
                $input->addValidation('main_category_char',$_POST['main_category'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('main_category_max',$_POST['main_category'], 'max=3', 'Periksa kembali input anda');
            }
            if(isset($_POST['pic']) && !empty($_POST['pic'])){
                $input->addValidation('pic_char', $_POST['pic'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('pic_max', $_POST['pic'], 'max=8', 'Periksa kembali input anda');
            }
            if(isset($_POST['ticket_type']) && !empty($_POST['ticket_type'])){
                $input->addValidation('status_char',$_POST['status'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('status_max',$_POST['status'], 'max=2', 'Periksa kembali input anda');
            }
            if(isset($_POST['sub_category']) && !empty($_POST['sub_category'])){
                $input->addValidation('sub_category_char',$_POST['sub_category'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('sub_category_max',$_POST['sub_category'], 'max=3', 'Periksa kembali input anda');
            }
            if(isset($_POST['store']) && !empty($_POST['store'])){
                $input->addValidation('store',$_POST['store'], 'alpha_numeric_sp', 'Please check your input');
                $input->addValidation('store_max',$_POST['store'], 'max=25', '25');
            }
            if(isset($_POST['date_start']) && !empty($_POST['date_start'])){
                $input->addValidation('date_start',$_POST['date_start'], 'date', 'Periksa kembali input anda');
            }
            if(isset($_POST['date_end']) && !empty($_POST['date_end'])){
                $input->addValidation('date_end',$_POST['date_end'], 'date', 'Periksa kembali input anda');
            }

            if ($input->validate()){
                $model_ticket = $this->load->model('Tickets');
                
                $option = array(
                    'page' => (isset($_POST['page']) ? $_POST['page'] : 1),
                    'result' => 10,
                    'order_by' => 'submit_date',
                    'order' => 'DESC'
                );
                
                $query = array();
                
                if($_POST['sort'] == 'ticket_asc'){
                    $option['order_by'] = 'ticket_id';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'ticket_desc'){
                    $option['order_by'] = 'ticket_id';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'title_asc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'title_desc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'outlet_asc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'outlet_desc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'submit_asc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'submit_desc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'category_asc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'category_desc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'pic_asc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'pic_desc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'status_asc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'status_desc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'DESC';
                }
                
                if(isset($_POST['ticket_title']) && !empty($_POST['ticket_title'])){
                    $query['ticket_title']['value'] = "%{$_POST['ticket_title']}%";
                    $query['ticket_title']['type'] = 'LIKE';
                }
                
                #if(isset($_POST['main_category']) && !empty($_POST['main_category']) && empty($_POST['sub_category'])) $query['category'] = $_POST['main_category'];
                
                if(isset($_POST['status']) && !empty($_POST['status'])){
                    $query['status']['value'] = $_POST['status'];
                    $query['status']['type'] = '=';
                }
                
                if(isset($_POST['pic']) && !empty($_POST['pic'])){
                  $query['assigned_staff']['value'] = $_POST['pic'];
                  $query['assigned_staff']['type'] = '=';  
                } 
                
                if(isset($_POST['store']) && !empty($_POST['store'])){
                    $query['store_id']['value'] = $_POST['store'];
                    $query['store_id']['type'] = '=';
                }
                
                if(isset($_POST['date_start']) && !empty($_POST['date_start'])) {
                    $query['submit_date']['value'] = $_POST['date_start'];
                    $query['submit_date']['type'] = '=';
                }
                
                if(isset($_POST['date_end']) && !empty($_POST['date_end'])){
                    $query['date_end']['value'] = $_POST['date_end'];
                    $query['date_end']['type'] = '=';
                }
                
                if(!empty($_POST['main_category']) && $_POST['sub_category'] == '999'){
                    unset($query['category']);
                    $query['get_all']['value'] = $_POST['main_category'];
                    $query['get_all']['type'] = '=';
                }

                if(!empty($_POST['sub_category']) && !empty($_POST['main_category']) && $_POST['sub_category'] != '999'){
                    $query['category']['value'] = $_POST['sub_category'];
                    $query['category']['type'] = '=';
                }
                
                if(!$this->isAdmin()){
                    $query['ticket_user']['value'] = $this->_mySession['user_id'];
                    $query['ticket_user']['type'] = '=';

                    $query['store_id']['value'] = $this->_mySession['outlet'];
                    $query['store_id']['type'] = '=';
                    
                    /*$my_info = $model_ticket->myOutlet(array('user_id' => $this->_mySession['user_id']));
                    $query['store_name'] = $my_info['store_name'];*/
                }
                
                $ticket_list = $model_ticket->showAllTicketFilteredSearch($query, $option);
                $this->setSession('last_query', $query);
                $this->setSession('option', $option);
                if(count($ticket_list) > 0){
                    
                    $this->setSession('last_page', ceil($model_ticket->getCountResult()/$option['result']));
                    $this->setSession('current_page', $option['page']);
                    $num = (($option['page']-1)*10) + 1;
                    foreach($ticket_list as $result){
                        $date_check = '';
                        $effective_date = ($result['start_time'] == '0000-00-00 00:00:00' ? strtotime($result['submit_date']) : strtotime($result['start_time']));
                        $diff_seconds  = ($result['resolved_date'] != '0000-00-00 00:00:00' ? strtotime($result['resolved_date']) - $effective_date : strtotime(date("Y-m-d H:i:s")) - $effective_date);
                        $stat = floor($diff_seconds/3600);
                        
                        if(($stat >= 24 && $stat < 48) && $result['status'] < 5){
                            
                            $date_check = "warning";
                        }elseif($stat >= 48 && $result['status'] < 5){
                            $date_check = "danger";
                        }elseif($result['status'] >= 5){
                            $date_check = "success";
                        }else {
                            $date_check = 'active';
                        }
                        
                        $submit_date = date("d-m-Y", strtotime($result['submit_date']));
                        $activate_date = (!empty($result['start_time']) ? date("d-m-Y", strtotime($result['start_time'])) : '-');
                        $last_update = (!empty($result['last_update']) ? date("d-m-Y", strtotime($result['last_update'])) : '-');
                            
                        if(!$this->isAdmin()){
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$num} {$query['assigned_staff']}</td>
                        <td>{$result['ticket_id']}</td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                        <td>{$result['parent_category']}</td>
                        <td><a href=\"result_by_cat_{$result['category_id']}.html\">{$result['category_name']}</a></td>
                        <td>{$result['staff_name']}</td>
                        <td><a href=\"result_by_status_{$result['status']}.html\">{$result['status_name']}</a></td>
                        <td>{$submit_date}</td>
                        <td>{$activate_date}</td>
                        <td>{$last_update}</td>
                    </tr>";    
                        }else{
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$result['ticket_id']}</td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                        <td>{$result['display_name']}</td>
                        <td>{$result['store_name']}</td>
                        <td>{$result['parent_category']}</td>
                        <td><a href=\"result_by_cat_{$result['category_id']}.html\">{$result['category_name']}</a></td>
                        <td>{$result['staff_name']}</td>
                        <td><a href=\"result_by_status_{$result['status']}.html\">{$result['status_name']}</a></td>
                        <td>{$submit_date}</td>
                        <td>{$activate_date}</td>
                        <td>{$last_update}</td>
                    </tr>";
                        }                
                        #$num++;
                    }
                } else {
                    echo "
                    <tr>
                        <td colspan=\"9\">Tiket Kosong</td>
                    </tr>";
                }           
            }
        } else {
            echo 'nope';
        }
        
    }
    
    public function getTotalPage(){
        $max = 9;
        $diff = 4;
        $ps = 1;
        $ls = $ps;
        $le = $max;
        $tp = (!empty($this->_mySession['last_page'])) ? $this->_mySession['last_page'] : 1;
        $cp = $this->_mySession['current_page'];
        $o = '';
        if($tp > $max){
            
            if($cp > $diff+1 ){
                $o .= "
                <li>
                    <a href=\"#\" class='page-navigate' data-id='1'>1</a>
                </li>
                <li>
                    <a href=\"#\">...</a>
                </li>";
                $ls = $cp-$diff;
                
                
                if($cp == $tp){
                    $le = $tp;
                }elseif($cp+$diff > $tp){
                    $le = $tp;
                }else{
                    $le = $cp+$diff;
                }
            }
            
            
            for($i=$ls; $i <= $le;$i++){
                $o .= "<li ".(($cp == $i) ? 'class="active"' : ' ')."><a class='page-navigate' data-id='{$i}'>{$i}</a></li>";
            }
            
            if($cp < ($tp-($diff+1)) ){
                $o .= "
                <li>
                    <a href=\"#\">...</a>
                </li>
                <li>
                    <a href=\"#\" class='page-navigate' data-id='{$tp}'>{$tp}</a>
                </li>
                ";
            }elseif($cp != $tp && $cp+$diff < $tp){
                $o .= "<li>
                    <a href='#'>{$tp}</a>
                </li>";
            }
        }else{
            for($i=$ps; $i < $tp;$i++){
                $o .= "<li ".(($cp == $i) ? 'class="active"' : ' ')."><a class='page-navigate' data-id='{$i}'>{$i}</a></li>";
            }
        }
        echo $o;
    }
    
    
    public function getCategoryEdit()
    {
        if(!$this->isGuest()){
            if(isset($_POST['cid'])){
            $input = $this->load->lib('Input');
            $input->addValidation('cid', $_POST['cid'], 'numeric', 'Periksa kembali input anda');
            
                if ($input->validate()) {
                    $m_category = $this->load->model('TicketCategory');
                    $result = $m_category->getCategoryId($_POST);
                    $sub_category = $m_category->getSubCategory($_POST['cid']);
                    if(is_array($result)){
                        echo json_encode($result);
                    } else {
                        echo 'false';
                    }
                } else {
                    echo 'false';
                }
            
            } else {
                echo 'false';
            }
        
        } else {
            echo 'false';
        }
    }
    
    public function editTicket(){
        if(isset($_POST['submit_type'])){
            $result = 'failed';
            $m_ticket = $this->load->model('Tickets');
            $input = $this->load->lib('input');
            $edit_status = 0;
            
            if($_POST['submit_type'] == 'Need Vendor'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input->addValidation('vendor_name', $_POST['vendor_name'], 'alpha_numeric_sp', 'Please check your input');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                    'vendor_name' => $input->cleanInput($_POST['vendor_name']),
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 3,
                            'vendor_name' => $input_filtered['vendor_name'],
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Need Vendor',
                        'ticketlog_title' => $this->_mySession['user_name']. " marked this ticket need a vendor",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (NEED VENDOR): ";
                    }
                }else{
                    print_r($input->_error);
                }
            }elseif($_POST['submit_type'] == 'Cancel Sparepart'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 2, #on-progress
                            'spare_part' => '',
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Cancel Sparepart',
                        'ticketlog_title' => $this->_mySession['user_name']. " canceled vendor",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (CANCEL VENDOR): ";
                    }
                }else{
                    print_r($input->_error);
                }
            }elseif($_POST['submit_type'] == 'Vendor Done'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 2, #on-progress
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Vendor Done',
                        'ticketlog_title' => $this->_mySession['user_name']. " - Vendor has done.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (VENDOR DONE): ";
                    }
                }else{
                    print_r($input->_error);
                }
            }elseif($_POST['submit_type'] == 'Comment'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if(isset($_POST['main_cat'])){
                    $input->addValidation('main', $_POST['main_cat'], 'numeric', 'Unknown ID');
                    if(!empty($_POST['sub_cat'])){
                        $input->addValidation('sub', $_POST['sub_cat'], 'numeric', 'Unknown ID');
                    }
                }
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    if(isset($_POST['main_cat'])){
                        
                        if(!empty($_POST['sub_cat'])){
                            $data['input']['category'] = $_POST['sub_cat'];
                        }else{
                            $data['input']['category'] = $_POST['main_cat'];
                        }
                    }
                    
                    $log = array(
                        'ticketlog_type'    => 'Comment',
                        'ticketlog_title'   => $this->_mySession['user_name']. " Commented this ticket",
                        'user_id'           => $this->_mySession['user_id'],
                        'ticket_id'         => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time'    => $data['input']['last_update'],
                        'ticketlog_show'    => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (COMMENT): ";
                    }
                }else{
                    print_r($input->_error);
                }
            }elseif($_POST['submit_type'] == 'Take Over'){
                
                
                $input_filtered = array(
                    'comment' => (!empty($_POST['comment'])) ? htmlspecialchars($_POST['comment'], ENT_QUOTES) : ''
                );
                if(!empty($_POST['comment'])) $input->addValidation('comment', $input_filtered['comment'], 'textarea_no_html', 'Wrong input format');
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'assigned_staff' => $this->_mySession['user_id'],
                            'status' => 2,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    if(isset($_POST['main_cat'])){
                        
                        if(!empty($_POST['sub_cat'])){
                            $data['input']['category'] = $_POST['sub_cat'];
                        }else{
                            $data['input']['category'] = $_POST['main_cat'];
                        }
                        $data['input']['ticket_type'] = (isset($_POST['ticket_type']) && !empty($_POST['ticket_type']) ? $_POST['ticket_type'] : 4);
                    }
                    $log = array(
                        'ticketlog_type' =>  'Take Over',
                        'ticketlog_title' => $this->_mySession['user_name']. " Taking over this ticket",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (TAKE OVER): ";
                    }
                }
            }elseif($_POST['submit_type'] == 'Assign'){
               
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input->addValidation('assigned_staff', $_POST['assigned_staff'], 'numeric', 'Unknown Staff');
                $input->addValidation('a_name', $_POST['a_name'], 'alpha_numeric_sc', 'Unknown Target');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'assigned_staff' => $_POST['assigned_staff'],
                            'status' => 2,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    if(isset($_POST['main_cat'])){
                        
                        if(!empty($_POST['sub_cat'])){
                            $data['input']['category'] = $_POST['sub_cat'];
                        }else{
                            $data['input']['category'] = $_POST['main_cat'];
                        }
                    }
                    $log = array(
                        'ticketlog_type' =>  'Assign',
                        'ticketlog_title' => $this->_mySession['user_name']. " Assigned ". $_POST['a_name'],
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (ASSIGN): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Set Date'){
               
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input->addValidation('start_date', $_POST['start_date'], 'date', 'Unknown Staff');
                //$input->addValidation('end_date', $_POST['end_date'], 'date', 'Unknown Target');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'start_time' => $_POST['start_date'],
                            'end_time' => $_POST['start_date'],
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    if(isset($_POST['main_cat'])){
                        
                        if(!empty($_POST['sub_cat'])){
                            $data['input']['category'] = $_POST['sub_cat'];
                        }else{
                            $data['input']['category'] = $_POST['main_cat'];
                        }
                    }
                    $log = array(
                        'ticketlog_type' =>  'Set Date',
                        'ticketlog_title' => $this->_mySession['user_name']. " Set active date for the ticket {$_POST['start_date']}.", //$this->_mySession['user_name']. " Set active date for the ticket {$_POST['start_date']} - {$_POST['end_date']}.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (ASSIGN): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Resolve'){
             
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input->addValidation('main', $_POST['main_cat'], 'numeric', 'Unknown ID');
                if(!empty($_POST['sub_cat'])){
                    $input->addValidation('sub', $_POST['sub_cat'], 'numeric', 'Unknown ID');
                }
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                    'problem_source' => $input->cleanInput($_POST['problem_source'])
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'resolved_solution' => $input_filtered['comment'],
                            'problem_source' => $input_filtered['problem_source'],
                            'status' => 5,
                            'resolved_date' => date("Y-m-d H:i:s"),
                            'last_update' => date("Y-m-d H:i:s"),
                            'ticket_type' => (isset($_POST['ticket_type']) && !empty($_POST['ticket_type']) ? $_POST['ticket_type'] : 4),
                            'category' => (isset($_POST['sub_cat']) && !empty($_POST['sub_cat']) ? $_POST['sub_cat'] : $_POST['main_cat'])
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Resolve',
                        'ticketlog_title' => $this->_mySession['user_name']. " Resolved this ticket",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (RESOLVE): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Force Close'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 8,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Force-Close',
                        'ticketlog_title' => $this->_mySession['user_name']. " Closing this ticket by Force.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (FORCE CLOSE): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Close'){
                
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 7,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Close',
                        'ticketlog_title' => $this->_mySession['user_name']. " Closing this ticket.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (CLOSED): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Re-open'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 2,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Re-Open',
                        'ticketlog_title' => $this->_mySession['user_name']. " Re-open this ticket.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (RE-OPEN): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Edit'){
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                    //'desc' => $input->cleanInput($_POST['description']) #edit_content_detail
                );
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');;
                $input->addValidation('main_category', $_POST['main_category'], 'numeric', 'Wrong Category');
                $input->addValidation('impact', $_POST['impact'], 'numeric', 'Check your input');
                if(isset($_POST['sub_category'])){
                    $input->addValidation('sub_category', $_POST['sub_category'], 'numeric', 'Wrong Sub-Category');
                }
                
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            //'content' => $input_filtered['desc'], #edit_content_detail
                            'category' => (isset($_POST['sub_category']) ? $_POST['sub_category'] : $_POST['main_category']),
                            'impact' => $_POST['impact'],
                            'ticket_type' => $_POST['ticket_type'],
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Edit Detail',
                        'ticketlog_title' => $this->_mySession['user_name']. " Edited the ticket detail.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'user_id' => $this->_mySession['user_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "IT Ticket #{$_POST['t_id']} (EDIT): ";
                    }
                }else{
                    print_r($_POST);
                    print_r($input->_error);
                }
            }
            if($edit_status == 1){
                $ticket_data = $m_ticket->showTicketBy(array('ticket_id' => $_POST['t_id']));
                $mail_option['recipient'] = $ticket_data['email'];
                $mail_option['recipient_name'] = $ticket_data['display_name'];
                $mail_option['title'] .= $ticket_data['ticket_title'];
                $this->send_email($mail_option, $ticket_data, $m_ticket);
                echo 'sukses';
            }
        }
    
    }    
    
    private function send_email($option, $data, $model_log = 0){
        $mail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail = $this->load->lib('STMail');
        if(!$model_log) $model_log = $this->load->model('Tickets');
        $log_data = $model_log->getLog(array('ticket_id' => $data['ticket_id']));
        $stmail->IsSMTP(); // telling the class to use SMTP
        $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $stmail->SMTPAuth   = true;                  // enable SMTP authentication
        $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $stmail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $stmail->Username   = "it.helpdesk@sushitei.co.id"; // SMTP account user_name
        $stmail->Password   = "St888@itoke";        // SMTP account password

        $stmail->SetFrom('it.helpdesk@sushitei.co.id', 'IT Helpdesk');

        $stmail->AddReplyTo('it.helpdesk@sushitei.co.id', 'IT Helpdesk');
        
        $stmail->isHTML('true');

        $stmail->Subject    = $option['title'];
        
        $stmail->MsgHTML($stmail->mailContent($data, $log_data));
        
        $stmail->AddAddress($option['recipient'], $option['recipient_name']);
        
        foreach($model_log->getMailRecipient() as $recipient){
            $stmail->AddBCC($recipient['email'], $recipient['display_name']);
        }
        
        if(isset($data['category_id']) && !empty($data['category_id'])){
            foreach($model_log->getEmailReceiver($data['category_id']) as $result){
                $stmail->AddBCC($result['email'], $result['recipient_name']);
            }
        }

        $send = $stmail->Send();
        if(!$send){
            error_log("Mailer Error: " . $stmail->ErrorInfo);
        }
        
        return ($stmail->Send() ? 1 : $stmail->ErrorInfo);
            
        
    }
}
/*
* End Home Class
*/