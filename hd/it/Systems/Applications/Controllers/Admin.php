<?php
class Admin extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('index'),
                'groups'=>array('Guest'),
            ),
            array('Allow', 
                'actions'=>array('index','exportbyuser', 'store_index', 'store_insert', 'store_edit'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin IT'),
            ),
            array('Allow', 
                'actions'=>array('exportbyuser', 'phpExcel'),
                'groups'=>array('Ticket Users', 'Admin IT', 'Admin View'),
            ),
        );
    }
    
    function index()
    {
        $data = array(
            'menu'=>'adminmenu',
            '_mySession' => $this->_mySession,
        );
        $this->load->template('admin/index',$data);
    }
    
    function exportbyuser()
    {
        $data = $this->load->model('Tickets');
        $option = array(
            'file_name' => 'Tiket IT Export',
            'context_title' => 'Rekap Ticket IT'
        );
        $query_option = array(
            'order_by' => 'submit_date',
            'order' => 'DESC'
        );
        if(empty($this->_mySession['last_query']) || !is_array($this->_mySession['last_query'])){
            if($this->_mySession['group'] > 5){
                $query = array();
            }else{
                $query = array(
                    'ticket_user' => $this->_mySession['user_id']
                );
            }
            
        }else{
            $query = $this->_mySession['last_query'];
        }
        #exit(print_r($query));
        //$this->phpExcel($data->showAllTicketFilteredSearch($query, $query_option), $option);
        $this->phpExcel($data->showAllTicketFiltered($query, $query_option), $option);
        print_r($data->showAllTicketFiltered($query, $query_option));
    }
    
    function phpExcel($ticket_result, $option)
    {
        $objPHPExcel = $this->load->lib('PHPExcel');
        $objPHPExcel->getProperties()->setCreator("Sushitei IT")
                             ->setLastModifiedBy("Sushitei IT")
                             ->setTitle("Ticketdesk IT")
                             ->setSubject("Ticketdesk IT")
                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Test result file");

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:R1')
                    ->setCellValue('A1', $option['context_title'])
                    ->mergeCells('A3:A4')
                    ->setCellValue('A3', 'No.')
                    ->mergeCells('B3:C3')
                    ->setCellValue('B3', 'Pemohon')
                    ->setCellValue('B4', 'Nama')
                    ->setCellValue('C4', 'Outlet')
                    ->mergeCells('D3:H3')
                    ->setCellValue('D3', 'Registrasi Tiket')
                    ->setCellValue('D4', 'No Tiket')
                    ->setCellValue('E4', 'Tanggal Registrasi')
                    ->setCellValue('F4', 'Tanggal Efektif')
                    ->setCellValue('G4', 'Tanggal Respon')
                    ->setCellValue('H4', 'Waktu Respon(Jam)')
                    ->mergeCells('I3:J3')
                    ->setCellValue('I3', 'Jenis Trouble')
                    ->setCellValue('I4', 'Kategori')
                    ->setCellValue('J4', 'Nama')
                    ->mergeCells('K3:L3')
                    ->setCellValue('K3', 'Analisa Permasalahan')
                    ->setCellValue('K4', 'Masalah/Kondisi saat ini')
                    ->setCellValue('L4', 'Penyebab Masalah')
                    ->mergeCells('M3:O3')
                    ->setCellValue('M3', 'Penanganan Masalah')
                    ->setCellValue('M4', 'Solusi')
                    ->setCellValue('N4', 'Estimasi Biaya')
                    ->setCellValue('O4', 'PIC')
                    ->setCellValue('P3', 'Status Penanganan')
                    ->mergeCells('Q3:S3')
                    ->setCellValue('Q3', 'Waktu')
                    ->setCellValue('Q4', 'Aktual')
                    ->setCellValue('R4', 'Tanggal Aktif')
                    ->setCellValue('S4', 'Durasi')
                    ->setCellValue('T4', 'Type');

        // Miscellaneous glyphs, UTF-8
        if(is_array($ticket_result) && !empty($ticket_result)){
            $no = 1;
            $row = 5;
            $status = array('Unknown','Open','On Progress','Pending','Need Vendor','Solved','Closed');
            $priority = array('Unknown','ASAP','High','Medium','Low');
            foreach($ticket_result as $result){

                $effective_date = ($result['start_time'] == '0000-00-00 00:00:00' ? '-' : date("m/d/Y", strtotime($result['start_time'])));
                $start_time     = ($result['start_time'] == '0000-00-00 00:00:00' ? '-' : date("m/d/Y H:i:s", strtotime($result['start_time'])));
                $end_time       = date('m/d/Y', strtotime($result['end_time']));
                $submit_date    = date('m/d/Y H:i:s', strtotime($result['submit_date']));
                $last_update    = date('m/d/Y H:i:s', strtotime($result['last_update']));
                $assign_time    = (is_null($result['assign_time']) ? '-' : strtotime($result['assign_time']));
                $assign_date    = (is_null($result['assign_time']) ? date("m/d/Y") : date("m/d/Y", strtotime($result['assign_time'])));
                $actual         = ($result['resolved_date'] == '0000-00-00 00:00:00') ? '-' : date("m/d/Y", strtotime($result['resolved_date']));

                if($start_time != '-'){
                    $duration = floor((strtotime($result['resolved_date']) - strtotime($start_time))/(60*60));
                }else{
                    $duration = floor((strtotime($result['resolved_date']) - strtotime($result['submit_date']))/(60*60));
                }
                $response_time = ($assign_time != '-') ? floor(($assign_time - strtotime($result['submit_date']))/(60*60)) : $assign_time;
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A{$row}", $no)
                            ->setCellValue("B{$row}", $result['display_name'])
                            ->setCellValue("C{$row}", $result['store_name'])
                            ->setCellValue("D{$row}", $result['ticket_id'])
                            ->setCellValue("E{$row}", $submit_date)
                            ->setCellValue("F{$row}", $effective_date)
                            ->setCellValue("G{$row}", $assign_date)
                            ->setCellValue("H{$row}", $response_time)
                            ->setCellValue("I{$row}", (!empty($result['parent_category']) && $result['store_name'] == 'HO') ? $result['parent_category'] : (!empty($result['parent_category']) && $result['store_name'] != 'HO' ? $result['parent_category'] : ''))
                            ->setCellValue("J{$row}", $result['category_name'])
                            ->setCellValue("K{$row}", str_replace("&nbsp;", " ", strip_tags(htmlspecialchars_decode($result['content']))))
                            ->setCellValue("L{$row}", $result['problem_source'])
                            ->setCellValue("M{$row}", $result['resolved_solution'])
                            ->setCellValue("O{$row}", $result['staff_name'])
                            ->setCellValue("P{$row}", $result['status_name'])
                            ->setCellValue("Q{$row}", $actual)
                            ->setCellValue("R{$row}", $effective_date)
                            ->setCellValue("S{$row}", ($effective_date != '-') ? "=Q{$row}-R{$row}" : "=Q{$row}-E{$row}")
                            ->setCellValue("T{$row}", $result['type_name']);
                $no++;
                $row++;
            }
        } else {
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:J4')
                    ->setCellValue('A4', "Tiket kosong");
        }
        
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Category');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename={$option['file_name']}.xlsx");
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
    
    function store_index()
    {
        
        $model_store = $this->load->model('Store');
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'store_name',
            'order' => 'ASC'
        );
        $data = array(
            '_mySession' => $this->_mySession,
            'store' => $model_store->getAllStore($query_option),
            'total' => $model_store->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
        );
        $this->load->template('admin/index_store', $data);
    }
    
    function store_insert(){
        $model_store = $this->load->model('Store');
        $data = array(
            '_mySession' => $this->_mySession,
            'location_list' => $model_store->getAllActiveLocation()
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'create_store'){
            $input = $this->load->lib('Input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->newStore($store)){
                    header("Location: admin_store.html");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/add_store', $data);
            }
        }else{
            $this->load->template('admin/add_store', $data);
        }
        
    }
    
    function store_edit(){
        $model_store = $this->load->model('Store');
        $input = $this->load->lib('Input');
        $data = array(
            '_mySession' => $this->_mySession
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_store'){
            $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->editStore($store, array('store_id' => $_GET['id']))){
                    header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/edit_store', $data);
            }
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                if($input->validate()){
                    $data['store'] = $model_store->getStoreBy(array('store_id' => $_GET['id']));
                    if(is_array($data['store']) && count($data['store']) > 1){
                        $data['location_list'] = $model_store->getAllActiveLocation();
                        $this->load->template('admin/edit_store', $data);
                    }
                }
                
            }
        }
        
    }
}
?>