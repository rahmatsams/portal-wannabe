<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* User Controller
*/
class User extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('index', 'insert_category', 'edit_user', 'new_user', 'my_account'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin IT'),
            ),
            array('Deny', 
                'actions'=>array('index', 'my_account', 'edit_user', 'edit_user', 'new_user'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
	public function load_model(){
		$model = $this->load->model('Users');
		return $model;
	}
	
    public function index(){
        $m_users = $this->load->model('Users');
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'user_name',
            'order' => 'ASC'
        );
        $data = array(
            '_mySession' => $this->_mySession,
            'users' => $m_users->getAllUser($query_option),
            'total' => $m_users->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
            'groups' => $m_users->getAllGroups(),
        );
        $option = array(
            'personal_js' => 1,
            'scriptload' => '
            <script type="text/javascript">
                $(document).ready(function(){
                    $(".edit-users").click(function(event){
                        event.preventDefault();
                        classid = this.id;
                        getUser(classid);
                    });
                });
               
            </script>
            '
        );
        $this->load->template('admin/index_users', $data, $option);
    }
    
    
    public function register_ok()
    {
        $data = array(
            '_mySession' => $this->_mySession,
        );
        $this->load->template('users/reg_ok', $data);
    }
    
    public function my_account(){
        $data = array(
            '_mySession' => $this->_mySession,
        );
        $account = $this->load->model('Users');
        $store = $this->load->model('Store');
        $data['store'] = $store->getAllActiveStore();
        if(isset($_POST['register']) && $_POST['register'] == 'Daftar' ){
            $input = $this->load->lib('Input');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc','Cek kembali input anda');
            $input->addValidation('email',$_POST['email'],'email','Cek kembali input anda');
            if(!empty($_POST['password'])){
                $input->addValidation('password', $_POST['password'], 'min=6', 'Password minimal 6 huruf');
                $input->addValidation('password_confirm', $_POST['password_confirm'], 'like=' . $_POST['password'], 'Password konfirmasi harus sama dengan password awal');
                $_POST['user_password'] = md5($_POST['password']);
            }
            if($input->validate()){
                $_POST['store_id'] = $_POST['outlet'];
                unset($_POST['outlet']);
                unset($_POST['register']);
                unset($_POST['password']);
                unset($_POST['password_confirm']);
                $where = array('user_id' => $this->_mySession['user_id']);
                if($account->editUser($_POST, $where)){
                    $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['user_id']));
                    $data['success'] = 1;
                    $this->load->template('users/my_account', $data);
                }else{
                    $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['user_id']));
                    $data['success'] = 0;
                    $this->load->template('users/my_account', $data);
                }
            }else{
                $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['user_id']));
                $data['success'] = 0;
                $data['error'] = $input->_error;
                $this->load->template('users/my_account', $data);
            }
            
        }else{
            $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['user_id']));
            if(is_array($data['account']) && count($data['account']) > 1){
                $this->load->template('users/my_account', $data);
            }else{
                $this->showError(2);
            }
        }
    }
    
    public function edit_user(){
        $account = $this->load->model('Users');
        $data = array(
            '_mySession' => $this->_mySession,
            'groups' => $account->getAllGroups(),
            'outlet_list' => $account->getAllActiveStore(),
        );
        $input = $this->load->lib('Input');
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_user' ){
            $input->addValidation('user_id',$_POST['user_id'], 'numeric', 'Cek kembali input anda');
            $input->addValidation('user_id',$_POST['user_id'], 'max=4', 'Cek kembali input anda');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Please check your input');
            $input->addValidation('user_name',$_POST['user_name'],'min=1', 'cannot be blank');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Only Accept numeric and t');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Cannot be blank');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('store_id', $_POST['store_id'],'min=1', 'Cannot be blank');
            $input->addValidation('store_id', $_POST['store_id'],'numeric', 'Please check your input');
            $input->addValidation('group_id', $_POST['group_id'],'min=1', 'Cannot be blank');
            $input->addValidation('group_id', $_POST['group_id'],'numeric', 'Please check your input');
            if(!empty($_POST['new_password'])){
                $input->addValidation('new_password', $_POST['new_password'], 'min=6', 'Password minimal 6 huruf');
                $input->addValidation('confirm_password', $_POST['confirm_password'], 'like=' . $_POST['new_password'], 'Password konfirmasi harus sama dengan password awal');
            }
            if($input->validate()){
                $input_data = array(
                    'users' => array(
                        'display_name' => $_POST['display_name'],
                        'group_id' => $_POST['group_id'],
                        'store_id' => $_POST['store_id'],
                        'email' => $_POST['email'],
                        'user_status' => $_POST['user_status'],
                    ),
                    'where' => array(
                        'user_id' => $_POST['user_id']
                    ),
                );
                if(!empty($_POST['new_password'])){
                    $input_data['users']['user_password'] = md5($_POST['new_password']);
                }
                
                if($account->editTicketUser($input_data)){
                    $data['user'] = $account->getUserBy(array('user_id' => $_POST['user_id']));
                    $data['success'] = 1;
                    $this->load->template('admin/edit_users', $data);
                }else{
                    $data['user'] = $account->getUserBy(array('user_id' => $_POST['user_id']));
                    $data['success'] = 0;
                    $this->load->template('admin/edit_users', $data);
                }
            }else{
                $data['user'] = $account->getUserBy(array('user_id' => $_POST['user_id']));
                $data['error'] = $input->_error;
                $this->load->template('admin/edit_users', $data);
            }
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'User not found');
                $input->addValidation('id', $_GET['id'], 'max=6', 'User not found');
                if($input->validate()){
                    $data['user'] = $account->getUserBy(array('user_id' => $_GET['id']));
                    $this->load->template('admin/edit_users', $data);
                }
            }
            
        }
    }
	
    public function new_user()
    {
        $m_users = $this->load->model('Users');
        $data = array(
            '_mySession' => $this->_mySession,
            'groups' => $m_users->getAllGroups(),
            'outlet_list' => $m_users->getAllActiveStore(),
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'new_user'){
            $input = $this->load->lib('Input');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Please check your input');
            $input->addValidation('user_name',$_POST['user_name'],'min=1', 'cannot be blank');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Please check your input');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Cannot be blank');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('new_password', $_POST['new_password'],'min=6', 'Password lenght should be greater than 6 character');
            $input->addValidation('confirm_password', $_POST['confirm_password'],'like='.$_POST['new_password'], 'Password confirmation is different');
            $input->addValidation('store_id', $_POST['store_id'],'min=1', 'Cannot be blank');
            $input->addValidation('store_id', $_POST['store_id'],'numeric', 'Please check your input');
            $input->addValidation('group_id', $_POST['group_id'],'min=1', 'Cannot be blank');
            $input->addValidation('group_id', $_POST['group_id'],'numeric', 'Please check your input');
            if($input->validate()){
                $insert_value = array(
                    'new_user' => array(
                        'user_name' => $_POST['user_name'],
                        'user_password' => MD5($_POST['new_password']),
                        'display_name' => $_POST['display_name'],
                        'email' => $_POST['email'],
                        'store_id' => $_POST['store_id'],
                        'group_id' => $_POST['group_id'],
                        'user_status' => $_POST['user_status']
                    )
                );
                if($m_users->checkUserName($_POST['user_name'])){
                    if($m_users->newTicketUser($insert_value)){
                        header("Location: admin_user.html");
                        
                    } else {
                        exit('Unexpected Error');
                    }
                }else{
                    $data['error']['user_name'] = 'is already used';
                }
            } else {
                $data['error'] = $input->_error;
            }
            $data['last_input'] = $_POST;
            
        }
        
        $this->load->template('admin/add_users', $data);
        
    }
    
	public function delete_user(){
		if(isset($_POST['rem_user'])){
			$input = $this->load->lib('Input');
			$input->addValidation('id',$_POST['id'], 'numeric', 'Periksa kembali input anda');
			if($input->validate()){
				if($this->load_model->deleteUser($_POST)){
					echo 'sukses';
				}
			}else{
				print_r($input->_error);
			}
		}
	}
}

/*
End User Controller
*/