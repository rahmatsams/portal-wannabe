$(document).ready(function(){
    var tinyMCE = tinymce.init({
        selector: "#ticket-content",
        //entity_encoding : "raw", //menambah karakter yg tidak penting
        theme: "modern",
        plugins: [
                    "table save",
                    "paste"
                 ],
        table_default_attributes: {
            class: "table table-bordered"
        },
        paste_remove_styles_if_webkit: false
    });
    /*end of tinymce*/

    $(document).ready(function(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
         if(dd<10){
                dd='0'+dd
            } 
            if(mm<10){
                mm='0'+mm
            } 

        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("dateMinToday").setAttribute("min", today); //set min
    });/*input date min today*/

    $(document).ready(function() { //SELECT2
        $(".selectUserList").select2({
            //placeholder: 'Select onbehalf'
        });
    });

    $(document).ready(function() { //CHECKBOX DISABLED ENABLED
       $("#checkOnBehalf").change(function() { 
            if (this.checked) {
                changeBehalf(); 
            }else{
                $("#userlist").attr("disabled", true);
            }
       });
    });

    $(document).ready(function(){
        $("#mainCategory").change(function(event){
            event.preventDefault();
            changeValue();
        });
        $("#onbehalf").change(function(event){
            event.preventDefault();
            if($("#onbehalf").prop("checked") == true){
                changeBehalf();
            } else {
                $("#userlist").attr("disabled", true);
            }
        });
        $("#addFileButton").click(function(event){
            event.preventDefault();
            num_files = $("#addFile input:file").length;
            if(num_files < 5){
                $("#addFile").append("<input name=\"image_upload[]\" type=\"file\" class=\"form-control\" accept=\"image/jpeg\">");
            }
        });
        $("#submitForm").submit(function(event) { //Event on submit
            tinyMCE.triggerSave(false, true); //Save content to textarea
            var text=$("#ticket-content").val(); //get content from textarea
            tinyMCE.activeEditor.setContent($.base64("encode", text));
            tinyMCE.triggerSave(false, true);
         });
    });/*end of document*/

})