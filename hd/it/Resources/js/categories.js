$(document).ready(function(){
    $(".edit-category").click(function(event){
        event.preventDefault();
        classid = this.id;
        editCategory(classid);
        getSubCategory(classid);
    });
    $(document).on("click", ".edit-sub-category", function(){
        event.preventDefault();
        dataid = this.getAttribute("data-id");
        editFormSubCategory(dataid);
    });
    $(document).on("click", ".edit-sub-category", function(){
        event.preventDefault();
        dataid = this.getAttribute("data-id");
        editFormSubCategory(dataid);
    });
    $(document).on("click", ".cancel-sub-category", function(){
        event.preventDefault();
        dataid = this.getAttribute("data-id");
        cancelFormSubCategory(dataid);
    });
    $("#editcategory").submit(function(event){
        // cancels the form submission
        event.preventDefault();
        submitEdit();
    });
    $("#addrowcategory").click(function(event){
        event.preventDefault();
        if($("#insertsub").hasClass("hidden")){
            $("#insertsub").removeClass("hidden");
        } else {
            $("#insertsub").addClass("hidden");
        }
    });
    $(document).on("submit", "#addsubcategory", function(event){
        // cancels the form submission
        event.preventDefault();
        var me = $(this);
        submitSubCategory(me);
    });
    $("#addcategory").submit(function(event){
        // cancels the form submission
        event.preventDefault();
        var me = $(this);
        submitCategory(me);
    });
    $("#editcategory").submit(function(event){
        // cancels the form submission
        event.preventDefault();
        var me = $(this);
        submitEditCategory(me);
    });
    $(".deleteconfirmationmodal").click(function(event){
        event.preventDefault();
        dataid = $(this).data("id");
        $("#deleteconfirm").attr("data-id", dataid);
    });
    $(document).on("click", "#deleteconfirm", function(){
        event.preventDefault();
        dataid = this.getAttribute("data-id");
        window.location.assign("delete_confirmation_"+dataid+".html");
    });
    $(document).on("click", ".delete-sub-category", function(){
        event.preventDefault();
        dataid = this.getAttribute("data-id");
        $("#deleteconfirm").attr("data-id", dataid);
    });
});