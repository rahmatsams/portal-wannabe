<?php 
/* @var $this Controller */ 
    $menu = array();
    if($_device->isMobile()){
        if(!$_option['admin']){
            $menu[0]['name'] = $_mySession['user_name'];
            $menu[0]['url'] = 'my_account.html';
            $menu[1]['name'] = 'Ticket Center';
            $menu[1]['url'] = 'index.html';
            $menu[2]['name'] = 'Create Ticket';
            $menu[2]['url'] = 'create_ticket.html';
            $menu[3]['name'] = 'Logout';
            $menu[3]['url'] = getConfig('base_domain').'user/logout';
        }else{
            $menu[0]['name'] = 'Administrator';
            $menu[0]['url'] = 'ticket_admin.html';
            $menu[1]['name'] = 'Ticket Center';
            $menu[1]['url'] = 'index.html';
            $menu[2]['name'] = 'Create Ticket';
            $menu[2]['url'] = 'create_ticket.html';
            $menu[3]['name'] = 'Logout';
            $menu[3]['url'] = getConfig('base_domain').'user/logout';
        }
    }else{
       
        $menu[0]['name'] = $_mySession['user_name'];
        $menu[0]['url'] = 'my_account.html';
        $menu[3]['name'] = 'Logout';
        $menu[3]['url'] = getConfig('base_domain').'user/logout';
       
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    
    <title>IT Helpdesk</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?="$_resource_css/bootstrap.min.css"?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?="$_resource_css/dashboard.css"?>" rel="stylesheet">
    <link href="<?="$_template_css/style.css"?>" rel="stylesheet">
    <?=(isset($_option['bdatepick_css']) && $_option['bdatepick_css'] == 1) ? "<link href=\"{$_resource_css}/bootstrap-datepicker3.min.css\" rel=\"stylesheet\">" : ''?>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="Resources/css/ie8-responsive-file-warning.js"?>"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?=getConfig('base_domain')?>assets/modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=getConfig('base_domain')?>assets/modules/select2/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?=getConfig('base_domain')?>assets/modules/select2/css/select2-bootstrap4.min.css">
    <!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
    <link rel="shortcut icon" href="Resources/images/Ico/favicon.png">
</head>

<body>
    <style type="text/css">
        .button2 {
          background-color: #222;
          border: none;
          color: #ffffff;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 14px;
          cursor: pointer;
        }
        .glyphicon{
          color: #A9A9A9;  
        }
        .container-fluid{
            padding-left: 0px;
            padding-right: 0px;
        }
    </style>
    <div class="container-fluid">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?=getConfig('base_domain')?>"><span class="glyphicon glyphicon-repeat"></span></a>
              <a class="navbar-brand" href="<?=getConfig('base_url')?>">Sushi Tei IT Helpdesk</a>
            </div>
        <?php if(count($menu)> 0):?>
            
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                <?php foreach($menu as $displayed):?>
                    <li><a href="<?=$displayed['url']?>"><b><?=$displayed['name']?></b></a></li>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php endforeach;?>
                
                </ul>
            </div>
        <?php endif;?>
        </nav>
        <div class="col-sm-12 col-xs-12" style="background-color: #f5f5f5;">
            <div class="row row-down">
                <?php
                    
                    $menu_list[0] = array(
                        'name' => 'Ticket Admin',
                        'url' => getConfig('base_url').'ticket_admin.html', 
                        'icon' => '<i class="fa fa-cog"></i>'
                    );
                    $menu_list[1] = array(
                        'name' => 'Ticket Center',
                        'url' => getConfig('base_url').'index.html',
                        'icon' => '<i class="fa fa-list-alt"></i>'
                    );
                    $menu_list[2] = array(
                        'name' => 'Submit Ticket',
                        'url' => getConfig('base_url').'create_ticket.html',
                        'icon' => '<i class="fa fa-pencil-square-o"></i>'
                    );
                    $menu_list[3] = array(
                        'name' => 'Logout User',
                        'url' => getConfig('base_domain').'user/logout',
                        'icon' => '<i class="fa fa-sign-out"></i>'
                    );
                    if(!$_option['admin']){
                        unset($menu_list[0]);
                    }
                
                    if(!empty($menu_list)):?>
                
                <div class="col-sm-2 col-xs-12 sidebar">
                    <ul class="nav nav-sidebar">
                        <?php foreach($menu_list as $l): ?>
                            <?php if($l['url'] == "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"): ?>
                            
                                <li class="active"><a href="<?=$l['url']?>"><?=$l['icon']?> <?=$l['name']?><span class="sr-only"></span></a></li>
                                
                            <?php else: ?>
                            
                                <li style="padding-left: 1.2em;"><a href="<?=$l['url']?>"><?=$l['icon']?>  <?=$l['name']?></a></li>
                            
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-sm-10 col-xs-12 main" style="padding-left: 1.2em; padding-right: 1.2em; background-color: #fff;">
                    <?=$this->view($_action,$_passed_var)?>
                </div>
                <?php else: ?>
                    
                <div class="main">
                    <?=$this->view($_action,$_passed_var)?>
                </div>
                    
                <?php endif; ?>
                
            </div>
        </div>
    </div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="<?=$_resource_js?>/jquery-1.12.3.min.js"></script>
    <?=(isset($_option['tinymce']) && $_option['tinymce'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/tinymce/tinymce.min.js\"></script>\n" : ''?>
    <?=(isset($_option['personal_js']) && $_option['personal_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/script.js\"></script>\n" : ''?>
    <?=(isset($_option['bdatepick_js']) && $_option['bdatepick_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/bootstrap-datepicker.min.js\"></script>\n" : ''?>
    <?=(isset($_option['trumbow']) && $_option['trumbow'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/trumbowyg.min.js\"></script>\n<script type=\"text/javascript\" src=\"{$_resource_js}/plugins/table/trumbowyg.table.js\"></script>\n" : ''?>
    <?=(isset($_option['scriptload'])  ? $_option['scriptload'] : '')?>
    
    <script>window.jQuery || document.write('<script src="<?="{$_resource_js}/vendor/jquery.min.js"?>"><\/script>')</script>
    <script type="text/javascript" src="<?="$_resource_js/bootstrap.min.js"?>"></script>
    <script type="text/javascript" src="<?=getConfig('base_domain')?>assets/modules/select2/js/select2.min.js"></script>
</body>
</html>
