<?php 
/* @var $this Controller */ 
    $menu = array();
    if($_device->isMobile()){
        if(!$_option['admin']){
            $menu[0]['name'] = $_mySession['user_name'];
            $menu[0]['url'] = 'my_account.html';
            $menu[1]['name'] = 'Ticket Center';
            $menu[1]['url'] = 'index.html';
            $menu[2]['name'] = 'Create Ticket';
            $menu[2]['url'] = 'create_ticket.html';
            $menu[3]['name'] = 'Logout';
            $menu[3]['url'] = getConfig('base_domain').'user/logout';
        }else{
            $menu[0]['name'] = 'Administrator';
            $menu[0]['url'] = 'ticket_admin.html';
            $menu[1]['name'] = 'Ticket Center';
            $menu[1]['url'] = 'index.html';
            $menu[2]['name'] = 'Create Ticket';
            $menu[2]['url'] = 'create_ticket.html';
            $menu[3]['name'] = 'Logout';
            $menu[3]['url'] = getConfig('base_domain').'user/logout';
        }
    }else{
       
        $menu[0]['name'] = $_mySession['user_name'];
        $menu[0]['url'] = 'my_account.html';
        $menu[3]['name'] = 'Logout';
        $menu[3]['url'] = getConfig('base_domain').'user/logout';
       
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    
    <title>IT Helpdesk</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?=getConfig('base_domain')."assets/modules/bootstrap-4.3.1/css/bootstrap.min.css"?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?="$_resource_css/dashboard.css"?>" rel="stylesheet">
    <link href="<?="$_template_css/style.css"?>" rel="stylesheet">
    <?=(isset($_option['bdatepick_css']) && $_option['bdatepick_css'] == 1) ? "<link href=\"{$_resource_css}/bootstrap-datepicker3.min.css\" rel=\"stylesheet\">" : ''?>
    
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="Resources/css/ie8-responsive-file-warning.js"?>"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="Resources/images/Ico/favicon.png">
</head>

<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
        <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="#">Sign out</a>
            </li>
        </ul>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <span data-feather="home"></span>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file"></span>
                  Orders
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="shopping-cart"></span>
                  Products
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="users"></span>
                  Customers
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="bar-chart-2"></span>
                  Reports
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="layers"></span>
                  Integrations
                </a>
              </li>
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Saved reports</span>
              <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
                <span data-feather="plus-circle"></span>
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Current month
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Last quarter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Social engagement
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Year-end sale
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <?=$this->view($_action, $_passed_var);?>
        </main>
      </div>
    </div>
    <!--
    <div class="container">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="http://helpdesk.sushitei.co.id/">Sushi Tei IT Helpdesk</a>
            </div>
            <?php if(count($menu)> 0):?>
            
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                <?php foreach($menu as $displayed):?>
                
                    <li><a href="<?=$displayed['url']?>"><?=$displayed['name']?></a></li>
                <?php endforeach;?>
                
                </ul>
            </div>
            <?php endif;?>
        </nav>
        <div class="container-fluid">
            <div class="row row-down">
                <?php
                    if($_option['admin']){
                        $menu_list = array(
                            'Ticket Admin' => getConfig('base_url').'ticket_admin.html',
                            'Ticket Center' => getConfig('base_url').'index.html',
                            'Submit Ticket' => getConfig('base_url').'create_ticket.html',
                            'Logout' => getConfig('base_url').'logout.html'
                        );
                        
                    } else{
                        $menu_list = array(
                            'Ticket Center' => getConfig('base_url').'index.html',
                            'Submit Ticket' => getConfig('base_url').'create_ticket.html',
                            'Logout' => getConfig('base_url').'logout.html'
                        );
                    }
                    if(isset($menu_list)){
                        echo "
                <div class=\"col-sm-2 sidebar\">
                    <ul class=\"nav nav-sidebar\">
                        ";
                        foreach($menu_list as $menu => $url){
                            if($url == "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"){
                                $link = "<li class=\"active\"><a href=\"{$url}\">{$menu}<span class=\"sr-only\">(current)</span></a></li>\n\t\t\t\t\t";
                            }else{
                                $link = "<li><a href=\"{$url}\">{$menu}</a></li>\n\t\t\t\t\t";
                            }
                            echo $link;
                        }
                    echo '
                    </ul>
                </div>
                <div class="col-sm-10 main">
                    ';
                    }else{
                        echo '
                        <div class="main">';
                    }
                
    $this->view($_action,$_passed_var);
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
    -->
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="<?=getConfig('base_domain')."assets/modules/jquery/jquery-3.3.1.min.js"?>"></script>
    <script type="text/javascript" src="<?=getConfig('base_domain')."assets/modules/bootstrap-4.3.1/js/bootstrap.min.js"?>"></script>
    <?=(isset($_option['tinymce']) && $_option['tinymce'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/tinymce/tinymce.min.js\"></script>\n" : ''?>
    <?=(isset($_option['personal_js']) && $_option['personal_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/script.js\"></script>\n" : ''?>
    <?=(isset($_option['bdatepick_js']) && $_option['bdatepick_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/bootstrap-datepicker.min.js\"></script>\n" : ''?>
    <?=(isset($_option['trumbow']) && $_option['trumbow'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/trumbowyg.min.js\"></script>\n<script type=\"text/javascript\" src=\"{$_resource_js}/plugins/table/trumbowyg.table.js\"></script>\n" : ''?>
    <?=(isset($_option['scriptload'])  ? $_option['scriptload'] : '')?>
</body>
</html>
