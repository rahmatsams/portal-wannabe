-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2019 at 06:59 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sushitei_rawmat`
--

-- --------------------------------------------------------

--
-- Table structure for table `sample_status`
--

CREATE TABLE `sample_status` (
  `status_id` tinyint(4) NOT NULL,
  `status_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sample_status`
--

INSERT INTO `sample_status` (`status_id`, `status_name`) VALUES
(1, 'Open'),
(2, 'FNB Approve'),
(3, 'FNB Reject'),
(4, 'QA Approve'),
(5, 'QA Reject'),
(6, 'KAHI Release'),
(7, 'KAHI Reject');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_document`
--

CREATE TABLE `ticket_document` (
  `document_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL,
  `document_type` tinyint(1) NOT NULL,
  `document_name` varchar(50) NOT NULL,
  `document_ext` varchar(10) NOT NULL,
  `document_location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_link`
--

CREATE TABLE `ticket_link` (
  `link_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_link`
--

INSERT INTO `ticket_link` (`link_id`, `supplier_id`, `producer_id`) VALUES
(1, 1, 4),
(2, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_producer`
--

CREATE TABLE `ticket_producer` (
  `producer_id` int(11) NOT NULL,
  `producer_name` varchar(100) NOT NULL,
  `producer_address` text NOT NULL,
  `producer_phone` varchar(15) NOT NULL,
  `producer_pic` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_producer`
--

INSERT INTO `ticket_producer` (`producer_id`, `producer_name`, `producer_address`, `producer_phone`, `producer_pic`) VALUES
(3, 'PT Tes', 'Jl Tes Coba No 1', '021456789', 'Fina'),
(4, 'PT Halo', 'Jl Halo No 2 Jakarta', '021333444', 'Pak Halo'),
(5, 'PT Test Lagi 3', 'Jl test tes 3', '021545678', 'tes pic 3');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_request`
--

CREATE TABLE `ticket_request` (
  `request_id` int(11) NOT NULL,
  `request_name` varchar(100) NOT NULL,
  `request_reason` varchar(50) NOT NULL,
  `request_date` datetime NOT NULL,
  `deadline_date` datetime NOT NULL,
  `request_user` smallint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_request`
--

INSERT INTO `ticket_request` (`request_id`, `request_name`, `request_reason`, `request_date`, `deadline_date`, `request_user`) VALUES
(8, 'test raw material request pertama', 'promo', '2019-07-09 00:00:00', '2019-07-17 00:00:00', 6520),
(9, 'test', 'promo', '2019-07-09 14:10:22', '2019-07-24 00:00:00', 6520);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_sample`
--

CREATE TABLE `ticket_sample` (
  `sample_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `sample_name` varchar(50) NOT NULL,
  `link_id` int(11) NOT NULL,
  `fnb_status` tinyint(4) NOT NULL,
  `qa_status` tinyint(4) NOT NULL,
  `kahi_status` tinyint(4) NOT NULL,
  `fnb_reason` varchar(50) NOT NULL,
  `qa_reason` varchar(50) NOT NULL,
  `kahi_reason` varchar(50) NOT NULL,
  `fnb_date` datetime NOT NULL,
  `qa_date` datetime NOT NULL,
  `kahi_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_supplier`
--

CREATE TABLE `ticket_supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` text NOT NULL,
  `supplier_phone` varchar(15) NOT NULL,
  `supplier_pic` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_supplier`
--

INSERT INTO `ticket_supplier` (`supplier_id`, `supplier_name`, `supplier_address`, `supplier_phone`, `supplier_pic`) VALUES
(1, 'test sup', 'jl sup', '0212222', 'tes ffina'),
(2, 'test sup 2', 'Jl sup 2', '021657986', 'sup 3');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rawmat`
-- (See below for the actual view)
--
CREATE TABLE `view_rawmat` (
`request_id` int(11)
,`request_name` varchar(100)
,`request_date` datetime
,`deadline_date` datetime
,`sample_id` int(11)
,`sample_name` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `view_rawmat`
--
DROP TABLE IF EXISTS `view_rawmat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rawmat`  AS  (select `ticket_request`.`request_id` AS `request_id`,`ticket_request`.`request_name` AS `request_name`,`ticket_request`.`request_date` AS `request_date`,`ticket_request`.`deadline_date` AS `deadline_date`,`ticket_sample`.`sample_id` AS `sample_id`,`ticket_sample`.`sample_name` AS `sample_name` from (`ticket_sample` left join `ticket_request` on((`ticket_sample`.`request_id` = `ticket_request`.`request_id`)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sample_status`
--
ALTER TABLE `sample_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `ticket_document`
--
ALTER TABLE `ticket_document`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `ticket_link`
--
ALTER TABLE `ticket_link`
  ADD PRIMARY KEY (`link_id`);

--
-- Indexes for table `ticket_producer`
--
ALTER TABLE `ticket_producer`
  ADD PRIMARY KEY (`producer_id`);

--
-- Indexes for table `ticket_request`
--
ALTER TABLE `ticket_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `ticket_supplier`
--
ALTER TABLE `ticket_supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sample_status`
--
ALTER TABLE `sample_status`
  MODIFY `status_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ticket_document`
--
ALTER TABLE `ticket_document`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_link`
--
ALTER TABLE `ticket_link`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ticket_producer`
--
ALTER TABLE `ticket_producer`
  MODIFY `producer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ticket_request`
--
ALTER TABLE `ticket_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ticket_supplier`
--
ALTER TABLE `ticket_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
