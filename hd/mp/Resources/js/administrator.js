/*DELETE SAMPLE LIST*/
jQuery(document).ready(function(){
    jQuery(".admin_delete_list").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#delete_list").attr("href", "delete_list_"+id+".html"); 
    });
});

/*DELETE DOCUMENT TYPE*/
jQuery(document).ready(function(){
    jQuery(".admin_delete_doc").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#delete_doc").attr("href", "delete_document_"+id+".html"); 
    });
});

/*DELETE PRODUCER*/
jQuery(document).ready(function(){
    jQuery(".admin_delete_producer").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#delete_producer").attr("href", "delete_producer_"+id+".html"); 
    });
});

// DELETE SUPPLIER
jQuery(document).ready(function(){
    jQuery(".admin_delete_supplier").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#delete_supplier").attr("href", "delete_supplier_"+id+".html"); 
    });
});

// DELETE SAMPLE STATUS
jQuery(document).ready(function(){
    jQuery(".admin_delete_status").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#delete_status").attr("href", "delete_status_"+id+".html"); 
    });
});

// DELETE SAMPLE CONDITION
jQuery(document).ready(function(){
    jQuery(".admin_delete_condition").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#delete_condition").attr("href", "delete_condition_"+id+".html"); 
    });
});

// DELETE REASON
jQuery(document).ready(function(){
    jQuery(".admin_delete_reason").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#delete_reason").attr("href", "delete_reason_"+id+".html"); 
    });
});