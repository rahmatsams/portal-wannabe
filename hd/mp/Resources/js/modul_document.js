document.addEventListener("DOMContentLoaded",function(){
    var elem = document.getElementById("selectDocument");
    elem.onchange = function(){
        console.log('Onchange ok');
        var sh = document.getElementsByClassName("sh-form");
        var shinput = document.getElementsByClassName("sh-input");
        if(this.value == 1){
            for(var i = 0; i < sh.length; i++)
            {
                sh[i].classList.remove('d-none');                
            }
            for(var i = 0; i < shinput.length; i++)
            {
                shinput[i].required = true;
            }
        }else{
            for(var i = 0; i < sh.length; i++)
            {
                sh[i].classList.add('d-none');
            }
            for(var i = 0; i < shinput.length; i++)
            {
                shinput[i].required = false;
            }
        }
    };
});