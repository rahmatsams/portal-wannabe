
$(document).ready(function(){
    var proccess = 0;
    $("#selectSupplier select[name='supplier']").select2({
        allowClear:true,
        placeholder: 'Select Supplier',
        theme: 'bootstrap4'
    });
    $("#selectManufacturer select[name='producer']").select2({
        allowClear:true,
        placeholder: 'Select Manufacturer',
        theme: 'bootstrap4'
    });
    /*
    $('#selectSupplier').change(function(e){
        event.preventDefault();
        if(this.value){
            if(proccess == 1){
                return false;
            }
            proccess = 1;
            $(this).prop('disable', true);
            $.ajax({
                type: "POST",
                url: "ajax_get_manufacturer.html",
                data: "supplier="+this.value,
                dataType: 'json',
                error: function (request, status, error) {
                    console.log(request.responseText);
                },
                success: function(data){
                    if(data.success){
                        $('#selectManufacturer').html('');
                        $.each(data.manufacturer, function(i, item) {
                            var newOption = new Option(item.producer_name, item.producer_id, false, false);
                            $('#selectManufacturer').append(newOption).trigger('change');
                        });
                        $('#selectManufacturer').prop('disabled', false);
                    } else {
                        console.log('Error');
                    }
                },
                complete: function(){
                    proccess = 0;
                    $(this).prop('disable', false);
                }
            });
        }
    });
    */
    $('#showDocument').click(function(e){
        event.preventDefault();
        $('#documentUpload').toggleClass('d-none');
    });
    
    $("#manIsNew").change(function() {
        if(this.checked) {
            $("#newManufacturer").removeClass('d-none');
            $("#newManufacturer input[name='manufacturer_name']").prop('disabled', 0);
            $("#newManufacturer input[name='manufacturer_name']").prop('required', 1);
            $("#selectManufacturer").addClass('d-none');
            $("#selectManufacturer select[name='producer']").prop('disabled', 1);
            $("#selectManufacturer select[name='producer']").prop('required', 0);
        }else{
            $("#newManufacturer").addClass('d-none');
            $("#newManufacturer input[name='manufacturer_name']").prop('disabled', 1);
            $("#newManufacturer input[name='manufacturer_name']").prop('required', 0);
            $("#selectManufacturer").removeClass('d-none');
            $("#selectManufacturer select[name='producer']").prop('disabled', 0);
            $("#selectManufacturer select[name='producer']").prop('required', 1);
        }
    });
    $("#supIsNew").change(function() {
        if(this.checked) {
            $("#newSupplier").removeClass('d-none');
            $("#newSupplier input[name='supplier_name']").prop('disabled', 0);
            $("#newSupplier input[name='supplier_name']").prop('required', 1);
            $("#selectSupplier").addClass('d-none');
            $("#selectSupplier select[name='supplier']").prop('disabled', 1);
            $("#selectSupplier select[name='supplier']").prop('required', 0);
        }else{
            $("#newSupplier").addClass('d-none');
            $("#newSupplier input[name='supplier_name']").prop('disabled', 1);
            $("#newSupplier input[name='supplier_name']").prop('required', 0);
            $("#selectSupplier").removeClass('d-none');
            $("#selectSupplier select[name='supplier']").prop('disabled', 0);
            $("#selectSupplier select[name='supplier']").prop('required', 1);
        }
    });
    $("#newSample").submit(function(e){
        e.preventDefault();
        var me =  $(this);
        if ( me.data('requestRunning') ) {
            return;
        }
        var form = $('#newSample')[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        me.data('requestRunning', true);
        
        $.ajax({
            type: "POST",
            dataType: "json",
            data: formData,
            contentType: false,
            processData: false,
            success: function(resp){
                if(resp.success == 1){
                    $('#newSample').trigger("reset");
                    $('#centralModalSuccess').modal('show');
                    console.log('success');
                } else if(resp.success == 0) {
                    alert('Failed');
                    console.log('failed');
                } else {
                    alert('Unknown error.');
                }
            },
            complete: function() {
                me.data('requestRunning', false);
                console.log('complete');
            },
        });
    });
});