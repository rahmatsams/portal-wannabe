<?php
/*
*   Load Class for load Model, View, Template, Library
*/
class Load
{
    protected $_input = array();
    
    protected $controller = null;
    
    protected $models = array();
    
    protected $libraries = array();
    
    protected $templates;
    
    protected $view;
    
    // Construct class using name of loaded controller
    public function __construct($controller)
    {
        $this->controller = $controller;
    }

    public function setHtmlHeader($option)
    {
        if(isset($option['js'])){
            $this->_header .= "<script type=\"text/javascript\">{$option['js']}</script>\n";
        }elseif(isset($option['jsfile'])){
            $this->_header .= "<script type=\"text/javascript\" src=\"{$option['jsfile']}\"></script>\n";
        }
    }
    
    public function setTemplate($name){
        $this->template = $name;
    }
    
    public function template($_action, $_passed_var = null, $_option = null)
    {
        $_template_path = getConfig('templates_dir') .  __DS__ . (!empty($this->templates) ? $this->templates : getConfig('default_template'));
        if (file_exists($_template_path . __DS__ . 'template.php')) {
			$css = $_template_path. '/css';
			$js = $_template_path. '/js';
			$img = $_template_path. '/images';
            $res_css = getConfig('resource_dir') . '/' . getConfig('css_folder');
            $res_image = getConfig('resource_dir') . '/' . getConfig('image_folder');
            $res_js = getConfig('resource_dir') . '/' . getConfig('js_folder');
            $res_media = getConfig('resource_dir') . '/' . getConfig('media_folder');
            if(is_array($_passed_var) && count($_passed_var) > 0){
                extract($_passed_var, EXTR_PREFIX_SAME, 'pre');
            }
            include_once($_template_path . __DS__ . 'template.php');
        } else {
            exit ("Template Not Found");
        }
    }
    
    // Load view page
    public function view($view, $passed_var=array(), $controller=null)
    {
        if(file_exists(__APPLICATIONS_DIR . 'Views' . __DS__ .
            ($controller != '' ? $controller . __DS__  : ''). $view .'.php')) {
            
            ob_start();
            
            if(is_array($passed_var) && count($passed_var) > 0){
                extract($passed_var, EXTR_PREFIX_SAME, 'pre');
            }
            
            include (__APPLICATIONS_DIR . 'Views' . __DS__ .
                        ($controller != '' ? $controller . __DS__  : ''). $view .'.php');
            $this->view = ob_get_contents();
            
            ob_end_clean();
            
           return $this->view;
            
        } else {
            exit ('Cannot find View File' . $view);
        }
    }
    
    public function render()
    {
        echo $this->view;
    }
    
    // Functions to  Load Model
    public function model($name)
    {
        if(file_exists(__APPLICATIONS_DIR . 'Models' . __DS__ . 'Model' .$name.'.php')) {
            require_once(__APPLICATIONS_DIR . 'Models' . __DS__ . 'Model' . $name.'.php');
            if(isset($this->models[$name])){
                return $this->models[$name];
            }
            $name = ucwords($name);
            $name = "Model{$name}";
            $this->models[$name] = new $name();
            return $this->models[$name];
        }else{
            exit ("Model {$name} doesn't found");
        }
    }
    
    // Functions to get all Loaded Model
    public function getLoadedModels()
    {
        $models = array();
        foreach($this->models as $key => $value){
            array_push($models,$key);
        }
        return $models;
    }
    
    // Functions to Load Library
    public function lib($name, $arguments=array(), $new=0)
    {
        if(isset($this->library[$name])) {
            return $this->library[$name];
        }
        $name = ucwords($name);
        if(file_exists(__LIBS_DIR . $name .'.php')){
           
            require_once(__LIBS_DIR . $name .'.php');
            
            if($new < 1){
                switch(count($arguments)){
                    case 1:
                        $this->library[$name] = new $name($arguments[0]);
                        break;
                    case 2:
                        $this->library[$name] = new $name($arguments[0],$arguments[1]);
                        break;
                    default:
                        $this->library[$name] = new $name();
                }
            }
            if($new < 1){
                return $this->library[$name];
            }
        }else{
            exit ("Library {$name} doesn't found");
        }
    }
    
    // Functions to get all Loaded Library
    public function getLoadedLibs()
    {
        $libs = array();
        foreach($this->library as $key => $value){
            array_push($libs, $key);
        }
        return $libs;
    }
}
// End Class Load