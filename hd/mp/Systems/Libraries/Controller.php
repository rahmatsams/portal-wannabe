<?php
    class Controller extends SISTER{
        protected $_controller;
        protected $_action;
        protected $_init;
        protected $load;
        protected $session;
        protected $_header;
        protected $_accessRules;
        protected $_baseUrl;
        protected $site;
        protected $skipCheck = array();
        protected $cmodel;
        
        // Controller Class Constructor
        public function __construct($cons_con,$cons_act)
        {
            $this->sessionOn();
            $this->site = getConfig('site_number');
            $this->_controller = $cons_con;
            $this->_action = $cons_act;
            // Load - Load Class as child Function of this Controller
            $this->load = new Load($this->_controller);

            if(!isset($_SESSION) || !isset($_SESSION['group']) || !isset($_SESSION['__last_generate'])) {
                $this->setSession('user_name','Guest');
                $this->setSession('group','Guest');
                $this->setSession('attempt',0);
                $this->setSession('role',1);
            }
            $this->session = $this->getSession();
            
            if(method_exists($this, $cons_act)) {
                $allowAccess = 1;
                $this->_accessRules = (!empty($this->accessRules()) && is_array($this->accessRules()) ? $this->accessRules() : '');
                
                if($this->checkAccess() > 0) {
                    $this->_init = $this->$cons_act();
                } else {
                    $this->showError(3);
                }
              
            } else {
                $this->showError(2);
            }
            
            $this->setSession('last_url', "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
            if(count($this->getFlashData()) >= 1) {
                $flashdata = $this->getFlashData();
                ($flashdata['last_uri'] != $_SERVER['REQUEST_URI'] ? $this->unsetFlashData() : '');
            }
            
            
        }
        // Check Access
        protected function checkAccess()
        {
            $allowed = 0;
            if(isset($_SESSION['site']) && is_array($_SESSION['site']) && array_key_exists($this->site, $_SESSION['site'])){
                if(!isset($_SESSION['__last_generate'])){
                    $this->showError(4);
                }
                if(is_array($this->_accessRules)) {
                    foreach($this->_accessRules as $rules){
                        if($rules[0] == 'Allow'){
                            foreach($rules['actions'] as $rule){
                                if($rule == $this->_action && isset($rules['users']) && (in_array($this->session['user_name'], $rules['users'], true) || in_array('*', $rules['users'], true))){
                                    $allowed = 1;
                                } elseif($rule == $this->_action && isset($rules['groups']) && (in_array($this->session['group'], $rules['groups'], true) || in_array('*', $rules['groups'], true))){
                                    $allowed = 1;
                                }
                            }
                        } elseif($rules[0] == 'Deny') {
                            foreach($rules['actions'] as $rule){
                                if($rule == $this->_action && isset($rules['users']) && (in_array($this->session['user_name'], $rules['users'], true) || in_array('*', $rules['users'], true))){
                                    $allowed = 0;
                                } elseif($rule == $this->_action && isset($rules['groups']) && (in_array($this->session['group'], $rules['groups'], true) || in_array('*', $rules['groups'], true))){
                                    $allowed = 0;
                                }
                            }
                        }
                    }
                } else{
                    $allowed = 1;
                }
            }
            return $allowed;
        }
        
        
        // Show Controller Error
        protected function showError($code)
        {
            switch($code) {
                case 1:
                    exit(header("Location: ".getConfig('base_domain')));
                break;
                case 2:
                    exit($this->load->view('notfound',array(),'error'));
                break;
                case 3:
                    exit(header("Location: ".getConfig('base_domain')));
                break;
                case 4:
                    exit(header("Location: ".getConfig('base_domain')."/user/logout"));
                break;
                default:
                    exit(header("HTTP/1.0 404 Not Found"));
            }
        }
        protected function isGuest()
        {
            if($this->session['user_name'] == 'Guest') {
                return 1;
            } else {
                return 0;
            }
        }
        
        protected function isAdmin() #Administrator
        {
            if($this->session['site'][$this->site] == 1) {
                return 1;
            } else {
                return 0;
            }
        }

        protected function isPurchasing() #Admin Purcashing
        {
            $m = $this->checkControllerModel();
            $return = 0;
            foreach($m->getAllPurchasingRole() as $r){
                if($r['role_name'] == $this->session['group']){
                    $return = 1;
                }
            }
            
            return $return;
        }

        protected function isAdminQA() #Admin Purcashing
        {
            $m = $this->checkControllerModel();
            $return = 0;
            foreach($m->getAllQARole() as $r){
                if($r['role_name'] == $this->session['group']){
                    $return = 1;
                }
            }
            
            return $return;
        }

        protected function isAdminFNB() #Admin Purcashing
        {
            $m = $this->checkControllerModel();
            $return = 0;
            foreach($m->getAllFNBRole() as $r){
                if($r['role_name'] == $this->session['group']){
                    $return = 1;
                }
            }
            
            return $return;
        }

        protected function isKahi() #Admin Purcashing
        {
            $m = $this->checkControllerModel();
            $return = 0;
            foreach($m->getAllKAHIRole() as $r){
                if($r['role_name'] == $this->session['group']){
                    $return = 1;
                }
            }
            
            return $return;
        }
        
        protected function checkControllerModel()
        {
            if(empty($this->cmodel)){
                $this->cmodel = $this->load->model('Controller');
            }
            return $this->cmodel;
        }
        
        protected function setFlashData($input)
        {
            foreach($input as $data => $value) {
                $_SESSION['flash_data'][$data] = $value; 
            }
            $_SESSION['flash_data']['last_uri'] = $_SERVER["REQUEST_URI"];
        }
        protected function getFlashData()
        {
            if(isset($_SESSION['flash_data'])) {
                return $_SESSION['flash_data'];
            }else{
                return array();
            }
        }
        protected function unsetFlashData()
        {
            if(isset($_SESSION['flash_data'])) {
                unset($_SESSION['flash_data']);
            }
        }
        
        protected function setSession($name, $value = 0)
        {
            if(!$value && is_array($name)){
                foreach($name as $key => $val){
                    $_SESSION[$key] = $val;
                }
            }else{
                $_SESSION[$name] = $value;
            }
            
        }
        
        protected function unsetSession($name)
        {
            if(isset($_SESSION[$name])) {
                unset($_SESSION[$name]);
            }
        }
        
        protected function getSession($name = false) {
            if(!$name){
                if(isset($_SESSION)) {
                    return $_SESSION;
                }else{
                    return 0;
                }
            }else{
                if(isset($_SESSION[$name])) {
                    return $_SESSION[$name];
                }else{
                    return 0;
                }
            }
        }

        protected function sessionOn()
        {
            $time = time();
            $expired = (isset($_SESSION['remember']) && $_SESSION['remember'] == 1 ? 60*60*24*30 : 1200 );
            $this->regenerateID(0, 1, $expired);
            /* generate session time */

            if(isset($_SESSION['__last_generate']) && $time > $_SESSION['__last_generate']+$expired){
                $this->regenerateID(1);
            }else{
                $_SESSION['__last_generate'] = $time;
            }
        }

        protected function regenerateID($force = 0, $gc = 0, $lifetime = 0)
        {
            // Call session_create_id() while session is active to
            // make sure collision free.
            if (session_status() !== PHP_SESSION_ACTIVE && $force === 0) {
                session_start();
            }
            if($force === 1 || session_status() === PHP_SESSION_NONE){

                // WARNING: Never use confidential strings for prefix!
                $time = time();
                $newid = hash('ripemd160', "portalsushitei2019-{$_SERVER['REMOTE_ADDR']}{$time}");
                $this->session_id = $newid;
                // Set deleted timestamp. Session data must not be deleted immediately for reasons.
                $_SESSION['__last_generate'] = $time;
                // Finish session
                session_commit();
                // Make sure to accept user defined session ID
                // NOTE: You must enable use_strict_mode for normal operations.
                ini_set('session.use_strict_mode', 0);
                // Set new custom session ID
                session_id($newid);
                // Start with custom session ID
                if($gc){
                    ini_set('session.gc_maxlifetime', $lifetime);
                    session_start(['cookie_lifetime' => $lifetime]);
                }else{
                    session_start();
                }
            }

        }
        
    }
// End Controller Class