<?php 

/**
 * MODEL QA DOCUMENT
 */
class ModelQaDocument extends Model
{
    
    function newQaDocument($form)
    {

        try {
            $result = $this->insertQuery('ticket_document', $form);
            $this->lastInsertID = $this->lastInsertID();
            return 1;       
        } catch (Exception $e) {
            if (getConfig('development')  == 1) {
                echo $e;
                exit;
            }
            return 0;
        }
    }
    
    function modifySample($form, $where)
    {
        try {
            $result = $this->editQuery('ticket_sample', $form, $where);
            return 1;       
        } catch (Exception $e) {
            if (getConfig('development')  == 1) {
                echo $e;
                exit;
            }
            return 0;
        }
    }

    function newSampleTicketDocument($data)
    {

        try {
            $result = $this->insertQuery('ticket_document', $data);
            $this->lastInsertID = $this->lastInsertID();
            return 1;       
        } catch (Exception $e) {
            if (getConfig('development')  == 1) {
                echo $e;
                exit;
            }
            return 0;
        }
    }

    function modifySampleTicketDocument($data, $where)
    {
        try {
            $result = $this->editQuery('ticket_document', $data, $where);
            return 1;       
        } catch (Exception $e) {
            if (getConfig('development')  == 1) {
                echo $e;
                exit;
            }
            return 0;
        }
    }

    //cek document exist belum bikin
    function checkDocumentExist($input)
    {
        /*$query = "SELECT document_name, document_location FROM 
        ticket_document WHERE ticket_sample=:ticket_sample 
        AND document_type=:document_type";*/

        $query = "SELECT td.document_id,td.document_name, 
                td.document_location,
                dt.type_name
                FROM ticket_document td
            LEFT JOIN document_type dt ON dt.type_id=td.document_type
            WHERE td.ticket_sample=:ticket_sample 
            AND td.document_type=:type_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function deleteQaDocument($nama_doc)
    {
        $query = "DELETE FROM ticket_document WHERE document_id='$nama_doc'";
        return $this->doQuery($query);
    }

    function getDocumentForPrint($input)
    {
        $query = "SELECT type_id,type_name AS document_name,IF(ISNULL(ticket_document.document_name), '0', '1') AS uploaded, document_expired, document_issued,document_acknowledged FROM document_type LEFT JOIN ticket_document ON document_type.type_id=ticket_document.document_type AND ticket_document.ticket_sample=:sample_id WHERE type_id NOT IN (7,8) ORDER BY type_id ASC";
        $result = $this->fetchAllQuery($query, $input);
        return $result;
    }
}


 ?>