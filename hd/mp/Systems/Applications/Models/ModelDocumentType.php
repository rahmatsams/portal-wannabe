<?php 

/**
 * DOCUMENT TYPE
 */
class ModelDocumentType extends Model
{
    
    function getAllActiveDoc()
    {
        $query = "SELECT * FROM document_type ORDER BY type_name ASC";

        $result = $this->fetchAllQuery($query);
        return $result;
    }

    function getAllQaDoc($input)
    {
        $query = "SELECT dt.*,
            td.document_name 
            FROM document_type dt 
            LEFT JOIN (SELECT * FROM ticket_document WHERE ticket_sample=:sample_id) td ON td.document_type=dt.type_id 
            WHERE dt.type_id != 8
            ORDER BY dt.type_id ASC";
        
        $result = $this->fetchAllQuery($query, $input);
        return $result;
    }

    function getAllCountDoc($query_option)
    {
        $query = "SELECT * FROM document_type ";
        #$query .= "LEFT JOIN  ON store.location_id=location.location_id";
        $count_query = "SELECT COUNT(*) AS row_total FROM document_type";
        
        $result = $this->pagingQuery($query, $count_query, $query_option);
        return $result;
    }

    function newDocumentType($input)
    {
        $result = $this->insertQuery('document_type', $input);
        return $result;
    }

    function getDocumentBySampleId($input)
    {
        $query = "SELECT dt.*,td.document_name FROM document_type dt 
    LEFT JOIN (SELECT * FROM ticket_document WHERE ticket_sample=:sample_id) td ON td.document_type=dt.type_id 
    WHERE dt.type_id != 8 AND td.document_name IS NULL";
        
        $result = $this->fetchAllQuery($query, $input);
        return $result;
    }

    function getDocumentById($input)
    {
        $query = "SELECT * FROM document_type ";
        $query .= "WHERE type_id=:type_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function editDocument($form  = array(), $where = array())
    {
        $table = 'document_type';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }

    function deleteDocument($id)
    {
        $query = $this->doQuery("DELETE FROM document_type WHERE type_id=$id");
    }


}


 ?>