<?php 

/**
 * MODEL SUPPLIER
 */
class ModelController extends Model
{
	
	function getAllFNBRole()
	{
		$query = "SELECT role_name FROM sushitei_portal.role_permission rp LEFT JOIN sushitei_portal.user_role ur ON ur.role_id=rp.role WHERE permission IN (
                    SELECT permission_id FROM sushitei_portal.portal_permission WHERE permission_site=11 AND permission_code='isFNB'
                )";

		$result = $this->fetchAllQuery($query);
		return (!empty($result) ? $result : array());
	}

    function getAllQARole()
	{
		$query = "SELECT role_name FROM sushitei_portal.role_permission rp LEFT JOIN sushitei_portal.user_role ur ON ur.role_id=rp.role WHERE permission IN (
                    SELECT permission_id FROM sushitei_portal.portal_permission WHERE permission_site=11 AND permission_code='isQA'
                )";

		$result = $this->fetchAllQuery($query);
		return (!empty($result) ? $result : array());
	}

    function getAllPurchasingRole()
	{
		$query = "SELECT role_name FROM sushitei_portal.role_permission rp LEFT JOIN sushitei_portal.user_role ur ON ur.role_id=rp.role WHERE permission IN (
                    SELECT permission_id FROM sushitei_portal.portal_permission WHERE permission_site=11 AND permission_code='isPurchasing'
                )";

		$result = $this->fetchAllQuery($query);
		return (!empty($result) ? $result : array());
	}
    
    function getAllKAHIRole()
	{
		$query = "SELECT role_name FROM sushitei_portal.role_permission rp LEFT JOIN sushitei_portal.user_role ur ON ur.role_id=rp.role WHERE permission IN (
                    SELECT permission_id FROM sushitei_portal.portal_permission WHERE permission_site=11 AND permission_code='isKAHI'
                )";

		$result = $this->fetchAllQuery($query);
		return (!empty($result) ? $result : array());
	}

    

}


 ?>