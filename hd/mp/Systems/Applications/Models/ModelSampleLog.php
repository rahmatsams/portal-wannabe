<?php 

/**
 * MODEL SAMPLE LOG
 */
class ModelSampleLog extends Model 
{
    
    function newSampleLog($form)
    {

        try {
            $result = $this->insertQuery('sample_log', $form);
            $this->lastInsertID = $this->lastInsertID();
            return 1;       
        } catch (Exception $e) {
            if (getConfig('development')  == 1) {
                echo $e;
                exit;
            }
            return 0;
        }
    }

    function getSampleById($input)
    {
        $query = "SELECT sample_id FROM ticket_sample WHERE sample_id=:sample_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result; 
    }


}


 ?>