<?php 

/**
 * MODEL SUPPLIER
 */
class ModelSupplier extends Model
{
	
	function getAllActiveSupplier()
	{
		$query = "SELECT * FROM ticket_supplier";

		$result = $this->fetchAllQuery($query);
		return $result;
	}

    function getSupplierById($input)
    {
        $query = "SELECT * FROM ticket_supplier ";
        $query .= "WHERE supplier_id=:supplier_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;

    }

    function getAllCountSupplier($query_option)
    {
        $query = "SELECT * FROM ticket_supplier ";

        $count_query = "SELECT COUNT(*) AS row_total FROM ticket_supplier";
        
        $result = $this->pagingQuery($query, $count_query, $query_option);
        return $result;
    }

    function deleteSupplier($id)
    {
        $query = $this->doQuery("DELETE FROM ticket_supplier WHERE supplier_id=$id");
    }

    function newSupplier($input)
    {
    	$result = $this->insertQuery('ticket_supplier', $input);
		return $result;
    }

    function newLink($input)
    {
        $result = $this->insertQuery('ticket_link', $input);
        return $result;
    }

    function getActiveSupplierLastInsert()
    {
        $query = "SELECT * FROM ticket_supplier ORDER BY supplier_id DESC LIMIT 1";

        $result = $this->fetchSingleQuery($query);
        return $result;
    }

    function editSupplier($form = array(), $where = array())
    {
        $table  = 'ticket_supplier';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }

    function editLink($form = array(), $where = array())
    {
        $table  = 'ticket_link';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }

    function getSupplierByLinkId($input)
    {
        $query = "SELECT 
                    ts.supplier_id AS supplier_id, 
                    ts.supplier_name, 
                    ts.supplier_address, 
                    ts.supplier_phone, 
                    ts.supplier_pic
                    tp.producer_id AS producer_id, 
                    tp.producer_name AS producer_name, 
                    tl.link_id AS link_id
                FROM ticket_supplier ts 
                LEFT JOIN ticket_link tl ON ts.supplier_id=tl.supplier_id
                LEFT JOIN ticket_producer tp ON tl.producer_id=tp.producer_id ";
        $query .= "WHERE ts.supplier_id=:supplier_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function getProducerbySupplier($input)
    {
        $query = "
                SELECT 
                    tp.producer_id,
                    tp.producer_name, 
                    IF(tl.link_id IS NOT NULL,'1','0') AS checked 
                FROM ticket_producer tp 
                    LEFT JOIN (SELECT * FROM ticket_link WHERE supplier_id=:supplier_id) AS tl 
                    ON tl.producer_id=tp.producer_id";
        $result = $this->fetchAllQuery($query, $input);
        return $result;
    }

    function getSupplierByLink()
    {
        $query = "SELECT 
                    ts.supplier_id, 
                    ts.supplier_name, 
                    ts.supplier_address, 
                    ts.supplier_phone, 
                    ts.supplier_pic, 
                    tp.producer_name AS producer_name, 
                    tl.link_id 
                FROM ticket_supplier ts 
                LEFT JOIN ticket_link tl ON ts.supplier_id=tl.supplier_id
                LEFT JOIN ticket_producer tp ON tl.producer_id=tp.producer_id";
        $result = $this->fetchAllQuery($query);
        return $result;
    }

}


 ?>