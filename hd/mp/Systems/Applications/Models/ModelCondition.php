<?php 

/**
 * SAMPLE CONDITION
 */
class ModelCondition extends Model
{
    
    function getAllActiveCondition()
    {
        $query = "SELECT * FROM sample_condition";

        $result = $this->fetchAllQuery($query);
        return $result;
    }

    function getAllCountCondition($query_option)
    {
        $query = "SELECT * FROM sample_condition ";
        #$query .= "LEFT JOIN  ON store.location_id=location.location_id";
        $count_query = "SELECT COUNT(*) AS row_total FROM sample_condition";
        
        $result = $this->pagingQuery($query, $count_query, $query_option);
        return $result;
    }

    function newCondition($input)
    {
        $result = $this->insertQuery('sample_condition', $input);
        return $result;
    }

    function deleteCondition($id)
    {
        $query = $this->doQuery("DELETE FROM sample_condition WHERE condition_id=$id");
    }

    function getConditionById($input)
    {
        $query = "SELECT * FROM sample_condition ";
        $query .= " WHERE condition_id=:condition_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function editCondition($form  = array(), $where = array())
    {
        $table = 'sample_condition';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }


}


 ?>