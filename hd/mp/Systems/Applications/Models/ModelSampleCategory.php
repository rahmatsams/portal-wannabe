<?php 

/**
 * MODEL SAMPLE CATEGORY
 */
class ModelSampleCategory extends Model
{
    
    function getAllActiveCategory()
    {
        $query = "SELECT * FROM sample_category";

        $result = $this->fetchAllQuery($query);
        return $result;
    }

    function getAllCountCategory($query_option)
    {
        $query = "SELECT * FROM sample_category ";
        #$query .= "LEFT JOIN  ON store.location_id=location.location_id";
        $count_query = "SELECT COUNT(*) AS row_total FROM sample_category";
        
        $result = $this->pagingQuery($query, $count_query, $query_option);
        return $result;
    }

    function newSampleCategory($input)
    {
        $result = $this->insertQuery('sample_category', $input);
        return $result;
    }

    function deleteCategory($id)
    {
        $query = $this->doQuery("DELETE FROM sample_category WHERE category_id=$id");
    }

    function getCategoryById($input)
    {
        $query = "SELECT * FROM sample_category ";
        $query .= "WHERE category_id=:category_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function editCategory($form  = array(), $where = array())
    {
        $table = 'sample_category';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }


}


 ?>