<?php 

/**
 * MODEL PRODUCER
 */
class ModelProducer extends Model
{
	
	function getAllActiveProducer()
	{
		$query = "SELECT * FROM ticket_producer ORDER BY producer_name";

		$result = $this->fetchAllQuery($query);
		return $result;
	}

	function getAllCountProducer($query_option)
    {
        $query = "SELECT * FROM ticket_producer ";
        #$query .= "LEFT JOIN  ON store.location_id=location.location_id";
        $count_query = "SELECT COUNT(*) AS row_total FROM ticket_producer";
        
        $result = $this->pagingQuery($query, $count_query, $query_option);
        return $result;
    }

    function newProducer($input)
    {
    	$result = $this->insertQuery('ticket_producer', $input);
		return $result;
    }

    function deleteProducer($id)
    {
        $query = $this->doQuery("DELETE FROM ticket_producer WHERE producer_id=$id");
    }

    function getProducerById($input)
    {
        $query = "SELECT * FROM ticket_producer ";
        $query .= "WHERE producer_id=:producer_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }
    
    function getProducerBySupplier($input)
    {
        $query = "SELECT tp.producer_id, tl.link_id AS id, tp.producer_name AS name FROM ticket_link tl LEFT JOIN ticket_producer tp USING(producer_id) ";
        $query .= "WHERE supplier_id=:supplier GROUP BY producer_id";
        $result = $this->fetchAllQuery($query, $input);
        return $result;
    }

    function editProducer($form  = array(), $where = array())
    {
        $table = 'ticket_producer';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }

    function getActiveProducerLastInsert()
    {
        $query = "SELECT * FROM ticket_producer ORDER BY producer_id DESC LIMIT 1";

        $result = $this->fetchSingleQuery($query);
        return $result;
    }


}


 ?>