<?php
/*
* Model for TicketActions
*/
class ModelActions extends Model {

	function getActionById($input)
	{
		$query = "SELECT * FROM ticket_actions WHERE action_id=:qid";
		$result = $this->fetchSingleQuery($query, $input);
		return $result;
	}

	function getAllActions()
	{
		$query = "SELECT 
			ta.action_id, 
			ta.action_name, 
			tc.condition_id, 
			tc.condition_name AS condition_name FROM ticket_actions ta
			LEFT JOIN ticket_condition tc ON ta.condition_id=tc.condition_id ORDER BY ta.condition_id ASC ";
		$result = $this->fetchAllQuery($query);
		return $result;
	}

	function getActionSub($id, $opt)
	{
		$query = 'SELECT * FROM ticket_actions WHERE condition_id=:con_id';
		$value = array(
			'con_id' => $id 
		);
		$result = $this->fetchAllQuery($query, $value, $opt);
		return $result;
	}

	function getSubActionId($input)
	{
		$query = "SELECT * FROM ticket_actions WHERE Action_id=:tid";
		$result = $this->fetchSingleQuery($query, $input);
		return $result;
	}

	#create
	function createAction($form)
	{
		try {
			$result = $this->insertQuery('ticket_actions', $form);
			$this->lastInsertID = $this->lastInsertID();
			return 1;
		} catch (Exception $e) {
			if (getConfig('development') == 1) {
				echo $e;
				exit;
			}
			return 0;
		}
	}

	#edit
	function editAction($form, $where)
	{
		$result = $this->editQuery('ticket_actions', $form, $where);
		return $result;
	}

	#delete
	function deleteAction($id)
	{
		$query = $this->doQuery("DELETE FROM  ticket_actions WHERE action_id=$id");
	}

}
