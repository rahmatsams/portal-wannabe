<?php 
/**
 * MODEL REASON
 */
class ModelReason extends Model
{
    
    function getAllCountReason($query_option)
    {
        $query = "SELECT * FROM request_type ";

        $count_query = "SELECT COUNT(*) AS row_total FROM request_type";

        $result = $this->pagingQuery($query, $count_query,$query_option);
        return  $result;
    }

    function newReason($input)
    {
        $result = $this->insertQuery('request_type', $input);

        return $result;
    }

    function deleteReason($id)
    {
        $query = $this->doQuery("DELETE FROM request_type WHERE type_id=$id");
    }

    function getReasonById($input)
    {
        $query = "SELECT * FROM request_type ";
        $query .= "WHERE type_id=:type_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function editReason($form  = array(), $where = array())
    {
        $table = 'request_type';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }
    
}

 ?>