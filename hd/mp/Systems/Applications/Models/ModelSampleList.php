<?php 

/**
 * MODEL SAMPLE LIST
 */
class ModelSampleList extends Model
{
    
    function getAllActiveList()
    {
        $query = "SELECT * FROM sample_list";

        $result = $this->fetchAllQuery($query);
        return $result;
    }

    function getAllCountList($query_option)
    {
        $query = "SELECT * FROM sample_list ";
        #$query .= "LEFT JOIN  ON store.location_id=location.location_id";
        $count_query = "SELECT COUNT(*) AS row_total FROM sample_list";
        
        $result = $this->pagingQuery($query, $count_query, $query_option);
        return $result;
    }

    function newSampleList($input)
    {
        $result = $this->insertQuery('sample_list', $input);
        return $result;
    }

    function deleteList($id)
    {
        $query = $this->doQuery("DELETE FROM sample_list WHERE list_id=$id");
    }

    function getListById($input)
    {
        $query = "SELECT * FROM sample_list ";
        $query .= "WHERE list_id=:list_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function editSampleList($form  = array(), $where = array())
    {
        $table = 'sample_list';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }


}


 ?>