<?php 

/**
 * MODEL REPORT EXCEL
 */
class ModelReports extends Model
{
    
    function showAllSampleListByRequest($query_option, $filter=[])
    {
        $query_string = "SELECT 
            ts.sample_id,
            ts.sample_name,
            ts.sample_condition,
            ts.sample_size,
            ts.sample_date,
            ts.sample_status,
            ts.last_update,
            ts.sample_price,
            ts.sample_currency,
            ts.sample_list, 
            supplier.supplier_name AS supplier_name, 
            manufacturer.producer_name AS producer_name,
            ss.status_name AS status,
            td.document_name,
            td.document_type,
            tr.request_id,
            tr.request_name,
            tr.request_date,
            tr.request_user,
            tr.deadline_date AS deadline,
            sl.log_content,
            sl.log_date,
            sl.sample_status AS log_status,
            sc.condition_name ";
        $num = 0;
        $q_request = "SELECT request_id FROM ticket_request ";
        if(count($filter) > 0){
            $q_request .= 'WHERE ';
            foreach($filter as $key => $input){
                if($key != 'request_end'){
                    if($num >= 1){
                        $q_request .= "AND ";
                    }
                    if($key == 'request_start'){
                        $q_request .= "request_date BETWEEN '{$input} 00:00:00' AND '{$filter['request_end']} 23:59:00' ";
                    }else{
                        $q_request .= "{$key}{$input['operator']}'{$input['value']}' ";
                    }
                }
                $num++;
            }
        }
        
        $query_string .= "FROM ticket_sample ts ";

        $query_string .= "LEFT JOIN sample_status ss ON ts.sample_status=ss.status_id ";

        $query_string .= "LEFT JOIN sample_log sl ON sl.ticket_sample=ts.sample_id AND sl.sample_status=ts.sample_status ";

        $query_string .= "LEFT JOIN (SELECT * FROM ticket_document WHERE document_type=8) td ON ts.sample_id=td.ticket_sample ";

        $query_string .= "LEFT JOIN ticket_request tr ON ts.request_id=tr.request_id ";

        $query_string .= "LEFT JOIN sample_condition sc ON sc.condition_id=ts.sample_condition ";

        $query_string .= "LEFT JOIN ticket_supplier supplier ON supplier.supplier_id=ts.supplier ";

        $query_string .= "LEFT JOIN ticket_producer manufacturer ON manufacturer.producer_id=ts.producer ";
        
        $query_string .= " WHERE ts.request_id IN ({$q_request})";

        $result = $this->fetchAllQuery($query_string, $query_option);

        return $result;
    }


    
}
?>