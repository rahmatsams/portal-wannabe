<?php 

/**
 * MODEL STATUS
 */
class ModelStatus extends Model
{
	
	function getAllActiveStatus()
	{
		$query = "SELECT * FROM sample_status";

		$result = $this->fetchAllQuery($query);
		return $result;
	}

	function getAllCountStatus($query_option)
    {
        $query = "SELECT * FROM sample_status ";
        #$query .= "LEFT JOIN  ON store.location_id=location.location_id";
        $count_query = "SELECT COUNT(*) AS row_total FROM sample_status";
        
        $result = $this->pagingQuery($query, $count_query, $query_option);
        return $result;
    }

    function newStatus($input)
    {
    	$result = $this->insertQuery('sample_status', $input);
		return $result;
    }

    function deleteStatus($id)
    {
        $query = $this->doQuery("DELETE FROM sample_status WHERE status_id=$id");
    }

    function getStatusById($input)
    {
        $query = "SELECT * FROM sample_status ";
        $query .= "WHERE status_id=:status_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function editStatus($form  = array(), $where = array())
    {
        $table = 'sample_status';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }


}


 ?>