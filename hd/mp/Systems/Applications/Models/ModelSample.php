<?php 

/**
 * MODEL SAMPLE
 */
class ModelSample extends Model
{
	
	function newSample($form)
	{

		try {
			$result = $this->insertQuery('ticket_sample', $form);
			$this->lastInsertID = $this->lastInsertID();
			return 1;		
		} catch (Exception $e) {
			if (getConfig('development')  == 1) {
				echo $e;
				exit;
			}
			return 0;
		}
	}
    
    function modifySample($form, $where)
	{
		try {
			$result = $this->editQuery('ticket_sample', $form, $where);
			return 1;		
		} catch (Exception $e) {
			if (getConfig('development')  == 1) {
				echo $e;
				exit;
			}
			return 0;
		}
	}

    function newSampleTicketDocument($data)
    {

        try {
            $result = $this->insertQuery('ticket_document', $data);
            $this->lastInsertID = $this->lastInsertID();
            return 1;       
        } catch (Exception $e) {
            if (getConfig('development')  == 1) {
                echo $e;
                exit;
            }
            return 0;
        }
    }

    function modifySampleTicketDocument($data, $where)
    {
        try {
            $result = $this->editQuery('ticket_document', $data, $where);
            return 1;       
        } catch (Exception $e) {
            if (getConfig('development')  == 1) {
                echo $e;
                exit;
            }
            return 0;
        }
    }

	function getAllSample()
	{
		$query = "SELECT * FROM ticket_sample";

		$result = $this->fetchAllQuery($query);
		return $result;
	}

	function getTicketSampleByID($input)
    {
        $query = "SELECT ts.*, 
                tss.supplier_name AS supplier_name, 
                tp.producer_name AS producer_name,
                ts.sample_status,
                ss.status_name,
                td.document_name AS document_name,
                td.document_type AS document_type,
                td.document_location AS document_location,
                tr.request_id,
                tr.request_name,
                tr.request_type,
                rt.type_name AS request,
                tr.deadline_date AS deadline,
                sc.condition_name AS condition_name,
                sl.user_type,
                sl.log_content,
                sl.log_date,
                sl.sample_status AS fnb_status,
                lt.list_name,
                rt.type_name AS request_type
                FROM ticket_sample ts
                LEFT JOIN ticket_supplier tss ON ts.supplier=tss.supplier_id
                LEFT JOIN ticket_producer tp ON ts.producer=tp.producer_id
                LEFT JOIN sample_status ss ON ts.sample_status=ss.status_id
                LEFT JOIN (SELECT * FROM ticket_document WHERE document_type=8 AND ticket_sample=:sample_id) td ON ts.sample_id=td.ticket_sample
                LEFT JOIN ticket_request tr ON ts.request_id=tr.request_id
                LEFT JOIN sample_condition sc ON sc.condition_id=ts.sample_condition
                LEFT JOIN sample_log sl ON sl.ticket_sample=ts.sample_id
                LEFT JOIN request_type rt ON rt.type_id=tr.request_type
                LEFT JOIN sample_list lt ON lt.list_id=ts.sample_list
        WHERE ts.sample_id=:sample_id
        ";
        
        $result = $this->fetchSingleQuery($query, $input);
        
        return $result;
        
    }
    
    function getSampleForPrint($input)
    {
        $query = "SELECT 
            ts.sample_id,
            ts.request_id AS request,
            ts.sample_name,
            ts.sample_date,
            ts.sample_size,
            tss.supplier_name AS supplier_name, 
            tp.producer_name AS producer_name,
            ss.status_name AS sstatus,
            sl.log_content AS snote,
            tr.request_name,
            tr.request_type,
            rt.type_name AS request,
            tr.deadline_date AS deadline,
            sc.condition_name AS condition_name,
            fnb.log_content AS fnb_content,
            fnb.log_date AS fnb_date,
            fnb.status_name AS fnb_status,
            rt.type_name AS request_type
            FROM ticket_sample ts
            LEFT JOIN ticket_supplier tss ON ts.supplier=tss.supplier_id
            LEFT JOIN ticket_producer tp ON ts.producer=tp.producer_id
            LEFT JOIN ticket_request tr ON ts.request_id=tr.request_id
            LEFT JOIN sample_condition sc ON sc.condition_id=ts.sample_condition
            LEFT JOIN sample_status ss ON ts.sample_status=ss.status_id
            LEFT JOIN sample_log sl ON sl.sample_status=ts.sample_status
            LEFT JOIN (SELECT log_date,log_content,ticket_sample,user_type,status_name,sample_status, display_name AS fnb_user FROM sample_log sl LEFT JOIN sushitei_portal.portal_user sppu ON sl.user=sppu.user_id LEFT JOIN sample_status ON sl.sample_status=sample_status.status_id) fnb ON fnb.ticket_sample=ts.sample_id AND fnb.sample_status IN (2,5)
            LEFT JOIN request_type rt ON rt.type_id=tr.request_type
            WHERE ts.sample_id=:sample_id
        ";
        
        $result = $this->fetchSingleQuery($query, $input);
        
        return $result;
        
    }
    
    function getSampleByName($input)
    {
        $query = "SELECT ts.*, 
                tss.supplier_name AS supplier_name, 
                tp.producer_name AS producer_name,
                ts.sample_status,
                ss.status_name,
                td.document_name AS document_name,
                td.document_type AS document_type,
                td.document_location AS document_location,
                tr.request_id,
                tr.request_name,
                tr.request_type,
                rt.type_name AS request,
                tr.deadline_date AS deadline,
                sc.condition_name AS condition_name,
                sl.user_type,
                sl.log_content,
                sl.log_date,
                sl.sample_status AS fnb_status,
                lt.list_name,
                rt.type_name AS request_type
                FROM ticket_sample ts
                LEFT JOIN ticket_supplier tss ON ts.supplier=tss.supplier_id
                LEFT JOIN ticket_producer tp ON ts.producer=tp.producer_id
                LEFT JOIN sample_status ss ON ts.sample_status=ss.status_id
                LEFT JOIN (SELECT * FROM ticket_document WHERE document_type=8) td ON ts.sample_id=td.ticket_sample
                LEFT JOIN ticket_request tr ON ts.request_id=tr.request_id
                LEFT JOIN sample_condition sc ON sc.condition_id=ts.sample_condition
                LEFT JOIN sample_log sl ON sl.ticket_sample=ts.sample_id
                LEFT JOIN request_type rt ON rt.type_id=tr.request_type
                LEFT JOIN sample_list lt ON lt.list_id=ts.sample_list
        WHERE ts.sample_name LIKE :sample_name GROUP BY ts.sample_id
        ";
        
        $result = $this->fetchAllQuery($query, $input);
        
        return $result;
        
    }

    function getSamplePending($input)
    {
        $query = "SELECT ts.sample_name,
                ts.sample_status,
                ts.sample_date,
                ts.sample_id,
    			tss.supplier_name, 
    			tp.producer_name,
                ss.status_name,
                tr.request_closed,
                tr.request_id,
                tr.request_name,
                sc.condition_name AS condition_name
    			FROM ticket_request tr
            LEFT JOIN ticket_sample ts ON tr.request_id=ts.request_id AND ts.sample_status=:sample_status
            LEFT JOIN ticket_supplier tss ON tss.supplier_id=ts.supplier
            LEFT JOIN ticket_producer tp ON tp.producer_id=ts.producer
            LEFT JOIN sample_status ss ON ts.sample_status=ss.status_id
            LEFT JOIN sample_condition sc ON sc.condition_id=ts.sample_condition 
            
        WHERE tr.request_closed=0 AND ss.status_name IS NOT NULL ORDER BY ts.last_update,sample_date DESC";
    	
        $result = $this->fetchAllQuery($query, $input);
        
        return $result;
        
    }

    function getSampleDocument($input)
    {
        $query = "SELECT * FROM ticket_document td JOIN document_type dt ON dt.type_id=td.document_type WHERE ticket_sample=:sample AND document_type != 8 ORDER BY type_id ASC";
        
        $result = $this->fetchAllQuery($query, $input);
        
        return $result;
        
    }

    function getTicketSampleByDoc($input) //model
    {
        $query = "SELECT ts.*, 
                qq.supplier_name, 
                qq.producer_name,
                ss.status_name,
                td.document_name,
                td.document_type,
                td.document_ext,
                tr.request_name,
                tr.deadline_date AS deadline,
                dt.type_name AS doc_type_name,
                sc.condition_name
                FROM ticket_sample ts
                LEFT JOIN (SELECT 
                    ts.supplier_id, 
                    ts.supplier_name, 
                    ts.supplier_address, 
                    ts.supplier_phone, 
                    ts.supplier_pic, 
                    tp.producer_name AS producer_name, 
                    tl.link_id 
                FROM ticket_supplier ts 
                LEFT JOIN ticket_link tl ON ts.supplier_id=tl.supplier_id
                LEFT JOIN ticket_producer tp ON tl.producer_id=tp.producer_id) qq ON ts.link_id=qq.link_id 
                LEFT JOIN sample_status ss ON ts.sample_status=ss.status_id
                LEFT JOIN ticket_document td ON ts.sample_id=td.ticket_sample
                LEFT JOIN ticket_request tr ON ts.request_id=tr.request_id
                LEFT JOIN sample_condition sc ON sc.condition_id=ts.sample_condition
                LEFT JOIN document_type dt ON td.document_type=dt.type_id 
        WHERE ts.sample_id=:sample_id AND td.document_type !=8";
        
        $result = $this->fetchAllQuery($query, $input);
        
        return $result;
        
    }

    function getSampleByRequestId($id, $opt)
    {
    	$query = "SELECT ts.*, 
    			tss.supplier_name AS supplier_name, 
    			tp.producer_name AS producer_name,
                ss.status_name,
                ts.sample_status,
                sc.condition_name AS condition_name
    			FROM ticket_sample ts
				LEFT JOIN ticket_supplier tss ON tss.supplier_id=ts.supplier
                LEFT JOIN ticket_producer tp ON tp.producer_id=ts.producer
                LEFT JOIN sample_status ss ON ts.sample_status=ss.status_id
                LEFT JOIN sample_condition sc ON sc.condition_id=ts.sample_condition 
                WHERE request_id=:r_id ORDER BY sample_date DESC";
    	$value = array(
    		'r_id' => $id 
    	);
    	$result = $this->fetchAllQuery($query, $value, $opt);
    	return $result;
    }

    function getSampleByLog($input)
    {
        $query = "
            SELECT 
                sl.log_content,
                sl.sample_status AS status_id,
                sl.registry_number AS registry_number,
                ts.sample_id,
                        request_id,
                        request_name,
                        request_reason,
                        request_date,
                        deadline_date, 
                        (CASE WHEN status_name IS NULL OR status_name = ''
                    THEN 'Open'
                    ELSE status_name
               END) AS STATUS FROM ticket_request tr 
               LEFT JOIN (SELECT MAX(sample_status) AS sample_status,request_id, sample_id FROM ticket_sample GROUP BY request_id) ts 
               USING (request_id) 
               LEFT JOIN sample_status sp ON ts.sample_status=status_id
               LEFT JOIN sample_log sl ON ts.sample_id=sl.ticket_sample
        WHERE ts.sample_id=:sample_id";

        $result = $this->fetchSingleQuery($query, $input);
        
        return $result;
    }

	//cek image exist
    function checkImageExist($input)
    {
        $query = "SELECT document_name,document_location FROM 
        ticket_document WHERE ticket_sample=:ticket_sample
        AND document_type=8";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    //history sample log
    function getDetailSampleByLog($input)
    {
      $query = "SELECT 
                    ts.sample_id,
                    ts.sample_name,
                    sl.log_id,
                    sl.log_content,
                    sl.log_date,
                    sl.sample_status AS status_id,
                    ss.status_name,
                    ut.user_type_name AS user_type
                FROM sample_log sl
                LEFT JOIN ticket_sample ts ON ts.sample_id=sl.ticket_sample 
                LEFT JOIN user_type ut ON sl.user_type=ut.user_type_id
                LEFT JOIN sample_status ss ON ss.status_id=sl.sample_status
                WHERE ts.sample_id=:ticket_sample";
      $result = $this->fetchAllQuery($query, $input);
      return $result;
    }

}


 ?>