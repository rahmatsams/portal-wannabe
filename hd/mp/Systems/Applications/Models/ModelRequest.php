<?php 

/**
 * MODEL REQUEST
 */
class ModelRequest extends Model
{
	
	function newRequest($input)
	{
		$result = $this->insertQuery('ticket_request', $input);
		return $result;
	}

	function getAllRequest() #INDEX TICKET LIST
	{
		$query = "SELECT 
                request_id,
                request_name,
                request_type,
                type_name AS reason,
                request_date,
                deadline_date,
                request_closed, 
                (CASE WHEN status_name IS NULL OR status_name = ''
            THEN 'Open'
            ELSE status_name
       END) AS status FROM ticket_request tr 
       LEFT JOIN (SELECT MAX(sample_status) AS sample_status,request_id FROM ticket_sample GROUP BY request_id) ts 
       USING (request_id) 
       LEFT JOIN sample_status sp ON ts.sample_status=status_id
       LEFT JOIN request_type rt ON tr.request_type=rt.type_id
       ORDER BY request_date DESC";

		$result = $this->fetchAllQuery($query);
		return $result;
	}

  function getFiltered($filter = array(), $query_option)
  {
    $query_string = "SELECT 
    request_id, 
    request_name, 
    request_type, 
    type_name AS reason, 
    request_date, 
    deadline_date, 
    request_closed, 
    sppu.display_name,
    (CASE WHEN status_name IS NULL OR status_name = '' THEN 'Open' ELSE status_name END) AS status 
    FROM ticket_request tr 
    LEFT JOIN (SELECT MAX(sample_status) AS sample_status,request_id FROM ticket_sample GROUP BY request_id) ts 
    USING (request_id) 
    LEFT JOIN sample_status sp ON ts.sample_status=status_id
    LEFT JOIN request_type rt ON tr.request_type=rt.type_id 
    LEFT JOIN sushitei_portal.portal_user sppu ON tr.request_user=sppu.user_id ";
    $num = 0;
    $count_query = "SELECT COUNT(*) AS row_total FROM ticket_request tr LEFT JOIN (SELECT MAX(sample_status) AS sample_status,request_id FROM ticket_sample GROUP BY request_id) ts 
        USING (request_id) 
        LEFT JOIN sample_status sp ON ts.sample_status=status_id
        LEFT JOIN request_type rt ON tr.request_type=rt.type_id ";
    #echo 'filer > '.count($filter);
    #print_r($filter);
    if(count($filter) > 0){
        $query_string .= 'WHERE ';
        $count_query .= 'WHERE ';
        foreach($filter as $key => $input){
            if($key != 'request_end'){
                if($num >= 1){
                    $query_string .= "AND ";
                    $count_query .= "AND ";
                }
                if($key == 'request_start'){
                    $query_string .= "request_date BETWEEN '{$input} 00:00:00' AND '{$filter['request_end']} 23:59:00' ";
                    $count_query .= "request_date BETWEEN '{$input} 00:00:00' AND '{$filter['request_end']} 23:59:00' ";
                }else{
                    $query_string .= "{$key}{$input['operator']}'{$input['value']}' ";
                    $count_query .= "{$key}{$input['operator']}'{$input['value']}' ";
                }
            }
            $num++;
        }
    }
    #exit($count_query);
    $result['data'] = $this->pagingQuery($query_string, $count_query, $query_option);
    $result['opt'] = $filter;
    $result['total'] = $this->getCountResult();
    return $result;
}

	function getTicketRequestByID($input, $select = 0)
    {
      $query = "SELECT ";
      if($select){
          $query .= $select;
      }else{
          $query .= '*';
      }
      
      $query .= " FROM ticket_request 
      LEFT JOIN request_type ON request_type.type_id=ticket_request.request_type
      LEFT JOIN sushitei_portal.portal_user sppu ON (request_user=sppu.user_id) 
      WHERE request_id=:request_id";
      
      $result = $this->fetchSingleQuery($query, $input);
      return $result;
        
    }
    
    function getTicketRequestBySampleID($input, $select = 0)
    {
        $query = "SELECT ";
        if($select){
            $query .= $select;
        }else{
            $query .= '*';
        }
        
        $query .= " FROM ticket_sample ts WHERE sample_id=:sample_id";
        
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
        
    }


    function getRequestById($input) #view request
    {
    	$query = "SELECT 
                tr.*,
                rt.type_name AS type_name
              FROM ticket_request tr
              LEFT JOIN request_type rt ON tr.request_type=rt.type_id 

              WHERE request_id=:req_id ";

    	$result = $this->fetchSingleQuery($query, $input);
    	return $result;
    }

   function getDataRequestById($input)
   {
   		$query = "SELECT * FROM ticket_request ";
   		$query .= "WHERE request_id=:request_id";
   		$result = $this->fetchSingleQuery($query, $input);
   		return $result;
   }

   function editRequest($form = array(), $where = array())
   {
   		$table = 'ticket_request';
   		$result = $this->editQuery($table, $form, $where);
   		return $result;
   }

   function getAllRequestType()
   {
      $query = "SELECT type_id, type_name FROM request_type";
      $result = $this->fetchAllQuery($query);
      return $result;
   }

    function getAllSampleStatus()
    {
        $query = "SELECT * FROM sample_status";
        $result = $this->fetchAllQuery($query);
        return $result;
    }

    function modifyRequest($id)
    {
      $query = $this->doQuery("UPDATE ticket_request SET request_closed=1, last_update=NOW() WHERE request_id=$id");
    }

  /*SELECT sppu.display_name, sl.log_date, ts.sample_name, ss.status_name, ut.user_type_name FROM sample_log sl 
LEFT JOIN ticket_sample ts ON sl.ticket_sample=ts.sample_id 
LEFT JOIN sample_status ss ON sl.sample_status=ss.status_id
LEFT JOIN user_type ut ON sl.user_type=ut.user_type_id 
LEFT JOIN sushitei_portal.portal_user sppu ON sl.user=sppu.user_id*/	


}


 ?>