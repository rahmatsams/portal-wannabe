            <div class="content">
                <div class="breadcrumbs">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1><i class="fa fa-book"></i> My Request</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <form id="searchMRP" class="forms-sample">
                                <div class="form-group mt-2">
                                    <a class="btn btn-sm btn-primary rounded collapsed" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                        <i class="fa fa-search"></i> Advanced Search
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <form id="advanceSearch" method="POST" action="<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"?>">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-3 form-group">
                                        <label for="ticketTitle">Subject</label>
                                        <input id="ticketPage" name="page" type="hidden" value="1">
                                        <input id="ticketTitle" name="title" type="text" placeholder="Type subject here.." class="form-control" maxlength="25"<?=(!empty($post['title']) ? " value='{$post['title']}'" : '')?>>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="ticketStatus">Status</label>
                                        <select id="ticketStatus" name="status" class="form-control">
                                            <option></option>
                                            <?php foreach($status as $val): ?>
                                            <option value="<?=$val['status_id']?>"<?=(!empty($post['status']) && $post['status'] == $val['status_id'] ? ' Selected' : '')?>><?=$val['status_name']?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="mainCategory">Reason</label>
                                        <select name="request_type" id="mainCategory" class="form-control">
                                            <option></option>
<?php foreach($request_type as $val): ?>
                                            <option value="<?=$val['type_id']?>"<?=(!empty($post['request_type']) && $post['request_type'] == $val['type_id'] ? ' Selected' : '')?>><?=$val['type_name']?></option>
<?php endforeach; ?>

                                        </select>
                                    </div>
                                    <!-- pic -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 form-group">
                                        <label for="submitStart">Date from</label>
                                        <input id="submitStart" name="submit_start" type="date" class="form-control" maxlength="10"<?=(!empty($post['submit_start']) ? " value='{$post['submit_start']}'" : '')?>>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="submitEnd">to date</label>
                                        <input id="submitEnd" name="submit_end" type="date" class="form-control" maxlength="10"<?=(!empty($post['submit_end']) ? " value='{$post['submit_end']}'" : '')?>>
                                    </div>
                                </div>

                                <input type="submit" name="submit" value="Search Now" class="btn btn-sm btn-primary mb-2 rounded">
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="content">
                <div class="table-responsive">
                  <table id="indexContent" class="table center-aligned-table table-striped">
                    <thead>
                      <tr class="bg-white">
                        <th>No</th>
                        <th>Subject</th>
                        <th>Requester</th>
                        <th>Request Reason</th>
                        <th>Request Date</th>
                        <th>Request Deadline</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody id="listTable">
                        <?php 
                          if (isset($request['total']) && $request['total'] > 0):
                          
                            $i = 1;
                            $d = array(
                                'FNB Release'  => 'bg-primary text-white',
                                'QA Release'   => 'bg-warning text-white',
                                'KAHI Release' => 'bg-success text-white',
                                'Other'        => 'bg-danger text-white',
                            );
                            
                            foreach ($request['data'] as $req){
                                $request_date  = date("d-m-Y", strtotime($req['request_date']));
                                $deadline_date = date("d-m-Y", strtotime($req['deadline_date']));
                                
                                echo "
                                <tr class='".(isset($d[$req['status']]) ? $d[$req['status']] : $d['Other'])."'>
                                    <td>{$req['request_id']}</td>
                                    <td><a href=\"view_request_{$req['request_id']}.html\" class=\"text-decoration-none text-white\">{$req['request_name']}</a></td>
                                    <td>{$req['display_name']}</td>
                                    <td>{$req['reason']}</td>
                                    <td>{$request_date}</td>
                                    <td>{$deadline_date}</td>
                                    <td>{$req['status']}</td>
                                </tr>
                                ";
                                $i++;

                            }
                          else:
                        ?>
                        <tr>
                            <td colspan="7">There's no Request Data</td>
                        </tr>  
                        <?php endif; ?>
                    </tbody>
                  </table>
                </div>
                <?php
                    $pages = ceil($request['total']/$filter['result']);
                    if($pages > 1):
                ?>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                    <?php if(($filter['page'] - 9) >= 0): ?>

                        <li class="page-item"><a href="index_<?=$filter['page']-1?>.html" class="page-link page-navigate">Previous</a></li>
                        <li class="page-item"><a href="#" class="page-link">...</a></li>
                    <?php
                        endif;
                        $page_start = 1;
                        $page_end = $pages;
                        if($pages > 9 && $filter['page'] > 9){
                            $page_start = (($filter['page']-4) < 1 ? 1 : $filter['page']-4);
                            $page_end = (($filter['page']+4) > $pages ? $pages : ($filter['page']+4));
                        }elseif($pages > 9 && $filter['page'] <= 9){
                            $page_start = (($filter['page']-4) < 1 ? 1 : $filter['page']-4);
                            $page_end = (($filter['page']+4) > 9 ? ($filter['page']+4) : 9);
                        }elseif($pages < 9 && $filter['page'] <= 9){
                            $page_start = (($filter['page']-4) < 1 ? 1 : $filter['page']-4);
                            $page_end = $pages;
                        }
                        for($i=$page_start;$i <= $page_end;$i++):
                    ?>

                        <li class="page-item"><a href="index_<?=$i?>.html" class='page-link page-navigate' data-id='<?=$i?>'><?=$i?></a></li>
                    <?php
                        endfor;
                        if(($filter['page'] + 4) < $pages): ?>
                        <li class="page-item"><a href="#" class="page-link">...</span></a></li>
                        <li class="page-item"><a href="index_<?=$filter['page']+1?>.html" class='page-link page-navigate' data-id='<?=$pages?>'><?=$pages?></a></li>
                    <?php endif; ?>
                    </ul>
                </nav>
                <?php endif; ?>
                <?php if($request['total'] > 0): ?>
                <a class="btn btn-sm btn-success rounded" role="button" href="export_rawmat.html" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-file-excel-o"></i> Export Data
                </a>
                <?php endif; ?>
            </div>