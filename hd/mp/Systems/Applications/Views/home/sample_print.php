<?php
    $num = 1;
?>
<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <!-- TemplateBeginEditable name='doctitle' -->
        <title>Rawmat 2019</title>
        <style>
            .container{
                width: 600px;
                height: 980px;
                margin: 0 auto;
                font-family: Arial;
            }
            .table1{
                border: 1px solid #000;
                border-width: 10%;
                width: 100%;
                font-size: 82%;
                margin-bottom: 10px;
                /* border-right: 0px; */
            }
            .table1 tr td{
                font-size: 105%;
                padding: 5px;
                text-align: center;
                font-weight: bold;
            }
            .table2{
                width: 100%;
                border: 1px solid #000;
                font-size: 82%;
                border-collapse: collapse;
                margin-bottom: 10px;
            }
            
            .table2 tr{
                border: 1px solid #000;
            }
            .border-right{
                border-right: 1px solid #000;
            }
            .border-bottom{
                border-bottom: 1px solid #000;
            }
            .table2 td{
                padding: 5px;
            }
            
            .table-page-2{
                width: 100%;
                border: 1px solid #000;
                font-size: 82%;
                border-collapse: collapse;
            }
            .table-page-2 tr td{
                padding: 3px;
            }
            .table2-inside{
                width: 100%;
                border-collapse: collapse;
                border: none;
                font-size: 90%;
            }
            .table-page2-inside{
                width: 100%;
                border-top: 1px solid #000;
                border-bottom: 1px solid #000;
                font-size: 100%;
            }
            .table-page2-inside tr td{
                text-align: center;
                border: 1px solid #000;
            }
            .table-page2-inside tr:first-child td{
                font-weight: bold;
            }
            .table-page2-inside tr td:first-child{
                border-left: 0;
            }
            .table-page2-inside tr td:last-child{
                border-right: 0;
            }
            .table2-inside tr{
                border: none;
            }
            .no-border{
                border: 0px;
            }
            
            .table-title{
                text-align: center;
                font-weight: bold;
                font-size: 105%;
                padding: 10px;
            }
            .table3{
                width: 100%;
                font-size: 57%;
                position:relative;
                top: -50px;
                height: 25px;
            }
            .table4{
                width: 100%;
                font-size: 80%;
                margin-bottom: 20px;
            }
            table{   
                padding-bottom: 2px;
                padding-top: 2px; 
                border-collapse: collapse;
            }
            .text-bold{
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <table class='table4'>
                <tr>
                    <td><img src='./Resources/images/LogoBig.jpg' style="width: 50px"/></td>
                    <td style="text-align: center;">
                        <span style="font-weight: bold; letter-spacing: 3px;">PT.SUSHI-TEI INDONESIA</span><br>
                        SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                        Grand Wijaya Center Blok E No. 18-19 <br>
                        Jl. Wijaya II - Kebayoran Baru <br>
                        Jakarta Selatan 12160 <br>
                        Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                    </td>
                </tr>
            </table>
            <table class="table1">
                <tr>
                    <td>FORM PENGUJIAN MATERIAL BARU</td>
                </tr>
            </table>
            <table class='table2'>
                <tr>
                    <td colspan='6' class="table-title">Diisi Oleh Bagian Purchasing</td>
                </tr>
                <tr>
                    <td>Nama Bahan Baku</td>
                    <td width="1%">:</td>
                    <td colspan="4"><?=$data['sample_name']?></td>
                </tr>
                <tr>
                    <td>Nama Produsen</td>
                    <td>:</td>
                    <td colspan="4"><?=$data['producer_name']?></td>
                </tr>
                <tr>
                    <td>Nama Supplier</td>
                    <td>:</td>
                    <td colspan="4"><?=$data['supplier_name']?></td>
                </tr>
                <tr>
                    <td>Tanggal Penerimaan Sampel</td>
                    <td>:</td>
                    <td colspan="4"><?=strftime("%d %B %Y", strtotime($data['sample_date']))?></td>
                </tr>
                <tr>
                    <td>Jumlah / Berat Sampel</td>
                    <td>:</td>
                    <td colspan="4"><?=$data['sample_size']?></td>
                </tr>
                <tr>
                    <td>Kemasan</td>
                    <td>:</td>
                    <td colspan="4"><?=$data['condition_name']?></td>
                </tr>
                <tr>
                    <td>Alasan pengujian Material</td>
                    <td>:</td>
                    <td class="border-right"><?=$data['request']?></td>
                    <td>Waktu Penggunaan</td>
                    <td>:</td>
                    <td><?=strftime("%d %B %Y", strtotime($data['deadline']))?></td>
                </tr>
                <tr>
                  <td colspan='6' class="table-title">Diisi Oleh Bagian Bersangkutan ( F&#38;B )</td>
                </tr>
                <tr>
                    <td>Tanggal Penerimaan Sample</td>
                    <td>:</td>
                    <td class="border-right"><?=(!empty($data['fnb_date']) ? strftime("%d %B %Y", strtotime($data['fnb_date'])) : '')?></td>
                    <td>Tanggal Mulai Pengujian</td>
                    <td>:</td>
                    <td><?=(!empty($data['fnb_date']) ? strftime("%d %B %Y", strtotime($data['fnb_date'])) : '')?></td>
                </tr>
                <tr>
                    <td>Metode Pengujian</td>
                    <td>:</td>
                    <td class="border-right"><?=(!empty($data['fnb_content']) ? $data['fnb_content'] : '')?></td>
                    <td>Tanggal Selesai Pengujian</td>
                    <td>:</td>
                    <td><?=(!empty($data['fnb_date']) ? strftime("%d %B %Y", strtotime($data['fnb_date'])) : '')?></td>
                </tr>
                <tr>
                    <td>Hasil Pengujian</td>
                    <td>:</td>
                    <td colspan="4"><?=(!empty($data['fnb_status']) ? $data['fnb_status'] : '')?></td>
                </tr>
                <tr>
                    <td colspan='6'>
                        <table class='table2-inside'>
                            <tr>
                                <td colspan='4'><span style="font-weight: bold">Kelengkapan Dokumen Material :</span></td>
                            </tr>
                            <?php foreach($documents as $doc): ?>
                            <?php if($doc['type_id'] == 1): ?>
                            <tr>
                                <td><?=$num?></td>
                                <td width="30%">Sertifikat Halal, Expired</td>
                                <td>:</td>
                                <td colspan="3"><?=(!empty($doc['document_expired']) ? strftime("%d %B %Y", strtotime($doc['document_expired'])) : '-')?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Dikeluarkan oleh</td>
                                <td>:</td>
                                <td><?=(!empty($doc['document_issued']) ? $doc['document_issued'] : '-')?></td>
                                <td colspan="2">Diakui oleh MUI : <?=$doc['document_acknowledged'] ? 'Ya' : 'Tidak'?></td>
                            </tr>
                            <?php $num++; else: ?>
                            <tr>
                                <td><?=$num?></td>
                                <td><?=$doc['document_name']?></td>
                                <td>:</td>
                                <td colspan="3"><?=($doc['uploaded'] ? '&#10004;' : '-')?></td>
                            </tr>
                            <?php $num++; endif; endforeach; ?>
                        </table>
                    </td>
                </tr>
            </table>
       
            <table class='table3'>
                <tr><br><br><br>
                    <td width='100'>No Dokumen</td>
                    <td width='10'>:</td>
                    <td>FRM/P&I/02/03</td>
                </tr>
                <tr>
                    <td>No Revisi</td>
                    <td width='10'>:</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>Tanggal Efektif</td>
                    <td width='10'>:</td>
                    <td>2 Januari 2019</td>
                </tr>
            </table>
        </div>
        <div class="container">
            <table class='table4'>
                <tr>
                    <td><img src='./Resources/images/LogoBig.jpg' style="width: 50px"/></td>
                    <td style="text-align: center;">
                        <span style="font-weight: bold; letter-spacing: 3px;">PT.SUSHI-TEI INDONESIA</span><br>
                        SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                        Grand Wijaya Center Blok E No. 18-19 <br>
                        Jl. Wijaya II - Kebayoran Baru <br>
                        Jakarta Selatan 12160 <br>
                        Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                    </td>
                </tr>
            </table>
            <table class='table-page-2'>
                <tr>
                    <td colspan='3'><span style="font-weight: bold;">Ketentuan</span> : </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">1.</td>
                    <td>Material dapat digunakan jika dapat melengkapi dokumen point 1 dan 2</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">2.</td>
                    <td>Jika material termasuk positive list, maka hanya diwajibkan melengkapi dokumen point 2, kecuali untuk buah, sayur, telur segar dapat langsung digunakan</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">3.</td>
                    <td>Jika point 2 tidak dapat dilengkapi dari supplier, maka wajib dilakukan uji mikrobiologi secara internal atau eksternal jika diperlukan</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">4.</td>
                    <td>Jika point 1 tidak dapat dilengkapi dari supplier, maka wajib dibuktikan dengan melengkapi point 2 s/d 6 yang dibuat oleh supplier untuk selanjutnya disubmit ke LPPOM MUI oleh KAHI untuk mendapatkan persetujuan sebelum barang dapat digunakan</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">5.</td>
                    <td>Jika merupakan item custom maka prosesnya wajib dilakukan di pabrik yang sudah bersertifikasi Halal dan apabila item custom belum bersertifikasi halal maka harus dilengkapi dengan dokumen   poin 2 s/d 6 serta sertifikat halal dari masing-masing ingredient yang digunakan</td>
                </tr>
                <tr class="border-bottom">
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">6.</td>
                    <td>Jika merupakan item repack maka wajib dilakukan audit supplier terlebih dahulu serta dilengkapi dokumen nomor 1 s/d 6</td>
                </tr>
                <tr>
                    <td colspan='3'><span style="font-weight: bold;">Note</span> : </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">a.</td>
                    <td>Apabila dokumen point 1 yang disubmit merupakan dokumen Sertifikat Halal yang dikeluarkan oleh MUI, maka harus diverifikasi ke dalam web halalmui.org. Jika bahan tersebut terdaftar dalam list halal produk MUI, maka bahan dapat langsung digunakan tanpa melaporkan ke MUI secara CEROL.</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">b.</td>
                    <td>Apabila dokumen point 1 merupakan dokumen Sertifikat Halal yang dikeluarkan oleh MUI dan bahan tersebut tidak tercantum dalam web halalmui.org, maka bahan tersebut harus dilaporkan &nbsp; ke MUI secara CEROL terlebih dahulu sebelum digunakan.</td>
                </tr>
                <tr class="border-bottom">
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">c.</td>
                    <td>Apabila dokumen point 1 merupakan dokumen Sertifikat Halal yang dikeluarkan oleh lembaga selain MUI, maka bahan tersebut harus dilaporkan ke MUI secara CEROL terlebih dahulu sebelum &nbsp;digunakan.</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><span style="font-weight: bold;">Kesimpulan: <?=($data['sstatus'] == 'QA Release' || $data['sstatus'] == 'KAHI Release' || $data['sstatus'] == 'FNB Release' ? 'Material dapat digunakan' : 'Material tidak dapat digunakan')?></span></td>
                </tr>
                <tr style="height: 75px;">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top;">Keterangan: <br><?=$data['snote']?></td>
                </tr>
                <tr>
                    <td colspan='3' style="padding: 0;">
                        <table class="table-page2-inside">
                            <tr>
                                <td width="25%">Pemohon</td>
                                <td width="25%">Penguji</td>
                                <td width="25%">Diketahui Oleh</td>
                                <td width="25%">Disetujui Oleh</td>
                            </tr>
                            <tr>
                                <td style="height: 70px">~Signed~</td>
                                <td style="height: 70px"><?=($data['sstatus'] == 'QA Release' || $data['sstatus'] == 'KAHI Release' || $data['sstatus'] == 'FNB Release' ? '<i>~Signed~</i>' : '')?></td>
                                <td style="height: 70px"><?=($data['sstatus'] == 'QA Release' || $data['sstatus'] == 'KAHI Release' ? '<i>~Signed~</i>' : '')?></td>
                                <td style="height: 70px"><?=($data['sstatus'] == 'KAHI Release' ? '<i>~Signed~</i>' : '')?></td>
                            </tr>
                            <tr>
                                <td>Purchasing</td>
                                <td>Executive Chef/F&#38;B</td>
                                <td>QA</td>
                                <td>KAHI</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- <td colspan="3" style="text-align: center;">Diketahui Oleh<br/><br/><br/><br/>GM Admin</td> -->
                </tr>
            </table>
            <div style="font-size: 52%; margin-bottom: 10px;">Coret yang tidak perlu*</div>
        </div>
        <div style="width: 600px; margin: 0 auto; font-family: Arial;">
            <table class='table3'>
                <tr>
                    <td width='100'>No Dokumen</td>
                    <td width='10'>:</td>
                    <td>FRM/P&I/02/03</td>
                </tr>
                <tr>
                    <td>No Revisi</td>
                    <td width='10'>:</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>Tanggal Efektif</td>
                    <td width='10'>:</td>
                    <td>2 Januari 2019</td>
                </tr>
            </table>
        </div>
    </body> 
</html>