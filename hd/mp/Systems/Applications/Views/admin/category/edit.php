<?php 
    $em = $producer['producer_id'];
?>
<div class="content-right">
    <div class="card-body col-md-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="admin_producer.html" class="btn btn-link"><u>Back</u></a></li>
            <li class="breadcrumb-item active"><a class="btn">Producer</a></li>
        </ol>
    </div>
    <div class="card-body col-lg-6">
        <div class="card-deck">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Edit Detail</strong>
                </div>  
    <div class="card-body p-0 pt-3 pb-3">
        <form id="editStore" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
            <div class="col-md-12 form-group">
                <?=(isset($error) && isset($error['producer_name']) ? "<div class=\"alert alert-danger\"><strong>Producer Name</strong>{$error['producer_name']}</div>" : '<label>Producer Name</label>')?>
                <input name="producer_name" type="text" value="<?=$producer['producer_name']?>" class="form-control" maxlength="25" required>
            </div>
            <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['producer_address']) ? "<div class=\"alert alert-danger\"><strong>Producer Name </strong>{$error['producer_address']}</div>" : '<label>Producer Address</label>')?>
                    <textarea name="producer_address" id="textarea-input" rows="3" class="form-control"><?=$producer['producer_address']?></textarea>
                </div>
<!-- Email -->
        <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['producer_phone']) ? "<div class=\"alert alert-danger\"><strong>Producer Name </strong>{$error['producer_phone']}</div>" : '<label>Producer Phone</label>')?>
                    <input name="producer_phone" type="number" value="<?=$producer['producer_phone']?>" class="form-control" maxlength="50" required>
                    <small class="form-text text-muted">Number only please</small>
                </div>
                <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['producer_pic']) ? "<div class=\"alert alert-danger\"><strong>Producer Name </strong>{$error['producer_pic']}</div>" : '<label>Producer PIC</label>')?>
                    <input name="producer_pic" type="text" value="<?=$producer['producer_pic']?>" class="form-control" maxlength="50" required>
                </div>
        <div class="col-md-12 form-group">
            <button type="submit" name="submit" value="edit_producer" class="btn btn-success rounded">Edit</button>
        </div>
        </div>
    </form>
</div>
</div>
					