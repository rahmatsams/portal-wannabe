    <div class="container-fluid">
        <ol class="breadcrumb">
             <li class="breadcrumb-item"><a href="administrator.html" class="btn btn-link"><u>Administrator</u></a></li>
            <li class="breadcrumb-item active"><a class="btn">Manage Sample Category</a></li>
        </ol>
        <div class="table-responsive">
            <a href="new_category.html" class="btn btn-primary btn-md waves-effect float-right fa fa-plus-circle"> New</a><br><br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sample Category</th>
                        <th>List Name</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($category) && !empty($category)){
                            $num = (($page-1)*10)+1;
                            foreach($category as $result){
                                
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['category_name']}</td>
                        <td>{$result['list']}</td>
                        <td><a href=\"edit_category_{$result['category_id']}.html\" class='edit-users btn btn-success btn-small menu-icon fa fa-pencil'></a>
                            
                            <a href=\"#\" class=\"admin_delete_category deleteconfirmationmodal\" data-id=\"{$result['category_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"menu-icon fa fa-trash btn btn-danger btn-md\"></button>
                            </a>
                        </td>
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="6">Ups Sorry, There's no data found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
            $pages = ceil($total/$max_result); #paginationbootstrap
                if($pages > 1){
                    echo '
                <nav>
                    <ul class="pagination pagination-sm">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>';
                    for($i=1;$i <= $pages;$i++){
                        echo "<li ". ($page == $i ? 'class="page-item active"' : '') ."><a class=\"page-link\" href='admin_category_{$i}.html'>{$i}</a></li>";
                    }
                    echo '
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                    </li>
                    </ul>
                </nav>';
                }
            ?>
        </div>

        <!--  -->
        <!-- DELETE CATEGORY -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">DELETE CATEGORY</h5>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure want to delete this data?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <a id="delete_category" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Yes, Sure</button></a>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                        $(document).ready(function(){
                            $(".admin_delete_category").click(function() {
                                var id = $(this).attr('data-id'); /*data-id ada di button Hapus*/
                                $("#delete_category").attr("href", "delete_category_"+id+".html"); 
                            });
                        });
                    </script>

           