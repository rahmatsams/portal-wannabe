<div class="content-right">
    
    <div class="card-body col-lg-6">
        <div class="card-deck">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">New Sample Category</strong>
                </div>    
	<div class="card-body p-0 pt-3 pb-3">
        <form id="addProducer" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['category_name']) ? "<div class=\"alert alert-danger\"><strong>Sample Category Name </strong>{$error['category_name']}</div>" : '<label>Sample Category Name</label>')?>
                    <input name="category_name" type="text" placeholder="Name here.." class="form-control" maxlength="50" required>
                </div>

            <!-- List -->
                 <div class="col-md-12 form-group">
                    <?=(isset($error) && isset($error['list']) ? "<div class=\"alert alert-danger\"><strong>Sample Category Name </strong>{$error['list']}</div>" : '<label>List</label>')?>
                    <input name="list" type="text" placeholder="Name here.." class="form-control" maxlength="50" required>
                </div>

                <div class="col-md-12 form-group">
            <button type="submit" name="submit" value="create_category" class="btn btn-primary rounded">Create</button></div>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>


					