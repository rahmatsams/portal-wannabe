<div class="content">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-edit"></i>  New Sample List</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="admin_list.html" class="btn-link"><u>Back</u></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-register col-md-6 mx-auto mt-3">
      <div class="card-body">
        <form id="addProducer" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-label-group">
                <?=(isset($error) && isset($error['list_name']) ? "<div class=\"alert alert-danger\"><strong>List Name </strong>{$error['list_name']}</div>" : '<label>List Name</label>')?>
                    <input name="list_name" type="text" placeholder="List here.." class="form-control" maxlength="50" required>
                </div>
              </div>
              
            </div>
          </div>
          <button type="submit" name="submit" value="create_list" class="btn btn-primary btn-block rounded">Create</button>
        </form>
      </div>
    </div>
  </div>