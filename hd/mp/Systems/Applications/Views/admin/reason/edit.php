<?php 
    $em = $reason['type_id'];
?>
<div class="content">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-edit"></i>  Edit Detail</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="admin_reason.html" class="btn-link"><u>Back</u></a></li>
                        <li class="active"><?=$reason['type_name']?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-register col-md-6 mx-auto mt-3">
      <div class="card-body">
        <form id="editStore" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-label-group">
                <?=(isset($error) && isset($error['type_name']) ? "<div class=\"alert alert-danger\"><strong>Reason </strong>{$error['type_name']}</div>" : '<label>Reason</label>')?>
                    <input name="type_name" type="text" value="<?=$reason['type_name']?>" class="form-control" maxlength="50" required>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" name="submit" value="edit_type" class="btn btn-success btn-block rounded">Edit</button>
        </form>
      </div>
    </div>
  </div>