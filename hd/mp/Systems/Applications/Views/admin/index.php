<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.html" class="btn btn-link"><u>Rawmat</u></a></li>
    <li class="breadcrumb-item active"><a class="btn">Administrator</a></li>
</ol>
<div class="row">
          <!-- Document Type -->
          <div class="col-xl-3 col-sm-6 mb-3 d-none">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa-check-circle"></i>
                </div>
                <div class="mr-5">Document Type</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_document.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-chevron-circle-right fa-lg"></i>
                </span>
              </a>
            </div>
          </div>

          <!-- Sample List -->
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa-bookmark"></i>
                </div>
                <div class="mr-5">Sample List</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_list.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-chevron-circle-right fa-lg"></i>
                </span>
              </a>
            </div>
          </div>

          <!-- Sample Category -->
          <!-- <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa-clipboard"></i>
                </div>
                <div class="mr-5">*Manage Sample Category</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_category.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div> -->

          <!-- Producer -->
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa-list"></i>
                </div>
                <div class="mr-5">Manufacturer</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_producer.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-chevron-circle-right fa-lg"></i>
                </span>
              </a>
            </div>
          </div>
          
          <!-- Supplier -->
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa-shopping-cart"></i>
                </div>
                <div class="mr-5">Supplier</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_supplier.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-chevron-circle-right fa-lg"></i>
                </span>
              </a>
            </div>
          </div>

          <!-- Status -->
          <div class="col-xl-3 col-sm-6 mb-3 d-none">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa-flag"></i>
                </div>
                <div class="mr-5">Sample Status</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_status.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-chevron-circle-right fa-lg"></i>
                </span>
              </a>
            </div>
          </div>

          <!-- Condition -->
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa-cloud"></i>
                </div>
                <div class="mr-5">Sample Condition</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_condition.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-chevron-circle-right fa-lg"></i>
                </span>
              </a>
            </div>
          </div>

          <!-- Reason -->
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-light text-dark o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fa fa-fw fa-lg fa fa-commenting-o"></i>
                </div>
                <div class="mr-5">Rawmat Reason</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="admin_reason.html">
                <span class="float-left" style="color:black">View Details</span>
                <span class="float-right" style="color:black">
                  <i class="fa fa-chevron-circle-right fa-lg"></i>
                </span>
              </a>
            </div>
          </div>
</div>