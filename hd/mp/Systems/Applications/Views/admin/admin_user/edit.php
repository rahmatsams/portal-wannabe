			
			<div class="maincontent">
				<div class="content">
					<h2>Add Administrator</h2>
					<?php
					$data = (count($this->getFlashData()) > 0) ? $this->getFlashData() : array();
					if(isset($data['view']) && isset($data['view']['ok']) && $data['view']['ok'] == 1){
						echo 'A new administrator has been made';
					}
					?>
					<form method="POST" action="<?php echo controllerUrl('admin','editadmins'); ?>" enctype="multipart/form-data">
						<input type="hidden" name="userID" value="<?php echo $userdetails['id_user']; ?>"/>
						<div id="quotation-form">
								<div class="formline">
									<div class="formcolumn">User Name</div>
									<div class="formcolumn">:</div>
									<div class="formcolumn"><input type="text" name="userName" id="userName" value="<?php echo $userdetails['user_name']; ?>" /></div>
								</div>
								<div class="formline">
									<div class="formcolumn">Old Password</div>
									<div class="formcolumn">:</div>
									<div class="formcolumn"><input type="password" name="oldPassword" id="oldPassword" value="<?php if(isset($last_input) && !empty($last_input['firstPassword'])){ echo $last_input['firstPassword']; } ?>" /></div>
								</div>
								<div class="formline">
									<div class="formcolumn">New Password</div>
									<div class="formcolumn">:</div>
									<div class="formcolumn"><input type="password" name="firstPassword" id="firstPassword" value="<?php if(isset($last_input) && !empty($last_input['firstPassword'])){ echo $last_input['firstPassword']; } ?>" /></div>
								</div>
								<div class="formline">
									<div class="formcolumn">Confirm New Password</div>
									<div class="formcolumn">:</div>
									<div class="formcolumn"><input type="password" name="secondPassword" id="secondPassword" value="<?php if(isset($last_input) && !empty($last_input['secondPassword'])){ echo $last_input['secondPassword']; } ?>" /></div>
								</div>
								<div class="formline">
									<div class="formcolumn"><input type="submit" name="edit" value="Edit"></div>
								</div>
							</div>
					</form>
				</div>
			</div>