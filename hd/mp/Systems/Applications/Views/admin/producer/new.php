<div class="content">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-edit"></i>  New Manufacturer</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="admin_producer.html" class="btn-link"><u>Back</u></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-register col-md-6 mx-auto mt-3">
      <div class="card-body">
        <form id="addProducer" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-label-group">
                <?=(isset($error) && isset($error['producer_name']) ? "<div class=\"alert alert-danger\"><strong>Producer Name </strong>{$error['producer_name']}</div>" : '<label>Producer Name</label>')?>
                    <input name="producer_name" type="text" placeholder="Name here.." class="form-control" maxlength="50" required>
                </div>
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <?=(isset($error) && isset($error['producer_address']) ? "<div class=\"alert alert-danger\"><strong>Producer Name </strong>{$error['producer_address']}</div>" : '<label>Producer Address</label>')?>
                    <textarea name="producer_address" id="textarea-input" rows="3" placeholder="Address here.." class="form-control"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                <?=(isset($error) && isset($error['producer_pic']) ? "<div class=\"alert alert-danger\"><strong>Producer Name </strong>{$error['producer_pic']}</div>" : '<label>Producer PIC</label>')?>
                    <input name="producer_pic" type="text" placeholder="PIC here.." class="form-control" maxlength="50" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                <?=(isset($error) && isset($error['producer_phone']) ? "<div class=\"alert alert-danger\"><strong>Producer Name </strong>{$error['producer_phone']}</div>" : '<label>Producer Phone</label>')?>
                    <input name="producer_phone" type="number" placeholder="Phone here.." class="form-control" maxlength="50" required>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" name="submit" value="create_producer" class="btn btn-primary btn-block rounded">Create</button></div>
        </form>
        
      </div>
    </div>
  </div>



					