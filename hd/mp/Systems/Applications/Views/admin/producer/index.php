<style type="text/css">
    /*body {
        font-family: 'Varela Round', sans-serif;
    }*/
    .modal-confirm {        
        color: #636363;
        width: 400px;
    }
    .modal-confirm .modal-content {
        padding: 20px;
        border-radius: 5px;
        border: none;
        text-align: center;
        font-size: 14px;
    }
    .modal-confirm .modal-header {
        border-bottom: none;   
        position: relative;
    }
    .modal-confirm h4 {
        text-align: center;
        font-size: 26px;
        margin: 30px 0 -10px;
    }
    .modal-confirm .close {
        position: absolute;
        top: -5px;
        right: -2px;
    }
    .modal-confirm .modal-body {
        color: #999;
    }
    .modal-confirm .modal-footer {
        border: none;
        text-align: center;     
        border-radius: 5px;
        font-size: 13px;
        padding: 10px 15px 25px;
    }
    .modal-confirm .modal-footer a {
        color: #999;
    }       
    .modal-confirm .icon-box {
        width: 80px;
        height: 80px;
        margin: 0 auto;
        border-radius: 50%;
        z-index: 9;
        text-align: center;
        border: 3px solid #f15e5e;
    }
    .modal-confirm .icon-box i {
        color: #f15e5e;
        font-size: 46px;
        display: inline-block;
        margin-top: 13px;
    }
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
        background: #60c7c1;
        text-decoration: none;
        transition: all 0.4s;
        line-height: normal;
        min-width: 120px;
        border: none;
        min-height: 40px;
        border-radius: 3px;
        margin: 0 5px;
        outline: none !important;
    }
    .modal-confirm .btn-info {
        background: #c1c1c1;
    }
    .modal-confirm .btn-info:hover, .modal-confirm .btn-info:focus {
        background: #a8a8a8;
    }
    .modal-confirm .btn-danger {
        background: #f15e5e;
    }
    .modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
        background: #ee3535;
    }
    .trigger-btn {
        display: inline-block;
        margin: 100px auto;
    }
</style>

    <div class="container-fluid">
        <ol class="breadcrumb">
             <li class="breadcrumb-item"><a href="administrator.html" class="btn btn-link"><u>Administrator</u></a></li>
            <li class="breadcrumb-item active"><a class="btn">Manufacturer</a></li>
        </ol>
        <div class="table-responsive">
            <a href="new_producer.html" class="btn btn-primary btn-md waves-effect float-right fa fa-plus-circle rounded"> New</a><br><br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Producer</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>PIC</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($producer) && !empty($producer)){
                            $num = (($page-1)*10)+1;
                            foreach($producer as $result){
                                /*switch($result['email_status']){
                                    case 1:
                                        $status = 'Enabled';
                                        break;
                                    default:
                                        $status = 'Disabled';
                                }*/
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['producer_name']}</td>
                        <td>{$result['producer_address']}</td>
                        <td>{$result['producer_phone']}</td>
                        <td>{$result['producer_pic']}</td>
                        <td><a href=\"edit_producer_{$result['producer_id']}.html\" class='edit-users btn btn-success rounded btn-small menu-icon fa fa-pencil'></a>
                            <a href=\"#\" class=\"admin_delete_producer deleteconfirmationmodal\" data-id=\"{$result['producer_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"menu-icon rounded fa fa-trash btn btn-danger btn-md\"></button>
                            </a>
                        </td>
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="6">Ups Sorry, There's no producer data found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
            $pages = ceil($total/$max_result); #paginationbootstrap
                if($pages > 1){
                    echo '
                <nav>
                    <ul class="pagination pagination-sm">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>';
                    for($i=1;$i <= $pages;$i++){
                        echo "<li ". ($page == $i ? 'class="page-item active"' : '') ."><a class=\"page-link\" href='admin_producer_{$i}.html'>{$i}</a></li>";
                    }
                    echo '
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                    </li>
                    </ul>
                </nav>';
                }
            ?>
        </div>

        <!-- DELETE PRODUCER -->
        <div id="modalconfirmdelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="icon-box">
                            <i class="fa fa-trash"> </i>
                        </div>
                        <h4 class="modal-title"><strong>Are you sure?</strong></h4><br>
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                    <div class="modal-body" style="align-content: center;">
                        <button type="button" class="btn btn-info" data-dismiss="modal" style="font-family: 'Varela Round', sans-serif;">Cancel</button>
                        <a id="delete_producer" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger rounded" style="font-family: 'Varela Round', sans-serif;">Delete</button></a> 
                    </div>
                </div>
            </div>
        </div>


           