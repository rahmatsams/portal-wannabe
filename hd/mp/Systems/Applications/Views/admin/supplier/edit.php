<?php 
    $s = $supplier['supplier_id'];
?>

<div class="content">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-edit"></i>  Edit Detail</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="admin_supplier.html" class="btn-link"><u>Back</u></a></li>
                        <li class="active"><?=$supplier['supplier_name']?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-register col-md-6 mx-auto mt-3">
      <div class="card-body">
        <form id="addSupplier" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-label-group">
                <?=(isset($error) && isset($error['supplier_name']) ? "<div class=\"alert alert-danger\"><strong>Supplier Name </strong>{$error['supplier_name']}</div>" : '<label>Supplier Name</label>')?>
                    <input name="supplier_name" type="text" class="form-control" maxlength="50" value="<?=$supplier['supplier_name']?>" required>
                </div>
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <?=(isset($error) && isset($error['supplier_address']) ? "<div class=\"alert alert-danger\"><strong>supplier Name </strong>{$error['supplier_address']}</div>" : '<label>Supplier Address</label>')?>
                <textarea name="supplier_address" id="textarea-input" rows="3" placeholder="Address here.." class="form-control"><?=$supplier['supplier_address']?></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <?=(isset($error) && isset($error['supplier_pic']) ? "<div class=\"alert alert-danger\"><strong>supplier Name </strong>{$error['supplier_pic']}</div>" : '<label>Supplier PIC</label>')?>
                    <input name="supplier_pic" type="text" placeholder="PIC here.." class="form-control" maxlength="50"  value="<?=$supplier['supplier_pic']?>" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <?=(isset($error) && isset($error['supplier_phone']) ? "<div class=\"alert alert-danger\"><strong>supplier Name </strong>{$error['supplier_phone']}</div>" : '<label>Supplier Phone</label>')?>
                    <input name="supplier_phone" type="number" placeholder="Phone here.." class="form-control" maxlength="50" value="<?=$supplier['supplier_phone']?>" required>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" name="submit" value="edit_supplier" class="btn btn-success btn-block rounded">Edit</button>
        </form>
      </div>
    </div>
  </div>
                    