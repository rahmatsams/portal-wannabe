					<div class="content-right">
						<h3 class="well">Add New Outlet</h3>
						<div class="row">
                            
                            <form id="addStore" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['store_name']) ? "<div class=\"alert alert-danger\"><strong>Outlet Name </strong>{$error['store_name']}</div>" : '<label>Outlet Name</label>')?>
                                            <input name="store_name" type="text" placeholder="Type store name.." class="form-control" maxlength="25" required>
                                        </div>
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['store_location']) ? "<div class=\"alert alert-danger\"><strong>Outlet Location </strong>{$error['store_location']}</div>" : '<label>Outlet Location</label>')?>
                                            <select name="store_location" class="form-control" required>
                                                <?php
                                                    if(is_array($location_list) && count($location_list) > 0){
                                                        foreach($location_list as $location ){
                                                            echo "
                                                <option value=\"{$location['location_id']}\">{$location['location_name']}</option>
                                                            ";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <!-- Store Email -->
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['store_email']) ? "<div class=\"alert alert-danger\"><strong>Outlet Email </strong>{$error['store_email']}</div>" : '<label>Outlet Email</label>')?>
                                            <input name="store_email" type="email" placeholder="Type store email.." class="form-control" maxlength="50" required>
                                        </div>

                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['store_status']) ? "<div class=\"alert alert-danger\"><strong>Status </strong>{$error['store_status']}</div>" : '<label>Status</label>')?>
                                            <select name="store_status" class="form-control">
                                                <option value="0">Disabled</option>
                                                <option value="1">Enabled</option>
                                            </select>
                                        </div>
                                        <button type="submit" name="submit" value="create_store" class="btn btn-primary">Create</button>
                                    </div>
                            </form>
                         </div>
					</div>
					