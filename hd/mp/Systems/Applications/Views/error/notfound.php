<!DOCTYPE html>
<html lang="en">
    <head>
        <title>RAWMAT</title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <style>a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:""}table{border-collapse:collapse;border-spacing:0}html{height:100%}body{font-family:-apple-system,\.SFNSText-Regular,Helvetica Neue,Roboto,Segoe UI,sans-serif;-webkit-font-smoothing:antialiased;font-size:16px;min-height:100%;background-color:#000;color:#fff}a{text-decoration:none}.background{position:absolute;top:0;left:0;width:100%;height:100%;background:url(../img/404/bg.gif) 50% no-repeat;background-size:cover;z-index:-1;opacity:.2}header{height:48px;background-color:#000}.logo{display:block;width:64px;height:48px}.message{text-align:center;position:absolute;width:100%;top:50%;transform:translateY(-50%)}.message h1{font-size:144px;font-weight:100;margin-bottom:16px}.message h2{margin-bottom:24px}.message h3{font-weight:700;font-size:16px;margin-bottom:32px}.lost{display:block;position:absolute;bottom:0;left:50%;margin-left:-111px}.btn-download{display:inline-block;line-height:20px;padding:12px 24px 12px 48px;background:#09f url(../img/404/download.svg) left -1px no-repeat;color:#fff;font-weight:700;border-radius:2px}.btn-download:hover{background-color:#3caffc}.toast{position:absolute;width:100%;top:100px;left:0;z-index:13;color:#000;text-align:center;height:0}.toast p{display:inline-block;background-color:#ffe168;padding:5px 10px;line-height:20px;border-radius:3px;box-shadow:0 3px 5px rgba(0,0,0,.3);position:relative}.toast p.close{padding-right:35px}.toast.toast-color-green p{background-color:#690}.toast.toast-color-yellow p{background-color:#ffe168}.toast.toast-color-red p{background-color:#c00}.toast a.btn-close{color:#000;position:absolute;top:5px;right:10px}
        /*# sourceMappingURL=error.css.map*/
        </style>
        <link rel="shortcut icon" href="https://assets-9gag-fun.9cache.com/s/fab0aa49/174d0b1ec891e220e79a7cb3af66a1fbb1eed154/static/dist/core/img/favicon.ico" />
        
    </head>
    <body>

        <div class="background">
        </div><!-- / background -->

        <header class="top-nav">
            <a class="logo" href="https://9gag.com/?ref=404">
                <svg xmlns="http://www.w3.org/2000/svg" width="64px" height="48px" viewBox="0 0 64 48" version="1.1"><title>logo</title><desc>Created with Sketch.</desc><g id="Asset" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="logo" fill="#FFFFFF"><path d="M32 12L21 18.25 21 20.75 32 27 38.6 23.25 38.6 28.25 32 32 25.4 28.25 21 30.75 32 37 43 30.75 43 18.25 32 12ZM27.6 19.5L32 17 36.4 19.5 32 22 27.6 19.5 27.6 19.5Z" id="Icon"/></g></g></svg>
            </a><!-- / logo -->
            <div class="confused"></div>
        </header>

        <div class="toast badge-toast-container" style="display:none">
    <p class="close">
    <span class="badge-toast-message"></span>
<a class="btn-close badge-toast-close" href="#">&#10006;</a>
</p>
</div>

        <div class="message">
            <h1>404</h1>
            <h2>There's nothing here.</h2>
        </div><!-- / message -->

        
<!-- Ads targeting for page -->
</body>
</html>
