
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-user"></i>  Follow Up Sample</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="card"> 
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="tableFU">
                    <thead>
                        <tr class="bg-primary text-white">
                            <th>&nbsp;</th>
                            <th>Request</th>
                            <th>Sample</th>
                            <th>Supplier</th>
                            <th>Manufacturer</th>
                            <th>Condition</th>
                            <th>Sample Date</th>
                            <th>Action</th>
                            <th>&nbsp;</th>
                            <?php (($qa || $purchasing || $admin) && (!empty($sample[0]['request_closed']) && $sample[0]['request_closed'] == 0)) ? '<th>&nbsp;</th>' : ''; ?>
                        </tr>
                    </thead>
                    <tbody>
        <?php 
            if(is_array($sample) && !empty($sample)){
                $i = 1;
                foreach($sample as $r){
                    $lu = (!empty($r['sample_date']) ? date('d-M-Y', strtotime($r['sample_date'])) : '-');
                    $o = "
                        <tr>
                            <td>{$i}</td>
                            <td><a href='view_request_{$r['request_id']}.html' class='btn-link d-block'>{$r['request_name']}</a></td>
                            <td><a href='view_sample_{$r['sample_id']}.html' class='btn-link d-block'>{$r['sample_name']}</a></td>
                            <td>{$r['supplier_name']}</td>
                            <td>{$r['producer_name']}</td>
                            <td>{$r['condition_name']}</td>
                            <td>{$lu}</td>";
            
                    if($r['request_closed'] == 0){ 
                        if($fnb && $r['sample_status'] == 1){
                            $o .= "
                            <td><a href='fnb_release_{$r['sample_id']}.html' class='btn btn-success btn-block'><i class='fa fa-check'></i> Release</a></td>
                            <td><a href='fnb_reject_{$r['sample_id']}.html' class='btn btn-danger btn-block'><i class='fa fa-times'></i> Reject</a></td>";
                        }elseif($qa && $r['sample_status'] == 5 && $r['request_closed'] == 0){
                            $o .= "
                            <td><a href='qa_release_{$r['sample_id']}.html' class='btn btn-success btn-block'><i class='fa fa-check'></i> Release</a></td>
                            <td><a href='qa_reject_{$r['sample_id']}.html' class='btn btn-danger btn-block'><i class='fa fa-times'></i> Reject</a></td>";
                        }elseif($kahi && $r['sample_status'] == 6 && $r['request_closed'] == 0){
                            $o .= "
                            <td><a href='kahi_release_{$r['sample_id']}.html' class='btn btn-success btn-block'><i class='fa fa-check'></i> Release</a></td>
                            <td><a href='kahi_reject_{$r['sample_id']}.html' class='btn btn-danger btn-block'><i class='fa fa-times'></i> Reject</a></td>";
                        }elseif(($admin || $purchasing) && $r['sample_status'] == 1 && $r['request_closed'] == 0){
                            $o .= "
                            <td><a href='edit_sample_{$r['sample_id']}.html' class='btn btn-success btn-block'><i class='fa fa-pencil'></i> Edit</a></td>
                            <td>&nbsp;</td>";
                        }
                            
                        if(($qa || $purchasing || $admin) && $r['request_closed'] == 0){
                            $o .= "
                            <td><a href='modul_document_{$r['sample_id']}.html' class='btn btn-warning btn-block'><i class='fa fa-plus-circle'></i> Document</a></td>";
                        }
                    }else{
                        $o .= '
                        <td></td>
                        <td></td>
                        ';
                    }
                    $o .= '
                    </tr>';
                    echo $o;
                    $i++;
                }
            }
        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

