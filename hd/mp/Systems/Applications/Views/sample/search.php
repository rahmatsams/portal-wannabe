
<div class="content">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-user"></i>  Search Sample</h1>
                </div>
            </div>
        </div>
    </div>
    
    <div class="card col-lg-12 px-0 mb-4"> 
        <div class="card-body">
            <div class="col-md-12">
                <form action="search_sample.html" method="POST" class="form-row">
                    <div class="col-7">
                        <input type="text" name="sample_name" class="form-control mr-2" placeholder="Search sample by name .." <?=(!empty($post['sample_name']) ? "value='{$post['sample_name']}' " : '')?>required>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2 rounded"><i class="fa fa-search"></i> Submit</button>
                </form>
            </div>
            <?php if(!empty($sample) && is_array($sample)): ?>
            
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-primary text-white">
                            <th>&nbsp;</th>
                            <th>Sample</th>
                            <th>Supplier</th>
                            <th>Manufacturer</th>
                            <th>Condition</th>
                            <th>Sample Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i = 1;
                        foreach($sample as $r): ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><a href="view_sample_<?=$r['sample_id']?>.html" class='btn-link d-block'><?=$r['sample_name']?></a></td>
                            <td><?=$r['supplier_name']?></td>
                            <td><?=$r['producer_name']?></td>
                            <td><?=ucfirst($r['condition_name'])?></td>
                            <td><?=date('d-M-Y', strtotime($r['sample_date']))?></td>
                            <td><?=$r['status_name']?></td>
                        </tr>
                    <?php 
                        $i++;
                        endforeach; 
                    ?>
                    

                    </tbody>
                </table>
            </div>
            <?php elseif(!empty($post)): ?>
            
                <div class="col-md-12 p-3 bg-warning">
                    Sample not found
                </div>
                
            <?php endif;?>
        </div>
    </div>
</div>
<div id="modalSample" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        Image
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6 border-right border-top border-left">
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">ID</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Type</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Supplier</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Manufacturer</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Condition</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Size</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Price</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Submit Date</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Current Status</div>
                                    <div class="col-md-7">: </div>
                                </div>
                            </div>
                            <div class="col-md-6 border-left border-top">
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Request Name</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Request Deadline</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    <div class="col-md-5">Request Name</div>
                                    <div class="col-md-7">: </div>
                                </div>
                                <div class="row border-bottom p-1">
                                    &nbsp;
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5">Documents</div>
                                    <div class="col-md-7">: </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

