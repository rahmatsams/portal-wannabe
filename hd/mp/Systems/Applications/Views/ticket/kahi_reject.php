<div class="content">
    <div class="breadcrumbs">                    
        <div class="page-header float-left">
            <div class="page-title">
                <h1><i class="fa fa-user"></i> Kahi Reject</h1>
            </div>
        </div>
        <div class="col-sm-10">
        </div>
    </div>
    <div class="card col-lg-12 px-0 mb-2 bg-danger text-white"> 
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Sample ID
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=$result['sample_id']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Request
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=$request['request_name']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Reason
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=$request['type_name']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Deadline
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=date('d-M-Y', strtotime($request['deadline_date']))?>
                </div>
            </div>
        </div>
    </div>
    <!-- BODY -->
    <div class="card">
        <div class="card-body card-block">
            <form action="kahi_reject.html" method="POST" enctype="multipart/form-data" class="form-horizontal">
                <!-- doc type -->
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Content (*)</label>
                    </div>
                    <input id="catid" name="ticket_sample" type="hidden" type="text" value="<?=$result['sample_id']?>" maxlength="6" required>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="hidden" id="text-input" name="ticket_sample" required="" value="<?=$result['sample_id']?>">
                            <textarea name="log_content" id="textarea-input" rows="5" placeholder="Type Here.." class="form-control" required=""></textarea>
                        </div>
                        <small class="form-text text-muted" >*Sample Testing Method</small>
                    </div>
                </div> <!-- end doc type -->
                
                <div class="col-md-12 form-group">
                    <input type='submit' class='btn btn-md btn-danger rounded' name="action" value="Kahi Reject">
                </div>
            </form>
        </div>                                    
    </div>    
</div>