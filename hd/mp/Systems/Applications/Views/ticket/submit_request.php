<style>
    #rcorners2 {
  border-radius: 25px;
  border: 2px solid #73AD21;
  padding: 20px; 
  width: 200px;
  height: 150px;  
}
</style>
<div class="content">
    <div class="breadcrumbs">                    
        <div class="page-header float-left">
            <div class="page-title">
                <h1><i class="fa fa-book"></i> Request Rawmat</h1>
            </div>
        </div>
        <div class="col-sm-10">
        </div>
    </div>
    <!-- BODY -->
    <div class="card">
        <div class="card-body card-block">
            <form action="submit_request.html" method="POST" enctype="multipart/form-data" class="form-horizontal">
                <!-- doc type -->
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Rawmat Name (*)</label>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="text" id="text-input" name="request_name" placeholder="you cannot leave this blank.." class="form-control" required="">
                        </div>
                    </div>
                </div> <!-- end doc type -->
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Reason (*)</label>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <select name="request_type" id="select" class="form-control" required="">
                                <?php 
                                    if (is_array($request_type) && count($request_type) > 0) {
                                        foreach ($request_type as $val) {
                                            echo "<option value=\"{$val['type_id']}\">{$val['type_name']}</option>";
                                            }
                                        }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Deadline (*)</label>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="hidden" id="text-input" name="request_date" class="form-control">
                            <input type="date" id="text-input" name="deadline_date" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col col-md-6"><input type="submit" value="Submit" name="submit_request" class="btn btn-block btn-primary btn-md rounded">
                    </div>
                    <div class="col col-md-6"><button type="reset" class="btn btn-block btn-danger btn-md rounded">Reset</button>
                    </div>
                </div>
              
            </form>
        </div>                                    
    </div>    
</div>