        <div class="content">
            <div class="breadcrumbs">
                <div class="col-sm-4">                    
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1><i class="fa fa-file"></i> New Document</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="view_request_<?=$result['request_id']?>.html" class="btn btn-sm btn-danger rounded"><i class="fa fa-repeat"></i> Back to Request</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="breadcrumbs">                    
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><i class="fa fa-file"></i> New Document</h1>
                    </div>
                </div>
                <div class="col-sm-10">
        </div>
            </div> -->
            <div class="card col-lg-12 px-0 mb-2 bg-success text-white"> 
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            Sample ID
                        </div>
                        <div class="col-sm-6 col-md-3">
                            :  <?=$result['sample_id']?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            Request
                        </div>
                        <div class="col-sm-6 col-md-3">
                            :  <?=$request['request_name']?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            Reason
                        </div>
                        <div class="col-sm-6 col-md-3">
                            :  <?=$request['type_name']?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            Deadline
                        </div>
                        <div class="col-sm-6 col-md-3">
                            :  <?=date('d-M-Y', strtotime($request['deadline_date']))?>
                        </div>
                    </div>
                </div>
            </div>
                    
            <div class="card">
                <div class="card-body card-block">
                    <form action="modul_document.html" method="POST" enctype="multipart/form-data" class="form-horizontal">
                        <!-- doc type -->
                        <div class="row">
                            <div class="col col-md-3">
                                <label class=" form-control-label">Document Type</label>
                            </div>
                            <input id="catid" name="ticket_sample" type="hidden" type="text" value="<?=$result['sample_id']?>" maxlength="6" required>
                            <div class="col">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">*</div>
                                    </div>
                                    <select name="document_type" id="selectDocument" class="form-control " placeholder="Doc" required>
                                        <option value="">- Select Type -</option>
                                    <?php if (is_array($doc) && count($doc) > 0): ?>
                                    <?php foreach ($doc as $val): ?>
                                    
                                         <option value='<?=$val['type_id']?>'><?=$val['type_name']?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div> <!-- end doc type -->
                        
                        <div class="row">
                            <div class="col col-md-3"><label for="file-input" class=" form-control-label">Document</label></div>
                            <div class="col-12 col-md-9 form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">*</div>
                                    </div>
                                    <input type="file" id="file-input" name="modul_document[0]" class="form-control" required>
                                </div>
                            </div>
                        </div>


                        <div class="row form-group sh-form d-none">
                            <div class="col col-md-3"><label class=" form-control-label">Expired (*)</label></div>
                             <div class="col-12 col-md-9 form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">*</div>
                                    </div>
                                    <input type="date" name="expired" class="form-control sh-input">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group sh-form d-none">
                            <div class="col col-md-3"><label class=" form-control-label">Issued By</label></div>
                            <div class="col-12 col-md-9 form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">*</div>
                                    </div>
                                    <input type="text" id="file-input" name="issued" class="form-control sh-input">
                                </div>
                            </div>                            
                        </div>
                        <div class="row form-group sh-form d-none">
                            <div class="col col-md-3"><label class=" form-control-label">Acknowledged by MUI (*)</label></div>
                            <div class="col-12 col-md-9 form-group">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">*</div>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="acknowledged" value="1" class="sh-input">Yes</label>
                                        &nbsp;
                                        <label><input type="radio" name="acknowledged" value="0">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 form-group">
                            <button type="submit" name="submit" value="qa_doc" class="btn btn-success btn-sm rounded">Add Doc</button>
                        </div>
                    </form>
                </div>
                                            
            </div>      
        </div>