<div class="content">
    <div class="breadcrumbs">                    
        <div class="page-header float-left">
            <div class="page-title">
                <h1><i class="fa fa-book"></i> Submit New Sample</h1>
            </div>
        </div>
    </div>
    <div class="card col-lg-12 px-0 mb-1 bg-success text-white"> 
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Request
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=$result['request_name']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Reason
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=$result['type_name']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Deadline
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=date('d-M-Y', strtotime($result['deadline_date']))?>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body card-block">
            <form method="POST" enctype="multipart/form-data" class="form-horizontal" id="newSample">
                <div class="row">
                    <div class="col">
                        <input id="catid" name="request_id" type="hidden" type="text" value="<?=$result['request_id']?>" maxlength="6" required>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="text" id="text-input" name="sample_name" placeholder="Sample Name" class="form-control " required>
                        </div>
                    </div>
                     <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <select name="sample_list" id="select" class="form-control " placeholder="List" required="">
                                <option value="">- Sample Type -</option>
                            <?php if (is_array($list) && count($list) > 0): ?>
                            <?php foreach ($list as $val): ?>
                            
                                 <option value='<?=$val['list_id']?>'><?=$val['list_name']?></option>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col" id="selectSupplier">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <select name="supplier" class="form-control" required="">
                                <option value="">- Supplier -</option>
                                <?php if (is_array($supplier) && count($supplier) > 0): ?>
                                <?php foreach ($supplier as $val) : ?>
                                
                                    <option value='<?=$val['supplier_id']?>'><?=$val['supplier_name']?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col d-none" id="newSupplier">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="text" class="form-control" name="supplier_name" placeholder="Input New Supplier" disabled>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-check pt-2">
                          <input class="form-check-input" type="checkbox" value="true" name="create_sup" id="supIsNew">
                          <label class="form-check-label" for="supIsNew">
                            New Supplier
                          </label>
                        </div>
                    </div>
                    <div class="col" id="selectManufacturer">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <select name="producer" class="form-control">
                                <option value="">- Manufacturer -</option>
                                <?php if (is_array($producer) && count($producer) > 0): ?>
                                <?php foreach ($producer as $val) : ?>
                                
                                    <option value='<?=$val['producer_id']?>'><?=$val['producer_name']?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col d-none" id="newManufacturer">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="text" class="form-control" name="manufacturer_name" placeholder="Input New Manufacturer" disabled>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-check pt-2">
                          <input class="form-check-input" type="checkbox" value="true" name="create_man" id="manIsNew">
                          <label class="form-check-label" for="manIsNew">
                            New Manufacturer
                          </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="text" id="text-input" name="sample_size" placeholder="Size" class="form-control " required>
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <select name="sample_condition" id="select" class="form-control " required>
                                <option value="">- Condition -</option>
                                <?php if (is_array($condition) && count($condition) > 0): ?>
                                <?php foreach ($condition as $v) : ?>
                            
                                    <option value="<?=$v['condition_id']?>"><?=$v['condition_name']?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <select name="currency" class="form-control">
                                        <option value="IDR" selected>IDR</option>
                                        <option value="USD">USD</option>
                                        <option value="SGD">SGD</option>
                                        <option value="JPY">JPY</option>
                                        <option value="RM">RM</option>
                                    </select>
                                </div>
                                <input type="number" min="0.01" step="0.01" id="text-input" name="sample_price" placeholder="Price" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">*</div>
                                </div>
                                <input type="file" accept="image/*" id="img_0" name="sample_image[0]" class="form-control p-1">
                                <div class="input-group-append">
                                    <div class="input-group-text">Upload Sample Image</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row d-none bg-info mb-1 pt-2" id="documentUpload">
                    <?php foreach($doctype as $doc) :?>
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="file-input" class="form-control-label font-weight-bold"><?=$doc['type_name']?></label>
                            <input type="file" id="file-input" name="sample_cert['<?=$doc['type_name']?>']" class="form-control p-1">
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="row mt-4">
                    <div class="col-md-3 d-none">
                        <div class="form-group">
                            <button type="button" id="showDocument" class="form-control btn btn-md btn-secondary rounded"><i class="fa fa-file-o"></i> Include Document</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class='form-control btn btn-md btn-primary rounded' name="action" value="Add Sample"><i class="fa fa-plus-circle"></i> Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                                    
    </div>      
</div>
<!-- Central Modal Medium Success -->
<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-success" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Success</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>
          <p>New sample successfully created</p>
        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a href="view_request_<?=$_GET['id']?>.html" type="button" class="btn btn-success">Back to Request <i class="far fa-gem ml-1 text-white"></i></a>
        <a type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal" id="sampleLoc">Check the sample</a>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!-- Central Modal Medium Success-->