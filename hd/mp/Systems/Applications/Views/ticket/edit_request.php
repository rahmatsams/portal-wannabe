<div class="col-lg-12">
    <div class="card">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="view_request_<?=$request['request_id']?>.html">Sample List <?=$request['request_id']?></a></li>
            <li class="breadcrumb-item"><strong>Edit Request</strong></li>
        </ol>
    </nav>
    <div class="card-body card-block">
        <form method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" enctype="multipart/form-data" class="form-horizontal">
        <div class="row form-group">
            <div class="col col-md-3"><label class=" form-control-label">Request Name</label></div>
            <div class="col-12 col-md-9">
                <input type="text" id="text-input" name="request_name" value="<?=$request['request_name']?>" class="form-control"><small class="form-text text-muted">This is a help text</small>
                <!-- <input type="text" id="text-input" name="request_id" value="<?=$request['request_id']?>" > -->
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Request Reason</label></div>
            <div class="col-12 col-md-9">
            <select name="request_type" id="select" class="form-control" required="">
                <option value="1" <?=($request['request_type'] == '1' ? 'selected' : '')?>>Promo Material</option>
                <option value="2" <?=($request['request_type'] == '2' ? 'selected' : '')?>>Alternatif Material</option>
                <option value="3" <?=($request['request_type'] == '3' ? 'selected' : '')?>>Pergantian Material</option>
            </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label class=" form-control-label" hidden="">Request Date</label></div>
            <div class="col-12 col-md-9">
                <input type="hidden" id="text-input" name="request_date" class="form-control">
            </div>
        </div>
        <div class="row form-group">
        <div class="col col-md-3"><label class=" form-control-label">Request Deadline</label></div>
            <div class="col-12 col-md-9">
                <input type="date" id="text-input" name="deadline_date" class="form-control" value="<?php echo date('Y-m-d',strtotime($request["deadline_date"])) ?>">
            </div>
        </div>
        <tr>
            <td colspan=6 class="fa fa-dot-circle-o"><input type="submit" value="Edit" name="edit_request" class="btn btn-success btn-md rounded"></td>
        </tr>
                                                            
        </form>   
    </div>
</div>      