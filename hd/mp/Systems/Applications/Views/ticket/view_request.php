<?php 
    $date = $result['deadline_date'];
?>
<div class="content">
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-user"></i>  Request Detail</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <?php if(is_array($list) && !empty($list)): ?>
                        <?php
                            $i = 1;
                            foreach($list as $re): ?>

                        <?php if ($admin && $re['sample_status'] == 7 && $result['request_closed'] == 0): ?>
                            <li><a href='#' class="admin_rawmat_closed btn btn-danger btn-sm rounded float-right" data-toggle="modal" data-id="<?=$result['request_id']?>" data-target="#modalconfirmclosed"><i class="fa fa-times"></i> Close Request</a></li>
                        <?php endif;?>

                        <?php 
                            $i++;
                            endforeach; 
                        ?>
                    <?php endif;?>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="card col-lg-12 px-0 mb-2"> 
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Name
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=$result['request_name']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Reason
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=$result['reason']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Deadline (*)
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=date('d-M-Y', strtotime($result['deadline_date']))?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Last Update
                </div>
                <div class="col-sm-6 col-md-3">
                    :  <?=date('d-M-Y', strtotime($result['last_update']))?>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-sm-6 col-md-3">
                    <?php if ($admin): ?>
                        <br><a href='edit_request_<?=$result['request_id']?>.html' class="btn btn-success btn-sm rounded"><i class="fa fa-plus-circle"></i> Edit</a>
                    <?php endif;?>
                </div>
            </div> -->
        </div>
    </div>
    <div class="card col-lg-12 px-0 mb-4"> 
        <div class="card-header">
            <div class="col-md-9">
                <strong class="card-title">Sample List</strong>
            </div>
            <?php if (($admin || $purchasing) && $result['request_closed'] == 0): ?>
            <div class="col-md-3">
                <a href='submit_sample_<?=$result['request_id']?>.html' class="btn btn-primary btn-sm rounded float-right"><i class="fa fa-plus-circle"></i> New Sample</a>
            </div>
            <?php endif;?>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-primary text-white">
                            <th>&nbsp;</th>
                            <th>Sample</th>
                            <th>Supplier</th>
                            <th>Manufacturer</th>
                            <th>Condition</th>
                            <th>Sample Date</th>
                            <th>Status</th>
                            <th colspan="3" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(is_array($list) && !empty($list)): ?>
                    <?php
                        $i = 1;
                        foreach($list as $r): ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><a href="view_sample_<?=$r['sample_id']?>.html" class='btn-link d-block'><?=$r['sample_name']?></a></td>
                            <td><?=$r['supplier_name']?></td>
                            <td><?=$r['producer_name']?></td>
                            <td><?=ucfirst($r['condition_name'])?></td>
                            <td><?=date('d-M-Y', strtotime($r['sample_date']))?></td>
                            <td><?=$r['status_name']?></td>
                            
                            <?php if($fnb && $r['sample_status'] == 1 && $result['request_closed'] == 0) :?>
                            <td>
                                <a href="fnb_release_<?=$r['sample_id']?>.html" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Release</a>
                            </td>
                            <td>
                                <a href="fnb_reject_<?=$r['sample_id']?>.html" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Reject</a>    
                            </td>
                            <?php elseif($qa && $r['sample_status'] == 5 && $result['request_closed'] == 0) :?>
                            <td>
                                <a href="qa_release_<?=$r['sample_id']?>.html" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Release</a>
                            </td>
                            <td>
                                <a href="qa_reject_<?=$r['sample_id']?>.html" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Reject</a>
                            </td>
                            <?php elseif($kahi && $r['sample_status'] == 6 && $result['request_closed'] == 0) :?>
                            <td>
                                <a href="kahi_release_<?=$r['sample_id']?>.html" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Release</a>
                            </td>
                            <td>
                                <a href="kahi_reject_<?=$r['sample_id']?>.html" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Reject</a>
                            </td>
                            <?php elseif(($admin || $purchasing) && $r['sample_status'] == 1 && $result['request_closed'] == 0): ?>
                            <td colspan="2">
                                <a href="edit_sample_<?=$r['sample_id']?>.html" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Edit Sample</a>
                            </td>
                            <?php else:?>
                            <td colspan="2">&nbsp;</td>
                            <?php endif;?>
                            <?php if(($qa || $purchasing || $admin) && $result['request_closed'] == 0): ?>
                            <td>
                                <a href="modul_document_<?=$r['sample_id']?>.html" class="btn btn-warning btn-sm"><i class="fa fa-plus-circle"></i> Upload Document</a>
                            </td>
                            <?php endif; ?>
                        </tr>
                    <?php 
                        $i++;
                        endforeach; 
                    ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="8">There's no sample found</td>
                        </tr>
                    <?php endif;?>

                    </tbody>
                </table>
            </div>

        <!-- MODAL CLOSE RAWMAT -->
        <div id="modalconfirmclosed" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="icon-box">
                            <center><i class="fa fa-shopping-bag fa-lg"> </i></center>
                        </div>
                        <h4 class="modal-title"><strong>This Request ID:<?=$result['request_id']?> - <?=$result['request_name']?> will be Closed</strong></h4><br>
                        <p>Do you really want to close this request? This process cannot be undone.</p>
                    </div>
                    <div class="modal-body" style="align-content: center;">
                        <button type="button" class="btn btn-light rounded" data-dismiss="modal" style="font-family: 'Varela Round', sans-serif;">Cancel</button>
                        <a id="rawmat_closed" href="#"><button id="deleteconfirm" type="button" class="btn btn-warning rounded" style="font-family: 'Varela Round', sans-serif;">&nbsp; Close Request &nbsp;</button></a> 
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
</div>

