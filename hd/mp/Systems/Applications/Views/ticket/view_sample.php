<div class="container-fluid">
    <div class="breadcrumbs">
      <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-archive"></i>  Sample Detail </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="card border-primary mb-3">
        <div class="card-header bg-primary text-white"><span class="text-uppercase font-weight-bold"><?=$sample['sample_name']?></span> <span class="float-right"><a href="<?=getConfig('base_url')."print_sample_{$sample['sample_id']}"?>.html" class="btn btn-sm btn-success rounded" target="_blank"><i class="fa fa-file-pdf-o"></i> Print Preview</a> <a href="view_request_<?=$sample['request_id']?>.html" class="btn btn-sm btn-danger rounded"><i class="fa fa-repeat"></i> Back to Request</a></span></div>
        
        <div class="card-body text-dark">
            <div class="row">
                <div class="col-md-3">
                    <?=(!empty($sample['document_name']) && file_exists("{$sample['document_location']}/{$sample['document_name']}") ? "<a href='{$sample['document_location']}/{$sample['document_name']}' target='_blank' onclick=\"window.open('{$sample['document_location']}/{$sample['document_name']}', 'newwindow', 'width=500,height=450'); return false;\"><img style='width:200px; height:200px;' src=\"{$sample['document_location']}{$sample['document_name']}\"></a>" : "<img style='width:200px; height:200px;' src=\"./Resources/images/image-not-found.jpg\">")?>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 border-right border-top border-left">
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">ID</div>
                                <div class="col-md-7">: #<?=$sample['sample_id']?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Type</div>
                                <div class="col-md-7">: <?=$sample['list_name']?> List</div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Supplier</div>
                                <div class="col-md-7">: <?=$sample['supplier_name']?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Manufacturer</div>
                                <div class="col-md-7">: <?=$sample['producer_name']?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Condition</div>
                                <div class="col-md-7">: <?=$sample['condition_name']?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Size</div>
                                <div class="col-md-7">: <?=$sample['sample_size']?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Price</div>
                                <div class="col-md-7">: <?="{$sample['sample_currency']} ". (function_exists('money_format') ? money_format('%!n', $sample['sample_price']) : money_format_rawmat('%!n', $sample['sample_price']))?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Submit Date</div>
                                <div class="col-md-7">: <?=date("d-M-Y", strtotime($sample['sample_date']))?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Current Status</div>
                                <div class="col-md-7">: <?=$sample['status_name']?></div>
                            </div>
                        </div>
                        <div class="col-md-6 border-left border-top">
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Request Name</div>
                                <div class="col-md-7">: <?=$sample['request_name']?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Request Deadline</div>
                                <div class="col-md-7">: <?=date("d-M-Y", strtotime($sample['deadline']))?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                <div class="col-md-5">Request Reason</div>
                                <div class="col-md-7">: <?=$sample['request_type']?></div>
                            </div>
                            <div class="row border-bottom p-1">
                                &nbsp;
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5">Documents</div>
                                <div class="col-md-7">: <?=(count($document) > 0 ? '' : 'None')?></div>
                            </div>
                            <?php foreach ($document as $doc): ?>
                                        
                                    <div class="col-md-6 mb-1">
                                        <a href="./<?=$doc['document_location'].'/'.$doc['document_name']?>" class="btn btn-sm btn-primary rounded d-block" target='_blank'><i class='fa fa-download'></i> <?=$doc['type_name']?></button></a>
                                    </div>
                                      
                            <?php endforeach; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HISTORY -->
    <div class="card border-secondary mb-3">
        <div class="card-header text-dark">
            <span class="text-uppercase font-weight-bold">History</span>
        </div>
        <div class="card-body text-dark">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Feedback</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Status</th>
                                            <th>User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  
                                            if (is_array($sample_log) && !empty($sample_log)) {
                                                $i = 1;
                                                foreach ($sample_log as $log) {
                                                    echo "
                                                    <tr>
                                                    <td>{$i}</td>
                                                    <td>{$log['log_content']}</td>
                                                    <td>".date('d-M-Y', strtotime($log['log_date']))."</td>
                                                    <td>".date('H:s', strtotime($log['log_date']))."</td>
                                                    <td>{$log['status_name']}</td>
                                                    <td>{$log['user_type']}</td>
                                                </tr>";
                                                $i++;
                                                }
                                                
                                            } else {

                                        ?>
                                        <tr>
                                            <td colspan="6">There's no data found</td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      

</div>
        <!-- /.container-fluid -->