<?php  
	/**
	 * 
	 */
	class Supplier extends Controller
	{
		
		function accessRules()
		{
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
			return array(
				array('Allow', 
	                'actions'=>array('viewSupplierIndex', 'viewDeleteSupplier', 'viewNewSupplier', 'viewEditSupplier'),
	                'groups'=>array_merge(array('Super Admin', 'Administrator'), $purchasing),
	            ),
	            array('Deny', 
	                'actions'=>array('viewSupplierIndex', 'viewDeleteSupplier', 'viewNewSupplier', 'viewEditSupplier'),
	                'groups'=>array('Guest'),
	            ),
		);
		}

		public function site()
	    {
	        $site = array(
	            'root' => 'prp'
	        );
	        return $site;
	    }

	    //SUPPLIER INDEX
		function viewSupplierIndex()
	    {
	        
	        $model_supplier = $this->load->model('Supplier');
	        $query_option = array(
	            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
	            'result' => 10,
	            'order_by' => 'supplier_name',
	            'order' => 'ASC'
	        );
	        $data = array(
	        	'admin'      => $this->isAdmin(),
	            'session' => $this->session,
	            'supplier' => $model_supplier->getAllCountSupplier($query_option),
	            'total' => $model_supplier->getCountResult(),
	            'page' => $query_option['page'],
	            'max_result' => $query_option['result'],
	            'option' => array(
	                'exjs' => array(
	                    './Resources/js/administrator.js'
	                )
	            ),
	        );
	        $this->load->template('admin/supplier/index', $data);
	    }	    

	    //SUBMIT SUPPLIER
	    function viewNewSupplier()
	    {
	    	$model_supplier = $this->load->model('Supplier');
	    	$data 			= array(
	    						'session' 		=> $this->session,
					    		'admin'      	=> $this->isAdmin(),
					            'page' 			=> 'Manage Supplier',
					            //'producer_list' => $model_producer->getAllActiveProducer() 
	    					);
	    	if (isset($_POST['submit']) && $_POST['submit'] == 'create_supplier') {
	    		$input = $this->load->lib('Input');
	            $input->addValidation('supplier_name', $_POST['supplier_name'], 'min=1', 'Must be filled');
	            $input->addValidation('supplier_address', $_POST['supplier_address'], 'min=1', 'Must be filled');
	            $input->addValidation('supplier_phone', $_POST['supplier_phone'], 'numeric', 'Only numeric characer allowed');
	            $input->addValidation('supplier_pic', $_POST['supplier_pic'], 'min=1', 'Check your input');
	            if ($input->validate()) {
	            		$supplier = array(
	            			'supplier_name' => $_POST['supplier_name'],
	            			'supplier_address' => $_POST['supplier_address'],
	            			'supplier_phone' => $_POST['supplier_phone'],
	            			'supplier_pic' => $_POST['supplier_pic'],
	            		);
	            		if ($model_supplier->newSupplier($supplier)) {
	            			//$last = $model_supplier->getActiveSupplierLastInsert();
	            			//$i = 1;
	            			//if (isset($_POST['producer'])) {
	            				//exit(print_r($_POST['producer']));
	            			//	foreach ($_POST['producer'] as $producer) {
	            			//		$model_supplier->newLink(
	            			//			array(
				            //			'supplier_id' => $last['supplier_id'],
				            //			'producer_id' => $producer
	            			//			)
	            			//		);
	            			//	}
	            			//}
	            			echo "<script>alert('Supplier Submit Success'); window.location.replace('admin_supplier.html');</script>";
	            		}
	            	}else{
	            		$data['error'] = $input->_error;
	            		$this->load->template('admin/supplier/new', $data);
	            	}	
	    	}else{
	    		$this->load->template('admin/supplier/new', $data);
	    	}
	    }

	    /*function viewNewSupplier()
	    {
	    	$model_supplier = $this->load->model('Supplier');
	    	$model_producer = $this->load->model('Producer');
	    	$input 			= $this->load->lib('Input');
	    	$data 			= array(
					    		'session' 		=> $this->session,
					    		'admin'      	=> $this->isAdmin(),
					            'page' 			=> 'Manage Supplier',
					            'producer_list' => $model_producer->getAllActiveProducer()
					    	);
	    	if (isset($_POST['submit']) && $_POST['submit'] == 'create_supplier') {
	            $input->addValidation('supplier_name', $_POST['supplier_name'], 'min=1', 'Must be filled');
	            $input->addValidation('supplier_address', $_POST['supplier_address'], 'min=1', 'Must be filled');
	            $input->addValidation('supplier_phone', $_POST['supplier_phone'], 'numeric', 'Only numeric characer allowed');
	            $input->addValidation('supplier_pic', $_POST['supplier_pic'], 'min=1', 'Check your input');
	            if ($input->validate()) {
	            		$supplier = array(
	            			'supplier_name' => $_POST['supplier_name'],
	            			'supplier_address' => $_POST['supplier_address'],
	            			'supplier_phone' => $_POST['supplier_phone'],
	            			'supplier_pic' => $_POST['supplier_pic'],
	            		);
	            		if ($model_supplier->newSupplier($supplier)) {
	            			$last = $model_supplier->getActiveSupplierLastInsert();
	            			$i = 1;
	            			if (isset($_POST['producer'])) {
	            				//exit(print_r($_POST['producer']));
	            				foreach ($_POST['producer'] as $producer) {
	            					$model_supplier->newLink(
	            						array(
				            			'supplier_id' => $last['supplier_id'],
				            			'producer_id' => $producer
	            						)
	            					);
	            				}
	            			}
	            			echo "<script>alert('Supplier Submit Success'); window.location.replace('admin_supplier.html');</script>";
	            		}
	            	}else{
	            		$data['error'] = $input->_error;
	            		$this->load->template('admin/supplier/new', $data);
	            	}	
	    	}else{
	    		$this->load->template('admin/supplier/new', $data);
	    	}
	    }*/

	    //EDIT SUPPLIER
	    public function viewEditSupplier()
	    {
	    	$model_supplier = $this->load->model('Supplier');
	    	$model_producer = $this->load->model('Producer');
	    	$input 			= $this->load->lib('Input');
	    	$data 			= array(
	    						'session' => $this->session,
	    						'admin'      => $this->isAdmin(),
	    						'page'		=> 'Manage Supplier',
	    						//'producer_list' => $model_producer->getAllActiveProducer() 
	    					);
	    	if (!empty($_POST)) {
	    		#$input->addValidation('supplier_name', $_POST['supplier_name'], 'min=1', 'Must be filled');
	            $input->addValidation('supplier_address', $_POST['supplier_address'], 'min=1', 'Must be filled');
	            $input->addValidation('supplier_phone', $_POST['supplier_phone'], 'numeric', 'Only numeric characer allowed');
	            $input->addValidation('supplier_pic', $_POST['supplier_pic'], 'min=1', 'Check your input');
		    	if ($input->validate()) {
                    $supplier = array(
                        'supplier_name' => $_POST['supplier_name'],
                        'supplier_address' => $_POST['supplier_address'],
                        'supplier_phone' => $_POST['supplier_phone'],
                        'supplier_pic' => $_POST['supplier_pic'],
                    );
                    if ($model_supplier->editSupplier($supplier, array('supplier_id' => $_GET['id']))) {
                        //$last = $model_supplier->getActiveSupplierLastInsert();
                        //$link = array(
                        //	'supplier_id' => $last['supplier_id'],
                        //	'producer_id' => $_POST['producer']
                        //);
                        //if ($model_supplier->editLink($link, array('link_id' => $_GET['id']))) {
                            header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                        //}
                    }
                }else{
                    $data['error'] = $input->_error;
                    $data['supplier'] = $model_supplier->getSupplierById(array('supplier_id'=>$_GET['id']));
                    $this->load->template('admin/supplier/edit', $data);
                }	
            }else{
                if (isset($_GET['id'])) {
	    			$input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            		$input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            		if ($input->validate()) {
            			$data['supplier'] = $model_supplier->getSupplierById(array('supplier_id'=>$_GET['id']));
            			//$data['producer'] = $model_supplier->getProducerbySupplier(array('supplier_id'=>$_GET['id'])); //untuk checkbox producer list
            			$this->load->template('admin/supplier/edit', $data);
            		} else {
            			$this->showError(2);
            		}
	    		} else {
	    			$this->showError(2);
	    		}
	    	}
	    }

	    //DELETE SUPPLIER
	    public function viewDeleteSupplier()
	    {
	    	$id = $_GET['idSupplier'];
	    	$model_supplier = $this->load->model('Supplier');
	    	$model_supplier->deleteSupplier($id);
	    	header("Location: admin_supplier.html");
	    }	

	}

?>