<?php  
	/**
	 * 
	 */
	class Producer extends Controller
	{
		
		function accessRules()
		{
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
			return array(
				array('Allow', 
	                'actions'=>array('viewProducerIndex', 'viewNewProducer', 'viewDeleteProducer', 'viewEditProducer'),
	                'groups'=>array_merge(array('Super Admin', 'Administrator'), $purchasing),
	            ),
	            array('Deny', 
	                'actions'=>array('viewProducerIndex', 'viewNewProducer', 'viewDeleteProducer', 'viewEditProducer'),
	                'groups'=>array('Guest'),
	            ),
		);
		}

		public function site()
	    {
	        $site = array(
	            'root' => 'rawmat'
	        );
	        return $site;
	    }

        //PRODUCER INDEX
		function viewProducerIndex()
	    {
	        
	        $model_producer = $this->load->model('Producer');
	        $query_option = array(
	            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
	            'result' => 10,
	            'order_by' => 'producer_name',
	            'order' => 'ASC',
	        );
	        $data = array(
	            'session' => $this->session,
	    		'admin'      => $this->isAdmin(),
	            'producer' => $model_producer->getAllCountProducer($query_option),
	            'total' => $model_producer->getCountResult(),
	            'page' => $query_option['page'],
	            'max_result' => $query_option['result'],
                'option' => array(
                    'exjs' => array(
                        './Resources/js/administrator.js'
                    )
                ),
	        );
	        $this->load->template('admin/producer/index', $data);
	    }
        
        //AJAX PRODUCER
        function viewAjaxGetProducer()
        {
            $data['success'] = 0;
            if(!empty($_POST['supplier'])){
                $input = $this->load->lib('Input');
                $input->addValidation('supplier_required', $_POST['supplier'], 'min=1', 'Must be filled');
                $input->addValidation('supplier_max', $_POST['supplier'], 'max=6', 'Too many');
                $input->addValidation('supplier_format', $_POST['supplier'], 'numeric', 'Must be filled');
                if($input->validate()){
                    $mproducer = $this->load->model('Producer');
                    $data['manufacturer'] = $mproducer->getAllActiveProducer();
                    if(count($data['manufacturer']) > 0){
                        $data['success'] = 1;
                    }
                }
            }
            header('Content-Type: application/json');
            exit(json_encode($data));
        }

        //SUBMIT PRODUCER
	    function viewNewProducer()
	    {
	    	$model_supplier = $this->load->model('Supplier');
            $model_producer = $this->load->model('Producer');
	    	$data = array(
	    		'session'       => $this->session,
	    		'admin'         => $this->isAdmin(),
	            'page'          => 'Manage Producer',
                //'supplier_list' => $model_supplier->getAllActiveSupplier()
	    	);
	    	if (isset($_POST['submit']) && $_POST['submit'] == 'create_producer') {
	    		$input = $this->load->lib('Input');
	            $input->addValidation('producer_name', $_POST['producer_name'], 'min=1', 'Must be filled');
	            $input->addValidation('producer_address', $_POST['producer_address'], 'min=1', 'Must be filled');
	            $input->addValidation('producer_phone', $_POST['producer_phone'], 'numeric', 'Only numeric characer allowed');
	            $input->addValidation('producer_pic', $_POST['producer_pic'], 'min=1', 'Check your input');
	            if ($input->validate()) {
	            		$producer = array(
	            			'producer_name' => $_POST['producer_name'],
	            			'producer_address' => $_POST['producer_address'],
	            			'producer_phone' => $_POST['producer_phone'],
	            			'producer_pic' => $_POST['producer_pic'],
	            		);
	            		if ($model_producer->newProducer($producer)) {
                        //    $last_prod = $model_producer->getActiveProducerLastInsert();
	            		//	$i = 1;
                        //    if (isset($_POST['supplier'])) {
                                //exit(print_r($_POST['supplier']));
                        //        foreach ($_POST['supplier'] as $supplier) {
                        //            $model_supplier->newLink(
                        //                array(
                        //                    'producer_id' => $last_prod['producer_id'], 
                        //                    'supplier_id' => $supplier
                        //                )
                        //            );
                        //        }
                        //    }
                            echo "<script>alert('Producer Submit Success'); window.location.replace('admin_producer.html');</script>";
	            		}
	            	}else{
	            		$data['error'] = $input->_error;
	            		$this->load->template('admin/producer/new', $data);
	            	}	
	    	}else{
	    		$this->load->template('admin/producer/new', $data);

	    	}
	    }

	    //EDIT PRODUCER
	    public function viewEditProducer()
	    {
	    	$model_producer = $this->load->model('Producer');
            $model_supplier = $this->load->model('Supplier');
	    	$input = $this->load->lib('Input');
	    	$data = array(
	    		'session' => $this->session,
	    		'admin'      => $this->isAdmin(),
	    		'page'		=> 'Manage Producer',
                'supplier_list' => $model_supplier->getAllActiveSupplier()
	    	);
	    	if (isset($_POST['submit']) && $_POST['submit'] == 'edit_producer') {
	    		$input->addValidation('producer_name', $_POST['producer_name'], 'min=1', 'Must be filled');
	            $input->addValidation('producer_address', $_POST['producer_address'], 'min=1', 'Must be filled');
	            $input->addValidation('producer_phone', $_POST['producer_phone'], 'numeric', 'Only numeric characer allowed');
	            $input->addValidation('producer_pic', $_POST['producer_pic'], 'min=1', 'Check your input');
	            if ($input->validate()) {
	            	$producer = array(
	            		'producer_name' => $_POST['producer_name'],
	            		'producer_address' => $_POST['producer_address'],
	            		'producer_phone' => $_POST['producer_phone'],
	            		'producer_pic' => $_POST['producer_pic'], 
	            	);
	            	if ($model_producer->editProducer($producer, array('producer_id' => $_GET['id'] ))) {
	            		header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
	            	}
	            }else{
            		$data['error'] = $input->_error;
            		$this->load->template('admin/producer/edit', $data);
            	}
	    	}else{
	    		if (isset($_GET['id'])) {
	    			$input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            		$input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            		if ($input->validate()) {
            			$data['producer'] = $model_producer->getProducerById(array('producer_id'=>$_GET['id']));
                        //$data['supplier'] = $model_producer->getSupplierbyProducer(array('producer_id'=>$_GET['id'])); //checkbox list supplier
            			$this->load->template('admin/producer/edit', $data);
            		} else {
            			$this->showError(2);
            		}
	    		} else {
	    			$this->showError(2);
	    		}
	    	}
	    }

	    //DELETE PRODUCER DATA
	    public function viewDeleteProducer()
	    {
	    	$id = $_GET['idProducer'];
	    	$model_producer = $this->load->model('Producer');
	    	$model_producer->deleteProducer($id);
	    	header("Location: admin_producer.html");
	    }	    

	}

?>