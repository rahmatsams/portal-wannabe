<?php  
    /**
     * 
     */
    class Condition extends Controller
    {
        
        function accessRules()
        {
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
            return array(
                array('Allow', 
                    'actions'=>array('viewConditionIndex', 'viewNewCondition', 'viewDeleteCondition', 'viewEditCondition', 'Purchasing'),
                    'groups'=>array_merge(array('Super Admin', 'Administrator'), $purchasing),
                ),
                array('Deny', 
                    'actions'=>array('viewConditionIndex', 'viewNewCondition', 'viewDeleteCondition', 'viewEditCondition'),
                    'groups'=>array('Guest'),
                ),
        );
        }

        public function site()
        {
            $site = array(
                'root' => 'rawmat'
            );
            return $site;
        }

        //SAMPLE CONDITION INDEX
        function viewConditionIndex()
        {
            
            $model_condition = $this->load->model('Condition');
            $query_option = array(
                'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
                'result' => 10,
                'order_by' => 'condition_id',
                'order' => 'ASC',
            );
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'condition' => $model_condition->getAllCountCondition($query_option),
                'total' => $model_condition->getCountResult(),
                'page' => $query_option['page'],
                'max_result' => $query_option['result'],
                'option' => array(
                    'exjs' => array(
                        './Resources/js/administrator.js'
                    )
                ),
            );
            $this->load->template('admin/condition/index', $data);
        }

        //SUBMIT SAMPLE CONDITION
        function viewNewCondition()
        {
            $model_condition = $this->load->model('Condition');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page' => 'Manage Condition',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'create_condition') {
                $input = $this->load->lib('Input');
                $input->addValidation('condition_name', $_POST['condition_name'], 'min=1', 'Must be filled');
                if ($input->validate()) {
                        $condition = array(
                            'condition_name' => $_POST['condition_name'],
                        );
                        if ($model_condition->newCondition($condition)) {
                            echo "<script>alert('Condition Submit Success'); window.location.replace('admin_condition.html');</script>";
                        }
                    }else{
                        $data['error'] = $input->_error;
                        $this->load->template('admin/condition/new', $data);
                    }   
            }else{
                $this->load->template('admin/condition/new', $data);

            }
        }

        //EDIT SAMPLE CONDITION
        public function viewEditCondition()
        {
            $model_condition = $this->load->model('Condition');
            $input = $this->load->lib('Input');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page'      => 'Condition',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'edit_condition') {
                $input->addValidation('condition_name', $_POST['condition_name'], 'min=1', 'Must be filled');
                if ($input->validate()) {
                    $condition = array(
                        'condition_name' => $_POST['condition_name'],
                    );
                    if ($model_condition->editCondition($condition, array('condition_id' => $_GET['id'] ))) {
                        header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                    }
                }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/condition/edit', $data);
                }
            }else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                    $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                    if ($input->validate()) {
                        $data['condition'] = $model_condition->getConditionById(array('condition_id'=>$_GET['id']));
                        $this->load->template('admin/condition/edit', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $this->showError(2);
                }
            }
        }

        //DELETE SAMPLE CONDITION
        public function viewDeleteCondition()
        {
            $id = $_GET['idCondition'];
            $model_condition = $this->load->model('Condition');
            $model_condition->deleteCondition($id);
            header("Location: admin_condition.html");
        }    

    }

?>