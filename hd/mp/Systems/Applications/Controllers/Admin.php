<?php
class Admin extends Controller
{
    public function accessRules()
    {
        $purchasing = array();
        foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
            $purchasing[] = $r['role_name'];
        }
        return array(
            array('Allow', 
                'actions'=>array('viewIndex'),
                'groups'=> array_merge(array('Super Admin', 'Administrator'), $purchasing),
            ),
            array('Deny', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Guest'),
            )
        );
    }
    
    protected function site()
    {
        $site = array(
            'root' => 'admin'
        );
        return $site;
    }
    
    //ADMINISTRATOR
    function viewIndex()
    {
        $data = array(
            'site'      => $this->Site(),
            'admin'     => $this->isAdmin(),
            'page'      => 'Administrator',
            'session'   => $this->session,
        );
        $this->load->template('admin/index', $data);
    }
    
    
    
}
?>