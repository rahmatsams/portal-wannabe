<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxCategory extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewSubCategory', 'viewVacantArea', 'viewGetTicketData'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewSubCategory','viewSubmit', 'viewGetTicketData'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
	function viewSubCategory(){
		if(isset($_POST['category'])){
			$input = $this->load->lib('Input');
			$input->addValidation('category', $_POST['category'] ,'numeric', 'Unknown Error');
			$input->addValidation('category', $_POST['category'] ,'max=4', 'Unknown Error');
			if($input->validate()){
				$mtc = $this->load->model('TicketCategory');
				$qo = array(
					'order_by' => 'category_name',
					'order' => 'ASC'
				);
				$result = array();
				$result['data'] = $mtc->getSubCategory($_POST['category'], $qo);
				if(is_array($result['data']) && count($result['data']) > 0){
					$result['success'] = 1;
				} else {
					$result['success'] = 0;
				}
				echo json_encode($result);
			}
		} else {
			echo 'false';
		}
	}
}
/*
* End Home Class
*/