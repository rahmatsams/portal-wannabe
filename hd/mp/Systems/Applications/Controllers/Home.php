<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Home extends Controller
{
    protected function mrequest()
    {
        return $this->load->model('Request');
    }

    public function site()
    {
        $site = array(
            'template' => getConfig('default_template'),
            'root' => 'home'
        );
        return $site;
    }
    
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewIndex', 'viewAjaxIndex'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Guest'),
            ),
            
        );
    }

    //INDEX REQUEST LIST
    public function viewIndex() 
    {
        $model_request = $this->load->model('Request');
        $qo['page'] = (!empty($_GET['page']) ? $_GET['page'] : 1);
        #$qo['order_by'] = (!empty($this->session['option']['order_by']) ? $this->session['option']['order_by'] : 'request_date');
        #$qo['order'] = (!empty($this->session['option']['order']) ? $this->session['option']['order'] : 'DESC');
        $qo['result'] = 10; //result data max 10
        $qo['order_by'] = 'request_id';
        $qo['order'] = 'DESC';
        $data['site'] = $this->Site();
        $data['admin'] = $this->isAdmin();
        $data['fnb'] = $this->isAdminFNB();
        $data['qa'] = $this->isAdminQA();
        $data['kahi'] = $this->isKahi();
        $data['purchasing'] = $this->isPurchasing();
        $data['page'] = 'Dashboard';
        if(!empty($this->session['filter'])){
            $data['filter'] = $qo;
            #exit(print_r($qo));
            $data['request'] = $this->mrequest()->getFiltered($this->session['filter'], $qo);
        }else{
            $data['filter'] = $qo;
            $data['request'] = $this->requestData($qo);
        }
        
        if(isset($_POST['submit']) && $_POST['submit'] == 'Search Now') $data['post'] = $_POST;

        $data['session'] = $this->session;
        //$data['option']['exjs'][]  = array('Resources/js/indexmrp.js');
        //$data['option']['exjs'][]  = 'Resources/js/indexmrp.js';
        
        $data['request_type'] = $this->mrequest()->getAllRequestType();
        $data['status'] = $this->mrequest()->getAllSampleStatus();

        $this->load->template('home/index', $data);

    }

    //Protected / Private
    protected function requestData($qo)
    {
        if(!$this->isAdmin()){
            #exit('here');
            $filter['request_user'] = array('operator' => '=','value' => $this->session['user_id']);
        }else{
            $filter = array();
        }
        if(isset($_POST['submit']) && $_POST['submit'] == 'Search Now'){
            $input = $this->load->lib('Input');
            if(!empty($_POST['page'])){
                $input->addValidation('page_format', $_POST['page'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('page_max', $_POST['page'], 'max=3', 'Cek kembali input anda');
            }
            if(!empty($_POST['status'])){
                $input->addValidation('status_format', $_POST['status'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('status_max', $_POST['status'], 'max=2', 'Cek kembali input anda');
            }
            if(!empty($_POST['request_type'])){
                $input->addValidation('type_format', $_POST['request_type'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('type_max', $_POST['request_type'], 'max=3', 'Cek kembali input anda');
            }
            if(!empty($_POST['submit_start'])) $input->addValidation('type_format', $_POST['submit_start'], 'date', 'Cek kembali input anda');

            if(!empty($_POST['submit_end'])) $input->addValidation('type_format', $_POST['submit_end'], 'date', 'Cek kembali input anda');

            if(!empty($_POST['title'])) $input->addValidation('title_format', $_POST['page'], 'numeric', 'Cek kembali input anda');

            if($input->validate()){

                if(!empty($_POST['page'])) $qo['page'] = $_POST['page'];

                if(!empty($_POST['status'])) $filter['ts.sample_status'] = array('operator' => '=','value' => "{$_POST['status']}");

                if(!empty($_POST['request_type'])) $filter['request_type'] = array('operator' => '=','value' => "{$_POST['request_type']}");

                if(!empty($_POST['title'])) $filter['request_name'] = array('operator' => ' LIKE ','value' => "%{$_POST['title']}%");

                if(!empty($_POST['submit_start'])){
                    $filter['request_start'] = $_POST['submit_start'];
                    if(!empty($_POST['submit_end'])){
                        $filter['request_end'] = $_POST['submit_end'];
                    } else{
                        $filter['request_end'] = $_POST['submit_start'];
                    }
                }
                
                #exit(print_r($filter));
                

            }else{
                $result = array('error' => $input->_error);
            }
        }
        $this->setSession('filter', $filter);
        $this->setSession('query', $qo);
        $result = $this->mrequest()->getFiltered($filter, $qo);
        

        return $result;
    }

}
/*
* End Home Class
*/