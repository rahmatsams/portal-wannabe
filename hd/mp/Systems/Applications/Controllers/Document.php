<?php  
    /**
     * 
     */
    class Document extends Controller
    {
        
        function accessRules()
        {
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
            return array(
                array('Allow', 
                    'actions'=>array('viewDocumentIndex', 'viewNewDocument', 'viewDeleteDocument', 'viewEditDocument'),
                    'groups'=> array_merge(array('Super Admin', 'Administrator'), $purchasing),
                ),
                array('Deny', 
                    'actions'=>array('viewDocumentIndex', 'viewNewDocument', 'viewDeleteDocument', 'viewEditDocument'),
                    'groups'=>array('Guest'),
                ),
        );
        }

        public function site()
        {
            $site = array(
                'root' => 'rawmat'
            );
            return $site;
        }

        //DOCUMENT TYPE INDEX 
        function viewDocumentIndex()
        {
            $model_doc = $this->load->model('DocumentType');
            $query_option = array(
                'page'      => (isset($_GET['page'])) ? $_GET['page'] : 1,
                'result'    => 10,
                'order_by'  => 'type_id',
                'order'     => 'ASC',
            );
            $data = array(
                'session'    => $this->session,
                'admin'      => $this->isAdmin(),
                'document'   => $model_doc->getAllCountDoc($query_option),
                'total'      => $model_doc->getCountResult(),
                'page'       => $query_option['page'],
                'max_result' => $query_option['result'],
                'option'     => array( 
                        'exjs' => array(
                            './Resources/js/administrator.js'
                        )
                ),
            );
            $this->load->template('admin/document/index', $data);
        }

        //SUBMIT DOCUMENT TYPE 
        function viewNewDocument()
        {
            $model_doc = $this->load->model('DocumentType');
            $data = array(
                'session'   => $this->session,
                'admin'     => $this->isAdmin(),
                'page'      => 'Document Type',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'create_doc') {
                $input = $this->load->lib('Input');
                $input->addValidation('type_name', $_POST['type_name'], 'min=1', 'Must be filled');
                
                if ($input->validate()) {
                        $doc = array(
                            'type_name' => $_POST['type_name'],    
                        );

                        if ($model_doc->newDocumentType($doc)) {
                            echo "<script>alert('Type Submit Success'); window.location.replace('admin_document.html');</script>";
                        }
                    }else{
                        $data['error'] = $input->_error;
                        $this->load->template('admin/document/new', $data);
                    }   
            }else{
                $this->load->template('admin/document/new', $data);
            }
        }

        //EDIT DOCUMENT TYPE
        public function viewEditDocument()
        {
            $model_doc = $this->load->model('DocumentType');
            $input = $this->load->lib('Input');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page'      => 'Document Type',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'edit_doc') {
                $input->addValidation('type_name', $_POST['type_name'], 'min=1', 'Must be filled');
                
                if ($input->validate()) {
                    $doc = array(
                        'type_name' => $_POST['type_name'],
                         
                    );
                    if ($model_doc->editDocument($doc, array('type_id' => $_GET['id'] ))) {
                        header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                    }
                }else{
                    $data['error'] = $input->_error;
                    $this->load->template('admin/document/edit', $data);
                }
            }else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                    $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                    if ($input->validate()) {
                        $data['doc'] = $model_doc->getDocumentById(array('type_id'=>$_GET['id']));
                        $this->load->template('admin/document/edit', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $this->showError(2);
                }
            }
        }

        //DELETE DOCUMENT TYPE
        public function viewDeleteDocument()
        {
            $id = $_GET['idDoc'];
            $model_doc = $this->load->model('DocumentType');
            $model_doc->deleteDocument($id);
            header("Location: admin_document.html");
        }       

    }

?>