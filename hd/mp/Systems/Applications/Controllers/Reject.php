<?php 
/**
 * 
 */
class Reject extends Controller
{
    private $message; //var email's contain

    function accessRules()
    {
        $kahi = array();
        $fnb = array();
        $qa = array();
        $purchasing = array();
        $m = $this->checkControllerModel();
        foreach($m->getAllKAHIRole() as $r){
            $kahi[] = $r['role_name'];
        }
        foreach($m->getAllFNBRole() as $r){
            $fnb[] = $r['role_name'];
        }
        foreach($m->getAllQARole() as $r){
            $qa[] = $r['role_name'];
        }
        foreach($m->getAllPurchasingRole() as $r){
            $purchasing[] = $r['role_name'];
        }
        return array(
            array('Allow', 
                'actions' => array('viewFnbReject'),
                'groups'=> array_merge(array('Super Admin', 'Administrator'), $fnb)
            ),
            array('Allow', 
                'actions' => array('viewQaReject'),
                'groups'=> array_merge(array('Super Admin', 'Administrator'), $qa)
            ),
            array('Allow', 
                'actions' => array('viewKahiReject'),
                'groups'=> array_merge(['Super Admin', 'Administrator'], $kahi)
            ),
            array('Deny', 
                'actions'=>array('viewFnbReject', 'viewQaReject', 'viewKahiReject'),
                'groups'=>array('Guest'),
            ),
        );
    }

    public function site()
    {
        $site = array(
            'template' => getConfig('default_template'),
            'root' => 'rawmat'
        );
        return $site;
    }

    protected function sampleModel()
    {
        return $this->load->model('Sample');
    }

    //FNB REJECT
    public function viewFnbReject()
    {
            $model_log = $this->load->model('SampleLog');
            $model_sample = $this->load->model('Sample');
            $model_request = $this->load->model('Request');
            $session = $this->getSession();
            $input = $this->load->lib('Input');
            $data = array(
                'site'          => $this->site(),
                'session'       => $this->session,
                'admin'         => $this->isAdmin(),
                'fnb'         => $this->isAdminFnb(),
                'page'          => 'Reject FNB',
                'fdata'         => $this->getFlashData(),
            );
            
            if (isset($_POST['action'])) {
                $data = array(
                    'success' => 0 
                );
                $parent = 1;
                if ($_POST['action'] == 'Reject') {
                    $input->addValidation('parent_max', $_POST['ticket_sample'], 'max=6', 'Please check your input again');
                    if ($input->validate()) {
                        unset($_POST['action']);
                        $fnb = array(
                            'ticket_sample' => $_POST['ticket_sample'],
                            'log_date' => date("Y-m-d H:i:s"),
                            'log_content' => $_POST['log_content'],
                            'sample_status' => 2, #fnb reject
                            'user_type' => 1, #fnb
                            'user' => $session['user_id'] 
                        );
                        if ($model_log->newSampleLog($fnb)) {
                            $model_sample = $this->load->model('Sample');
                            $model_sample->modifySample(array('sample_status' => $fnb['sample_status']), array('sample_id'=> $fnb['ticket_sample']));
                            $data['success'] = 1;

                            $mail_reject_fnb = array( //email rejected by qa
                                'title' => "Material Procurement #{$_POST['ticket_sample']} (FNB SAMPLE REJECT) ",
                                        'view' => 'email/mail_reject_fnb',
                                        'data' => array(
                                            'detail' => $model_sample->getTicketSampleByID(array('sample_id' => $_POST['ticket_sample'])),
                                            'content' => 'Material Procurement Sample Detail:',
                                            'header'  => ''
                                        ),
                                        'recipient' => array(
                                            'address' => $this->session['email'],
                                            'name' => $this->session['user_name'],
                                        ), 

                            );

                            $return = array(
                                    'success' => 1,
                                    'email'   => 0, 
                            );

                            if ($this->send_email($mail_reject_fnb)) {
                                $return['email'] = 1;
                            }
                            $return['message'] = $this->message;

                            $model_request = $this->load->model('Request');
                            $request = $model_request->getTicketRequestBySampleID(array('sample_id' => $fnb['ticket_sample']), 'request_id');
                            echo "<script>alert('Sample Rejected by FNB'); window.location.replace('fu_sample.html');</script>";
                        }else {
                            $data['error'] = 'Unknown Error';
                        }
                }else{
                    $data['error'] = $input->_error;
                    }

                }
            } else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                    $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                    if ($input->validate()) {
                        $data['result'] = $model_log->getSampleById(array('sample_id' => $_GET['id']));
                        $data['result2'] = $model_sample->getTicketSampleByID(array('sample_id'=>$_GET['id']));
                        $data['request'] = $model_request->getRequestById(array('req_id' => $data['result2']['request_id']));
                        $this->load->template('ticket/fnb_reject', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                        $this->showError(2);
                }
            }
        }

        //QA REJECT
        public function viewQAReject()
        {
            $model_log = $this->load->model('SampleLog');
            $model_sample = $this->load->model('Sample');
            $model_request = $this->load->model('Request');
            $session = $this->getSession();
            $input = $this->load->lib('Input');
            $data = array(
                'site'          => $this->site(),
                'session'       => $this->session,
                'admin'         => $this->isAdmin(),
                'qa'            => $this->isAdminQA(),
                'page'          => 'Reject QA',
                'fdata'         => $this->getFlashData(),
            );
            
            if (isset($_POST['action'])) {
                $data = array(
                    'success' => 0 
                );
                $parent = 1;
                if ($_POST['action'] == 'QA Reject') {
                    $input->addValidation('parent_max', $_POST['ticket_sample'], 'max=6', 'Please check your input again');
                    if ($input->validate()) {
                        unset($_POST['action']);
                        $qa = array(
                            'ticket_sample' => $_POST['ticket_sample'],
                            'log_date' => date("Y-m-d H:i:s"),
                            'log_content' => $_POST['log_content'],
                            'sample_status' => 3, #qa reject
                            'user_type' => 2, #qa
                            'user' => $session['user_id'] 
                        );
                        if ($model_log->newSampleLog($qa)) {
                            $model_sample = $this->load->model('Sample');
                            $model_sample->modifySample(array('sample_status' => $qa['sample_status']), array('sample_id'=> $qa['ticket_sample']));
                            $data['success'] = 1;

                            $mail_reject_qa = array( //email rejected by qa
                                'title' => "Material Procurement  #{$_POST['ticket_sample']} (QA SAMPLE REJECT) ",
                                'view' => 'email/mail_reject_qa',
                                'data' => array(
                                    'detail' => $model_sample->getTicketSampleByID(array('sample_id' => $_POST['ticket_sample'])),
                                    'content' => 'Material Procurement Sample detail:',
                                    'header' => ''
                                ),
                                'recipient' => array(
                                    'address' => $this->session['email'],
                                    'name' => $this->session['user_name'],
                                ), 
                            );

                            $return = array(
                                'success'  => 1,
                                'email'    => 0, 
                            );

                            if ($this->send_email($mail_reject_qa)) {
                                $return['email'] = 1;
                            }
                            $return['message'] = $this->message;

                            $model_request = $this->load->model('Request');
                            $request = $model_request->getTicketRequestBySampleID(array('sample_id' => $qa['ticket_sample']), 'request_id');
                            echo "<script>alert('Rejected by QA'); window.location.replace('fu_sample.html');</script>";
                        }else {
                            $data['error'] = 'Unknown Error';
                        }
                }else{
                    $data['error'] = $input->_error;
                    }

                }
            } else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                    $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                    if ($input->validate()) {
                        $data['result'] = $model_log->getSampleById(array('sample_id' => $_GET['id']));
                        $data['result2'] = $model_sample->getTicketSampleByID(array('sample_id'=>$_GET['id']));
                        $data['request'] = $model_request->getRequestById(array('req_id' => $data['result2']['request_id']));
                        $this->load->template('ticket/qa_reject', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                        $this->showError(2);
                }
            }
        }

        //KAHI REJECT
        public function viewKahiReject()
        {
            $model_log = $this->load->model('SampleLog');
            $model_sample = $this->load->model('Sample');
            $model_request = $this->load->model('Request');
            $session = $this->getSession();
            $input = $this->load->lib('Input');
            $data = array(
                'site'          => $this->site(),
                'session'       => $this->session,
                'admin'         => $this->isAdmin(),
                'kahi'            => $this->isKahi(),
                'page'          => 'Reject Kahi',
                'fdata'         => $this->getFlashData(),
            );
            
            if (isset($_POST['action'])) {
                $data = array(
                    'success' => 0 
                );
                $parent = 1;
                if ($_POST['action'] == 'Kahi Reject') {
                    $input->addValidation('parent_max', $_POST['ticket_sample'], 'max=6', 'Please check your input again');
                    if ($input->validate()) {
                        unset($_POST['action']);
                        $kahi = array(
                            'ticket_sample' => $_POST['ticket_sample'],
                            'log_date' => date("Y-m-d H:i:s"),
                            'log_content' => $_POST['log_content'],
                            'sample_status' => 4, #kahi reject
                            'user_type' => 3, #kahi
                            'user' => $session['user_id'] 
                        );
                        if ($model_log->newSampleLog($kahi)) {
                            $model_sample = $this->load->model('Sample');
                            $model_sample->modifySample(array('sample_status' => $kahi['sample_status']), array('sample_id'=> $kahi['ticket_sample']));
                            $data['success'] = 1;

                            $mail_reject_kahi = array( //email rejected by kahi
                                'title' => "Material Procurement #{$_POST['ticket_sample']} (KAHI SAMPLE REJECT) ",
                                'view' => 'email/mail_reject_kahi',
                                'data' => array(
                                    'detail' => $model_sample->getTicketSampleByID(array('sample_id' => $_POST['ticket_sample'])),
                                    'content' => 'Material Procurement Sample detail:',
                                    'header' => ''
                                ),
                                'recipient' => array(
                                    'address' => $this->session['email'],
                                    'name' => $this->session['user_name'],
                                ),
                            );

                            $return = array(
                                'success' => 1,
                                'email'   => 0, 
                            );

                            if ($this->send_email($mail_reject_kahi)) {
                                $return['email'] = 1;
                            }
                            $return['message'] = $this->message;

                            $model_request = $this->load->model('Request');
                            $request = $model_request->getTicketRequestBySampleID(array('sample_id' => $kahi['ticket_sample']), 'request_id');
                            echo "<script>alert('Rejected by Kahi'); window.location.replace('fu_sample.html');</script>";
                        }else {
                            $data['error'] = 'Unknown Error';
                        }
                }else{
                    $data['error'] = $input->_error;
                    }

                }
            } else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                    $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                    if ($input->validate()) {
                        $data['result'] = $model_log->getSampleById(array('sample_id' => $_GET['id']));
                        $data['result2'] = $model_sample->getTicketSampleByID(array('sample_id'=>$_GET['id']));
                        $data['request'] = $model_request->getRequestById(array('req_id' => $data['result2']['request_id']));
                        $this->load->template('ticket/kahi_reject', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                        $this->showError(2);
                }
            }
        }

        /* MAILING */
        private function send_email($option){
            $stmail   = $this->load->lib('PHPMailer');
            $smtp   = $this->load->lib('SMTP');
            $stmail   = new PHPMailer(true);
            #$stmail = $this->load->lib('STMail');
            try
            {
                $stmail->IsSMTP(); // telling the class to use SMTP
                $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
                $stmail->SMTPDebug  = 0;                    // enables SMTP debug information (for testing)
                                                           // 1 = errors and messages
                                                           // 2 = messages only
                $stmail->SMTPAuth   = true;                  // enable SMTP authentication
                $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
                $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
                $stmail->Username   = "it6.jkt@sushitei.co.id"; // SMTP account username email dkirim dari alamat email ini
                $stmail->Password   = "(Jannah123)";        // SMTP account password

                $stmail->SetFrom('it6.jkt@sushitei.co.id', 'Material Procurement'); //pengirim

                $stmail->AddReplyTo('it6.jkt@sushitei.co.id', 'Material Procurement'); //reply to
                
                $stmail->isHTML('true');

                $stmail->Subject    = $option['title'];
                
                $returned = $this->load->view($option['view'], $option['data']); //ini var viewnya
                
                $stmail->MsgHTML($returned);
                
                $stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
                
                $memail = $this->load->model('Email');
                foreach($memail->getMailRecipient() as $recipient){
                    $stmail->AddBCC($recipient['email'], $recipient['display_name']);
                }
                /*foreach($m->getAdminAddress() as $recipient){
                    $stmail->AddBCC($recipient['email'], $recipient['display_name']);
                }*/ //to admin
                return ($stmail->Send() ? 1 : 0);
            } catch (phpmailerException $e) {
                $this->message = $e->errorMessage();
                return 0;
                
            } catch (Exception $e) {
                $this->message = $e->getMessage();
                return 0;
                
            }
            
        }


} //END HERE


 ?>