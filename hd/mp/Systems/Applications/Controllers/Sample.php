  <?php 

/**
 * 
 */
//require __DIR__.'/../../../vendor/autoload.php';
//use Spipu\Html2Pdf\Html2Pdf;

class Sample extends Controller
{
    private $message;
    
    function accessRules()
    {
        $kahi = array();
        $fnb = array();
        $qa = array();
        $purchasing = array();
        $m = $this->checkControllerModel();
        foreach($m->getAllKAHIRole() as $r){
            $kahi[] = $r['role_name'];
        }
        foreach($m->getAllFNBRole() as $r){
            $fnb[] = $r['role_name'];
        }
        foreach($m->getAllQARole() as $r){
            $qa[] = $r['role_name'];
        }
        foreach($m->getAllPurchasingRole() as $r){
            $purchasing[] = $r['role_name'];
        }
        return array(
            array('Allow', 
                'actions' => array('viewRequestSample', 'viewFUSample', 'viewNewSample', 'viewDetailSample','viewPrintSample', 'viewSearchSample'),
                'groups'=>array_merge(array('Super Admin', 'Administrator'), $fnb,$kahi,$qa,$purchasing),
            ),
            array('Allow', 
                'actions' => array('viewEditSample'),
                'groups'=>array_merge(array('Super Admin', 'Administrator'), $qa, $purchasing),
            ),
            array('Deny', 
                'actions'=>array('viewRequestSample', 'viewNewSample'),
                'groups'=>array('Guest'),
            ),
        );
    }

        public function site()
        {
            $site = array(
                'root' => 'rawmat'
            );
            return $site;
        }

        protected function sampleModel() //email
        {
            return $this->load->model('Sample');
        }

        //SUBMIT SAMPLE
        public function viewNewSample()
        {
            $model_supplier  = $this->load->model('Supplier');
            $model_sample    = $this->load->model('Sample');
            $model_request   = $this->load->model('Request');
            $model_condition = $this->load->model('Condition');
            $model_list      = $this->load->model('SampleList');
            $mdoc_type       = $this->load->model('DocumentType');
            $mproducer       = $this->load->model('Producer');
            $input           = $this->load->lib('Input');
            
            if (!empty($_POST['request_id'])) {
                $data['success'] = 0;
                $data['error'] = 0;
                $image_uploaded = 0;
                $date = date("Y-m-d H:i:s");
                $input->addValidation('request', $_POST['request_id'], 'min=1', 'Unknown Error');
                $input->addValidation('request', $_POST['request_id'], 'max=6', 'Unknown Error');
                $input->addValidation('size', $_POST['sample_size'], 'min=1', 'Sample size must be filled');
                if(!empty($_POST['create_sup'])){
                    $input->addValidation('new_supplier', $_POST['supplier_name'], 'min=1', 'Supplier name cannot be empty');
                }
                if(!empty($_POST['create_man'])){
                    $input->addValidation('new_manufacturer', $_POST['manufacturer_name'], 'min=1', 'Supplier name cannot be empty');
                }
                if($_FILES['sample_image']['error'][0] > 0) {
                    $data['error']++;
                    $data['message']['image'] = 'You must upload Sample Image';
                }else{
                    $upload[] = $this->doUpload($this->reArrayFiles($_FILES['sample_image']));
                }
                
                if($input->validate() && $data['error'] == 0) {
                    unset($_POST['action']);
                    
                    if(isset($upload[0]) && !empty($upload[0]['upload_name'])){
                        $_POST['sample_img'] = $upload[0]['upload_name']; 
                    }
                    //print_r($upload);exit();
                    if(!empty($_POST['create_sup'])){
                        $model_supplier->newSupplier(
                            array(
                                'supplier_name' => $_POST['supplier_name'],
                                'supplier_pic' => 'to be filled..'
                            )
                        );
                        $sample['supplier'] = $model_supplier->lastInsertID();
                    }else{
                        $sample['supplier'] = $_POST['supplier'];
                    }
                    if(!empty($_POST['create_man'])){
                        
                        $mproducer->newProducer(
                            array(
                                'producer_name' => $_POST['manufacturer_name'],
                                'producer_pic' => 'to be filled..'
                            )
                        );
                        $sample['producer'] = $mproducer->lastInsertID();
                    }else{
                        $sample['producer'] = $_POST['producer'];
                    }
                    $sample['request_id'] = $_POST['request_id'];
                    $sample['sample_name'] = $_POST['sample_name'];
                    $sample['sample_list'] = $_POST['sample_list'];
                    $sample['sample_condition'] = $_POST['sample_condition'];
                    $sample['sample_currency'] = $_POST['currency'];
                    $sample['sample_size'] = $_POST['sample_size'];
                    $sample['sample_date'] = $date;
                    $sample['sample_status'] = 1;
                    $sample['sample_price'] = $_POST['sample_price'];
                    
                    if ($model_sample->newSample($sample)) {
                        unset($_POST['sample_img']);
                        $data['success'] = 1;
                        $last_id = $model_sample->lastInsertID; //modelnya sample
                        $parent = $_POST['request_id'];
                        $date = date("Y-m-d H:i:s");
                        $data_insert_td = array(
                            'ticket_sample' => $last_id,
                            'document_type' => 8, //image
                            'document_name' => $upload[0][0]['upload_name'],
                            'document_ext' => $upload[0][0]['upload_extension'],
                            'document_location' => $upload[0][0]['upload_location'] ,
                            'upload_time' => $date
                        );
                        $model_sample->newSampleTicketDocument($data_insert_td);
                        //$this->setFlashData($data);
                        //header("Location: ".controllerUrl('Sample','RequestSample', array('id' => $parent)));
                        
                        $mail_sample = array( //email new sample
                                        'title'  => "Material Procurement #".$last_id." (NEW SAMPLE)",
                                        'view'   => 'email/new_sample',
                                        'data'   =>  array(
                                            'detail' => $model_sample->getTicketSampleByID(array('sample_id' => $last_id)),
                                            'content' => 'Material Procurement New Sample Detail:',
                                            'header'  => ''
                                        ),
                                        'recipient' =>   array(
                                            'address' => $this->session['email'],
                                            'name' => $this->session['user_name'], 
                                        ), 
                        );
                        $return = array(
                            'success' => 1,
                            'email'   => 0, 
                        );

                        if ($this->send_email($mail_sample)) {
                             $return['email'] = 1;
                         }
                         $return['message'] = $this->message; 

                    }else {
                        $data['message']['input'] = 'Unknown Error';
                    }
                }else{
                    $data['message'] = $input->_error;
                }
                
                header('Content-Type: application/json');
                exit(json_encode($data));
            } elseif (isset($_GET['id'])) {
                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                if ($input->validate()) {
                    $data = array(
                        'site'          => $this->site(),
                        'session'       => $this->session,
                        'admin'         => $this->isAdmin(),
                        'page'          => 'New Sample',
                        'fdata'         => $this->getFlashData(),
                        'supplier'      => $model_supplier->getAllActiveSupplier(),
                        'producer'      => $mproducer->getAllActiveProducer(),
                        'condition'     => $model_condition->getAllActiveCondition(),
                        'list'          => $model_list->getAllActiveList(),
                        'doctype'   => $mdoc_type->getAllActiveDoc(),
                        'option'        => array(
                            'stylesheet' => array(getConfig('base_domain').'assets/modules/select2/css/select2.min.css',getConfig('base_domain').'assets/modules/select2/css/select2-bootstrap4.min.css'),
                            'injs' => array(),
                            'exjs' => array( getConfig('base_domain').'assets/modules/select2/js/select2.min.js', 'Resources/js/rawmat_submit_sample.js')
                        )
                    );
                    $data['result'] = $model_request->getRequestById(array('req_id'=>$_GET['id']));
                    $this->load->template('ticket/submit_sample', $data);
                } else {
                    $this->showError(2);
                }
            } else {
                    $this->showError(2);
            }
            #$this->load->template('ticket/submit_sample', $data);
        }
        
        public function viewFUSample()
        {
            $model_sample = $this->load->model('Sample');
            $data = array(
                'site'          => $this->site(),
                'session'       => $this->session,
                'admin'         => $this->isAdmin(),
                'fnb'           => $this->isAdminFnb(),
                'qa'            => $this->isAdminQA(),
                'kahi'          => $this->isKahi(),
                'purchasing'    => $this->isPurchasing(),
                'page'          => 'View Sample',
                'fdata'         => $this->getFlashData(),
                'option' => [
                    'exjs' => [
                        './Resources/js/rawmat_closed.js',
                        getConfig('base_domain').'assets/modules/DataTables/datatables.min.js',
                        getConfig('base_domain').'assets/modules/DataTables/dataTables.bootstrap4.min.js'
                    ],
                    'stylesheet' => [
                        getConfig('base_domain').'assets/modules/DataTables/dataTables.bootstrap4.min.css'
                    ]
                ]
            );
            $opt = array(
                'order_by' => 'request_id',
                'order' => 'DESC'
            );
            if($this->isKahi()){
                $data['sample'] = $model_sample->getSamplePending(array('sample_status' => 6));
            }elseif($this->isAdminQA()){
                $data['sample'] = $model_sample->getSamplePending(array('sample_status' => 5));
            }elseif($this->isAdminFnb()){
                $data['sample'] = $model_sample->getSamplePending(array('sample_status' => 1));
            }else{
                $data['sample'] = array();
            }
            
            $this->load->template('sample/fu_sample', $data);
           
        }

        //VIEW DETAIL SAMPLE 
        public function viewDetailSample()
        {
            $input = $this->load->lib('Input');
            $model_sample = $this->load->model('Sample');
            $model_request = $this->load->model('Request');
            $model_log = $this->load->model('SampleLog');
            $data = array(
                'site'          => $this->site(),
                'session'       => $this->session,
                'admin'         => $this->isAdmin(),
                'purcashing'    => $this->isPurchasing(),
                'page'          => 'Sample Detail',
                'fdata'         => $this->getFlashData(),
            );
            $opt = array(
                'order_by' => 'request_id',
                'order' => 'DESC'
            );
            
            if (isset($_GET['id'])) {
                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                if ($input->validate()) {
                    $data['option'] = array('exjs' => array('./Resources/js/adminprp.js', getConfig('base_domain').'/assets/modules/jsPDF-1.3.2/dist/jspdf.min.js'));
                    $data['sample'] = $model_sample->getTicketSampleByID(array('sample_id'=>$_GET['id']));
                    $data['document'] = $model_sample->getSampleDocument(array('sample'=>$_GET['id']));
                    $data['sample_log'] = $model_sample->getDetailSampleByLog(array('ticket_sample'=>$_GET['id']));

                    if(is_array($data['sample'])){
                        $this->load->template('ticket/view_sample', $data);
                    }else{
                        exit($this->load->view('error/notfound'));
                    }
                } else {
                    $this->showError(2);
                }
            } else {
                    $this->showError(2);
            }
        }
        
        public function viewSearchSample()
        {
            $msample = $this->load->model('Sample');
            $qo['page'] = (!empty($_GET['page']) ? $_GET['page'] : 1);
            #$qo['order_by'] = (!empty($this->session['option']['order_by']) ? $this->session['option']['order_by'] : 'request_date');
            #$qo['order'] = (!empty($this->session['option']['order']) ? $this->session['option']['order'] : 'DESC');
            $qo['result'] = 10; //result data max 10
            $data['site'] = $this->Site();
            $data['admin'] = $this->isAdmin();
            $data['fnb'] = $this->isAdminFNB();
            $data['qa'] = $this->isAdminQA();
            $data['kahi'] = $this->isKahi();
            $data['purchasing'] = $this->isPurchasing();
            $data['page'] = 'Search Sample';
            $data['filter'] = $qo;
            if(!empty($_POST['sample_name'])){
                $data['post']['sample_name'] = $_POST['sample_name'];
                $data['sample'] = $msample->getSampleByName(array('sample_name' => "%{$_POST['sample_name']}%"));
            }

            $data['session'] = $this->session;
            //$data['option']['exjs'][]  = array('Resources/js/indexmrp.js');
            //$data['option']['exjs'][]  = 'Resources/js/indexmrp.js';
            
            

            $this->load->template('sample/search', $data);
        }

        /* PRINT SAMPLE */
        public function viewPrintSample()
        {
            // $input = $this->load->lib('Input');
            // $model_sample = $this->load->model('Sample');
            // $model_request = $this->load->model('Request');
            // $model_log = $this->load->model('SampleLog');
            // $data = array(
            //     'site'          => $this->site(),
            //     'session'       => $this->session,
            //     'admin'         => $this->isAdmin(),
            //     'purcashing'    => $this->isPurchasing(),
            //     'page'          => 'Manage Sample',
            //     'fdata'         => $this->getFlashData()
            // );
            // $opt = array(
            //     'order_by' => 'request_id',
            //     'order' => 'DESC'
            // );
            
            if (isset($_GET['id'])) {
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_GET['id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_GET['id'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_GET['id'],'max=4', 'Max capacity is 9999');
            if ($input->validate()) {
                $model_sample = $this->load->model('Sample');
                $model_request = $this->load->model('Request');
                $model_log = $this->load->model('SampleLog');
                $s = array(
                    'sample_id' => $_GET['id']
                );
                $data = $model_sample->getSampleForPrint($s);
                //$optuser = $model_sample->getTicketSampleByIDPDF($s);
                if (count($data) > 0) #Print Sample
                { 
                    $mdoc = $this->load->model('QaDocument');
                    $t['data'] = $data;
                    $t['documents'] = $mdoc->getDocumentForPrint($s);
                    //exit(var_dump($t));
                    exit($this->load->view('home/sample_print', $t));
                    //error_reporting(E_ALL);
                    //ini_set('display_errors', true);
                    //$html2pdf = new Html2Pdf();
                    //$html2pdf->setModeDebug();
                    //$html2pdf->writeHTML($this->load->view('home/sample_print', $t));
                    ///$html2pdf->output();
                }
                else
                {
                    
                    echo "<script>
                            alert('No Data');
                            </script>";
                }
            }
        }

    }

        //EDIT SAMPLE
        public function viewEditSample()
        {
            $input           = $this->load->lib('Input');
            $model_sample    = $this->load->model('Sample');
            $model_request   = $this->load->model('Request');
            $model_log       = $this->load->model('SampleLog');
            $model_supplier  = $this->load->model('Supplier');
            $model_condition = $this->load->model('Condition');
            $mproducer       = $this->load->model('Producer');
            $model_list      = $this->load->model('SampleList');
            $data = array(
                'site'          => $this->site(),
                'session'       => $this->session,
                'admin'         => $this->isAdmin(),
                'purchasing'    => $this->isPurchasing(),
                'page'          => 'Edit Sample',
                'producer'      => $mproducer->getAllActiveProducer(),
                'fdata'         => $this->getFlashData(),
                'supplier'      => $model_supplier->getAllActiveSupplier(),
                'list'          => $model_list->getAllActiveList(),
                'condition'     => $model_condition->getAllActiveCondition(),
                'option'        => array(
                            'stylesheet' => array(getConfig('base_domain').'assets/modules/select2/css/select2.min.css',getConfig('base_domain').'assets/modules/select2/css/select2-bootstrap4.min.css'),
                            'injs' => array(),
                            'exjs' => array( getConfig('base_domain').'assets/modules/select2/js/select2.min.js', 'Resources/js/rawmat_edit_sample.js')
                        )
                );
            if (isset($_POST['action'])) {
                $id_back = $_POST['sample_id'];
                $data = array(
                    'success' => 0 
                );
                $parent = 1;
                //$exist_pdf_upload = 0;
                $date = date("Y-m-d H:i:s");
                //$checkgambarubah = 0;
                if ($_POST['action'] == 'Edit Sample') {
                    
                    if($_FILES['sample_image']['error'][0] < 1) {
                        $upload[] = $this->doUpload($this->reArrayFiles($_FILES['sample_image']));
                    }
                    
                    $where_sample = array(
                        'sample_id' => $id_back
                    );

                    if(!empty($_POST['create_sup'])){
                        $model_supplier->newSupplier(
                            array(
                                'supplier_name' => $_POST['supplier_name'],
                                'supplier_pic' => 'to be filled..'
                            )
                        );
                        $sample['supplier'] = $model_supplier->lastInsertID();
                    }else{
                        $sample['supplier'] = $_POST['supplier'];
                    }
                    if(!empty($_POST['create_man'])){
                        
                        $mproducer->newProducer(
                            array(
                                'producer_name' => $_POST['manufacturer_name'],
                                'producer_pic' => 'to be filled..'
                            )
                        );
                        $sample['producer'] = $mproducer->lastInsertID();
                    }else{
                        $sample['producer'] = $_POST['producer'];
                    }
                    $sample['request_id']       = $_POST['request_id'];
                    $sample['sample_name']      = $_POST['sample_name'];
                    $sample['sample_list']      = $_POST['sample_list'];
                    $sample['sample_condition'] = $_POST['sample_condition'];
                    $sample['sample_size']      = $_POST['sample_size'];
                    $sample['sample_currency']      = $_POST['currency'];
                    $sample['sample_date']      = $date;
                    $sample['sample_status']    = 1;
                    $sample['sample_price']     = $_POST['sample_price'];
                    $sample['last_update']      = $date;

                    //exit(print_r($sample));
                    $model_sample->modifySample($sample, $where_sample);  
                    $data['success'] = 1;
                    $parent = $_POST['request_id'];
                    $date = date("Y-m-d H:i:s");
                    if (count($upload) > 0) {
                        $img = $model_sample->checkImageExist(array('ticket_sample' => $id_back));
                        if(is_array($img) && file_exists("{$img['document_location']}/{$img['document_name']}")){
                            unlink("{$img['document_location']}/{$img['document_name']}");
                        }
                        $where = array(
                            'ticket_sample' => $id_back,
                            'document_type' => '8',
                        );
                        $data_insert_td = array(
                            'document_name' => $upload[0][0]['upload_name'],
                            'document_ext' => $upload[0][0]['upload_extension'],
                            'document_location' => $upload[0][0]['upload_location'] 
                        );
                        $model_sample->modifySampleTicketDocument($data_insert_td, $where);
                    }
                    
                    /*if ($exist_pdf_upload == 1) {
                        $where = array(
                            'ticket_sample' => $id_back,
                            'document_type' => 'pdf',
                            'document_ext' => '.pdf'
                        );
                        //print_r($uploadpdf);exit();
                        $data_insert_td = array(
                            'document_name' => $uploadpdf[0][0]['upload_name'],
                            'document_location' => $uploadpdf[0][0]['upload_location'] 
                        );
                        $model_sample->modifySampleTicketDocument($data_insert_td, $where);
                    }*/

                    $this->setFlashData($data);
                    header("Location: ".controllerUrl('Sample','EditSample', array('id' => $id_back)));
                }
            }else{

                if (isset($_GET['id'])) {
                    $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                    $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                    if ($input->validate()) {
                        $data['result'] = $model_sample->getTicketSampleByID(array('sample_id' => $_GET['id']));
                        $data['request'] = $model_request->getRequestById(array('req_id' => $data['result']['request_id']));
                        $data['result2'] = $model_sample->getSampleDocument(array('sample' => $_GET['id']));
                        
                        $this->load->template('ticket/edit_sample', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                        $this->showError(2);
                }
            }
            
        }


        protected function doUpload($file, $location=0, $allowed=0)
        {
            
            if(count($file) > 0){
                $uploaded = array();
                $num=0;
                $upload = $this->load->lib('Upload', $file);
                for($uid=0;$uid < count($file); $uid++){
                    $file[$uid] = new Upload($file[$uid]);
                    if($file[$uid]->uploaded) {
                        $file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num;
                        $file[$uid]->image_resize = true;
                        $file[$uid]->image_x = 800;
                        $file[$uid]->image_ratio_y = true;
                        if(!$allowed){
                            $file[$uid]->allowed = array('image/jpeg', 'image/png');
                            $file[$uid]->image_convert = 'jpg';
                        }else{
                            $file[$uid]->allowed = $allowed;
                        }
                        if(!$location) $location = 'Resources/images/sample';
                        
                        $file[$uid]->process($location);
                        if ($file[$uid]->processed) {
                            $file[$uid]->clean();
                            $uploaded[$num] = array(
                                'upload_name' => $file[$uid]->file_dst_name,
                                'upload_extension' => $file[$uid]->file_dst_name_ext,
                                'upload_location' => $file[$uid]->file_dst_path,
                                'upload_time' => date("Y-m-d H:i:s"),
                                'upload_temp' => 0,
                                'upload_user' => $this->session['user_id'],
                            );
                            $num++;
                        }else{
                            $uploaded[$num]['error'] = $file[$uid]->error;
                        }
                    }
                }
                return $uploaded;
            }
        }

        protected function reArrayFiles(&$file_post)
        {
            $file_ary = array();
            $file_count = count($file_post['name']);
            $file_keys = array_keys($file_post);

            for ($i=0; $i<$file_count; $i++) {
                foreach ($file_keys as $key) {
                    $file_ary[$i][$key] = $file_post[$key][$i];
                }
            }
            return $file_ary;
        }

        /* MAILING */
    private function send_email($option){ //ini proses nya
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail   = new PHPMailer(true);
        #$stmail = $this->load->lib('STMail');
        try
        {
            $stmail->IsSMTP(); // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                    // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "it6.jkt@sushitei.co.id"; // SMTP account username email dkirim dari alamat email ini
            $stmail->Password   = "(Jannah123)";        // SMTP account password

            $stmail->SetFrom('it6.jkt@sushitei.co.id', 'Material Procurement'); //pengirim

            $stmail->AddReplyTo('it6.jkt@sushitei.co.id', 'Material Procurement'); //reply to
            
            $stmail->isHTML('true');

            $stmail->Subject    = $option['title'];
            
             //ini var viewnya
            $returned = $this->load->view($option['view'], $option['data']);
            
            $stmail->MsgHTML($returned);
            
            $stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
            
            $memail = $this->load->model('Email');
            foreach($memail->getMailRecipient() as $recipient){
                $stmail->AddBCC($recipient['email'], $recipient['display_name']);
            }
            /*foreach($m->getAdminAddress() as $recipient){
                $stmail->AddBCC($recipient['email'], $recipient['display_name']);
            }*/ //to admin
            return ($stmail->Send() ? 1 : 0);
        } catch (phpmailerException $e) {
            $this->message = $e->errorMessage();
            return 0;
            
        } catch (Exception $e) {
            $this->message = $e->getMessage();
            return 0;
            
        }
        
    }
}


 ?>