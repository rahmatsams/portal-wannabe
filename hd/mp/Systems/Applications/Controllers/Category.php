<?php  
    /**
     * 
     */
    class Category extends Controller
    {
        
        function accessRules()
        {
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
            return array(
                array('Allow', 
                    'actions'=>array('viewCategoryIndex', 'viewNewCategory', 'viewDeleteCategory', 'viewEditCategory'),
                    'groups'=> array_merge(array('Super Admin', 'Administrator'), $purchasing),
                ),
                array('Deny', 
                    'actions'=>array('viewCategoryIndex', 'viewNewCategory', 'viewDeleteCategory', 'viewEditCategory'),
                    'groups'=>array('Guest'),
                ),
        );
        }
            //SAMPLE CATEGORY MASIH D DISABLED
        public function site()
        {
            $site = array(
                'root' => 'rawmat'
            );
            return $site;
        }

        //SAMPLE CATEGORY INDEX
        function viewCategoryIndex()
        {
            
            $model_cat = $this->load->model('SampleCategory');
            $query_option = array(
                'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
                'result' => 10,
                'order_by' => 'category_name',
                'order' => 'ASC',
                'option' => array(
                'exjs' => array(
                    './Resources/js/administrator.js'
                )
            ),
            );
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'category' => $model_cat->getAllCountCategory($query_option),
                'total' => $model_cat->getCountResult(),
                'page' => $query_option['page'],
                'max_result' => $query_option['result'],
            );
            $this->load->template('admin/category/index', $data);
        }

        //SUBMIT SAMPLE CATEGORY 
        function viewNewCategory()
        {
            $model_cat = $this->load->model('SampleCategory');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page' => 'Manage Sample Category',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'create_category') {
                $input = $this->load->lib('Input');
                $input->addValidation('category_name', $_POST['category_name'], 'min=1', 'Must be filled');
                $input->addValidation('link', $_POST['link'], 'min=1', 'Must be filled');
                
                if ($input->validate()) {
                        $category = array(
                            'category_name' => $_POST['category_name'],
                            'link' => $_POST['link'],
                            
                        );
                        if ($model_cat->newCategory($category)) {
                            //header("Location: admin_producer.html");
                            echo "<script>alert('Category Submit Success'); window.location.replace('admin_category.html');</script>";
                        }
                    }else{
                        $data['error'] = $input->_error;
                        $this->load->template('admin/category/new', $data);
                    }   
            }else{
                $this->load->template('admin/category/new', $data);

            }
        }

        //EDIT SAMPLE CATEGORY
        public function viewEditProducer()
        {
            $model_cat = $this->load->model('Producer');
            $input = $this->load->lib('Input');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page'      => 'Manage Producer',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'edit_producer') {
                $input->addValidation('producer_name', $_POST['producer_name'], 'min=1', 'Must be filled');
                $input->addValidation('producer_address', $_POST['producer_address'], 'min=1', 'Must be filled');
                $input->addValidation('producer_phone', $_POST['producer_phone'], 'numeric', 'Only numeric characer allowed');
                $input->addValidation('producer_pic', $_POST['producer_pic'], 'min=1', 'Check your input');
                if ($input->validate()) {
                    $producer = array(
                        'producer_name' => $_POST['producer_name'],
                        'producer_address' => $_POST['producer_address'],
                        'producer_phone' => $_POST['producer_phone'],
                        'producer_pic' => $_POST['producer_pic'], 
                    );
                    if ($model_cat->editProducer($producer, array('producer_id' => $_GET['id'] ))) {
                        header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                    }
                }else{
                    $data['error'] = $input->_error;
                    $this->load->template('admin/producer/edit', $data);
                }
            }else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                    $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                    if ($input->validate()) {
                        $data['producer'] = $model_cat->getProducerById(array('producer_id'=>$_GET['id']));
                        $this->load->template('admin/producer/edit', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $this->showError(2);
                }
            }
        }

        //DELETE SAMPLE CATEGORY
        public function viewDeleteProducer()
        {
            $id = $_GET['idProducer'];
            $model_cat = $this->load->model('Producer');
            $model_cat->deleteProducer($id);
            header("Location: admin_producer.html");
        }       

    }

?>