<?php  
	/**
	 * 
	 */
	class Status extends Controller
	{
		
		function accessRules()
		{
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
			return array(
				array('Allow', 
	                'actions'=>array('viewStatusIndex', 'viewNewStatus', 'viewDeleteStatus', 'viewEditStatus'),
	                'groups'=>array_merge(array('Super Admin', 'Administrator'), $purchasing),
	            ),
	            array('Deny', 
	                'actions'=>array('viewStatusIndex', 'viewNewStatus', 'viewDeleteStatus', 'viewEditStatus'),
	                'groups'=>array('Guest'),
	            ),
		);
		}

		public function site()
	    {
	        $site = array(
	            'root' => 'prp'
	        );
	        return $site;
	    }

	    //STATUS INDEX
		function viewStatusIndex()
	    {
	        
	        $model_status = $this->load->model('Status');
	        $query_option = array(
	            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
	            'result' => 10,
	            'order_by' => 'status_id',
	            'order' => 'ASC',
	        );
	        $data = array(
	            'session' => $this->session,
	    		'admin'      => $this->isAdmin(),
	            'status' => $model_status->getAllCountStatus($query_option),
	            'total' => $model_status->getCountResult(),
	            'page' => $query_option['page'],
	            'max_result' => $query_option['result'],
	            'option' => array(
	                'exjs' => array(
	                    './Resources/js/administrator.js'
	                )
	            ),
	        );
	        $this->load->template('admin/status/index', $data);
	    }

	    //SUBMIT STATUS
	    function viewNewStatus()
	    {
	    	$model_status = $this->load->model('Status');
	    	$data = array(
	    		'session' => $this->session,
	    		'admin'      => $this->isAdmin(),
	            'page' => 'Manage Status',
	    	);
	    	if (isset($_POST['submit']) && $_POST['submit'] == 'create_status') {
	    		$input = $this->load->lib('Input');
	            $input->addValidation('status_name', $_POST['status_name'], 'min=1', 'Must be filled');
	            if ($input->validate()) {
	            		$status = array(
	            			'status_name' => $_POST['status_name'],
	            		);
	            		if ($model_status->newStatus($status)) {
	            			#header("Location: admin_producer.html");
	            			echo "<script>alert('Status Submit Success'); window.location.replace('admin_status.html');</script>";
	            		}
	            	}else{
	            		$data['error'] = $input->_error;
	            		$this->load->template('admin/status/new', $data);
	            	}	
	    	}else{
	    		$this->load->template('admin/status/new', $data);

	    	}
	    }

	    //EDIT STATUS
	    public function viewEditStatus()
	    {
	    	$model_status = $this->load->model('Status');
	    	$input = $this->load->lib('Input');
	    	$data = array(
	    		'session' => $this->session,
	    		'admin'      => $this->isAdmin(),
	    		'page'		=> 'Sample Status',
	    	);
	    	if (isset($_POST['submit']) && $_POST['submit'] == 'edit_status') {
	    		$input->addValidation('status_name', $_POST['status_name'], 'min=1', 'Must be filled');
	            if ($input->validate()) {
	            	$status = array(
	            		'status_name' => $_POST['status_name'],
	            	);
	            	if ($model_status->editStatus($status, array('status_id' => $_GET['id'] ))) {
	            		header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
	            	}
	            }else{
            	$data['error'] = $input->_error;
            	$this->load->template('admin/status/edit', $data);
            	}
	    	}else{
	    		if (isset($_GET['id'])) {
	    			$input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            		$input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            		if ($input->validate()) {
            			$data['status'] = $model_status->getStatusById(array('status_id'=>$_GET['id']));
            			$this->load->template('admin/status/edit', $data);
            		} else {
            			$this->showError(2);
            		}
	    		} else {
	    			$this->showError(2);
	    		}
	    	}
	    }

	    //DELETE STATUS
	    public function viewDeleteStatus()
	    {
	    	$id = $_GET['idStatus'];
	    	$model_status = $this->load->model('Status');
	    	$model_status->deleteStatus($id);
	    	header("Location: admin_status.html");
	    }	    

	}

?>