<?php 

/**
 * 
 */
class ModulDocument extends Controller
{
    
    function accessRules()
        {
            $qa = array();
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
            foreach($this->checkControllerModel()->getAllQARole() as $r){
                $qa[] = $r['role_name'];
            }
            return array(
                array('Allow', 
                    'actions'=>array('viewModulDocument'),
                    'groups'=>array_merge(array('Super Admin', 'Administrator'), $purchasing, $qa),
                ),
                array('Deny', 
                    'actions'=>array('viewModulDocument'),
                    'groups'=>array('Guest'),
                ),
        );
        }

        public function site()
        {
            $site = array(
                'root' => 'rawmat'
            );
            return $site;
        }

        //SUBMIT MODUL DOCUMENT 
        public function viewModulDocument()
        {
            $model_sample = $this->load->model('Sample');
            $model_request = $this->load->model('Request');
            $model_list = $this->load->model('SampleList');
            $model_doc = $this->load->model('DocumentType');
            $model_document = $this->load->model('QaDocument');
            $input = $this->load->lib('Input');
            if(!empty($_GET['id'])){
                $data = array(
                    'site'          => $this->site(),
                    'session'       => $this->session,
                    'admin'         => $this->isAdmin(),
                    'qa'            => $this->isAdminQA(),
                    'purchasing'    => $this->isPurchasing(),
                    'page'          => 'Modul Document',
                    'fdata'         => $this->getFlashData(),
                    'doc'           => $model_doc->getAllQaDoc(array('sample_id' => $_GET['id'] )),
                    'option'        => array(
                                        'injs' => array(),
                                        'exjs' => array('Resources/js/modul_document.js')
                                    )
                );
            }

            if (isset($_POST['submit'])) {
                $data = array(
                    'success' => 0 
                );
                $date = date("Y-m-d H:i:s");
                
                $input->addValidation('parent_max', $_POST['ticket_sample'], 'max=6', 'Cek kembali input anda');

                if(count($_FILES['modul_document']) > 0) $files = $this->reArrayFiles($_FILES['modul_document']);
                
                if($input->validate()){
                    unset($_POST['submit']);
                    $upload_location = 'Resources/cert/qa';
                    $upload = $this->doUpload($files, $upload_location);
                    if(isset($upload[0]) && !empty($upload[0]['upload_name'])){
                        $doc_exist = $model_document->checkDocumentExist(array('ticket_sample' => $_POST['ticket_sample'], 'type_id' => $_POST['document_type']));
                        //exit(print_r($doc_exist));
                        if (is_array($doc_exist) && file_exists("{$doc_exist['document_location']}/{$doc_exist['document_name']}")) {
                            unlink("{$doc_exist['document_location']}/{$doc_exist['document_name']}");
                            $model_document->deleteQaDocument("{$doc_exist['document_id']}");
                        }
                        $_POST['modul_document'] = $upload[0]['upload_name'];
                    }

                    $doc = array( 
                        'ticket_sample'     => $_POST['ticket_sample'],
                        'document_type'     => $_POST['document_type'],
                        'document_name'     => $upload[0]['upload_name'],
                        'document_ext'      => $upload[0]['upload_extension'],
                        'document_location' => $upload_location,
                        'upload_time'       => $date
                    );
                    if($doc['document_type'] == 1){
                        $doc['document_expired'] = $_POST['expired'];
                        $doc['document_acknowledged'] = $_POST['acknowledged'];
                        $doc['document_issued'] = $_POST['issued'];
                    }
                    //print_r($upload);exit();
                    if($model_document->newQaDocument($doc)){
                        
                        unset($_POST['modul_document']);
                        $data['success'] = 1;
                        
                        echo "<script>alert('Uploaded Successfully'); window.location.replace('view_sample_{$_POST['ticket_sample']}.html');</script>";
                    } else {
                        $data['error'] = 'Unknown Error';
                    }
                }else{
                    $data['error'] = $input->_error;
                }
            
            } else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                    $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                    if ($input->validate()) {
                        $data['result'] = $model_sample->getTicketSampleByID(array('sample_id'=>$_GET['id']));
                        $data['request'] = $model_request->getRequestById(array('req_id' => $data['result']['request_id']));
                        $this->load->template('ticket/modul_document', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                        $this->showError(2);
                }
            }

        } /*END OF QA DOC SUBMIT*/


        protected function doUpload($file, $location='Resources/cert/qa')
        {
            
            if(count($file) > 0){
                $uploaded = array();
                $num=0;
                $upload = $this->load->lib('Upload', $file);
                for($uid=0;$uid < count($file); $uid++){
                    $file[$uid] = new Upload($file[$uid]);
                    if($file[$uid]->uploaded) {
                        $file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num;
                        $file[$uid]->image_resize = true;
                        $file[$uid]->image_x = 640;
                        $file[$uid]->image_y = 480;
                        //$file[$uid]->allowed = array('image/jpeg', 'image/png', 'application/pdf'); /*disabled agar semua file allowed*/
                        //$file[$uid]->image_convert = 'jpg';
                        $file[$uid]->process($location);
                        if ($file[$uid]->processed) {
                            $uploaded[$num] = array(
                                'upload_name' => (!empty($file[$uid]->file_dst_name) ? $file[$uid]->file_dst_name : $file[$uid]->file_new_name_body),
                                'upload_extension' => $file[$uid]->file_dst_name_ext,
                                'upload_location' => $file[$uid]->file_dst_path,
                                'upload_time' => date("Y-m-d H:i:s"),
                                'upload_temp' => 0,
                                'upload_user' => $this->session['user_id'],
                            );
                            $file[$uid]->clean();
                            $num++;
                        }else{
                            $uploaded[$num]['error'] = $file[$uid]->error;
                        }
                    }
                }
                return $uploaded;
            }
        }

        protected function reArrayFiles(&$file_post)
        {
            $file_ary = array();
            $file_count = count($file_post['name']);
            $file_keys = array_keys($file_post);

            for ($i=0; $i<$file_count; $i++) {
                foreach ($file_keys as $key) {
                    $file_ary[$i][$key] = $file_post[$key][$i];
                }
            }
            return $file_ary;
        }
}


 ?>