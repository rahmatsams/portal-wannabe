<?php  
    /**
     * 
     */
    class Reason extends Controller
    {
        
        function accessRules()
        {
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }
            return array(
                array('Allow', 
                    'actions'=>array('viewReasonIndex', 'viewNewReason', 'viewDeleteReason', 'viewEditReason'),
                    'groups'=>array_merge(array('Super Admin', 'Administrator'), $purchasing),
                ),
                array('Deny', 
                    'actions'=>array('viewReasonIndex', 'viewNewReason', 'viewDeleteReason', 'viewEditReason'),
                    'groups'=>array('Guest'),
                ),
        );
        }

        public function site()
        {
            $site = array(
                'root' => 'prp'
            );
            return $site;
        }

        //REASON INDEX
        function viewReasonIndex()
        {
            
            $model_reason = $this->load->model('Reason');
            $query_option = array(
                'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
                'result' => 10,
                'order_by' => 'type_id',
                'order' => 'ASC',
            );
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'reason' => $model_reason->getAllCountReason($query_option),
                'total' => $model_reason->getCountResult(),
                'page' => $query_option['page'],
                'max_result' => $query_option['result'],
                'option' => array(
                    'exjs' => array(
                        './Resources/js/administrator.js'
                    )
                ),
            );
            $this->load->template('admin/reason/index', $data);
        }

        //SUBMIT REASON
        function viewNewReason()
        {
            $model_reason = $this->load->model('Reason');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page' => 'Manage Reason',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'create_type') {
                $input = $this->load->lib('Input');
                $input->addValidation('type_name', $_POST['type_name'], 'min=1', 'Must be filled');
                if ($input->validate()) {
                        $reason = array(
                            'type_name' => $_POST['type_name'],
                        );
                        if ($model_reason->newReason($reason)) {

                            echo "<script>alert('Reason Submit Success'); window.location.replace('admin_reason.html');</script>";

                        }
                    }else{
                        $data['error'] = $input->_error;
                        $this->load->template('admin/reason/new', $data);
                    }   
            }else{
                $this->load->template('admin/reason/new', $data);

            }
        }

        //EDIT REASON
        public function viewEditReason()
        {
            $model_reason = $this->load->model('Reason');
            $input = $this->load->lib('Input');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page'      => 'Sample Reason',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'edit_type') {
                $input->addValidation('type_name', $_POST['type_name'], 'min=1', 'Must be filled');
                if ($input->validate()) {
                    $reason = array(
                        'type_name' => $_POST['type_name'],
                    );
                    if ($model_reason->editReason($reason, array('type_id' => $_GET['id'] ))) {
                        header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                    }
                }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/reason/edit', $data);
                }
            }else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                    $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                    if ($input->validate()) {
                        $data['reason'] = $model_reason->getReasonById(array('type_id'=>$_GET['id']));
                        $this->load->template('admin/reason/edit', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $this->showError(2);
                }
            }
        }

        //DELETE REASON
        public function viewDeleteReason()
        {
            $id = $_GET['idReason'];
            $model_reason = $this->load->model('Reason');
            $model_reason->deleteReason($id);
            header("Location: admin_reason.html");
        }       

    }

?>