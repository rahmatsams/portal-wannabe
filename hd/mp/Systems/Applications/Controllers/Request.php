<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

/**
 * 
 */
class Request extends Controller
{
	private $message;
    
    protected function mrequest()
    {
        return $this->load->model('Request');
    }
    
	function accessRules()
	{
        $kahi = array();
        $fnb = array();
        $qa = array();
        $purchasing = array();
        $m = $this->checkControllerModel();
        foreach($m->getAllKAHIRole() as $r){
            $kahi[] = $r['role_name'];
        }
        foreach($m->getAllFNBRole() as $r){
            $fnb[] = $r['role_name'];
        }
        foreach($m->getAllQARole() as $r){
            $qa[] = $r['role_name'];
        }
        foreach($m->getAllPurchasingRole() as $r){
            $purchasing[] = $r['role_name'];
        }
		return array(
			array('Allow', 
                'actions'=>array('viewSubmitRequest', 'viewRequest', 'viewEditRequest'),
                'groups'=>array('Super Admin', 'Administrator'),
            ),
            array('Allow', 
                'actions'=>array('viewSubmitRequest', 'viewRequest', 'viewEditRequest'),
                'groups'=>array_merge($fnb,$qa,$kahi,$purchasing),
            ),
            array('Allow', 
                'actions'=>array('viewCloseRequest'),
                'groups'=>array('Super Admin', 'Administrator', 'KAHI'),
            ),
            array('Allow', 
                'actions'=>array('viewCloseRequest'),
                'groups'=> $kahi,
            ),
            array('Deny', 
                'actions'=>array('viewEdit', 'viewSubmit', 'viewSubmitUpload', 'viewTicket', 'viewDeletePending', 'viewProccessSubmit','viewGroup','viewExportReport', 'viewAreaData'),
                'groups'=>array('Guest'),
            ),
        );
	}

	public function site()
    {
        $site = array(
            'root' => 'rawmat'
        );
        return $site;
    }

    //EMAIL
    protected function requestModel() 
    {
        return $this->load->model('Request');
    }

    //NEW REQUEST
    public function viewSubmitRequest()
    {
    	$model_request = $this->load->model('Request');
        $session = $this->getSession();
        $data = array(
            'session'      => $session,
            'admin'        => $this->isAdmin(),
            'purchasing'   => $this->isPurchasing(),
            'page'         => 'New Rawmat',
            'request_type' => $model_request->getAllRequestType()
        );
    	if (isset($_POST['submit_request']) && $_POST['submit_request'] == 'Submit') {
            $input = $this->load->lib('Input');
            $input->addValidation('request_name', $_POST['request_name'], 'min=1', 'Must be filled');
            $input->addValidation('request_type', $_POST['request_type'], 'min=1', 'Must be filled');
            $input->addValidation('deadline_date', $_POST['deadline_date'], 'min=1', 'Must be filled');

            if ($input->validate()) {
                $request = array(
                    'request_name'      => $_POST['request_name'],
                    'request_type'      => $_POST['request_type'],
                    'request_date'      => date('Y-m-d H:i:s'),
                    'last_update'       => date('Y-m-d H:i:s'),
                    'deadline_date'     => date_format(date_create($_POST['deadline_date']),"Y-m-d H:i:s"),
                    'request_user'      => $session['user_id'],
                    'request_closed'    => 0, //OPEN
                );
                if ($model_request->newRequest($request)) {

                    $idTerakhir = $model_request->lastInsertID();

                    $mail_request = array( //request new email
                                'title' => 'Material Procurement #'.$idTerakhir.' (NEW REQUEST) ',
                                'view' => 'email/new',
                                'data' => array(
                                    'detail' => $model_request->getTicketRequestByID(array('request_id' => $idTerakhir), "request_id, request_name, request_type, type_name AS reason, request_date, last_update, deadline_date, display_name, type_name"),
                                    'content' => 'Material Procurement Request detail:',
                                    'header' => ''
                                ),
                                'recipient' => array(
                                    'address' => $this->session['email'],
                                    'name' => $this->session['user_name'],
                                ),
                                
                            );
                    $return = array(
                        'success' => 1,
                        'email' => 0, 
                    );

                    if($this->send_email($mail_request)){
                                $return['email'] = 1; 
                            }
                            $return['message'] = $this->message;

                    echo "<script>alert('Request Submit Success'); window.location.replace('index.html');</script>";
                }
            }else{
                $data['error'] = $input->_error;
                echo "<script>alert('Error'); window.location.replace('submit_request.html');</script>";
            }

        }else{
            $this->load->template('ticket/submit_request', $data);
        }
    }

    //EDIT REQUEST
    public function viewEditRequest()
    {
        $session = $this->getSession();
        $input = $this->load->lib('Input');
        $model_request = $this->load->model('Request');
        $data = array(
            'site'          => $this->Site(),
            'session'       => $session,
            'admin'         => $this->isAdmin(),
            'purchasing'   => $this->isPurchasing(),
            'page'          => 'Edit Request'
        );
        if (isset($_POST['edit_request']) && $_POST['edit_request'] == 'Edit') {
            $input->addValidation('request_name', $_POST['request_name'], 'min=1', 'Must be filled');
            $input->addValidation('request_type', $_POST['request_type'], 'min=1', 'Must be filled');
            $input->addValidation('deadline_date', $_POST['deadline_date'], 'min=1', 'Must be filled');
            if ($input->validate()) {
                $request = array(
                    'request_name' => $_POST['request_name'],
                    'request_type' => $_POST['request_type'],
                    'last_update' => date('Y-m-d H:i:s'),
                    'deadline_date' => date_format(date_create($_POST['deadline_date']),"Y-m-d H:i:s"),
                    'request_user' => $session['user_id']
                     );
                if ($model_request->editRequest($request, array('request_id' => $_GET['id'] ))) {
                    header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                }
            } else {
                $data['error'] = $input->_error;
                $this->load->template('admin/ticket/edit_request', $data);
            }
            
        } else {
            if (isset($_GET['id'])) {
                    $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                    $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                    if ($input->validate()) {
                        $data['request'] = $model_request->getDataRequestById(array('request_id' => $_GET['id']));
                        $this->load->template('ticket/edit_request', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $this->showError(2);
                }
            }
        //$this->load->template('ticket/edit_request', $data);
    }
    
	//VIEW REQUEST
    public function viewRequest()
    {
        $input = $this->load->lib('Input');
        $model_sample = $this->load->model('Sample');
        $model_request = $this->load->model('Request');
        $data = array(
            'site'          => $this->site(),
            'session'       => $this->session,
            'admin'         => $this->isAdmin(),
            'fnb'           => $this->isAdminFnb(),
            'qa'            => $this->isAdminQA(),
            'kahi'          => $this->isKahi(),
            'purchasing'	=> $this->isPurchasing(),
            'page'          => 'View Request',
            'fdata'         => $this->getFlashData(),
            'option' => array(
                    'exjs' => array(
                        './Resources/js/rawmat_closed.js'
                    )
            )
        );
        $opt = array(
            'order_by' => 'request_id',
            'order' => 'DESC'
        );
       
        if (isset($_GET['id'])) {
            $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
            $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
            if ($input->validate()) {
                $data['result'] = $model_request->getTicketRequestByID(array('request_id'=>$_GET['id']), "request_id, request_closed, request_name, request_type, type_name AS reason, request_date, last_update, deadline_date, display_name, type_name");
                $data['list'] = $model_sample->getSampleByRequestId($_GET['id'], $opt);
                $this->load->template('ticket/view_request', $data);
            } else {
                $this->showError(2);
            }
        } else {
                $this->showError(2);
        }
        
    }
    
    public function viewMyRequest() 
    {
        $model_request = $this->load->model('Request');
        $qo['page'] = (!empty($_GET['page']) ? $_GET['page'] : 1);
        #$qo['order_by'] = (!empty($this->session['option']['order_by']) ? $this->session['option']['order_by'] : 'request_date');
        #$qo['order'] = (!empty($this->session['option']['order']) ? $this->session['option']['order'] : 'DESC');
        $qo['result'] = 10; //result data max 10
        $data['site'] = $this->Site();
        $data['admin'] = $this->isAdmin();
        $data['fnb'] = $this->isAdminFNB();
        $data['qa'] = $this->isAdminQA();
        $data['kahi'] = $this->isKahi();
        
        $data['page'] = 'My Request';
        $data['filter'] = $qo;
        if(isset($_POST['submit']) && $_POST['submit'] == 'Search Now') $data['post'] = $_POST;

        $data['session'] = $this->session;
        //$data['option']['exjs'][]  = array('Resources/js/indexmrp.js');
        //$data['option']['exjs'][]  = 'Resources/js/indexmrp.js';
        $data['request'] = $this->requestData($qo);
        $data['request_type'] = $this->mrequest()->getAllRequestType();
        $data['status'] = $this->mrequest()->getAllSampleStatus();

        $this->load->template('home/my_request', $data);

    }

    //CLOSE REQUEST
    public function viewCloseRequest()
    {

        $id = $_GET['idRequest'];
        $model_request = $this->load->model('Request');
        $model_sample = $this->load->model('Sample');
        $model_request->modifyRequest($id);

        $mail_close_request = array( //email request closed
            'title'  => "Request ID #".$id." (CLOSED)",
            'view'   => 'email/close_request',
            'data'   =>  array(
                'detail' => $model_request->getTicketRequestByID(array('request_id' => $id), "request_id, request_name, request_type, type_name AS reason, request_date, last_update, deadline_date, display_name"),
                'content' => 'Request detail:',
                'header'  => ''
            ),
            'recipient' =>   array(
                'address' => "it6.jkt@sushitei.co.id",
                'name'    => "FINA A J", 
            ),
        );
        $return = array(
                    'success' => 1,
                    'email'   => 0, 
                );
        if ($this->send_email($mail_close_request)) {
            $return['email'] = 1;
        }
        $return['message'] = $this->message;

        header("Location: index.html");
    }

    
    protected function requestData($qo)
    {
        if(isset($_POST['submit']) && $_POST['submit'] == 'Search Now'){
            $input = $this->load->lib('Input');
            if(!empty($_POST['page'])){
                $input->addValidation('page_format', $_POST['page'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('page_max', $_POST['page'], 'max=3', 'Cek kembali input anda');
            }
            if(!empty($_POST['status'])){
                $input->addValidation('status_format', $_POST['status'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('status_max', $_POST['status'], 'max=2', 'Cek kembali input anda');
            }
            if(!empty($_POST['request_type'])){
                $input->addValidation('type_format', $_POST['request_type'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('type_max', $_POST['request_type'], 'max=3', 'Cek kembali input anda');
            }
            if(!empty($_POST['submit_start'])) $input->addValidation('type_format', $_POST['submit_start'], 'date', 'Cek kembali input anda');

            if(!empty($_POST['submit_end'])) $input->addValidation('type_format', $_POST['submit_end'], 'date', 'Cek kembali input anda');

            if(!empty($_POST['title'])) $input->addValidation('title_format', $_POST['page'], 'numeric', 'Cek kembali input anda');

            if($input->validate() == TRUE){

                if(!empty($_POST['page'])) $qo['page'] = $_POST['page'];

                if(!empty($_POST['status'])) $filter['ts.sample_status'] = array('operator' => '=','value' => "{$_POST['status']}");

                if(!empty($_POST['request_type'])) $filter['request_type'] = array('operator' => '=','value' => "{$_POST['request_type']}");

                if(!empty($_POST['title'])) $filter['request_name'] = array('operator' => ' LIKE ','value' => "%{$_POST['title']}%");

                if(!empty($_POST['submit_start'])){
                    $filter['request_start'] = $_POST['submit_start'];
                    if(!empty($_POST['submit_end'])){
                        $filter['request_end'] = $_POST['submit_end'];
                    } else{
                        $filter['request_end'] = $_POST['submit_start'];
                    }
                }
                
                $filter['request_user'] = array('operator' => '=','value' => $this->session['user_id']);
                #exit(print_r($filter));
                if(!empty($filter) && count($filter) > 0){
                    $result = $this->mrequest()->getFiltered($filter, $qo);
                }else{
                    $result = $this->mrequest()->getFiltered(array(), $qo);;
                }

            }else{
                $result = array('error' => $input->_error);
            }
        }else{
            $result = $this->mrequest()->getFiltered(array(), $qo);
        }

        return $result;
    }

    /* MAILING */
    private function send_email($option){ 
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail   = new PHPMailer(true);
        //$stmail = $this->load->lib('STMail');
        try
        {
            $stmail->IsSMTP(); // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                    // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "it6.jkt@sushitei.co.id"; // SMTP account username email dkirim dari alamat email ini
            $stmail->Password   = "(Jannah123)";        // SMTP account password

            $stmail->SetFrom('it6.jkt@sushitei.co.id', 'Material Procurement');

            $stmail->AddReplyTo('it6.jkt@sushitei.co.id', 'Material Procurement');
            
            $stmail->isHTML('true');

            $stmail->Subject    = $option['title'];
            
            $returned = $this->load->view($option['view'], $option['data']); 
            
            $stmail->MsgHTML($returned); // return view data
            
            $stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
            
            $memail = $this->load->model('Email');
            $emailcc = $memail->getMailRecipient();
            foreach($emailcc as $recipient){
                $stmail->AddBCC($recipient['email'], $recipient['display_name']);

                //print_r($emailcc);exit();
            }
            
            return ($stmail->Send() ? 1 : 0);
            
        } catch (phpmailerException $e) {
            $this->message = $e->errorMessage();
            return 0;
            
        } catch (Exception $e) {
            $this->message = $e->getMessage();
            return 0;
            
        }
        
    }

} /*END HERE*/