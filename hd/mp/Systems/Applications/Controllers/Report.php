<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Report extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewExport', 'phpExcel'),
                'groups'=>array('*'),
                //'groups'=>array('Super Admin', 'Admin QA', 'Purchasing', 'Administrator'),
            ),
        );
    }
    
    public function site()
    {
        $site = array(
            'root' => 'rawmat'
        );
        return $site;
    }

    function index()
    {
        $data = array(
            'menu'=>'adminmenu',
            'session'      => $session,
        );
        $this->load->template('home/index',$data);
    }
    
    //EXPORT REPORT TO EXCEL
    function viewExport()
    {
        $data = $this->load->model('Reports');
        $option = array(
            'file_name' => 'MP-Export',
            'now'       => date("d-M-Y_H.i"),
            'context_title' => 'Sushi Tei Indonesia - Data Sample Material Procurement'
        );
        $query_option = array(
            'order_by' => 'request_id',
            'order'    => 'DESC'
        );
        if (empty($this->session['last_query']) || is_array($this->session['last_query'])) {
            if ($this->session['group'] > 5) {
                $query = array();
            }else{
                $query = array(
                    'ticket_user' => $this->session['user_id'] 
                );
            }
        }else{
            $query = $this->session['last_query'];
        }
        
        #exit(json_encode($_SESSION));
        $this->phpExcel($data->showAllSampleListByRequest($query_option, $this->session['filter']), $option);
        #exit(print_r($data->showAllSampleListByRequest($query_option)));
    }

    function phpExcel($ticket_result, $option)
    {
        $objPHPExcel = $this->load->lib('PHPExcel');
        $objPHPExcel->getProperties()->setCreator("Sushitei IT")
                    ->setLastModifiedBy("Sushitei IT")
                    ->setTitle("Material Procurement")
                    ->setSubject("Material Procurement")
                    ->setDescription("In America.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("MP");

                    /*KONTEN DISINI*/
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:P1')
                    ->setCellValue('A1', $option['context_title'])
                    ->setCellValue('A3', 'No.')
                    ->setCellValue('B3', 'Nama Request')
                    ->setCellValue('C3', 'Tanggal Request')
                    ->setCellValue('D3', 'Nama Sample')
                    ->setCellValue('E3', 'Tanggal Sample')
                    ->setCellValue('F3', 'Produsen')
                    ->setCellValue('G3', 'Supplier')
                    ->setCellValue('H3', 'Kemasan(Berat)')
                    ->setCellValue('I3', 'Kondisi')
                    ->setCellValue('J3', 'Mata Uang')
                    ->setCellValue('K3', 'Harga')
                    ->setCellValue('L3', 'Foto Sample')
                    ->setCellValue('M3', 'Status Sample')
                    ->setCellValue('N3', 'Tanggal Uji')
                    ->setCellValue('O3', 'Hasil Uji')
                    ->setCellValue('P3', 'Alasan Reject');

        // Miscellaneous glyphs, UTF-8
        $sheet = $objPHPExcel->getActiveSheet();
        if(is_array($ticket_result) && !empty($ticket_result)){
            $no = 1;
            $row = 4;
            foreach($ticket_result as $result){
                $request_date = date('d-M-Y', strtotime($result['request_date']));
                $sample_date = date('d-M-Y', strtotime($result['sample_date']));
                $last_update = date('d-M-Y', strtotime($result['last_update']));
                $log_date = date('d-M-Y H:i', strtotime($result['log_date']));
               
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A{$row}", $no)
                            ->setCellValue("B{$row}", $result['request_name'])
                            ->setCellValue("C{$row}", $request_date)
                            ->setCellValue("D{$row}", $result['sample_name'])
                            ->setCellValue("E{$row}", $sample_date)
                            ->setCellValue("F{$row}", $result['producer_name'])
                            ->setCellValue("G{$row}", $result['supplier_name'])
                            ->setCellValue("H{$row}", $result['sample_size'])
                            ->setCellValue("I{$row}", $result['condition_name'])
                            ->setCellValue("J{$row}", $result['sample_currency'])
                            ->setCellValue("K{$row}", $result['sample_price'])
                            ->setCellValue("M{$row}", $result['status'])
                            ->setCellValue("N{$row}", ($result['log_status'] != 1) && strtotime($result['log_date']) != 0 ? $log_date : '-')

                            ->setCellValue("O{$row}", (!empty($result['status']) && $result['status'] == 'FNB Release' || $result['status'] == 'QA Release' || $result['status'] == 'KAHI Release') ? $result['log_content'] : '-')

                            ->setCellValue("P{$row}", (!empty($result['status']) && $result['status'] == 'FNB Reject' || $result['status'] == 'QA Reject' || $result['status'] == 'KAHI Reject') ? $result['log_content'] : '-');

                            /*Gambar*/
                            $location = 'Resources/images/sample/';
                            if (!empty($result['document_name']) && file_exists($location.$result['document_name'])) {
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Foto Sample');
                                $objDrawing->setDescription('Foto Sample');
                                $objDrawing->setPath($location.$result['document_name']);
                                $objDrawing->setHeight(150);
                                $objDrawing->setCoordinates("L{$row}");
                                $objDrawing->setOffsetY(10);
                                $objDrawing->setOffsetX(10);
                                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                                $sheet->getRowDimension($row)->setRowHeight(150);
                                $sheet->getColumnDimension('L')->setWidth(40);
                            }
                            
                $no++;
                $row++;
            }
        } else {
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:P4')
                    ->setCellValue('A4', "Tiket kosong");
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Category');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=Sample-Export_{$option['now']}.xlsx");
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit();

    }

/*
* End Home Class
*/    
}
