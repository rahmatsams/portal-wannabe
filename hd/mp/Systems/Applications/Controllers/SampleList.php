<?php  
    /**
     * 
     */
    class SampleList extends Controller
    {
        
        function accessRules()
        {
            $purchasing = array();
            foreach($this->checkControllerModel()->getAllPurchasingRole() as $r){
                $purchasing[] = $r['role_name'];
            }   
            return array(
                array('Allow', 
                    'actions'=>array('viewListIndex', 'viewNewList', 'viewDeleteList', 'viewEditList'),
                    'groups'=>array_merge(array('Super Admin', 'Administrator'), $purchasing),
                ),
                array('Deny', 
                    'actions'=>array('viewListIndex', 'viewNewList', 'viewDeleteList', 'viewEditList'),
                    'groups'=>array('Guest'),
                ),
        );
        }

        public function site()
        {
            $site = array(
                'root' => 'rawmat'
            );
            return $site;
        }

        //SAMPLE LIST INDEX 
        function viewListIndex()
        {
            $model_list = $this->load->model('SampleList');
            $query_option = array(
                'page'      => (isset($_GET['page'])) ? $_GET['page'] : 1,
                'result'    => 10,
                'order_by'  => 'list_id',
                'order'     => 'ASC',
            );
            $data = array(
                'session'    => $this->session,
                'admin'      => $this->isAdmin(),
                'list'       => $model_list->getAllCountList($query_option),
                'total'      => $model_list->getCountResult(),
                'page'       => $query_option['page'],
                'max_result' => $query_option['result'],
                'option'     => array(
                        'exjs' => array(
                            './Resources/js/administrator.js'
                        )
                ),
            );
            $this->load->template('admin/samplelist/index', $data);
        }

        //SUBMIT SAMPLE LIST Positive Non-positive 
        function viewNewList()
        {
            $model_list = $this->load->model('SampleList');
            $data = array(
                'session'   => $this->session,
                'admin'     => $this->isAdmin(),
                'page'      => 'Manage Sample List',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'create_list') {
                $input = $this->load->lib('Input');
                $input->addValidation('list_name', $_POST['list_name'], 'min=1', 'Must be filled');
                
                if ($input->validate()) {
                        $list = array(
                            'list_name' => $_POST['list_name'],    
                        );

                        if ($model_list->newSampleList($list)) {
                            echo "<script>alert('List Submit Success'); window.location.replace('admin_list.html');</script>";
                        }
                    }else{
                        $data['error'] = $input->_error;
                        $this->load->template('admin/samplelist/new', $data);
                    }   
            }else{
                $this->load->template('admin/samplelist/new', $data);
            }
        }

        //EDIT SAMPLE LIST
        public function viewEditList()
        {
            $model_list = $this->load->model('SampleList');
            $input = $this->load->lib('Input');
            $data = array(
                'session' => $this->session,
                'admin'      => $this->isAdmin(),
                'page'      => 'Manage Sample List',
            );
            if (isset($_POST['submit']) && $_POST['submit'] == 'edit_list') {
                $input->addValidation('list_name', $_POST['list_name'], 'min=1', 'Must be filled');
                
                if ($input->validate()) {
                    $list = array(
                        'list_name' => $_POST['list_name'],
                         
                    );
                    if ($model_list->editSampleList($list, array('list_id' => $_GET['id'] ))) {
                        header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                    }
                }else{
                    $data['error'] = $input->_error;
                    $this->load->template('admin/samplelist/edit', $data);
                }
            }else{
                if (isset($_GET['id'])) {
                    $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                    $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                    if ($input->validate()) {
                        $data['list'] = $model_list->getListById(array('list_id'=>$_GET['id']));
                        $this->load->template('admin/samplelist/edit', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $this->showError(2);
                }
            }
        }

        //DELETE SAMPLE LIST
        public function viewDeleteList()
        {
            $id = $_GET['idList'];
            $model_list = $this->load->model('SampleList');
            $model_list->deleteList($id);
            header("Location: admin_list.html");
        }       

    }

?>