$(document).ready(function(){
        $("#mainCategory").change(function(event){
            event.preventDefault();
            changeValue();
        });
        $(document).on("click", ".page-navigate", function(event){
            event.preventDefault();
            data_id = $(this).attr("data-id");
            $("#ticketPage").val(data_id);
            var me = $(this);
            searchForm(me);
            
        });
        $("#advanceSearch").submit(function(event){
            // cancels the form submission
            $("#ticketPage").val("1");
            event.preventDefault();
            var me = $(this);
            searchForm(me);
        });
        
        $("#sortResult").change(function(event){
            // cancels the form submission
            $("#ticketPage").val("1");
            event.preventDefault();
            var me = $(this);
            searchForm(me);
        });
        
        function searchForm(me){
            title=$("#ticketTitle").val();
            status=$("#ticketStatus").val();
            main=$("#mainCategory").val();
            sub=$("#subCategory").val();
            date_start=$("#ticketDateFirst").val();
            date_end=$("#ticketDateLast").val();
            store=$("#storeInput").val();
            page=$("#ticketPage").val();
            sort=$("#sortResult").val();
            if ( me.data("requestRunning") ) {
                $("#advanceSearch").attr("disabled", "disabled");
                return;
            }

            me.data("requestRunning", true);
            $.ajax({
                type: "POST",
                url: "ajax_search.html",
                data: "sort="+sort+"&page="+page+"&ticket_title="+title+"&status="+status+"&main_category="+main+"&sub_category="+sub+"&date_start="+date_start+"&date_end="+date_end+"&store="+store,
                success: function(html){
                    $("#listTable").html(html);
                },
                complete: function() {
                    $.ajax({
                        type: "POST",
                        url: "ajax_search_page.html",
                        success: function(html){
                            $("#pagingBottom").html(html);
                        },
                        complete: function() {
                            me.data("requestRunning", false);
                            $("#advanceSearch").removeAttr("disabled");
                        },
                    });
                },
            });
            
        }
        
        function changeValue(){
            main_category=$("#mainCategory").val();
            if(main_category == 0){
                $("#subCategory").html("<option value=\'0\' selected></option>");
                $("#subCategory").attr("disabled", true);
            }else{
                $.ajax({
                    type: "POST",
                    url: "ajax_subcategory.html",
                    data: "category="+ main_category,
                    success: function(json){
                        
                        if(json=="false")    {
                            $("#subCategory").html("<option value=\'0\' selected></option>");
                            $("#subCategory").attr("disabled", true);
                        } else {
                            var data = $.parseJSON(json);
                            $("#subCategory").html("");
                            $("#subCategory").append("<option value=\'999\'>All</option>");
                            $(data).each(function() {
                                var jsondata = "<option value=\'"+this.category_id+"\'>"+this.category_name+"</option>";
                                $("#subCategory").append(jsondata);
                            });
                            $("#subCategory").attr("disabled", false);
                        }
                    },
                });
            }
            return false;
        }
    });