$(document).ready(function(){
    $("#mainCategory").change(function(event){
        event.preventDefault();
        changeValue();
    });
    $("#subCategory").change(function(event){
        event.preventDefault();
        var option = $("#subCategory option:selected").data("upload");
        if(option == 1){
            $("#addFile").removeClass("hidden");
            $("#fileUpload").prop("disabled", 0);
        }else{
            $("#addFile").addClass("hidden");
            $("#fileUpload").prop("disabled", 1);
        }
    });
    $("#onbehalf").change(function(event){
        event.preventDefault();
        if($("#onbehalf").prop("checked") == true){
            changeBehalf();
        } else {
            $("#userlist").attr("disabled", true);
        }
        
    });
   
});