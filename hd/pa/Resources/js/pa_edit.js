$(document).ready(function(){
    $("#mainCategory").change(function(event){
        event.preventDefault();
        changeValue();
    });
    var detail_height = $("#tickettabledetail").height();
    $("#tickettabledesc").css("min-height", detail_height);
    $("#commentbutton").click(function(event){
        var text = "Komentar";
        var text2 = "Tambahan komentar";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#assignbutton").click(function(event){
        var text = "Assign";
        var text2 = "Tugaskan tiket ke staf yang terdaftar.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleAssign();
    });
    $("#takeoverbutton").click(function(event){
        var text = "Ambil Alih Tiket";
        var text2 = "Mengambil alih tiket.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#needsparebutton").click(function(event){
        var text = "Need Spare-part";
        var text2 = "Mark this ticket, need to order sparepart.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleSparePart();
    });
    $("#cancelsparebutton").click(function(event){
        var text = "Cancel Spare-part";
        var text2 = "Cancel order sparepart.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#donesparebutton").click(function(event){
        var text = "Sparepart Done";
        var text2 = "Mark this ticket, the ordered sparepart has arrived.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#resolvebutton").click(function(event){
        var text = "Solusi";
        var text2 = "Solusi";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleResolve();
    });
    $("#closebutton").click(function(event){
        var text = "Close ticket";
        var text2 = "Close ticket after assigned staff solve it.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });

    
    $("#forcebutton").click(function(event){
        var text = "Force Close";
        var text2 = "Force close the ticket";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#vendorbutton").click(function(event){
        var text = "Vendor";
        var text2 = "Note the ticket need a vendor to solve it";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#reopenbutton").click(function(event){
        var text = "Re-Open Ticket";
        var text2 = "Re-open closed ticket";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#cancelstatus").click(function(event){
        showActivity();
        if($("#submitstatus").html() == "Edit"){
            toggleEdit();
        }
        if($("#submitstatus").html() == "Assign"){
            toggleAssign();
        }
        if($("#submitstatus").html() == "Resolve"){
            toggleResolve();
        }
        $("#tickettitle").html("Ticket Command");
    });
    $("#editbutton").click(function(event){
        var text = "Edit Tiket";
        var text2 = "Edit detail tiket";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleEdit();
    });
    $("#updateticketstatus").submit(function(event){
        event.preventDefault();
        var me = $(this);
        editTicket(me);
        
    });
    
});