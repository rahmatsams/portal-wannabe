<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl . '/css/style.css'; ?>" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl . '/css/smoothness/jquery-ui-1.9.1.custom.css'; ?>" type="text/css" media="screen, projection" />
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl . '/js/jquery-1.8.2.js'; ?> "></script>
    <script type="text/javascript" src="<?php echo  Yii::app()->theme->baseUrl . '/js/script.js' ?>"></script>
    <script type="text/javascript" src="<?php echo  Yii::app()->theme->baseUrl . '/js/delete.js' ?>"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl . '/js/jquery-ui-1.9.1.custom.js'; ?>"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
    <div class="header">
        <div class="place-over-logo"><img src="<?php echo Yii::app()->theme->baseUrl . '/images/logo2.png'; ?>" /><span style="font-weight:bold;">Ticketdesk</span> Sushi Tei</div>
        <div id="welcome-message">
            <div class="portal-name"><b>Selamat Datang <?php echo Yii::app()->user->name; ?></div>
        </div><!-- header -->
	
        <div id="main-menu">
            <ul>
                <?php
                if(!Yii::app()->user->isGuest){
                    $menulist = array(
                        'Home' => Yii::app()->request->baseUrl,
                        'Ticket Center'  =>  array(
                            'Create New Ticket' => Yii::app()->request->baseUrl.'/my_details.html',
                            'My Open Ticket' => Yii::app()->request->baseUrl.'/change_password.html',
							'My Closed Ticket' => Yii::app()->request->baseUrl.'/change_password.html',
                        ),
                        'Logout' => Yii::app()->request->baseUrl.'/logout.html'
                    );
                } else {
                    $menulist = array(
                        'Home' => Yii::app()->request->baseUrl,
                        'Login'  => Yii::app()->request->baseUrl .'/login.html',
                        'About'  =>  array(
                            'News' => Yii::app()->request->baseUrl . '/news.html',
                            'Events' => Yii::app()->request->baseUrl .'/events.html',
                            'Knowledge Base' => Yii::app()->request->baseUrl .'/knowledge_base.html',
                            'Contacts' => Yii::app()->request->baseUrl .'/contacts.html',
                        )
                    );
                }
                
                foreach($menulist as $menu => $url){
                    if(is_array($url)){
                        echo "
                <li>
                    <a href=\"#\" class=\"has-child\">{$menu}</a>
                    <div class=\"sub-menu\">
                        <ul>";
                        foreach($url as $menu2 => $url2){
                            echo "
                            <li><a href=\"{$url2}\">{$menu2}</a></li>";
                        }
                    echo '
                        </ul>
                    </div>
                </li>
                    ';
                    } else {
                        echo "
                <li><a href=\"{$url}\" class=\"top-menu-list ". ($url == "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}" ? 'active' : '') ."\">{$menu}</a></li>";
                    }
                }
            ?>
            </ul>
        </div><!-- mainmenu -->
    </div>
    
    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear-view"></div>
    <div class="footer">
        <br/>Copyright &copy; PT. Sushi Tei 2015
    </div><!-- footer -->

</div><!-- page -->

</body>
</html>
