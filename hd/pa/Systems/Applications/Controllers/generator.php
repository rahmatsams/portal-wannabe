<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Generator extends Controller
{
	public function accessRules()
	{
		return array(
			array('Deny', 
				'actions'=>array('employee_logout', 'submit_ticket'),
				'groups'=>array('Guest'),
			),
			array('Allow', 
				'actions'=>array('index', 'employee_login'),
				'role'=>array('*'),
			),
		);
	}
	
    public function index()
    {
		
		$data = array(
            '_mySession' => $this->_mySession,
        );
		if($this->isGuest() < 1) {
			$ticket = $this->load->model('Tickets');
			$option = array(
				'page' => 1,
				'result' => 10,
				'order_by' => 'submit_date',
				'order' => 'DESC'
			);
			if($this->_mySession['role'] > 5) {
				$data['ticket_list'] = $ticket->showAllTicket($option);
				$this->load->template('home/home', $data);
			} else {
				$data['ticket_list'] = $ticket->showMyActiveTicket($this->_mySession['userid'],$option);
				$this->load->template('ticket/myticket',$data);
			}
			
		} else {
			header("Location: ".siteUrl().'login.html');
		}
        
	}
	
    public function employee_login()
    {
		$data = array(
			'_mySession' => $this->_mySession,
		);
		$option = array(
				'tinymce' => 1,
				'scriptload' => '
<script>
$(document).ready(function(){
	$("#login").submit(function(event){
		// cancels the form submission
		event.preventDefault();
		submitForm();
	});
	
	function submitForm(){
		username=$("#inputName").val();
		password=$("#inputPassword").val();
		$.ajax({
			type: "POST",
			url: "ajax_login.html",
			data: "ticket_email="+username+"&ticket_password="+password,
			success: function(html){    
				if(html=="true")    {
					window.location.assign("index.html");
				} else {
					$("#userName").addClass("has-error");
					$("#userName .control-label").removeClass("sr-only");
				}
			},
		});
		return false;
	}
});
</script>
				',
				
				
		);
		
		$this->load->template('home/login_member', $data, $option);
		
    }
	
	public function employee_logout()
	{
		if($this->isGuest() < 1) {
			$this->unsetSession('user_id');
			$this->setSession('username','Guest');
			$this->setSession('group','Guest');
			$this->setSession('role',1);
			header("Location: ".siteUrl());
		} else {
			header("Location: ".siteUrl().'login.html');
		}
	}
	
	public function view_ticket()
	{
		
	}
}
/*
* End Home Class
*/