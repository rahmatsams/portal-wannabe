<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Categories extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('index', 'insert_category' , 'drop_category', 'edit_category', 'delete_categories'),
                'groups'=>array('Guest'),
            ),
            array('Allow', 
                'actions'=>array('index', 'insert_category' , 'drop_category', 'edit_category', 'manage', 'manage_sub'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin PA Desk', 'Admin GA'),
            ),
        );
    }
    
    public function index(){
        $m_category = $this->load->model('TicketCategory');
        $data = array(
            '_mySession' => $this->_mySession,
            'category' => $m_category->getMainCategory(),
        );
        $option = array(
            'admin' => $this->isAdmin(),
            'personal_js' => 1,
            'scriptload' => '
            <script type="text/javascript">
                $(document).ready(function(){
                    $(".edit-category").click(function(event){
                        event.preventDefault();
                        classid = this.id;
                        editCategory(classid);
                        getSubCategory(classid);
                    });
                    $(document).on("click", ".edit-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        editFormSubCategory(dataid);
                    });
                    $(document).on("click", ".edit-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        editFormSubCategory(dataid);
                    });
                    $(document).on("click", ".cancel-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        cancelFormSubCategory(dataid);
                    });
                    $("#editcategory").submit(function(event){
                        // cancels the form submission
                        event.preventDefault();
                        submitEdit();
                    });
                    $("#addrowcategory").click(function(event){
                        event.preventDefault();
                        if($("#insertsub").hasClass("hidden")){
                            $("#insertsub").removeClass("hidden");
                        } else {
                            $("#insertsub").addClass("hidden");
                        }
                    });
                    $(document).on("submit", "#addsubcategory", function(event){
                        // cancels the form submission
                        event.preventDefault();
                        var me = $(this);
                        submitSubCategory(me);
                    });
                    $("#addcategory").submit(function(event){
                        // cancels the form submission
                        event.preventDefault();
                        var me = $(this);
                        submitCategory(me);
                    });
                    $("#editcategory").submit(function(event){
                        // cancels the form submission
                        event.preventDefault();
                        var me = $(this);
                        submitEditCategory(me);
                    });
                    $(".deleteconfirmationmodal").click(function(event){
                        event.preventDefault();
                        dataid = $(this).data("id");
                        $("#deleteconfirm").attr("data-id", dataid);
                    });
                    $(document).on("click", "#deleteconfirm", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        window.location.assign("delete_confirmation_"+dataid+".html");
                    });
                    $(document).on("click", ".delete-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        $("#deleteconfirm").attr("data-id", dataid);
                    });
                });
               
            </script>
            '
        );
        $this->load->template('admin/index_category', $data, $option);
    }
    
    public function insert_category(){
        if(isset($_POST['category'])){
            $input = $this->load->lib('input');
            $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
            if(isset($_POST['sub_category'])){
                $input->addValidation('category_parent_id', $_POST['category_parent_id'], 'numeric', 'Cek kembali input anda');
            }
            $input->addValidation('description', $_POST['description'], 'min=1', 'Cek kembali input anda');
            if($input->validate()){
                unset($_POST['category']);
                if(!isset($_POST['category_parent_id'])){
                    $_POST['category_parent_id'] = 1;
                }
                $m_ticketcategory = $this->load->model('TicketCategory');
                if($m_ticketcategory->createCategory($_POST)){
                    echo $m_ticketcategory->lastInsertID;
                } else {
                    echo 'failed';
                }
            }else{
                echo 'failed';
            }
            
        }
    }
    
    public function edit_category(){
        if(isset($_POST['category_id'])){
            $input = $this->load->lib('input');
            $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
            $input->addValidation('category_id', $_POST['category_id'], 'numeric', 'Cek kembali input anda');
            $input->addValidation('category_id', $_POST['category_id'], 'min=1', 'Cek kembali input anda');
            $input->addValidation('category_id', $_POST['category_id'], 'max=3', 'Cek kembali input anda');
            $input->addValidation('description', $_POST['description'], 'min=1', 'Cek kembali input anda');
            if($input->validate()){
                $update = array(
                    'category_name' => $_POST['category_name'],
                    'description' => $_POST['description'],
                );
                $where = array(
                    'category_id' => $_POST['category_id']
                );
                $m_ticketcategory = $this->load->model('TicketCategory');
                if($m_ticketcategory->editCategory($update, $where)){
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }else{
                echo 'failed';
            }
            
        }
    }
    
    public function drop_category(){
        if(isset($_GET['category_id'])){
            $input = $this->load->lib('Input');
            $input->addValidation('category_id', $_GET['category_id'], 'max=6', 'Exceding allowed range');
            $input->addValidation('category_id', $_GET['category_id'], 'numeric', 'Exceding allowed range');
            if($input->validate()){
                $m_category = $this->load->model('Category');
                if($m_category->deleteCategory(array('category_id' => $_GET['category_id']))){
                    header("Location: category_admin.html");
                }
            } else {
                print_r($input->_error);
            }
        }else{
            echo 'there';
        }
    }

    public function manage(){
        $input = $this->load->lib('Input');
        $m_category = $this->load->model('TicketCategory');
        $data = array('_mySession' => $this->_mySession);

        if(isset($_POST['action'])){
            if($_POST['action'] == 'Add Category'){
                $input->addValidation('parent_max', $_POST['category_parent_id'], 'max=6', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['category_parent_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['description'], 'min=1', 'Cek kembali input anda');
                if($input->validate()){
                    $m_ticketcategory = $this->load->model('TicketCategory');
                    $form['category_parent_id'] = $_POST['category_parent_id'];
                    $form['category_name'] = $_POST['category_name'];
                    $form['description'] = $_POST['description'];
                    if(!empty($_POST['need_photo'])){
                        $form['need_photo'] = 1;
                    }
                    if($m_ticketcategory->createCategory($form)){
                        $data['success'] = 1;
                    } else {
                        $data['success'] = 0;
                    }
                }else{
                    $data['success'] = 0;
                }
            }elseif($_POST['action'] == "Update"){
                $input = $this->load->lib('input');
                $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
                $input->addValidation('category_id', $_POST['category_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('category_id', $_POST['category_id'], 'min=1', 'Cek kembali input anda');
                $input->addValidation('category_id', $_POST['category_id'], 'max=3', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['description'], 'min=1', 'Cek kembali input anda');
                if($input->validate()){
                    $update = array(
                        'category_name' => $_POST['category_name'],
                        'description' => $_POST['description'],
                        'need_photo' => (!empty($_POST['description']) ? 1 : 0),
                    );
                    $where = array(
                        'category_id' => $_POST['category_id']
                    );
                    $m_ticketcategory = $this->load->model('TicketCategory');
                    if($m_ticketcategory->editCategory($update, $where)){
                        $data['success'] = 1;
                    } else {
                        $data['success'] = 0;
                    }
                }else{
                    $data['success'] = 0;
                }
            }
        }
        
        if(isset($_GET['id'])){
            $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
            $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
            if($input->validate()){
                $data['_mySession'] = $this->_mySession;
                $data['category'] = $m_category->getCategoryId(array('cid'=>$_GET['id']));
                $data['list_category'] = $m_category->getSubCategory($_GET['id']);
                $this->load->template('admin/manage_category', $data);
            } else {
                $this->showError(2);
            }
        }else{
            $this->showError(2);
        }
        
    }
    
    public function manage_sub(){
        $input = $this->load->lib('Input');
        $m_category = $this->load->model('TicketCategory');
        $data = array('_mySession' => $this->_mySession);

        if(isset($_POST['action'])){
            if($_POST['action'] == 'Add Entry'){
                $input->addValidation('category_max', $_POST['category'], 'max=6', 'Cek kembali input anda');
                $input->addValidation('category_format', $_POST['category'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('recipient_name', $_POST['recipient_name'], 'alpha_numeric', 'Cek kembali input anda');
                $input->addValidation('email_format', $_POST['email'], 'email', 'Cek kembali input anda');
                if($input->validate()){
                    $m_ticketcategory = $this->load->model('TicketCategory');
                    unset($_POST['action']);
                    if($m_ticketcategory->insertRecipient($_POST)){
                        $data['success'] = 1;
                    } else {
                        $data['success'] = 0;
                    }
                }else{
                    $data['success'] = 0;
                }
            }elseif($_POST['action'] == "Update"){
                $input = $this->load->lib('input');
                $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
                $input->addValidation('category_id', $_POST['category_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('category_id', $_POST['category_id'], 'min=1', 'Cek kembali input anda');
                $input->addValidation('category_id', $_POST['category_id'], 'max=3', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['description'], 'min=1', 'Cek kembali input anda');
                if($input->validate()){
                    $update = array(
                        'category_name' => $_POST['category_name'],
                        'description' => $_POST['description'],
                        'need_photo' => (!empty($_POST['need_upload']) ? 1 : 0)
                    );
                    $where = array(
                        'category_id' => $_POST['category_id']
                    );
                    if($m_category->editCategory($update, $where)){
                        $data['success'] = 1;
                    } else {
                        $data['success'] = 0;
                    }
                }else{
                    $data['success'] = 0;
                }
            }
        }
        
        if(isset($_GET['id'])){
            $input = $this->load->lib('Input');
            $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
            $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
            if($input->validate()){
                $m_category = $this->load->model('TicketCategory');
                $data['_mySession'] = $this->_mySession;
                $data['category'] = $m_category->getSubCategoryId(array('cid'=>$_GET['id']));
                $this->load->template('admin/manage_sub_category', $data);
            } else {
                $this->showError(2);
            }
        }else{
            $this->showError(2);
        }
        
        
    }
}
/*
* End Home Class
*/