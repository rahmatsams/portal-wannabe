	<div class="container-fluid">
        <h2 class="sub-header">Manage User</h2>
        <ol class="breadcrumb">
            <li><a href="ticket_admin.html">Admin Page</a></li>
            <li class="active"><a href="#">Manage User</a></li>
        </ol>
		<div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Display Name</th>
                        <th>User Group</th>
                        <th>Status</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($users) && !empty($users)){
                            $num = (($page-1)*10)+1;
                            foreach($users as $result){
                                switch($result['user_status']){
                                    case "1":
                                        $status = 'Active';
                                    break;
                                    default:
                                        $status = 'Disabled';
                                }
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['user_name']}</td>
                        <td>{$result['display_name']}</td>
                        <td>{$result['group_name']}</td>
                        <td>{$status}</td>
                        <td><a href=\"admin_edit_user_{$result['user_id']}.html\" id=\"{$result['user_id']}\" class='btn btn-primary btn-sm'>Edit</a></td>
                        <td><a class=\"delete-user\" id=\"deleteconfirmationmodal\" data-id=\"{$result['user_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\">Delete</a></td>
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="3">Result not found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
            $pages = ceil($total/$max_result);
                if($pages > 1){
                    echo '
                <nav>
                    <ul class="pagination pagination-sm">
                        <li>
                            <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
                        </li>';
                    for($i=1;$i <= $pages;$i++){
                        echo "<li ". ($page == $i ? 'class="active"' : '') ."><a href='admin_user_{$i}.html'>{$i}</a></li>";
                    }
                    echo '
                    <li>
                        <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                    </li>
                    </ul>
                </nav>';
                }
            ?>
            
            <a href="admin_create_user.html" class="btn btn-primary btn-sm">Create New User</a>
        </div>
        
    </div>