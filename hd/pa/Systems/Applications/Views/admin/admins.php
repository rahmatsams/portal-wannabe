	<div class="maincontent">
		<div class="content">
			<h3>Manage Admins</h3>
			<?php
				if(count($adminsdata) > 1){
					echo '<table style="border: 2px outset #fff;">';
					echo '	<tr>';
					echo '		<td>No.</td>';
					echo '		<td>User Name</td>';
					echo '		<td colspan="2">Action</td>';
					echo '	</tr>';
					$i = (isset($_GET['page']) ? ($_GET['page'] > 1 ? $_GET['page']*15-15+1 : 1) : 1);
					foreach($adminsdata as $admins){
						if(count($admins) > 1){
							echo '	<tr>';
							echo '		<td>'.$i.'</td>';
							echo '		<td>'.$admins['user_name'].'</td>';
							echo '		<td><a href="'.controllerUrl('admin','editadmins').'&id='.$admins['id_user'].'">Edit</a></td>';
							echo '		<td><a href="'.controllerUrl('admin','deleteadmins').'&id='.$admins['id_user'].'">Delete</a></td>';
							echo '	</tr>';
							$i++;
						}
					}
					echo '</table>';
				}else{
					echo 'No records Found';
				}
			?>
		</div>
	</div>