	<div class="container-fluid">
        <h2 class="sub-header">Kelola Kategori</h2>
        <ol class="breadcrumb">
            <li><a href="ticket_admin.html">Admin Page</a></li>
            <li><a href="category_admin.html">Kelola Kategori</a></li>
            <li><a href="manage_category_<?=$category['parent_id']?>.html"><?=$category['parent_name']?></a></li>
            <li class="active"><?=$category['category_name']?></li>
        </ol>
		<div class="table-responsive">
            <?php if(isset($success) && $success == 1): ?>
            
            <div class="col-md-12 bg-success" style="padding: 10px; margin-bottom: 10px;">
                Update Success
            </div>
            <?php elseif(isset($success) && $success == 0): ?>
            
            <div class="col-md-12 bg-danger" style="padding: 10px; margin-bottom: 20px;">
                Update Failed
            </div>
            <?php endif; ?>
            <div class="col-md-12 well">
                <div class="row">
                    <form id="editcategory" method="POST">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama</label>
                                <input id="catid" name="category_id" type="hidden" value="<?=$category['category_id']?>" class="form-control" maxlength="6" required>
                                <input id="catname" name="category_name" type="text" value="<?=$category['category_name']?>" class="form-control" maxlength="25" required>
                            </div>
                            <div class="form-group">
                                <label>Deskrispi</label>
                                <textarea id="catdesc" name="description" cols="20" rows="4" class="form-control" required><?=$category['description']?></textarea>
                            </div>
                            <div class="form-group">
                                <label class="checkbox-inline"><input type="checkbox" name="need_upload" value="1"<?=($category['need_photo'] == 1 ? ' checked' : '')?>>Butuh Gambar</label>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="action" value="Update" class="btn btn-sm btn-primary">
                            </div>
                        </div>
                    </form>
                 </div>
                 
            </div>
        </div>
    </div>