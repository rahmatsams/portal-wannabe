        <div class="container-fluid">
            <div class="row">
                <?php
                    if($_mySession['username'] != 'Guest' && $_mySession['group'] != 'Guest' && $_mySession['group'] != 'Administrator' && $_mySession['role'] < 10){
                        $menu_list = array(
                            'Overview' => 'index.html',
                            'Buat Tiket' => 'create_ticket.html',
                            'Logout' => 'logout.html'
                        );
                    } elseif($_mySession['role'] > 10){
                        $menu_list = array(
                            'Admin Tiket' => 'ticket_admin.html',
                            'Admin User' => 'ticket_users.html',
                            'Overview' => 'index.html',
                            'Buat Tiket' => 'create_ticket.html',
                            'Logout' => 'logout.html'
                        );
                    }
                    if(isset($menu_list)){
                        echo '
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        ';
                        foreach($menu_list as $menu => $url){
                            if($url == "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"){
                                $link = "<li class=\"active\"><a href=\"{$url}\">{$menu}<span class=\"sr-only\">(current)</span></a></li>\n\t\t\t\t\t";
                            }else{
                                $link = "<li><a href=\"{$url}\">{$menu}</a></li>\n\t\t\t\t\t";
                            }
                            echo $link;
                        }
                    echo '
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    ';
                    }else{
                        echo '
                        <div class="main">';
                    }
                ?>
                                    
                        <div class="content">
                            <div class="row">
                                <form id="register" method="POST" action="register.html">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label <?=isset($error['user_name']) ? '' : 'sr-only'?>" for="userName"><?=isset($error['user_name']) ? $error['user_name'] : ''?></label>
                                            <label for="userName">Nama User</label>
                                            <input id="userName" name="user_name" type="text" placeholder="Cth: stpim (tidak ada spasi dan Karakter Spesial)" class="form-control" maxlength="25" value="<?=(isset($input) && !empty($input['user_name']) ? $input['user_name'] : '')?>" required>
                                            <?=(isset($error) && !empty($error['user_name']) ? "<div class=\"warning\">{$error['user_name']}</div>" : '')?>
                                        </div>					
                                        <div class="form-group">
                                            <label class="control-label <?=isset($error['display_name']) ? '' : 'sr-only'?>" for="displayName"><?=isset($error['display_name']) ? $error['display_name'] : ''?></label>
                                            <label for="displayName">Nama tampilan</label>
                                            <input id="displayName" name="display_name" type="text" placeholder="Cth: Leonardo Da Vinci" class="form-control" maxlength="25" value="<?=(isset($input) && !empty($input['display_name']) ? $input['display_name'] : '')?>" required>
                                        </div>	
                                        <div class="row">
                                            <div class="col-sm-6 form-group" id="passwordForm">
                                                <label class="control-label <?=isset($error['password']) ? '' : 'sr-only'?>" for="passwordInput"><?=isset($error['password']) ? $error['password'] : ''?></label>
                                                <label>Password</label>
                                                <input id="passwordInput" name="password" type="password" placeholder="Masukkan password.." class="form-control" required>
                                            </div>	
                                            <div class="col-sm-6 form-group" id="passwordConfirmForm">
                                                <label class="control-label <?=isset($error['password_confirm']) ? '' : 'sr-only'?>" for="passwordConfirm"><?=isset($error['password_confirm']) ? $error['password_confirm'] : ''?></label>
                                                <label>Konfirmasi password</label>
                                                <input id="passwordConfirm" name="password_confirm" type="password" placeholder="Masukkan konfirmasi password.." class="form-control" required>
                                            </div>	
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label <?=isset($error['email']) ? '' : 'sr-only'?>" for="eMail"><?=isset($error['email']) ? $error['email'] : ''?></label>
                                            <label>Alamat Email</label>
                                            <input id="eMail" name="email" type="email" placeholder="Masukkan alamat email.." class="form-control" value="<?=(isset($input) && !empty($input['email']) ? $input['email'] : '')?>" required>
                                        </div>		
                                        <div class="form-group">
                                            <label>Outlet</label>
                                            <select name="outlet" class="form-control" required>
                                            <?php
                                                if(is_array($store)){
                                                    foreach($store as $outlet){
                                                        echo " 
                                                        <option value='{$outlet['store_id']}'>{$outlet['store_name']}</option>";
                                                    }
                                                    
                                                }
                                            ?>
                                            </select>
                                        </div>	
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <label>Pertanyaan keamanan</label>
                                                <select name="sec_quest" class="form-control" required>
                                                    <option value="" default></option>
                                                    <option value="1">Dimanakah outlet pertama anda?</option>
                                                    <option value="2">Siapakah nama panggilan kecil anda?</option>
                                                    <option value="3">Siapakah nama ibu anda?</option>
                                                    <option value="4">Apakah makanan kesukaan anda?</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Jawaban</label>
                                                <input name="answer" type="text" placeholder="Masukkan jawaban pertanyaan keamanan.." class="form-control" value="<?=(isset($input) && !empty($input['answer']) ? $input['answer'] : '')?>" required>
                                            </div>
                                        </div>
                                        <button class="btn btn-lg btn-primary" type="submit" class="btn btn-lg btn-info" name="register" value="Daftar">Daftar</button>
                                    </div>
                                </form> 
                            </div>    
                        </div>
                    </div>
                </div>