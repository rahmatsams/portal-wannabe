            <?php
                if(isset($flash_data['insert_status']) && $flash_data['insert_status'] == 1){
                    echo '<h2>A new category has been added</h2>';
                }
            ?>
			<h3>Kelola Kategori</h3>
			<?php
				if(is_array($categories_data) && count($categories_data) > 1){
					echo '<table>';
					echo '	<tr>';
					echo '		<td>No.</td>';
					echo '		<td>Category Name</td>';
					echo '		<td>Description</td>';
                    echo '		<td>Published</td>';
                    echo '		<td>Front</td>';
					echo '		<td colspan="2">Action</td>';
					echo '	</tr>';
					foreach($categories_data as $category){
                        echo '	<tr>';
                        echo '		<td>'.$count_start.'</td>';
                        echo '		<td><a href="'.controllerUrl('AdminCategory','view_category').'&id='.$category['category_id'].'">'.$category['category_name'].'</a></td>';
                        echo '		<td>'.trimText(htmlspecialchars_decode($category['category_desc']), 10, 'more',' ...').'</td>';
                        echo '      <td><a href="'.controllerUrl('AdminCategory','toogle_publish').'&id='.$category['category_id'].'">'.($category['published'] == 1 ? 'Unpublish' : 'Publish').'</a></td>';
                        echo '      <td><a href="'.controllerUrl('AdminCategory','toogle_front').'&id='.$category['category_id'].'">'.($category['front'] == 1 ? 'Don\'t Show' : 'Show').'</a></td>';
                        echo '		<td><a href="'.controllerUrl('AdminCategory','edit_category').'&id='.$category['category_id'].'">Edit</a></td>';
                        echo '		<td><a href="'.controllerUrl('AdminCategory','delete_category').'&id='.$category['category_id'].'">Delete</a></td>';
                        echo '	</tr>';
                        $count_start++;
					}
					echo '</table>';
				}else{
					echo 'No records Found';
				}
            if($total_page > 1){
                echo "
             <div class=\"page-nav\">
                Page :
                <ul>";
                for($i=1; $i <= $total_page; $i++){
                    $url = controllerUrl('AdminCategory');
                    $url .= "&page={$i}";
                    echo "
                    <li><a href=\"{$url}\">{$i}</a><li>
                    ";
                }
                echo "
                </ul>
            </div>";
            }
            ?>