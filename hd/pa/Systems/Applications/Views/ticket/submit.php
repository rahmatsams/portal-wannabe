                    <div class="container-fluid">
                        <h3 class="well" style="background: rgb(51,122, 183); color: #fff;">Submit new Ticket</h3>
                        <?=(isset($error) ? '<div class="alert alert-danger">Please check your Highlighted input below</div>' : '')?>
                        <div class="col-lg-12 well">
                            <div class="row">
                                <form action="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
                                    <div class="col-lg-12">
                                        <div class="form-group <?=(isset($error) && isset($error['title']) ? 'has-error' : '')?>">
                                            <label>Subject</label>
                                            <input name="title" type="text" placeholder="Type here.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['title']) ? "value='{$input['title']}'" : '')?> autocomplete="off" required>
                                            <?=(isset($error) && isset($error['title']) ? "<div class='alert alert-danger'>{$error['title']}</div>" : '')?>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-lg-4 form-group">
                                                <label>Ticket Type</label>
                                                <select name="type" class="form-control" required>
                                                    <option value="" default></option>
                                                    <?php
                                                        foreach($type as $t){
                                                            echo "\n<option value=\"{$t['type_id']}\">{$t['type_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div> -->
                                            <div class="col-lg-6 form-group">
                                                <label>Category</label>
                                                <select name="main_category" id="mainCategory" class="form-control" required>
                                                    <option value="" default></option>
                                                    <?php
                                                        foreach($main_category as $category_form){
                                                            echo "\n<option value=\"{$category_form['category_id']}\">{$category_form['category_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label>Sub-Category</label>
                                                <select name="sub_category" class="form-control" id="subCategory" disabled>
                                                    <option value="" default></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" hidden="">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="1" name="customer_impact">Impact Customer</input>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label>Current Condition</label>
                                            <textarea id="ticket-content" name="detail" placeholder="Type here.." class="form-control" rows="3"><?=(isset($input) && isset($input['detail']) ? $input['detail'] : '')?></textarea>
                                        </div>
                                        <div class="form-group" id="addFile">
                                            <label>Upload Image (JPEG)</label>
                                            <input name="image_upload[]" id="fileUpload" type="file" class="form-control" accept="image/jpeg" disabled="" required>
                                        </div>
                                        <button type="submit" name="submit" value="kirim" class="btn btn-md btn-primary">Submit Ticket</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    