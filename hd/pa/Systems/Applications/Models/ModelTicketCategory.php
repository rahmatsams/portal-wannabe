<?php
/*
* Model for Ticketdesk
*/
class TicketCategory extends Model
{
	public $lastInsertID;
    function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM pa_category WHERE category_parent_id=1';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
    function getCategoryId($input){
        $query_string = 'SELECT category_id, category_name, description, work_time FROM pa_category WHERE category_parent_id=1 AND category_id=:cid';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }
    
     function getSubCategoryId($input){
        $query_string = 'SELECT pa_category.category_id, pa_category.need_photo,pa_category.category_name, pa_category.description, pa_category.work_time,b.category_name AS parent_name, b.category_id AS parent_id FROM pa_category LEFT JOIN pa_category AS b ON b.category_id=pa_category.category_parent_id WHERE pa_category.category_id=:cid ORDER BY category_name ASC';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }
    
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM pa_category WHERE category_parent_id=:cat_id ORDER BY category_name ASC';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getParentCategory($id)
	{
		$query_string = 'SELECT * FROM pa_category WHERE category_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function createCategory($form){
		try{
			$result = $this->insertQuery('pa_category', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
		} catch(Exception $e){
			if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
		}
	}
    function editCategory($form, $where){
			$result = $this->editQuery('pa_category', $form, $where);
            
            return $result;
    }
}