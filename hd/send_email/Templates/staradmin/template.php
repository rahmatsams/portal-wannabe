<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?=getConfig('site_name')?> - <?=$page?></title>
		<link rel="stylesheet" href="../node_modules/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="./Resources/assets/bootstrap-4.0.0/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" />
        <link rel="stylesheet" href="../node_modules/flag-icon-css/css/flag-icon.min.css" />
        <link rel="stylesheet" href="<?=$_template_css?>/style.css" />
        <link rel="shortcut icon" href="<?=$_template_image?>/favicon.png" />
    </head>

    <body>
        <div class=" container-scroller">
        <!-- partial:partials/_navbar.html -->
            <nav class="navbar navbar-default col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="bg-white text-center navbar-brand-wrapper">
                    <a class="navbar-brand brand-logo" href="index.html">PRP</a>
                    <a class="navbar-brand brand-logo-mini" href="index.html">PRP</a>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-center">
                    <button class="navbar-toggler navbar-toggler d-none d-lg-block navbar-dark align-self-center mr-3" type="button" data-toggle="minimize">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <form class="form-inline mt-2 mt-md-0 d-none d-lg-block">
                        <input class="form-control mr-sm-2 search" type="text" placeholder="Search">
                    </form>
                    <ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
                        <li class="nav-item">
                            <a class="nav-link profile-pic" href="#"><img class="rounded-circle" src="Resources/images/profile/unknown.png" alt=""></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fa fa-th"></i></a>
                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-dark navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </nav>

            <!-- partial -->
            <div class="container-fluid">
                <div class="row row-offcanvas row-offcanvas-right">
                <!-- partial:partials/_sidebar.html -->
                    <nav class="bg-white sidebar sidebar-offcanvas" id="sidebar">
                        <div class="user-info">
                            <img src="Resources/Images/Profile/unknown.png" alt="">
                            <p class="name"><?=$session['username']?></p>
                            <!--<p class="designation">Manager</p>-->
                            <span class="online"></span>
                        </div>
                        <ul class="nav">
                            <li class="nav-item <?=($site['root'] == 'home' ? 'active' : '')?>">
                                <a class="nav-link" href="index.html">
                                    <img src="<?=$_template_path?>/images/icons/1.png" alt="">
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </li>
                        <?php
                            if($session['group'] == 'Super Admin' || $session['group'] == 'Administrator' || $session['group'] == 'Admin QA')
                            {
                        ?>
                            <li class="nav-item <?=($site['root'] == 'admin' ? 'active' : '')?>">
                                <a class="nav-link" href="administrator.html">
                                    <img src="<?=$_template_path?>/images/icons/9.png" alt="">
                                    <span class="menu-title">Material Procurement</span>
                                </a>
                            </li>
    
                            <li class="nav-item <?=($site['root'] == 'prp' ? 'active' : '')?>">
                                <a class="nav-link" href="submit_mrp.html">
                                    <img src="<?=$_template_path?>/images/icons/005-forms.png" alt="">
                                    <span class="menu-title">Submit MRP</span>
                                </a>
                            </li>
                        <?php
                            }
                        ?>        
                            <li class="nav-item <?=($site['root'] == 'report' ? 'active' : '')?>">
                                <a class="nav-link" href="pages/charts/index.html">
                                    <img src="<?=$_template_path?>/images/icons/6.png" alt="">
                                    <span class="menu-title">Reports</span>
                                </a>
                            </li>
            
                            <li class="nav-item">
                                <a class="nav-link" href="logout.html">
                                    <img src="<?=$_template_path?>/images/icons/020-locked.png" alt="">
                                    <span class="menu-title">Logout</span>
                                </a>
                            </li>
                        </ul>
                    </nav>

                    <!-- partial -->
                    <div class="content-wrapper">
                    <?php
                        $this->view($_action,$_passed_var);
                    ?>
                    </div>
                    <!-- partial:partials/_footer.html -->
                    <footer class="footer">
                        <div class="container-fluid clearfix">
                            <span class="float-right">
                                <a href="#">Sushi Tei</a> &copy; 2018
                            </span>
                        </div>
                    </footer>

                <!-- partial -->
                </div>
            </div>
        </div>

        <script src="../node_modules/jquery/dist/jquery.min.js"></script>
        <script src="./Resources/assets/bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>
  
        <!--
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5NXz9eVnyJOA81wimI8WYE08kW_JMe8g&callback=initMap" async defer></script>
        <script src="<?=$_template_path?>/node_modules/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?=$_template_path?>/node_modules/chart.js/dist/Chart.min.js"></script>
        <script src="<?=$_template_path?>/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="<?=$_template_path?>/js/off-canvas.js"></script>
        <script src="<?=$_template_path?>/js/hoverable-collapse.js"></script>
        <script src="<?=$_template_path?>/js/misc.js"></script>
        <script src="<?=$_template_path?>/js/chart.js"></script>
        <script src="<?=$_template_path?>/js/maps.js"></script>
        -->
  
        <?php
        if(isset($option['injs']) && count($option['injs']) > 0){
            foreach($option['injs'] as $js){
                echo "
        <script src='{$_template_path}/{$js}'></script>";
            }
        }
        if(isset($option['exjs']) && count($option['exjs']) > 0){
            foreach($option['exjs'] as $js){
                echo "
        <script src='{$js}'></script>";
            }
        }
        ?>
        
    </body>

</html>
