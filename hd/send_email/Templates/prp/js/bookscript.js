$(document).ready(function(){
	$("#timeFrom").datepicker({
        dateFormat: "yy-mm-dd",
        maxViewMode: 2,
        todayHighlight: true,
        minDate: 0
    });
	$("#timeTo").datepicker({
        dateFormat: "yy-mm-dd",
        maxViewMode: 2,
        todayHighlight: true,
        minDate: 0
    });
});
