<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=(!empty($page) ? "{$page} - " : '')?><?=getConfig('site_name')?></title>
    <meta name="description" content="Email Blaster, Sushi Tei">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="<?=getConfig('base_domain')?>apple-icon.png">
    <link rel="shortcut icon" href="<?=getConfig('base_domain')?>favicon.ico">
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/css/normalize.css">
    <link rel="stylesheet" href="<?=getConfig('base_domain')?>assets/modules/bootstrap-4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=getConfig('base_domain')?>assets/modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/scss/style.css">
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/css/lib/vector-map/jqvmap.min.css">

    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' type='text/css'>
    <?php
            if(isset($option['stylesheet']) && count($option['stylesheet']) > 0){
                foreach($option['stylesheet'] as $css){
                    echo "
        <link rel='stylesheet' type='text/css' href='{$css}'>";
                }
            }
    ?>
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <?php
        if(isset($option['jstop']) && count($option['jstop']) > 0){
            foreach($option['jstop'] as $js){
                echo "
        <script src='{$js}'></script>";
            }
        }
    ?>
    
</head>
<body>


        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
            <nav class="navbar navbar-expand-sm navbar-default">

                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand brand-logo" href="index.html">Email Blaster</a>
                    <a class="navbar-brand hidden" href="./">&nbsp;</a>
                </div>

                <div id="main-menu" class="main-menu collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <h3 class="menu-title">JOB</h3>
                        <li><a href="index.html"><i class="menu-icon fa fa-list-ul"></i> Job Center</a></li>
                        <li><a href="new_job.html"><i class="menu-icon fa fa-edit"></i> Create Job</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

        <div id="right-panel" class="right-panel">

            <!-- Header-->
            <header id="header" class="header" style="background:272c33; background-color:rgb(39, 44, 51);">

                <div class="header-menu">

                    <div class="col-sm-7">
                        <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    </div>
                    <div class="col-sm-5">
                        <div class="user-area dropdown float-right">
                           <a class="dropdown-toggle text-white" href="<?=getConfig('base_domain')?>user/logout" aria-haspopup="true" aria-expanded="true"><i class="menu-icon fa fa-sign-out"></i>Logout
                           </a>
                        </div>

                        <div class="user-area dropdown float-right">
                            <a href="#" class="dropdown-toggle text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-user-o"></i>
                                <?=$session['user_name']?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </a>
                        </div>

                        

                    </div>
                    
                </div>

            </header><!-- /header -->
            <!-- Header-->

            <div class="content mt-3">

        <?php
            $this->view($_action, $_passed_var);
            $this->render();
        ?>

            </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->
    <script src="./Templates/sufee-admin-dashboard-master/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="<?=getConfig('base_domain')?>assets/modules/bootstrap-4.3.1/js/bootstrap.min.js"></script>
<?php
    if(isset($option['injs']) && count($option['injs']) > 0){
        foreach($option['injs'] as $js){
            echo "
    <script src='{$_template_path}/{$js}'></script>";
        }
    }
    if(isset($option['exjs']) && count($option['exjs']) > 0){
        foreach($option['exjs'] as $js){
            echo "
    <script src='{$js}'></script>";
        }
    }
?>
</body>
</html>
