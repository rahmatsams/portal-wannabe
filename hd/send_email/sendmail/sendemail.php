<?php
require_once('./PHPMailer.php');
require_once('./SMTP.php');

function sendemail($sender_name, $sender_email, $honorific, $name, $toaddr, $subject, $msg, $nama_attachment)
{
    try {
        $mail = new PHPMailer(true);   
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug  = 0;  // enables SMTP debug information (for testing)
                             // 1 = errors and messages
                             // 2 = messages only
        //$mail->Debugoutput = 'error_log';

        $mail->SMTPAuth   = true;  // enable SMTP authentication
        $mail->SMTPOptions = array(
                'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                )
        );
        
        $mail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $mail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "info@sushitei.co.id"; // SMTP account username
        $mail->Password   = "JKT258^1439j";        // SMTP account password

        $mail->SetFrom($sender_email, $sender_name);

        $mail->AddReplyTo($sender_email, $sender_name);
        
        /*if exclude me = 1 brti tidak bcc*/

        //$mail->AddBCC($sender_email, $sender_name);

        $mail->Subject    = $subject;

        $mail->AltBody    = 'To view the message, please use an HTML compatible email viewer!'; // optional, comment out and test
        
        //$mail->addAttachment(__dir__.'\attachment\IK-QAS-00-02.pdf');
        //$mail->addAttachment('../Resources/attachment/'.$nama_attachment);
        //$mail->addAttachment(__dir__.'\Manual Send Receive Outlook 2007, 2010, 2013, dan 2016.pdf');
        //$mail->addStringAttachment(file_get_contents($url), 'Hasil PRP.xlsx');
        $fileku = "../Resources/attachment/{$nama_attachment}";
        if (file_exists($fileku) && is_file($fileku)) {
            $mail->addAttachment("../Resources/attachment/{$nama_attachment}");
        }

        $mail->isHTML('true');
        
        $vars = array(
            '{$sender_name}' => $sender_name,
            '{$honorific}' => $honorific,
            '{$name}' => $name,
            '{$nama_attachment}' => $nama_attachment
        );
        
        $mail->MsgHTML(strtr($msg, $vars));

        $mail->AddAddress($toaddr, $toaddr);

        $_logfilename = 'log_'.date("d-m-Y").'.txt';
        
        $mail->Send();
        return array('status' => 1);
    } catch (phpmailerException $e) {
        return array('status' => 0, 'message' => $e->errorMessage());
    } catch (Exception $e) {
        return array('status' => 0, 'message' => $e->getMessage());
    }
    /*
	if(!$mail->Send()) 
	{
		return 0;
		//$status = "Error: " . $mail->ErrorInfo;
		if (!file_exists($_logfilename)) {
			$_logfilehandler = fopen($_logfilename, 'w');
		}else{
			$_logfilehandler = fopen($_logfilename, 'a');
		}
		fwrite($_logfilehandler, "File log untuk Email ".$mail->errorMessage()."\n");
		fclose($_logfilehandler);
	} else {
		return 1;
		//$status =  "Success"; mumet
		if (!file_exists($_logfilename)) {
			$_logfilehandler = fopen($_logfilename, 'w');
		}else{
			$_logfilehandler = fopen($_logfilename, 'a');
		}
		fwrite($_logfilehandler, "Email Success \n");
		fclose($_logfilehandler);
	}
    */
}


	set_time_limit(1000);

	$server_name = 'localhost';
	$user_name = 'sushitei_sendoemairu';
	$password = 'F?nejXe!)=$u';
	$db_name = 'sushitei_sendoemairu';
	$is_db_connected = 0;

	try {
		$conn_pdo = new PDO("mysql:host=$server_name;dbname=$db_name", $user_name, $password);
		// set the PDO error mode to exception
		$conn_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo "Connected successfully"."<br />\n";
		$is_db_connected = 1;
	}catch(PDOException $e){
		echo "Connection failed: " . $e->getMessage();
	}

    /*2=sending, 5=test ok*/
	if($is_db_connected == 1){
		$send_qty = 100;

		$job_result = $conn_pdo->query('
			SELECT * FROM view_job 
			WHERE time_active<=NOW()
					AND (status=2 OR status=5)
					AND (user_cancel <= 0 OR user_cancel=Null)
			ORDER BY
				time_active,
				job_id
			LIMIT 1
		');
		while ($job_row = $job_result->fetch()) {
			echo "{$job_row['subject']}<br />\n";
			$query = "SELECT * 
				FROM job_email_list 
				WHERE job_id='{$job_row['job_id']}'
				AND status=1
				ORDER BY job_id
				LIMIT {$send_qty}
			";
			$job_email_result = $conn_pdo->query($query);

			$email_send=0;
			while ($job_email_row = $job_email_result->fetch()) 
			{
				$email_send++;

				echo "Email ".$job_email_row['email']." : ";
				$s = sendemail($job_row['display_name'], $job_row['email_sender'], $job_email_row['honorific'], $job_email_row['name'], $job_email_row['email'], $job_row['subject'], $job_row['content'], $job_row['attachment']);
				
				
				if($s['status'] == 1){
                    $status = 2;
                    $desc = 'Success';
				}else{
					$status = 3;
                    $desc = $s['message'];
				}
					
				$sql_update_list = "
                    UPDATE job_email_list SET 
						status='{$status}',
						send_time=NOW(),
                        status_desc='{$desc}'
                    WHERE id='{$job_email_row['id']}'";
				$result_update_list = $conn_pdo->prepare($sql_update_list);
				$result_update_list->execute();
				echo $result_update_list->rowCount() . " records UPDATED<br />\n";
			}
			
			if($email_send==0){ //jika sudah selesai, tidak ada email lagi yg dkirim makan job done
			
				$sql_update_list = 
						'UPDATE job SET '.
						'status=3 '.
						'WHERE job_id='.$job_row['job_id'];
				$result_update_list = $conn_pdo->prepare($sql_update_list);
				$result_update_list->execute();
				echo "Job '".$job_row['subject']."' : job done <br />\n";
			}else{
				
				$sql_update_list = 
						'UPDATE job SET '.
						'status=2 '.
						'WHERE job_id='.$job_row['job_id'];
				$result_update_list = $conn_pdo->prepare($sql_update_list);
				$result_update_list->execute();
				echo "Job '".$job_row['subject']."' : sending <br />\n";
			}
		}
	}
?>