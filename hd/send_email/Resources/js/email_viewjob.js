$(document).ready(function(){
    tinymce.init({
        selector: '#contentEmailview',
        width: 700,
        menubar: false,
        plugins: [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste image imagetools"
        ],
        toolbar: false,
        paste_data_images: true,
        file_picker_types: 'image',
  /* and here's our custom image picker*/
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function () {
              var file = this.files[0];

              var reader = new FileReader();
              reader.onload = function () {
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);

                cb(blobInfo.blobUri(), { title: file.name });
              };
              reader.readAsDataURL(file);
            };

            input.click();
        }
    });
    tinymce.activeEditor.setMode('readonly');
   
    $('#modalListJob').on('shown.bs.modal', function () {
        if ( ! $.fn.DataTable.isDataTable( '#dtable' ) ) {
            $('#dtable').DataTable( {
              "dom": '<"top"if>rt<"bottom"p><"clear">',
              "ajax": {
                  "url": './list_email.html',
                  "type": "POST",
                  "data": function ( d ) {
                      d.job_id = $('#modalListJob').data("id");
                  },
                  
              },
              "columns": [
                    {"data": "email"       },
                    {"data": "send_time"   },
                    {"data": "status_desc" },
              ]
          });
        }
    });

    $('#testEmail').click(function(event){
      var me = $(this);
      testEmail(me);
    });

});

function testEmail(me){
  if (me.data('requestRunning')) {
    return;
  }
  me.data('requestRunning', true);
  var dataid = me.data("id");
  $.ajax({
    type: "POST",
    url: "test_email_"+dataid+".html",
    data: 'job_id='+dataid,
    dataType: "json",
    success: function(json){
      $('#modalConfirmTest').modal('hide');
      if(json.success==1) {
        $('#successModal').modal('show');
        location.reload();
      }else if(json.success==0){
        $('#failedModal').modal('show');
      }
    },
    complete: function() {
      me.data('requestRunning', false);
    },
  });
  return false;
}

//close_request_<?=$result['request_id']?>.html