$(document).ready(function(){
      $('#modalListJob').on('shown.bs.modal', function () {
          $('#dtable').DataTable( {
              //"info":     false, //hide Showing..entries
              "dom": '<"top"if>rt<"bottom"p><"clear">',
              "ajax": {
                  "url": './list_email.html',
                  "type": 'POST', 
                  "data": function ( d ) {
                      d.job_id = $('#modalListJob').data("id");
                  }
              },
              "columns": [
                  {"data": "email"       },
                  {"data": "send_time"   },
                  {"data": "status_desc" },
              ]
              
          });
      })
});