<?php
require_once(__dir__ . '\PHPMailer.php');
require_once(__dir__ . '\SMTP.php');

function sendemail($sender_name, $sender_email, $bapakibu, $name, $toaddr, $subject, $msg, &$status)
{
	$mail = new PHPMailer(true);   
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
																						 // 1 = errors and messages
																						 // 2 = messages only
	//$mail->Debugoutput = 'error_log';

	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	$mail->SMTPOptions = array(
			'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
			)
	);
	
	$mail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
	$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
	$mail->Username   = "info@sushitei.co.id"; // SMTP account username
	$mail->Password   = "JKT258^1439j";        // SMTP account password

	$mail->SetFrom($sender_email, $sender_name);

	$mail->AddReplyTo($sender_email, $sender_name);
	$mail->AddBCC($sender_email, $sender_name);

	$mail->Subject    = $subject;

	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	$mail->addAttachment(__dir__.'\attachment\IK-QAS-00-02.pdf');
	//$mail->addAttachment(__dir__.'\Manual Send Receive Outlook 2007, 2010, 2013, dan 2016.pdf');
	//$mail->addStringAttachment(file_get_contents($url), 'Hasil PRP.xlsx');
	$mail->isHTML('true');
	
	$vars = array(
			'{$sender_name}' => $sender_name,
			'{$bapakibu}' => $bapakibu,
			'{$name}' => $name,
			);
	$mail->MsgHTML(strtr($msg, $vars));

	$mail->AddAddress($toaddr, $toaddr);

	if(!$mail->Send()) 
	{
		$status = "Error: " . $mail->ErrorInfo;
	} 
	else 
	{
		$status =  "Success";
	}
}
set_time_limit(1000);

$server_name = "localhost";
$user_name = "root";
$password = "";
$db_name = "send_email";
$is_db_connected = 0;

try 
{
	$conn_pdo = new PDO("mysql:host=$server_name;dbname=$db_name", $user_name, $password);
	// set the PDO error mode to exception
	$conn_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "Connected successfully"."<br />\n";
	$is_db_connected = 1;
}
catch(PDOException $e)
{
	echo "Connection failed: " . $e->getMessage();
}

if($is_db_connected == 1)
{
	$send_qty = 100;

	$job_result = $conn_pdo->query(
			'SELECT * '.
			'FROM job '.
			'WHERE time_active<=NOW() '.
			'AND (status=1 OR status=2)'.
			'AND (user_cancel <= 0 OR user_cancel=Null) '.
			'ORDER BY '.
			'	time_active, '.
			'	id '.
			'LIMIT 1 '
			);
	while ($job_row = $job_result->fetch()) 
	{
		echo $job_row['subject']."<br />\n";
		$job_email_result = $conn_pdo->query(
				'SELECT * '.
				'FROM job_email_list '.
				'WHERE job_id='. $job_row['id'].' '.
				'AND status=1 '.
				'ORDER BY id '.
				'LIMIT '.$send_qty.' '
				);	

		$email_send=0;
		while ($job_email_row = $job_email_result->fetch()) 
		{
			$email_send++;

			echo "Email ".$job_email_row['email']." : ";
			sendemail('Kris', 'krishandaya@sushitei.co.id', $job_email_row['honorific'], $job_email_row['name'], $job_email_row['email'], $job_row['subject'], $job_row['content'], $statusdesc);
			echo $statusdesc." - ";
			
			if($statusdesc == 'Success')
			{
				$status=2;
			}
			else
			{
				$status=3;
			}
				
			$sql_update_list = 
					'UPDATE job_email_list SET '.
					'status='.$status.', '.
					'send_time=NOW(), '.
					'status_desc="'.$statusdesc.'" '.
					'WHERE id='.$job_email_row['id'];
			$result_update_list = $conn_pdo->prepare($sql_update_list);
			$result_update_list->execute();
			echo $result_update_list->rowCount() . " records UPDATED<br />\n";
		}
		
		if($email_send==0)
		{
			$sql_update_list = 
					'UPDATE job SET '.
					'status=3 '.
					'WHERE id='.$job_row['id'];
			$result_update_list = $conn_pdo->prepare($sql_update_list);
			$result_update_list->execute();
			echo "Job '".$job_row['subject']."' : job done <br />\n";
		}
		else
		{
			$sql_update_list = 
					'UPDATE job SET '.
					'status=2 '.
					'WHERE id='.$job_row['id'];
			$result_update_list = $conn_pdo->prepare($sql_update_list);
			$result_update_list->execute();
			echo "Job '".$job_row['subject']."' : sending <br />\n";
		}
		
	}
}
?>