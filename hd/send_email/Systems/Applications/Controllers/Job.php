<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

/**
 * 
 */
class Job extends Controller
{
    
    private $message;

    function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewNewJob', 'viewDetail', 'viewDeleteJob', 'viewEditJob', 'viewCancelJob', 'viewTestEmailJob', 'viewEmailListJob'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewNewJob', 'viewDetail', 'viewDeleteJob', 'viewEditJob', 'viewCancelJob', 'viewTestEmailJob'),
                'groups'=>array('Guest'),
            ),
        );
    }

    public function site()
    {
        $site = array(
            'root' => 'send_email'
        );
        return $site;
    }

    //NEW JOB
    public function viewNewJob()
    {
        $m_job = $this->load->model('Job');
        $session = $this->getSession();
        $data = array(
            'site'         => $this->site(),
            'session'      => $session,
            'admin'        => $this->isAdmin(),
            'page'         => 'New Job',
        );
        if (isset($_POST['new_job']) && $_POST['new_job'] == 'Save Job') {
            $input = $this->load->lib('Input');
            $input->addValidation('job_subject', $_POST['job_subject'], 'min=1', 'Must be filled');
            $input->addValidation('job_content', $_POST['job_content'], 'min=1', 'Must be filled');
            $input->addValidation('time_active', $_POST['time_active'], 'min=1', 'Must be filled');
            
            if ($input->validate()) {
                $upload = $this->doUpload($this->reArrayFiles($_FILES['attachment_job']), 'Resources/attachment');
                $upload2 = $this->doUpload($this->reArrayFiles($_FILES['attachment_receiver']), 'Resources/attachment_receiver');                
                
                if (empty($upload) == 1) {
                    $jobs = array(
                        'subject'          => $_POST['job_subject'],
                        'content'          => $_POST['job_content'],
                        'submit_time'      => date('Y-m-d H:i:s'),
                        'time_active'      => date_format(date_create($_POST['time_active']),"Y-m-d H:i:s"),
                        'user'             => $session['user_id'],
                        'status'           => 1, //OPEN
                        'email_sender'     => $_POST['email_sender'],
                        'attachment'       => "",
                        'last_update'      => date('Y-m-d H:i:s'),
                    );
                }else
                {
                    $jobs = array(
                        'subject'          => $_POST['job_subject'],
                        'content'          => $_POST['job_content'],
                        'submit_time'      => date('Y-m-d H:i:s'),
                        'time_active'      => date_format(date_create($_POST['time_active']),"Y-m-d H:i:s"),
                        'user'             => $session['user_id'],
                        'status'           => 1, //OPEN
                        'email_sender'     => $_POST['email_sender'],
                        'attachment'       => $upload[0]['upload_name'],
                        'last_update'      => date('Y-m-d H:i:s'),
                    );
                }
                
                if (count($upload2) > 0 && $m_job->newJob($jobs)) {
                    $csv = array();
                    $file_csv = fopen('./Resources/attachment_receiver/'.$upload2[0]['upload_name'],"r");
                    $data_job_last = $m_job->getJobLastInput();
                    while(!feof($file_csv))
                    {
                        $a = fgetcsv($file_csv);
                        if(!empty($a[1])){
                            $job_email_lists = array(
                                'job_id' => $data_job_last['job_id'],
                                'name' => $a[1],
                                'honorific' => $a[0],
                                'email' => $a[2],
                                'send_time' => date('Y-m-d H:i:s'),
                                'status' => 1,
                                'status_desc' => 'New'
                            );

                            $m_job->newJobEmailList($job_email_lists);
                        }
                    }
                    fclose($file_csv);
                    echo "<script>alert('Successfully Submited'); window.location.replace('index.html');</script>";
                }else{
                    echo "<script>alert('CSV Not Found'); window.location.replace('index.html');</script>";
                }
            }else{
                $data['error'] = $input->_error;
                echo "<script>alert('Error'); window.location.replace('new_job.html');</script>";
            }

        }else{
            $data['option']['exjs'][] = getConfig('base_domain').'assets/modules/tinymce/tinymce512/tinymce.min.js';
            $data['option']['exjs'][] = getConfig('base_url').'Resources/js/email_newjob.js';
            $this->load->template('job/new_job', $data);
        }
    }

    //VIEW JOB
    public function viewDetail()
    {
        $input = $this->load->lib('Input');
        $m_job = $this->load->model('Job');
        $session = $this->getSession();
        $data = array(
                 'site'     => $this->site(),
                 'session'  => $this->session,
                 'admin'    => $this->isAdmin(),
                 'page'         => 'Job Detail',
                 'option' => array(
                    'exjs' => array(
                        './Resources/js/delete_job.js',
                        './Resources/js/test_email.js'
                    )
            )
        );
        $opt = array(
                'order_by'  => 'job_id',
                'order'     => 'DESC' 
        );

        if (isset($_GET['id'])) {
            $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
            $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');

        if ($input->validate()) {
                $data['result'] = $m_job->getJobById(array('job_id'=>$_GET['id']), "job_id, subject, email_sender, content AS job_content, attachment AS attachment_job, time_active, status, submit_time, display_name, last_update, cancel_desc, time_cancel, user_cancel");
                $data['option']['stylesheet'][] = getConfig('base_domain').'assets/modules/DataTables/datatables.min.css';
                #$data['option']['stylesheet'][] = getConfig('base_domain').'assets/modules/DataTables/dataTables.bootstrap4.min.css';
                $data['option']['exjs'][] = getConfig('base_domain').'assets/modules/tinymce/tinymce512/tinymce.min.js';
                $data['option']['exjs'][] = getConfig('base_domain').'assets/modules/DataTables/datatables.min.js';
                #$data['option']['exjs'][] = getConfig('base_domain').'assets/modules/DataTables/dataTables.bootstrap4.min.js';
                $data['option']['exjs'][] = getConfig('base_url').'Resources/js/email_viewjob.js';
                #$data['option']['exjs'][] = getConfig('base_url').'Resources/js/email_joblist.js';
                $this->load->template('job/view_job', $data);
            } else {
                $this->showError(2);
            }
        } else {
                $this->showError(2);
        }
    }/*END OF VIEW JOB*/


    //EDIT JOB
    public function viewEditJob()
    {
        $session = $this->getSession();
        $input = $this->load->lib('Input');
        $m_job = $this->load->model('Job');
        $data = array(
            'site'          => $this->site(),
            'session'       => $this->session,
            'admin'         => $this->isAdmin(),
            'page'          => 'Edit Job'
        );
        if (isset($_POST['edit_job']) && $_POST['edit_job'] == 'Edit') {
            $input->addValidation('job_subject', $_POST['job_subject'], 'min=1', 'Must be filled');
            $input->addValidation('job_content', $_POST['job_content'], 'min=1', 'Must be filled');
            $input->addValidation('time_active', $_POST['time_active'], 'min=1', 'Must be filled');

            if ($input->validate()) {
                $upload = $this->doUpload($this->reArrayFiles($_FILES['attachment_job']), 'Resources/attachment');
                $upload2 = $this->doUpload($this->reArrayFiles($_FILES['attachment_receiver']), 'Resources/attachment_receiver');
                if (count($upload) > 0) {
                    $data_job_byid = $m_job->getJobById(array('job_id'=>$_POST['job_id']), "job_id, subject, email_sender, content, attachment AS attachment_job, time_active, status, submit_time, display_name, last_update");
                    if (empty($data_job_byid['attachment_job'])) {
                        $jobs = array(
                            'subject'          => $_POST['job_subject'],
                            'content'          => $_POST['job_content'],
                            'last_update'      => date('Y-m-d H:i:s'),
                            'time_active'      => date_format(date_create($_POST['time_active']),"Y-m-d H:i:s"),
                            'user'             => $session['user_id'],
                            'status'           => 1, //OPEN
                            'email_sender'     => $_POST['email_sender'],
                            'attachment'       => $upload[0]['upload_name'],
                        );
                        $where_job = array(
                            'job_id' => $_POST['job_id']
                        );
                        if ($m_job->editJob($jobs, $where_job)) {
                            if (count($upload2) > 0) {
                                $csv = array();
                                $file_csv = fopen('./Resources/attachment_receiver/'.$upload2[0]['upload_name'],"r");
                                $m_job->deleteJobEmailListByJobID($_POST['job_id']);
                                while(!feof($file_csv))
                                {
                                    $a = fgetcsv($file_csv);
                                    if(!empty($a[1])){
                                        $job_email_lists = array(
                                            'job_id' => $_POST['job_id'],
                                            'name' => $a[1],
                                            'honorific' => $a[0],
                                            'email' => $a[2],
                                            'send_time' => date('Y-m-d H:i:s'),
                                            'status' => 1,
                                            'status_desc' => 'New' //edit hanya bisa jika status 1
                                        );

                                        $m_job->newJobEmailList($job_email_lists);
                                    }
                                }
                                fclose($file_csv);
                                
                                echo "<script>alert('Successfully Edited'); window.location.replace('edit_job_".$_POST['job_id'].".html');</script>";
                            }else{
                                echo "<script>alert('Successfully Edited'); window.location.replace('edit_job_".$_POST['job_id'].".html');</script>";
                            }
                        }else{
                            echo "Failed";
                        }
                    }else{
                        if ($data_job_byid['attachment_job'] != "") {
                            $rc = $m_job->checkImageJob(array('job_id' => $_POST['job_id']));
                            foreach ($rc as $att) {
                                $file = "Resources/attachment/".$att['attachment'];
                                if (file_exists($file) && is_file($file)) 
                                {
                                    unlink($file);
                                }
                            }

                        }
                        $jobs = array(
                            'subject'          => $_POST['job_subject'],
                            'content'          => $_POST['job_content'],
                            'last_update'      => date('Y-m-d H:i:s'),
                            'time_active'      => date_format(date_create($_POST['time_active']),"Y-m-d H:i:s"),
                            'user'             => $session['user_id'],
                            'status'           => 1, //OPEN
                            'email_sender'     => $_POST['email_sender'],
                            'attachment'       => $upload[0]['upload_name'],
                        );
                        $where_job = array(
                            'job_id' => $_POST['job_id']
                        );
                        if ($m_job->editJob($jobs, $where_job)) {
                            if (count($upload2) > 0) {
                                $csv = array();
                                $file_csv = fopen('./Resources/attachment_receiver/'.$upload2[0]['upload_name'],"r");
                                $m_job->deleteJobEmailListByJobID($_POST['job_id']);
                                while(!feof($file_csv))
                                {
                                    $a = fgetcsv($file_csv);
                                    if(!empty($a[1])){
                                        $job_email_lists = array(
                                            'job_id' => $_POST['job_id'],
                                            'name' => $a[1],
                                            'honorific' => $a[0],
                                            'email' => $a[2],
                                            'send_time' => date('Y-m-d H:i:s'),
                                            'status' => 1,
                                            'status_desc' => 'New'
                                        );

                                        $m_job->newJobEmailList($job_email_lists);
                                    }
                                }
                                fclose($file_csv);
                                
                                echo "<script>alert('Successfully Edited'); window.location.replace('edit_job_".$_POST['job_id'].".html');</script>";
                            }else{
                                echo "<script>alert('Successfully Edited'); window.location.replace('edit_job_".$_POST['job_id'].".html');</script>";
                            }
                        }else{
                            echo "Failed";
                        }
                    }
                    
                } else
                {
                    //echo "Masuk Sini";exit();
                    $jobs = array(
                        'subject'          => $_POST['job_subject'],
                        'content'          => $_POST['job_content'],
                        'last_update'      => date('Y-m-d H:i:s'),
                        'time_active'      => date_format(date_create($_POST['time_active']),"Y-m-d H:i:s"),
                        'user'             => $session['user_id'],
                        'status'           => 1, //OPEN
                        'email_sender'     => $_POST['email_sender']
                    );
                     $where_job = array(
                            'job_id' => $_POST['job_id']
                        );
                        if ($m_job->editJob($jobs, $where_job)) {
                            if (count($upload2) > 0) {
                                $csv = array();
                                $file_csv = fopen('./Resources/attachment_receiver/'.$upload2[0]['upload_name'],"r");
                                $m_job->deleteJobEmailListByJobID($_POST['job_id']);
                                while(!feof($file_csv))
                                {
                                    $a = fgetcsv($file_csv);
                                    if(!empty($a[1])){
                                        $job_email_lists = array(
                                            'job_id' => $_POST['job_id'],
                                            'name' => $a[1],
                                            'honorific' => $a[0],
                                            'email' => $a[2],
                                            'send_time' => date('Y-m-d H:i:s'),
                                            'status' => 1,
                                            'status_desc' => 'New'
                                        );

                                        $m_job->newJobEmailList($job_email_lists);
                                    }
                                }
                                fclose($file_csv);
                                
                                echo "<script>alert('Successfully Edited'); window.location.replace('edit_job_".$_POST['job_id'].".html');</script>";
                            }else{
                                echo "<script>alert('Successfully Edited'); window.location.replace('edit_job_".$_POST['job_id'].".html');</script>";
                            }
                        }else{
                            echo "Failed";
                        }
                }
            } else {
                $data['error'] = $input->_error;
                $this->load->template('admin/ticket/edit_job', $data);
            }
            
        } else {
            if (isset($_GET['id'])) {
                    $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                    $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                    if ($input->validate()) {
                        $data['result'] = $m_job->getJobById(array('job_id'=>$_GET['id']), "job_id, subject, email_sender, content, attachment AS attachment_job, time_active, status, submit_time, display_name");
                        $data['option']['exjs'][] = getConfig('base_domain').'assets/modules/tinymce/tinymce512/tinymce.min.js';
                        $data['option']['exjs'][] = getConfig('base_url').'Resources/js/email_editjob.js';
                        $this->load->template('job/edit_job', $data);
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $this->showError(2);
                }
            }
    } /*THE END OF EDITJOB*/

    //DELETE JOB
    public function viewDeleteJob()
    {
        $id = $_GET['idJob'];
        $m_job = $this->load->model('Job');
        $m_job_list = $this->load->model('JobList');
        if (is_array($m_job->getJobById(array('job_id' => $_GET['idJob'] )))) 
        {
            $rc = $m_job->checkImageJob(array('job_id' => $_GET['idJob']));
            foreach ($rc as $img) {
                $file = "Resources/attachment/".$img['attachment'];
                if (file_exists($file) && is_file($file)) 
                {
                    unlink($file);
                }
            }
            $m_job->deleteJob($id);
            $m_job_list->deleteJobEmailListByJobID($id);
            
            header("Location: index.html");
        }else{
            exit('No Data');
        }
    }

    //CANCEL JOB
    public function viewCancelJob()
    {
        $session = $this->getSession();
        $input = $this->load->lib('Input');
        $m_job = $this->load->model('Job');
        $data = array(
            'site'          => $this->Site(),
            'session'       => $session,
            'admin'         => $this->isAdmin(),
            'page'          => 'Cancel Job'
        );
            
        if (isset($_POST['action'])) {
            $data = array(
                'success' => 0 
            );
            $parent = 1;
            if ($_POST['action'] == 'Cancel') {
                $input->addValidation('parent_max', $_POST['job_id'], 'max=6', 'Please check your input again');
                if ($input->validate()) {
                    unset($_POST['action']);
                    $cancel_job = array(
                                    'user_cancel'   => $session['user_id'],
                                    'time_cancel'   => date('Y-m-d H:i:s'),
                                    'cancel_desc'   => $_POST['cancel_desc'], 
                                    'status'        => 4, #Cancel
                    );
                    $where_job = array(
                        'job_id' => $_POST['job_id']
                    );
                    if ($m_job->cancelJob($cancel_job, $where_job)) {
                        $data['success'] = 1;

                        echo "<script>alert('Job has Canceled'); window.location.replace('index.html');</script>";
                    }else {
                        $data['error'] = 'Unknown Error';
                    }
            }else{
                $data['error'] = $input->_error;
                }

            }
        } else{
            if (isset($_GET['id'])) {
                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                if ($input->validate()) {
                    $data['result'] = $m_job->getJobById(array('job_id'=>$_GET['id']));
                    $data['option']['exjs'][] = getConfig('base_domain').'assets/modules/tinymce/tinymce512/tinymce.min.js';
                    $data['option']['exjs'][] = getConfig('base_url').'Resources/js/email_editjob.js';
                    $this->load->template('job/cancel_job', $data);
                } else {
                    $this->showError(2);
                }
            } else {
                $this->showError(2);
            }
        }
    }/*end of cancel job*/

    /*TEST EMAIL*/
    public function viewTestEmailJob()
    {
        $id = $_GET['id'];
        $m_job = $this->load->model('Job');

        $test_email = array( //test email job
            'title'  => "Test Email Blaster ID #".$id." ",
            'view'   => 'email/test',
            'data'   =>  array(
                'detail' => $m_job->getJobById(array('job_id'=>$id), "job_id, subject, email_sender, content, attachment AS attachment_job, time_active, status, submit_time, display_name"),
                'header'  => ''
            ),
            'recipient' => array(
                'address' => $this->session['email'],
                'name' => $this->session['user_name'],
            ),
        );
        /*$return = array(
            'success' => 1,
            'email'   => 0, 
        );*/
        if ($this->send_email($test_email)) {
            $m_job->modifyJobTestEmail($id);

            $data['success'] = 1;
            $data['email'] = 1;

        }else{

            $data['success'] = 0;
            $data['message'] = $this->message;

        }

        echo json_encode($data);

    } /*End of test email*/
    

    public function viewEmailListJob()
    {
       $return['success'] = 0;
       if(!empty($_POST['job_id'])){
            $input = $this->load->lib('Input');
            $input->addValidation('id_length', $_POST['job_id'], 'max=6', 'Excedding allowed range');
            $input->addValidation('id_format', $_POST['job_id'], 'numeric', 'Excedding allowed range');
            if($input->validate()){
                $mjl = $this->load->model('JobList');
                $q = array('job_id' => $_POST['job_id']);
                $return['data'] = $mjl->jobListFromJob($q);
                if(count($return['data']) > 0){
                    $return['success'] = 1;
                }
                
            }else{
                $return['error'] = $input->_error;
            }
       }
       header('Content-Type: application/json');
       echo json_encode($return);
    } 


    /* MAILING */
    private function send_email($option)
    { 
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail   = new PHPMailer(true);
        //$stmail = $this->load->lib('STMail');
        try
        {
            $stmail->IsSMTP(); // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                    // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "info@sushitei.co.id"; // SMTP account username email dkirim dari alamat email ini
            $stmail->Password   = "JKT258^1439j";        // SMTP account password

            $stmail->SetFrom($option['recipient']['address'], $option['recipient']['name']);

            $stmail->AddReplyTo($option['recipient']['address'], $option['recipient']['name']);
            
            //$stmail->addAttachment('Resources/attachment/'.$option['data']['detail']['attachment_job']);

            $stmail->isHTML('true');

            $stmail->Subject = "Test Blast: {$option['title']}";
            
            $returned = $this->load->view($option['view'], $option['data']);

            $stmail->MsgHTML($returned);
            
            $stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
            
            return ($stmail->Send() ? 1 : 0);
            
        } catch (phpmailerException $e) {
            $this->message = $e->errorMessage();
            return 0;
            
        } catch (Exception $e) {
            $this->message = $e->getMessage();
            return 0;
            
        }
        
    }

    protected function doUpload($file, $location='Resources/attachment')
    {
        if(count($file) > 0){
            $uploaded = array();
            $num=0;
            $upload = $this->load->lib('Upload', $file);
            for($uid=0;$uid < count($file); $uid++){
                $file[$uid] = new Upload($file[$uid]);
                if($file[$uid]->uploaded) {
                    $file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num; #strtotime(date("Y-m-d H:i:s")).$num
                    $file[$uid]->image_resize = true;
                    $file[$uid]->image_x = 640;
                    $file[$uid]->image_y = 480;
                    //$file[$uid]->allowed = array('image/jpeg', 'image/png', 'application/pdf'); /*disabled agar semua file allowed*/
                    //$file[$uid]->image_convert = 'jpg';
                    $file[$uid]->process($location);
                    if ($file[$uid]->processed) {
                        $file[$uid]->clean();
                        $uploaded[$num] = array(
                            'upload_name'      => $file[$uid]->file_dst_name,
                            'upload_extension' => $file[$uid]->file_dst_name_ext, //get type file
                            'upload_location'  => $file[$uid]->file_dst_path,
                            'upload_time'      => date("Y-m-d H:i:s"),
                            'upload_temp'      => 0,
                            'upload_user'      => $this->session['user_id'],
                        );
                        $num++;
                    }else{
                        $uploaded[$num]['error'] = $file[$uid]->error;
                    }
                }
            }
            return $uploaded;
        }
    }

    protected function reArrayFiles(&$file_post)
    {
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }


} /*job class end here*/



?>