<?php
class Admin extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Super Admin', 'Administrator', 'Purchasing'),
            ),
            array('Deny', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Guest'),
            )
        );
    }
    
    protected function site()
    {
        $site = array(
            'root' => 'admin'
        );
        return $site;
    }
    
    //ADMINISTRATOR
    function viewIndex()
    {
        $data = array(
            'site'      => $this->Site(),
            'admin'     => $this->isAdmin(),
            'page'      => 'Administrator',
            'session'   => $this->session,
        );
        $this->load->template('admin/index', $data);
    }
    
    
    
}
?>