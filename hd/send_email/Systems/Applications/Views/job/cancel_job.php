<div class="content">
    <div class="breadcrumbs">                    
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-user"></i>  Cancel Job #<?=$result['job_id']?></h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="view_job_<?=$result['job_id']?>.html" class="btn btn-sm btn-danger"><i class="fa fa-repeat"></i> Back to Job Detail</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- BODY -->
    <div class="card col-lg-12 px-0 mb-2 bg-info text-white"> 
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Sender Name
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=$result['display_name']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Email Sender
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=$result['email_sender']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Subject
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=$result['subject']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Time Active (*)
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=date('d-M-Y', strtotime($result['time_active']))?>
                </div>
            </div>
        </div>
    </div>
    <!-- BODY -->
    <div class="card">
        <div class="card-body card-block">
            <form action="cancel_job.html" method="POST" enctype="multipart/form-data" class="form-horizontal">
                <!-- doc type -->
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Description</label>
                    </div>
                    <input type="text" id="text-input" name="job_id" value="<?=$result['job_id']?>" class="form-control" required="" hidden>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <textarea name="cancel_desc" id="textarea-input" rows="3" placeholder="You cannot leave this blank.." class="form-control" required=""></textarea>
                        </div>
                        <small class="form-text text-muted" >*Cancel description</small>
                    </div>
                </div>
                
                <div class="col-md-12 form-group">
                    <input type='submit' class='btn btn-md btn-warning' name="action" value="Cancel">
                </div>
            </form>
        </div>                                    
    </div>    
</div>