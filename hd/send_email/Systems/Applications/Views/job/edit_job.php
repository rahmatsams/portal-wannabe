<style>
    #rcorners2 {
  border-radius: 25px;
  border: 2px solid #73AD21;
  padding: 20px; 
  width: 200px;
  height: 150px;  
}
</style>
<?php 
    $files = $result['attachment_job'];
 ?>
<div class="content">
    <div class="breadcrumbs">                    
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-envelope-open"></i>  Edit Job #<?=$result['job_id']?></h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="view_job_<?=$result['job_id']?>.html" class="btn btn-sm btn-danger"><i class="fa fa-repeat"></i> Back to Job Detail</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- BODY -->
    <div class="card">
        <div class="card-body card-block">
            <form method="POST" action="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" enctype="multipart/form-data" class="form-horizontal">
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Job Subject</label>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="text" id="text-input" name="job_subject" value="<?=$result['subject']?>" class="form-control" required="">
                            <input type="text" id="text-input" name="job_id" value="<?=$result['job_id']?>" class="form-control" required="" hidden>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Email Sender</label>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="email" id="text-input" name="email_sender" value="<?=$result['email_sender']?>" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-3"><label for="file-input" class=" form-control-label">Attachment Job</label></div>
                    
                    <?php if(!empty($files)) :?>
                    <div class="col-12 col-md-2 form-group">                        
                        <a href="./Resources/attachment/<?=$result['attachment_job']?>" class="btn btn-sm btn-link" target='_blank'><i class='fa fa-download'></i> <?=$result['attachment_job']?></button></a>
                    </div>
                    <?php else:?>
                    <div class="col-12 col-md-2 form-group">
                        <p>No file found</p>
                    </div>
                    <?php endif;?>
                    
                    <div class="col-12 col-md-7 form-group">
                        <input type="file" id="file-input" name="attachment_job[0]" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Job Content</label>
                    </div>
                    <div class="col-12 col-md-9 form-group">
                        <textarea id="contentEmail" name="job_content" class="form-control" rows="10" placeholder=""><p><?=$result['content']?></p></textarea>
                    </div>
                </div> 
                <div class="row">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Time Active</label>
                    </div>
                    <div class="col">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="date" id="text-input" name="time_active" value="<?=date_format(date_create($result['time_active']), 'Y-m-d')?>" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-3"><label for="file-input" class=" form-control-label">Attachment Receiver(.csv)</label></div>
                    <div class="col-12 col-md-9 form-group">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="file" id="file-input" name="attachment_receiver[0]" class="form-control">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col col-md-6"><input type="submit" value="Edit" name="edit_job" class="btn btn-block btn-success btn-md">
                    </div>
                    <div class="col col-md-6"><button type="reset" class="btn btn-block btn-danger btn-md">Reset</button>
                    </div>
                </div>
              
            </form>
        </div>                                    
    </div>    
</div>
