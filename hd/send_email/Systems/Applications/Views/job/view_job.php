<style type="text/css">
    .modal-confirm .icon-box {
        width: 80px;
        height: 80px;
        margin: 0 auto;
        border-radius: 50%;
        z-index: 9;
        text-align: center;
        border: 3px solid #f15e5e;
    }
    .modal-confirm .icon-box i {
        color: #f15e5e;
        font-size: 46px;
        display: inline-block;
        margin-top: 13px;
    }

    .modal-confirm .icon-box1 {
        width: 80px;
        height: 80px;
        margin: 0 auto;
        border-radius: 50%;
        z-index: 9;
        text-align: center;
        border: 3px solid #33a5ff;
    }
    .modal-confirm .icon-box1 i {
        color: #33a5ff;
        font-size: 46px;
        display: inline-block;
        margin-top: 13px;
    }
</style>

<?php 
    $date = $result['submit_time'];
    $status = $result['status'];
    $submit_time = date('d-M-Y', strtotime($result['submit_time']));
    $last_update = date('d-M-Y', strtotime($result['last_update']));
    $time_active = date('d-M-Y', strtotime($result['time_active']));
    $cancel_time = date('d-M-Y', strtotime($result['time_cancel']));
?>
    <div class="card border-secondary mb-3">
        <div class="card-body text-dark">
            <div class="col-md-12 col-12 card-title bg-secondary text-white">
                <div class="row p-2">
                    <h4 class="col-md-5 col-5">
                        <i class="fa fa-envelope-open"></i> Job Detail
                    </h4>
                    <div class="col-md-7 col-7">
                        <div class="row">
                            <div class="col-md-1 col-1 p-1"></div>
                            <?php if ($status == 1): ?>
                            <div class="col-md-2 col-2 p-1">
                                <a href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#modalConfirmTest"><i class="fa fa-check"></i> Test</a>
                            </div>
                            <?php endif;?>
                            <?php if ($status == 1 || $status == 5): ?>
                            <div class="col-md-2 col-2 p-1">
                                <a href="edit_job_<?=$result['job_id']?>.html" class="btn btn-block btn-info"><i class="fa fa-edit"></i> Edit</a>
                            </div>
                            <div class="col-md-2 col-2 p-1">
                                <a href="cancel_job_<?=$result['job_id']?>.html" class="btn btn-block btn-warning"><i class="fa fa-times"></i> Cancel</a>
                            </div>
                            <div class="col-md-2 col-2 p-1">
                                <a href="#" class="delete_job btn btn-block btn-danger" data-toggle="modal" data-id="<?=$result['job_id']?>" data-target="#modalconfirmdelete"><i class="fa fa-trash"></i> Delete</a>
                            </div>
                            <?php endif;?>
                            <div class="col-md-3 col-3 p-1">
                                <a href="send_list_<?=$result['job_id']?>.html" class="btn btn-block btn-success" data-toggle="modal" data-target="#modalListJob"><i class="fa fa-edit"></i> Recipient</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body text-dark">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Current Status
                </div>
                <div class="col-sm-6 col-md-6">
                    :   <?php if ($status == 5): ?> <!-- TEST OK -->
                            Test OK
                        <?php elseif ($status == 4): ?> <!-- CANCELED -->
                            Canceled
                        <?php elseif($status == 3) :?> <!-- DONE -->
                            Done
                        <?php elseif($status == 2) :?> <!-- SENDING -->
                            Sending
                        <?php else:?> <!-- NEW -->
                            New
                        <?php endif;?>
                </div>
            </div>
            <div class="row">
                <?php if ($status == 4): ?>
                    <div class="col-sm-6 col-md-3">
                        Cancel Time
                    </div>
                    <div class="col-sm-6 col-md-6">
                        : <?=$cancel_time?>
                    </div>
                <?php else:?>
                    <div class="col-sm-6 col-md-3">
                        Last Update
                    </div>
                    <div class="col-sm-6 col-md-6">
                        : <?=$last_update?>
                    </div>
                <?php endif;?>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Sender Name
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=$result['display_name']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Email Sender
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=$result['email_sender']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Subject
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=$result['subject']?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    Time Active (*)
                </div>
                <div class="col-sm-6 col-md-6">
                    :  <?=$time_active?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    Content
                </div>
                <div class="col-sm-8 col-md-6">
                    <textarea id="contentEmailview" rows="20" disabled><?=$result['job_content']?></textarea>
                </div>
            </div>
            <div class="row">
                <?php if ($status == 4): ?>
                <div class="col-sm-4 col-md-3">
                    Cancel Desc
                </div>
                <div class="col-sm-8 col-md-6">
                    :  <?=$result['cancel_desc']?>
                </div>
                <?php else:?>
                    <?php echo "" ?>
                <?php endif;?>
            </div>
        </div>
    </div>


        <!-- MODAL DELETE JOB -->
        <div id="modalconfirmdelete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="icon-box">
                            <center><i class="fa fa-trash fa-3x"> </i></center>
                        </div>
                        <br>
                        <p>You are about to delete one job, this procedure cannot be undone.
                           Are you sure?
                        </p>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="btn btn-light float-right" data-dismiss="modal">&nbsp; No &nbsp;</button>
                        <a id="admin_delete_job" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger float-right">&nbsp; Yes &nbsp;</button></a> 
                    </div>
                </div>
            </div>
        </div><!-- end of delete job -->

        <!-- MODAL TEST EMAIL -->
        <div id="modalConfirmTest" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="icon-box1">
                            <center><i class="fa fa-envelope fa-3x"> </i></center>
                        </div>
                        <br>
                        <p>You will send one job content to your email.
                           Do you want to proceed?
                        </p>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="btn btn-light float-right" data-dismiss="modal">&nbsp; No &nbsp;</button>
                        <button id="testEmail" data-id="<?=$result['job_id']?>" type="button" class="btn btn-primary float-right">&nbsp; Yes &nbsp;</button>

                        <!-- <button type="button" class="btn btn-light float-right" data-dismiss="modal">&nbsp; No &nbsp;</button>
                        <a id="admin_test_email" href="#"><button id="deleteconfirm" type="button" class="btn btn-primary float-right">&nbsp; Yes &nbsp;</button></a> --> 
                    </div>
                </div>
            </div>
        </div>
        <div id="successModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Success</h4>
                        </div>
                        <div class="modal-body">Email has been sent to you.</div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="failedModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Failed</h4>
                        </div>
                        <div class="modal-body">Send Email Failed.</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL DATATABLE EMAILJOBLIST -->
        <div id="modalListJob" data-id="<?=$result['job_id']?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <table id="dtable" class="table table-stripped">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Send Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- end of test email -->