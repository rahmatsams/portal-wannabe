
    <!-- BODY -->
    <div class="card">
        <div class="card-body card-block">
            <h4 class="card-title bg-primary text-white p-3">
                <i class="fa fa-envelope-open"></i> Create Job
            </h4>
            <form action="new_job.html" method="POST" enctype="multipart/form-data" class="col-md-12 col-12">
                <div class="row mb-2">
                    <label class="col-2 col-md-2 form-control-label">Subject</label>
                    <div class="col-10 col-md-10">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="text" id="text-input" name="job_subject" placeholder="Subject.." class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-2 col-md-2 form-control-label">Sender Address</label>
                    <div class="col-10 col-md-10">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="email" id="text-input" name="email_sender" placeholder="Sender Email Address.." class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="file-attachment" class="col-2 col-md-2 form-control-label">Attachment</label>
                    <div class="col-10 col-md-10">
                        <input type="file" id="file-attachment" name="attachment_job[0]" class="form-control-file">
                    </div>
                </div>
                <div class="row mb-2">
                    <label class="form-control-label col-2 col-md-2">Email Content</label>
                    <div class="col-10 col-md-10">
                        <textarea id="contentEmail" name="job_content" rows="10"></textarea>
                    </div>
                </div> 
                <div class="row mb-2">
                    <label class="col-2 col-md-2 form-control-label">Time Active</label>
                    <div class="col-10 col-md-10">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <input type="hidden" id="text-input" name="request_date" class="form-control">
                            <input type="date" id="text-input" name="time_active" placeholder="" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="file-input" class="col-2 col-md-2 form-control-label">Recipient(.csv)</label>
                    <div class="col-10 col-md-10 form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">*</div>
                            </div>
                            <div class="custom-file">
                                <input type="file" id="file-recipient" name="attachment_receiver[0]" class="form-control-file" required>
                                <label class="custom-file-label" for="file-recipient">Choose file</label>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="row mb-2" hidden="">
                    <label for="file-input" class="col-2 col-md-2 form-control-label">Exclude BCC</label>
                    <div class="col-10 col-md-10 form-group">
                        <div class="input-group">
                            <input type="checkbox" name="exclude_me" value="1">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col col-md-6"><input type="submit" value="Save Job" name="new_job" class="btn btn-block btn-success btn-md">
                    </div>
                    <div class="col col-md-6"><button type="reset" class="btn btn-block btn-danger btn-md">Reset</button>
                    </div>
                </div>
              
            </form>
        </div>                                    
    </div>    
