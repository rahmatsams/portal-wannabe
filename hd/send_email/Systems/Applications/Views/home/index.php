            <div class="content">
                <div class="breadcrumbs">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1><i class="fa fa-briefcase"></i> Job Centers</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                           
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="content">
                <div class="table-responsive">
                  <table id="indexContent" class="table center-aligned-table table-striped">
                    <thead>
                      <tr class="bg-white">
                        <th>No</th>
                        <th>Subject</th>
                        <th>Sender Name</th>
                        <th>Email Sender</th>
                        <th>Time Active</th>
                        <th>Submit Time</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody id="listTable">
                    <?php if (is_array($job_list) && !empty($job_list)): ?>
                        <?php 
                            $i = 1;
                            foreach ($job_list as $jobs): ?>
                            <tr>
                                <td><?=$i?></td>
                                <td><a href="view_job_<?=$jobs['job_id']?>.html" class='btn-link d-block'><?=$jobs['subject']?></a></td>
                                <td><?=$jobs['display_name']?></td>
                                <td><?=$jobs['email_sender']?></td>
                                <td><?=date('d-M-Y', strtotime($jobs['time_active']))?></td>
                                <td><?=date('d-M-Y', strtotime($jobs['submit_time']))?></td>
                                <td>
                                    <?php if ($jobs['status'] == 5): ?>
                                        Test OK
                                    <?php elseif ($jobs['status'] == 4): ?>
                                        Canceled
                                    <?php elseif($jobs['status'] == 3) :?>
                                        Done
                                    <?php elseif($jobs['status'] == 2) :?>
                                        Sending
                                    <?php elseif($jobs['status'] == 4) :?>
                                        Canceled
                                    <?php else:?>
                                        New
                                    <?php endif;?>
                                </td>
                            </tr>
                    <?php 
                        $i++;
                        endforeach; 
                    ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="8">There's no job data</td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                  </table>
                </div>
                
                <!-- <div class="bg-warning text-dark p-3">There is no data found.</div> -->
                            
                
            </div>