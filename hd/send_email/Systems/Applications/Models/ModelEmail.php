<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class ModelEmail extends Model{
        
        function getMailRecipient()
        {
            $query_string = 'SELECT pu.email,pu.display_name FROM sushitei_portal.portal_user pu ';
            $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
            $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
            $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="receiveEmail" AND pp.permission_site='.getConfig('site_number').' GROUP BY pu.email';

            /*$query_string = 'SELECT pu.email,pu.display_name FROM sushitei_portal.portal_user pu ';
            $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
            $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
            $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="receiveEmail" AND pp.permission_site=11 GROUP BY pu.email';*/
            
            $result = $this->fetchAllQuery($query_string);
            return $result;
        }
    }
?>