<?php 
/* MODEL JOB EMAIL LIST */

class ModelJobList extends Model
{
    
    function newJobList($input)
    {
        $result = $this->insertQuery('job_email_list', $input);
        return $result;
    }
    
    function jobListFromJob($input)
    {
        $q = "SELECT email,status_desc, send_time FROM job_email_list WHERE job_id=:job_id";
        $result = $this->fetchAllQuery($q, $input);
        return $result;
    }

    function deleteJobEmailListByJobID($id)
    {
        $query = $this->doQuery("DELETE FROM job_email_list WHERE job_id=$id");
    }
}

?>