<?php 
/* MODEL JOB */

class ModelJob extends Model
{
    
    function newJob($input)
    {
        $result = $this->insertQuery('job', $input);
        return $result;
    }

    function newJobEmailList($input)
    {
        $result = $this->insertQuery('job_email_list', $input);
        return $result;
    }

    function getJobLastInput()
    {
        $query = "SELECT * FROM job ORDER BY job_id DESC";
        $result = $this->fetchSingleQuery($query);
        return $result;
    }

    public function getPendingUpload($uid)
        {
            $q = 'SELECT * FROM ticket_upload WHERE upload_user=:user_id AND upload_temp=1';
            return $this->fetchAllQuery($q, $uid);
        }

    function getJobListbyUser($input)
    {
        $query = "SELECT * FROM view_job
         WHERE user=:user_id       
        ";
        $result = $this->fetchAllQuery($query, $input);
        return $result;
    }

    function getJobList()
    {
        $query = "SELECT * FROM view_job      
        ";
        $result = $this->fetchAllQuery($query);
        return $result;
    }

    function getJobById($input, $select = 0)
    {
        $query = "SELECT ";
        if ($select) {
            $query .= $select;
        }else{
            $query .= '*';
        }

        $query .= " FROM job
        LEFT JOIN sushitei_portal.portal_user sppu ON (user=sppu.user_id) 
        WHERE job_id=:job_id";

        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function getJobId($input)
    {
        $query = "SELECT * FROM job WHERE job_id=:jobid";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function deleteJob($id)
    {
        $query = $this->doQuery("DELETE FROM job WHERE job_id=$id");
    }

    function deleteJobEmailListByJobID($id)
    {
        $query = $this->doQuery("DELETE FROM job_email_list WHERE job_id=$id");
    }

    function editJob($form = array(), $where = array())
   {
        $table = 'job';
        $result = $this->editQuery($table, $form, $where);
        return $result;
   }

   #cek ada file image d job
    function checkImageJob($input)
    {
        $query_string = "SELECT attachment FROM job WHERE job_id=:job_id AND attachment IS NOT NULL ";
        $result = $this->fetchAllQuery($query_string, $input);
        return $result;
    }

    #cancel job
    function cancelJob($form = array(), $where = array())
    {
        $table = 'job';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }

    function modifyJobTestEmail($id)
    {
      $query = $this->doQuery("UPDATE job SET status=5, last_update=NOW() WHERE job_id={$id}");
    }

    function modifyJobStatus($form = array())
    {
        $table = 'job';
        $this->beginTransction();
        try {
            $this->editQuery($table, $form['value'], $form['where']);

            $this->commit();
            return 1;
        } catch (Exception $e) {
            $this->rollBack();
            return 0;
        }
    }

    function getUserStatusByUserId($input){
        $query = "SELECT 
                    email,
                    display_name,
                    user_status,
                    user
                    FROM view_job
                    WHERE user=:user_id AND user_status=1 ";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

} /*Model Job End Here*/

?>