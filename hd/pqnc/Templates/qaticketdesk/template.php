<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    
    <title>PQNC QA</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?="$_resource_css/bootstrap.min.css"?>" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="<?="$_resource_css/dashboard.css"?>" rel="stylesheet">
    <link href="<?="$_template_css/style.css"?>" rel="stylesheet">
    <?=(isset($_option['bdatepick_css']) && $_option['bdatepick_css'] == 1) ? "<link href=\"{$_resource_css}/bootstrap-datepicker3.min.css\" rel=\"stylesheet\">" : ''?>

    <link rel="shortcut icon" href="Resources/images/Ico/favicon.png">
</head>

<body>
    <div class="containerx">
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                <img src="<?=$_template_image?>/logo2.png" style="width: 100px; height: 100px; border:2px solid #fff;">
              </a>
              <a class="navbar-brand" href="http://helpdesk.sushitei.co.id/">Sushi Tei - PQNC QA</a>
            </div>
            <?php
            if($_mySession['user_name'] != 'Guest' && $_mySession['group'] != 'Guest'){
                
            ?>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="my_account.html"><?=$_mySession['user_name']?></a></li>
                <li><a href="create_ticket.html">Submit Ticket</a></li>
                <li><a href="<?=getConfig('base_domain').'user/logout'?>">Logout</a></li>
              </ul>
            </div>
            <?php
            }
            ?>
          </div>
        </nav>
        <div class="container-fluid">
            <div class="row row-down">
                <?php
                    if(!$_option['admin']){
                        $menu_list = array(
                            'PQNC Center' => getConfig('base_url').'index.html',
                            'Submit Ticket' => getConfig('base_url').'create_ticket.html',
                            'Logout' => getConfig('base_domain').'/user/logout'
                        );
                    } else {
                        $menu_list = array(
                            'Ticket Admin' => getConfig('base_url').'ticket_admin.html',
                            'PQNC Center' => getConfig('base_url').'index.html',
                            'Submit Ticket' => getConfig('base_url').'create_ticket.html',
                            'Logout' => getConfig('base_domain').'/user/logout'
                        );
                    }
                    if(isset($menu_list)){
                        echo "
                <div class=\"col-sm-2 sidebar\">
                    <ul class=\"nav nav-sidebar\">
                        ";
                        foreach($menu_list as $menu => $url){
                            if($url == "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"){
                                $link = "<li class=\"active\"><a href=\"{$url}\">{$menu}<span class=\"sr-only\">(current)</span></a></li>\n\t\t\t\t\t";
                            }else{
                                $link = "<li><a href=\"{$url}\">{$menu}</a></li>\n\t\t\t\t\t";
                            }
                            echo $link;
                        }
                    echo '
                    </ul>
                </div>
                <div class="col-sm-10 main">
                    ';
                    }else{
                        echo '
                        <div class="main">';
                    }
                
    $this->view($_action,$_passed_var);
                ?>
                </div>
            </div>
        </div>
    </div>
</div>    
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=$_resource_js?>/jquery-1.12.3.min.js"></script>
    <?=(isset($_option['tinymce']) && $_option['tinymce'] == 1) ? "<script src=\"http://cdn.tinymce.com/4/tinymce.min.js\"></script>\n" : ''?>
    <?=(isset($_option['personal_js']) && $_option['personal_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/script.js\"></script>\n" : ''?>
    <?=(isset($_option['bdatepick_js']) && $_option['bdatepick_js'] == 1) ? "<script type=\"text/javascript\" src=\"{$_resource_js}/bootstrap-datepicker.min.js\"></script>\n" : ''?>
    <?=(isset($_option['scriptload'])  ? $_option['scriptload'] : '')?>
    
    <script>window.jQuery || document.write('<script src="<?="{$_resource_js}/vendor/jquery.min.js"?>"><\/script>')</script>
    <script src="<?="$_resource_js/bootstrap.min.js"?>"></script>
</body>
</html>
