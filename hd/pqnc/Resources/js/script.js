function editCategory(classid){
    $.ajax({
        type: "POST",
        url: "ajax_editcategory.html",
        data: "cid="+classid,
        success: function(html){
            if(html=="false")    {
                $("#modal-content").html("No Data Available");
            } else {
                var data = $.parseJSON(html);
                
                $(data).each(function() {
                    $("#catid").attr("value", this.category_id);
                    $("#subcategoryid").attr("value", this.category_id);
                    $("#catname").attr("value", this.category_name);
                    $("#catdesc").html(this.description);
                });
            }
        },
    });
    return false;
}



function submitEdit(){
    username=$("#inputName").val();
    password=$("#inputPassword").val();
    $.ajax({
        type: "POST",
        url: "ajax_login.html",
        data: "ticket_email="+username+"&ticket_password="+password,
        success: function(html){
            if(html=="true")    {
                window.location.assign("index.html");
            } else {
                $("#userName").addClass("has-error");
                $("#userName .control-label").removeClass("sr-only");
            }
        },
    });
    return false;
}


function getSubCategory(classid){
    $.ajax({
        type: "POST",
        url: "ajax_subcategory.html",
        data: "category="+classid,
        success: function(html){
            if(html=="false")    {
                $("#tablesub").html("Tidak ada sub categori");
                $("#tablesub").addClass("sr-only");
            } else {
                var data = $.parseJSON(html);
                $("#tablesub").removeClass("sr-only");
                $("#tablesub").html("");
                $(data).each(function() {
                    var row_data = "<div class=\"row\">"+
                    "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-name form-control\" name=\"category_name\" value=\""+this.category_name+"\" data-id=\""+this.category_id+"\" disabled/></div>"+
                    "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-desc form-control\" name=\"description\" value=\""+this.description+"\" data-id=\""+this.category_id+"\" disabled/></div>"+
                    "<div class=\"col-sm-3 form-group\"><button type='button' class='edit-sub-category btn btn-primary btn-sm' data-id=\""+this.category_id+"\">Edit</button> <button type='button' class='delete-sub-category btn btn-danger btn-sm' data-id=\""+this.category_id+"\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\">Delete</button> <button type='button' class='update-sub-category btn btn-primary btn-sm hidden' data-id=\""+this.category_id+"\">Edit</button> <button type='button' class='cancel-sub-category btn btn-sm hidden' data-id=\""+this.category_id+"\">Batal</button></div>"+
                    "</div>";
                    $("#tablesub").append(row_data);
                });
            }
        },
    });
    return false;
}

function editFormSubCategory(dataid){
    if($(".update-sub-category[data-id='"+dataid+"']").hasClass("hidden")){
        $(".sub-category-name[data-id='"+dataid+"']").attr("disabled", false);
        $(".sub-category-desc[data-id='"+dataid+"']").attr("disabled", false);
        $(".update-sub-category[data-id='"+dataid+"']").removeClass("hidden");
        $(".cancel-sub-category[data-id='"+dataid+"']").removeClass("hidden");
        $(".edit-sub-category[data-id='"+dataid+"']").addClass("hidden");
        $(".delete-sub-category[data-id='"+dataid+"']").addClass("hidden");
    }
}

function cancelFormSubCategory(dataid){
    if($(".edit-sub-category[data-id='"+dataid+"']").hasClass("hidden")){
        $(".sub-category-name[data-id='"+dataid+"']").attr("disabled", true);
        $(".sub-category-desc[data-id='"+dataid+"']").attr("disabled", true);
        $(".update-sub-category[data-id='"+dataid+"']").addClass("hidden");
        $(".cancel-sub-category[data-id='"+dataid+"']").addClass("hidden");
        $(".edit-sub-category[data-id='"+dataid+"']").removeClass("hidden");
        $(".delete-sub-category[data-id='"+dataid+"']").removeClass("hidden");
    }
}

function changeValue(){
    main_category=$("#mainCategory").val();
    $.ajax({
        type: "POST",
        url: "ajax_subcategory.html",
        data: "category="+ main_category,
        success: function(json){
            
            if(json=="false")    {
                $("#subCategory").html("<option value=\'0 selected></option>");
                $("#subCategory").attr("disabled", true);
            } else {
                var data = $.parseJSON(json);
                $("#subCategory").html("");
                $(data).each(function() {
                    var jsondata = "<option value=\'"+this.category_id+"\'>"+this.category_name+"</option>";
                    $("#subCategory").append(jsondata);
                });
                $("#subCategory").attr("disabled", false);
            }
        },
    });
    return false;
}

function changeBehalf(){
    $.ajax({
        type: "POST",
        url: "ajax_user.html",
        data: "user=1",
        success: function(json){
            
            if(json=="false")    {
                $("#userlist").html("");
                $("#userlist").attr("disabled", true);
            } else {
                var data = $.parseJSON(json);
                $("#userlist").html("");
                $(data).each(function() {
                    var jsondata = "<option value='"+this.user_id+"' data-id='"+this.display_name+"'>"+this.display_name+"</option>";
                    $("#userlist").append(jsondata);
                });
                $("#userlist").attr("disabled", false);
            }
        },
    });
    return false;
}

function submitSubCategory(me){
    cid = $("#subcategoryid").val();
    name = $("#categoryname").val();
    desc = $("#categorydesc").val();
    if ( me.data('requestRunning') ) {
        return;
    }

    me.data('requestRunning', true);

    $.ajax({
        type: "POST",
        url: "ajax_insert_category.html",
        data: "category_parent_id="+cid+"&category_name="+name+"&description="+desc+"&category=1",
        success: function(html){
            if(html!="failed"){
                $("#categoryname").val('');
                $("#categorydesc").val('');
                $("#categorytime").val('');
                var row_data = "<div class=\"row\">"+
                "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-name form-control\" name=\"category_name\" value=\""+name+"\" data-id=\""+html+"\" disabled/></div>"+
                "<div class=\"col-sm-3 form-group\"><input type=\"text\" class=\"sub-category-desc form-control\" name=\"description\" value=\""+desc+"\" data-id=\""+html+"\" disabled/></div>"+
                "<div class=\"col-sm-3 form-group\"><button type='button' class='edit-sub-category btn btn-primary btn-sm' data-id=\""+html+"\">Edit</button> <button type='button' class='delete-sub-category btn btn-danger btn-sm' data-id=\""+html+"\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\">Delete</button> <button type='button' class='update-sub-category btn btn-primary btn-sm sr-only' data-id=\""+html+"\">Edit</button> <button type='button' class='cancel-sub-category btn btn-sm sr-only' data-id=\""+html+"\">Batal</button></div>"+
                "</div>";
                $("#tablesub").append(row_data);
              
            } else {
                alert("Terjadi kesalahan");
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}

function submitCategory(me){
    name = $("form#addcategory input[name=category_name]").val();
    desc = $("form#addcategory textarea[name=category_description]").val();
    if ( me.data('requestRunning') ) {
        return;
    }

    me.data('requestRunning', true);

    $.ajax({
        type: "POST",
        url: "ajax_insert_category.html",
        data: "category_name="+name+"&description="+desc+"&category=1",
        success: function(html){
            if(html!="failed"){
		var rowCount = parseInt($('#listTable tr').length)+1;
                $("form#addcategory input[name=category_name]").val('');
                $("form#addcategory textarea[name=category_description]").val('');
                $("form#addcategory input[name=category_treshold]").val('');
                var row_data = "<tr>"+
                "<td>"+ rowCount +"</td>"+
                "<td>"+ name +"</td>"+
                "<td>"+ desc +"</td>"+
                "<td><button type='button' id='"+ html +"' class='edit-category btn btn-primary btn-sm' data-toggle='modal' data-target='#myModal'>Edit</button></td>"+
                "<td><a class='delete_category' id='"+ html +"' href=\"#\">Delete</a></td>"+
                "</tr>";
                $("#listTable").append(row_data);
                alert('New Category added');
                
            } else {
                alert('Category failed to add');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}


function submitEditCategory(me){
    id = $("form#editcategory input[name=category_id]").val();
    name = $("form#editcategory input[name=category_name]").val();
    desc = $("form#editcategory textarea[name=description]").val();
    if ( me.data('requestRunning') ) {
        return;
    }

    me.data('requestRunning', true);

    $.ajax({
        type: "POST",
        url: "ajax_edit_category.html",
        data: "category_id="+id+"&category_name="+name+"&description="+desc+"&category=1",
        success: function(html){
            if(html!="failed"){
                alert('Edited Successfully');
                location.reload();
                
            } else {
                alert('Failed to edit');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}

function toogleEditor(){
    $("#ticket-content").tinymce({
        // Location of TinyMCE script
        script_url : "./Resources/js/tiny_mce/tiny_mce.js",
        entity_encoding : "raw",
    });
}

function showComment(text_title, text_label, button_name){
    $("#tickettablecomment").toggleClass("hidden");
    $("#labelcomment").html(text_label);
    $("#tickettitle").html(text_title);
    $("#submitstatus").html(button_name);
    $("#submitstatus").val(button_name);
    $("#activitybutton").toggleClass("hidden");
}

function showActivity(){
    $("#tickettablecomment").toggleClass("hidden");
    $("#activitybutton").toggleClass("hidden");
}

function toggleEdit(){
    $("#tiketttabledetail").toggleClass("hidden");
    $("#tickettableedit").toggleClass("hidden");
    $("#tickettabledetail").toggleClass("hidden");
    $("#tickettabledesc").toggleClass("hidden");
    $("#tickettableraw").toggleClass("hidden");
}

function toggleAssign(){
    $("#assignform").toggleClass("hidden");
}

function toggleResolve(){
    $("#coreProblem").toggleClass("hidden");
}
function toggleSparePart(){
    $("#spareParts").toggleClass("hidden");
}

function editTicket(){
    var reasons = $('#updatereason').val();
    var tid = $("form#detailedit input[name=ticket_id]").val();
    var data_list = "submit_type="+$('#submitstatus').val()+"&comment="+reasons+"&t_id="+tid;
    if(!$('#assignform').hasClass("hidden")){
        pic=$("#userlist").val();
        pic_name=$("#userlist").find(':selected').data('id');
        data_list += "&assigned_staff="+pic+"&a_name="+pic_name;
    }
    if(!$('#coreProblem').hasClass("hidden")){
        cause=$("#coreproblem").val();
        data_list += "&problem_source="+cause;
    }
    if(!$('#sparePartName').hasClass("hidden")){
        cause=$("#sparePartName").val();
        data_list += "&spare_part="+cause;
    }
    if($('#submitstatus').val() == 'Edit'){
        var desc = $('#ticketcontent').val();
        var type = $("form#detailedit select[name=type]").val();
        var main = $("form#detailedit select[name=main_category]").val();
        var sub = $("form#detailedit select[name=sub_category]").val();
        var impact = $("form#detailedit input[name=customer_impact]").val();
        data_list += "&description="+desc+"&ticket_type="+type+"&main_category="+main+"&sub_category="+sub+"&impact="+impact;
    }
    
    
    $.ajax({
        type: "POST",
        url: "ajax_edit_ticket.html",
        data: data_list,
        success: function(html){
            if(html == 'sukses'){
                location.reload();
            }else{
                alert(html);
            }
        },
    });
    return false;
}

function getUser(classid){
    $.ajax({
        type: "POST",
        url: "ajax_user_detail.html",
        data: "uid="+classid,
        success: function(html){
            if(html=="false")    {
                $("#modal-content").html("No Data Available");
            } else {
                var data = $.parseJSON(html);
                
                $(data).each(function() {
                    $("#userid").attr('value', this.user_id);
                    $("#username").attr('value', this.user_name);
                    $("#displayname").attr('value', this.display_name);
                    $("#email").attr('value', this.email);
                    $("#groupid").val(this.group_id);
                    $("#userstatus").val(this.user_status);
                });
            }
        },
    });
    return false;
}

function loginForm(me){
    username=$("#inputName").val();
    password=$("#inputPassword").val();
    last_url=$("#lastUrl").val();
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_login.html",
        data: "ticket_email="+username+"&ticket_password="+password,
        success: function(html){
            if(html=="true")    {
                if(last_url != ''){
                    window.location.assign(last_url);
                }else{
                    window.location.assign("index.html");
                }
                
            } else {
                $("#userName").addClass("has-error");
                $("#userName .control-label").removeClass("sr-only");
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}