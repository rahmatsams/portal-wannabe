<?php
/** ------------------------------------------------------------------
* SISTER (SISTEM TERINTEGRASI) Framework 
*  by Rahmat samsudin (siegleind@gmail.com)
*  v1.02 BETA
* ------------------------------------------------------------------
*  Defining default Folder location
*/ 

#Uncomment for local host for directly using file with actual path
#define('__DS__', DIRECTORY_SEPARATOR);
#define('__ROOT__', dirname(dirname(__FILE__)) . __DS__); 
define('__DS__', '/');
define('__ROOT__', '');
define('__BASE_DIR', '');
define('__DEV_MODE', 1); /* Set site environment */
define('__SYSTEM_DIR', __ROOT__ . __BASE_DIR . 'Systems' . __DS__);
define('__CONFIGS_DIR', __SYSTEM_DIR . 'Configs' . __DS__);
define('__APPLICATIONS_DIR', __SYSTEM_DIR .  'Applications' . __DS__);
define('__LIBS_DIR', __SYSTEM_DIR . 'Libraries' . __DS__);
define('__LANGUAGE_DIR', __SYSTEM_DIR . 'Languages' . __DS__);

/* Defining base file */
define('__BASE_CONFIG_FILE','Config.php');
define('__BASE_LIBRARY_FILE','Routes.php');

/* Defining url command */
define('__CONTROLLER_COMMAND','s');
define('__ACTION_COMMAND','i');
/* Set time Zone */
date_default_timezone_set("Asia/Jakarta");

/* Include base library files */
require_once(__LIBS_DIR . __BASE_LIBRARY_FILE);
/* End index.php */