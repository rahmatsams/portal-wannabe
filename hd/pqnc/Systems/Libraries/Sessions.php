<?php
	class Session {
		// session-lifetime
		var $lifeTime;
		// mysql-handle
		var $dbHandle;
		function open($savePath, $sessName) {
		   // get session-lifetime
		   $this->lifeTime = 10;
		   // open database-connection
		   
			$dbHandle = @mysql_connect(config('host'),config('username'),config('password')) or die('Unable to connect to database');
			 $dbSel = @mysql_select_db(config('db_name'),$dbHandle) or die('Database not Found');
		   //return success
		   if(!$dbHandle || !$dbSel)
			   return false;
		   $this->dbHandle = $dbHandle;
		   return true;
		}
		function close() {
			$this->gc(ini_get('session.gc_maxlifetime'));
			// close database-connection
			return @mysql_close($this->dbHandle);
		}
		function read($sessID) {
			// fetch session-data
			$res = mysql_query("SELECT session_data AS d FROM ptcs_sessions
								WHERE session_id = '".$sessID."'
								AND session_expires > ".time(),$this->dbHandle);
			// return data or an empty string at failure
			if($row = mysql_fetch_assoc($res))
				return $row['d'];
			return "";
		}
		function write($sessID,$sessData) {
			// new session-expire-time
			$newExp = time() + $this->lifeTime;
			// is a session with this id in the database?
			$res = mysql_query("SELECT * FROM ptcs_sessions
								WHERE session_id = '".$sessID."'",$this->dbHandle);
			// if yes,
			if(mysql_num_rows($res)) {
				// ...update session-data
				mysql_query("UPDATE ptcs_sessions
							 SET session_expires = '".$newExp."',
							 session_data = '".$sessData."'
							 WHERE session_id = '".$sessID."'",$this->dbHandle);
				// if something happened, return true
				if(mysql_affected_rows($this->dbHandle))
					return true;
			}
			// if no session-data was found,
			else {
				// create a new row
				mysql_query("INSERT INTO ptcs_sessions (
							 session_id,
							 session_expires,
							 create_date,
							 session_data)
							 VALUES(
							 '".$sessID."',
							 '".$newExp."',
							 NOW(),
							 '".$sessData."')",$this->dbHandle);
				// if row was created, return true
				if(mysql_affected_rows($this->dbHandle))
					return true;
			}
			// an unknown error occured
			return false;
		}
		function destroy($sessID) {
			// delete session-data
			mysql_query("DELETE FROM ptcs_sessions WHERE session_id = '".$sessID."'",$this->dbHandle);
			// if session was deleted, return true,
			if(mysql_affected_rows($this->dbHandle))
				return true;
			// ...else return false
			return false;
		}
		function gc($sessMaxLifeTime) {
			// delete old sessions
			mysql_query("DELETE FROM ptcs_sessions WHERE session_expires < ".time(),$this->dbHandle);
			// return affected rows
			return mysql_affected_rows($this->dbHandle);
		}
	}
?>