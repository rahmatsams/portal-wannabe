<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ajax extends Controller
{
	public function accessRules()
	{
		return array(
            array('Allow', 
				'actions'=>array('getLoginData','getSubCategory', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
				'groups'=>array('*'),
			),
			array('Deny', 
				'actions'=>array('getSubCategory', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
				'groups'=>array('Guest'),
			),
		);
	}
	
    public function getLoginData()
    {
		if($this->isGuest() > 0){
			if(isset($_POST['ticket_email']) && isset($_POST['ticket_password'])){
				$input = $this->load->lib('Input');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'min=1', 'Alamat email harus diisi');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'user_name', 'Format email salah');
                $input->addValidation('ticket_password', $_POST['ticket_password'], 'min=1', 'Password harus diisi');
		
				if ($input->validate()) {
                    $model_users = $this->load->model('Users');
					$result = $model_users->doLogin($_POST);
					if($result > 0){
						$this->setSession('user_id',$result['user_id']);
						$this->setSession('user_name',$result['display_name']);
                        $this->setSession('email',$result['email']);
						$this->setSession('group',$result['group_name']);
						$this->setSession('role',$result['level']);
						echo 'true';
					} else {
						echo 'not found';
					}
				} else {
					print_r($input->_error);
				}
			
			} else {
				echo 'false';
			}
		
		}
	}
	
	function getSubCategory(){
		if(isset($_POST['category'])){
			$input = $this->load->lib('Input');
			$input->addValidation('category', $_POST['category'] ,'numeric','Terjadi kesalahan');
			if($input->validate()){
				$model_ticket = $this->load->model('TicketCategory');
				$result = $model_ticket->getSubCategory($_POST['category']);
				if(is_array($result) && !empty($result)){
					echo json_encode($result);
				} else {
					echo 'false';
				}
				
			}
		} else {
			echo 'false';
		}
	}
    
    function getUserDetail(){
		if(isset($_POST['uid'])){
			$input = $this->load->lib('Input');
			$input->addValidation('uid', $_POST['uid'] ,'numeric','Terjadi kesalahan');
			if($input->validate()){
                $data = array(
                    'user_id' => $_POST['uid']
                );
				$m_users = $this->load->model('Users');
				$result = $m_users->getUserBy($data);
				if(is_array($result) && !empty($result)){
					echo json_encode($result);
				} else {
					echo 'false';
				}
			}
	
		
		} else {
			echo 'false';
		}
	}
    
    function getUserList(){
		if(isset($_POST['user'])){
			$input = $this->load->lib('Input');
			$input->addValidation('user', $_POST['user'] ,'numeric','Terjadi kesalahan');
			if($input->validate()){
				$model_ticket = $this->load->model('Users');
				$result = $model_ticket->getAllNotAdmin();
				if(is_array($result) && !empty($result)){
					echo json_encode($result);
				} else {
					echo 'false';
				}
				
			}
		} else {
			echo 'false';
		}
	}
	
    public function getTicketData(){
        if(!$this->isGuest()){
            $input = $this->load->lib('Input');
            $input->addValidation('page',$_POST['page'],'numeric', 'Periksa kembali input anda');
            if(isset($_POST['main_category']) && !empty($_POST['main_category'])){
                $input->addValidation('main_category_char',$_POST['main_category'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('main_category_max',$_POST['main_category'], 'max=3', 'Periksa kembali input anda');
            }
            if(isset($_POST['ticket_type']) && !empty($_POST['ticket_type'])){
                $input->addValidation('status_char',$_POST['status'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('status_max',$_POST['status'], 'max=2', 'Periksa kembali input anda');
            }
            if(isset($_POST['store']) && !empty($_POST['store'])){
                $input->addValidation('store',$_POST['store'], 'alpha_numeric_sp', 'Please check your input');
                $input->addValidation('store_max',$_POST['store'], 'max=25', '25');
            }
            if(isset($_POST['date_start']) && !empty($_POST['date_start'])){
                $input->addValidation('date_start',$_POST['date_start'], 'date', 'Periksa kembali input anda');
            }
            if(isset($_POST['date_end']) && !empty($_POST['date_end'])){
                $input->addValidation('date_end',$_POST['date_end'], 'date', 'Periksa kembali input anda');
            }
            

            if ($input->validate()){
                $model_ticket = $this->load->model('Tickets');
                
                $option = array(
                    'page' => (isset($_POST['page']) ? $_POST['page'] : 1),
                    'result' => 10,
                    'order_by' => 'submit_date',
                    'order' => 'DESC'
                );
                
                $query = array();
                
                if($_POST['sort'] == 'title_asc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'title_desc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'outlet_asc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'outlet_desc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'submit_asc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'submit_desc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'category_asc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'category_desc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'pic_asc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'pic_desc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'status_asc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'status_desc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'DESC';
                }
                
                if(isset($_POST['ticket_title']) && !empty($_POST['ticket_title'])){
                    $query['ticket_title']['value'] = "%{$_POST['ticket_title']}%";
                    $query['ticket_title']['type'] = 'LIKE';
                }
                
                if(isset($_POST['main_category']) && !empty($_POST['main_category']) ){
                  $query['category']['value'] = "%{$_POST['main_category']}%";
                  $query['main_category']['type'] = 'LIKE';
                } 
                
                if(isset($_POST['status']) && !empty($_POST['status'])){
                    $query['status']['value'] = $_POST['status'];
                    $query['status']['type'] = '=';
                }
                
                if(isset($_POST['store']) && !empty($_POST['store'])){
                    $query['store_id']['value'] = $_POST['store'];
                    $query['store_id']['type'] = '=';
                }
                
                if(isset($_POST['date_start']) && !empty($_POST['date_start'])) {
                    $query['submit_date']['value'] = $_POST['date_start'];
                    $query['submit_date']['type'] = '=';
                }
                
                if(isset($_POST['date_end']) && !empty($_POST['date_end'])){
                    $query['date_end']['value'] = $_POST['date_end'];
                    $query['date_end']['type'] = '=';
                }
                
                if(!$this->isAdmin()){
                    $query['ticket_user']['value'] = $this->_mySession['user_id'];
                    $query['ticket_user']['type'] = '=';
                    
                    /*$my_info = $model_ticket->myOutlet(array('user_id' => $this->_mySession['user_id']));
                    $query['store_name'] = $my_info['store_name'];*/
                    $query['store_id']['value'] = $this->_mySession['outlet'];
                    $query['store_id']['type'] = '=';
                }
               
                
                $ticket_list = $model_ticket->showAllTicketFiltered($query, $option);
                $this->setSession('last_query', $query);
                $this->setSession('option', $option);
                if(count($ticket_list) > 0){
                    $this->setSession('last_page', ceil($model_ticket->getCountResult()/$option['result']));
                    $this->setSession('current_page', $option['page']);
                    $num = (($option['page']-1)*10) + 1;
                    foreach($ticket_list as $result){
                        $date_check = '';
                        $diff_seconds  = ($result['resolved_date'] != '0000-00-00 00:00:00' ? strtotime($result['resolved_date']) - strtotime($result['submit_date']) : strtotime(date("Y-m-d H:i:s")) - strtotime($result['submit_date']));
                        $stat = floor($diff_seconds/3600);
                        
                        if(($stat >= 24 && $stat < 48) && $result['status'] < 5){
                            
                            $date_check = "warning";
                        }elseif($stat >= 48 && $result['status'] < 5){
                            $date_check = "danger";
                        }elseif($result['status'] >= 5){
                            $date_check = "success";
                        }else {
                            $date_check = 'active';
                        }
						
						$submit_date = date("d-m-Y", strtotime($result['submit_date']));
						$last_update = (!empty($result['last_update']) ? date("d-m-Y", strtotime($result['last_update'])) : '-');
                            
                        if(!$this->isAdmin()){
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$num} </td>
                        <td>{$result['ticket_id']}</td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                        <td><a href=\"result_by_cat_{$result['category_id']}.html\">{$result['category_name']}</a></td>
                        <td>{$result['staff_name']}</td>
                        <td><a href=\"result_by_status_{$result['status']}.html\">{$result['status_name']}</a></td>
                        <td>{$submit_date}</td>
                        <td>{$last_update}</td>
                    </tr>";    
                        }else{
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$num} </td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                        <td>{$result['display_name']}</td>
                        <td><a href=\"result_by_cat_{$result['category_id']}.html\">{$result['category_name']}</a></td>
                        <td>{$result['store_name']}</td>
                        <td>{$result['staff_name']}</td>
                        <td><a href=\"result_by_status_{$result['status']}.html\">{$result['status_name']}</a></td>
                        <td>{$submit_date}</td>
                        <td>{$last_update}</td>
                    </tr>";
                        }                
                        $num++;
                    }
                } else {
					print_r($query);
                    echo "
                    <tr>
                        <td colspan=\"9\">Tiket Kosong</td>
                    </tr>";
                }           
            }else{
                print_r($input->_error);
            }
		} else {
            echo 'nope';
        }
        
    }
    
    public function getTotalPage(){
        if($this->_mySession['last_page'] > 1){
            
            if(($this->_mySession['current_page'] - 5) > 0){
                echo"
                <li>
                    <a href=\"#\" class='page-navigate' data-id='1'>1</a>
                </li>
                <li>
                    <a href=\"#\">...</a>
                </li>";
            }
            $page_start = 1;
            $page_end = $this->_mySession['last_page'];
            if($this->_mySession['last_page'] > 9 && $this->_mySession['current_page'] > 9)
            {
                $page_start = (($this->_mySession['current_page']-4) < 1 ? 1 : $this->_mySession['current_page']-4);
                $page_end = (($this->_mySession['current_page']+4) > $this->_mySession['last_page'] ? $this->_mySession['last_page'] : ($this->_mySession['current_page']+4));
            }
            elseif($this->_mySession['last_page'] > 9 && $this->_mySession['current_page'] <= 9)
            {
                $page_start = (($this->_mySession['current_page']-4) < 1 ? 1 : $this->_mySession['current_page']-4);
                $page_end = (($this->_mySession['last_page']) > 9 ? 9+4 : 9);
            }elseif($this->_mySession['last_page'] < 9 && $this->_mySession['current_page'] <= 9)
            {
                $page_start = (($this->_mySession['current_page']-4) < 1 ? 1 : $this->_mySession['current_page']-4);
                $page_end = $this->_mySession['last_page'];
            }
            
            for($i=$page_start;$i < $page_end;$i++){
                echo "<li ".($this->_mySession['current_page'] == $i ? 'class="active"' : '')."><a class='page-navigate' data-id='{$i}'>{$i}</a></li>";
            }
            if($this->_mySession['current_page'] < $this->_mySession['last_page']){
                echo "
                        <li>
                            <a href=\"#\">...</span></a>
                        </li>";
            }
            echo "
                <li>
                    <a class='page-navigate' data-id='{$this->_mySession['last_page']}'>{$this->_mySession['last_page']}</a>
                </li>";
        }else{
            echo '';
        }
    }
    
    public function getCategoryEdit()
    {
		if(!$this->isGuest()){
			if(isset($_POST['cid'])){
			$input = $this->load->lib('Input');
            $input->addValidation('cid', $_POST['cid'], 'numeric', 'Periksa kembali input anda');
            
				if ($input->validate()) {
					$m_category = $this->load->model('TicketCategory');
					$result = $m_category->getCategoryId($_POST);
                    $sub_category = $m_category->getSubCategory($_POST['cid']);
					if(is_array($result)){
                        echo json_encode($result);
					} else {
						echo 'false';
					}
				} else {
					echo 'false';
				}
			
			} else {
				echo 'false';
			}
		
		} else {
            echo 'false';
        }
	}
    
    public function editTicket(){
        if(isset($_POST['submit_type'])){
            $result = 'failed';
            $m_ticket = $this->load->model('Tickets');
            $input = $this->load->lib('input');
            $edit_status = 0;
            
            if($_POST['submit_type'] == 'Take Over'){
               
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input->addValidation('assigned_staff', $_POST['assigned_staff'], 'numeric', 'Unknown Staff');
                $input->addValidation('a_name', $_POST['a_name'], 'alpha_numeric_sc', 'Unknown Target');
				$input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'assigned_staff' => $_POST['assigned_staff'],        
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Take Over',
                        'ticketlog_title' => $this->_mySession['user_name']. " take over the item to ". $_POST['a_name'],
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "PQNC #{$_POST['t_id']} (ASSIGN): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Force Close'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 5,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Force-Close',
                        'ticketlog_title' => $this->_mySession['user_name']. " Closing this ticket.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "PQNC #{$_POST['t_id']} (FORCE CLOSE): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Close'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 5,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Force-Close',
                        'ticketlog_title' => $this->_mySession['user_name']. " Closing this ticket.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "PQNC #{$_POST['t_id']} (CLOSE): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Release'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 2,
                            'resolved_date' => date("Y-m-d H:i:s"),
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Release',
                        'ticketlog_title' => $this->_mySession['user_name']. " Releasing the item.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "PQNC #{$_POST['t_id']} (Release): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Return'){
                
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 3,
                            'resolved_date' => date("Y-m-d H:i:s"),
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Return',
                        'ticketlog_title' => $this->_mySession['user_name']. " Returning the item.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "PQNC #{$_POST['t_id']} (RETURN): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }elseif($_POST['submit_type'] == 'Reject'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 4,
                            'resolved_date' => date("Y-m-d H:i:s"),
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'ticketlog_type' =>  'Reject',
                        'ticketlog_title' => $this->_mySession['user_name']. " Rejecting the item.",
                        'user_id' => $this->_mySession['user_id'],
                        'ticket_id' => $_POST['t_id'],
                        'ticketlog_content' => $input_filtered['comment'],
                        'ticketlog_time' => $data['input']['last_update'],
                        'ticketlog_show' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option['title'] = "PQNC #{$_POST['t_id']} (REJECT): ";
                    }
                } else {
                    print_r($input->_error);
                    print_r($_POST);
                }
            }
            if($edit_status == 1){
                $ticket_data = $m_ticket->showTicketBy(array('ticket_id' => $_POST['t_id']));
                $mail_option['recipient'] = $ticket_data['email'];
                $mail_option['recipient_name'] = $ticket_data['display_name'];
                $mail_option['title'] .= $ticket_data['ticket_title'];
                $email = $this->send_email($mail_option, $ticket_data, $m_ticket);
                if($email == 1){
                    echo 'sukses';
                }else{
                    echo 'kirim email error '.$email;
                }
            }
        }
	
    }    
    
    private function send_email($option, $data, $model_log = 0){
        $mail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail = $this->load->lib('STMail');
        if(!$model_log) $model_log = $this->load->model('Tickets');
        $log_data = $model_log->getLog(array('ticket_id' => $data['ticket_id']));
        try{
            $stmail->IsSMTP(); // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "qa.helpdesk@sushitei.co.id"; // SMTP account user_name
            $stmail->Password   = "QAHelpdesk123";        // SMTP account password

            $stmail->SetFrom('qa.helpdesk@sushitei.co.id', 'QA Helpdesk');

            $stmail->AddReplyTo('qa.helpdesk@sushitei.co.id', 'QA Helpdesk');
            
            $stmail->isHTML('true');

            $stmail->Subject    = $option['title'];
            
            $stmail->MsgHTML($stmail->mailContent($data, $log_data));
            
            $stmail->AddAddress($option['recipient'], $option['recipient_name']);
            $stmail->AddAddress('ppfbspv@sushitei.co.id', 'PPFB SPV');
            $stmail->AddAddress('fbspv3@sushitei.co.id', 'PPFB SPV 3');
            
            foreach($model_log->getMailRecipient() as $recipient){
                $stmail->AddBCC($recipient['email'], $recipient['display_name']);
            }
            $stmail->Send();
        } catch (phpmailerException $e) {
          return $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
          return $e->getMessage(); //Boring error messages from anything else!
        }
        return 1;
    }
}
/*
* End Home Class
*/