<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Email extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('employee_logout', 'submit_ticket'),
                'groups'=>array('Guest'),
            ),
            array('Allow', 
                'actions'=>array('index', 'employee_login'),
                'groups'=>array('*'),
            ),
        );
    }
    
    public function register_success()
    {
        $data = array(
            '_mySession' => $this->_mySession,
            'flash_data' => $this->getFlashData(),
            'js_folder' => './'.getConfig('resource_dir').'/'.getConfig('js_folder'),
            'css_folder' => './'.getConfig('resource_dir').'/'.getConfig('css_folder'),
            'user_name' => $_GET['user_name'],
            'password' => $_GET['password']
        );
        
        $this->load->view('email/register_success', $data);
        
    }
    
    public function employee_login()
    {
        $data = array(
            '_mySession' => $this->_mySession,
            'flash_data' => $this->getFlashData(),
            'js_folder' => './'.getConfig('resource_dir').'/'.getConfig('js_folder'),
            'css_folder' => './'.getConfig('resource_dir').'/'.getConfig('css_folder'),
        );
        
        $this->load->view('home/login_member', $data);
        
    }
    
    public function employee_logout()
    {
        if($this->isGuest() < 1) {
            $this->unsetSession('user_id');
            $this->setSession('username','Guest');
            $this->setSession('group','Guest');
            $this->setSession('role',1);
            header("Location: ".siteUrl());
        } else {
            header("Location: ".siteUrl().'login.html');
        }
    }
    
    public function new_user()
    {
        if(isset($_POST['register']) && $_POST['register'] == 'Daftar'){
            $m_users = $this->load->model('users');
            $input = $this->load->lib('input');
            $input->addValidation('user_name',$_POST['user_name'],'alpha_numeric', 'Periksa kembali input anda');
            $input->addValidation('user_name',$_POST['user_name'],'min=1', 'User Name wajib diisi');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric', 'Periksa kembali input anda');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Display Name wajib diisi');
            $input->addValidation('password', $_POST['password'],'min=6', 'Password minimal 6 karakter');
            $input->addValidation('password_confirm', $_POST['password_confirm'],'like='.$_POST['password'], 'Password konfirmasi berbeda');
            $input->addValidation('email',$_POST['email'],'email', 'Periksa kembali input anda');
            $input->addValidation('sec_quest', $_POST['sec_quest'],'min=1', 'Periksa kembali input anda');
            $input->addValidation('sec_quest', $_POST['sec_quest'],'numeric', 'Periksa kembali input anda');
            $input->addValidation('answer', $_POST['sec_quest'],'min=1', 'Periksa kembali input anda');
            if($input->validate()){
                $insert_value = array(
                    'user_name' => $_POST['user_name'],
                    'user_password' => MD5($_POST['password']),
                    'display_name' => $_POST['display_name'],
                    'email' => $_POST['email'],
                    'store_id' => '99',
                    'department_id' => '0',
                    'group_id' => '4',
                    'security_question' => $_POST['sec_quest'],
                    'security_answer' => $_POST['answer'],
                    'user_status' => '1'
                );
                if($m_users->newUser($insert_value)){
                    $mail_option = array(
                        'title' => 'Pendaftaran user baru MRP Ticketdesk Sushitei Indonesia',
                        'recipient' => $insert_value['email'],
                        'recipient_name' => $insert_value['display_name'],
                    );
                    $this->send_email($mail_option);
                    header("Location: ".siteUrl().'register_ok.html');
                } else {
                    header("Location: ".siteUrl().'login.html');
                }
            } else {
                $this->setFlashData($input->_error);
                header("Location: ".siteUrl().'login.html');
            }
            
        }else{
            header("Location: ".siteUrl().'login.html');
        }
    }
    
    private function send_email($option){
        $mail             = $this->load->lib('PHPMailer');
        $smtp             = $this->load->lib('SMTP');
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "mail.sushitei.co.id"; // SMTP server
        $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $mail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "it1.jkt@sushitei.co.id"; // SMTP account username
        $mail->Password   = "Fangblade_99";        // SMTP account password

        $mail->SetFrom('it1.jkt@sushitei.co.id', 'Rahmat Samsudin');

        $mail->AddReplyTo('it1.jkt@sushitei.co.id', 'Rahmat Samsudin');

        $mail->Subject    = $option['title'];

        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

        $mail->MsgHTML("Test gogogogogo");

        $mail->AddAddress($option['recipient'], $option['recipient_name']);

        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
          echo "Message sent!";
        }
            
        
    }
    function tests(){
        $data = array(
            '_mySession' => $this->_mySession,
            'flash_data' => $this->getFlashData(),
            'js_folder' => './'.getConfig('resource_dir').'/'.getConfig('js_folder'),
            'css_folder' => './'.getConfig('resource_dir').'/'.getConfig('css_folder'),
        );
        
        $content = file_get_contents(__ROOT__ . __DS__ . __BASE_DIR . __DS__ . __SYSTEM_DIR . __DS__ . __APPLICATIONS_DIR . __DS__ . 'Views' . __DS__ .'home\login_member.php');
        $content = preg_replace("/\\\\/",'',$content);
        echo $content;
    }
}
/*
* End Home Class
*/