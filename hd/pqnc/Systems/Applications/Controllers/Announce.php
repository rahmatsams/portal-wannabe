<?php
class Announce extends Controller
{
    function index()
    {
        $data = array(
          '_mySession' => $this->_mySession,
        );
        $input = $this->load->lib('Input');
        $model = $this->load->model('AnnounceMail');
        if(isset($_POST['submit']) && $_POST['submit'] == 'Search'){            
            $model = $this->load->model('AnnounceMail');
            $input->addValidation('update_first', $_POST['update_first'], 'date', 'Wrong input');
            if(isset($_POST['update_last']) && !empty($_POST['update_last'])){
                $input->addValidation('update_last', $_POST['update_last'], 'date', 'Wrong input');
            }else{
                unset($_POST['update_last']);
            }
                
            if($input->validate()){
                
                $data['input'] = $_POST;
                $data['doc_list'] = $model->getDocByUpdateTime($_POST);
            }
            #print_r($_POST);
            $this->load->template('announce/index', $data);
            
        }elseif(isset($_POST['submit']) && $_POST['submit'] == 'Email' && isset($_POST['start_date'])){
            $input->addValidation('start_date', $_POST['start_date'], 'date', 'Wrong input');
            if(isset($_POST['end_date']) && !empty($_POST['end_date'])) $input->addValidation('end_date', $_POST['end_date'], 'date', 'Wrong input');
            
            if($input->validate()){
                $data_value = array(
                    'start_date' => $_POST['start_date'],
                );
                if(isset($_POST['end_date']) && !empty($_POST['end_date'])) $data_value['end_date'] = $_POST['end_date'];
                $document_data = $model->getMailAddress($data_value);
                $data['failed'] = array();
                $data['success'] = array();
                $num_fail = 0;
                $num_success = 0;
                foreach($document_data as $mail_recipient){
                    $data_value['email'] = $mail_recipient['email'];
                    $mail_option = array(
                        'title' => "New update on DCR site. ",
                        'recipient' => $mail_recipient['email'],
                        'recipient_name' => $mail_recipient['display_name'],
                    );
                    $document_detail = $model->getDocInfoByMail($data_value);
                    #print_r($document_detail);
                    if($this->send_email($mail_option, $document_detail)){
                        $data['success'][$num_success] = $mail_recipient;
                        $num_success++;
                    }else{
                        $data['failed'][$num_fail] = $mail_recipient;
                        $num_fail++;
                    }
                    
                }
                
                $this->load->template('announce/success', $data);
               
            }
            
        }else{
            $this->load->template('announce/index', $data);
        }
        
    }
  
    private function send_email($option, $data){
        $this->load->lib('PHPMailer');
        $stmail   = new PHPMailer();
        $smtp   = $this->load->lib('SMTP');
        #$stmail = $this->load->lib('STMail');
        
        #$log_data = $model_log->getLog(array('ticket_id' => $data['ticket_id']));
        $stmail->IsSMTP(); // telling the class to use SMTP
        $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
        $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $stmail->SMTPAuth   = true;                  // enable SMTP authentication
        $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $stmail->Username   = "qa.helpdesk@sushitei.co.id"; // SMTP account username
        $stmail->Password   = "St123456";        // SMTP account password

        $stmail->SetFrom('qa.helpdesk@sushitei.co.id', 'Web DCR');

        $stmail->AddReplyTo('qa.helpdesk@sushitei.co.id', 'Web DCR');
        
        $stmail->isHTML('true');

        $stmail->Subject    = $option['title'];
        
        $stmail->MsgHTML($this->mailContent($data));
        
        $stmail->AddAddress($option['recipient'], $option['recipient_name']);


        return ($stmail->Send() ? 1 : 0);
            
        
    }
    
    private function mailContent($data){
        $body = "
        <html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><meta http-equiv=Content-Type content=\"text/html; charset=us-ascii\"><meta name=Generator content=\"Microsoft Word 15 (filtered medium)\"><style><!--
/* Font Definitions */
@font-face
	{font-family:\"Cambria Math\";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:\"Calibri\",sans-serif;}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:#0563C1;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:#954F72;
	text-decoration:underline;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{mso-style-priority:34;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:\"Calibri\",sans-serif;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:\"Calibri\",sans-serif;
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:\"Calibri\",sans-serif;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
/* List Definitions */
@list l0
	{mso-list-id:2066832120;
	mso-list-type:hybrid;
	mso-list-template-ids:-1302972358 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l0:level1
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level3
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l0:level4
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level5
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level6
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
@list l0:level7
	{mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level8
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:left;
	text-indent:-.25in;}
@list l0:level9
	{mso-level-number-format:roman-lower;
	mso-level-tab-stop:none;
	mso-level-number-position:right;
	text-indent:-9.0pt;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext=\"edit\" spidmax=\"1026\" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext=\"edit\">
<o:idmap v:ext=\"edit\" data=\"1\" />
</o:shapelayout></xml><![endif]--></head>
<body lang=EN-US link=\"#0563C1\" vlink=\"#954F72\">
    <div class=WordSection1>
        <p class=\"MsoNormal\" style=\"text-align:justify;line-height:150%\"><span style=\"font-size:10.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\" lang=\"EN-US\">
            Diinformasikan bahwa Document Controller PT
            Sushi Tei Indonesia telah mengunggah <b>Dokumen Termutakhir
            </b>di dalam web DCR<o:p></o:p></span>
        </p>
        <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none'>
            <tr>
                <td width=104 valign=top style='width:77.9pt;border:solid windowtext 1.0pt;border-left:none;background:#A6A6A6;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b><span style='font-size:12.0pt'>Department<o:p></o:p></span></b></p></td>
                <td width=104 valign=top style='width:77.9pt;border:solid windowtext 1.0pt;border-left:none;background:#A6A6A6;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b><span style='font-size:12.0pt'>Nomor Dokumen<o:p></o:p></span></b></p></td>
                <td width=175 valign=top style='width:131.5pt;border:solid windowtext 1.0pt;border-left:none;background:#A6A6A6;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b><span style='font-size:12.0pt'>Nama Dokumen<o:p></o:p></span></b></p></td>
                <td width=104 valign=top style='width:77.9pt;border:solid windowtext 1.0pt;border-left:none;background:#A6A6A6;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b><span style='font-size:12.0pt'>Revisi<o:p></o:p></span></b></p></td>
                <td width=104 valign=top style='width:77.9pt;border:solid windowtext 1.0pt;border-left:none;background:#A6A6A6;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><b><span style='font-size:12.0pt'>Status<o:p></o:p></span></b></p></td>

            </tr>
        ";
        foreach($data as $mail_content){
            $status = ($mail_content['status'] == 1 ? 'Aktif' : 'Obsolete');
            $body .= "
            <tr>
                <td width=175 valign=top style='width:131.5pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><span style='font-size:12.0pt'><o:p>{$mail_content['dept_name']}</o:p></span></p></td>
                <td width=104 valign=top style='width:77.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><span style='font-size:12.0pt'><o:p>{$mail_content['number']}</o:p></span></p></td>
                <td width=104 valign=top style='width:77.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><span style='font-size:12.0pt'><o:p><a href=\"viewdoc_{$mail_content['number']}.html\">{$mail_content['name']}</a></o:p></span></p></td>
                <td width=104 valign=top style='width:77.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><span style='font-size:12.0pt'><o:p>{$mail_content['rev']}</o:p></span></p></td>
                <td width=104 valign=top style='width:77.9pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt'><p class=MsoNormal><span style='font-size:12.0pt'><o:p>{$status}</o:p></span></p></td>
            </tr>
            ";
        }
        $body .= "
        </table>
        <p class=\"MsoNormal\" style=\"text-align:justify;line-height:150%\"><span
style=\"font-size:10.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\"
            lang=\"SV\">Keterangan:<o:p></o:p></span></p>
        <p class=\"MsoNormal\" style=\"text-align:justify;line-height:150%\"><b><span
style=\"font-size:10.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\"
              lang=\"SV\">Active </span></b><span
style=\"font-size:10.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\"
            lang=\"SV\">: Dokumen update<o:p></o:p></span></p>
        <p class=\"MsoNormal\" style=\"text-align:justify;line-height:150%\"><b><span
style=\"font-size:10.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\"
              lang=\"SV\">Obsolete </span></b><span
style=\"font-size:10.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\"
            lang=\"SV\">: Dokumen ditarik<o:p></o:p></span></p>
        <p class=\"MsoNormal\" style=\"text-align:justify;line-height:150%\"><span
style=\"font-size:10.0pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\"
            lang=\"SV\">Mohon kepada seluruh User <b>memberikan info by
              email kepada DCR PT Sushi Tei Indonesia bahwa dokumen
              tersebut telah diterima dalam web DCR</b>.<b> DCR akan
              mengirimkan Daftar Distribusi Dokumen kepada masing-masing
              User untuk diparaf</b>.<o:p></o:p></span></p>
        <p class=\"MsoNormal\"><span
style=\"font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;\"
            lang=\"SV\">Demikian hal ini saya sampaikan. Terimakasih atas
            perhatian dan kerjasamanya. Bila ada hal yang kurang jelas
            silahkan menghubungi DCR PT Sushi Tei Indonesia.</span><span
            style=\"font-family:&quot;Arial
            Narrow&quot;,&quot;sans-serif&quot;;color:#5F497A\"
            lang=\"EN-US\"><o:p></o:p></span></p>
        <p class=\"MsoNormal\"><span style=\"font-family:&quot;Arial
            Narrow&quot;,&quot;sans-serif&quot;;color:#5F497A\"
            lang=\"EN-US\"><o:p> </o:p></span></p>
    </div>
</body></html>
        ";
        
        return $body;
    }
}