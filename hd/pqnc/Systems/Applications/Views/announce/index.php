<div class="container-fluid">    
    <h2 class="sub-header">Announce Email</h2>
	<div class="col-md-12">
        <form id="advanceSearch" method="POST" action="announce.html">
            <div class="col-md-12 well">
                <div class="row">
                    <div class="col-sm-3 form-group">
                        <label>Update from</label>
                        <input id="ticketUpdateFirst" name="update_first" type="date" class="form-control" maxlength="10" <?=(isset($input['update_first']) ? "value=\"{$input['update_first']}\"" : '')?>>
                    </div>                            
                    <div class="col-sm-3 form-group">
                        <label>to</label>
                        <input id="ticketUpdateLast" name="update_last" type="date" class="form-control" maxlength="10" <?=(isset($input['update_last']) ? "value=\"{$input['update_last']}\"" : '')?>>
                    </div>
                </div>
                
                <button type="submit" name="submit" value="Search" class="btn btn-sm">Search</button>
            </div>
        </form> 
    </div>
    <div class="col-md-12 table-responsive">
        <form id="sendWithEmail" method="POST" action="announce.html">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr class="center-title">
                        <th>#</th>
                        <th>Document Name</th>
                        <th>Rev</th>
                        <th>Last Update</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(isset($input)){
                            if(isset($input['update_first'])) echo "<input type=\"hidden\" name=\"start_date\" value=\"{$input['update_first']}\">";
                            if(isset($input['update_last'])) echo "<input type=\"hidden\" name=\"end_date\" value=\"{$input['update_last']}\">";
                        }
                        if(isset($doc_list)){
                            $num = 1;
                            foreach($doc_list as $doc){
                                echo "
                    <tr>
                        <td>{$num}</td>
                        <td>{$doc['name']}</a></td>
                        <td>{$doc['rev']}</td>
                        <td>{$doc['upload_date']}</td>
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="9">There's no Data</td>
                    </tr>
                    <?php
                        }
                        
                    ?>
                </tbody>
            </table>
            <button type="submit" name="submit" value="Email" class="btn btn-sm">Send Mail Notification</button>
        </form>
    </div>
</div>