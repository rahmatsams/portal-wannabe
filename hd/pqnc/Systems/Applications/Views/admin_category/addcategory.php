
					<h2>Add New Product Categories</h2>
					<form method="POST" action="<?php echo controllerUrl('adminCategory','add_category'); ?>" enctype="multipart/form-data">
						<table id="quotation-form">
								<tr>
									<td>Category Name</td>
									<td>:</td>
									<td><input type="text" name="c_name" id="catName" value="<?php if(isset($last_input) && !empty($last_input['catName'])){ echo $last_input['catName']; } ?>" /></td>
								</tr>
								<tr>
									<td>Parent Category</td>
									<td>:</td>
									<td>
										<select name="c_parents">
											<option value="0">None</option>
											<?php
												foreach($categories_data as $category){
													if(count($category) > 1){
														echo "\t\t\t\t\t\t\t\t\t\t\t";
														echo '<option value="'.$category['category_id'].'">'.$category['category_name'].'</option>';
														echo "\n";
													}
												}
											?>
										</select>
									</td>
								</tr>
								<tr>
									<td>Category Image</td>
									<td>:</td>
									<td><input type="file" name="c_images" id="catImages" value="<?php if(isset($last_input) && !empty($last_input['catImages'])){ echo $last_input['catImages']; } ?>"/></td>
								</tr>
								<tr>
									<td>Category Description</td>
									<td>:</td>
									<td style="height: 300px;"><textarea id="elm1" name="c_description" rows="15" cols="80" style="width: 60%" class="tiny_mce"></textarea></td>
								</tr>
                                <tr>
									<td>Published</td>
									<td>:</td>
									<td><input type="checkbox" name="publish" value="1" <?php echo (isset($last_input['published']) && $last_input['published'] == 1 ? 'checked="checked"' : '') ?>/></td>
								</tr>
                                <tr>
									<td>Front Page</td>
									<td>:</td>
									<td><input type="checkbox" name="front" value="1" <?php echo (isset($last_input['front']) && $last_input['front'] == 1 ? 'checked="checked"' : '') ?>/></td>
								</tr>
								<tr>
									<td><input type="submit" name="add" value="Add"></td>
								</tr>
							</table>
					</form>
					<p><span style="font-size: 80%; font-style: italic;">*For 'None' Category, Please upload an image file.</span></p>
                    <script type="text/javascript">
                        
                        //<![CDATA[
                            $(document).ready(function(){
                                
                                $('.tiny_mce').each(function(){
                                    $(this).tinymce({
                                        // Location of TinyMCE script
                                        script_url : 'Resources/js/tiny_mce/jquery.tinymce.js',

                                        // General options
                                        theme : "advanced",
                                        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

                                        // Theme options
                                        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                                        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                                        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                                        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                                        theme_advanced_toolbar_location : "top",
                                        theme_advanced_toolbar_align : "left",
                                        theme_advanced_statusbar_location : "bottom",
                                        theme_advanced_resizing : true,

                                    });
                                });
                            });   
                        //]]>
                    </script>    