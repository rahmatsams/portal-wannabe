                    <div class="container-fluid">
                        <h3 class="well" style="background: rgb(51,122, 183); color: #fff;">Buat PQNC Baru</h3>
                        <?=(isset($error) ? '<div class="alert alert-danger">Please check your Highlighted input below</div>' : '')?>
                        <div class="col-lg-12 well">
                            <div class="row">
                                <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6 form-group <?=(isset($error['title']) ? 'has-error' : '')?>">
                                                <label>Nama Barang & Masalah</label>
                                                <input name="title" type="text" placeholder="Hanya menerima huruf, angka, dan spasi.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['title']) ? "value='{$input['title']}'" : '')?> required>
                                            </div>
                                            <div class="col-lg-3 form-group <?=(isset($error['spare_part']) ? 'has-error' : '')?>">
                                                <label>Jumlah</label>
                                                <input name="spare_part" type="text" placeholder="Hanya menerima huruf, angka, koma, dan spasi.." class="form-control" maxlength="10" <?=(isset($input) && isset($input['spare_part']) ? "value='{$input['spare_part']}'" : '')?> required>
                                            </div>
                                            <div class="col-lg-3 form-group <?=(isset($error['problem_source']) ? 'has-error' : '')?>">
                                                <label>Supplier</label>
                                                <input name="problem_source" type="text" placeholder="Hanya menerima huruf, angka, dan spasi.." class="form-control" maxlength="50" <?=(isset($input) && isset($input['problem_source']) ? "value='{$input['problem_source']}'" : '')?> required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 form-group <?=(isset($error['main_category']) ? 'has-error' : '')?>">
                                                <label>Jenis dan Asal barang</label>
                                                <select name="main_category" id="mainCategory" class="form-control" required>
                                                    <option value="" default></option>
                                                    <?php
                                                        foreach($main_category as $category_form){
                                                            echo "\n<option value=\"{$category_form['category_id']}\" ".(isset($input['main_category']) && $input['main_category'] == $category_form['category_id'] ? 'selected' : '').">{$category_form['category_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6 form-group <?=(isset($error['type']) ? 'has-error' : '')?>">
                                                <label>Penyimpangan</label>
                                                <select name="type" class="form-control" required>
                                                    <?php
                                                        foreach($type as $type_list){
                                                            echo "\n<option value=\"{$type_list['type_id']}\">{$type_list['type_name']}</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Current Condition</label>
                                            <textarea id="ticket-content" name="detail" placeholder="Type here.." class="form-control" rows="6"><?=(isset($input) && isset($input['detail']) ? $input['detail'] : 'Tanggal Terima:
Tanggal Expired/POD: 
Tanggal Buka Kemasan:
Tindakan Perbaikan Outlet:
Lain-lain:')?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5 form-group hidden">
                                                <label>Captcha Confirmation</label>
                                                <div class="g-recaptcha <?=(isset($error) && isset($error['recaptcha']) ? 'alert alert-danger' : '')?>" data-sitekey="6LemwyETAAAAACZCN9RkUJ1D4j_bxeBvu1lgmAZP"></div>
                                                
                                            </div>
                                            <div class="col-lg-7">
                                                <div class="row">
                                                    <div class="col-lg-9 form-group" id="addFile">
                                                        <label>Upload Image (JPEG)</label>
                                                        <input name="image_upload[]" type="file" class="form-control" accept="image/jpeg">
                                                    </div>
                                                    <div class="col-lg-3 form-group">
                                                        <label>&nbsp;</label>
                                                        <button id="addFileButton" type="button" name="addfile" class="btn btn-sm btn-success form-control">Add File</button>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <button type="submit" name="submit" value="kirim" class="btn btn-md btn-primary btn-block form-control">Submit Ticket</button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    