<div class="container-fluid">
    <div class="">
        <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Advanced Search
        </a>

        <div class="collapse collapse-well" id="collapseExample">
                <form id="advanceSearch">
                    <div class="col-md-12 well">
                        <div class="row">
                            <div class="col-sm-3 form-group">
                                <label>Judul Tiket</label>
                                <input id="ticketTitle" name="title" type="text" placeholder="Masukkan judul.." class="form-control" maxlength="25">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Jenis Tiket</label>
                                <select id="ticketType" name="type" class="form-control">
                                    <option value="" default></option>
                                    <option value="1">Question</option>
                                    <option value="2">Problem</option>
                                    <option value="3">Request</option>
                                    <option value="4">Work Request</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Kategori</label>
                                <select name="main_category" id="mainCategory" class="form-control">
                                    <option value="" default></option>
                                    <?php
                                        
                                        foreach($main_category as $category_form){
                                            echo "\n
                                            <option value=\"{$category_form['category_id']}\">{$category_form['category_name']}</option>\n";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Sub Kategori</label>
                                <select name="sub_category" class="form-control" id="subCategory" disabled>
                                    <option value="" default></option>
                                </select>
                            </div>
                        </div>
                        
                        <button type="submit" name="submit" value="kirim" class="btn btn-sm">Search</button>
                    </div>
                </form> 
        </div>
    </div>
    <h2 class="sub-header">Tiket Aktif Saya</h2>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Type</th>
                    <th>Kategori</th>
                    <th>PIC</th>
                    <th>Status</th>
                    <th>Prioritas</th>
                    <th>Tanggal Buat</th>
                    <th>Update</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(is_array($ticket_list) && !empty($ticket_list)){
                        $num = 1;
                        foreach($ticket_list as $result){
                            $date_check = '';
                            
                                $diff_seconds  = strtotime(date("Y-m-d H:i:s")) - strtotime($result['last_update']);
                                $stat = floor($diff_seconds/3600);
                                
                                if($stat >= 24){
                                    if($result['category_id'] == '2' || $result['category_parent_id'] == '2'){
                                        $date_check = "warning";
                                    } elseif($result['category_id'] == '3') {
                                        $date_check = "danger";
                                    }
                                }
                                elseif($stat >= 48){
                                    if($result['category_id'] == '2' || $result['category_parent_id'] == '2'){
                                        $date_check = "danger";
                                    }
                                } else {
                                    $date_check = 'info';
                                }
                            
                                    
                            echo "
                <tr class=\"{$date_check}\">
                    <td>{$num} </td>
                    <td><a href=\"edit_ticket_{$result['ticket_id']}.html\">{$result['ticket_title']}</a></td>
                    <td>{$result['type_name']}</td>
                    <td>{$result['category_name']}</td>
                    <td>{$result['staff_name']}</td>
                    <td>{$result['status_name']}</td>
                    <td>{$result['priority_name']}</td>
                    <td>{$result['submit_date']}</td>
                    <td>{$result['last_update']}</td>
                </tr>";
                            $num++;
                        }
                    } else {
                ?>
                <tr>
                    <td colspan="10">Tiket Kosong</td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
        <?php
            $page = ceil($total/$max_result);
            if($page > 1){
                echo '
            <nav>
                <ul class="pagination pagination-sm">
                    <li>
                        <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
                    </li>';
                for($i=1;$i <= $page;$i++){
                    echo "<li><a href=\"index_page_{$i}.html\">{$i}</a></li>";
                }
                echo '
                <li>
                    <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                </li>
                </ul>
            </nav>';
            }
        ?>
        
    </div>
</div>