<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    
    <title>MRP Ticket Desk</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?="{$css_folder}/bootstrap.min.css"?>" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?="{$css_folder}/ie10-viewport-bug-workaround.css"?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?="{$css_folder}/dashboard.css"?>" rel="stylesheet">
    <link href="<?="{$css_folder}/style.css"?>" rel="stylesheet">
    
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="<?="{$css_folder}/ie8-responsive-file-warning.js"?>"></script><![endif]-->
    <script src="<?="{$css_folder}/ie-emulation-modes-warning.js"?>"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Sushi Tei MRP Ticketdesk</a>
        </div>
      </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <?php
                    echo '
                    <div class="main">';
            ?>
            					
					
					<div class="content-right">
						<h1 class="well">Registrasi Berhasil</h1>
						<div class="well">
							<div class="row">
								Selamat ID MRP Ticketdesk anda sudah dapat digunakan.<br/>
                                Nama user : <?=$user_name?>
                                Password  : <?=$password?>
							</div>
						</div>
					</div>
            </div>
        </div>
    </div>
</body>
</html>
