<?php
class Admin extends Controller
{
    public function accessRules()
	{
		return array(
			array('Deny', 
				'actions'=>array('index'),
				'groups'=>array('Guest'),
			),
            array('Allow', 
				'actions'=>array('index','exportbyuser', 'store_index', 'store_insert', 'store_edit'),
				'groups'=>array('Super Admin','Administrator'),
			),
            array('Allow', 
				'actions'=>array('index'),
				'groups'=>array('Admin QA'),
			),
            array('Allow', 
				'actions'=>array('exportbyuser', 'phpExcel'),
				'groups'=>array('Ticket Users', 'Admin QA'),
			),
		);
	}
	
    function index()
    {
        $data = array(
			'menu'=>'adminmenu',
			'_mySession' => $this->_mySession,
		);
        $this->load->template('admin/index',$data);
    }
	
	function exportbyuser()
	{
		$data = $this->load->model('Tickets');
		$option = array(
			'file_name' => 'PQNC_Report',
			'context_title' => 'Report PQNC'
		);
		$query_option = array(
            'order_by' => 'submit_date',
            'order' => 'DESC'
        );
        if(empty($this->_mySession['last_query']) || !is_array($this->_mySession['last_query'])){
            if($this->_mySession['role'] > 5){
                $query = array();
            }else{
                $query = array(
                    'ticket_user' => $this->_mySession['userid']
                );
            }
            
        }else{
            $query = $this->_mySession['last_query'];
        }
		$this->phpExcel($data->showAllTicketFiltered($query, $query_option), $option);
        #print_r($data->showAllTicketFiltered($query, $query_option));
        #exit();
	}
	
    function phpExcel($ticket_result, $option)
    {
		$objPHPExcel = $this->load->lib('PHPExcel');
		$objPHPExcel->getProperties()->setCreator("Sushitei IT")
							 ->setLastModifiedBy("Sushitei IT")
							 ->setTitle("PQNC")
							 ->setSubject("PQNC QA")
							 ->setDescription("Hasil export PQNC.")
							 ->setKeywords("PQNC QA Export")
							 ->setCategory("Export PQNC");

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1')
					->setCellValue('A1', $option['context_title'])
                    ->mergeCells('A3:A4')
					->setCellValue('A3', 'No.')
                    ->mergeCells('B3:C3')
                    ->setCellValue('B3', 'Pemohon')
					->setCellValue('B4', 'Nama')
					->setCellValue('C4', 'Outlet')
                    ->mergeCells('D3:G3')
                    ->setCellValue('D3', 'Registrasi Tiket')
					->setCellValue('D4', 'No Tiket')
					->setCellValue('E4', 'Tanggal Registrasi')
                    ->setCellValue('F4', 'Tanggal Respon')
					->setCellValue('G4', 'Waktu Respon')
                    ->mergeCells('H3:L3')
                    ->setCellValue('H3', 'Barang')
					->setCellValue('H4', 'Nama Barang & Masalah')
                    ->setCellValue('I4', 'Jenis/Asal Barang')
                    ->setCellValue('J4', 'Penyimpangan')
					->setCellValue('K4', 'Jumlah Barang')
                    ->setCellValue('L4', 'Kondisi Saat Ini')
                    ->mergeCells('M3:N3')
                    ->setCellValue('M3', 'Analisa Permasalahan')
					->setCellValue('M4', 'Supplier')
                    ->setCellValue('N4', 'PIC')
                    ->mergeCells('O3:O4')
					->setCellValue('O3', 'Status Penanganan')
                    ->mergeCells('P3:Q3')
                    ->setCellValue('P3', 'Waktu')
					->setCellValue('P4', 'Aktual')
                    ->setCellValue('Q4', 'Durasi');

		// Miscellaneous glyphs, UTF-8
		if(is_array($ticket_result) && !empty($ticket_result)){
			$no = 1;
			$row = 5;
			$status = array('Unknown','Open','On Progress','Pending','Need Vendor','Solved','Closed');
			$priority = array('Unknown','ASAP','High','Medium','Low');
			foreach($ticket_result as $result){
                $resolve_date = ($result['resolved_date'] != '0000-00-00 00:00:00' ? date('m/d/Y', strtotime($result['resolved_date'])) : '');
                $submit_date = date('m/d/Y', strtotime($result['submit_date']));
                #$last_update = ($result['last_update'] != '0000-00-00 00:00:00' ? date('m/d/Y H:i:s', strtotime($result['last_update'])) : '');
                $assign_date = ($result['assign_time'] != '' ? date('m/d/Y', strtotime($result['assign_time'])) : '');
				$objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A{$row}", $no)
                            ->setCellValue("B{$row}", $result['display_name'])
                            ->setCellValue("C{$row}", $result['store_name'])
                            ->setCellValue("D{$row}", $result['ticket_id'])
                            ->setCellValue("E{$row}", $submit_date)
                            ->setCellValue("F{$row}", $assign_date)
                            ->setCellValue("G{$row}", "=F{$row}-E{$row}")
                            ->setCellValue("H{$row}", $result['ticket_title'])
                            ->setCellValue("I{$row}", $result['category_name'])
                            ->setCellValue("J{$row}", $result['type_name'])
                            ->setCellValue("K{$row}", $result['spare_part'])
                            ->setCellValue("L{$row}", $result['content'])
                            ->setCellValue("M{$row}", $result['problem_source'])
                            ->setCellValue("N{$row}", $result['staff_name'])
                            ->setCellValue("O{$row}", $result['status_name'])
                            ->setCellValue("P{$row}", $resolve_date)
                            ->setCellValue("Q{$row}", "=P{$row}-E{$row}");
				$no++;
				$row++;
			}
		} else {
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:J4')
					->setCellValue('A4', "Tiket kosong");
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('PQNC Report');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename='{$option['file_name']}.xlsx'");
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
    }
    
    function store_index()
    {
        
        $model_store = $this->load->model('Store');
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'store_name',
            'order' => 'ASC'
        );
        $data = array(
            '_mySession' => $this->_mySession,
            'store' => $model_store->getAllStore($query_option),
            'total' => $model_store->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
        );
        $this->load->template('admin/index_store', $data);
    }
    
    function store_insert(){
        $model_store = $this->load->model('Store');
        $data = array(
            '_mySession' => $this->_mySession,
            'location_list' => $model_store->getAllActiveLocation()
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'create_store'){
            $input = $this->load->lib('Input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->newStore($store)){
                    header("Location: admin_store.html");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/add_store', $data);
            }
        }else{
            $this->load->template('admin/add_store', $data);
        }
        
    }
    
    function store_edit(){
        $model_store = $this->load->model('Store');
        $input = $this->load->lib('Input');
        $data = array(
            '_mySession' => $this->_mySession
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_store'){
            $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->editStore($store, array('store_id' => $_GET['id']))){
                    header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/edit_store', $data);
            }
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                if($input->validate()){
                    $data['store'] = $model_store->getStoreBy(array('store_id' => $_GET['id']));
                    if(is_array($data['store']) && count($data['store']) > 1){
                        $data['location_list'] = $model_store->getAllActiveLocation();
                        $this->load->template('admin/edit_store', $data);
                    }
                }
                
            }
        }
        
    }
}
?>