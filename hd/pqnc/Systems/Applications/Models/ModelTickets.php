<?php
/*
* Model for Ticketdesk
*/
class ModelTickets extends Model
{
	public $tableName = "pqnc_main";
    public $lastInsertID;
	
	function createTicket($form = array(), $log = array()){
		$this->beginTransaction();
		try{
			$this->insertQuery('pqnc_main',$form);
			$log['ticket_id'] = $this->lastInsertId();
			$this->lastInsertID = $this->lastInsertId();
			$this->insertQuery('pqnc_log',$log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
    function editTicket($form = array(), $log = array()){
        $table_ticket = 'pqnc_main';
        $table_log = 'pqnc_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table_ticket, $form['input'], $form['where']);
			$this->insertQuery($table_log, $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
	
    function showAllTicket($query_option)
    {
        $query_string = "SELECT 
        ticket_id, 
        ticket_title, 
        ticket_type, 
        pqnc_type.type_name, 
        pqnc_category.category_name, 
        spare_part, 
        category_id, 
        category_parent_id, 
        pqnc_status.status_name, 
        ticket_priority.priority_name, 
        impact, 
        content, 
        status, 
        st.user_name, 
        st.display_name, 
        store_name, 
        assigned_staff, 
        astaff.user_name AS staff, 
        astaff.display_name AS staff_name, 
        ticket_user, 
        cname.display_name AS creator_name, 
        submit_date,
        last_update, 
        problem_source, 
        resolved_date, 
        resolved_solution, 
        sid.display_name AS resolver_name, 
        ticketlog_content ";
		$query_string .= "FROM pqnc_main LEFT JOIN pqnc_type ON pqnc_main.ticket_type=pqnc_type.type_id ";
		$query_string .= "LEFT JOIN pqnc_status ON pqnc_main.status=pqnc_status.status_id ";
		$query_string .= "LEFT JOIN pqnc_category ON pqnc_main.category=pqnc_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON pqnc_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON pqnc_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM pqnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON pqnc_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN (SELECT pqnc_log.ticket_id AS aidee,MAX(ticketlog_time) AS solve_time, ticketlog_content, pqnc_log.user_id, sushitei_portal.portal_user.display_name FROM pqnc_log LEFT JOIN sushitei_portal.portal_user ON pqnc_log.user_id=sushitei_portal.portal_user.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return' GROUP BY aidee) sid ON pqnc_main.ticket_id=sid.aidee ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON pqnc_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON pqnc_main.ticket_user=st.user_id ";
        
        $count_query = "SELECT COUNT(*) AS row_total FROM pqnc_main";
        
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
    function showAllTicketFiltered($filter, $query_option)
    {
        $query_string = "SELECT 
        ticket_id, 
        ticket_title, 
        ticket_type, 
        pqnc_type.type_name, 
        pqnc_category.category_name,
        category_id, 
        spare_part, 
        pqnc_status.status_name, 
        ticket_priority.priority_name, 
        impact, 
        content, 
        status, 
        st.user_name, 
        st.display_name, 
        store_name,
        #st.store_id, 
        assigned_staff, 
        astaff.user_name AS staff, 
        astaff.display_name AS staff_name, 
        ticket_user, 
        cname.display_name AS creator_name, 
        submit_date,
        last_update, 
        problem_source, 
        resolved_date, 
        resolved_solution,
        sid.display_name AS resolver_name, 
        ticketlog_content ";
		$query_string .= "FROM pqnc_main LEFT JOIN pqnc_type ON pqnc_main.ticket_type=pqnc_type.type_id ";

		$query_string .= "LEFT JOIN pqnc_status ON pqnc_main.status=pqnc_status.status_id ";

		$query_string .= "LEFT JOIN pqnc_category ON pqnc_main.category=pqnc_category.category_id ";

		$query_string .= "LEFT JOIN ticket_priority ON pqnc_main.priority=ticket_priority.priority_id ";

		$query_string .= "LEFT JOIN (SELECT user_id,user_name,display_name FROM sushitei_portal.portal_user) astaff ON pqnc_main.assigned_staff=astaff.user_id ";

        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM pqnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON pqnc_main.ticket_id=atime.tid ";

        $query_string .= "LEFT JOIN (SELECT pqnc_log.ticket_id AS aidee,MAX(ticketlog_time) AS solve_time, ticketlog_content, pqnc_log.user_id, sushitei_portal.portal_user.display_name FROM pqnc_log LEFT JOIN sushitei_portal.portal_user ON pqnc_log.user_id=sushitei_portal.portal_user.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return' GROUP BY aidee) sid ON pqnc_main.ticket_id=sid.aidee ";

        $query_string .= "LEFT JOIN (SELECT user_id,display_name FROM sushitei_portal.portal_user) cname ON pqnc_main.ticket_creator=cname.user_id ";

        $query_string .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON pqnc_main.ticket_user=st.user_id ";

        $num = 0;
        $count_query = "SELECT COUNT(*) AS row_total FROM pqnc_main ";
        $count_query .= "LEFT JOIN (SELECT ps.store_id, ps.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) AS st ON pqnc_main.ticket_user=st.user_id ";
        if(count($filter) > 0){
            $query_string .= "WHERE ";
            $count_query .= "WHERE ";
        }
        foreach($filter as $cat => $arr){
            if($cat != 'date_end' && !empty($arr['value'])){
                if(!empty($cat) && $num > 0){
                    $query_string .= "AND ";
                    $count_query .= "AND ";
                } 
                if($cat == 'submit_date' && !empty($filter['date_end']['value'])){
                    $query_string .= "submit_date BETWEEN '{$arr['value']} 00:00:00' AND '{$filter['date_end']['value']} 23:59:00' ";
                    $count_query .= "submit_date BETWEEN '{$arr['value']} 00:00:00' AND '{$filter['date_end']['value']} 23:59:00' ";                    
                }elseif($cat == 'get_all'){
                    $query_string .= "category IN (SELECT category_id FROM ticket_category WHERE category_parent_id='{$arr['value']}') ";
                    $count_query .= "category IN (SELECT category_id FROM ticket_category WHERE category_parent_id='{$arr['value']}') ";
                }else{
                    $query_string .=  "{$cat} {$arr['type']} '{$arr['value']}' ";
                    $count_query .= "{$cat} {$arr['type']} '{$arr['value']}' ";
                }
            }
            $num++;
        }
        #exit($query_string);
        $result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
	function showMyActiveTicket($my_id, $query_option)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, pqnc_type.type_name, pqnc_category.category_name, spare_part, category_id, category_parent_id, pqnc_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, sid.display_name AS resolver_name, ticketlog_content ";
		$query_string .= "FROM pqnc_main LEFT JOIN pqnc_type ON pqnc_main.ticket_type=pqnc_type.type_id ";
        $query_string .= "LEFT JOIN pqnc_status ON pqnc_main.status=pqnc_status.status_id ";
		$query_string .= "LEFT JOIN pqnc_category ON pqnc_main.category=pqnc_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON pqnc_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON pqnc_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM pqnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON pqnc_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN (SELECT pqnc_log.ticket_id AS aidee,MAX(ticketlog_time) AS solve_time, ticketlog_content, pqnc_log.user_id, sushitei_portal.portal_user.display_name FROM pqnc_log LEFT JOIN sushitei_portal.portal_user ON pqnc_log.user_id=sushitei_portal.portal_user.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return' GROUP BY aidee) sid ON pqnc_main.ticket_id=sid.aidee ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON pqnc_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id) AS st ON pqnc_main.ticket_user=st.user_id ";
		$query_string .= "WHERE ticket_user='{$my_id}'";
        $count_query = "SELECT COUNT(*) AS row_total FROM pqnc_main WHERE ticket_user='{$my_id}'";
		
        $result = $this->pagingQuery($query_string,$count_query,$query_option);
        return $result;
    }
    
    function showTicketBy($input)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, pqnc_type.type_name, pqnc_category.category_name, spare_part, category_id, category_parent_id, pqnc_status.status_name, ticket_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, sid.display_name AS resolver_name, st.email ";
		$query_string .= "FROM pqnc_main LEFT JOIN pqnc_type ON pqnc_main.ticket_type=pqnc_type.type_id ";
        $query_string .= "LEFT JOIN pqnc_status ON pqnc_main.status=pqnc_status.status_id ";
		$query_string .= "LEFT JOIN pqnc_category ON pqnc_main.category=pqnc_category.category_id ";
		$query_string .= "LEFT JOIN ticket_priority ON pqnc_main.priority=ticket_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON pqnc_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM pqnc_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON pqnc_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN (SELECT pqnc_log.ticket_id AS aidee, pqnc_log.user_id, sushitei_portal.portal_user.display_name FROM pqnc_log LEFT JOIN sushitei_portal.portal_user ON pqnc_log.user_id=sushitei_portal.portal_user.user_id WHERE ticketlog_type='Reject' OR ticketlog_type='Release' OR ticketlog_type='Return') sid ON pqnc_main.ticket_id=sid.aidee ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON pqnc_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name, email FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id) AS st ON pqnc_main.ticket_user=st.user_id ";
        if(isset($input['my_id'])){
            $query_string .= "WHERE ticket_user=:my_id AND ticket_id=:ticket_id";
        } else {
            $query_string .= "WHERE ticket_id=:ticket_id";
        }
        $result = $this->fetchSingleQuery($query_string, $input);
        
		return $result;
    }
    
    function getLog($input){
        $query_string = "SELECT ticketlog_title, pqnc_log.user_id, display_name, ticketlog_type, ticketlog_content, ticketlog_time FROM pqnc_log ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user ON pqnc_log.user_id=sushitei_portal.portal_user.user_id ";
        $query_string .= "WHERE ticket_id = :ticket_id AND ticketlog_show = 1 ORDER BY ticketlog_time DESC";
        $result = $this->fetchAllQuery($query_string, $input);
		return $result;
    }
    
	function getTicketType()
    {
        $query_string = 'SELECT * FROM pqnc_type';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM pqnc_category WHERE category_parent_id=1';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM pqnc_category WHERE category_parent_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getUploadedFile($query_value)
	{
		$query_string = 'SELECT * FROM pqnc_upload WHERE ticket_id=:ticket_id';
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getAllActiveStore()
    {
        $query_string = "SELECT * FROM sushitei_portal.portal_store WHERE store_status=1 ORDER BY store_name ASC";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getMailRecipient()
    {
        $query_string = 'SELECT pu.email,pu.display_name FROM sushitei_portal.portal_user pu ';
        $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
        $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
        $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="receiveEmail" AND pp.permission_site='.getConfig('site_number').' GROUP BY pu.email';
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function myOutlet($input){
        $query_string = "SELECT store_name FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id WHERE user_id=:user_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    function getAllPQNC(){
        $query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE role_id=9";
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
}