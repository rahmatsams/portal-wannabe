<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title><?php echo getConfig('site_name'); ?></title>
        <link rel="stylesheet" href="<?php echo $template_path; ?>css/style.css" type="text/css" media="all" />
        <script type="text/javascript" src="<?php echo $template_path; ?>js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $template_path; ?>js/jquery-ui-1.8.17.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo $template_path; ?>js/script.js"></script>
        <link rel="stylesheet" href="<?php echo $template_path; ?>css/ui-lightness/jquery-ui-1.8.17.custom.css" type="text/css" media="all" />
        <?php
            echo (isset($_option['slider']) && $_option['slider'] == 1 ? "<script type=\"text/javascript\" src=\"{$template_path}js/s3Slider.js\"></script>" : '' );
            echo (isset($_option['tinymce']) && $_option['tinymce'] == 1 ? "<script type=\"text/javascript\" src=\"{$template_path}js/tiny_mce/jquery.tinymce.js\"></script>
        <script type=\"text/javascript\" src=\"{$template_path}js/tiny_mce/tiny_mce.js\"></script>" : '' 
            ); 
        ?>
        
    </head>

    <body>
        <div class="container">
        
            <div class="header">
                <div id="top-menu">
                        <ul><?php
                            $menulist = array(
                                'Home' => controllerUrl('admin'),
                                'Products'  =>  array(
                                    'Manage Products' => controllerUrl('AdminProduct'),
                                    'Add Products' => controllerUrl('AdminProduct','add_products')
                                ),
                                'Product Categories'  =>  array(
                                    'Manage Categories' => controllerUrl('AdminCategory'),
                                    'Add Categories' => controllerUrl('AdminCategory','add_categories')
                                ),
                                'News'  =>  array(
                                    'Manage News' => controllerUrl('AdminNews'),
                                    'Add News' => controllerUrl('AdminNews','add_news')
                                ),
                                'Quotations'  =>  array(
                                    'Manage Quotations' => controllerUrl('AdminQuotation'),
                                    'Add Quotations' => controllerUrl('AdminQuotation','add_quotation')
                                ),
                                'Events'  =>  array(
                                    'Manage Events' => controllerUrl('AdminEvent'),
                                    'Add Events' => controllerUrl('AdminQuotation','add_event')
                                ),
                                'Guestbook'  =>  array(
                                    'Manage Guestbook' => controllerUrl('AdminGuestbook')
                                )
                            );
                            foreach($menulist as $menu => $url){
                                if(is_array($url)){
                                    echo "
                            <li>
                                <a href=\"#\" class=\"has-child\">{$menu}</a>
                                <div class=\"sub-menu\">
                                    <ul>";
                                    foreach($url as $menu2 => $url2){
                                        echo "
                                        <li><a href=\"{$url2}\">{$menu2}</a></li>";
                                    }
                                echo '
                                    </ul>
                                </div>
                            </li>
                                ';
                                } else {
                                    echo "
                            <li><a href=\"{$url}\" class=\"top-menu-list ". ($url == "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}" ? 'active' : '') ."\">{$menu}</a></li>";
                                }
                            }
                        ?>
                        
                    </ul>
               
                </div>  
            </div>
            <div class="clear-float">&nbsp;</div>
            <div class="content">
                <?php $this->view($_action,$_passed_var); ?>
            </div>
            
            <div class="clear-float">&nbsp;</div>
            <div class="footer">
                <br/>Copyright &copy; PTCS 2012
            </div>
        </div>
    </body>
</html>