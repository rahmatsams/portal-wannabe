$(document).ready(function(){
    $("#mainCategory").change(function(event){
        event.preventDefault();
        changeValue();
    });
    $("#resolveMainCat").change(function(event){
        event.preventDefault();
        changeValueSub();
    });
    $("#setdatebutton").click(function(event){
        event.preventDefault();
        var text = "Set Date";
        var text2 = "Set active and end date.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleDate();
    });
    var detail_height = $("#tickettabledetail").height();
    $("#tickettabledesc").css("min-height", detail_height);
    $("#commentbutton").click(function(event){
        var text = "Comment";
        var text2 = "Comment on ticket as additional note.";
        var button_name = $(this).html();
        if(category_set == 0 && category_set2 == 1){
            toogleCategory();
        }
        showComment(text, text2, button_name);
    });
    $("#assignbutton").click(function(event){
        var text = "Assign";
        var text2 = "Assign the ticket to the listed staff.";
        var button_name = $(this).html();
        if(category_set == 0){
            toogleCategory();
        }
        showComment(text, text2, button_name);
        toggleAssign();
    });
    $("#takeoverbutton").click(function(event){
        var text = "Take Over";
        var text2 = "Take over this ticket to assign it to yourself.";
        var button_name = $(this).html();
        if(category_set == 0){
            toogleCategory();
        }
        showComment(text, text2, button_name);
    });
    $("#needsparebutton").click(function(event){
        var text = "Need Spare-part";
        var text2 = "Mark this ticket, need to order sparepart.";
        var button_name = $(this).html();
        if(category_set == 0){
            toogleCategory();
        }
        showComment(text, text2, button_name);
        toggleSparePart();
    });
    $("#cancelsparebutton").click(function(event){
        var text = "Cancel Spare-part";
        var text2 = "Cancel order sparepart.";
        var button_name = $(this).html();
        if(category_set == 0){
            toogleCategory();
        }
        showComment(text, text2, button_name);
    });
    $("#donesparebutton").click(function(event){
        var text = "Sparepart Done";
        var text2 = "Mark this ticket, the ordered sparepart has arrived.";
        var button_name = $(this).html();
        if(category_set == 0){
            toogleCategory();
        }
        showComment(text, text2, button_name);
    });
    $("#resolvebutton").click(function(event){
        var text = "Solve Ticket";
        var text2 = "Solve your ticket.";
        var button_name = $(this).html();
        if(category_set == 0){
            toogleCategory();
        }
        showComment(text, text2, button_name);
        toggleResolve();
    });
    $("#closebutton").click(function(event){
        var text = "Close ticket";
        var text2 = "Close ticket after assigned staff solve it.";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });

    
    $("#forcebutton").click(function(event){
        var text = "Force Close";
        var text2 = "Force close the ticket";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    
    $("#reopenbutton").click(function(event){
        var text = "Re-Open Ticket";
        var text2 = "Re-open closed ticket";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
    });
    $("#cancelstatus").click(function(event){
        showActivity();
        if($("#submitstatus").html() == "Edit"){
            toggleEdit();
        }
        if($("#submitstatus").html() == "Assign"){
            toggleAssign();
        }
        if($("#submitstatus").html() == "Resolve"){
            toggleResolve();
        }
        if($("#submitstatus").html() == "Set Date"){
            toggleDate();
        }
        if(!$('#categoryInput').hasClass("hidden")){
            $("#categoryInput").toggleClass("hidden");
        }
        $("#tickettitle").html("Ticket Command");
    });
    $("#editbutton").click(function(event){
        var text = "Edit Ticket";
        var text2 = "Edit the ticket detail";
        var button_name = $(this).html();
        showComment(text, text2, button_name);
        toggleEdit();
    });
    $("#updateticketstatus").submit(function(event){
        event.preventDefault();
        var me = $(this);
        editTicket(me);
        
    });
    
});