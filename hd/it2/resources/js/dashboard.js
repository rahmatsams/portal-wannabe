
    jQuery(document).ready(function(){
        jQuery("#mainCategory").change(function(event){
            event.preventDefault();
            changeValue();
        });
        jQuery(document).on("click", ".page-navigate", function(event){
            event.preventDefault();
            data_id = jQuery(this).attr("data-id");
            jQuery("#ticketPage").val(data_id);
            var me = jQuery(this);
            searchForm(me);
            
        });
        jQuery("#advanceSearch").submit(function(event){
            // cancels the form submission
            jQuery("#ticketPage").val("1");
            event.preventDefault();
            var me = jQuery(this);
            searchForm(me);
        });
        
        jQuery("#sortResult").change(function(event){
            // cancels the form submission
            jQuery("#ticketPage").val("1");
            event.preventDefault();
            var me = jQuery(this);
            searchForm(me);
        });
        
        function searchForm(me){
            title=jQuery("#ticketTitle").val();
            status=jQuery("#ticketStatus").val();
            main=jQuery("#mainCategory").val();
            sub=jQuery("#subCategory").val();
            date_start=jQuery("#ticketDateFirst").val();
            date_end=jQuery("#ticketDateLast").val();
            store=jQuery("#storeInput").val();
            page=jQuery("#ticketPage").val();
            sort=jQuery("#sortResult").val();
            pic=jQuery("#picInput").val();
            if ( me.data("requestRunning") ) {
                jQuery("#advanceSearch").attr("disabled", "disabled");
                return;
            }

            me.data("requestRunning", true);
            jQuery.ajax({
                type: "POST",
                url: "ajax_search.html",
                data: "sort="+sort+"&page="+page+"&ticket_title="+title+"&status="+status+"&main_category="+main+"&sub_category="+sub+"&date_start="+date_start+"&date_end="+date_end+"&store="+store+"&pic="+pic,
                success: function(html){
                    jQuery("#listTable").html(html);
                },
                complete: function() {
                    jQuery.ajax({
                        type: "POST",
                        url: "ajax_search_page.html",
                        success: function(html){
                            jQuery("#pagingBottom").html(html);
                        },
                        complete: function() {
                            me.data("requestRunning", false);
                            jQuery("#advanceSearch").removeAttr("disabled");
                        },
                    });
                },
            });
            
        }
        
        function changeValue(){
            main_category=jQuery("#mainCategory").val();
            if(main_category == 0){
                jQuery("#subCategory").html("<option value=\'0\' selected></option>");
                jQuery("#subCategory").attr("disabled", true);
            }else{
                jQuery.ajax({
                    type: "POST",
                    url: "ajax_subcategory.html",
                    data: "category="+ main_category,
                    success: function(json){
                        
                        if(json=="false")    {
                            jQuery("#subCategory").html("<option value=\'0\' selected></option>");
                            jQuery("#subCategory").attr("disabled", true);
                        } else {
                            var data = jQuery.parseJSON(json);
                            jQuery("#subCategory").html("");
                            jQuery("#subCategory").append("<option value=\'999\'>All</option>");
                            jQuery(data).each(function() {
                                var jsondata = "<option value=\'"+this.category_id+"\'>"+this.category_name+"</option>";
                                jQuery("#subCategory").append(jsondata);
                            });
                            jQuery("#subCategory").attr("disabled", false);
                        }
                    },
                });
            }
            return false;
        }
    });