<?php
    class Controller extends SISTER{
        protected $_controller;
        protected $_action;
        protected $_init;
        protected $load;
        protected $_mySession;
        protected $_header;
        protected $_accessRules;
        protected $_baseUrl;
        protected $site;
        protected $skipCheck = array();
        
        // Controller Class Constructor
        public function __construct($cons_con,$cons_act)
        {
            $this->site = getConfig('site_number');
            $this->_controller = $cons_con;
            $this->_action = $cons_act;
            // Load - Load Class as child Function of this Controller
            $this->load = new Load($this->_controller);
            if(!__MT_MODE){
                
                session_start();
                if(!isset($_SESSION) || !isset($_SESSION['group']) || !isset($_SESSION['__last_generate'])) {
                    $this->setSession('user_name','Guest');
                    $this->setSession('group','Guest');
                    $this->setSession('attempt',0);
                    $this->setSession('role',1);
                }
                $this->_mySession = $this->getSession();
                
                if(method_exists($this, $cons_act)) {
                    $allowAccess = 1;
                    if($this->checkAccess() > 0) {
                        $this->_init = $this->$cons_act();
                    } else {
                        $this->showError(3);
                    }
                  
                } else {
                    $this->showError(2);
                }
                
                $this->setSession('last_url', "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
                if(count($this->getFlashData()) >= 1) {
                    $flashdata = $this->getFlashData();
                    ($flashdata['last_uri'] != $_SERVER['REQUEST_URI'] ? $this->unsetFlashData() : '');
                }
            }else{
                $this->showError(4);
            }
            
        }
        // Check Access
        protected function checkAccess()
        {
            $allowed = 0;
            if(array_key_exists($this->site, $_SESSION['site'])){
                if(isset($_COOKIE['ci_session']) && $_COOKIE['ci_session'] != $_SESSION['ci_session']){
                    $this->showError(4);
                }
                if(is_array($this->_accessRules)) {
                    foreach($this->_accessRules as $rules){
                        if($rules[0] == 'Allow'){
                            foreach($rules['actions'] as $rule){
                                if($rule == $this->_action && isset($rules['users']) && (in_array($this->_mySession['user_name'], $rules['users'], true) || in_array('*', $rules['users'], true))){
                                    $allowed = 1;
                                } elseif($rule == $this->_action && isset($rules['role']) && $rules['role'] > $this->_mySession['role']){
                                    $allowed = 1;
                                } elseif($rule == $this->_action && isset($rules['groups']) && (in_array($this->_mySession['group'], $rules['groups'], true) || in_array('*', $rules['groups'], true))){
                                    $allowed = 1;
                                }
                            }
                        } elseif($rules[0] == 'Deny') {
                            foreach($rules['actions'] as $rule){
                                if($rule == $this->_action && isset($rules['users']) && (in_array($this->_mySession['user_name'], $rules['users'], true) || in_array('*', $rules['users'], true))){
                                    $allowed = 0;
                                } elseif($rule == $this->_action && isset($rules['role']) && $rules['role'] > $this->_mySession['role']){
                                    $allowed = 0;
                                } elseif($rule == $this->_action && isset($rules['groups']) && (in_array($this->_mySession['group'], $rules['groups'], true) || in_array('*', $rules['groups'], true))){
                                    $allowed = 0;
                                }
                            }
                        }
                    }
                } else{
                    $allowed = 1;
                }
            }
            return $allowed;
        }
        
        
        // Show Controller Error
        protected function showError($code)
        {
            switch($code) {
                case 1:
                    exit(header("Location: ".getConfig('base_domain')));
                break;
                case 2:
                    $this->load->view('notfound',array(),'error');
                break;
                case 4:
                    exit(header("Location: ".getConfig('base_domain')."/user/logout"));
                break;
                default:
                    exit(header("HTTP/1.0 404 Not Found"));
            }
        }
        protected function isGuest()
        {
            if($this->_mySession['user_name'] == 'Guest') {
                return 1;
            } else {
                return 0;
            }
        }
        
        protected function isAdmin()
        {
            if($this->_mySession['site'][$this->site] == 1) {
                return 1;
            } else {
                return 0;
            }
        }
        
        protected function setFlashData($input)
        {
            foreach($input as $data => $value) {
                $_SESSION['flash_data'][$data] = $value; 
            }
            $_SESSION['flash_data']['last_uri'] = $_SERVER["REQUEST_URI"];
        }
        protected function getFlashData()
        {
            if(isset($_SESSION['flash_data'])) {
                return $_SESSION['flash_data'];
            }else{
                return array();
            }
        }
        protected function unsetFlashData()
        {
            if(isset($_SESSION['flash_data'])) {
                unset($_SESSION['flash_data']);
            }
        }
        
        protected function setSession($name, $value = 0)
        {
            if(!$value && is_array($name)){
                foreach($name as $key => $val){
                    $_SESSION[$key] = $val;
                }
            }else{
                $_SESSION[$name] = $value;
            }
            
        }
        
        protected function unsetSession($name)
        {
            if(isset($_SESSION[$name])) {
                unset($_SESSION[$name]);
            }
        }
        
        protected function getSession($name = false) {
            if(!$name){
                if(isset($_SESSION)) {
                    return $_SESSION;
                }else{
                    return 0;
                }
            }else{
                if(isset($_SESSION[$name])) {
                    return $_SESSION[$name];
                }else{
                    return 0;
                }
            }
        }
        
    }
// End Controller Class