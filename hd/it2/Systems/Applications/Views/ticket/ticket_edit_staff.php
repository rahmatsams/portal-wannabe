            
            <div class="content mt-3">
                <div class="card-deck m-2 mb-4">
                    <div class="card text-white bg-flat-color-5">
                        <div class="card-header"><strong class="card-title">Status</strong></div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="indexContent" class="table">
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Ticket ID</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['ticket_id']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Ticket Title</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['ticket_title']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Type</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['type_name']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Status</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['status_name']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Category</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['category_name']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Owner</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['display_name']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">PIC</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['staff_name']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Affect Customer</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=($ticket['impact'] == 1 ? 'Yes' : 'No')?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Submit Date</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['submit_date']?></td>
                                    </tr>
                                    <tr>
                                        <td class="pt-1 pb-1 pl-3 pr-3">Last Update</td>
                                        <td class="pt-1 pb-1 pl-3 pr-3"><?=$ticket['last_update']?></td>
                                    </tr>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            