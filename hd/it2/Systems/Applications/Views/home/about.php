                    
                    <h3 class="black-font">About <b>PT. Control Systems</b></h3>
					<p class="about-desc"><img src="Resources/images/home2.gif"><b>PT. Control Systems</b> was established in 1980 as a company committed to providing the Process Industries with value added services in the field of process Control and Instrumentation. Our company is fully staffed to assist you in all phase of process control and instrumentation from planning stages through engineering, project management, product and system integration, site supervision and after sales support. One of our missions is to provide solutions to improve your business results by improving throughput, availability and quality, and by reducing costs for engineering, commissioning, operation and maintenance, safety, health and environment. 
					<b>PT. Control Systems</b> has gained a reputation for expertise in process control and instrumentation which is reflected by the confidence of our long list of repeat customers as well as by internationally famous company that have chosen us to represent them in Indonesia such as Emerson Process Management.
					</p>
                    <div class="sub-about">
                        <h4>ENGINEERING AND SUPPORT CAPABILITIES</h4>
						<ul class="about-lists">
							<li>Concept &amp; Design</li>
							<li>Engineering Application</li>
							<li>Project Management</li>
							<li>Product &amp; System Integration</li>
							<li>Start-up &amp; Commissioning</li>
							<li>Loop Checking</li>
							<li>Training (inhouse / at-site)</li>
							<li>Maintenance Contract</li>
							<li>Asset Management &amp; Predictive Maintenance</li>
							<li>Remote Diagnostic</li>
						</ul>
					</div>
                    <div class="sub-about">
                        <h4>PROJECT HIGHLIGHTS</h4>
						<ul class="about-lists">
							<li>DCS &amp; SCADA Selected Project Experiences</li>
							<li>Pinang Gas Plant Control Project (CPI)</li>
							<li>Balam &amp; Bangko Water Injection DCS &amp; SCADA (CPI)</li>
							<li>Hydrocyclone Skid Control System (Baker Process)</li>
							<li>Thermoflux Control System (Unocal)</li>
							<li>DSF Area 10 &amp; 11 Steam Quality Monitoring &amp; Control (CPI)</li>
							<li>DCS for CO2 Removal Plant (Pertamina DOH JBB)</li>
							<li>SCADA for Peciko (Total Indonesie)</li>
							<li>RTU for NSO Platform (ExxonMobil)</li>
							<li>Duri Area 11 Automatic Well Test (CPI)</li>
							<li>DCS for Gas Lift Injection (Unocal)</li>
							<li>DCS for Gas Plant (ConocoPhillips South Jambi)</li>
							<li>DCS for Refinery (TPPI)</li>
							<li>DCS Migration at Papa Platform (BP)</li>
							<li>Control Room Relocation (COPI WNTS)</li>
						</ul>
					</div>
                    <div class="sub-about">
                        <h4>System Integration Selected Project Experiences</h4>
						<ul class="about-lists">
							<li>CEMS (EMOI)</li>
							<li>CEMS including installation at 5 location (COPI)</li>
							<li>Filling Shed meter &amp; control, 12 set (Pertamina Cikampek)</li>
							<li>Filling Shed meter &amp; control, 46 set (Pertamina UPMS Palembang)</li>
							<li>Radar Tank Gauging Upgrade, 23 tanks (Pertamina Balikpapan)</li>
							<li>RTU for water Injection Control (CPI)</li>
							<li>Flow Computer Upgrade (BP)</li>
							<li>Flow Computer Upgrade (Total)</li>
							<li>Flow Computer Upgrade (COPI)</li>
							<li>Meter Recalibration &amp; Installation (COPI)</li>
							<li>Meter Regulating Skid, 16 set (PGN)</li>
							<li>Batch Plant Automation (Monagro)</li>
							<li>Fatty Acid Plant Automation (Ecogreen)</li>
							<li>Melamine Plant Automation (DKM)</li>
						</ul>
					</div>
                    <div class="sub-about">
                        <h4>Selected Metering System Project Experiences</h4>
						<ul class="about-lists">
							<li>Pertamina UP III Plaju Liquid Meter (Turbine)</li>
							<li>Unocal West Seno Gas &amp; Crude Meter &amp; Bidi Prover (USM &amp; PD)</li>
							<li>Pertamina UP II Dumai LPG meter &amp; Bidi Prover (Turbine)</li>
							<li>ExxonMobil Pase B Gas &amp; Condensate Meter (orifice &amp; turbine)</li>
							<li>Petrochina Gas Meter (USM)</li>
							<li>Pertamina UP VII Kasim Crude &amp; White Product Meter &amp; BCP (PD)</li>
							<li>PGN-RAPP Gas Meter (USM)</li>
							<li>Petrochina Salawati Liquid Metering System at FPSO</li>
							<li>TAC Pertamina ? Pertalahan Crude Metering System &amp; BCP at FSO</li>
							<li>Pertamina DOH Sumbagsel Crude Metering System &amp; BCP (PD)</li>
							<li>Pertamina UPIV Cilacap Gasoline Metering System &amp; BCP (Turbine)</li>
							<li>Pertamina UPIV Cilacap Paraxylene Ship Loading System (MMI)</li>
							<li>Pertamina DOH Sumbagsel Gas Metering System (orifice)</li>
							<li>Pertamina UPMS IV Semarang Mobile Proving System (BCP)</li>
							<li>Pertamina UP II Dumai Gasoline Metering System (Turbine)</li>
							<li>Pertamina UP II Dumai Diesel Oil Metering System (Turbine)</li>
						</ul>
					</div>
                    <div class="sub-about">
                        <h4>Maintenance Contract Reference List</h4>
						<ul class="about-lists">
							<li>BP Indonesia Maintenance contract for all instrument (3 years)</li>
							<li>CPI Central Maintenance Workshop Operation (2 years)</li>
							<li>COPI Belanak GC &amp; USM Maintenance contract (2 years)</li>
							<li>COPI Grissik GC Maintenance Contract (2 years)</li>
							<li>COPI Belanak control valve maintenance contract (2 years)</li>
							<li>WNTS DeltaV maintenance contract (2 years)</li>
							<li>CPI SLN all instrument maintenance contract (6 months)</li>
							<li>CPI Stock Utilization (18 months)</li>
							<li>CPI Main Instrument Shop Management (2 years)</li>
							<li>CPI Well Injector Maintenance Contract (2 years)</li>
						</ul>
                    </div>
                    <script type="text/javascript">
                        var t = 0;
                        $('.sub-about').each(function () {
                            $this = $(this);
                            if ( $this.outerHeight() > t ) {
                                t = $this.outerHeight();
                            }
                        });
                        $('.sub-about').height(t);
                        $('.sub-about').css("border","1px solid #848484");
                    </script>
                    