<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ticket extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('edit_ticket','submit_ticket'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('edit_ticket','submit_ticket'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
    
    
    public function submit_ticket()
    {
        $category = $this->load->model('TicketCategory');
        $data = array(
            '_mySession' => $this->_mySession,
            'main_category' => $category->getMainCategory(),
        );
        $option = array(
                'personal_js' => 1,
                'tinymce' => 1,
                'scriptload' => '
        <script type="text/javascript">
            $(function() {
                var tinyMCE = tinymce.init({
                    selector: "textarea",
                    entity_encoding : "raw",
                    theme: "modern",
                    plugins: ["table save"],
                    table_default_attributes: {
                        class: "table table-bordered"
                    }
                });
                $(document).ready(function(){
                    $("#mainCategory").change(function(event){
                        event.preventDefault();
                        changeValue();
                    });
                    $("#onbehalf").change(function(event){
                        event.preventDefault();
                        if($("#onbehalf").prop("checked") == true){
                            changeBehalf();
                        } else {
                            $("#userlist").attr("disabled", true);
                        }
                        
                    });
                    $("#addFileButton").click(function(event){
                        event.preventDefault();
                        num_files = $("#addFile input:file").length;
                        if(num_files < 5){
                            $("#addFile").append("<input name=\"image_upload[]\" type=\"file\" class=\"form-control\" accept=\"image/jpeg\">");
                        }
                    });
                    $("#submitForm").submit(function(event) { //Event on submit
                        tinyMCE.triggerSave(false, true); //Save content to textarea
                        var text=$("#ticket-content").val(); //get content from textarea
                        tinyMCE.activeEditor.setContent($.base64("encode", text));
                        tinyMCE.triggerSave(false, true);
                     });
                });
            })
        </script>
		<script src="https://www.google.com/recaptcha/api.js"></script>
        ');
        if(isset($_POST['submit']) && $_POST['submit'] == 'kirim' && isset($_POST['g-recaptcha-response'])){
			$captcha = $_POST['g-recaptcha-response'];
			$secretKey = '6LemwyETAAAAABp7aR2EScUPsLoLwCSHPvGMQTpH';
            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$captcha}&remoteip={$_SERVER['REMOTE_ADDR']}");
			$obj = json_decode($response);
			if($obj->success == true) {
            //if(1 == 1) {
				$input = $this->load->lib('Input');
				$input->addValidation('title_req', $_POST['title'] ,'min=1','Ticket Title should be filled');
				$input->addValidation('title_max', $_POST['title'] ,'max=50','Ticket Title Max 50 Character');
				$input->addValidation('title', $_POST['title'] ,'alpha_numeric_sc','Only A-z and 0-9 Character allowed');
				$input->addValidation('type', $_POST['type'],'numeric','Something wrong happened');
				//$input->addValidation('main_category', $_POST['main_category'],'numeric','Terjadi kesalahan');
				//if(isset($_POST['sub_category']) && !empty($_POST['sub_category'])){
				//	$input->addValidation('sub_category', $_POST['sub_category'],'numeric','Terjadi kesalahan');
				//}
				if(isset($_POST['behalf']) && $_POST['behalf'] == 1){
					$input->addValidation('ticket_user', $_POST['user'],'numeric','Terjadi kesalahan');
				}
				
				if ($input->validate()){
                    $uploaded_files = $this->reArrayFiles($_FILES['image_upload']);
					if($uploaded_files[0]['name'] != ''){
						$num=0;
                        $uid=0;
						$upload_lib = $this->load->lib('Upload', array($uploaded_files[$uid]));
						for($uid=0;$uid < count($uploaded_files); $uid++){
							$file[$uid] = new Upload($uploaded_files[$uid]);
							if($file[$uid]->uploaded) {
								$file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num;
								$file[$uid]->image_resize = true;
								$file[$uid]->image_x = 640;
								$file[$uid]->image_y = 480;
								$file[$uid]->allowed = array('image/jpeg');
								$file[$uid]->process('Resources/images/ticket');
								if ($file[$uid]->processed) {
									$file[$uid]->clean();
									$successful_upload[$num] = array(
										'upload_name' => $file[$uid]->file_dst_name,
										'upload_location' => $file[$uid]->file_dst_path,
										'upload_time' => date("Y-m-d H:i:s"),
									);
									$num++;
								}
							}
						}
						
					}

					$ticket = $this->load->model('Tickets');
					$m_priority = $this->load->model('TicketPriority');
					$real_field = array(
						'ticket_title' => $_POST['title'],
						'ticket_type' => $_POST['type'],
						//'category' => (isset($_POST['sub_category']) && !empty($_POST['sub_category'])) ? $_POST['sub_category'] : $_POST['main_category'],
						'content' => htmlspecialchars($_POST['detail'], ENT_QUOTES),
						'status' => 1,
						'ticket_user' => (isset($_POST['behalf']) ? $_POST['user'] : $this->_mySession['user_id']),
						'ticket_creator' => $this->_mySession['user_id'],
						'submit_date' => date("Y-m-d H:i:s")
					);
					
					$log = array(
						'ticketlog_type' =>  'Submit',
						'user_id' => $this->_mySession['user_id'],
						'ticketlog_title' => $this->_mySession['username']. " Created a new Ticket",
						'ticketlog_content' => $real_field['content'],
						'ticketlog_time' => date("Y-m-d H:i:s"),
						'ticketlog_show' => '1'
					);
					
					if($ticket->createTicket($real_field, $log)){
						if(isset($successful_upload) && count($successful_upload) > 0){
							$m_upload = $this->load->model('Uploads');
							foreach($successful_upload as $insert_upload){
								$insert_upload['ticket_id'] = $ticket->lastInsertID;
								$m_upload->newUpload($insert_upload);
							}
						}
                        $mud = $this->load->model('UserData');
                        $mud->increaseDataValue(array('data_user'=>$this->_mySession['user_id'], 'data_site' => $this->site, 'data_name' => 'total_ticket'));
						$inserted_data = $ticket->showTicketBy(array('ticket_id' => $ticket->lastInsertID));
						$mail_option = array(
							'title' => "IT Ticket #{$ticket->lastInsertID} (NEW): {$_POST['title']}",
							'recipient' => $this->_mySession['email'],
							'recipient_name' => $this->_mySession['username'],
						);
						$this->send_email($mail_option, $inserted_data);
                        
						header("Location: edit_ticket_{$ticket->lastInsertID}.html");
					} else {
						$this->showError(2);
					}
				} else {
					$data['error'] = $input->_error;
					$data['input'] = $_POST;
					$this->load->template('ticket/submit', $data, $option);
				}
			} else {
				$data['input'] = $_POST;
				$data['error']['recaptcha'] = 1;
				$this->load->template('ticket/submit', $data, $option);
			}
            
        } else {
            $this->load->template('ticket/submit', $data, $option);
        }
    }
    
    function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
    
    public function edit_ticket()
    {
        $input = $this->load->lib('Input');
        $input->addValidation('ticket_id', $_GET['n'], 'numeric', 'Terjadi kesalahan');
        if($input->validate()){
            $ticket = $this->load->model('Tickets');
            $m_users = $this->load->model('Users');
            $query = array(
                'my_id' => (isset($this->_mySession['user_id']) ? $this->_mySession['user_id'] : 0),
                'ticket_id' => $_GET['n']
            );
            
            if($this->_mySession['group'] == 'Super Admin' || $this->_mySession['group'] == 'Administrator' || $this->_mySession['group'] == 'Admin IT' || $this->_mySession['group'] == 'Admin View'){
                unset($query['my_id']);
            }
            $data = array(
                '_mySession' => $this->_mySession,
                'ticket' => $ticket->showTicketBy($query),
                'main_category' => $ticket->getMainCategory(),
                'ticket_type' => $ticket->getTicketType(),
                'uploaded' => $ticket->getUploadedFile(array('ticket_id'=> $_GET['n'])),
                'log' => $ticket->getLog(array('ticket_id'=> $_GET['n'])),
                'staff' => $m_users->getAllEngineer()
            );
            if($data['ticket']['category_parent_id'] != 1){
                $data['sub_category'] = $ticket->getSubCategory($data['ticket']['category_parent_id']);
            }
            
            $option = array(
                'personal_js' => 1,
                'scriptload' => '
    <script type="text/javascript">
        var category_set = '.(empty($data['ticket']['category_id']) ? '0' : '1').';
        var category_set2 = '.($this->_mySession['group'] == 'Ticket User' ? '0' : '1').';
    </script>
    <script type="text/javascript" src="Resources/js/ticketjs.js"></script>'
            );
            
            $this->load->template('ticket/ticket_edit_staff', $data, $option);
        }
    }
    
    private function send_email($option, $data){
        $mail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail = $this->load->lib('STMail');
        $model_log = $this->load->model('Tickets');
        $log_data = $model_log->getLog(array('ticket_id' => $data['ticket_id']));
        $stmail->IsSMTP(); // telling the class to use SMTP
        $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $stmail->SMTPAuth   = true;                  // enable SMTP authentication
        $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $stmail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $stmail->Username   = "it.helpdesk@sushitei.co.id"; // SMTP account username
        $stmail->Password   = "St888@itoke";        // SMTP account password

        $stmail->SetFrom('it.helpdesk@sushitei.co.id', 'IT Helpdesk');

        $stmail->AddReplyTo('it.helpdesk@sushitei.co.id', 'IT Helpdesk');
        
        $stmail->isHTML('true');

        $stmail->Subject    = $option['title'];
        
        $stmail->MsgHTML($stmail->mailContent($data, $log_data));
        
        $stmail->AddAddress($option['recipient'], $option['recipient_name']);
        
        foreach($model_log->getMailRecipient() as $recipient){
            $stmail->AddBCC($recipient['email'], $recipient['display_name']);
        }
        
        


        return ($stmail->Send() ? 1 : 0);
            
        
    }
}
/*
* End Home Class
*/