<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class UserSessions extends Model
    {
        function __construct()
        {
            $config = array('db' => 'MySQL','host' => 'localhost','db_name' => 'sushitei_portal','user_name' => 'root','password' => '');
            #$config = array('db' => 'MySQL','host' => 'localhost','db_name' => 'sushitei_ticketdesk','user_name' => 'h71332_ticket','password' => 'a)_1qjbW!0-5');
            parent::__construct($config);
        }
        
		function getSession($sessid)
        {
            $time = strtotime(date("Y-m-d H:i:s"));
			$query = "SELECT * FROM user_sessions WHERE id='{$sessid}' AND timestamp <= '{$time}'";
            $result = $this->fetchSingleQuery($query);
			return $result;
		}
        
        function getDepartmentDetail($sess)
        {
			$query = "SELECT gm_email, gm_name, gm_position FROM sushitei_portal.users 
                        LEFT JOIN sushitei_portal.department spd ON sushitei_portal.users.department_id=spd.department_id 
                        WHERE user_id=:user_id";
            $result = $this->fetchSingleQuery($query, $sess);
			return $result;
        }
    }