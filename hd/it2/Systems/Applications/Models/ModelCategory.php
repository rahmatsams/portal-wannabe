<?php 
/*
* Class Model Categories for Category Controller
*/
class Category extends Model
{
    function getChildCategory($catparent)
    {
        $query_string = 'SELECT category_name,category_id FROM product_categories WHERE category_parents = :category_parent ORDER BY category_name ASC';
        $query_value = array(
            'category_parent' => $catparent
        );
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    function getCategory()
    {
        $query_string = 'SELECT * FROM product_categories WHERE category_parents NOT IN (0)';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
    function getCategoryProducts($cat)
    {
        $query_string = 'SELECT * FROM product_categories WHERE category_name = :category';
        $query_value = array(
            'category' => $cat
        );
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
    
    function viewCategoryByID($input){
        $query_string = 'SELECT * FROM product_categories WHERE category_id = :category';
        $query_value = array(
            'category' => $input
        );
        $result = $this->fetchSingleQuery($query_string);
		return $result;
    }
    function addCategory($input){
        $table = 'product_categories';
        $query_input = array(
            'category_parents' => $input['c_parents'],
            'category_name' => $input['c_name'],
            'category_images' => $input['c_images'],
            'category_description' => $input['c_description']
        );
		$result = $this->insertQuery($table,$query_input);
        return $result;
    }
    function editCategory($input){
        $table = 'product_categories';
        $query_input = array(
            'category_parents' => $input['c_parents'],
            'category_name' => $input['c_name'],
            'category_images' => $input['c_images'],
            'category_description' => $input['c_description']
        );
        $where = array(
            'category_id' => $input['c_id']
        );
		$result = $this->editQuery($table,$query_input,$where);
        return $result;
    }
    function deleteCategory($input){
        $query_string = 'DELETE FROM ticket_category WHERE category_id = :category_id';
		$query_value = array(
            'category_id' => $input
		);
		$result = $this->doQuery($query_string, $input);
		return $result;
        
    }
}
