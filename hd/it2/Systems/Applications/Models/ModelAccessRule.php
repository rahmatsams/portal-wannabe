<?php
class AccessRule extends Model
{
    public $result;

    public function check($input)
    {
        $query = 'SELECT rp.role, ps.site_name, pp.permission_url ';
        $query .= 'FROM sushitei_portal.portal_permissions pp ';
        $query .= 'INNER JOIN sushitei_portal.role_permissions rp ON rp.permission=pp.permission_id ';
        $query .= 'INNER JOIN sushitei_portal.portal_sites ps ON ps.site_id=pp.permission_site ';
        $query .= "WHERE rp.role=:gid ";
        $query .= "AND pp.permission_type=0 ";
        $query .= "AND pp.permission_site=:site ";
        $query .= "AND pp.permission_controller=:controller ";
        $query .= "AND pp.permission_method=:method";
        $this->result = $this->fetchSingleQuery($query, $input);
        return $this->result;
        
    }

    public function getMainMenu($gid)
    {
        $query = 'SELECT pp.permission_site,ps.site_name,ps.site_icon,pp.permission_order,pp.permission_name,pp.permission_url ';
        $query .= 'FROM sushitei_portal.role_permissions rp ';
        $query .= 'INNER JOIN sushitei_portal.portal_permissions pp ON rp.permission=pp.permission_id ';
        $query .= 'INNER JOIN sushitei_portal.portal_sites ps ON ps.site_id=pp.permission_site ';
        $query .= "WHERE rp.role='{$gid}' ";
        $query .= 'AND pp.show_on_menu=1 ';
        $query .= 'ORDER BY pp.permission_site,pp.permission_order,pp.permission_name ASC';
        $this->result = $this->fetchAllQuery($query);
        return $this->result;

    }
}