<?php
/*
* Model for Ticketdesk
*/
class TicketCategory extends Model
{
	public $lastInsertID;
    function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM it_category WHERE category_parent_id=1 AND deleted=0';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
    function getCategoryId($input){
        $query_string = 'SELECT category_id, category_name, description, work_time FROM it_category WHERE category_parent_id=1 AND category_id=:cid';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }
    
    function getSubCategoryId($input){
        $query_string = 'SELECT it_category.category_id, it_category.category_name, it_category.description, it_category.work_time,b.category_name AS parent_name, b.category_id AS parent_id FROM it_category LEFT JOIN it_category AS b ON b.category_id=it_category.category_parent_id WHERE it_category.category_id=:cid';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }
    
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM it_category WHERE category_parent_id=:cat_id ORDER BY category_name ASC';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getEmailReceiver($id)
	{
		$query_string = 'SELECT * FROM it_category_pic WHERE category=:cat_id ORDER BY recipient_name ASC';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getParentCategory($id)
	{
		$query_string = 'SELECT * FROM it_category WHERE category_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function createCategory($form){
		try{
			$result = $this->insertQuery('it_category', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
		} catch(Exception $e){
			if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
		}
	}
    function insertRecipient($form){
        $result = $this->insertQuery('it_category_pic', $form);
        return $result;
    
	}
    function editCategory($form, $where){
			$result = $this->editQuery('it_category', $form, $where);
            
            return $result;
    }
    
    function deleteCategory($where){
        $query = "DELETE FROM it_category WHERE category_id=:category_id";
        $result = $this->doQuery($query, $where);
        return $result;
    }
}