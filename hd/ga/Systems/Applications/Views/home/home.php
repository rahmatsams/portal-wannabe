<div class="container-fluid">    
    <h2 class="sub-header">Ticket Center</h2>
	<div class="col-md-12">
        <div class="row form-inline" style="margin-bottom: 10px;">
            <div class="col-sm-2 form-group">
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Advanced Search
                </a>
            </div>
            <div class="col-sm-4 col-lg-offset-6 form-group">
                <label>Sort</label>
                <select id="sortResult" name="sort_result" class="form-control">
                    <option value=""></option>
                    <option value="title_asc">Title ▲</option>
                    <option value="title_desc">Title ▼</option>
                    <option value="outlet_asc">Outlet ▲</option>
                    <option value="outlet_desc">Outlet ▼</option>
                    <option value="submit_asc">Submit Date ▲</option>
                    <option value="submit_desc">Submit Date ▼</option>
                    <option value="category_asc">Category ▲</option>
                    <option value="category_desc">Category ▼</option>
                    <option value="pic_asc">PIC ▲</option>
                    <option value="pic_desc">PIC ▼</option>
                    <option value="status_asc">Status ▲</option>
                    <option value="status_desc">Status ▼</option>
                </select>
            </div>
            
        </div>
        <div class="collapse collapse-well clearleftright" id="collapseExample">
                <form id="advanceSearch">
                    <div class="col-md-12 well">
                        <div class="row">
                            <div class="col-sm-3 form-group">
                                <label>Title</label>
                                <input id="ticketPage" name="page" type="hidden" value="1">
                                <input id="ticketTitle" name="title" type="text" placeholder="Type ticket subject.." class="form-control" maxlength="25">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Status</label>
                                <select id="ticketStatus" name="status" class="form-control">
                                    <option value="" default></option>
                                    <option value="1">Open</option>
                                    <option value="2">On-Progress</option>
                                    <option value="4">Need Sparepart</option>
                                    <option value="5">Solved</option>
                                    <option value="7">Closed</option>
                                    <option value="8">Force-Closed</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Outlet</label>
                                <select name="store_name" id="storeInput" class="form-control">
                                    <option value="" default></option>
                                    <?php
                                        
                                        foreach($outlet_list as $outlet){
                                            echo "\n
                                            <option value=\"{$outlet['store_name']}\">{$outlet['store_name']}</option>\n";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Category</label>
                                <select name="main_category" id="mainCategory" class="form-control">
                                    <option value="" default></option>
                                    <?php
                                        
                                        foreach($main_category as $category_form){
                                            echo "\n
                                            <option value=\"{$category_form['category_id']}\">{$category_form['category_name']}</option>\n";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Sub Category</label>
                                <select name="sub_category" class="form-control" id="subCategory" disabled>
                                    <option value="" default></option>
                                </select>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-sm-3 form-group">
                                <label>Submit date from</label>
                                <input id="ticketDateFirst" name="date_first" type="date" class="form-control" maxlength="10">
                            </div>                            
							<div class="col-sm-3 form-group">
								<label>to date</label>
                                <input id="ticketDateLast" name="date_last" type="date" class="form-control" maxlength="10">
                            </div>
                        </div>
                        
                        <button type="submit" name="submit" value="kirim" class="btn btn-sm">Search</button>
                    </div>
                </form> 
        </div>
    </div>
    <div class="table-responsive clearleftright">
        <table class="table table-striped table-bordered">
            <thead>
                <tr class="center-title">
                    <th>#</th>
                    <th>Subject</th>
                    <th>Owner</th>
                    <th>Outlet</th>
                    <th>Category</th>
                    <th>PIC</th>
                    <th>Status</th>
                    <th>Submit Date</th>
                    <th>Last Update</th>
                </tr>
            </thead>
            <tbody id="listTable">
                <?php
                    if(is_array($ticket_list) && !empty($ticket_list)){
                        $num = 1;
                        foreach($ticket_list as $result){
                            $date_check = '';
							$diff_seconds  = ($result['resolved_date'] != '0000-00-00 00:00:00' ? strtotime($result['resolved_date']) - strtotime($result['submit_date']) : strtotime(date("Y-m-d H:i:s")) - strtotime($result['submit_date']));
							$stat = floor($diff_seconds/3600);
							
							if(($stat >= 24 && $stat < 48) && $result['status'] < 5){
                                
								$date_check = "warning";
							}elseif($stat >= 48 && $result['status'] < 5){
								$date_check = "danger";
							}elseif($result['status'] >= 5){
								$date_check = "success";
							}else {
								$date_check = 'active';
							}
                            
                            $submit_date = date("d-m-Y", strtotime($result['submit_date']));
                            $last_update = (!empty($result['last_update']) ? date("d-m-Y", strtotime($result['last_update'])) : '-');
                            
                            echo "
                <tr class=\"{$date_check}\">
                    <td>{$num}</td>
                    <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                    <td>{$result['display_name']}</td>
                    <td>{$result['store_name']}</td>
                    <td>{$result['category_name']}</td>
                    <td>{$result['staff_name']}</td>
                    <td>{$result['status_name']}</td>
                    <td>{$submit_date}</td>
                    <td>{$last_update}</td>
                </tr>";
                            $num++;
                        }
                    } else {
                ?>
                <tr>
                    <td colspan="9">There's no Ticket</td>
                </tr>
                <?php
                    }
                    
                ?>
            </tbody>
        </table>
        <?php
            $pages = ceil($total/$max_result);
            if($pages > 1){
                echo '
            <nav>
                <ul class="pagination pagination-sm" id="pagingBottom">
                ';
                if(($current_page - 9) > 1){
                    echo"
                    <li>
                        <a href=\"#\" class='page-navigate' data-id='1'>1</a>
                    </li>
                    <li>
                        <a href=\"#\">...</a>
                    </li>";
                }
                $page_start = 1;
                $page_end = $pages;
                if($pages > 9 && $current_page > 9)
                {
                $page_start = (($current_page-4) < 1 ? 1 : $current_page-4);
                $page_end = (($current_page+4) > $pages ? $pages : ($current_page+4));
                }
                if($current_page <= 9)
                {
                $page_start = (($current_page-4) < 1 ? 1 : $current_page-4);
                $page_end = (($current_page+4) > 9 ? ($current_page+4) : 9);
                }
                for($i=$page_start;$i <= $page_end;$i++){
                    echo "<li><a class='page-navigate' data-id='{$i}'>{$i}</a></li>";
                }
                echo "
                    <li>
                        <a href=\"#\">...</span></a>
                    </li>
                    <li>
                        <a href=\"#\" class='page-navigate' data-id='{$pages}'>{$pages}</a>
                    </li>
                </ul>
            </nav>";
            }
        ?>
        <a class="btn btn-primary" role="button" href="export_data.html" aria-expanded="false" aria-controls="collapseExample">
            Export Data
        </a>
    </div>
</div>