<div class="container-fluid">
	<button class="btn btn-lg btn-primary">Buat Tiket Baru</button>
    <h2 class="sub-header">Tiket Aktif Saya</h2>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
					<th>#</th>
					<th>Judul</th>
					<th>Type</th>
					<th>Kategori</th>
					<th>Pembuat</th>
					<th>PIC</th>
					<th>Status</th>
					<th>Prioritas</th>
					<th>Pembuatan tiket</th>
					<th>Update</th>
                </tr>
			</thead>
			<tbody>
				<?php
					if(is_array($ticket_list) && !empty($ticket_list)){
						$num = 1;
						$status = array('Unknown','Open','On Progress','Pending','Need Vendor','Solved','Closed');

						foreach($ticket_list as $result){
							$date_check = '';
							
								$diff_seconds  = strtotime(date("Y-m-d H:i:s")) - strtotime($result['last_update']);
								$stat = floor($diff_seconds/3600);
								
								if($stat >= 24){
									if($result['category_name'] == 'Hardware'){
										$date_check = "warning";
									} elseif($result['category_name'] == 'Software') {
										$date_check = "danger";
									}
								}
								elseif($stat >= 48){
									if($result['category_name'] == 'Hardware'){
										$date_check = "danger";
									}
								} else {
									$date_check = 'info';
								}
							
									
							echo "
				<tr class=\"{$date_check}\">
					<td>{$num} </td>
					<td><a href=\"edit_ticket_{$result['ticket_id']}.html\">{$result['ticket_title']}</a></td>
					<td>{$result['type_name']}</td>
					<td>{$result['category_name']}</td>
					<td>{$result['user_name']}</td>
					<td>{$result['staff']}</td>
					<td>{$status[$result['status']]}</td>
					<td>{$result['priority_name']}</td>
					<td>{$result['submit_date']}</td>
					<td>{$result['last_update']}</td>
				</tr>";
							$num++;
						}
					} else {
				?>
				<tr>
					<td colspan="10">Tiket Kosong</td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>