<?php
/*
* Model for Ticketdesk
*/
class TicketType extends Model
{
	
    function getTicketType()
    {
        $query_string = 'SELECT * FROM ga_type';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
    function getCategoryId($input){
        $query_string = 'SELECT category_id, category_name, description, work_time FROM ga_category WHERE category_parent_id=1 AND category_id=:cid';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }
    
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM ga_category WHERE category_parent_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function createCategory($form){
		try{
			$result = $this->insertQuery('ga_category',$form);
            return 1;
		} catch(Exception $e){
			if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
		}
	}
}