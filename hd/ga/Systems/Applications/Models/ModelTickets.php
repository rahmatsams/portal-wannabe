<?php
/*
* Model for Ticketdesk
*/
class Tickets extends Model
{
	public $tableName = "ga_main";
    public $lastInsertID;
	
	function createTicket($form = array(), $log = array()){
		$this->beginTransaction();
		try{
			$this->insertQuery('ga_main',$form);
			$log['ticket_id'] = $this->lastInsertId();
			$this->lastInsertID = $this->lastInsertId();
			$this->insertQuery('ga_log',$log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
    function editTicket($form = array(), $log = array()){
        $table_ticket = 'ga_main';
        $table_log = 'ga_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table_ticket, $form['input'], $form['where']);
			$this->insertQuery('ga_log', $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
	
    function showAllTicket($query_option)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, ga_type.type_name, ga_category.category_name, spare_part, category_id, category_parent_id, ga_status.status_name, ga_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution ";
		$query_string .= "FROM ga_main LEFT JOIN ga_type ON ga_main.ticket_type=ga_type.type_id ";
		$query_string .= "LEFT JOIN ga_status ON ga_main.status=ga_status.status_id ";
		$query_string .= "LEFT JOIN ga_category ON ga_main.category=ga_category.category_id ";
		$query_string .= "LEFT JOIN ga_priority ON ga_main.priority=ga_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON ga_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ga_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ga_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON ga_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id) AS st ON ga_main.ticket_user=st.user_id ";
        $count_query = "SELECT COUNT(*) AS row_total FROM ga_main";
        
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
    function showAllTicketFiltered($filter, $query_option)
    {
        
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, ga_type.type_name, ga_category.category_name, spare_part, category_id, category_parent_id, ga_status.status_name, ga_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution ";
		$query_string .= "FROM ga_main LEFT JOIN ga_type ON ga_main.ticket_type=ga_type.type_id ";
		$query_string .= "LEFT JOIN ga_status ON ga_main.status=ga_status.status_id ";
		$query_string .= "LEFT JOIN ga_category ON ga_main.category=ga_category.category_id ";
		$query_string .= "LEFT JOIN ga_priority ON ga_main.priority=ga_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON ga_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ga_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ga_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON ga_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id) AS st ON ga_main.ticket_user=st.user_id ";
        $num = 1;
        $count_query = "SELECT COUNT(*) AS row_total FROM ga_main ";
        $count_query .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id) AS st ON ga_main.ticket_user=st.user_id ";
        foreach($filter as $key => $value){
            if($num > 1){
                if($key != 'date_end' && !empty($value)){
                    if($key == 'submit_date' && !empty($filter['date_end'])){
                        $query_string .= "AND submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                        $count_query .= "AND submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";                    
                    }elseif($key == 'get_all'){
                        $query_string .= "AND category IN (SELECT category_id FROM ga_category WHERE category_parent_id='{$value}') ";
                        $count_query .= "AND category IN (SELECT category_id FROM ga_category WHERE category_parent_id='{$value}') ";
                    }else{
                        $query_string .= "AND {$key} LIKE '%{$value}%' ";
                        $count_query .= "AND {$key} LIKE '%{$value}%' ";
                    }
                }
            } else {
                if($key != 'date_end' && !empty($value)){
                    if($key == 'submit_date' && !empty($filter['date_end'])){
                        $query_string .= "WHERE submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                        $count_query .= "WHERE submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";                    
                    }elseif($key == 'get_all'){
                        $query_string .= "WHERE category IN (SELECT category_id FROM ga_category WHERE category_parent_id='{$value}') ";
                        $count_query .= "WHERE category IN (SELECT category_id FROM ga_category WHERE category_parent_id='{$value}') ";
                    }else{
                        $query_string .=  "WHERE {$key} LIKE '%{$value}%' ";
                        $count_query .= "WHERE {$key} LIKE '%{$value}%' ";
                    }
                }
            }
            $num++;
        }
        
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
	function showMyActiveTicket($my_id, $query_option)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, ga_type.type_name, ga_category.category_name, spare_part, category_id, category_parent_id, ga_status.status_name, ga_priority.priority_name, impact, content, status, st.user_name, st.display_name, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution ";
		$query_string .= "FROM ga_main LEFT JOIN ga_type ON ga_main.ticket_type=ga_type.type_id ";
        $query_string .= "LEFT JOIN ga_status ON ga_main.status=ga_status.status_id ";
		$query_string .= "LEFT JOIN ga_category ON ga_main.category=ga_category.category_id ";
		$query_string .= "LEFT JOIN ga_priority ON ga_main.priority=ga_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON ga_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ga_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ga_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON ga_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id) AS st ON ga_main.ticket_user=st.user_id ";
		$query_string .= "WHERE ticket_user='{$my_id}'";
        $count_query = "SELECT COUNT(*) AS row_total FROM ga_main WHERE ticket_user='{$my_id}'";
		
		
        $result = $this->pagingQuery($query_string,$count_query,$query_option);
        return $result;
    }
    
    function showTicketBy($input)
    {
        $query_string = "SELECT ticket_id, ticket_title, ticket_type, ga_type.type_name, ga_category.category_name, category_id, category_parent_id, ga_status.status_name, ga_priority.priority_name, impact, content, status, st.user_name, st.display_name, st.email, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution, spare_part ";
		$query_string .= "FROM ga_main LEFT JOIN ga_type ON ga_main.ticket_type=ga_type.type_id ";
        $query_string .= "LEFT JOIN ga_status ON ga_main.status=ga_status.status_id ";
		$query_string .= "LEFT JOIN ga_category ON ga_main.category=ga_category.category_id ";
		$query_string .= "LEFT JOIN ga_priority ON ga_main.priority=ga_priority.priority_id ";
		$query_string .= "LEFT JOIN sushitei_portal.portal_user astaff ON ga_main.assigned_staff=astaff.user_id ";
        $query_string .= "LEFT JOIN (SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM ga_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime ON ga_main.ticket_id=atime.tid ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user cname ON ga_main.ticket_creator=cname.user_id ";
        $query_string .= "LEFT JOIN (SELECT store.store_id, store.store_name, user_id, user_name, display_name, email FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id) AS st ON ga_main.ticket_user=st.user_id ";
        if(isset($input['my_id'])){
            $query_string .= "WHERE ticket_user=:my_id AND ticket_id=:ticket_id";
        } else {
            $query_string .= "WHERE ticket_id=:ticket_id";
        }

        $result = $this->fetchSingleQuery($query_string, $input);
        
		return $result;
    }
    
    function getLog($input){
        $query_string = "SELECT ticketlog_title, ga_log.user_id, display_name, ticketlog_type, ticketlog_content, ticketlog_time FROM ga_log ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user ON ga_log.user_id=sushitei_portal.portal_user.user_id ";
        $query_string .= "WHERE ticket_id = :ticket_id AND ticketlog_show = 1 ORDER BY ticketlog_time DESC";
        $result = $this->fetchAllQuery($query_string, $input);
		return $result;
    }
    
	function getTicketType()
    {
        $query_string = 'SELECT * FROM ga_type';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM ga_category WHERE category_parent_id=1';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM ga_category WHERE category_parent_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getUploadedFile($query_value)
	{
		$query_string = 'SELECT * FROM ga_upload WHERE ticket_id=:ticket_id';
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getAllActiveStore()
    {
        $query_string = "SELECT * FROM sushitei_portal.portal_store WHERE store_status=1";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getMailRecipient()
    {
        $query_string = 'SELECT pu.email,pu.display_name FROM sushitei_portal.portal_user pu ';
        $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
        $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
        $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="receiveEmail" AND pp.permission_site='.getConfig('site_number').' GROUP BY pu.email';
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function myOutlet($input){
        $query_string = "SELECT store_name FROM sushitei_portal.portal_user LEFT JOIN store ON sushitei_portal.portal_user.store_id=store.store_id WHERE user_id=:user_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
}