<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class Users extends Model{
		public $_result = array();
		private $table_field = array('user_id','user_name','user_password','group_id');
		
		function doLogin($input){
			$password = md5($input['ticket_password']);
			$query = $this->prepare("SELECT user_id, display_name, group_name, email, level FROM sushitei_portal.portal_user LEFT JOIN user_groups USING (group_id) WHERE user_name=:username AND user_password=:password");
			$query->bindParam(':username',$input['ticket_email']);
			$query->bindParam(':password',$password);
			$query->execute();
			if($query->rowCount() == 1){
				$resultarray = $query->fetch();
				return $resultarray;
			}else{
				return 0;
			}
		}
        
        function newUser($input)
        {
            
            try{
                $this->insertQuery('users', $input);
                return 1;
            } catch(Exception $e){
                if(getConfig('development') == 1){
                    echo $e;
                    exit;
                }
                return 0;
            }
        }
        
        function newTicketUser($input)
        {
            $result = $this->insertQuery('users', $input['new_user']);
            return $result;
        }
        
        function editTicketUser($input = array()){
            $result = $this->editQuery('users', $input['users'], $input['where']);
            return $result;

        }
		
        
        function checkUserName($input){
			$query = $this->prepare("SELECT user_name FROM sushitei_portal.portal_user WHERE user_name=:username");
			$query->bindParam(':username',$input);
			$query->execute();
			if($query->rowCount() > 0){
				return 0;
			}else{
				return 1;
			}
        }
        function getAllNotAdmin(){
			$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE role_id=4";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllNotUser(){
			$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE role_id NOT IN ('4')";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllEngineer(){
			$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE role_id=11";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        
        function getAllUser($query_option)
        {
            $query_string = "SELECT  user_id, user_name, display_name, email, spur.role_name AS group_name, user_status ";
            $query_string .= "FROM sushitei_portal.portal_user LEFT JOIN sushitei_portal.user_role spur USING (role_id)";
            $count_query = "SELECT COUNT(*) AS row_total FROM sushitei_portal.portal_user";
            
            $result = $this->pagingQuery($query_string, $count_query, $query_option);
            return $result;
        }
		function getUserBy($input)
        {
            $query_string = "SELECT  user_id, user_name, display_name, email, spur.role_name AS group_name, user_status ";
            $query_string .= "FROM sushitei_portal.portal_user LEFT JOIN sushitei_portal.user_role spur USING (role_id)";
            $query_string .= "WHERE user_id=:user_id";            
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }
        
        function editUser($form = array(), $where = array()){
            $table = 'users';
            try{
                $this->editQuery($table, $form, $where);
                return 1;
            } catch(Exception $e){
                return $e;
            }
                
        }
		
		function deleteUser($id){
			
			$result = $this->deleteQuery('sushitei_portal.portal_user' ,$id);
			return $result;
			
		}
        
        function getAllGroups(){
			$query_string = "SELECT role_id, role_name FROM user_role";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
		function getAllActiveStore()
        {
            $query_string = "SELECT * FROM sushitei_portal.portal_store WHERE store_status=1";
            
            $result = $this->fetchAllQuery($query_string);
            return $result;
        }
	}
?>