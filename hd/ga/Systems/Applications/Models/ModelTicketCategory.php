<?php
/*
* Model for Ticketdesk
*/
class TicketCategory extends Model
{
	public $lastInsertID;
    function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, work_time FROM ga_category WHERE category_parent_id=1';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
    function getCategoryId($input){
        $query_string = 'SELECT category_id, category_name, description, work_time FROM ga_category WHERE category_parent_id=1 AND category_id=:cid';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }
    
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM ga_category WHERE category_parent_id=:cat_id ORDER BY category_name ASC';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getParentCategory($id)
	{
		$query_string = 'SELECT * FROM ga_category WHERE category_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function createCategory($form){
		try{
			$result = $this->insertQuery('ga_category', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
		} catch(Exception $e){
			if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
		}
	}
    function editCategory($form, $where){
			$result = $this->editQuery('ga_category', $form, $where);
            
            return $result;
    }
}