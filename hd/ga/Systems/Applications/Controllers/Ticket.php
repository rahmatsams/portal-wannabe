<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ticket extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('edit_ticket','submit_ticket'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('edit_ticket','submit_ticket'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
    
    
    public function submit_ticket()
    {
        $category = $this->load->model('TicketCategory');
        $data = array(
            '_mySession' => $this->_mySession,
            'main_category' => $category->getMainCategory(),
            'admin' => $this->isAdmin()
        );
        
        $option = array(
                'admin' => $this->isAdmin(),
                'personal_js' => 1,
                'scriptload' => '
        <script type="text/javascript">
            $(document).ready(function(){
                $("#mainCategory").change(function(event){
                    event.preventDefault();
                    changeValue();
                });
                $("#onbehalf").change(function(event){
                    event.preventDefault();
                    if($("#onbehalf").prop("checked") == true){
                        changeBehalf();
                    } else {
                        $("#userlist").attr("disabled", true);
                    }
                    
                });
                $("#addFileButton").click(function(event){
                    event.preventDefault();
                    num_files = $("#addFile input:file").length;
                    if(num_files < 5){
                        $("#addFile").append("<input name=\"image_upload[]\" type=\"file\" class=\"form-control\" accept=\"image/jpeg\">");
                    }
                });
            });
            
        </script>
		<script src="https://www.google.com/recaptcha/api.js"></script>
        ');
        #exit(print_r($_POST));
        if(isset($_POST['submit']) && $_POST['submit'] == 'kirim' && isset($_POST['g-recaptcha-response'])){
            echo '1';
			$captcha = $_POST['g-recaptcha-response'];
			$secretKey = '6LemwyETAAAAABp7aR2EScUPsLoLwCSHPvGMQTpH';
			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$captcha}&remoteip={$_SERVER['REMOTE_ADDR']}");
			$obj = json_decode($response);
			#if($obj->success == 1) {
            if(1 == 1){
                echo '2';
				$input = $this->load->lib('Input');
				$input->addValidation('title', $_POST['title'] ,'min=1','Judul wajib diisini');
				$input->addValidation('title', $_POST['title'] ,'max=50','Judul maximal 49 karakter');
				$input->addValidation('title', $_POST['title'] ,'alpha_numeric_sc','Judul hanya boleh Huruf dan Angka');
				$input->addValidation('type', $_POST['type'],'numeric','Terjadi kesalahan');
				$input->addValidation('main_category', $_POST['main_category'],'numeric','Terjadi kesalahan');
				if(isset($_POST['sub_category']) && !empty($_POST['sub_category'])){
					$input->addValidation('sub_category', $_POST['sub_category'],'numeric','Terjadi kesalahan');
				}
				if(isset($_POST['behalf']) && $_POST['behalf'] == 1){
					$input->addValidation('ticket_user', $_POST['user'],'numeric','Terjadi kesalahan');
				}
				
				if ($input->validate()){
                    echo '3';
                    $uploaded_files = $this->reArrayFiles($_FILES['image_upload']);
					if($uploaded_files[0]['name'] != ''){
						$num=0;
                        $uid=0;
						$upload_lib = $this->load->lib('Upload', array($uploaded_files[$uid]));
						for($uid=0;$uid < count($uploaded_files); $uid++){
							$file[$uid] = new Upload($uploaded_files[$uid]);
							if($file[$uid]->uploaded) {
								$file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num;
								$file[$uid]->image_resize = true;
								$file[$uid]->image_x = 640;
								$file[$uid]->image_y = 480;
								$file[$uid]->allowed = array('image/jpeg');
								$file[$uid]->process('Resources/images/Ticket');
								if ($file[$uid]->processed) {
									$file[$uid]->clean();
									$successful_upload[$num] = array(
										'upload_name' => $file[$uid]->file_dst_name,
										'upload_location' => $file[$uid]->file_dst_path,
										'upload_time' => date("Y-m-d H:i:s"),
									);
									$num++;
								}
							}
						}
						
					}

					$ticket = $this->load->model('Tickets');
					$m_priority = $this->load->model('TicketPriority');
					$real_field = array(
						'ticket_title' => $_POST['title'],
						'ticket_type' => $_POST['type'],
						'category' => (isset($_POST['sub_category']) && !empty($_POST['sub_category'])) ? $_POST['sub_category'] : $_POST['main_category'],
						'content' => htmlspecialchars($_POST['detail'], ENT_QUOTES),
						'status' => 1,
						'ticket_user' => (isset($_POST['behalf']) ? $_POST['user'] : $this->_mySession['user_id']),
						'ticket_creator' => $this->_mySession['user_id'],
						'submit_date' => date("Y-m-d H:i:s")
					);
					
					$log = array(
						'ticketlog_type' =>  'Submit',
						'user_id' => $this->_mySession['user_id'],
						'ticketlog_title' => $this->_mySession['username']. " Created a new Ticket",
						'ticketlog_content' => $real_field['content'],
						'ticketlog_time' => date("Y-m-d H:i:s"),
						'ticketlog_show' => '1'
					);
					
					if($ticket->createTicket($real_field, $log)){
                        echo '4';
						if(isset($successful_upload) && count($successful_upload) > 0){
							$m_upload = $this->load->model('Uploads');
							foreach($successful_upload as $insert_upload){
								$insert_upload['ticket_id'] = $ticket->lastInsertID;
								$m_upload->newUpload($insert_upload);
							}
						}
                        $mud = $this->load->model('UserData');
                        $mud->increaseDataValue(array('data_user'=>$this->_mySession['user_id'], 'data_site' => $this->site, 'data_type' => 1));
						$inserted_data = $ticket->showTicketBy(array('ticket_id' => $ticket->lastInsertID));
						$mail_option = array(
							'title' => "GA Ticket #{$ticket->lastInsertID} (NEW): {$_POST['title']}",
							'recipient' => $this->_mySession['email'],
							'recipient_name' => $this->_mySession['username'],
						);
						$this->send_email($mail_option, $inserted_data);
						header("Location: edit_ticket_{$ticket->lastInsertID}.html");
					} else {
						$this->showError(2);
					}
				} else {
					$data['error'] = $input->error;
					$data['input'] = $_POST;
					$this->load->template('ticket/submit', $data, $option);
				}
			} else {
				$data['input'] = $_POST;
				$data['error']['recaptcha'] = 1;
				$this->load->template('ticket/submit', $data, $option);
			}
            
        } else {
            $this->load->template('ticket/submit', $data, $option);
        }
    }
    
    function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
    
    public function edit_ticket()
    {
        $input = $this->load->lib('Input');
        $input->addValidation('ticket_id', $_GET['n'], 'numeric', 'Terjadi kesalahan');
        if($input->validate()){
            $ticket = $this->load->model('Tickets');
            $m_users = $this->load->model('Users');
            $query = array(
                'my_id' => (isset($this->_mySession['user_id']) ? $this->_mySession['user_id'] : 0),
                'ticket_id' => $_GET['n']
            );
            $option = array(
                'admin' => $this->isAdmin(),
                'personal_js' => 1,
                'scriptload' => '
                    <script type="text/javascript">
                    $(document).ready(function(){
                        $("#mainCategory").change(function(event){
                            event.preventDefault();
                            changeValue();
                        });
                        var detail_height = $("#tickettabledetail").height();
                        $("#tickettabledesc").css("min-height", detail_height);
                        $("#commentbutton").click(function(event){
                            var text = "Comment";
                            var text2 = "Comment on ticket as additional note.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#assignbutton").click(function(event){
                            var text = "Assign";
                            var text2 = "Assign the ticket to the listed staff.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                            toggleAssign();
                        });
                        $("#takeoverbutton").click(function(event){
                            var text = "Take Over";
                            var text2 = "Take over this ticket to assign it to yourself.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#needsparebutton").click(function(event){
                            var text = "Need Spare-part";
                            var text2 = "Mark this ticket, need to order sparepart.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                            toggleSparePart();
                        });
                        $("#cancelsparebutton").click(function(event){
                            var text = "Cancel Spare-part";
                            var text2 = "Cancel order sparepart.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#donesparebutton").click(function(event){
                            var text = "Sparepart Done";
                            var text2 = "Mark this ticket, the ordered sparepart has arrived.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#resolvebutton").click(function(event){
                            var text = "Solve Ticket";
                            var text2 = "Solve your ticket.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                            toggleResolve();
                        });
                        $("#closebutton").click(function(event){
                            var text = "Close ticket";
                            var text2 = "Close ticket after assigned staff solve it.";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });

                        
                        $("#forcebutton").click(function(event){
                            var text = "Force Close";
                            var text2 = "Force close the ticket";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#vendorbutton").click(function(event){
                            var text = "Vendor";
                            var text2 = "Note the ticket need a vendor to solve it";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#reopenbutton").click(function(event){
                            var text = "Re-Open Ticket";
                            var text2 = "Re-open closed ticket";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                        });
                        $("#cancelstatus").click(function(event){
                            showActivity();
                            if($("#submitstatus").html() == "Edit"){
                                toggleEdit();
                            }
                            if($("#submitstatus").html() == "Assign"){
                                toggleAssign();
                            }
							if($("#submitstatus").html() == "Resolve"){
                                toggleResolve();
                            }
                            $("#tickettitle").html("Ticket Command");
                        });
                        $("#editbutton").click(function(event){
                            var text = "Edit Ticket";
                            var text2 = "Edit the ticket detail";
                            var button_name = $(this).html();
                            showComment(text, text2, button_name);
                            toggleEdit();
                        });
                        $("#updateticketstatus").submit(function(event){
                            event.preventDefault();
                            var me = $(this);
                            editTicket(me);
                            
                        });
                        
                    });
                    </script>'
            );
            if($this->isAdmin()){
                unset($query['my_id']);
            }
            $data = array(
                '_mySession' => $this->_mySession,
                'ticket' => $ticket->showTicketBy($query),
                'main_category' => $ticket->getMainCategory(),
                'ticket_type' => $ticket->getTicketType(),
                'uploaded' => $ticket->getUploadedFile(array('ticket_id'=> $_GET['n'])),
                'log' => $ticket->getLog(array('ticket_id'=> $_GET['n'])),
                'staff' => $m_users->getAllEngineer(),
                'admin' => $this->isAdmin()
            );
            if($data['ticket']['category_parent_id'] != 1){
                $data['sub_category'] = $ticket->getSubCategory($data['ticket']['category_parent_id']);
            }
            $this->load->template('ticket/ticket_edit_staff', $data, $option);
        }
    }
    
    private function send_email($option, $data){
        $mail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail = $this->load->lib('STMail');
        $model_log = $this->load->model('Tickets');
        $log_data = $model_log->getLog(array('ticket_id' => $data['ticket_id']));
        $stmail->IsSMTP(); // telling the class to use SMTP
        $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
        $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $stmail->SMTPAuth   = true;                  // enable SMTP authentication
        $stmail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $stmail->Username   = "ga.helpdesk@sushitei.co.id"; // SMTP account username
        $stmail->Password   = "sushitei2003";        // SMTP account password

        $stmail->SetFrom('ga.helpdesk@sushitei.co.id', 'GA Helpdesk');

        $stmail->AddReplyTo('ga.helpdesk@sushitei.co.id', 'GA Helpdesk');
        
        $stmail->isHTML('true');

        $stmail->Subject    = $option['title'];
        
        $stmail->MsgHTML($stmail->mailContent($data, $log_data));
        
		$stmail->AddAddress('ga.helpdesk@sushitei.co.id', 'GA Helpdesk');
        $stmail->AddAddress($option['recipient'], $option['recipient_name']);
        
        foreach($model_log->getMailRecipient() as $recipient){
            $stmail->AddBCC($recipient['email'], $recipient['display_name']);
        }
        
        


        return ($stmail->Send() ? 1 : 0);
            
        
    }
}
/*
* End Home Class
*/