<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Categories extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('index', 'insert_category' , 'drop_category', 'edit_category'),
                'groups'=>array('Guest'),
            ),
            array('Allow', 
                'actions'=>array('index', 'insert_category' , 'drop_category', 'edit_category'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin GA'),
            ),
        );
    }
    
    public function index(){
        $m_category = $this->load->model('TicketCategory');
        $data = array(
            '_mySession' => $this->_mySession,
            'category' => $m_category->getMainCategory(),
        );
        $option = array(
            'personal_js' => 1,
            'scriptload' => '
            <script type="text/javascript">
                $(document).ready(function(){
                    $(".edit-category").click(function(event){
                        event.preventDefault();
                        classid = this.id;
                        editCategory(classid);
                        getSubCategory(classid);
                    });
                    $(document).on("click", ".edit-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        editFormSubCategory(dataid);
                    });
                    $(document).on("click", ".edit-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        editFormSubCategory(dataid);
                    });
                    $(document).on("click", ".cancel-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        cancelFormSubCategory(dataid);
                    });
                    $("#editcategory").submit(function(event){
                        // cancels the form submission
                        event.preventDefault();
                        submitEdit();
                    });
                    $("#addrowcategory").click(function(event){
                        event.preventDefault();
                        if($("#insertsub").hasClass("hidden")){
                            $("#insertsub").removeClass("hidden");
                        } else {
                            $("#insertsub").addClass("hidden");
                        }
                    });
                    $(document).on("submit", "#addsubcategory", function(event){
                        // cancels the form submission
                        event.preventDefault();
                        var me = $(this);
                        submitSubCategory(me);
                    });
                    $("#addcategory").submit(function(event){
                        // cancels the form submission
                        event.preventDefault();
                        var me = $(this);
                        submitCategory(me);
                    });
                    $("#editcategory").submit(function(event){
                        // cancels the form submission
                        event.preventDefault();
                        var me = $(this);
                        submitEditCategory(me);
                    });
                    $(".deleteconfirmationmodal").click(function(event){
                        event.preventDefault();
                        dataid = $(this).data("id");
                        $("#deleteconfirm").attr("data-id", dataid);
                    });
                    $(document).on("click", "#deleteconfirm", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        window.location.assign("delete_confirmation_"+dataid+".html");
                    });
                    $(document).on("click", ".delete-sub-category", function(){
                        event.preventDefault();
                        dataid = this.getAttribute("data-id");
                        $("#deleteconfirm").attr("data-id", dataid);
                    });
                });
               
            </script>
            '
        );
        $this->load->template('admin/index_category', $data, $option);
    }
    
    public function insert_category(){
        if(isset($_POST['category'])){
            $input = $this->load->lib('input');
            $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
            if(isset($_POST['sub_category'])){
                $input->addValidation('category_parent_id', $_POST['category_parent_id'], 'numeric', 'Cek kembali input anda');
            }
            $input->addValidation('description', $_POST['description'], 'min=1', 'Cek kembali input anda');
            if($input->validate()){
                unset($_POST['category']);
                if(!isset($_POST['category_parent_id'])){
                    $_POST['category_parent_id'] = 1;
                }
                $m_ticketcategory = $this->load->model('TicketCategory');
                if($m_ticketcategory->createCategory($_POST)){
                    echo $m_ticketcategory->lastInsertID;
                } else {
                    echo 'failed';
                }
            }else{
                echo 'failed';
            }
            
        }
    }
    
    public function edit_category(){
        if(isset($_POST['category_id'])){
            $input = $this->load->lib('input');
            $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
            $input->addValidation('category_id', $_POST['category_id'], 'numeric', 'Cek kembali input anda');
            $input->addValidation('category_id', $_POST['category_id'], 'min=1', 'Cek kembali input anda');
            $input->addValidation('category_id', $_POST['category_id'], 'max=3', 'Cek kembali input anda');
            $input->addValidation('description', $_POST['description'], 'min=1', 'Cek kembali input anda');
            if($input->validate()){
                $update = array(
                    'category_name' => $_POST['category_name'],
                    'description' => $_POST['description'],
                );
                $where = array(
                    'category_id' => $_POST['category_id']
                );
                $m_ticketcategory = $this->load->model('TicketCategory');
                if($m_ticketcategory->editCategory($update, $where)){
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }else{
                echo 'failed';
            }
            
        }
    }
    
    public function drop_category(){
        if(isset($_GET['category_id'])){
            $input = $this->load->lib('Input');
            $input->addValidation('category_id', $_GET['category_id'], 'max=6', 'Excedding allowed range');
            $input->addValidation('category_id', $_GET['category_id'], 'numeric', 'Excedding allowed range');
            if($input->validate()){
                $m_category = $this->load->model('Category');
                if($m_category->deleteCategory(array('category_id' => $_GET['category_id']))){
                    header("Location: category_admin.html");
                }
            } else {
                print_r($input->_error);
            }
        }else{
            echo 'there';
        }
    }
}
/*
* End Home Class
*/