<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
    if(file_exists(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __CONFIGS_DIR .__BASE_CONFIG_FILE)){
        $__config = array();
        require_once(__ROOT__ . __BASE_DIR . __SYSTEM_DIR . __CONFIGS_DIR .__BASE_CONFIG_FILE);
    }else{
        exit ('Configuration File Cannot Be Found');
    }

    function getConfig($item)
    {
        global $_config;
        $data = $_config[$item];
        return $data;
    }

    function siteUrl()
    {
        return getConfig('base_url');
    }
    
    function controllerUrl($controller,$action=null)
    {
        if (empty($action)) {
            $action = getConfig('default_action');
        }
        return siteurl() .getConfig('index_page') .'?'.__CONTROLLER_COMMAND.'='.$controller.'&'.__ACTION_COMMAND.'='.$action;
    }
	
    function redirect($controller,$view,$exit=1)
    {
        if ($exit == 1) {
            header('Location: '.controllerUrl($controller,$view));
            exit;
        } else {
            header('Location: '.controllerUrl($controller,$view));
        }
    }
    function trimText($text, $limit)
    {
        preg_match_all('/(\S+\s+)/',strip_tags($text),$matches);
        if ($limit < count($matches[0])) {
            return rtrim(implode('',array_slice($matches[0],0,$limit)));
        } else {
            return $text;
        }
    }

    /* Normalizing array index on file upload */
    function getNormalizedFILES() 
    { 
        $newfiles = array(); 
        foreach($_FILES as $fieldname => $fieldvalue) 
            foreach($fieldvalue as $paramname => $paramvalue) 
                foreach((array)$paramvalue as $index => $value) 
                    $newfiles[$fieldname][$index][$paramname] = $value; 
        return $newfiles; 
    }
?>