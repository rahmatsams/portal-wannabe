<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_model extends CI_Model 
{
    public $result;
    
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('ticket', true);
    }
    public function getHome($opt, $where = 0, $like = 0)
    {
        $query = $this->db
        ->select('im.ticket_id, im.ticket_title, sppu.display_name AS owner, spps.store_name AS store, im.status, is.status_name, im.submit_date, im.resolved_date, im.last_update')
        ->from('it_main im')
        ->join('it_status is', 'im.status=is.status_id', 'left')
        ->join('sushitei_portal.portal_user sppu', 'im.ticket_creator=sppu.user_id', 'left')
        ->join('sushitei_portal.portal_store spps', 'sppu.store_id=spps.store_id', 'left');
        if($where){
            $query->where($where);
        }
        if($like){
            $query->like($like);
        }
        
        foreach($opt['order'] as $name => $direction){
            $query->order_by($name, $direction);
        }
        if(isset($opt['page']) && $opt['page'] > 1){
            $query->limit($opt['result'],($opt['page']-1)*$opt['result']);
        }else{
            if(isset($opt['result'])) $query->limit($opt['result'], 0);
        }
        
        #foreach($query->get()->result_array() AS $row){
        #    $this->result['data'][] = array($row['user_id'], $row['email'], $row['display_name'], $row['status'], $row['group_name'], $row['store_name']);
        #}
        $this->result['data'] = $query->get()->result_array();
        if(count($this->result['data']) > 0){
            $this->result['total'] = $this->getCount($opt, $where, $like);
        }else{
            $this->result['total'] = 0 ;
        }
        
        
        return $this->result;
    }
    
    public function getCount($opt, $where = 0, $like = 0)
    {
        $this->db->from('it_main');
        if($where){
            $query->where($where);
        }
        if($like){
            $query->like($like);
        }
        
        return $this->db->count_all_results();
    }
    
    public function getDetailFrom($input)
    {
        $query = $this->db
        ->select(' ticket_id, ticket_title, ticket_type, it_type.type_name, it_category.category_name, category_id, category_parent_id, it_status.status_name, impact, content, status, st.user_name, st.display_name, st.email, store_name, assigned_staff, astaff.user_name AS staff, astaff.display_name AS staff_name, assign_time, ticket_user, cname.display_name AS creator_name, submit_date,last_update, problem_source, resolved_date, resolved_solution ')
        ->from('it_main im')
        ->join('it_status is', 'im.status=is.status_id', 'left')
        ->join('it_category ic', 'im.category=ic.category_id', 'left')
		->join('sushitei_portal.portal_user astaff','im.assigned_staff=astaff.user_id','left')
        ->join("(SELECT ticket_id AS tid,MIN(ticketlog_time) AS assign_time FROM it_log WHERE ticketlog_type='Assign' OR ticketlog_type='Take Over' GROUP BY ticket_id) atime", 'im.ticket_id=atime.tid', 'left')
        ->join('sushitei_portal.portal_user cname', 'im.ticket_creator=cname.user_id ', 'left')
        ->join("(SELECT store.store_id, store.store_name, user_id, user_name, display_name, email FROM sushitei_portal.portal_user pu LEFT JOIN sushitei_portal.portal_store ps ON pu.store_id=ps.store_id) st", 'im.ticket_user=st.user_id', 'left');
        if(!empty($input['where'])){
            $query->where($input['where']);
        }
        if(!empty($input['like'])){
            $query->like($input['like']);
        }
        
        return $query->get()->row();
    }

}