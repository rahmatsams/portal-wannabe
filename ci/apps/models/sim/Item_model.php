<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_model extends CI_Model 
{
    public $result;
    
    public function __construct(){
        parent::__construct();
        $this->db = $this->load->database('item', true);
    }
    
    public function all()
    {
        $query = $this->db->from('view_master');
        
        $this->result['data'] = $query->get()->result_array();
        
        return $this->result;
    }
    
    public function pagedDataTable($opt, $where = false, $like = false)
    {
        $query = $this->db
        ->from('view_master');
        
        if($where){
            $query->where($where);
        }
        if($like){
            $query->like($like);
        }
        
        foreach($opt['order'] as $name => $direction){
            $query->order_by($name, $direction);
        }
        if(isset($opt['page']) && $opt['page'] > 1){
            $query->limit($opt['result'],($opt['page']-1)*$opt['result']);
        }else{
            if(isset($opt['result'])) $query->limit($opt['result'], 0);
        }
        
        #foreach($query->get()->result_array() AS $row){
        #    $this->result['data'][] = array($row['user_id'], $row['email'], $row['display_name'], $row['status'], $row['group_name'], $row['store_name']);
        #}
        $this->result['data'] = $query->get()->result_array();
        if(count($this->result['data']) > 0){
            $this->result['recordsFiltered'] = $this->getCount($opt, $where, $like);
            $this->result['recordsTotal'] = $this->getCount($opt);
        }else{
            $this->result['total'] = 0 ;
        }
        
        
        return $this->result;
    }
    
    public function getCount($opt, $where = false, $like = false)
    {
        $this->db->from('master_item');
        if($where){
            $query->where($where);
        }
        if($like){
            $query->like($like);
        }
        
        return $this->db->count_all_results();
    }

}