<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Default Controller
*/
class Ticket extends SISTER_Controller
{
    public $skipCheck = ['index', 'detail'];
    
    public function __construct()
    {
        parent::__construct();
        $this->session->isLoggedIn(true);
        #$this->state['menu']['']
        session_write_close();
        $this->load->helper('html','url');
    }
    
    public function detail($id=0)
    {   
        #exit(json_encode($this->state['menu']));
        $this->load->model('it/hd/Ticket_model', 'mticket');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $_POST['ticket_id'] = $id;
        $this->form_validation->set_rules('ticket_id', 'Ticket', array('required', 'numeric', 'max_length[11]'));
        if ($this->form_validation->run() == FALSE){
            echo "Error: ".form_error('Ticket');
        }else{
            $data['body']['page'] = 'Ticket Detail';
            $data['body']['session'] = $this->state;        
            $data['body']['option']['stylesheet'][0] = base_url().'assets/modules/DataTables/dataTables.bootstrap4.min.css';
            $data['body']['option']['js'][2] = base_url().'assets/scripts/portal/portal.js';
            $data['content']['ticket'] = $this->mticket->getDetailFrom($data['content']['opt']);
            #exit(print_r($data['content']['ticket']));
            $data['content']['session'] = $this->state;
            $data['body']['content'] = $this->load->view('it/hd/contents/ticket/detail', $data['content'], TRUE);
            $this->load->view('it/hd/templates/sufee/template', $data['body']);

        }
    }
}
/*
* End Home Class
*/