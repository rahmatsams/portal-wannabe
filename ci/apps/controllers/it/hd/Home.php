<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Default Controller
*/
class Home extends SISTER_Controller
{
    public $skipCheck = ['index'];
    
    public function __construct()
    {
        parent::__construct();
        $this->session->isLoggedIn(true);
        #$this->state['menu']['']
        session_write_close();

    }
    
    public function index($page=1)
    {   
        #exit(json_encode($this->state['menu']));
        $this->load->helper('html','url');
        $this->load->model('it/hd/Ticket_model', 'mticket');
        $data['content']['opt']['order'] = array('ticket_id', 'DESC');
        $data['content']['opt']['result'] = 10;
        $data['content']['opt']['page'] = is_numeric((strlen($page) < 5 ? $page : 1)) ? $page : 1;
        $data['body']['page'] = 'Dashboard';
        $data['body']['session'] = $this->state;        
        $data['body']['option']['stylesheet'][0] = base_url().'assets/modules/DataTables/dataTables.bootstrap4.min.css';
        $data['body']['option']['js'][2] = base_url().'assets/scripts/portal/portal.js';
        $data['content']['ticket'] = $this->mticket->getHome($data['content']['opt']);
        #exit(print_r($data['content']['ticket']));
        $data['content']['session'] = $this->state;
        $data['body']['content'] = $this->load->view('it/hd/contents/home/index', $data['content'], TRUE);
        $this->load->view('it/hd/templates/sufee/template', $data['body']);
    }
}
/*
* End Home Class
*/