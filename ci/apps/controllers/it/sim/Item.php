<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Default Controller
*/
class Item extends SISTER_Controller
{
    public $skipCheck = ['index'];
    
    public function __construct()
    {
        parent::__construct();
        $this->session->isLoggedIn(true);
        session_write_close();
        $this->menu = array();
        $this->menu[0]['permission_site'] = 2;
        $this->menu[0]['site_name'] = 'Administrator';
        $this->menu[0]['site_icon'] = 'fa-cog';
        $this->menu[0]['permission_order'] = 0;
        $this->menu[0]['permission_name'] = 'Dashboard';
        $this->menu[0]['permission_url'] = 'it/sim';
        $this->menu[0]['site_group'] = 1;

        $this->menu[1]['permission_site'] = 2;
        $this->menu[1]['site_name'] = 'Administrator';
        $this->menu[1]['site_icon'] = 'fa-user';
        $this->menu[1]['permission_order'] = 1;
        $this->menu[1]['permission_name'] = 'Manage User';
        $this->menu[1]['permission_url'] = 'it/sim';
        $this->menu[1]['site_group'] = 1;

        $this->menu[2]['permission_site'] = 3;
        $this->menu[2]['site_name'] = 'sim';
        $this->menu[2]['site_icon'] = 'fa-user';
        $this->menu[2]['permission_order'] = 0;
        $this->menu[2]['permission_name'] = 'Dashboard';
        $this->menu[2]['permission_url'] = 'it/sim';
        $this->menu[2]['site_group'] = 1;

    }
    
    public function index()
    {
        $this->load->helper('html','url');
        $this->load->model('sim/Item_model', 'mitem');
        $content['session'] = $this->state;
        $content['csrf_token_name'] = $this->security->get_csrf_token_name();
        $content['csrf_hash'] = $this->security->get_csrf_hash();
        $data['body']['page'] = 'Item List';
        $data['body']['session'] = $this->state;
        $data['body']['list_menu'] = $this->menu;
        $data['body']['option']['stylesheet'][0] = base_url().'assets/modules/DataTables/dataTables.bootstrap4.min.css';
        $data['body']['option']['stylesheet'][1] = base_url().'assets/modules/chosen_v1.8.7/chosen.min.css';
        $data['body']['option']['js'][0] = base_url().'assets/modules/DataTables/datatables.min.js';
        $data['body']['option']['js'][1] = base_url().'assets/modules/DataTables/dataTables.bootstrap4.min.js';
        $data['body']['option']['js'][2] = base_url().'assets/modules/chosen_v1.8.7/chosen.jquery.min.js';
        $data['body']['option']['js'][3] = base_url().'assets/scripts/sim/item.js';
        $data['body']['content'] = $this->load->view('sim/contents/item/index', $content, TRUE);
        $this->load->view('sim/templates/sufee/template', $data['body']);
    }
}
/*
* End Home Class
*/