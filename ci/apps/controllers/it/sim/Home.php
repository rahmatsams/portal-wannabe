<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
* Default Controller
*/
class Home extends SISTER_Controller
{
    public $skipCheck = ['index'];
    
    public function __construct()
    {
        parent::__construct();
        $this->session->isLoggedIn(true);
        session_write_close();
        $this->menu = array();
        $this->menu[0]['permission_site'] = 2;
        $this->menu[0]['site_name'] = 'Administrator';
        $this->menu[0]['site_icon'] = 'fa-cog';
        $this->menu[0]['permission_order'] = 0;
        $this->menu[0]['permission_name'] = 'Dashboard';
        $this->menu[0]['permission_url'] = 'it/sim';
        $this->menu[0]['site_group'] = 1;

        $this->menu[1]['permission_site'] = 2;
        $this->menu[1]['site_name'] = 'Administrator';
        $this->menu[1]['site_icon'] = 'fa-user';
        $this->menu[1]['permission_order'] = 1;
        $this->menu[1]['permission_name'] = 'Manage User';
        $this->menu[1]['permission_url'] = 'it/sim';
        $this->menu[1]['site_group'] = 1;

        $this->menu[2]['permission_site'] = 3;
        $this->menu[2]['site_name'] = 'sim';
        $this->menu[2]['site_icon'] = 'fa-user';
        $this->menu[2]['permission_order'] = 0;
        $this->menu[2]['permission_name'] = 'Dashboard';
        $this->menu[2]['permission_url'] = 'it/sim';
        $this->menu[2]['site_group'] = 1;

    }
    
    public function index($page=1)
    {
        $this->load->helper('html','url');
        #$this->load->model('sim/Ticket_model', 'mticket');
        #$data['content']['opt']['order'] = array('ticket_id', 'DESC');
        #$data['content']['opt']['result'] = 25;
        #$data['content']['opt']['page'] = is_numeric((strlen($page) < 5 ? $page : 1)) ? $page : 1;
        #$data['content']['ticket'] = $this->mticket->getHome($data['content']['opt']);
        $data['content']['session'] = $this->state;
        $data['body']['page'] = 'Dashboard';
        $data['body']['session'] = $this->state;
        $data['body']['list_menu'] = $this->menu;
        $data['body']['option']['js'][2] = base_url().'assets/scripts/portal/portal.js';
        $data['body']['content'] = $this->load->view('sim/contents/home/index', $data['content'], TRUE);
        $this->menu[2]['active'] = 1;
        #exit(json_encode($this->menu));
        $this->load->view('sim/templates/sufee/template', $data['body']);
    }

}
/*
* End Home Class
*/