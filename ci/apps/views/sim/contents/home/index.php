
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><i class="fa fa-home"></i>  Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=site_url('it/hitams')?>">Hitams</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content p-2 m-1 row">
            <div class="col-lg-4 col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <img class="rounded-circle mx-auto d-block" src="<?=base_url()?>/assets/images/profiles/unknown.png" alt="Card image cap">
                            <h5 class="text-sm-center mt-2 mb-1"><?=$session['user_name']?></h5>
                            <div class="location text-sm-center"><i class="fa fa-key"></i> <?=$session['group']?></div>
                        </div>
                        <hr>
                        <div class="card-text text-sm-center">
                            <a href="#"><i class="fa fa-facebook pr-1"></i></a>
                            <a href="#"><i class="fa fa-twitter pr-1"></i></a>
                            <a href="#"><i class="fa fa-linkedin pr-1"></i></a>
                            <a href="#"><i class="fa fa-pinterest pr-1"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="col-md-6 col-lg-6">
                    <div class="card bg-flat-color-1 text-light">
                        <div class="card-body">
                            <div class="h4 m-0">89.9%</div>
                            <div>ITEMS</div>
                            <div class="progress-bar bg-light mt-2 mb-2" role="progressbar" style="width: 20%; height: 5px;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                            <a href="<?=base_url('it/hitams/home/item')?>"><small class="text-light">View All Item</small></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>