
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1><i class="fa fa-home"></i>  Item List</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=site_url('it/sim')?>">SIM</a></li>
                            <li class="active">Item List</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content p-2 m-1 row">
            <div class="card bg-secondary mb-3" style="width: 18rem;">
                <div class="card-header text-white">
                    <strong class="card-title">Advanced Search</strong>
                </div>
                <div class="card-body">
                    <form id="advSearch" method="POST" action="<?=base_url('it/sim/restful/item/get')?>">
                        <div class="row">
                            <div class="form-group">
                                <label class="form-control-label text-white">Serial Number</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                    <input class="form-control" name="item_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" form-control-label text-white">Outlet</label>
                                <select data-placeholder="Select Outlet ..." class="standardSelect" tabindex="5">
                                    <option value=""></option>
                                    <optgroup label="NFC EAST">
                                        <option>Dallas Cowboys</option>
                                        <option>New York Giants</option>
                                        <option>Philadelphia Eagles</option>
                                        <option>Washington Redskins</option>
                                    </optgroup>
                                    <optgroup label="NFC NORTH">
                                        <option>Chicago Bears</option>
                                        <option>Detroit Lions</option>
                                        <option>Green Bay Packers</option>
                                        <option>Minnesota Vikings</option>
                                    </optgroup>
                                    <optgroup label="NFC SOUTH">
                                        <option>Atlanta Falcons</option>
                                        <option>Carolina Panthers</option>
                                        <option>New Orleans Saints</option>
                                        <option>Tampa Bay Buccaneers</option>
                                    </optgroup>
                                    <optgroup label="NFC WEST">
                                        <option>Arizona Cardinals</option>
                                        <option>St. Louis Rams</option>
                                        <option>San Francisco 49ers</option>
                                        <option>Seattle Seahawks</option>
                                    </optgroup>
                                    <optgroup label="AFC EAST">
                                        <option>Buffalo Bills</option>
                                        <option>Miami Dolphins</option>
                                        <option>New England Patriots</option>
                                        <option>New York Jets</option>
                                    </optgroup>
                                    <optgroup label="AFC NORTH">
                                        <option>Baltimore Ravens</option>
                                        <option>Cincinnati Bengals</option>
                                        <option>Cleveland Browns</option>
                                        <option>Pittsburgh Steelers</option>
                                    </optgroup>
                                    <optgroup label="AFC SOUTH">
                                        <option>Houston Texans</option>
                                        <option>Indianapolis Colts</option>
                                        <option>Jacksonville Jaguars</option>
                                        <option>Tennessee Titans</option>
                                    </optgroup>
                                    <optgroup label="AFC WEST">
                                        <option>Denver Broncos</option>
                                        <option>Kansas City Chiefs</option>
                                        <option>Oakland Raiders</option>
                                        <option>San Diego Chargers</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-sm btn-primary rounded" name="submit_aja" value="Search">
                    </form>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                  
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tableData" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Serial Number</th>
                                        <th>Type</th>
                                        <th>Current User</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div> 
                  
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var items = '<?=base_url('it/sim/restful/item/get')?>';
            var ctn = '<?=$csrf_token_name?>';
            var ch = '<?=$csrf_hash?>';
        </script>