<?php
        $pages = ceil($ticket['total']/$opt['result']);
        if($pages > 1){
            $page_start = 1;
            $page_end = $pages;
            if($pages > 9 && $opt['page'] > 9)
            {
                $page_start = (($opt['page']-4) < 1 ? 1 : $opt['page']-4);
                $page_end = (($opt['page']+4) > $pages ? $pages : ($opt['page']+4));
            }
            elseif($pages > 9 && $opt['page'] <= 9)
            {
                $page_start = (($opt['page']-4) < 1 ? 1 : $opt['page']-4);
                $page_end = (($opt['page']+4) > 9 ? ($opt['page']+4) : 9);
            }
            elseif($pages < 9 && $opt['page'] <= 9)
            {
                $page_start = (($opt['page']-4) < 1 ? 1 : $opt['page']-4);
                $page_end = $pages;
            }
        }
?>    
    
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-home"></i>  Ticket Center</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="card text-white bg-info collapse fade" id="collapseExample">
        <div class="card-body ">
            <form id="advanceSearch">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-3 form-group">
                            <label>Title</label>
                            <input id="ticketPage" name="page" type="hidden" value="1">
                            <input id="ticketTitle" name="title" type="text" placeholder="Type ticket subject.." class="form-control" maxlength="25">
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Status</label>
                            <select id="ticketStatus" name="status" class="form-control">
                                <option value="" selected>-</option>
                                <option value="1">Open</option>
                                <option value="2">On-Progress</option>
                                <option value="5">Solved</option>
                                <option value="7">Closed</option>
                                <option value="8">Force-Closed</option>
                            </select>
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Outlet</label>
                            <select name="store_name" id="storeInput" class="form-control">
                                <option value="" selected>-</option>
                        <?php foreach($outlet_list as $outlet):?>                                        
                                <option value="<?=$outlet['store_name']?>"><?=$outlet['store_name']?></option>
                        <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Category</label>
                            <select name="main_category" id="mainCategory" class="form-control">
                                <option value="" selected>-</option>
                            <?php foreach($main_category as $mc): ?>
                                <option value="<?=$mc['category_id']?>"><?=$mc['category_name']?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Sub Category</label>
                            <select name="sub_category" class="form-control" id="subCategory" disabled>
                                <option value="" selected>-</option>
                            </select>
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>PIC</label>
                            <select name="pic" id="picInput" class="form-control">
                                <option value="" selected></option>
                            <?php foreach($pic_list as $pic):?>
                                <option value="<?=$pic['user_id']?>"><?=$pic['display_name']?></option>
                            <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 form-group">
                            <label>Submit date from</label>
                            <input id="ticketDateFirst" name="date_first" type="date" class="form-control" maxlength="10">
                        </div>                            
                        <div class="col-sm-3 form-group">
                            <label>to date</label>
                            <input id="ticketDateLast" name="date_last" type="date" class="form-control" maxlength="10">
                        </div>
                    </div>
                    
                    <button type="submit" name="submit" value="kirim" class="btn btn-primary">Search</button>
                </div>
            </form> 
        </div>
    </div>
    
	<div class="col-md-12">
        <div class="row form-inline mb-2">
            <div class="col-md-2 form-group">
                <a class="btn btn-success" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-search"></i> Advanced Search</a>
            </div>
            <div class="col-md-3 offset-md-7 form-group">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fa fa-list"></i></div>
                    </div>
                    <select id="sortResult" name="sort_result" class="form-control">
                        <option value="">Sort</option>
                        <option value="ticket_asc">Ticket ID ▲</option>
                        <option value="ticket_desc">Ticket ID ▼</option>
                        <option value="title_asc">Title ▲</option>
                        <option value="title_desc">Title ▼</option>
                        <option value="outlet_asc">Outlet ▲</option>
                        <option value="outlet_desc">Outlet ▼</option>
                        <option value="submit_asc">Submit Date ▲</option>
                        <option value="submit_desc">Submit Date ▼</option>
                        <option value="category_asc">Category ▲</option>
                        <option value="category_desc">Category ▼</option>
                        <option value="pic_asc">PIC ▲</option>
                        <option value="pic_desc">PIC ▼</option>
                        <option value="status_asc">Status ▲</option>
                        <option value="status_desc">Status ▼</option>
                    </select>
                </div>
                
            </div>
            
        </div>
        <div class="card text-dark bg-light">
            <div class="card-body ">
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr class="center-title">
                            <th>#</th>
                            <th>Subject</th>
                            <th>Owner</th>
                            <th>Outlet</th>
                            <th>Status</th>
                            <th>Submit Date</th>
                            <th>Last Update</th>
                        </tr>
                    </thead>
                    <tbody id="listTable">
                        <?php
                            if(count($ticket['data']) > 0){
                                foreach($ticket['data'] as $result){
                                    $date_check = '';
                                    $diff_seconds  = ($result['resolved_date'] != '0000-00-00 00:00:00' ? strtotime($result['resolved_date']) - strtotime($result['submit_date']) : strtotime(date("Y-m-d H:i:s")) - strtotime($result['submit_date']));
                                    $stat = floor($diff_seconds/3600);
                                    
                                    if(($stat >= 24 && $stat < 48) && $result['status'] < 5){
                                        
                                        $date_check = "bg-warning text-white";
                                    }elseif($stat >= 48 && $result['status'] < 5){
                                        $date_check = "bg-danger text-white";
                                    }elseif($result['status'] >= 5){
                                        $date_check = "bg-success text-white";
                                    }
                                    $submit_date = date("d-m-Y", strtotime($result['submit_date']));
                                    $last_update = (!empty($result['last_update']) ? date("d-m-Y", strtotime($result['last_update'])) : '-');
                                    
                                    echo "
                        <tr>
                            <td>{$result['ticket_id']}</td>
                            <td><a href=\"./ticket/detail/{$result['ticket_id']}\" target=\"_blank\" class='text-dark font-weight-bold'>{$result['ticket_title']}</a></td>
                            <td>{$result['owner']}</td>
                            <td>{$result['store']}</td>
                            <td class=\"{$date_check}\">{$result['status_name']}</td>
                            <td>{$submit_date}</td>
                            <td>{$last_update}</td>
                        </tr>";
                                }
                            } else {
                        ?>
                        <tr>
                            <td colspan="9">There's no Ticket</td>
                        </tr>
                        <?php
                            }
                            
                        ?>
                    </tbody>
                </table>
<?php if($opt['page'] > 1): ?>
                
                <nav aria-label="Page navigation example" id="pagingBottom">
                    <ul class="pagination text-dark">
<?php if(($opt['page'] - 5) > 0):?>
                <li class='page-item'><a href="./1" class='page-link' data-id='1'>1</a></li>
                <li class='page-item'><a class='page-link'>...</a></li>
<?php endif;
for($i=$page_start;$i <= $page_end;$i++):?>
                        <li class="page-item<?=($i == $opt['page'] ? ' active' : '')?>"><a href="./hd/home/index/<?=$i?>" class='page-link' data-id="<?=$i?>"><?=$i?></a></li>
<?php endfor;?>
                        <li class="page-item"><a class='page-link'>...</span></a></li>
                        <li class="page-item"><a href="./hd/home/index/<?=$pages?>" class='page-link page-navigate' data-id='<?=$pages?>'><?=$pages?></a></li>
                        </ul>
                    </nav>
<?php endif;?>
                
            </div>
            <div class="card-footer">  
                <a class="btn btn-primary" role="button" href="export_data.html" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-file-excel-o"></i>  Export Data</a>
            </div>
        </div>
    </div>