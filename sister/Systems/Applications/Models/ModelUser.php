<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelUser extends Model
    {
		public $_result = array();
		private $table_field = array('user_id','user_name','user_password','group_id');        
        
		function doLogin($input)
        {
			$query = "SELECT user_id, display_name, user_password, role_name, portal_user.role_id, email, store_id, gm_email, gm_name, gm_position FROM portal_user LEFT JOIN ";
            $query .= "user_role USING (role_id) ";
            $query .= "LEFT JOIN staff_department USING(department_id) ";
            $query .= "WHERE email=:email";
            $result = $this->fetchSingleQuery($query, $input);
			return $result;
		}
        
        function allowedSite($input)
        {
            $query = "SELECT site_id,is_admin FROM (SELECT ps.site_id
        FROM sushitei_portal.portal_permission pp 
        INNER JOIN sushitei_portal.role_permission rp ON rp.permission=pp.permission_id 
        INNER JOIN sushitei_portal.portal_sites ps ON ps.site_id=pp.permission_site 
        WHERE rp.role=:role_id 
        AND pp.permission_type=1 
        AND pp.permission_code='siteAccess') X1 
LEFT JOIN (SELECT (pp2.permission_id IS NOT NULL) AS is_admin, ps2.site_id AS situs
        FROM sushitei_portal.portal_permission pp2 
        INNER JOIN sushitei_portal.role_permission rp2 ON rp2.permission=pp2.permission_id 
        INNER JOIN sushitei_portal.portal_sites ps2 ON ps2.site_id=pp2.permission_site 
        WHERE rp2.role=:role_id 
        AND pp2.permission_type=1 
        AND pp2.permission_code='isAdmin') X2 ON X2.situs=X1.site_id";
            
            $result = $this->fetchAllQuery($query, $input);
			return $result;
            
        }
	}
?>