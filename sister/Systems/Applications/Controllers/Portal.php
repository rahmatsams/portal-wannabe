<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Portal extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewIndex', 'viewLogin', 'viewLoginProccess', 'viewLogout', 'viewDeleteUser'),
                'role'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Guest'),
            ),
            
        );
    }
    
    public function viewLogin()
    {
        print_r($_SESSION);
    }
    
    public function viewLoginProccess()
    {
		if($this->isGuest()){
            $current_attempt = $this->getSession('attempt');
            $recaptcha = 1;
            if($current_attempt > 5){
                #
                # Verify captcha
                $post_data = http_build_query(
                    array(
                        'secret' => '6LemwyETAAAAABp7aR2EScUPsLoLwCSHPvGMQTpH',
                        'response' => $_POST['g-recaptcha-response'],
                        'remoteip' => $_SERVER['REMOTE_ADDR']
                    )
                );
                $opts = array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $post_data
                    )
                );
                $context  = stream_context_create($opts);
                $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
                $result = json_decode($response);
                if (!$result->success) {
                    $recaptcha = 0;
                }
			}
            if(isset($_POST['ticket_email']) && isset($_POST['ticket_password']) && $recaptcha == 1){
                $input = $this->load->lib('Input');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'min=1', 'User name should be filled');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'email', 'Wrong User Name format');
                $input->addValidation('ticket_password', $_POST['ticket_password'], 'min=1', 'Password must be filled');
                if ($input->validate()) {
                    $login_data = array(
                        'email' => $_POST['ticket_email'],
                    );
                    $model_users = $this->load->model('User');
                    $result = $model_users->doLogin($login_data);
                    if(is_array($result) && count($result) > 0){
                        if (password_verify($_POST['ticket_password'], $result['user_password'])) {
                            $s = array(
                                'user_id'  => $result['user_id'],
                                'user_name'     => $result['display_name'],
                                'outlet'   => $result['store_id'],
                                'group'      => $result['role_name'],
                                'email'      => $result['email'],
                                'gm_email'      => $result['gm_email'],
                                'gm_position'      => $result['gm_position'],
                                'gm_name'      => $result['gm_name'],
                                'ci_session' => $_POST['api_key'],
                                'attempt'      => 0
                            );
                            foreach($model_users->allowedSite(array('role_id' => $result['role_id'])) as $site){
                                $s['site'][$site['site_id']] = (empty($site['is_admin']) ? 0 : 1);
                            }
                            $this->setSession($s);
                            
                            $return = array(
                                'success' => 1,
                                'session' => $_SESSION,
                                'last_url' => (isset($_POST['last_url']) ? $_POST['last_url'] : '')
                            );
                        }else{
                            $return = array(
                                'success' => 0,
                                'message' => 'Login Failed'
                            );
                        }
                    } else {
                        $current_attempt++;
                        $this->setSession('attempt', $current_attempt);
                        $return = array(
                            'login_attempt' => $current_attempt,
                            'message' => "Username or Password does not match.",
                            'success' => 0
                        );
                        
                    }
                } else {
                    $current_attempt++;
                    $this->setSession('attempt', $current_attempt);
                    $return = array(
                        'login_attempt' => $current_attempt,
                        'error' => $input->_error,
                        'success' => 0
                    );
                }
            
            } else {
                $current_attempt++;
                $return = array(
                    'login_attempt' => $current_attempt,
                    'success' => 0,
                    'data' => $_POST
                );
                
            }
        
            
		}else{
            $return = array(
                'success' => 1
            );
        }
        header("Access-Control-Allow-Origin: http://portal.sushitei.co.id");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json");
        echo json_encode($return);
	}
    
    public function viewLogout()
    {        
        $_SESSION = array();
        header("Access-Control-Allow-Origin: http://portal.sushitei.co.id");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json");
        echo json_encode(1);
    }

    #delete User
    public function viewDeleteUser()
    {
        $id = $_GET['idUser'];
        $model_users = $this->load->model('User');
        $model_users->deleteUser($id);
        header('Location: admin_user.html');
    }

}
/*
* End Home Class
*/