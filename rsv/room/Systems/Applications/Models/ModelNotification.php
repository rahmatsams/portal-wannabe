<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelNotification extends Model
	{
		
		public function getNotification($input)
		{
			$q = "SELECT COUNT(*) AS total FROM event_notification WHERE notification_user=:user_id AND notification_status=0";
			
			return $this->fetchSingleQuery($q, $input);
		}
		
        public function newNotification($input)
        {
			$t = 'event_notification';
			return $this->insertQuery($t, $input);
		}
        public function clearNotification($w)
        {
			$t = 'event_notification';
			return $this->editQuery($t,array('notification_status' => 1), $w);
		}
        
	}