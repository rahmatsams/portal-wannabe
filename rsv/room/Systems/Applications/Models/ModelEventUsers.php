<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelEventUsers extends Model
	{
		
		public function getEUbyEvent($o)
		{
			$q = "SELECT * FROM event_user WHERE event_id=:event_id";
			
			return $this->fetchAllQuery($q, $o);
		}
		
		public function getEUby($input)
		{
			$q = "SELECT *, 
					a.meal_name AS meal1, 
					b.meal_name AS meal2, 
					a.meal_price AS price1, 
					b.meal_price AS price2  
					FROM event_user 
					LEFT JOIN meal a ON a.meal_id=event_user.euser_value 
					LEFT JOIN meal b ON b.meal_id=event_user.euser_value 
					WHERE 
						event_id=:event_id";

			return $this->fetchSingleQuery($q, $input);
		}
		
		public function insertEUserOption($input){
			$t = 'event_user';
			return $this->insertQuery($t, $input);
		}

		public function editEventUsers($form = array(), $where = array())
		{
			$table = 'event_user';
			$result = $this->editQuery($table, $form, $where);
			return $result;
		}
	}