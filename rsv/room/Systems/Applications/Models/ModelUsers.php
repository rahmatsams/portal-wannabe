<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class ModelUsers extends Model{
		public $_result = array();
		private $table_field = array('user_id','user_name','user_password','group_id');
		
		function doLogin($input){
			$query = "SELECT
                        gm_email,
                        gm_name,
                        gm_position, 
                        user_id, 
                        display_name, 
                        group_name, 
                        email, 
                        level, 
                        store_id,
                        event_department.department_id 
                        FROM sushitei_portal.portal_user 
                        LEFT JOIN user_groups USING (group_id)
                        LEFT JOIN event_department ON sushitei_portal.portal_user.department_id=event_department.department_id 
                        WHERE 
                            user_name=:user_name 
                            AND user_password=:user_password";
            $result = $this->fetchSingleQuery($query, $input);
			return $result;
		}
        
        function checkForgotRequirement($input){
			$query = "SELECT * FROM sushitei_portal.portal_user WHERE user_name=:user_name AND security_question=:security_question AND security_answer=:security_answer";
            $result = $this->fetchSingleQuery($query, $input);
			return $result;
		}
        
        function newUser($input)
        {
            
            try{
                $this->insertQuery('sushitei_portal.portal_user', $input);
                return 1;
            } catch(Exception $e){
                if(getConfig('development') == 1){
                    echo $e;
                    exit;
                }
                return 0;
            }
        }
        
        function newTicketUser($input)
        {
            $result = $this->insertQuery('sushitei_portal.portal_user', $input['new_user']);
            return $result;
        }
        
        function editTicketUser($input = array()){
            $result = $this->editQuery('sushitei_portal.portal_user', $input['sushitei_portal.portal_user'], $input['where']);
            return $result;

        }
        
        function checkUserName($input){ //cek is username already exist
			$query = $this->prepare("SELECT user_name FROM sushitei_portal.portal_user WHERE user_name=:username");
			$query->bindParam(':username',$input);
			$query->execute();
			if($query->rowCount() > 0){
				return 0;
			}else{
				return 1;
			}
        }

        function getAllNotAdmin(){
			$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE group_id=4";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}

        function getAllUser2(){
			$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}

        function getAllUser3()
        {
            $query = "SELECT * FROM sushitei_portal.portal_user WHERE user_status=1";
            $result = $this->fetchAllQuery($query);
            return $result;
        }
        
        function getAllNotUser(){
			$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE group_id NOT IN ('4')";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllEngineer(){
			$query_string = "SELECT user_id,display_name FROM sushitei_portal.portal_user WHERE group_id=3";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllUser($query_option)
        {
            $query_string = "SELECT  user_id, user_name, display_name, email, sushitei_portal.portal_user.department_id, user_groups.group_name, event_department.department_name, user_status ";
            $query_string .= "FROM sushitei_portal.portal_user LEFT JOIN user_groups ON sushitei_portal.portal_user.group_id=user_groups.group_id LEFT JOIN event_department ON event_department.department_id=sushitei_portal.portal_user.department_id";
            $count_query = "SELECT COUNT(*) AS row_total FROM sushitei_portal.portal_user";
            
            $result = $this->pagingQuery($query_string, $count_query, $query_option);
            return $result;
        }
        
        function countAllUser()
        {
            $query_string = "SELECT COUNT(*) AS total FROM sushitei_portal.portal_user";
            
            $result = $this->doQuery($query_string);
            return $result['total'];
        }
        
		function getUserBy($input)
        {
            $query_string = "SELECT  user_id, user_name, user_password, display_name, store_id, department_id, email, user_groups.group_name, sushitei_portal.portal_user.group_id, user_status, level ";
            $query_string .= "FROM sushitei_portal.portal_user LEFT JOIN user_groups ON sushitei_portal.portal_user.group_id=user_groups.group_id ";
            $query_string .= "WHERE user_id=:user_id";            
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }
        
        function editUser($form = array(), $where = array()){
            $table = 'sushitei_portal.portal_user';
            try{
                $this->editQuery($table, $form, $where);
                return 1;
            } catch(Exception $e){
                return $e;
            }
                
        }
		
		/*function deleteUser($id){
            #$query = $this->doQuery("UPDATE sushitei_portal.portal_user SET user_status=2 WHERE user_id=$id");
			$query = $this->doQuery("DELETE FROM sushitei_portal.portal_user WHERE user_id=$id")
			/*$result = $this->deleteQuery('sushitei_portal.portal_user' ,$id);
			return $result;
		}*/

        function deleteUser($id)
        {
            $query_string = $this->doQuery("DELETE FROM sushitei_portal.portal_user WHERE user_id=$id");
        }
        
        function getAllGroups(){
			$query_string = "SELECT group_id, group_name FROM user_groups";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
		function getAllActiveStore()
        {
            $query_string = "SELECT * FROM store WHERE store_status=1";
            
            $result = $this->fetchAllQuery($query_string);
            return $result;
        }
	}
?>