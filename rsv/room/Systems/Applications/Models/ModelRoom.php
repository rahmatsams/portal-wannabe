<?php
/*
* Model for Ticketdesk
*/
class ModelRoom extends Model
{
	
    function getAllActiveRoom()
    {
        $query_string = "SELECT * FROM room WHERE room_status=1";
		
		$result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function newRoom($input)
    {
        $result = $this->insertQuery('room', $input);
        return $result;
    }
	
	function getRoomByID($input)
    {
        $query_string = "SELECT * FROM room WHERE room_id=:room_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    function getAllRoom()
    {
        $query_string = "SELECT * FROM room ";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function deleteRoom($id)
    {
        $query = $this->doQuery("UPDATE room SET room_status=2 WHERE room_id=$id");
    }
    
    function getStoreBy($input)
    {
        $query_string = "SELECT * from store ";
        $query_string .= "WHERE store_id=:store_id";            
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    
    function editRoom($form = array(), $where = array()){
        $table = 'room';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }
    
    function deleteStore($id){
        
        $result = $this->deleteQuery('room' ,$id);
        return $result;
        
    }
    
}