<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
*   Class for Home Controller
*/
class ModelHome extends Model
{
    function getLatestNews()
    {
        $query_string = 'SELECT 
                            news_id,
                            news_title,
                            time_submit,
                            news_content 
                            FROM news 
                            WHERE 
                                front = 1 
                                ORDER BY news_id DESC LIMIT 0,5';
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    function Sub()
    {
        $query_string = 'SELECT * 
                        FROM events 
                        WHERE front = 1 
                        ORDER BY event_start ASC LIMIT 0,5';
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    function getMvpProducts()
    {
        $query_string = 'SELECT 
                            product_id,
                            product_name,
                            product_images,
                            product_description 
                            FROM products 
                            WHERE
                                mvp = 1 
                                ORDER BY product_id DESC LIMIT 0,5';
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    function getOpenRecruitment()
    {
        $query_string = 'SELECT * 
                        FROM company_position 
                        WHERE 
                            open_recruit = 1 ORDER BY position_name';
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    function getRecruitRequirement($input)
    {
        $query_string = 'SELECT requirement_detail FROM position_requirement WHERE position_id = :pid';
        $query_value = array(
            'pid' => $input
        );
        $result = $this->fetchAllQuery($query_string, $query_value);
        return $result;
    }
}
?>