<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelEventOptions extends Model
	{
		
		public function getActiveOption()
		{
			$q = "SELECT * FROM event_options WHERE option_status=1 ORDER BY option_category ASC";
			
			return $this->fetchAllQuery($q);
		}
		public function getAllOption()
		{
			$q = "SELECT * FROM event_options LEFT JOIN event_option_category ON event_option_category.category_id=event_options.option_category ORDER BY option_category, option_name";
			
			return $this->fetchAllQuery($q);
		}
        public function getAllCategory()
		{
			$q = "SELECT * FROM event_option_category";
			
			return $this->fetchAllQuery($q);
		}
        
		/*public function getOverlapDate($i)
		{
			$q = "SELECT 
					event_id,
					event_name,
					room.room_name,
					room.room_color,
					start_time,
					end_time 
					FROM events 
					LEFT JOIN room ON events.room_id=room.room_id 
					WHERE 
						events.room_id=:room 
					AND start_time 
					BETWEEN :start_time 
					AND :end_time 
					OR end_time 
					BETWEEN :start_time 
					AND :end_time 
					OR :start_time 
					BETWEEN start_time 
					AND end_time 
					OR :end_time 
					BETWEEN start_time 
					AND end_time";
			
			return $this->fetchAllQuery($q, $i);
		}*/
		
		public function newOption($input)
        {
            $result = $this->insertQuery('event_options', $input);
            return $result;
        }
        
        public function getOptionByID($input)
        {
            $query_string = "SELECT * FROM event_options WHERE option_id=:option_id";
            
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }

        public function getEventUserByEventIDAndOptionID($input)
        {
            $query_string = "SELECT * FROM event_user WHERE event_id=:event_id AND option_id=:option_id";
            
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }
        

		public function insertEUserOption($input)
		{
			$t = 'event_user';
			return $this->insertQuery($t, $input);
			#var_dump($t, $input);
		}
		
		public function editEventUsers($form = array(), $where = array())
		{
			$table = 'event_user';
			$result = $this->editQuery($table, $form, $where);
			return $result;
		}

		public function getOptionType($number)
		{
			$q = "SELECT * FROM event_options WHERE option_category={$number} AND option_status=1";
			
			return $this->fetchAllQuery($q);
		}	
		
		public function deleteEUserOption($input) //QUERY UNTUK EDIT MEAL
		{
			$q = 'DELETE FROM event_user WHERE event_id=:event_id AND option_id=:option_id';
			return $this->doQuery($q, $input);
		}
        
        function editOption($form = array(), $where = array()){
            $table = 'event_options';
            $result = $this->editQuery($table, $form, $where);
            return $result;
        }
        
        function deleteOption($id){
            
        	$query = $this->doQuery("UPDATE event_options SET option_status=2 WHERE option_id=$id");

        	#$result = $this->deleteQuery('event_options' ,$id);
        	#return $result;
            
        }
	}