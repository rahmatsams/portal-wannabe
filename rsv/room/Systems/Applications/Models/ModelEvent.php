<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelEvent extends Model
	{
		
		public function getEventRangeUser($input)
		{
			$q = "SELECT 
                    event_id,
                    event_name,
                    room.room_name,
                    display_name,
                    room.room_color,
                    date_from,
                    date_until,
                    time_from,
                    time_until,
                    event_status 
                    FROM events 
                    LEFT JOIN room ON events.room_id=room.room_id 
                    LEFT JOIN sushitei_portal.portal_user ON event_creator=sushitei_portal.portal_user.user_id 
                    WHERE 
                        event_status=1 
                        AND (date_from BETWEEN :st1 
                        AND :st2 
                        OR date_until BETWEEN :ed1 
                        AND :ed2)";
			
			return $this->fetchAllQuery($q, $input);
		}
		
		public function getOverlapDate($i)
		{
			$q = "SELECT 
                    event_id,
                    event_name,
                    room.room_name,
                    display_name,
                    room.room_color,
                    date_from,
                    date_until,
                    time_from,
                    time_until
                    FROM events 
                    LEFT JOIN room ON events.room_id=room.room_id 
                    LEFT JOIN sushitei_portal.portal_user ON event_creator=sushitei_portal.portal_user.user_id 
                    WHERE 
                        event_status=1 
                        AND events.room_id=:room 
                        AND (time_from BETWEEN :time_from 
                        AND :time_until 
                        OR time_until BETWEEN  :time_from 
                        AND :time_until 
                        OR :time_from BETWEEN time_from 
                        AND time_until 
                        OR :time_until BETWEEN time_from 
                        AND time_until 
                        OR time_from=:time_from 
                        OR time_until=:time_until)
                        AND (date_from BETWEEN :date_from 
                        AND :date_until 
                        OR date_until BETWEEN  :date_from 
                        AND :date_until 
                        OR :date_from BETWEEN date_from 
                        AND date_until 
                        OR :date_until BETWEEN date_from 
                        AND date_until 
                        OR date_from=:date_from 
                        OR date_until=:date_until)";
			
			return $this->fetchAllQuery($q, $i);
		}
		
		public function newEvent($input)
        {
			$t = 'events';
			return $this->insertQuery($t, $input);
		}

        public function newEventGroups($input)
        {
            $t = 'event_groups';
            return $this->insertQuery($t, $input);
        }

        public function newEventPerson($input)
        {
            $t = 'event_person';
            return $this->insertQuery($t, $input);
        }
        
        public function getUnconfirmedEvent($f, $opt)
		{
			$q = "SELECT 
                    event_id,
                    event_name,
                    status_name,
                    display_name,
                    room_name,
                    room_capacity,
                    date_format(submit_time,'%d-%m-%Y') AS book_date,
                    date_format(time_from,'%H:%i') AS start,
                    date_format(time_until,'%H:%i') AS end 
                    FROM view_events 
                    WHERE 
                        date_from >= CURDATE() 
                        AND event_status=2 ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE date_from >= CURDATE() AND event_status=2 ";
            foreach($f as $key => $value){
                $q .=  " AND {$key} LIKE '%{$value}%' ";
                $c .= " AND {$key} LIKE '%{$value}%' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getFutureEvent($f, $opt)
		{
			$q = "SELECT 
                    event_id,
                    event_name,
                    status_name,
                    display_name,
                    room_name,
                    room_capacity,
                    date_format(date_from,'%d-%m-%Y') AS book_date,
                    date_format(date_from,'%d-%m-%Y') AS start,
                    date_format(date_until,'%d-%m-%Y') AS end 
                FROM view_events 
                WHERE date_until >= CURDATE() 
                    AND event_status=1 ";

            $c = "SELECT COUNT(*) row_total 
                FROM view_events 
                WHERE date_until >= CURDATE() #future
                    AND event_status=1 ";

            foreach($f as $key => $value){
                $q .=  " AND {$key} LIKE '%{$value}%' ";
                $c .= " AND {$key} LIKE '%{$value}%' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getPastEvent($f, $opt)
		{
            $q = "SELECT 
                    event_id,
                    event_name,
                    status_name,
                    display_name,
                    room_name,
                    room_capacity,
                    date_format(date_from,'%d-%m-%Y') AS book_date,
                    date_format(date_from,'%d-%m-%Y') AS start,
                    date_format(date_until,'%d-%m-%Y') AS end 
                    FROM view_events 
                    WHERE 
                        date_from < CURDATE() ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE date_until > CURDATE() "; #past
            foreach($f as $key => $value){
                $q .=  " AND {$key} LIKE '%{$value}%' ";
                $c .= " AND {$key} LIKE '%{$value}%' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getEventBy($input)
        {
            $q = "SELECT 
                    u.display_name AS display_name,
                    e.event_creator,
                    u.email,
                    r.room_name AS room_name,
                    r.room_id AS room_id,
                    r.room_capacity AS room_capacity, 
                    e.submit_time, 
                    e.date_from, 
                    e.date_until,
                    e.time_from, 
                    e.time_until, 
                    et.event_type_id AS event_type_id,
                    et.event_type_name AS event_type, 
                    e.event_name,
                    e.event_id AS event_id,
                    snack.vendor_id AS vendor_id_snack, 
                    snack.vendor_name AS vendor_snack, 
                    snack.meal_id AS meal_id_snack,
                    snack.meal_name AS snack, 
                    snack.meal_price AS snack_price,
                    lunch.vendor_id AS vendor_id_lunch,
                    lunch.vendor_name AS vendor_lunch,
                    lunch.meal_id AS meal_id_lunch, 
                    lunch.meal_name AS lunch, 
                    lunch.meal_price AS lunch_price,
                    projector.projector,
                    flipchart.flipchart,
                    round_seat.round_seat,
                    column_seat.column_seat,
                    e.room_pax
                FROM events e 
                LEFT JOIN (SELECT * FROM event_user LEFT JOIN meal ON event_user.`euser_value`=meal.meal_id 
                LEFT JOIN meal_vendor ON meal_vendor.vendor_id=meal.meal_vendor WHERE option_id=7) lunch ON e.event_id=lunch.event_id 
                LEFT JOIN (SELECT * FROM event_user LEFT JOIN meal ON event_user.`euser_value`=meal.meal_id 
                LEFT JOIN meal_vendor ON meal_vendor.vendor_id=meal.meal_vendor WHERE option_id=1) snack ON e.event_id=snack.event_id
                LEFT JOIN (SELECT event_id,euser_value AS projector FROM event_user WHERE option_id=2 AND event_id=:event_id) projector ON e.event_id=projector.event_id 
                LEFT JOIN (SELECT event_id,euser_value AS flipchart FROM event_user WHERE option_id=8 AND event_id=:event_id) flipchart ON e.event_id=flipchart.event_id 
                LEFT JOIN (SELECT event_id,euser_value AS round_seat FROM event_user WHERE option_id=5 AND event_id=:event_id) round_seat ON e.event_id=round_seat.event_id 
                LEFT JOIN (SELECT event_id,euser_value AS column_seat FROM event_user WHERE option_id=6 AND event_id=:event_id) column_seat ON e.event_id=column_seat.event_id 
                LEFT JOIN room r ON r.room_id=e.room_id
                LEFT JOIN sushitei_portal.portal_user u ON u.user_id=e.event_creator
                LEFT JOIN event_type et ON et.event_type_id=e.event_type_id 
                WHERE e.event_id=:event_id";
            
            return $this->fetchSingleQuery($q, $input);
        }

        public function getEventByPerson($input) 
        {
            $q = "SELECT 
                    u.display_name AS display_name,
                    e.event_creator,
                    u.email,
                    r.room_name AS room_name,
                    r.room_id AS room_id,
                    r.room_capacity AS room_capacity, 
                    e.submit_time, 
                    e.date_from, 
                    e.date_until,
                    e.time_from, 
                    e.time_until, 
                    et.event_type_id AS event_type_id,
                    et.event_type_name AS event_type, 
                    e.event_name,
                    e.event_id AS event_id,
                    snack.vendor_id AS vendor_id_snack, 
                    snack.vendor_name AS vendor_snack, 
                    snack.meal_id AS meal_id_snack,
                    snack.meal_name AS snack, 
                    snack.meal_price AS snack_price,
                    lunch.vendor_id AS vendor_id_lunch,
                    lunch.vendor_name AS vendor_lunch,
                    lunch.meal_id AS meal_id_lunch, 
                    lunch.meal_name AS lunch, 
                    lunch.meal_price AS lunch_price,
                    projector.projector,
                    flipchart.flipchart,
                    round_seat.round_seat,
                    column_seat.column_seat,
                    e.room_pax,
                    ep.id_event_person,
                    ep.person_name
                FROM events e 
                LEFT JOIN (SELECT * FROM event_user LEFT JOIN meal ON event_user.`euser_value`=meal.meal_id 
                LEFT JOIN meal_vendor ON meal_vendor.vendor_id=meal.meal_vendor WHERE option_id=7) lunch ON e.event_id=lunch.event_id 
                LEFT JOIN (SELECT * FROM event_user LEFT JOIN meal ON event_user.`euser_value`=meal.meal_id 
                LEFT JOIN meal_vendor ON meal_vendor.vendor_id=meal.meal_vendor WHERE option_id=1) snack ON e.event_id=snack.event_id
                LEFT JOIN (SELECT event_id,euser_value AS projector FROM event_user WHERE option_id=2 AND event_id=:event_id) projector ON e.event_id=projector.event_id 
                LEFT JOIN (SELECT event_id,euser_value AS flipchart FROM event_user WHERE option_id=8 AND event_id=:event_id) flipchart ON e.event_id=flipchart.event_id 
                LEFT JOIN (SELECT event_id,euser_value AS round_seat FROM event_user WHERE option_id=5 AND event_id=:event_id) round_seat ON e.event_id=round_seat.event_id 
                LEFT JOIN (SELECT event_id,euser_value AS column_seat FROM event_user WHERE option_id=6 AND event_id=:event_id) column_seat ON e.event_id=column_seat.event_id 
                LEFT JOIN room r ON r.room_id=e.room_id
                LEFT JOIN sushitei_portal.portal_user u ON u.user_id=e.event_creator
                LEFT JOIN event_type et ON et.event_type_id=e.event_type_id
                LEFT JOIN event_person ep ON ep.id_event=e.event_id 
                WHERE e.event_id=:event_id";
            
            return $this->fetchSingleQuery($q, $input);
        }
        
        public function getMyEvent($input)
        {
            $q = "SELECT 
                    event_id,
                    event_name,
                    event_status,
                    display_name,
                    room.room_name,
                    room.room_capacity,
                    date_from,
                    date_until,
                    time_from,
                    time_until
                    #start_time,
                    #end_time 
                    FROM events 
                    LEFT JOIN room ON events.room_id=room.room_id 
                    LEFT JOIN sushitei_portal.portal_user ON sushitei_portal.portal_user.user_id=events.event_creator 
                    WHERE event_creator=:event_creator";
            return $this->fetchAllQuery($q, $input);
        }

        public function getAllPerson2($input)
        {
            $q = "SELECT 
                    person_name,
                    person_status,
                    date_groups,
                    id_event,
                    event_person.event_groups_id
                    FROM event_person
                    LEFT JOIN event_groups ON event_person.event_groups_id = event_groups.event_groups_id
                    WHERE event_person.id_event=:id_event AND event_groups.date_groups =:tanggal";
            return $this->fetchAllQuery($q, $input);
        }

        public function getAllPerson($input)
        {
            $q = "SELECT 
                    person_name,
                    department_name,
                    store_name,
                    person_status
                    FROM event_person
                    WHERE id_event=:id_event";
            return $this->fetchAllQuery($q, $input);
        }

        public function getDepartmentByUsername($input)
        {
            $q = "SELECT 
                    department_id
                    FROM sushitei_portal.portal_user
                    WHERE display_name=:username";
            return $this->fetchAllQuery($q, $input);
        }

        public function getGMByIdDepartment($input)
        {
            $q = "SELECT 
                    gm_position
                    FROM event_department
                    WHERE department_id=:department_id";
            return $this->fetchAllQuery($q, $input);
        }
		
		function editEvent($form = array(), $where = array())
		{
			$table = 'events';
			$result = $this->editQuery($table, $form, $where);
			return $result;
		}
		
		function getAdminAddress()
		{
			$q = "SELECT * FROM sushitei_portal.portal_user WHERE group_id=6";
            return $this->fetchAllQuery($q);
		}
        
        public function getEventList($f, $opt)
        {
            $q = "SELECT * FROM view_events ";
            $c = "SELECT COUNT(*) row_total FROM view_events ";
            $n = 1;
            foreach($f as $key => $value){
                if($n > 1){
                    
                    $q .= "AND {$key} LIKE '%{$value}%' ";
                    $c .= "AND {$key} LIKE '%{$value}%' ";
                    
                } else {
                   
                    $q .=  "WHERE {$key} LIKE '%{$value}%' ";
                    $c .= "WHERE {$key} LIKE '%{$value}%' ";
                   
                }
                $n++;
            }
            
            $result = $this->pagingQuery($q, $c, $opt);
            return $result;
        }

        public function getAllEvent2($title, $store_name, $date_first, $date_last)  #export
        {
            $q = "SELECT 
                    ep.person_name AS person_name,
                    tb.department_id,
                    tb.store_id,
                    aa.department_name,
                    bb.store_name,
                    ta.*,
                    tb.event_id AS event_id,
                    tb.event_name,
                    tb.status_name,
                    tb.room_capacity,
                    tb.display_name, 
                    tb.date_from,
                    tb.date_until,
                    tb.time_from,
                    tb.time_until,
                    tb.event_type,
                    tb.department_id,
                    DATE_FORMAT(tb.date_from,'%Y-%m-%d') AS book_date,
                    IF(tc.option_id IS NULL, '', 'Y') AS  snack,
                    IF(td.option_id IS NULL, '', 'Y') AS  lunch,
                    tdr.vendor_name AS vendor_snack,
                    tdq.meal_name AS meal_snack,
                    tdq.meal_price AS snack_price,
                    
                    tdv.vendor_name AS vendor_lunch,
                    tdm.meal_name AS meal_lunch,
                    tdm.meal_price AS lunch_price
                    
                FROM room ta
                LEFT OUTER JOIN view_events tb ON ta.room_name=tb.room_name
                LEFT JOIN event_department aa ON tb.department_id=aa.department_id
                LEFT JOIN store bb ON tb.store_id=bb.store_id

                LEFT JOIN event_user tc ON tb.event_id=tc.event_id AND tc.option_id=1
                LEFT JOIN meal tdq ON tc.`euser_value`=tdq.meal_id 
                LEFT JOIN meal_vendor tdr ON tdr.vendor_id=tdq.meal_vendor
                 
                LEFT JOIN event_user td ON tb.event_id=td.event_id AND td.option_id=7 
                LEFT JOIN meal tdm ON td.`euser_value`=tdm.meal_id 
                LEFT JOIN meal_vendor tdv ON tdv.vendor_id=tdm.meal_vendor 


                LEFT JOIN event_person ep ON ep.id_event=tb.event_id 
                WHERE 
                    tb.status_name='Approved' ";
                 if(!empty($title)){
                    $q .= " AND tb.event_name LIKE '%{$title}%' ";
                 }
                 if (!empty($store_name)) {
                     $q .= " AND user_id='".$store_name."' ";
                 }
                 if (!empty($date_first)) {
                     $q .= " AND (date_from >= '".date('Y-m-d', strtotime($date_first))."' ";
                 }
                 if (!empty($date_last)) {
                     $q .= " AND date_until <= '".date('Y-m-d', strtotime($date_last))."') ";
                 }    
                    
                $q .= " ";
            
            
            return $this->fetchAllQuery($q);
            //return $q;
        }

         /*public function getAllEvent2($title, $store_name, $date_first, $date_last)  #export
        {
            $q = "SELECT ta.*,
                    tb.event_id,
                    tb.event_name,
                    tb.status_name,
                    tb.room_capacity,
                    tb.display_name, 
                    tb.date_from,
                    tb.date_until,
                    tb.time_from,
                    tb.time_until,
                    tb.event_type,
                    DATE_FORMAT(tb.date_from,'%Y-%m-%d') AS book_date,
                IF(tc.option_id = 1 IS NULL, '', 'Y') AS  snack,
                IF(tc.option_id = 7 IS NULL, '', 'Y') AS  lunch,
                IF(tc.option_id = 2 IS NULL, '', 'Y') AS  projector,
                IF(tc.option_id = 8 IS NULL, '', 'Y') AS  flip,
                IF(tc.option_id = 5 IS NULL, '', 'Y') AS  ROUND,
                IF(tc.option_id = 6 IS NULL, '', 'Y') AS  col,
                ep.person_name AS person_name,
                qa.store_name AS store,
                jj.vendor_name AS vendor_lunch,
                jj.meal_price AS lunch_price,
                qq.vendor_name AS vendor_snack,
                qq.meal_price AS snack_price
                FROM room ta 
                LEFT OUTER JOIN view_events tb ON ta.room_name=tb.room_name

                LEFT JOIN event_user tc ON 
                (tb.event_id=tc.event_id AND tc.option_id=1
                OR tb.event_id=tc.event_id AND tc.option_id=7 
                OR tb.event_id=tc.event_id AND tc.option_id=2 
                OR tb.event_id=tc.event_id AND tc.option_id=8 
                OR tb.event_id=tc.event_id AND tc.option_id=5 
                OR tb.event_id=tc.event_id AND tc.option_id=6)

                LEFT JOIN event_person ep ON ep.id_event=tb.event_id

                LEFT JOIN (SELECT us.user_name, tb.room_id, st.store_name FROM sushitei_portal.portal_user us
                LEFT JOIN view_events tb ON us.user_id=tb.user_id
                LEFT JOIN store st ON st.store_id=us.store_id) qa ON qa.room_id=ta.room_id

                LEFT JOIN
                (SELECT e.event_id, e.event_name, e.room_id, lunch.vendor_name, lunch.meal_id, lunch.meal_price FROM view_events e LEFT JOIN 
                (SELECT *  FROM event_user eu LEFT JOIN meal mm ON eu.euser_value=mm.meal_id
                LEFT JOIN meal_vendor mv ON mv.vendor_id=mm.meal_vendor WHERE option_id=7) lunch ON e.event_id=lunch.event_id) jj ON jj.room_id=ta.room_id

                LEFT JOIN
                (SELECT e.event_id, e.event_name, e.room_id, snack.vendor_name, snack.meal_id, snack.meal_price FROM view_events e LEFT JOIN 
                (SELECT *  FROM event_user eu LEFT JOIN meal mm ON eu.euser_value=mm.meal_id
                LEFT JOIN meal_vendor mv ON mv.vendor_id=mm.meal_vendor WHERE option_id=1) snack ON e.event_id=snack.event_id) qq ON qq.room_id=ta.room_id
                WHERE 
                    tb.status_name='Approved' ";
                 if(!empty($title)){
                    $q .= " AND tb.event_name LIKE '%{$title}%' ";
                 }
                 if (!empty($store_name)) {
                     $q .= " AND user_id='".$store_name."' ";
                 }
                 if (!empty($date_first)) {
                     $q .= " AND date_from >= '".date('Y-m-d', strtotime($date_first))."' ";
                 }
                 if (!empty($date_last)) {
                     $q .= " AND date_until <= '".date('Y-m-d', strtotime($date_last))."' ";
                 }    
                    
                #$q .= " GROUP BY tb.event_id, ep.person_name ORDER BY book_date,ta.room_id ASC";
            
            
            return $this->fetchAllQuery($q);
            //return $q;
        }*/
        
        public function getUnconfirmedEventUser($f, $opt)
		{
			$q = "SELECT 
                    event_id,
                    event_name,
                    status_name,
                    display_name,
                    room_name,
                    room_capacity,
                    date_format(date_from,'%d-%m-%Y') AS book_date,
                    date_format(date_from,'%d-%m-%Y') AS start,
                    date_format(date_until,'%d-%m-%Y') AS end 
                    FROM view_events 
                    WHERE 
                        date_from >= CURDATE() 
                        AND event_status=2 ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE date_from >= CURDATE() AND event_status=2 ";
            foreach($f as $key => $value){
                $q .=  " AND {$key}='{$value}' ";
                $c .= " AND {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getFutureEventUser($f, $opt)
		{
			
			$q = "SELECT 
                    event_id,
                    event_name,
                    status_name,
                    display_name,
                    room_name,
                    room_capacity,
                    date_format(date_from,'%d-%m-%Y') AS book_date,
                    date_format(date_from,'%d-%m-%Y') AS start,
                    date_format(date_until,'%d-%m-%Y') AS end  
                    FROM view_events 
                    WHERE 
                        date_from >= CURDATE() 
                        AND event_status=1 ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE date_from >= CURDATE() AND event_status=1 ";
            foreach($f as $key => $value){
                $q .=  " AND {$key}='{$value}' ";
                $c .= " AND {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getPastEventUser($f, $opt)
		{
            $q = "SELECT 
                    event_id,
                    event_name,
                    status_name,
                    display_name,
                    room_name,
                    room_capacity,
                    date_format(date_from,'%d-%m-%Y') AS book_date,
                    date_format(date_from,'%d-%m-%Y') AS start,
                    date_format(date_until,'%d-%m-%Y') AS end 
                    FROM view_events 
                    WHERE date_from < CURDATE() ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE date_from < CURDATE() ";
            foreach($f as $key => $value){
                $q .=  " AND {$key}='{$value}' ";
                $c .= " AND {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getAllEventUser($f, $opt)
		{
            $q = "SELECT 
                    event_id,
                    event_name,
                    status_name,
                    display_name,
                    room_name,
                    room_capacity,
                    date_format(date_from,'%d-%m-%Y') AS book_date,
                    date_format(date_from,'%d-%m-%Y') AS start,
                    date_format(date_until,'%d-%m-%Y') AS end 
                    FROM view_events ";
            $c = "SELECT COUNT(*) row_total FROM view_events ";
            foreach($f as $key => $value){
                $q .=  " WHERE {$key}='{$value}' ";
                $c .= " WHERE {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getNotified($day)
		{
			$date = new DateTime();
            $date->modify("+{$day} day");
			$q = "SELECT f.*,g.display_name, e.room_name, a.meal_name AS meal1, g.email, b.meal_name AS meal2, c.vendor_name AS provider1, d.vendor_name AS provider2, a.meal_price AS price1, b.meal_price AS price2 FROM events f LEFT JOIN room e ON e.room_id=f.room_id LEFT JOIN sushitei_portal.portal_user g ON g.user_id=f.event_creator LEFT JOIN meal a ON a.meal_id=f.meal_snack LEFT JOIN meal b ON b.meal_id=f.meal_lunch LEFT JOIN meal_vendor c ON c.vendor_id=f.provider_snack LEFT JOIN meal_vendor d ON d.vendor_id=f.provider_lunch LEFT JOIN event_type ON f.event_type_id=event_type.event_type_id WHERE event_status=1 AND start_time BETWEEN '".$date->format('Y-m-d')." 00:00:00' AND '".$date->format('Y-m-d')." 23:59:59'";
            return $this->fetchAllQuery($q);
		}

         /*  public function getAllEvent()
        {
            $q = "SELECT ta.*,tb.event_id,tb.event_name,tb.status_name,tb.room_capacity,tb.display_name, tb.start_time, tb.end_time,date_format(tb.start_time,'%Y-%m-%d') AS book_date,IF(tc.option_id IS NULL, '', 'y') AS  snack,IF(td.option_id IS NULL, '', 'y') AS  lunch,IF(te.option_id IS NULL, '', 'y') AS  projector,IF(tf.option_id IS NULL, '', 'y') AS  flip,IF(tg.option_id IS NULL, '', 'y') AS  round,IF(th.option_id IS NULL, '', 'y') AS  col FROM room ta LEFT OUTER JOIN view_events tb ON ta.room_name=tb.room_name LEFT JOIN event_user tc ON tb.event_id=tc.event_id AND tc.option_id=1 LEFT JOIN event_user td ON tb.event_id=td.event_id AND td.option_id=7 LEFT JOIN event_user te ON tb.event_id=te.event_id AND te.option_id=2 LEFT JOIN event_user tf ON tb.event_id=tf.event_id AND tf.option_id=8 LEFT JOIN event_user tg ON tb.event_id=tg.event_id AND tg.option_id=5 LEFT JOIN event_user th ON tb.event_id=th.event_id AND th.option_id=6 WHERE tb.status_name='Approved' ORDER BY book_date,ta.room_id ASC";
            
            
            return $this->fetchAllQuery($q);
        }*/

        public function getGroupId($input)
        {
            $q = "SELECT 
                event_groups_id                    
                FROM event_groups 
                WHERE event_id=:event_id AND date_groups=:date_groups";
            
            return $this->fetchSingleQuery($q, $input);
        }
        
	}

    /*public function getEventBy2($input)
        {
            $q = "SELECT * 
                  FROM events 
                  LEFT JOIN room ON room.room_id=events.room_id 
                  LEFT JOIN sushitei_portal.portal_user ON sushitei_portal.portal_user.user_id=events.event_creator 
                  LEFT JOIN event_type ON events.event_type_id=event_type.event_type_id 
                  WHERE 
                    event_id=:event_id";
            
            return $this->fetchSingleQuery($q, $input);
        }*/