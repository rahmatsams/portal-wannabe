<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelDepartment extends Model
	{
		
		#INDEX DATA DEPARTMENT
		public function getAllDepartment()
		{
			#$q = "SELECT * FROM sushitei_portal.staff_department WHERE department_status=1 ORDER BY gm_position ASC";
			$q = "SELECT * FROM sushitei_portal.staff_department ORDER BY gm_position DESC";
			
			return $this->fetchAllQuery($q);
		}
		
		#DELETE DEPARTMENT
		public function deleteDepartment($id)
		{
			#$q = $this->doQuery("UPDATE sushitei_portal.staff_department SET department_status=2 WHERE department_id=$id");
			$q = $this->doQuery("DELETE FROM sushitei_portal.staff_department WHERE department_id=$id");
		}
		
		#CREATE NEW DEPARTMENT
		public function newDepartment($input)
		{
			$result = $this->insertQuery('sushitei_portal.staff_department', $input);
			return $result;
		}

		function getDepartmentByID($input)
	    {
	        $query_string = "SELECT * FROM sushitei_portal.staff_department WHERE department_id=:department_id";
	        
	        $result = $this->fetchSingleQuery($query_string, $input);
	        return $result;
	    }

	    #EDIT DEPARTMENT
	    function editDepartment($form = array(), $where = array())
		{
			$table = 'sushitei_portal.staff_department';
			$result = $this->editQuery($table, $form, $where);
			return $result;
		}

	}