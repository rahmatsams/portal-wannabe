<?php
/*
* Model for Ticketdesk
*/
class ModelProvider extends Model
{
	
    function getAllActiveProvider()
    {
        $query_string = "SELECT * FROM meal_vendor WHERE vendor_status=1";
		
		$result = $this->fetchAllQuery($query_string);
        return $result;
    }

    function getAllActiveProviderSnack()
    {

        $query_string = "SELECT 
                            mv.vendor_name AS vendor_name, 
                            mv.vendor_id AS vendor_id
                            FROM `meal` m, `meal_vendor` mv 
                            WHERE 
                                m.meal_vendor = mv.vendor_id 
                                AND mv.vendor_status=1 
                                AND m.meal_category=1 
                                GROUP BY mv.vendor_id";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }

    function getAllActiveProviderLunch()
    {

        $query_string = "SELECT 
                            mv.vendor_name AS vendor_name, 
                            mv.vendor_id AS vendor_id
                            FROM `meal` m, `meal_vendor` mv 
                            WHERE 
                                m.meal_vendor = mv.vendor_id 
                                AND mv.vendor_status=1 
                                AND m.meal_category=2 
                                GROUP BY mv.vendor_id";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function newProvider($input)
    {
        $result = $this->insertQuery('meal_vendor', $input);
        return $result;
    }
	
	function getProviderByID($input)
    {
        $query_string = "SELECT * FROM meal_vendor WHERE vendor_id=:vendor_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    function getAllProvider()
    {
        $query_string = "SELECT * FROM meal_vendor WHERE vendor_status=1";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    
    function getProviderBy($input)
    {
        $query_string = "SELECT * from meal_vendor ";
        $query_string .= "WHERE vendor_id=:vendor_id";            
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    
    function editProvider($form = array(), $where = array()){
        $table = 'meal_vendor';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }
    //controller e
    function deleteProvider($id)
    {
        $query = $this->doQuery("UPDATE meal_vendor SET vendor_status=2 WHERE vendor_id=$id");
        
    }
    
}