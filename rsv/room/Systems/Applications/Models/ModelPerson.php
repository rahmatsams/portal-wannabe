<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

/**
 * 
 */
class ModelPerson extends Model
{
	
	function getAllPerson()
	{
		$query = "SELECT * 
					FROM event_person 
					WHERE person_status=1 ";
					
		$result = $this->fetchAllQuery($query);
		return $result;
	}

	function getAllPersonByEvent($input)
	{
		$query = "SELECT es.event_name, ep.* 
					FROM event_person ep
					LEFT JOIN EVENTS es ON es.event_id=ep.id_event
					WHERE ep.id_event=:event_id ";

		$result = $this->fetchAllQuery($query, $input);
		return $result;
	}
}


 ?>