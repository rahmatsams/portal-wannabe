<?php

/**
* 
*/
class ModelMeal extends Model
{
	
	public function getActiveMeal()
	{
		$query = "SELECT * FROM meal WHERE meal_status=1 ORDER BY meal_category ASC";

		return $this->fetchAllQuery($query); 
	}

	public function getAllMeal()
	{
		$query = "SELECT 
					meal_id,
					meal_name,
					vendor_name,
					meal_category_name,
					meal_price,
					meal_status 
					FROM meal m 
					LEFT JOIN meal_category mc ON mc.meal_category_id=m.meal_category 
					LEFT JOIN meal_vendor mv ON mv.vendor_id=m.meal_vendor 
					WHERE 
						meal_status=1 ORDER BY meal_category, meal_name";
		
		
		return $this->fetchAllQuery($query);
	}

	public function getAllMealSnack()
	{
		$query = "SELECT 
					meal_id,
					meal_name,
					vendor_name,
					meal_category_name,
					meal_price,
					meal_status 
					FROM meal m 
					LEFT JOIN meal_category mc ON mc.meal_category_id=m.meal_category 
					LEFT JOIN meal_vendor mv ON mv.vendor_id=m.meal_vendor 
					WHERE 
						meal_status=1 
					AND meal_category=1
						";
		
		
		return $this->fetchAllQuery($query);
	}

	public function getAllMealLunch()
	{
		$query = "SELECT 
					meal_id,
					meal_name,
					vendor_name,
					meal_category_name,
					meal_price,
					meal_status 
					FROM meal m 
					LEFT JOIN meal_category mc ON mc.meal_category_id=m.meal_category 
					LEFT JOIN meal_vendor mv ON mv.vendor_id=m.meal_vendor 
					WHERE 
						meal_status=1 
					AND meal_category=2
						";
		
		return $this->fetchAllQuery($query);
	}

	public function getAllMealCategory()
	{
		$query = "SELECT * FROM meal_category";

		return $this->fetchAllQuery($query);
	}

	public function newMeal($input)
	{
		$result = $this->insertQuery('meal', $input);
		return $result;
	}

	public function getMealID($input)
	{
		$query = "SELECT * FROM meal WHERE meal_id=:meal_id";

		$result = $this->fetchSingleQuery($query, $input);
		return $result;
	}

	public function getMealType($number)
	{
		$query = "SELECT * FROM meal WHERE meal_category={$number} AND meal_status=1";

		return $this->fetchAllQuery($query);
	}

	public function getMealVendor($idVendor, $number)
	{
		$query = "SELECT * FROM meal WHERE meal_vendor={$idVendor} AND meal_category={$number} AND meal_status=1"; 

		return $this->fetchAllQuery($query);
	}

	function editMeal($form = array(), $where = array())
	{
		$table = 'meal';
		$result = $this->editQuery($table, $form, $where);
		return $result;
	}

	function deleteMeal($id)
	{
		$query = $this->doQuery("UPDATE meal SET meal_status=2 WHERE meal_id=$id");
		
	}
}

?>