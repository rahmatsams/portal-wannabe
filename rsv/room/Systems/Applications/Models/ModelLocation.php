<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class ModelLocation extends Model{
	
        function newLocation($input)
        {
            $result = $this->insertQuery('location', $input);
            return $result;
        }
        
        function getAllLocation($query_option)
        {
            $query_string = "SELECT * FROM location";
            $count_query = "SELECT COUNT(*) AS row_total FROM location";
            
            $result = $this->pagingQuery($query_string, $count_query, $query_option);
            return $result;
        }
        
        function getActiveLocation()
        {
            $query_string = "SELECT * FROM location WHERE location_status=1";
            
            $result = $this->fetchAllQuery($query_string);
            return $result;
        }

		function getLocationBy($input)
        {
            $query_string = "SELECT * from location ";
            $query_string .= "WHERE location_name=:location_id";            
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }

        function getLocationByStore($input)
        {
            $query_string = "SELECT * from location ";
            $query_string .= "WHERE location_id=:location_id";
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }
        
        function editLocation($form = array(), $where = array()){
            $table = 'location';
            $result = $this->editQuery($table, $form, $where);
            return $result;
        }
		
		function deleteLocation($id){
			
			$result = $this->deleteQuery('location' ,$id);
			return $result;
		}
        
        function getStoreByLocation(){
            
        }
      
	}
?>