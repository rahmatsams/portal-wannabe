<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

class Admin extends Controller
{
    public function accessRules()
    {
		return array(
            array('Deny', 
				'actions'=>array('viewIndex', 'viewUserIndex', 'viewRoomIndex', 'viewProviderIndex', 'viewExport', 'viewAgendaIndex'),
				'groups'=>array('User'),
			),
            array('Allow', 
				'actions'=>array('viewIndex', 'viewUserIndex', 'viewRoomIndex', 'viewProviderIndex', 'viewExport', 'viewAgendaIndex', 'viewOptionIndex', 'viewMealIndex', 'viewEventUserIndex', 'viewDepartmentIndex'),
				'groups'=>array('Administrator', 'Super Admin', 'Admin Booking Room'),
			),
			
		);
    }
    
	private function _adminMenu()
	{
		$menu = '
		<ul class="nav nav-stacked">
			<li class="active"><a href="index.html"><i class="glyphicon glyphicon-home"></i> Front Page Home</a></li>
			<li role="separator" class="divider"></li>
			<li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu">Admin <i class="glyphicon glyphicon-chevron-down"></i></a>
				<ul class="nav nav-stacked collapsed" id="userMenu">
					<li><a href="admin_calendar.html"><i class="glyphicon glyphicon-calendar"></i> Manage Calendar</a></li>
					<li class="active"><a href="admin_room.html"><i class="glyphicon glyphicon-bookmark"></i> Manage Room</a></li>
					<li><a href="admin_user.html"><i class="glyphicon glyphicon-user"></i> Manage User</a></li>

			<li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Catering<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="admin_catering.html"><i class="glyphicon glyphicon-glass"></i>&nbsp;&nbsp;Vendor</a>
					</li>
					<li><a href="admin_meal.html"><i class="glyphicon glyphicon-cutlery"></i>&nbsp;&nbsp;Catering</a>
					</li>
				</ul>
			</li>
					<li><a href="admin_option.html"><i class="glyphicon glyphicon-option-horizontal"></i> Manage Option</a></li>
					<li><a href="admin_department.html"><i class="glyphicon glyphicon-briefcase"></i> Manage Department</a></li>
					<li><a href="logout.html"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
				</ul>
			</li>
			
		
		</ul>
		';
		return $menu;
	}

	#INDEX ADMIN PAGE
    public function viewIndex()
    {
        $data = array(
        	'session' => $this->_mySession
		);
		$option = array(
            'page_name' => 'Admin Page',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'admin' => $this->isAdmin(),
        );
        $this->load->template('admin/index', $data, $option);
    }
  
    #INDEX MANAGE ROOM
	public function viewRoomIndex()
	{
		$m = $this->load->model('Room');
		$data = array(
			'page_name' => 'Manage Room',
			'menu' => $this->_adminMenu(),
			'room' => $m->getAllRoom(),
			'page' => 1,
		);
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Room',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'style' => '
			<link rel="stylesheet" href="Resources/css/calendar.css">',
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/ascript.js'></script>
			",
			
        );
        $view = $this->load->template('admin/room/index', $data, $option);
	}
    
	#INDEX MANAGE USER
	public function viewUserIndex()
	{
        $m_users = $this->load->model('Users');
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'user_name',
            'order' => 'ASC',
        );
        $data = array(
            '_mySession' => $this->_mySession,
            'users' => $m_users->getAllUser($query_option),
            'total' => $m_users->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
            'groups' => $m_users->getAllGroups(),
			'menu' => $this->_adminMenu()
        );
        $option = array(
            'page_name' => 'Manage User',
            'session' => $this->_mySession,
            'admin' => $this->isAdmin()
        );
         
        $this->load->template('admin/users/index', $data, $option);
    }
	
	#INDEX MANAGE CATERING
	public function viewProviderIndex()
	{
		$m = $this->load->model('Provider');
		$data = array(
			'page_name' => 'Manage Vendor Catering',
			'menu' => $this->_adminMenu(),
			'catering' => $m->getAllProvider(),
			'page' => 1,
		);
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Catering',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/bscript.js'></script>
			",
			
        );
        $view = $this->load->template('admin/catering/index', $data, $option);
	}

	#INDEX EVENT USER
	public function viewEventUserIndex()
	{
		$m = $this->load->model('EventUsers');
		$data = array(
			'page_name' => 'Manage Event User',
			'menu' => $this->_adminMenu(),
			'eventuser' => $m->getAllEventUser(),
			'page' => 1,
			 );
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Event User',
			'config' => $this->load->model('Config'),
			'session' => $this->_mySession,
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/bscript.js'></script>
			",
			 );
		$view = $this->load->template('admin/eventuser/index', $data, $option);
	}
    
	#INDEX MEAL PAGE
	public function viewMealIndex()
	{
		$m = $this->load->model('Meal');
        $mp = $this->load->model('Provider');
		$query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'meal_category_name',
            'order' => 'ASC',
        );
		$data = array(
			'page_name' => 'Manage Catering',
			'menu' => $this->_adminMenu(),
			'meal' => $m->getAllMeal(),
			'total' => $m->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
			'meal_category' => $m->getAllMealCategory(),
            'meal_vendor' => $mp->getAllProvider(),
			#'page' => 1,
		);
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Catering',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/escript.js'></script>
			",
        );
        $this->load->template('admin/meal/index', $data, $option);
	}

    #INDEX MANAGE AGENDA
	public function viewAgendaIndex() //modal eventData Futurelist
	{
		$m = $this->load->model('Event');
		$mr = $this->load->model('Room');
		$mt = $this->load->model('Provider');
		$mmeal = $this->load->model('Meal');
		$m_users = $this->load->model('Users');
        $moption = $this->load->model('EventOptions');
        $mtype = $this->load->model('EventType');
		$data = array(
			'page_name' => 'Manage Calendar',
			'menu' => $this->_adminMenu(),
			'room' => $mr->getallActiveRoom(),
            'meal' => $moption->getOptionType('1'),
			'equipment' => $moption->getOptionType('2'),
			'layout' => $moption->getOptionType('3'),
			'users' => $m_users->getAllUser3(),
			'vendor_snack' => $mt->getAllActiveProviderSnack(),
			'vendor_lunch' => $mt->getAllActiveProviderLunch(),
			'meal_snack' => $mmeal->getAllMealSnack(),
			'meal_lunch' => $mmeal->getAllMealLunch(),
			'session' => $this->_mySession,
			'event_type' => $mtype->getAllActiveEventType()
			#'page' => 1,
		);
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Calendar',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
            'style' => "
            <link rel='stylesheet' href='Resources/css/jquery.dataTables.min.css'>
            ",
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/cscript.js'></script>
            <script type='text/javascript' src='Resources/js/jquery.dataTables.min.js'></script>
			",
			
        );
        $view = $this->load->template('admin/agenda/index', $data, $option);
	}
    
    #INDEX MANAGE OPTION
	public function viewOptionIndex()
	{
		$m = $this->load->model('EventOptions');
		$data = array(
			'page_name' => 'Manage Option',
			'menu' => $this->_adminMenu(),
			'catering' => $m->getAllOption(),
            'option_category' => $m->getAllCategory(),
			'page' => 1,
		);
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Option',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/dscript.js'></script>
			",
        );
        $view = $this->load->template('admin/option/index', $data, $option);
	}

	#INDEX MANAGE DEPARTMENT
	public function viewDepartmentIndex()
	{
		$md = $this->load->model('Department');
		$query_option = array(
			'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
			'result' => 10,
			'order_by' => 'gm_position',
			'order' => 'ASC', 
		);
		$data = array(
			#'page_name' => 'Manage Department',
			'menu' => $this->_adminMenu(),
			'department' => $md->getAllDepartment(),
			'total' => $md->getCountResult(),
			'page' => $query_option['page'],
			'max_result' => $query_option['result'],
		);
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Department',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/gscript.js'></script>
			",
        );
        $view = $this->load->template('admin/department/index', $data, $option);
	}

     #INDEX EXPORT
	/*public function viewExport()
	{
		if (isset($_POST['title'])) {
			$title = $_POST['title'];
		}else{
			$title = "";
		}
		if (isset($_POST['store_name'])) {
			$store_name = $_POST['store_name'];
		}else{
			$store_name = "";
		}
		if (isset($_POST['date_first'])) {
			$date_first = $_POST['date_first'];
		}else{
			$date_first = "";
		}
		if (isset($_POST['date_last'])) {
			$date_last = $_POST['date_last'];
		}else{
			$date_last = "";
		}

		$m = $this->load->model('Event');
		$mr = $this->load->model('EventOptions');
		$fn = 'data_booking.csv';
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename='.$fn);
        header('Pragma: no-cache');
        header("Expires: 0");
        
        $outstream = fopen("php://output", "w");
        $data = $m->getAllEvent2($title, $store_name, $date_first, $date_last); //filter data
        $opt = $mr->getActiveOption();
        //exit(var_dump($data));
        if(is_array($data) && count($data) > 0){
        	#exit(var_dump($data));
            $ld = 0;
            foreach($data as $l){
            	//exit(var_dump($l));
                $sd = date("Y-m-d", strtotime($l['date_from']));
                if($ld != $sd){
                    echo "\n"; //new_line
                    //echo '"'.date("l, d-m-Y", strtotime($sd))."\"\n";
                    echo 'Nama Pemohon, Jabatan, Departemen, Permohonan, Ruangan,Tanggal Mulai,Tanggal Selesai, Waktu Mulai, Waktu Selesai, Nama Kegiatan, Nama Peserta, Outlet, Hadir, Tidak Hadir, Snack, Budget Snack, Lunch, Budget Lunch'; //columns
                    /*foreach($opt as $d){
                        echo ",{$d['option_name']}"; //event_options
                    }
                    echo "\n";
                    $ld = date("Y-m-d", strtotime($l['date_from']));
                    
                }
                echo "{$l['display_name']},,,{$l['event_type']},{$l['room_name']},".(date("d-m-Y", strtotime($l['date_from']))).",".(date("d-m-Y", strtotime($l['date_until']))).",".(date("H:i", strtotime($l['time_from']))).",".(date("H:i", strtotime($l['time_until']))).",{$l['event_name']},{$l['person_name']},{$l['store']},,,{$l['vendor_snack']},{$l['snack_price']},{$l['vendor_lunch']},{$l['lunch_price']}\n";
            }
        }
        fclose($outstream);
	}*/

	public function viewExport()
	{
		if (isset($_POST['title'])) {
			$title = $_POST['title'];
		}else{
			$title = "";
		}
		if (isset($_POST['store_name'])) {
			$store_name = $_POST['store_name'];
		}else{
			$store_name = "";
		}
		if (isset($_POST['date_first'])) {
			$date_first = $_POST['date_first'];
		}else{
			$date_first = "";
		}
		if (isset($_POST['date_last'])) {
			$date_last = $_POST['date_last'];
		}else{
			$date_last = "";
		}

		$m = $this->load->model('Event');
		$mr = $this->load->model('EventOptions');
		$fn = 'data_booking.csv';
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename='.$fn);
        header('Pragma: no-cache');
        header("Expires: 0");
        
        $outstream = fopen("php://output", "w");
        $data = $m->getAllEvent2($title, $store_name, $date_first, $date_last); //filter data
        $opt = $mr->getActiveOption();
        $temporaryeventtype = "";
        $kosong = "";
        //exit(var_dump($data));
        if(is_array($data) && count($data) > 0){
            $ld = 0;
            foreach($data as $l){
                $sd = date("Y-m-d", strtotime($l['date_from']));
                if($ld != $sd){
                    echo "\n"; //new_line
                    echo '"'.date("l, d-m-Y", strtotime($sd))."\"\n";
                    echo 'Nama Pemohon, Department, Permohonan, Ruangan,Tanggal Mulai,Tanggal Selesai, Waktu Mulai, Waktu Selesai, "Activity Name", Outlet, Peserta, Snack, Total Budget Snack, Lunch, Total Budget Lunch, Nama Peserta'; //columns
                    
                    /*foreach($opt as $d){
                        echo ",{$d['option_name']}"; //event_options
                    }*/
                    
                    echo "\n";
                    $ld = date("Y-m-d", strtotime($l['date_from']));
                    //exit(var_dump($data));
                }
                if ($temporaryeventtype == $l['event_id']) { //jk event_id sama, maka lainnya akan kosong kcli person_name
                	echo "{$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$kosong}, {$l['person_name']} \n";
                }else{
                	echo "{$l['display_name']}, {$l['department_name']}, {$l['event_type']}, {$l['room_name']}, ".(date("d-m-Y", strtotime($l['date_from']))).", ".(date("d-m-Y", strtotime($l['date_until']))).", ".(date("H:i", strtotime($l['time_from']))).", ".(date("H:i", strtotime($l['time_until']))).", {$l['event_name']}, {$l['store_name']}, {$l['room_capacity']}, {$l['vendor_snack']}, ".$l['snack_price']*$l['room_capacity'].", {$l['vendor_lunch']}, ".$l['lunch_price']*$l['room_capacity'].", {$l['person_name']} \n";
                	$temporaryeventtype = $l['event_id'];
                }
            }
        }
        fclose($outstream);
	}
    
}