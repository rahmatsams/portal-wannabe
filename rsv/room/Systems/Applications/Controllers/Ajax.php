<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ajax extends Controller
{
	public function accessRules()
	{
		return array(
            array('Allow', 
				'actions'=>array('viewLoginData','viewEventList', 'viewNewBook', 'viewActiveOptionData', 'viewEventData', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
				'groups'=>array('*'),
			),
			array('Allow', 
				'actions'=>array('viewNewRoom','viewEditRoom','viewRoomData','viewProviderData','viewNewProvider','viewEditProvider','viewAdminEventOption','viewEditEvent', 'viewDeleteProvider', 'viewDeleteRoom'),
				'groups'=>array('Administrator', 'Super Admin', 'Admin Booking Room'),
			),
			array('Deny', 
				'actions'=>array('viewEventList', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
				'groups'=>array('Guest'),
			),
		);
	}
	
    public function viewLoginData() #MYSESSION
    {
		if($this->isGuest()){
            $current_attempt = $this->_mySession['attempt'];
            if($current_attempt > 3){
                $return = array(
                    'login_attempt' => $current_attempt,
                    'message' => "You're blocked from login for 20 minutes. ",
                    'success' => 0
                );
			}else{
                if(isset($_POST['ticket_email']) && isset($_POST['ticket_password'])){
                    $input = $this->load->lib('Input');
                    $input->addValidation('ticket_email', $_POST['ticket_email'], 'min=1', 'Email Address must be filled');
                    $input->addValidation('ticket_email', $_POST['ticket_email'], 'username', 'Email not recognized');
                    $input->addValidation('ticket_password', $_POST['ticket_password'], 'min=1', 'Password must be filled');
                    if ($input->validate()) {
                        $login_data = array(
                            'user_name' => $_POST['ticket_email'],
                            'user_password' => MD5($_POST['ticket_password'].getConfig('salt')),
                        );
                        $model_users = $this->load->model('Users');
                        $result = $model_users->doLogin($login_data);
                        if(is_array($result) && count($result) > 0){
                            $this->setSession('userid',$result['user_id']);
                            $this->setSession('username',$result['display_name']);
                            $this->setSession('outlet',$result['store_id']);
                            $this->setSession('group',$result['group_name']);
                            $this->setSession('role',$result['level']);
							$this->setSession('email',$result['email']);
							$this->setSession('gm_email',$result['gm_email']);
                            $this->setSession('attempt', 0);
                            $return = array(
                                'success' => 1
                            );
                        } else {
                            $current_attempt++;
                            $this->setSession('attempt', $current_attempt);
                            $return = array(
                                'login_attempt' => $current_attempt,
                                'success' => 0
                            );
                            
                        }
                    } else {
                        $current_attempt++;
                        $this->setSession('attempt', $current_attempt);
                        $return = array(
                            'login_attempt' => $current_attempt,
                            'error' => $input->_error,
                            'success' => 0
                        );
                    }
                
                } else {
                    $current_attempt++;
                    $return = array(
                        'login_attempt' => $current_attempt,
                        'success' => 0
                    );
                    
                }
            }
            echo json_encode($return);
		}else{
            $return = array(
                'success' => 1
            );
             echo json_encode($return);
        }
	}
	
	public function viewNewRoom()
	{
		if(isset($_POST['room_name']) && isset($_POST['room_capacity']) && isset($_POST['room_color'])){
			$input = $this->load->lib('Input');
			$input->addValidation('room_name_format',$_POST['room_name'],'alpha_numeric_sp', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('room_name_required',$_POST['room_name'],'min=4', 'Room Name should contain at least 4 character');
			$input->addValidation('room_name_max',$_POST['room_name'],'max=50', 'Room Name is too long');
			$input->addValidation('room_capacity_format',$_POST['room_capacity'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('room_capacity_required',$_POST['room_capacity'],'min=1', 'Is Required');
			$input->addValidation('room_capacity_max',$_POST['room_capacity'],'max=4', 'Max capacity is 9999');
			$input->addValidation('room_color_format',$_POST['room_color'],'alpha_numeric_sc', 'Please check your input');
			$input->addValidation('room_color_required',$_POST['room_color'],'min=1', 'Is Required');
			$input->addValidation('room_color_max',$_POST['room_color'],'max=14', 'Please check your input');
			if($input->validate()){
				$m = $this->load->model('Room');
				if($m->newRoom($_POST)){
					$return = array(
						'success' => 1
					);
					echo json_encode($return);
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0));
		}
	}
	
	#EDIT ROOM
	public function viewEditRoom()
	{
		if(isset($_POST['room_name']) && isset($_POST['room_capacity']) && isset($_POST['room_color'])){
			$input = $this->load->lib('Input');
			$input->addValidation('room_name_format',$_POST['room_name'],'alpha_numeric_sp', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('room_name_required',$_POST['room_name'],'min=4', 'Room Name should contain at least 4 character');
			$input->addValidation('room_name_max',$_POST['room_name'],'max=50', 'Room Name is too long');
			$input->addValidation('room_capacity_format',$_POST['room_capacity'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('room_capacity_required',$_POST['room_capacity'],'min=1', 'Is Required');
			$input->addValidation('room_capacity_max',$_POST['room_capacity'],'max=4', 'Max capacity is 9999');
			$input->addValidation('room_color_format',$_POST['room_color'],'alpha_numeric_sc', 'Please check your input');
			$input->addValidation('room_color_required',$_POST['room_color'],'min=1', 'Is Required');
			$input->addValidation('room_color_max',$_POST['room_color'],'max=14', 'Please check your input');
			$input->addValidation('room_id_format',$_POST['room_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('room_id_required',$_POST['room_id'],'min=1', 'Is Required');
			$input->addValidation('room_id_max',$_POST['room_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->load->model('Room');
				$w = array(
					'room_id' => $_POST['room_id']
				);
				unset($_POST['room_id']);
				if($m->editRoom($_POST, $w)){
					echo json_encode(array('success'=>1));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0));
		}
	}
	
	public function viewRoomData()
	{
		if(isset($_POST['room_id'])){
			$input = $this->load->lib('Input');
			$input->addValidation('room_id_format',$_POST['room_id'],'numeric', 'Cannot find Room');
			$input->addValidation('room_id_required',$_POST['room_id'],'min=1', 'Is Required');
			$input->addValidation('room_id_max',$_POST['room_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->load->model('Room');
				$data = $m->getRoomByID($_POST);
				if(count($data) > 0){
					echo json_encode(array('success'=>1, 'data' => $data));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0));
		}
	}
	
	#deleteRoom
	public function viewDeleteRoom()
	{
		$id = $_GET['idRoom'];
		$m = $this->load->model('Room');
		$m->deleteRoom($id);
		header('Location: admin_room.html');
	}

	public function viewProviderData()
	{
		if(isset($_POST['vendor_id'])){
			$input = $this->load->lib('Input');
			$input->addValidation('vendor_id_format',$_POST['vendor_id'],'numeric', 'Cannot find Room');
			$input->addValidation('vendor_id_required',$_POST['vendor_id'],'min=1', 'Is Required');
			$input->addValidation('vendor_id_max',$_POST['vendor_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->load->model('Provider');
				$data = $m->getProviderByID($_POST);
				if(count($data) > 0){
					echo json_encode(array('success'=>1, 'data' => $data));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0, 'error' => $_POST));
		}
	}
	
	#CATERING_VENDOR
	public function viewNewProvider()
	{
		if(isset($_POST['vendor_name'])){
			$input = $this->load->lib('Input');
			$input->addValidation('room_name_format',$_POST['vendor_name'],'alpha_numeric_sc', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('room_name_required',$_POST['vendor_name'],'min=4', 'Room Name should contain at least 4 character');
			$input->addValidation('room_name_max',$_POST['vendor_name'],'max=50', 'Room Name is too long');
			if(isset($_POST['vendor_address'])){
				$input->addValidation('vendor_address_max',$_POST['vendor_address'],'max=100', 'Address is exceeding 100 character.');
			}
			if(isset($_POST['vendor_phone'])){
				$input->addValidation('vendor_phone_format',$_POST['vendor_phone'],'numeric', 'Please check your input');
				$input->addValidation('vendor_phone_max',$_POST['vendor_phone'],'max=15', 'Please check your input');
			}
			if($input->validate()){
				$m = $this->load->model('Provider');
				$_POST['vendor_status'] = 1;
				if($m->newProvider($_POST)){
					echo json_encode(array('success'=>1));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0,'error'=>$_POST));
		}
	}
	
	#EDIT_VENDOR
	public function viewEditProvider()
	{
		if(isset($_POST['vendor_id']) && isset($_POST['vendor_name'])){
			$input = $this->load->lib('Input');
			$input->addValidation('vendor_id_format',$_POST['vendor_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('vendor_id_required',$_POST['vendor_id'],'min=1', 'Is Required');
			$input->addValidation('vendor_id_max',$_POST['vendor_id'],'max=4', 'Max capacity is 9999');
			$input->addValidation('vendor_name_format',$_POST['vendor_name'],'alpha_numeric_sp', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('vendor_name_required',$_POST['vendor_name'],'min=4', 'Room Name should contain at least 4 character');
			$input->addValidation('vendor_name_max',$_POST['vendor_name'],'max=50', 'Room Name is too long');
			if(isset($_POST['vendor_address'])){
				$input->addValidation('vendor_address_max',$_POST['vendor_address'],'max=100', 'Max capacity is 9999');
			
			}
			if(isset($_POST['vendor_phone'])){
				$input->addValidation('vendor_phone_format',$_POST['vendor_phone'],'numeric', 'Please check your input');
				$input->addValidation('vendor_phone_max',$_POST['vendor_phone'],'max=15', 'Please check your input');
			}
			if($input->validate()){
				$m = $this->load->model('Provider');
				$w = array(
					'vendor_id' => $_POST['vendor_id']
				);
				unset($_POST['vendor_id']);
				if($m->editProvider($_POST, $w)){
					echo json_encode(array('success'=>1));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0));
		}
	}

	#DELETE_VENDOR
	public function viewDeleteProvider()
	{
		$id=$_GET['idVendor'];
		$m = $this->load->model('Provider');
		$m->deleteProvider($id);
		header('Location: admin_catering.html');
		
	}

    
	#EVENT_OPTION
	public function viewAdminEventOption()
	{
		$m = $this->load->model('EventOptions');
		$meu = $this->load->model('EventUsers');
		$data = $m->getActiveOption();
		$data2 = $meu->getEUbyEvent($_POST);
		$m = null;
		if(count($data) > 0){
			foreach($data as $key => $val){
				$m .= "<div class='col-sm-6 form-group checkbox'>";
				$m .= "<label><input name='option[]' type='checkbox' value='{$val['option_id']}'";
				foreach($data2 as $key2 => $val2){
					if($val['option_id'] == $val2['option_id'] && $val2['euser_value'] == 1){
						$m .= " checked";
					}
				}
				 $m .= "> {$val['option_name']}</label></div>";
			}
			#print_r($data2);
			
			echo json_encode(array('success'=>1, 'data' => $m));
		}else{
			echo json_encode(array('success'=>0));
		}		
	}
	
    public function getUserDetail()
	{
		if(isset($_POST['uid'])){
			$input = $this->load->lib('Input');
			$input->addValidation('uid', $_POST['uid'] ,'numeric','Terjadi kesalahan');
			if($input->validate()){
                $data = array(
                    'user_id' => $_POST['uid']
                );
				$m_users = $this->load->model('Users');
				$result = $m_users->getUserBy($data);
				if(is_array($result) && !empty($result)){
					echo json_encode($result);
				} else {
					echo 'false';
				}
			}
		} else {
			echo 'false';
		}
	}
	
    public function getUserList()
	{
		if(isset($_POST['user'])){
			$input = $this->load->lib('Input');
			$input->addValidation('user', $_POST['user'] ,'numeric','Terjadi kesalahan');
			if($input->validate()){
				$model_ticket = $this->load->model('Users');
				$result = $model_ticket->getAllNotAdmin();
				if(is_array($result) && !empty($result)){
					echo json_encode($result);
				} else {
					echo 'false';
				}
			}
		} else {
			echo 'false';
		}
	}

}
/*
* End Home Class
*/