<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AdminAgenda extends Controller
{
    
	public function accessRules()
	{
		return array(
            array('Allow', 
				'actions'=>array('viewNewMeal', 'viewEditMeal','viewMealData'),
				'groups'=>array('Administrator','Super Admin', 'Admin Booking Room'),
			),
            array('Allow', 
				'actions'=>array('viewMealData'),
				'groups'=>array('*'),
			),
			array('Deny', 
				'actions'=>array('viewEventData'),
				'groups'=>array('Guest'),
			),
		);
	}
	
    protected function vType()
    {
        return (isset($_GET['t']) ? $_GET['t'] : '');
    }
    
    
    public function viewEventListAdmin()
	{
        $m = $this->eventModel();
        $data = 0;
        $f = array();

        $o = array(
            'page' => ($_POST['start']/$_POST['length'] > 0 ? ($_POST['start']/$_POST['length']+1) : 1),
            'result' => $_POST['length'],
            'order_by' => 'date_from',
            'order' => 'ASC'
        );
        switch($_GET['id']){
            case 1:
                $data = $m->getUnconfirmedEvent($f, $o);
            break;
            case 2:
                $o['order_by'] = 'date_from,room_name';
                $data = $m->getFutureEvent($f, $o);
            break;
            default:
                $o['order'] = 'DESC';
                $data = $m->getPastEvent($f, $o);
        }
        $result = array(
            'draw' => $_POST['draw'],
            'data' => $data,
            'recordsTotal' => $m->getCountResult(),
            'recordsFiltered' => $m->getCountResult(),
        );
        header("Content-Type: application/json");
        echo json_encode($result);
 
	}
    
	
}
/*
* End Home Class
*/