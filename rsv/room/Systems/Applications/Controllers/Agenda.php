<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Agenda extends Controller
{
    
    public function accessRules()
    {
		return array(
            array('Allow', 
				'actions'=>array('viewIndex', 'viewBook', 'viewGetMealSnackByVendor', 'viewGetMealLunchByVendor'),
				'groups'=>array('*'),
			),
			array('Deny', 
				'actions'=>array('viewIndex', 'viewGetMealSnackByVendor', 'viewGetMealLunchByVendor'),
				'groups'=>array('Guest'),
			),
		);
    }
    
    
    public function viewIndex()
    {
        $mroom = $this->load->model('Room');
		$moption = $this->load->model('EventOptions');
        $mprovider = $this->load->model('Provider');
        $mmeal = $this->load->model('Meal');
        $mtype = $this->load->model('EventType');
        $data = array(
            'session' => $this->_mySession,
            'room' => $mroom->getAllActiveRoom(),
			'meal' => $moption->getOptionType('1'),
			'equipment' => $moption->getOptionType('2'),
			'layout' => $moption->getOptionType('3'),
            'provider_snack' => $mprovider->getAllActiveProviderSnack(),
            'provider_lunch' => $mprovider->getAllActiveProviderLunch(),
            'snack' => $mmeal->getMealType('1'),
            'lunch' => $mmeal->getMealType('2'),
            'event_type' => $mtype->getAllActiveEventType(),
        );
       /* print_r($data);*/
		
        $option = array(
            'page_name' => 'Dashboard',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
            'admin' => $this->isAdmin(),
			'style' => '
			<link rel="stylesheet" href="Resources/css/calendar.css">',
			'scripts' => "
			<script type='text/javascript' src='Resources/js/underscore-min.js'></script>
			<script type='text/javascript' src='Resources/js/calendar.js'></script>
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/myscript.js'></script>
			",
			
        );
        $this->load->Template('agenda/home', $data, $option);
        
    }

    public function viewGetMealSnackByVendor()
    {
        $idVendor = $_POST['vendor_id'];
        $mmeal = $this->load->model('Meal');
        $data = $mmeal->getMealVendor($idVendor, '1');  
        $data2 = "";
        foreach ($data as $w) {
        $data2 .= "
            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
        }
        echo $data2;
    }

    public function viewGetMealLunchByVendor()
    {
        $idVendor = $_POST['vendor_id'];
        $mmeal = $this->load->model('Meal');
        $data = $mmeal->getMealVendor($idVendor, '2');
        $ll = ""; 
        foreach ($data as $w) {
        $ll .= "
            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
        }
        echo $ll;
    }
	
	public function viewBook()
    {
        
        $data = array(
            '_mySession' => $this->_mySession
        );
        $option = array(
            'page_name' => 'Book Room',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'style' => '
			<link rel="stylesheet" href="Resources/css/calendar.css">
            <link rel="stylesheet" href="Resources/css/jquery-ui.css">
            ',
			'scripts' => "
            <script type='text/javascript' src='Resources/js/jquery-ui.min.js'></script>
			<script type='text/javascript' src='Resources/js/underscore-min.js'></script>
			<script type='text/javascript' src='Resources/js/calendar.js'></script>
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/myscript.js'></script>
            <script type='text/javascript' src='Templates/".getConfig('default_template')."/js/bookscript.js'></script>
			",
			
        );
        $this->load->Template('agenda/book', $data, $option);
        
    }
    
}
/*
* End Home Class
*/