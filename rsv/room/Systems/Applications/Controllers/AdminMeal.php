<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AdminMeal extends Controller
{
    
	public function accessRules()
	{
		return array(
            array('Allow', 
				'actions'=>array('viewNewMeal', 'viewEditMeal','viewMealData', 'viewDeleteMeal'),
				'groups'=>array('Administrator','Super Admin','Admin Booking Room'),
			),
            array('Allow', 
				'actions'=>array('viewMealData'),
				'groups'=>array('*'),
			),
			array('Deny', 
				'actions'=>array('viewEventData'),
				'groups'=>array('Guest'),
			),
		);
	}
	
    protected function vType()
    {
        return (isset($_GET['t']) ? $_GET['t'] : '');
    }
    
    #MEAL
	public function viewMealData()
	{
		if(isset($_POST['meal_id'])){
			$input = $this->load->lib('Input');
			$input->addValidation('meal_id_format',$_POST['meal_id'],'numeric', 'Cannot find Meal');
			$input->addValidation('meal_id_required',$_POST['meal_id'],'min=1', 'Should be Filled');
			$input->addValidation('meal_id_max',$_POST['meal_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->load->model('Meal');
				$data = $m->getMealID($_POST);
				if(count($data) > 0){
					echo json_encode(array('success'=>1, 'data' => $data));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0, 'error' => $_POST));
		}
	}

    public function viewNewMeal()
	{
		if(isset($_POST['meal_category'])){
			$input = $this->load->lib('Input');
			$input->addValidation('mn_format',$_POST['meal_name'],'alpha_numeric_sc', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('mn_req',$_POST['meal_name'],'min=1', 'Should be Filled');
			$input->addValidation('mn_max',$_POST['meal_name'],'max=50', 'Room Name is too long');
            $input->addValidation('mc_req',$_POST['meal_category'],'min=1', 'Should be filled');
            $input->addValidation('mc_format',$_POST['meal_category'],'numeric', 'Should be filled');
            $input->addValidation('mc_max',$_POST['meal_category'],'max=4', 'Should be filled');
            $input->addValidation('mv_req',$_POST['meal_vendor'],'min=1', 'Should be filled');
            $input->addValidation('mv_format',$_POST['meal_vendor'],'numeric', 'Should be filled');
            $input->addValidation('mv_max',$_POST['meal_vendor'],'max=4', 'Should be filled');
            $input->addValidation('mp_format',$_POST['meal_price'],'numeric', 'Please check your input');
            $input->addValidation('mp_min',$_POST['meal_price'],'min=1', 'Should be filled');
            $input->addValidation('mp_max',$_POST['meal_price'],'max=12', 'Please check your input');
			
			if($input->validate()){
				$m = $this->load->model('Meal');
				$insert_value = array(
						'meal_category' => $_POST['meal_category'],
                        'meal_vendor' => $_POST['meal_vendor'],
						'meal_name'	=> $_POST['meal_name'],
						'meal_price' => $_POST['meal_price'],
						'meal_status' => 1
				);

				
				if($m->newMeal($insert_value)){
                    if($this->vType() == 'ajax'){
                        echo json_encode(array('success'=>1));
                    }else{
                        header("Location: admin_meal.html");
                    }
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => 2));
			}
		}else{
			echo json_encode(array('success'=>0,'error'=>$_POST));
		}
	}
	
    #EDIT MEAL
    public function viewEditMeal()
	{
		if(isset($_POST['meal_category'])){
			$input = $this->load->lib('Input');
            $input->addValidation('mi_format',$_POST['meal_id'],'numeric', 'Should be filled');
            $input->addValidation('mi_max',$_POST['meal_id'],'max=6', 'Should be filled');
            $input->addValidation('mi_req',$_POST['meal_id'],'min=1', 'Should be filled');
			$input->addValidation('mn_format',$_POST['meal_name'],'alpha_numeric_sc', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('mn_req',$_POST['meal_name'],'min=1', 'Should be Filled');
			$input->addValidation('mn_max',$_POST['meal_name'],'max=50', 'Room Name is too long');
            $input->addValidation('mc_req',$_POST['meal_category'],'min=1', 'Should be filled');
            $input->addValidation('mc_format',$_POST['meal_category'],'numeric', 'Should be filled');
            $input->addValidation('mc_max',$_POST['meal_category'],'max=4', 'Should be filled');
            $input->addValidation('mv_req',$_POST['meal_vendor'],'min=1', 'Should be filled');
            $input->addValidation('mv_format',$_POST['meal_vendor'],'numeric', 'Should be filled');
            $input->addValidation('mv_max',$_POST['meal_vendor'],'max=4', 'Should be filled');
            $input->addValidation('mp_format',$_POST['meal_price'],'numeric', 'Please check your input');
            $input->addValidation('mp_min',$_POST['meal_price'],'min=1', 'Should be filled');
            $input->addValidation('mp_max',$_POST['meal_price'],'max=12', 'Please check your input');
			
			if($input->validate()){
				$m = $this->load->model('Meal');
				$is = array(
                        'meal_vendor' => $_POST['meal_vendor'],
						'meal_category' => $_POST['meal_category'],
						'meal_name'	=> $_POST['meal_name'],
						'meal_price' => $_POST['meal_price'],
						'meal_status' => $_POST['meal_status']
				);
                $w = array(
                    'meal_id' => $_POST['meal_id']
                );
				
				if($m->editMeal($is, $w)){
					echo json_encode(array('success'=>1));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0,'error'=>$_POST));
		}
	}

	#deleteMeal
	public function viewdeleteMeal()
	{
		$id=$_GET['idMeal'];
		$m = $this->load->model('Meal');
		$m->deleteMeal($id);
		header('Location: admin_meal.html');
		
	}
   
	
}
/*
* End Home Class
*/