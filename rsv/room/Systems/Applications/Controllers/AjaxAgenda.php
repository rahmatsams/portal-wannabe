<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxAgenda extends Controller
{
    private $message;
    
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewEventList', 'viewNewEvent', 'viewActiveOptionData', 'viewEventData', 'viewEventCancel', 'viewTest', 'viewEventListUser', 'viewUserCancel','viewEventDetail', 'viewEventDetailUser', 'viewEventPrint', 'viewEventPrintTraining', 'viewEventPerson', 'viewEventParticipants'),
                'groups'=>array('*'),
            ),
            array('Allow', 
                'actions'=>array('viewEventDecline' ,'viewEditEvent', 'viewEventListAdmin', 'viewEventPerson', 'viewEventParticipants'),
                'groups'=>array('Super Admin', 'Administrator','Admin Booking Room'),
            ),
            array('Deny', 
                'actions'=>array('viewEventList', 'viewNewEvent', 'viewActiveOptionData', 'viewEventData', 'viewEventCancel', 'viewTest', 'viewEventListUser', 'viewUserCancel','viewEventDetail', 'viewEventDetailUser', 'viewEventPrint', 'viewEventPrintTraining'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
    protected function eventModel() //
    {
        return $this->load->model('Event');
    }
    
    public function viewEventList()
    {
        if(isset($_GET['from']) && isset($_GET['to'])){
            $input = $this->load->lib('Input');
            $input->addValidation('from_format', $_GET['from'], 'numeric', 'Please check your input.');
            $input->addValidation('from_req', $_GET['from'], 'min=1', 'Please check your input.');
            $input->addValidation('from_length', $_GET['from'], 'max=15', 'Please check your input.');
            $input->addValidation('to_format', $_GET['to'], 'numeric', 'Please check your input.');
            $input->addValidation('to_req', $_GET['to'], 'min=1', 'Please check your input.');
            $input->addValidation('to_length', $_GET['to'], 'max=15', 'Please check your input.');
            if($input->validate()){
                $m = $this->load->model('Event');
                $data = array(
                    'st1' => date('Y-m-d', $_GET['from']/1000),
                    'st2' => date('Y-m-d', $_GET['to']/1000),
                    'ed1' => date('Y-m-d', $_GET['from']/1000),
                    'ed2' => date('Y-m-d', $_GET['to']/1000)
                );
                $edata = $m->getEventRangeUser($data);
                if(is_array($edata) && count($edata) > 0){
                    $r = array(
                        'success' => 1,
                        'result' => array()
                    );
                    $i = 0;
                    foreach($edata as $cdata){
                        if(isset($cdata['event_id'])){
                            $r['result'][$i]['id'] = $cdata['event_id'];
                            $r['result'][$i]['title'] = "{$cdata['room_name']} - {$cdata['event_name']}";
                            switch($this->_mySession['group']){
                                case "Administrator":
                                    $r['result'][$i]['url'] = "event_detail_admin_{$cdata['event_id']}.html";
                                    break;
                                case "Super Admin":
                                    $r['result'][$i]['url'] = "event_detail_admin_{$cdata['event_id']}.html";
                                    break;
                                default:
                                    $r['result'][$i]['url'] = "event_detail_user_{$cdata['event_id']}.html";
                            }
                            $r['result'][$i]['class'] = $cdata['room_color'];
                            $r['result'][$i]['time_start'] = date("H:i", strtotime($cdata['time_from']));
                            $r['result'][$i]['time_end'] = date("H:i", strtotime($cdata['time_until']));
                            $r['result'][$i]['start'] = strtotime($cdata['date_from']).'000';
                            $r['result'][$i]['end'] = strtotime($cdata['date_until']).'000';
                            $i++;
                        }
                    }
                }else{
                    $r = array(
                        'success' => 1,
                        'result' => array()
                    );
                }
                echo json_encode($r);
            }else{
                echo json_encode($input->_error);
            }
        }
    }
    
    public function viewEventListAdmin()
    {
        $m = $this->eventModel();
        $data = 0;
        $f = array();

        $o = array(
            'page' => ($_POST['start']/$_POST['length'] > 0 ? ($_POST['start']/$_POST['length']+1) : 1),
            'result' => $_POST['length'],
            'order_by' => 'date_from',
            'order' => 'ASC'
        );
        switch($_GET['id']){
            case 1:
                $data = $m->getUnconfirmedEvent($f, $o);
            break;
            case 2:
                $o['order_by'] = 'date_from,room_name';
                $data = $m->getFutureEvent($f, $o);
            break;
            default:
                $o['order'] = 'DESC';
                $data = $m->getPastEvent($f, $o);
        }
        #print_r($data);
        $result = array(
            'draw' => $_POST['draw'],
            'data' => $data,
            'recordsTotal' => $m->getCountResult(),
            'recordsFiltered' => $m->getCountResult(),
        );
        header("Content-Type: application/json");
        echo json_encode($result);
    }
    
    public function viewEventListUser()
    {
        $m = $this->eventModel();
        $data = 0;
        $f = array(
            'user_id' => $this->_mySession['user_id'],
        );

        $o = array(
            'page' => ($_POST['start']/$_POST['length'] > 0 ? ($_POST['start']/$_POST['length']+1) : 1),
            'result' => $_POST['length'],
            'order_by' => 'date_from',
            'order' => 'ASC'
        );
        switch($_GET['id']){
            case 1:
                $data = $m->getUnconfirmedEventUser($f, $o);
            break;
            case 2:
                $o['order_by'] = 'date_from,room_name';
                $data = $m->getFutureEventUser($f, $o);
            break;
            default:
                $o['order'] = 'DESC';
                $data = $m->getAllEventUser($f, $o);
        }
        $result = array(
            'draw' => $_POST['draw'],
            'data' => $data,
            'recordsTotal' => $m->getCountResult(),
            'recordsFiltered' => $m->getCountResult(),
        );
        header('Content-Type: application/json');
        echo json_encode($result);
 
    }
    
    public function viewNewEvent() #NEWBOOKING
    {
        if(isset($_POST['room']) && isset($_POST['event_name']) && isset($_POST['start_hour']) && isset($_POST['start_minute']) && isset($_POST['end_hour']) && isset($_POST['end_minute']) && isset($_POST['room_pax'])){
            $input = $this->load->lib('Input');
            $input->addValidation('room_required', $_POST['room'], 'min=1', 'Room is required');
            $input->addValidation('room_format', $_POST['room'], 'numeric', 'Please check your room input');
            $input->addValidation('event_required', $_POST['event_name'], 'min=1', 'Event name should be filled');
            $input->addValidation('event_format', $_POST['event_name'], 'alpha_numeric_sc', 'Please check your room input');
            $input->addValidation('start_required', $_POST['start_hour'].':'.$_POST['start_minute'], 'min=1', 'Book starting time is empty.');
            $input->addValidation('start_format', $_POST['start_hour'].':'.$_POST['start_minute'], 'hour', 'Please check your start time input');
            $input->addValidation('end_required', $_POST['end_hour'].':'.$_POST['end_minute'], 'min=1', 'Book ending time is empty.');
            $input->addValidation('end_format', $_POST['end_hour'].':'.$_POST['end_minute'], 'hour', 'Please check your end time input');
            if($input->validate()){
                $m = $this->load->model('Event');
                $mroom = $this->load->model('Room');
                $mcatering = $this->load->model('Provider');
                $st_time = date_format(date_create($_POST['start_date']),"Y-m-d").' '.$_POST['start_hour'].':'.$_POST['start_minute'].':00'; 
                if($_POST['end_date'] == ""){
                    $ed_time = date_format(date_create($_POST['start_date']),"Y-m-d").' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00';
                }else{
                    $ed_time = date_format(date_create($_POST['end_date']),"Y-m-d").' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00';
                }
                
                if($ed_time < $st_time){
                    if($_POST['end_date'] == ""){
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'time_from' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'time_until' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'room' => $_POST['room']
                        );
                    }else{
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['end_date']),"Y-m-d"),
                            'time_from' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'time_until' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'room' => $_POST['room']
                        );
                    }
                    
                }else{
                    if($_POST['end_date'] == ""){
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'time_from' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'time_until' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'room' => $_POST['room']
                        );
                    }else{
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['end_date']),"Y-m-d"),
                            'time_from' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'time_until' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'room' => $_POST['room']
                        );
                    }
                    
                }
                $data = $m->getOverlapDate($overlap);
                if(is_array($data) && count($data) > 0){
                    $output = array(
                        'user_name' => $data[0]['display_name'],
                        'current_event' => $data[0]['event_name'],
                        'event_start' => date("d-M-Y", strtotime($data[0]['date_from'])),
                        'event_end' => date("d-M-Y", strtotime($data[0]['date_until'])),
                        'time_start' => date("H:i", strtotime($data[0]['time_from'])),
                        'time_end' => date("H:i", strtotime($data[0]['time_until']))
                    );
                    $return = array(
                        'success' => 0,
                        'message' => 1,
                        'data' => $output
                    );
                }else{
                    $date_from = $overlap['date_from'];
                    $date_until = $overlap['date_until'];
                    $eo = $this->load->model('EventOptions');
                    while($date_from <= $date_until){ //submit with different event_id
                        $idTerakhir = 0;
                        $submit = array(
                            'event_name' => $_POST['event_name'],
                            'room_id' => $_POST['room'],
                            'room_pax' => $_POST['room_pax'],
                            'event_creator' => $this->_mySession['user_id'],
                            'submit_time' => date("Y-m-d H:i:s"),
                            'date_from' => $date_from,
                            'date_until' => $date_from,
                            'time_from' => $overlap['time_from'],
                            'time_until' => $overlap['time_until'],
                            'event_status' => 1,
                            'event_type_id' => $_POST['event_type']
                        );
                        if($m->newEvent($input->cleanInputArray($submit))){
                            $i = 1;
                            if(isset($_POST['option'])){
                                foreach($_POST['option'] as $option) 
                                {
                                    if ($option == 1) {
                                        $msl = $_POST['meal_snack']; 
                                    } 
                                    else if ($option == 7) 
                                    {
                                        $msl = $_POST['meal_lunch']; 
                                    }
                                    else
                                    {
                                        $msl = 1; 
                                    }

                                    $idTerakhir = $m->lastInsertID();
                                    $data = array(
                                        'event_id' => $m->lastInsertID(),
                                        'option_id' => $option, 
                                        'euser_value' => $msl
                                    );

                                    $eo->insertEUserOption($data);
                                }
                            }
                            /**/
                           

                            //if ($posisi_gm == "GM Admin") {
                                $mail_data_to_gm = array(
                                    'title' => 'Reservation #'.$idTerakhir.' (NEW) '.$this->_mySession['gm_position'],
                                    'view' => 'email/new',
                                    'data' => array(
                                        'detail' => $m->getEventBy(array('event_id' => $idTerakhir)),
                                        'content' => 'Permintaan booking ruangan dengan detail:',
                                        'header' => 'Permintaan Booking Ruangan (New)'
                                    ),
                                    'recipient' => array(
                                        'address' => $this->_mySession['gm_email'],
                                        'name' => $this->_mySession['gm_name']." (".$this->_mySession['gm_position'].")",
                                    ),
                                    
                                );
                            /*}elseif ($posisi_gm == "GM Operation") {
                                $mail_data_to_gm = array(
                                    'title' => 'Reservation #'.$idTerakhir.' (NEW) '.$posisi_gm,
                                    'view' => 'email/new',
                                    'data' => array(
                                        'detail' => $m->getEventBy(array('event_id' => $idTerakhir)),
                                        'content' => 'Permintaan booking ruangan dengan detail:',
                                        'header' => 'Permintaan Booking Ruangan (New)'
                                    ),
                                    'recipient' => array(
                                        'address' => "",
                                        'name' => "Yanuar (GM Operation)",
                                    ),
                                    
                                );
                            }elseif ($posisi_gm == "GM People&Inventory Management") {
                                $mail_data_to_gm = array(
                                    'title' => 'Reservation #'.$idTerakhir.' (NEW) '.$posisi_gm,
                                    'view' => 'email/new',
                                    'data' => array(
                                        'detail' => $m->getEventBy(array('event_id' => $idTerakhir)),
                                        'content' => 'Permintaan booking ruangan dengan detail:',
                                        'header' => 'Permintaan Booking Ruangan (New)'
                                    ),
                                    'recipient' => array(
                                        'address' => "",
                                        'name' => "Henky Pho (GM People&Inventory Management)",
                                    ),
                                    
                                );
                            }elseif ($posisi_gm == "Asst. GM Operation TS&SK") {
                                $mail_data_to_gm = array(
                                    'title' => 'Reservation #'.$idTerakhir.' (NEW) '.$posisi_gm,
                                    'view' => 'email/new',
                                    'data' => array(
                                        'detail' => $m->getEventBy(array('event_id' => $idTerakhir)),
                                        'content' => 'Permintaan booking ruangan dengan detail:',
                                        'header' => 'Permintaan Booking Ruangan (New)'
                                    ),
                                    'recipient' => array(
                                        'address' => "training2.jkt@sushitei.co.id", #vera.kusliawan@sushitei.co.id
                                        'name' => "Vera Kusliawan (Asst. GM Operation)",
                                    ),
                                    
                                );
                            }elseif ($posisi_gm == "Asst. GM Operation") {
                                $mail_data_to_gm = array(
                                    'title' => 'Reservation #'.$idTerakhir.' (NEW) '.$posisi_gm,
                                    'view' => 'email/new',
                                    'data' => array(
                                        'detail' => $m->getEventBy(array('event_id' => $idTerakhir)),
                                        'content' => 'Permintaan booking ruangan dengan detail:',
                                        'header' => 'Permintaan Booking Ruangan (New)'
                                    ),
                                    'recipient' => array(
                                        'address' => "training2.jkt@sushitei.co.id", #vera.kusliawan@sushitei.co.id
                                        'name' => "Vera Kusliawan (Asst. GM Operation)",
                                    ),
                                    
                                );
                            }else{
                                $mail_data_to_gm = array(
                                    'title' => 'Reservation #'.$idTerakhir.' (NEW) '.$posisi_gm,
                                    'view' => 'email/new',
                                    'data' => array(
                                        'detail' => $m->getEventBy(array('event_id' => $idTerakhir)),
                                        'content' => 'Permintaan booking ruangan dengan detail:',
                                        'header' => 'Permintaan Booking Ruangan (New)'
                                    ),
                                    'recipient' => array(
                                        'address' => "",
                                        'name' => "",
                                    ),
                                    
                                );
                            }*/

                            /*$mail_data_to_gm = array(
                                'title' => 'Reservation #'.$idTerakhir.' (NEW) '.$posisi_gm,
                                'view' => 'email/new',
                                'data' => array(
                                    'detail' => $m->getEventBy(array('event_id' => $idTerakhir)),
                                    'content' => 'Permintaan booking ruangan anda dengan detail:',
                                    'header' => ''
                                ),
                                'recipient' => array(
                                    'address' => "finaaj1806@gmail.com",
                                    'name' => "FINA ALF J",
                                ),
                                
                            );*/
                            $return = array(
                                'success' => 1,
                                'email' => 0,
                            );
                            
                            if($this->send_email($mail_data_to_gm)){
                                
                                $return['email'] = 1; 
                            }
                            $return['message'] = $this->message;
                        }
                        $date_from = date('Y-m-d', strtotime($date_from . "+1 days"));
                    }
                }
            }else{
                $return = array(
                    'success' => 0,
                    'email' => 0,
                    'error' => $input->_error
                );
            }
            header('Content-Type: application/json');
            echo json_encode($return);
        }else{
            header('Content-Type: application/json');
            echo json_encode(array('success'=>0));
        }
    }
    
    public function viewActiveOptionData()
    {
        $m = $this->load->model('EventOptions');
        $data = $m->getActiveOption();
        if(count($data) > 0){
            echo json_encode(array('success'=>1, 'data' => $data));
        }else{
            echo json_encode(array('success'=>0));
        }
    }
    
    #Print Event by Event Type
    public function viewEventPrint()
    {
        if (isset($_GET['eid'])) {
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_GET['eid'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_GET['eid'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_GET['eid'],'max=4', 'Max capacity is 9999');
            if ($input->validate()) {
                $m = $this->load->model('Event');
                $meo = $this->load->model('EventOptions');
                $meu = $this->load->model('EventUsers');
                $s = array(
                    'event_id' => $_GET['eid']
                );
                $data = $m->getEventBy($s);
                $optuser = $meu->getEUbyEvent($s);
                $m = null;
                if (count($data) > 0 && $data['event_type'] == "Training") #Print Training
                { 
                    $t = array(
                        'data' => $data, 
                        'web' => array(
                            'title' => 'Print Training'
                        )
                    );
                    #exit(var_dump($t));
                    #$this->load->view('home/print_training', $t);
                    echo "
                            <!doctype html>
                            <html>
                            <head>
                            <meta charset='utf-8'>
                            <!-- TemplateBeginEditable name='doctitle' -->
                            <title>Print Training</title>
                            <style>
                              .table1{
                                  border-width: 10%;
                                  font-size: 82%;
                                  font-family: Arial;
                                  /* border-right: 0px; */
                              }
                              .table2{
                                  border-width: 10%;
                                  font-size: 82%;
                                  font-family: Arial;
                              }
                              .table3{
                                  border-width: 10%;
                                  font-size: 57%;
                                  font-family: Arial;
                              }
                              .td1{
                                border-right: 0px;
                              }
                              .td2
                              {
                                border-right: 0px;
                                border-left: 0px;
                              }
                              .td3
                              {
                                border-left: 0px;
                              }
                              .table4
                              {
                                font-family: Tahoma;
                                font-size: 80%;
                              }
                              table 
                              {   
                                  padding-bottom: 2px;
                                  padding-top: 2px; 
                                  border-collapse: collapse;
                              }

                            </style>
                            <!-- TemplateEndEditable -->
                            <!-- TemplateBeginEditable name='head' -->
                            <!-- TemplateEndEditable -->
                                <script type='text/css'>
                                    
                                </script>
                            </head>
                                  <body>
                                    <div class='a'><br><br>
                                        <center>
                                    <table class='table4' width='600' border='0'>
                                        <tbody>
                                            <tr>
                                                <td width='100'><img src='Resources/images/LogoBig.jpg' alt='' width='50'/></td>
                                                    <center><td>
                                                    <center>
                                                        <b>
                                                            P T. S U S H I - T E I  &nbsp I N D O N E S I A  
                                                         </b><br>
                                                          SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                                                          Grand Wijaya Center Blok E No. 18-19 <br>
                                                          Jl. Wijaya II - Kebayoran Baru <br>
                                                          Jakarta Selatan 12160 <br>
                                                          Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                                                          </center>       
                                                        </td></center>
                                                    </tr>
                                                  </tbody>
                                                </table></center>
                                                    <br>
                                                    <center>
                                                      <h3>
                                                        
                                                          <font face='arial' size='2'>
                                                              FORM PERMOHONAN TRAINING
                                                          </font>
                                                        
                                                      </h3>
                                                    </center>
                                                        <div class='modal-body'>
                                                            <center><table width='600' border='0' class='table1'>
                                                              <tbody>
                                                                <tr>
                                                                  <td height='25'>
                                                                    <b>
                                                                        A. Data Diri Pemohon 
                                                                    </b>
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                        </table>
                                                      </center>
                                                            
                                                            <center>
                                                              <table width='600' border='1' class='table1'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='150' class='td1'>&nbsp Nama</td>
                                                                  <td width='450' class='td2'>:&nbsp &nbsp {$data['display_name']}</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Jabatan</td>
                                                                  <td class='td2'>:</td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </center>
                                                        
                                                            <center>
                                                              <table width='600' border='0' class='table1'>
                                                              <tbody><br>
                                                                <tr>
                                                                  <td height='25'><b>B. Informasi Pelaksaan Training</b></td>
                                                                </tr>
                                                              </tbody>
                                                              </table>
                                                            </center>

                                                        <center>
                                                            <table width='600' border='1' class='table1'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='150' class='td1'>&nbsp Nama Penyelenggara</td>
                                                                  <td width='450' class='td2' colspan='2'>:</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>
                                                                  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp     
                                                                  a. Internal</td>
                                                                  <td class='td2' colspan='2'>:</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>
                                                                  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp    
                                                                  b. Eksternal</td>
                                                                  <td class='td2' colspan='2'>:</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Judul Training</td>
                                                                  <td class='td2' colspan='2'>:&nbsp &nbsp {$data['event_name']}</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Tempat</td>
                                                                  <td class='td2' colspan='2'>:&nbsp &nbsp {$data['room_name']}</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Tanggal</td>
                                                                  <td class='td2' colspan='2'>:&nbsp &nbsp 
                                                                    Mulai &nbsp &nbsp 
                                                                    ".date("d-m-Y", strtotime($data['date_from']))."
                                                                    &nbsp &nbsp &nbsp &nbsp &nbsp  
                                                                    Selesai  &nbsp &nbsp 
                                                                    ".date("d-m-Y", strtotime($data['date_until']))." 
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Waktu</td>
                                                                  <td class='td2' colspan='2'>:&nbsp &nbsp 
                                                                    Mulai &nbsp &nbsp  ".date("H:i", strtotime($data['time_from']))." 
                                                                      &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                                                      Selesai &nbsp &nbsp  ".date("H:i", strtotime($data['time_until']))."
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Peserta</td>
                                                                  <td class='td2' colspan='2'>:</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Jumlah Peserta</td>
                                                                  <td class='td2' colspan='2'>:&nbsp &nbsp {$data['room_pax']} &nbsp orang</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Biaya <br>
                                                                    <font size='1'>&nbsp &nbsp (* Lingkari yang diperlukan)</font>
                                                                    <br><br></td>
                                                                  <td class='td2' width='100'>:
                                                                  &nbsp  
                                                                  a. Snack <br>
                                                                  &nbsp &nbsp 
                                                                  b. Lunch <br>
                                                                  &nbsp &nbsp 
                                                                  c. Lain-lain
                                                                  </td>
                                                                  <td class='td3' width='350'>:
                                                                        &nbsp {$data['vendor_snack']} 
                                                                        ".($data['snack'] != null ? '-' : ' ')."
                                                                        {$data['snack']}
                                                                        &nbsp ".($data['snack'] != null ? 'Rp' : ' ')." &nbsp 
                                                                        ".($data['snack'] != null ? $data['snack_price']*$data['room_pax'] : ' ')."
                                                                        <br>:
                                                                        &nbsp {$data['vendor_lunch']} 
                                                                        ".($data['lunch'] != null ? '-' : ' ')."
                                                                        {$data['lunch']}
                                                                        &nbsp ".($data['lunch'] != null ? 'Rp' : ' ')." &nbsp 
                                                                        ".($data['lunch'] != null ? $data['lunch_price']*$data['room_pax'] : ' ')."
                                                                        <br>:
                                                                        <br>
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                        </center>
                                                            
                                                      <center>
                                                        <table class='table1' width='600' border='0'>
                                                              <tbody><br>
                                                                <tr>
                                                                  <td height='25'><b>C. Proses Persetujuan<b></td>
                                                                </tr>
                                                              </tbody>
                                                        </table>
                                                      </center>
                                                            
                                                            <center>
                                                              <table width='600' border='1' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='210'>&nbsp;</td>
                                                                  <td width='130'><center><b>Nama</b></center></td>
                                                                  <td width='130'><center><b>Jabatan</b></center></td>
                                                                  <td width='130'><center><b>Tanggal</b></center></td>
                                                                </tr>
                                                                <tr>
                                                                  <td height='35'>Diajukan oleh :<br>
                                                                    <b>Pemohon</b>
                                                                    </td>
                                                                  <td>&nbsp;</td>
                                                                  <td>&nbsp;</td>
                                                                  <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                  <td height='35'>Disetujui oleh :<br>
                                                                    <b>Head Dept/BOM/BOD</b>
                                                                    </td>
                                                                  <td>&nbsp;</td>
                                                                  <td>&nbsp;</td>
                                                                  <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                  <td height='35'>Diproses oleh :<br>
                                                                    <b>Training Dept</b>
                                                                    </td>
                                                                  <td>&nbsp;</td>
                                                                  <td>&nbsp;</td>
                                                                  <td>&nbsp;</td>
                                                                </tr>
                                                              </tbody>
                                                            </table></center>
                                                            <br><br><br><br><br><br><br>
                                                            <center><table width='600' border='0' class='table3'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td width='100'>No Dokumen</td>
                                                                        <td width='10'>:</td>
                                                                        <td>FRM/TRG/01/01</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>No Revisi</td>
                                                                        <td width='10'>:</td>
                                                                        <td>3</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tanggal Efektif</td>
                                                                        <td width='10'>:</td>
                                                                        <td>14 November 2016</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                             </center>
                                          </div>
                                      </div>
                                </body> 
                            </html>

                    ";
                    
                }
                else if (count($data) > 0 && $data['event_type'] == "Meeting") #Print Meeting
                {
                   $y = array(
                        'data' => $data,
                        'web'  => array(
                                    'title' => 'Print Meeting', 
                                )
                    );
                   #echo "test print meeting {$data['event_id']}";
                   #exit(var_dump($y));
                   #$this->load->view('home/print_meeting', $y);
                    echo "
                    <!doctype html>
                    <html>
                    <head>
                    <meta charset='utf-8'>
                    <!-- TemplateBeginEditable name='doctitle' -->
                    <title>Print Meeting</title>
                    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'>
                    <link rel='stylesheet' href='Resources/assets/bootstrap-3.3.7-dist/css/bootstrap.min.css'>
                    <link rel='stylesheet' href='Resources/assets/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css'>
                    <style>
                      .table1{
                        font-size: 89%;
                        font-family: Arial;
                        border: 0;
                      }
                      .border-bottom-table{
                          border-bottom: 1px solid #000;
                      }
                      .border-top-table{
                          border-top: 1px solid #000;
                      }
                      @media print {
                      .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                        float: left;
                      }
                      .col-sm-12 {
                        width: 100%;
                      }
                      .col-sm-11 {
                        width: 91.66666667%;
                      }
                      .col-sm-10 {
                        width: 83.33333333%;
                      }
                      .col-sm-9 {
                        width: 75%;
                      }
                      .col-sm-8 {
                        width: 66.66666667%;
                      }
                      .col-sm-7 {
                        width: 58.33333333%;
                      }
                      .col-sm-6 {
                        width: 50%;
                      }
                      .col-sm-5 {
                        width: 41.66666667%;
                      }
                      .col-sm-4 {
                        width: 33.33333333%;
                      }
                      .col-sm-3 {
                        width: 25%;
                      }
                      .col-sm-2 {
                        width: 16.66666667%;
                      }
                      .col-sm-1 {
                        width: 8.33333333%;
                      }
                      .col-sm-pull-12 {
                        right: 100%;
                      }
                      .col-sm-pull-11 {
                        right: 91.66666667%;
                      }
                      .col-sm-pull-10 {
                        right: 83.33333333%;
                      }
                      .col-sm-pull-9 {
                        right: 75%;
                      }
                      .col-sm-pull-8 {
                        right: 66.66666667%;
                      }
                      .col-sm-pull-7 {
                        right: 58.33333333%;
                      }
                      .col-sm-pull-6 {
                        right: 50%;
                      }
                      .col-sm-pull-5 {
                        right: 41.66666667%;
                      }
                      .col-sm-pull-4 {
                        right: 33.33333333%;
                      }
                      .col-sm-pull-3 {
                        right: 25%;
                      }
                      .col-sm-pull-2 {
                        right: 16.66666667%;
                      }
                      .col-sm-pull-1 {
                        right: 8.33333333%;
                      }
                      .col-sm-pull-0 {
                        right: auto;
                      }
                      .col-sm-push-12 {
                        left: 100%;
                      }
                      .col-sm-push-11 {
                        left: 91.66666667%;
                      }
                      .col-sm-push-10 {
                        left: 83.33333333%;
                      }
                      .col-sm-push-9 {
                        left: 75%;
                      }
                      .col-sm-push-8 {
                        left: 66.66666667%;
                      }
                      .col-sm-push-7 {
                        left: 58.33333333%;
                      }
                      .col-sm-push-6 {
                        left: 50%;
                      }
                      .col-sm-push-5 {
                        left: 41.66666667%;
                      }
                      .col-sm-push-4 {
                        left: 33.33333333%;
                      }
                      .col-sm-push-3 {
                        left: 25%;
                      }
                      .col-sm-push-2 {
                        left: 16.66666667%;
                      }
                      .col-sm-push-1 {
                        left: 8.33333333%;
                      }
                      .col-sm-push-0 {
                        left: auto;
                      }
                      .col-sm-offset-12 {
                        margin-left: 100%;
                      }
                      .col-sm-offset-11 {
                        margin-left: 91.66666667%;
                      }
                      .col-sm-offset-10 {
                        margin-left: 83.33333333%;
                      }
                      .col-sm-offset-9 {
                        margin-left: 75%;
                      }
                      .col-sm-offset-8 {
                        margin-left: 66.66666667%;
                      }
                      .col-sm-offset-7 {
                        margin-left: 58.33333333%;
                      }
                      .col-sm-offset-6 {
                        margin-left: 50%;
                      }
                      .col-sm-offset-5 {
                        margin-left: 41.66666667%;
                      }
                      .col-sm-offset-4 {
                        margin-left: 33.33333333%;
                      }
                      .col-sm-offset-3 {
                        margin-left: 25%;
                      }
                      .col-sm-offset-2 {
                        margin-left: 16.66666667%;
                      }
                      .col-sm-offset-1 {
                        margin-left: 8.33333333%;
                      }
                      .col-sm-offset-0 {
                        margin-left: 0%;
                      }
                      .visible-xs {
                        display: none !important;
                      }
                      .hidden-xs {
                        display: block !important;
                      }
                      table.hidden-xs {
                        display: table;
                      }
                      tr.hidden-xs {
                        display: table-row !important;
                      }
                      th.hidden-xs,
                      td.hidden-xs {
                        display: table-cell !important;
                      }
                      .hidden-xs.hidden-print {
                        display: none !important;
                      }
                      .hidden-sm {
                        display: none !important;
                      }
                      .visible-sm {
                        display: block !important;
                      }
                      table.visible-sm {
                        display: table;
                      }
                      tr.visible-sm {
                        display: table-row !important;
                      }
                      th.visible-sm,
                      td.visible-sm {
                        display: table-cell !important;
                      }
                    }
                    </style>
                    <!-- TemplateEndEditable -->
                    <!-- TemplateBeginEditable name='head' -->
                    <!-- TemplateEndEditable -->
                    </head>

                        <body style='max-width: 650px; margin: 40px auto;'>
                            <div>
                                <table class='table4' border='0' width='600'>
                                    <tbody>
                                        <tr>
                                            <td width='100'><img src='Resources/images/LogoBig.jpg' width='50'/></td>
                                            <td style='text-align:center;'>
                                              <b>P T. S U S H I - T E I  &nbsp I N D O N E S I A </b><br>
                                              SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                                              Grand Wijaya Center Blok E No. 18-19 <br>
                                              Jl. Wijaya II - Kebayoran Baru <br>
                                              Jakarta Selatan 12160 <br>
                                              Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h3 style='text-align:center; font: arial; font-size: 110%; font-weight: bold;'>FORMULIR PERMOHONAN MEETING</h3>
                                <div style='font-size: 85%;'>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>1.</div>
                                            <div class='col-sm-3'>Nama Pemohon</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7 border-bottom-table'>{$data['display_name']}</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>2.</div>
                                            <div class='col-sm-3'>Jabatan Pemohon</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7 border-bottom-table'>&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>3.</div>
                                            <div class='col-sm-3'>Tanggal Meeting</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7 border-bottom-table'>".date("d-m-Y", strtotime($data['date_from']))."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>4.</div>
                                            <div class='col-sm-3'>Waktu Meeting</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7 border-bottom-table'>".date("H:i", strtotime($data['time_from']))." &nbsp &nbsp s/d &nbsp &nbsp ".date("H:i", strtotime($data['time_until']))."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>5.</div>
                                            <div class='col-sm-3'>Agenda Meeting</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7 border-bottom-table'>{$data['event_name']}</div>
                                        </div>
                                    </div>
                                    
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>6.</div>
                                            <div class='col-sm-3'>Tempat Meeting</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7 border-bottom-table'>{$data['room_name']}</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>7.</div>
                                            <div class='col-sm-3'>Jumlah Peserta Meeting</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7 border-bottom-table'>{$data['room_pax']} Orang</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>8.</div>
                                            <div class='col-sm-3'>Perkiraan Biaya</div>
                                            <div class='col-sm-8'>:</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-3' style='text-align: right;'>a.</div>
                                            <div class='col-sm-9 border-bottom-table'>".($data['snack'] != null ? "Snack : {$data['vendor_snack']}  Rp. ".$data['snack_price']*$data['room_pax'] : '&nbsp;')."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-3' style='text-align: right;'>b.</div>
                                            <div class='col-sm-9 border-bottom-table'>".($data['lunch'] != null ? "Lunch : {$data['vendor_lunch']}  -  Rp. ".$data['lunch_price']*$data['room_pax'] : '&nbsp;')."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-3' style='text-align: right;'>c.</div>
                                            <div class='col-sm-9 border-bottom-table'>&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-3' style='text-align: right;'>d.</div>
                                            <div class='col-sm-9 border-bottom-table'>&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12' style='margin-top: 10px;'>
                                        <div class='row'>
                                            <div class='col-sm-1'>9.</div>
                                            <div class='col-sm-3'>Sifat Meeting</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7'>a. Wajib, mendesak, dan harus dilaksanakan</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-5'>&nbsp;</div>
                                            <div class='col-sm-7'>b. Wajib dan tidak mendesak</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-5'>&nbsp;</div>
                                            <div class='col-sm-7'>c. Sebagai pelengkap</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>10.</div>
                                            <div class='col-sm-3'>Equipment</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7'>".($data['projector'] == 1 ? 'Projector' : '-')."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-5'>&nbsp;</div>
                                            <div class='col-sm-7'>".($data['flipchart'] == 1 ? 'Flipchart' : '-')."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-1'>11.</div>
                                            <div class='col-sm-3'>Layout</div>
                                            <div class='col-sm-1'>:</div>
                                            <div class='col-sm-7'>".($data['round_seat'] == 1 ? 'Round Seating' : '-')."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12'>
                                        <div class='row'>
                                            <div class='col-sm-5'>&nbsp;</div>
                                            <div class='col-sm-7'>".($data['column_seat'] == 1 ? 'Column Seating' : '-')."</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12' style='margin-top: 30px;'>
                                        <div class='row'>
                                            <div class='col-sm-1'>&nbsp;</div>
                                            <div class='col-sm-3 text-center'>Pemohon</div>
                                            <div class='col-sm-5'>&nbsp;</div>
                                            <div class='col-sm-3 text-center'>Menyetujui Head Department :</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12' style='margin-top: 70px;'>
                                        <div class='row'>
                                            <div class='col-sm-1'>&nbsp;</div>
                                            <div class='col-sm-3 text-center border-top-table'>TTD Tanggal</div>
                                            <div class='col-sm-5'>&nbsp;</div>
                                            <div class='col-sm-3 text-center border-top-table'>TTD Tanggal</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12' style='margin-top: 30px;'>
                                        <div class='row'>
                                            <div class='col-sm-4'>&nbsp;</div>
                                            <div class='col-sm-4 text-center'>Mengetahui GM :</div>
                                            <div class='col-sm-4'>&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class='col-lg-12' style='margin-top: 90px;'>
                                        <div class='row'>
                                            <div class='col-sm-4'>&nbsp;</div>
                                            <div class='col-sm-4 text-center border-top-table'>TTD Tanggal</div>
                                            <div class='col-sm-4'>&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                                
                                <table width='600' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='100'><font face='arial' size='1'>No Dokumen</font></td>
                                            <td width='10'><font face='arial' size='1'>:</font></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><font face='arial' size='1'>No Revisi</font></td>
                                            <td width='10'><font face='arial' size='1'>:</font></td>
                                            <td><font face='arial' size='1'>0</font></td>
                                        </tr>
                                        <tr>
                                            <td><font face='arial' size='1'>Tanggal Efektif</font></td>
                                            <td width='10'><font face='arial' size='1'>:</font></td>
                                            <td><font face='arial' size='1'>12 Juli 2018</font></td>
                                        </tr>
                                    </tbody>
                                </table>
                            <script src='Resources/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js'></script>
                        </body>
                    </html>

                    ";
                }
                else
                {
                    #echo json_encode(array('success'=>0));
                    #echo 'Event Type not found';
                    echo "<script>
                            alert('\"Activity Type\" belum dilengkapi');
                            </script>";
                }
            }
        }

    }

    public function viewEventDetail() #DETAIL EVENT UNTUK ADMIN
    {
        if(isset($_GET['eid'])){
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_GET['eid'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_GET['eid'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_GET['eid'],'max=4', 'Max capacity is 9999');
            if($input->validate()){
                $m = $this->load->model('Event');
                $meo = $this->load->model('EventOptions');
                $meu = $this->load->model('EventUsers');
                $s = array(
                    'event_id' => $_GET['eid']
                );
                $data = $m->getEventBy($s);
                $optuser = $meu->getEUbyEvent($s);
                #print_r($data);
                $m = null;
                if(count($data) > 0){
                    echo "
                    <div class='modal-body2 bg-primary'>
                    <div class='modal-body2'>
                    <form id=\"iOptionList\">
                        <div class='row'>
                            <div class='col-lg-12'>
                             <div class='row'>
                                    <div class='col-sm-4 form-group1'>
                                        <label>User Name</label>
                                    <input value=\"{$data['display_name']}\" id='iUserName' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-4 form-group1'>
                                         <label>Room Name</label>
                                    <input value=\"{$data['room_name']}\" id='iUserName' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-2 form-group1'>
                                         <label>&nbsp</label><br>
                                    <a href=\"print_meeting_{$_GET['eid']}.html\" class=\"btn1\" target=\"_blank\"><i class=\"glyphicon glyphicon-print\"></i>&nbsp <strong>Print</strong></a>
                                    </div>
                                    <div class='col-sm-2 form-group1'>
                                         <label>&nbsp</label><br>
                                    <a href=\"participants_list_{$_GET['eid']}.html\" class=\"btn1\" target=\"_blank\"><i class=\"glyphicon glyphicon-th-list\"></i><strong>Participants</strong></a>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-4 form-group1'>
                                     <label>Book Date</label>
                                    <input name=\"start_date\" value=".date("d-M-Y", strtotime($data['submit_time']))." id='iBookDate' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-4 form-group1'>
                                     <label>From</label>
                                    <input name=\"start_date\" value='".date("d-M-Y", strtotime($data['date_from']))." time ".date("H:i", strtotime($data['time_from']))."' id='iBookDate' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    
                                    <div class='col-sm-4 form-group1'>
                                     <label>Until</label>
                                    <input name=\"start_date\" value='".date("d-M-Y", strtotime($data['date_until']))." time ".date("H:i", strtotime($data['time_until']))."' id='iBookDate' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-3 form-group1'>
                                        <label>Activity Type</label>
                                    <input value=\"{$data['event_type']}\" id='eventType' placeholder='-' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-6 form-group1'>
                                        <label>Activity Name</label>
                                     <input value=\"{$data['event_name']}\" id='iEventName' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-3 form-group1'>
                                        <label>PAX</label>
                                    <input value=\"{$data['room_pax']} Person\" id='iPax' type=\"text\" placeholder='-' class=\"form-control\" disabled>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-4 form-group1'>
                                        <label>Snack</label>
                                    <input value=\"{$data['vendor_snack']}\" id='vendorSnack' placeholder='-' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-4 form-group1'>
                                        <label>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</label>
                                     <input value=\"{$data['snack']}\" id='mealSnack' placeholder='-' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-4 form-group1'>
                                        <label>Total</label>
                                     <input value=\"Rp ".$data['snack_price']*$data['room_pax']."\" id='snackPrice' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-4 form-group1'>
                                        <label>Lunch</label>
                                    <input value=\"{$data['vendor_lunch']}\" placeholder='-' id='vendorLunch' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-4 form-group1'>
                                        <label>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</label>
                                     <input value=\"{$data['lunch']}\" id='mealLunch' placeholder='-' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-4 form-group1'>
                                        <label>Total</label>
                                     <input value=\"Rp ".$data['lunch_price']*$data['room_pax']."\" id='lunchPrice' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-6 form-group1'>
                                        <label>Equipment</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Projector' disabled>
                                          <div class='input-group-addon'>".($data['projector'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                    <div class='col-sm-6 form-group1'>
                                        <label>&nbsp</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Flipchart' disabled>
                                          <div class='input-group-addon'>".($data['flipchart'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-6 form-group1'>
                                        <label>Layout</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Round Seating' disabled>
                                          <div class='input-group-addon'>".($data['round_seat'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                    <div class='col-sm-6 form-group1'>
                                        <label>&nbsp</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Column Seating' disabled>
                                          <div class='input-group-addon'>".($data['column_seat'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                </div>
                                <div class='row' id='iOptionList'>
                                </div>
                                 </form>
                                </div>
                                <br>
                            </div>
                            ";      
                }else{
                    echo json_encode(array('success'=>0));
                }
            }
        }
    }
    
    public function viewEventDetailUser() #EVENT DETAIL UNTUK PEMINJAM
    {
        if(isset($_GET['eid'])){
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_GET['eid'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_GET['eid'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_GET['eid'],'max=4', 'Max capacity is 9999');
            if($input->validate()){
                $m = $this->load->model('Event');
                $s = array(
                    'event_id' => $_GET['eid']
                );
                $data = $m->getEventBy($s);
                $m = null;
                if(count($data) > 0){
                    #print_r($data);
                    echo "
                    <div class='modal-body2 bg-primary'>
                     <div class='modal-body2'>
                    <form id=\"iOptionList\">
                        <div class='row'>
                            <div class='col-sm-7 form-group1'>
                                <label>User Name:&nbsp</label>
                                <input value=\"{$data['display_name']}\" id='iUserName' type=\"text\" class=\"form-control\" disabled>
                            </div>
                            <div class='col-sm-2 form-group1'>
                                 <label>&nbsp</label><br>
                                 <a href=\"print_meeting_{$_GET['eid']}.html\" class=\"btn1\" target=\"_blank\"><i class=\"glyphicon glyphicon-print\"></i>&nbsp <strong>Print</strong></a>
                            </div> 
                            <div class='col-sm-3 form-group1'>
                                         <label>&nbsp</label><br>
                                    <a href=\"participants_list_{$_GET['eid']}.html\" class=\"btn1\" target=\"_blank\"><i class=\"glyphicon glyphicon-th-list\"></i><strong>Participants</strong></a>
                                    </div>
                        </div>
                        <div class='row'>
                            <div class='col-lg-12'>
                                <div class='form-group1'>
                                    <label>Room Name</label>
                                    <select name=\"room\" class=\"form-control\" id=\"roomList\" disabled>
                                            <option>{$data['room_name']}</option>
                                    </select>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-4 form-group1'>
                                        <label>Book Date</label>
                                        <input name=\"start_date\" value=".date("d-m-Y", strtotime($data['submit_time']))." id='iBookDate' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-4 form-group1'>
                                        <label>From</label>
                                    <input name=\"start_date\" value='".date("d-M-Y", strtotime($data['date_from']))."  ".date("H:i", strtotime($data['time_from']))."' id='iBookDate' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    
                                    <div class='col-sm-4 form-group1'>
                                     <label>Until</label>
                                    <input name=\"start_date\" value='".date("d-M-Y", strtotime($data['date_until']))."  ".date("H:i", strtotime($data['time_until']))."' id='iBookDate' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-3 form-group1'>
                                        <label>Activity Type</label>
                                    <input value=\"{$data['event_type']}\" id='eventType' placeholder='-' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-6 form-group1'>
                                        <label>Activity Name</label>
                                     <input value=\"{$data['event_name']}\" id='iEventName' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                    <div class='col-sm-3 form-group1'>
                                        <label>PAX</label>
                                    <input value=\"{$data['room_pax']} Person\" id='iPax' type=\"text\" class=\"form-control\" disabled>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-6 form-group1'>
                                        <label>Equipment</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Projector' disabled>
                                          <div class='input-group-addon'>".($data['projector'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                    <div class='col-sm-6 form-group1'>
                                        <label>&nbsp</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Flipchart' disabled>
                                          <div class='input-group-addon'>".($data['flipchart'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-6 form-group1'>
                                        <label>Layout</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Round Seating' disabled>
                                          <div class='input-group-addon'>".($data['round_seat'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                    <div class='col-sm-6 form-group1'>
                                        <label>&nbsp</label>
                                        <div class='input-group'>
                                          <input type='text' class='form-control' id='exampleInputAmoun' value='Column Seating' disabled>
                                          <div class='input-group-addon'>".($data['column_seat'] == 1 ? 'Yes' : 'No')."</div>
                                        </div>
                                    </div>
                                </div>                          
                                <div class='row' id='iOptionList'>
                                 </form>
                                </div>
                                <br>
                            </div>
                    ";
                }else{
                    echo json_encode(array('success'=>0));
                }
            }
        }
    }
    
    public function viewEventData()
    {
            $m = $this->load->model('Event');
            $meo = $this->load->model('EventOptions');
            $meu = $this->load->model('EventUsers');
            $data = $m->getEventBy($_POST);
            $opt = $meo->getActiveOption();
            $optuser = $meu->getEUbyEvent($_POST);
            $m = null;
            if(count($data) > 0){
                foreach($opt as $key => $val){
                    $m .= "<div class='col-sm-6 form-group checkbox'>";
                    $m .= "<label><input name='option[]' type='checkbox' value='{$val['option_id']}'";
                    foreach($optuser as $key2 => $val2){
                        if($val['option_id'] == $val2['option_id'] && $val2['euser_value'] == 1){
                            $m .= " checked";
                        }
                    }
                     $m .= "> {$val['option_name']}</label></div>";
                }
                
                $output = array(
                    'room_id' => $data['room_id'],
                    'display_name' => $data['display_name'],
                    'room_name' => $data['room_name'],
                    'event_id' => $data['event_id'],
                    'event_type' => $data['event_type'],
                    'event_type_id' => $data['event_type_id'],
                    'event_name' => $data['event_name'],
                    'event_date' => date('Y-m-d', strtotime($data['date_from'])),
                    'event_start' => date('Y-m-d', strtotime($data['date_from'])),
                    'event_end' => date('Y-m-d', strtotime($data['date_until'])),
                    'time_from' => date('H', strtotime($data['time_from'])),
                    'time_from_2' => date('i', strtotime($data['time_from'])),
                    'time_until' => date('H', strtotime($data['time_until'])),
                    'time_until_2' => date('i', strtotime($data['time_until'])),
                    'room_pax' => $data['room_pax'],
                    'room_capacity' => $data['room_capacity'],
                    'vendor_id_snack' => $data['vendor_id_snack'], //vendor_id
                    'vendor_id_lunch' => $data['vendor_id_lunch'],
                    'vendor_snack' => $data['vendor_snack'],
                    'meal_id_snack' => $data['meal_id_snack'],
                    'meal_id_lunch' => $data['meal_id_lunch'],
                    'snack' => $data['snack'],
                    'jumlah' => $data['snack_price']*$data['room_pax'],
                    'vendor_lunch' => $data['vendor_lunch'],
                    'lunch' => $data['lunch'],
                    'total' => $data['lunch_price']*$data['room_pax'],
                    'opt' => $optuser
                );
                #print_r($output);
                header('Content-Type: application/json');
                echo json_encode(array('success'=>1, 'data' => $output));
            }else{
                echo json_encode(array('success'=>0));
            }
    }
    
    public function viewEventDecline()
    {
        if(isset($_POST['event_id']) && isset($_POST['decline_reason'])){
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
            if($input->validate()){
                $m = $this->load->model('Event');
                
                $i = array(
                    'status_desc' => $_POST['decline_reason'],
                    'event_status' => 0,
                );
                $w = array(
                    'event_id' => $_POST['event_id']
                );
                
                if($m->editEvent($i, $w)){
                    $data = $m->getEventBy(array('event_id' => $_POST['event_id']));
                    $n = $this->load->model('Notification');
                    $n_data = array(
                        'notification_user' => $data['event_creator'],
                        'notification_type' => 2,#Decline
                        'notification_status'=>0, #Unread
                    );
                    $n->newNotification($n_data);
                    $mail_data = array(
                        'title' => "Reservation #{$data['event_id']}.' (Declined)",
                        'view' => 'email/decline',
                        'data' => array(
                            'detail' => $data,
                            'content' => 'Permintaan booking ruangan anda dengan detail:',
                            'header' => 'Permintaan Anda Ditolak (Declined)'
                        ),
                        'recipient' => array(
                            'address' => $data['email'],
                            'name' => $data['display_name'],
                        ),
                        
                    );
                    $return = array(
                        'success' => 1,
                        'email' => 0,
                    );
                    
                    if($this->send_email($mail_data)){
                        $return['email'] = 1;
                    }
                    echo json_encode($return);
                }else{
                    echo json_encode(array('success'=>0));
                }
            
                
            }else{
                echo json_encode(array('success'=>0, 'error' => $input->_error));
            }
        }else{
            print_r($_POST);
            echo json_encode(array('success'=>0));
        }
    }
    
    public function viewEventCancel()
    {
        if(isset($_POST['event_id']) && isset($_POST['cancel_reason'])){
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
            if($input->validate()){
                $m = $this->eventModel();
                
                $i = array(
                    'status_desc' => $_POST['cancel_reason'],
                    'event_status' => 4,
                );
                $w = array(
                    'event_id' => $_POST['event_id']
                );
                
                if($m->editEvent($i, $w)){
                    $data = $m->getEventBy(array('event_id' => $_POST['event_id']));
                    $n = $this->load->model('Notification');
                    $n_data = array(
                        'notification_user' => $data['event_creator'],
                        'notification_type' => 4,#Decline
                        'notification_status'=>0, #Unread
                    );
                    $n->newNotification($n_data);
                    $mail_data = array(
                        'title' => "Reservation #{$data['event_id']}.' (Cancelled)",
                        'view' => 'email/cancel',
                        'data' => array(
                            'detail' => $data,
                            'content' => 'Permintaan booking ruangan anda dengan detail:',
                            'header' => 'Permintaan Anda Dibatalkan (Cancelled)'
                        ),
                        'recipient' => array(
                            'address' => $data['email'],
                            'name' => $data['display_name'],
                        ),
                        
                    );
                    $return = array(
                        'success' => 1,
                        'email' => 0,
                    );
                    
                    if($this->send_email($mail_data)){
                        $return['email'] = 1;
                    }
                    echo json_encode($return);
                }else{
                    echo json_encode(array('success'=>0));
                }
            
                
            }else{
                echo json_encode(array('success'=>0, 'error' => $input->_error));
            }
        }else{
            print_r($_POST);
            echo json_encode(array('success'=>0));
        }
    }
    
    public function viewUserCancel()
    {
        if(isset($_POST['event_id']) && isset($_POST['cancel_reason'])){
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
            if($input->validate()){
                $m = $this->eventModel();
                
                $i = array(
                    'status_desc' => $_POST['cancel_reason'],
                    'event_status' => 5,
                );
                $w = array(
                    'event_id' => $_POST['event_id']
                );
                
                $return = array('success' => 0);
                $data = $m->getEventBy(array('event_id' => $_POST['event_id']));
                
                if(date('Y-m-d') <= date('Y-m-d', strtotime($data['date_from']."-2 days"))){
                    if($m->editEvent($i, $w)){
                        $n = $this->load->model('Notification');
                        $n_data = array(
                            'notification_user' => $data['event_creator'],
                            'notification_type' => 5,#Decline
                            'notification_status'=>0, #Unread
                        );
                        $n->newNotification($n_data);
                        $mail_data = array(
                            'title' => "Reservation #{$data['event_id']}.' (Cancelled)",
                            'view' => 'email/cancel',
                            'data' => array(
                                'detail' => $data,
                                'content' => 'Permintaan booking ruangan anda dengan detail:',
                                'header' => 'Anda membatalkan request'
                            ),
                            'recipient' => array(
                                'address' => $data['email'],
                                'name' => $data['display_name'],
                            ),
                            
                        );
                        $return = array(
                            'success' => 1,
                            'email' => 0,
                        );
                        
                        if($this->send_email($mail_data)){
                            $return['email'] = 1;
                        }
                        
                    }
                }else{
                    $return['error'] = 'Pembatalan booking maksimal h-1';
                }
                echo json_encode($return);
            
                
            }else{
                echo json_encode(array('success'=>0, 'error' => $input->_error));
            }
        }else{
            print_r($_POST);
            echo json_encode(array('success'=>0));
        }
    }
    
    public function viewEditEvent() #EDITEVENT Future List
    {
        if(isset($_POST['event_id'])){
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
            if($input->validate()){
                $m = $this->load->model('Event');
                $st_time = date_format(date_create($_POST['start_date']),"Y-m-d").' '.$_POST['start_hour'].':'.$_POST['start_minute'].':00';
                if($_POST['end_date'] == ""){
                    $ed_time = date_format(date_create($_POST['start_date']),"Y-m-d").' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00';
                }else{
                    $ed_time = date_format(date_create($_POST['end_date']),"Y-m-d").' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00';
                }
                #$ed_time = date_format(date_create($_POST['end_date']),"Y-m-d").' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00';
                $dm = $m->getEventBy(array('event_id' => $_POST['event_id']));
                if($ed_time < $st_time){
                    if($_POST['end_date'] == ""){
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'time_from' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'time_until' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'room' => $_POST['room']
                        );
                    }else{
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['end_date']),"Y-m-d"),
                            'time_from' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'time_until' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'room' => $_POST['room']
                        );
                    
                    }
                    #print_r("1");
                }else{
                    if($_POST['end_date'] == ""){
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'time_from' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'time_until' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'room' => $_POST['room']
                        );
                    }else{
                        $overlap = array(
                            'date_from' => date_format(date_create($_POST['start_date']),"Y-m-d"),
                            'date_until' => date_format(date_create($_POST['end_date']),"Y-m-d"),
                            'time_from' => $_POST['start_hour'].':'.$_POST['start_minute'].':00',
                            'time_until' => $_POST['end_hour'].':'.$_POST['end_minute'].':00',
                            'room' => $_POST['room']
                        );
                    }
                #print_r("2");
                }
                $data= $m->getOverlapDate($overlap);
                #print_r($data);
                #exit("{$dm['event_id']} != {$_POST['event_id']}");
                if((is_array($data) && count($data) > 0) && $data[0]['event_id'] != $_POST['event_id']){
                    $return = array(
                        'success' => 0,
                        'message' => 1,
                        'data' => $data[0],
                    );
                    exit(json_encode($return));
                }else{
                    $i = array(
                        'room_id' => $_POST['room'],
                        'date_from' => $overlap['date_from'],
                        'date_until' => $overlap['date_until'],
                        'time_from' => $overlap['time_from'],
                        'time_until' => $overlap['time_until'],
                        'event_name' => $_POST['event_name'],
                        'room_pax' => $_POST['room_pax'],
                        'event_status' => 1,
                        'event_type_id' => $_POST['event_type'],
                    );
                    #var_dump($i);
                    $w = array(
                        'event_id' => $_POST['event_id']
                    );

                    #EDIT SNACK
                    if ( isset($_POST['meal_snack']) ) 
                    {
                        $eu = $this->load->model('EventOptions');
                        $eu->deleteEUserOption(array('event_id' => $_POST['event_id'],
                                                     'option_id' => 1 )); //jika dnyalakan lunch bs d edit apabila snack juga d edit
                        $ir = array(
                                    'event_id' => $_POST['event_id'],
                                    'option_id' => 1,
                                    'euser_value' => $_POST['meal_snack']    
                                 );
                        $eu->insertEUserOption($ir);
                        #var_dump($ir);
                    }
                    #EDITLUCH
                    if ( isset($_POST['meal_lunch']) ) 
                    {
                        $el = $this->load->model('EventOptions');
                        $el->deleteEUserOption(array('event_id' => $_POST['event_id'],
                                                    'option_id' => 7)); //jika d aktifkan maka snack tidak bs edit
                        $ih = array(
                                    'event_id' => $_POST['event_id'],
                                    'option_id' => 7,
                                    'euser_value' => $_POST['meal_lunch']    
                                 );
                        $el->insertEUserOption($ih);
                        #var_dump($ih);
                    }

                    #OPTIONCHECKBOX
                    if($m->editEvent($i, $w)){
                        $eo = $this->load->model('EventOptions');
                        
                        //Kerjaaaa
                        if(isset($_POST['option']))
                        {
                            foreach($_POST['option'] as $option) // untuk insert checkbox
                            {
                                
                                $msl = 1; 
                                $data = array(
                                    'event_id' => $_POST['event_id'],
                                    'option_id' => $option, 
                                    'euser_value' => $msl //bernilai 1
                                );
                                $data2 = array(
                                    'event_id' => $_POST['event_id'],
                                    'option_id' => $option
                                );
                                $cek = $eo->getEventUserByEventIDAndOptionID($data2);
                                if ($cek == NULL) {
                                    $eo->insertEUserOption($data);
                                }
                            }
                        }

                        $ed = $m->getEventBy(array('event_id' => $_POST['event_id']));
                        
                        $mail_data = array(
                            'title' => "Reservation #{$ed['event_id']}.' (Approved)",
                            'view' => 'email/approve',
                            'data' => array(
                                'detail' => $dm,
                                'content' => 'Permintaan booking ruangan anda dengan detail:',
                                'header' => 'Permintaan Anda Diterima (Approved)'
                            ),
                            'recipient' => array(
                                'address' => $dm['email'],
                                'name' => $dm['display_name'],
                            ),
                            
                        );
                        $return = array(
                            'success' => 1,
                            'email' => 0,
                        );
                        
                        if($this->send_email($mail_data)){ //mengirim email
                            $return['email'] = 1;
                        }
                        echo json_encode($return);
                    }else{
                        echo json_encode(array('success'=>0));
                    }
                }
                
            }else{
                echo json_encode(array('success'=>0, 'error' => $input->_error));
            }
        }else{
            echo json_encode(array('success'=>0));
        }
    }
    
    /* MAILING */
    private function send_email($option){ //ini proses nya
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail   = new PHPMailer(true);
        #$stmail = $this->load->lib('STMail');
        try
        {
            $stmail->IsSMTP(); // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                    // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "it6.jkt@sushitei.co.id"; // SMTP account username email dkirim dari alamat email ini
            $stmail->Password   = "(Jannah123)";        // SMTP account password

            $stmail->SetFrom('it6.jkt@sushitei.co.id', 'Room Booking'); //pengirim

            $stmail->AddReplyTo('it6.jkt@sushitei.co.id', 'Room Booking'); //reply to
            
            $stmail->isHTML('true');

            $stmail->Subject    = $option['title'];
            
             //ini var viewnya
            $returned = $this->load->view($option['view'], $option['data']);
            
            $stmail->MsgHTML($returned);
            
            $stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
            
            $m = $this->eventModel();
            /*foreach($m->getAdminAddress() as $recipient){
                $stmail->AddBCC($recipient['email'], $recipient['display_name']);
            }*/ //to admin
            return ($stmail->Send() ? 1 : 0);
        } catch (phpmailerException $e) {
            $this->message = $e->errorMessage();
            return 0;
            
        } catch (Exception $e) {
            $this->message = $e->getMessage();
            return 0;
            
        }
        
    }

    public function viewEventPerson() #DAFTARHADIR
    {
        $input = $this->load->lib('Input');
        $m = $this->load->model('Event');

        for($i = 0;$i < count($_POST['person_name']); $i++){
            if($_POST['person_name'][$i] != ""){
                $submit = array(
                    'id_event' => $_POST['event_id'],
                    'person_name' => $_POST['person_name'][$i],
                    'department_name' => $_POST['department_name'][$i],
                    'store_name' => $_POST['store_name'][$i],
                    'person_status' => 1 //Hadir
                );

                $m->newEventPerson($input->cleanInputArray($submit));
            }
        }

        $return = array(
            'success' => 1,
            'email' => 0,
        );

        echo json_encode($return);
        #header('Location: history.html');
    }

    public function viewEventParticipants()
    {
        if (isset($_GET['eid'])) {
            $input = $this->load->lib('Input');
            $input->addValidation('event_id_format',$_GET['eid'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
            $input->addValidation('event_id_required',$_GET['eid'],'min=1', 'Is Required');
            $input->addValidation('event_id_max',$_GET['eid'],'max=4', 'Max capacity is 9999');
            if ($input->validate()) {
                $m = $this->load->model('Event');
                $meo = $this->load->model('EventOptions');
                $meu = $this->load->model('EventUsers');
                $s = array(
                    'event_id' => $_GET['eid']
                );
                $s2 = array(
                    'id_event' => $_GET['eid']
                );
                $data = $m->getEventByPerson($s);
                $optuser = $meu->getEUbyEvent($s);
                $data2 = $m->getAllPerson($s2);
                $m = null;
                $page = 1;
                for($i = count($data2); $i > 10; $i = $i-10){
                    $page = $page + 1;
                }
                if (count($data) > 0 && $data['event_type'] == "Training") #Print Training
                { 
                    $t = array(
                        'data' => $data, 
                        'web' => array(
                            'title' => 'Peserta Training'
                        )
                    );
                    for($k = 1; $k <= $page; $k++){
                    $awal = ($k * 10) - 9;
                    echo "
                            <!doctype html>
                            <html>
                            <head>
                            <meta charset='utf-8'>
                            <!-- TemplateBeginEditable name='doctitle' -->
                            <title>Peserta Training</title>
                            <style>
                              .table1{
                                  border-width: 10%;
                                  font-size: 82%;
                                  font-family: Arial;
                                  /* border-right: 0px; */
                              }
                              .table2{
                                  border-width: 10%;
                                  font-size: 82%;
                                  font-family: Arial;
                              }
                              .table3{
                                  border-width: 10%;
                                  font-size: 57%;
                                  font-family: Arial;
                              }
                              .td1{
                                border-right: 0px;
                              }
                              .td2
                              {
                                border-right: 0px;
                                border-left: 0px;
                              }
                              .td3
                              {
                                border-left: 0px;
                              }
                              .table4
                              {
                                font-family: Tahoma;
                                font-size: 80%;
                              }
                              table 
                              {   
                                  padding-bottom: 2px;
                                  padding-top: 2px; 
                                  border-collapse: collapse;
                              }

                            </style>
                            <!-- TemplateEndEditable -->
                            <!-- TemplateBeginEditable name='head' -->
                            <!-- TemplateEndEditable -->
                                <script type='text/css'>
                                    
                                </script>
                            </head>
                                  <body>
                                                    <div class='a'><br><br>
                                                        <center>
                                                          <table class='table4' width='600' border='0'>
                                                  <tbody>
                                                    <tr>
                                                      <td width='100'><img src='Resources/images/LogoBig.jpg' alt='' width='50'/></td>
                                                      <center><td>
                                                        <center>
                                                          <b>
                                                            P T. S U S H I - T E I  &nbsp I N D O N E S I A  
                                                         </b><br>
                                                          SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                                                          Grand Wijaya Center Blok E No. 18-19 <br>
                                                          Jl. Wijaya II - Kebayoran Baru <br>
                                                          Jakarta Selatan 12160 <br>
                                                          Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                                                          </center>       
                                                        </td></center>
                                                    </tr>
                                                  </tbody>
                                                </table></center>
                                                    <br>
                                                    <center>
                                                      <h3>
                                                          <font face='arial' size='2'>
                                                              DAFTAR HADIR TRAINING
                                                          </font>
                                                      </h3>
                                                    </center>
                                                        <div class='modal-body'>
                                                            
                                                            <center>
                                                              <table width='600' border='0' class='table1'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='200' class='td1'>&nbsp Nama Training</td>
                                                                  <td width='400' class='td2'>:&nbsp &nbsp {$data['event_name']}</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Tempat dan Tanggal Training</td>
                                                                  <td class='td2'>:&nbsp &nbsp ".date("d-m-Y", strtotime($data['date_from']))."&nbsp s/d &nbsp".date("d-m-Y", strtotime($data['date_until']))." &nbsp ".date("H:i", strtotime($data['time_from']))."&nbsp-&nbsp".date("H:i", strtotime($data['time_until']))."&nbspWIB</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Nama Pembicara</td>
                                                                  <td class='td2'>:&nbsp;&nbsp;&nbsp 1.</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp </td>
                                                                  <td class='td2'>&nbsp;&nbsp;&nbsp;&nbsp 2.</td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </center>
                                                        
                                                        </br>
                                                            <center>
                                                              <table width='700' border='1' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='40' height='35' bgcolor='#D3D3D3'><center><b>NO</b></center></td>
                                                                  <td width='180' bgcolor='#D3D3D3'><center><b>NAMA</b></center></td>
                                                                  <td width='50' bgcolor='#D3D3D3'><center><b>NIK</b></center></td>
                                                                  <td width='50' bgcolor='#D3D3D3'><center><b>POSISI</b></center></td>
                                                                  <td width='100' bgcolor='#D3D3D3'><center><b>DEPARTEMEN</b></center></td>
                                                                  <td width='100' bgcolor='#D3D3D3'><center><b>OUTLET</b></center></td>
                                                                  <td width='80' bgcolor='#D3D3D3'><center><b>TANDA TANGAN</b></center></td>
                                                                </tr>";
                                                                if (count($data2) > 0) {
                                                                    $i = 1;
                                                                    $loop = 1;
                                                                foreach ($data2 as $key) {
                                                                    if($i >= $awal && $loop <= 10){
                                                                 ?>
                                                                    <tr>
                                                                      <td height='35' style="text-align: center;"><?php echo $i; ?><br>
                                                                        <b></b>
                                                                        </td>
                                                                      <td style="padding-left: 10px;"><?php echo $key['person_name'] ?></td>
                                                                      <td>&nbsp;</td>
                                                                      <td>&nbsp;</td>
                                                                      <td style="padding-left: 10px;"><?php echo $key['department_name'] ?></td>
                                                                      <td style="padding-left: 10px;"><?php echo $key['store_name'] ?></td>
                                                                      <td>&nbsp;</td>
                                                                    </tr>
                                                                <?php $i = $i + 1; $loop = $loop + 1; }else{$i = $i + 1;}}}
                                                              echo "</tbody>
                                                            </table></center>
                                                            <br><br>
                                                            <center><table width='700' border='0' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='250' bgcolor='#ffffff'>Tanda Tangan Trainer:</td>
                                                                  <td width='350' bgcolor='#ffffff'><center></center></td>
                                                                </tr>
                                                                </tbody>
                                                            </table></center>
                                                            <br>
                                                            <center><table width='700' border='0' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='250' style='border-bottom: 1px solid #000000;' bgcolor='#ffffff'>1.</td>
                                                                  <td width='350' bgcolor='#ffffff'><center></center></td>
                                                                </tr>
                                                                </tbody>
                                                            </table></center>
                                                            <br>
                                                            <center><table width='700' border='0' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='250' style='border-bottom: 1px solid #000000;' bgcolor='#ffffff'>2.</td>
                                                                  <td width='350' bgcolor='#ffffff'><center></center></td>
                                                                </tr>
                                                                </tbody>
                                                            </table></center>
                                                            <br>
                                                            <center><table width='300' border='1' class='table3'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td style='text-align:center'>Dibuat Oleh</td>
                                                                        <td style='text-align:center'>Diperiksa Oleh</td>
                                                                        <td style='text-align:center'>Disetujui Oleh</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Jabatan</td>
                                                                        <td style='text-align:center'>Training Manager</td>
                                                                        <td style='text-align:center'>GM Admin</td>
                                                                        <td style='text-align:center'>KTH</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></br>Tanda Tangan</br>&nbsp;</td>
                                                                        <td style='text-align:center'></br></td>
                                                                        <td style='text-align:center'></td>
                                                                        <td style='text-align:center'></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Nama</td>
                                                                        <td style='text-align:center'>Irpan Dodi</td>
                                                                        <td style='text-align:center'>Henky</td>
                                                                        <td style='text-align:center'>Ferita Deasy</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tanggal</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>   
                                                                </tbody>
                                                            </table>
                                             </center>
                                          </div>
                                      </div>
                                </body> 
                            </html><br/><hr size='1px'>

                    ";
                    
                }
            }
                else if (count($data) > 0 && $data['event_type'] == "Meeting") #Print Meeting
                {
                   $y = array(
                        'data' => $data,
                        'web'  => array(
                                    'title' => 'Peserta Meeting', 
                                )
                    );
                   
                for($k = 1; $k <= $page; $k++){
                    $awal = ($k * 10) - 9;
                    echo "<!doctype html>
                            <html>
                            <head>
                            <meta charset='utf-8'>
                            <!-- TemplateBeginEditable name='doctitle' -->
                            <title>Peserta Meeting</title>
                            <style>
                              .table1{
                                  border-width: 10%;
                                  font-size: 82%;
                                  font-family: Arial;
                                  /* border-right: 0px; */
                              }
                              .table2{
                                  border-width: 10%;
                                  font-size: 82%;
                                  font-family: Arial;
                              }
                              .table3{
                                  border-width: 10%;
                                  font-size: 57%;
                                  font-family: Arial;
                              }
                              .td1{
                                border-right: 0px;
                              }
                              .td2
                              {
                                border-right: 0px;
                                border-left: 0px;
                              }
                              .td3
                              {
                                border-left: 0px;
                              }
                              .table4
                              {
                                font-family: Tahoma;
                                font-size: 80%;
                              }
                              table 
                              {   
                                  padding-bottom: 2px;
                                  padding-top: 2px; 
                                  border-collapse: collapse;
                              }

                            </style>
                            <!-- TemplateEndEditable -->
                            <!-- TemplateBeginEditable name='head' -->
                            <!-- TemplateEndEditable -->
                                <script type='text/css'>
                                    
                                </script>
                            </head>
                                  <body>
                                                    <div class='a'><br><br>
                                                        <center>
                                                          <table class='table4' width='600' border='0'>
                                                  <tbody>
                                                    <tr>
                                                      <td width='100'><img src='Resources/images/LogoBig.jpg' alt='' width='50'/></td>
                                                      <center><td>
                                                        <center>
                                                          <b>
                                                            P T. S U S H I - T E I  &nbsp I N D O N E S I A  
                                                         </b><br>
                                                          SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                                                          Grand Wijaya Center Blok E No. 18-19 <br>
                                                          Jl. Wijaya II - Kebayoran Baru <br>
                                                          Jakarta Selatan 12160 <br>
                                                          Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                                                          </center>       
                                                        </td></center>
                                                    </tr>
                                                  </tbody>
                                                </table></center>
                                                    <br>
                                                    <center>
                                                      <h3>
                                                          <font face='arial' size='2'>
                                                              DAFTAR HADIR MEETING
                                                          </font>
                                                      </h3>
                                                    </center>
                                                        <div class='modal-body'>
                                                            
                                                            <center>
                                                              <table width='600' border='0' class='table1'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='200' class='td1'>&nbsp Agenda Meeting</td>
                                                                  <td width='400' class='td2'>:&nbsp &nbsp {$data['event_name']}</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Tempat dan Tanggal Meeting</td>
                                                                  <td class='td2'>: &nbsp ".date("d-m-Y", strtotime($data['date_from']))."&nbsp s/d &nbsp".date("d-m-Y", strtotime($data['date_until']))." &nbsp ".date("H:i", strtotime($data['time_from']))."&nbsp-&nbsp".date("H:i", strtotime($data['time_until']))."&nbspWIB</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp Nama Pembicara</td>
                                                                  <td class='td2'>:&nbsp 1. ".$data['display_name']."</td>
                                                                </tr>
                                                                <tr>
                                                                  <td class='td1'>&nbsp </td>
                                                                  <td class='td2'>&nbsp;&nbsp 2.</td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </center>
                                                        
                                                        </br>
                                                            <center>
                                                              <table width='700' border='1' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  <td width='40' height='35' bgcolor='#D3D3D3'><center><b>NO</b></center></td>
                                                                  <td width='250' bgcolor='#D3D3D3'><center><b>NAMA</b></center></td>
                                                                  <td width='140' bgcolor='#D3D3D3'><center><b>DEPARTEMEN</b></center></td>
                                                                  <td width='140' bgcolor='#D3D3D3'><center><b>OUTLET</b></center></td>
                                                                  <td width='90' bgcolor='#D3D3D3'><center><b>TANDA TANGAN</b></center></td>
                                                                </tr>";
                                                                if (count($data2) > 0) {
                                                                    $i = 1;
                                                                    $loop = 1;
                                                                foreach ($data2 as $key) {
                                                                    if($i >= $awal && $loop <= 10){
                                                                 ?>
                                                                    <tr>
                                                                      <td height='35' style="text-align: center;"><?php echo $i; ?><br>
                                                                        <b></b>
                                                                        </td>
                                                                      <td style="padding-left: 10px;"><?php echo $key['person_name'] ?></td>
                                                                      <td style="padding-left: 10px;"><?php echo $key['department_name'] ?></td>
                                                                      <td style="padding-left: 10px;"><?php echo $key['store_name'] ?></td>
                                                                      <td>&nbsp;</td>
                                                                    </tr>
                                                                <?php $i = $i + 1; $loop = $loop + 1; }else{$i = $i + 1;}}}
                                                              echo "</tbody>
                                                            </table></center>
                                                            <br><br>
                                                            <center><table width='600' border='0' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  
                                                                  <td width='250' bgcolor='#ffffff'><center>Penanggung Jawab Meeting:</center></td>
                                                                  <td width='100' bgcolor='#ffffff'><center></center></td>
                                                                  <td width='250' bgcolor='#ffffff'><center>Diketahui Oleh:</center></td>
                                                                  
                                                                </tr>
                                                                </tbody>
                                                            </table></center>
                                                            <br><br><br><br><br>
                                                            <center><table width='600' border='0' class='table2'>
                                                              <tbody>
                                                                <tr>
                                                                  
                                                                  <td width='250' style='border-top: 1px solid #000000;' bgcolor='#ffffff'><center>( Pemohon Meeting )</center></td>
                                                                  <td width='100' bgcolor='#ffffff'><center></center></td>
                                                                  <td width='250' style='border-top: 1px solid #000000;' bgcolor='#ffffff'><center>( GM )</center></td>
                                                                  
                                                                </tr>
                                                                </tbody>
                                                            </table></center>
                                                            <br>
                                                            <center><table width='600' border='0' class='table3'>
                                                                <tbody>
                                                                    <tr>
                                                                        <td width='100'>No Dokumen</td>
                                                                        <td width='10'>:</td>
                                                                        <td>FRM/TRG/09/02</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>No Revisi</td>
                                                                        <td width='10'>:</td>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Tanggal Efektif</td>
                                                                        <td width='10'>:</td>
                                                                        <td>1 Agustus 2018</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                             </center>
                                          </div>
                                      </div>
                                </body> 
                            </html>

                    ";
                }
                }
                else
                {
                    #echo json_encode(array('success'=>0));
                    #echo 'Event Type not found';
                    #echo "<script>
                    #        alert('\"Activity Type\" belum dilengkapi');
                    #        </script>";
                }
            }
        }

    }
    
}
/*
* End Home Class
*/