<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxAdmin extends Controller
{
	public function accessRules()
	{
		return array(
            array('Allow', 
				'actions'=>array('viewEventList', 'viewNewBook', 'viewActiveOptionData', 'viewEventData', 'viewEventDecline', 'viewNewOption', 'viewEditOption', 'viewOptionData', 'viewNewMeal', 'viewEditMeal', 'viewDeleteOption'),
				'groups'=>array('Administrator','Super Admin','Admin Booking Room'),
			),
			array('Deny', 
				'actions'=>array('viewEventList', 'viewNewBook', 'viewActiveOptionData', 'viewEventData'),
				'groups'=>array('Guest'),
			),
		);
	}
	
	public function viewEventData()
	{
		
            $m = $this->load->model('Event');
            $data = $m->getEventBy($_POST);
            if(count($data) > 0){
				$output = array(
					'room_id' => $data['room_id'],
					'event_id' => $data['event_id'],
					'event_name' => $data['event_name'],
					'event_date' => date('Y-m-d', strtotime($data['start_time'])),
					'event_start' => date('H:i:s', strtotime($data['start_time'])),
					'event_end' => date('H:i:s', strtotime($data['end_time'])),
					'room_pax' => $data['room_pax'],
					'room_capacity' => $data['room_capacity'],
				);
                echo json_encode(array('success'=>1, 'data' => $output));
            }else{
                echo json_encode(array('success'=>0));
            }
	}
	
    
    #OPTION_DATA
	public function viewOptionData()
	{
		if(isset($_POST['option_id'])){
			$input = $this->load->lib('Input');
			$input->addValidation('option_id_format',$_POST['option_id'],'numeric', 'Cannot find Room');
			$input->addValidation('option_id_required',$_POST['option_id'],'min=1', 'Is Required');
			$input->addValidation('option_id_max',$_POST['option_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->load->model('EventOptions');
				$data = $m->getOptionByID($_POST);
				if(count($data) > 0){
					echo json_encode(array('success'=>1, 'data' => $data));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0, 'error' => $_POST));
		}
	}
	
	#SUBMIT_OPTION
	public function viewNewOption()
	{
		if(isset($_POST['option_name'])){
			$input = $this->load->lib('Input');
			$input->addValidation('option_name_format',$_POST['option_name'],'alpha_numeric_sc', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('option_name_required',$_POST['option_name'],'min=4', 'Room Name should contain at least 4 character');
			$input->addValidation('option_name_max',$_POST['option_name'],'max=50', 'Room Name is too long');
			
			if($input->validate()){
				$m = $this->load->model('EventOptions');
                $_POST['option_status'] = 1;
				if($m->newOption($_POST)){
					echo json_encode(array('success'=>1));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0,'error'=>$_POST));
		}
	}
	
	#EDIT_OPTION
	public function viewEditOption()
	{
		if(isset($_POST['option_id']) && isset($_POST['option_name'])){
			$input = $this->load->lib('Input');
			$input->addValidation('option_id_format',$_POST['option_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('option_id_required',$_POST['option_id'],'min=1', 'Is Required');
			$input->addValidation('option_id_max',$_POST['option_id'],'max=4', 'Max capacity is 9999');
			$input->addValidation('option_name_format',$_POST['option_name'],'alpha_numeric_sp', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('option_name_required',$_POST['option_name'],'min=4', 'Room Name should contain at least 4 character');
			$input->addValidation('option_name_max',$_POST['option_name'],'max=50', 'Room Name is too long');
			
			if($input->validate()){
				$m = $this->load->model('EventOptions');
				$w = array(
					'option_id' => $_POST['option_id']
				);
				unset($_POST['option_id']);
				if($m->editOption($_POST, $w)){
					echo json_encode(array('success'=>1));
				}else{
					echo json_encode(array('success'=>0));
				}
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
            print_r($_POST);
			#echo json_encode(array('success'=>0));
		}
	}

	#DELETE_OPTION
	public function viewDeleteOption()
	{
		$id = $_GET['idOption'];
		$m = $this->load->model('EventOptions');
		$m->deleteOption($id);
		header('Location: admin_option.html');
	}

	/* Mailing */
	private function send_email($option){
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        #$stmail = $this->load->lib('STMail');
		try
		{
			$stmail->IsSMTP(); // telling the class to use SMTP
			$stmail->Host       = "mail.sushitei.co.id"; // SMTP server
			$stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
													   // 1 = errors and messages
													   // 2 = messages only
			$stmail->SMTPAuth   = true;                  // enable SMTP authentication
			$stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
			$stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
			$stmail->Username   = "any@sushitei.co.id"; // SMTP account username
			$stmail->Password   = "St123456";        // SMTP account password

			$stmail->SetFrom('any@sushitei.co.id', 'Web Booking');

			$stmail->AddReplyTo('any@sushitei.co.id', 'Web Booking');
			
			$stmail->isHTML('true');

			$stmail->Subject    = $option['title'];
			
            ob_start();
            $this->load->view($option['view'], $option['data']);
            $returned = ob_get_contents();
            ob_end_clean();
            
			$stmail->MsgHTML($returned);
			
			$stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
			
			$m = $this->eventModel();
			foreach($m->getAdminAddress() as $recipient){
				$stmail->AddBCC($recipient['email'], $recipient['display_name']);
			}
			return ($stmail->Send() ? 1 : 0);
		} catch (phpmailerException $e) {
		    $this->message = $e->errorMessage();
			return 0;
		    
		} catch (Exception $e) {
			$this->message = $e->getMessage();
			return 0;
			
		}
        
        
    }
	
}
/*
* End Home Class
*/