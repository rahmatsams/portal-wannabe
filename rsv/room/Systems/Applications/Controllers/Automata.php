<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Automata extends Controller
{
    
    public function accessRules()
    {
		return array(
			array('Allow', 
				'actions'=>array('viewIndex'),
				'groups'=>array('*'),
			),
		);
    }
    
    public function viewIndex()
    {
        $m = $this->load->model('Event');
        $days = 3;
        $d = array();
        $n = 0;
        foreach($m->getNotified($days) as $data){
           $d[$n] = $this->send_email($data);
           $n++;
        }
        print_r($d);
    }

    private function send_email($data){
        $option = array(
            'title' => "Reservation #{$data['event_id']}.' (Confirmation)",
            'view' => 'email/confirmation',
            'data' => array(
                'detail' => $data,
                'content' => 'Permintaan booking ruangan anda dengan detail:',
                'header' => 'Email ini hanya berisi informasi mengenai ruangan yang anda booking sebagai konfirmasi terakhir sebelum Proses Cancel di tutup (H-2).'
            ),
            'recipient' => array(
                'address' => $data['email'],
                'name' => $data['display_name'],
            ),
            
        );
        $returned = $this->load->view($option['view'], $option['data']);
        
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail   = new PHPMailer(true);
        #$stmail = $this->load->lib('STMail');
        try
        {
            $stmail->IsSMTP();                          // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "it6.jkt@sushitei.co.id"; // SMTP account username
            $stmail->Password   = "(Jannah123)";        // SMTP account password

            $stmail->SetFrom('it6.jkt@sushitei.co.id', 'Room Booking');

            $stmail->AddReplyTo('it6.jkt@sushitei.co.id', 'Room Booking');
            
            $stmail->isHTML('true');

            $stmail->Subject    = $option['title'];
            
            $stmail->MsgHTML($returned);
            
            $stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
            $stmail->AddAddress('it6.jkt@sushitei.co.id', 'Fina Alfiatul Jannah');
            
            $m = $this->load->model('Event');
            foreach($m->getAdminAddress() as $recipient){
                $stmail->AddBCC($recipient['email'], $recipient['display_name']);
            }
            
            $sd = array(
                'user' => $option['recipient']['address'],
                'email_status'  => $stmail->Send()
            );
            return $sd;
            
        } catch (phpmailerException $e) {
            $sd = array(
                'user' => $option['recipient']['address'],
                'email_status'  => 0,
                'message' => $e->errorMessage()
            );
            
            return $sd;
            
        } catch (Exception $e) {
                $sd = array(
                    'user' => $option['recipient']['address'],
                    'email_status'  => 0,
                    'message' => $e->getMessage(),
                    'data' => $returned
                );
                return $sd;
        }
        
        
    }
    
}
/*
* End Home Class
*/