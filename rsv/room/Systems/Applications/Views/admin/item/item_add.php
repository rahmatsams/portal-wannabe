
<a href="#"><strong><i class="glyphicon glyphicon-dashboard"></i> Add Item</strong></a>
<hr>
  <div class="col-lg-12 well">
    <div class="row">
      <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
        <div class="col-sm-12">
          <div class="form-group">
            <label>Code</label>
            <input name="item_code" type="text" value="<?=$item['item_code']?>" class="form-control" maxlength="20" required>
          </div>
          <div class="form-group">
            <label>Name</label>
            <input name="item_name" type="text" value="<?=$item['item_name']?>" class="form-control" maxlength="75" required>
          </div>
          <div class="form-group">
            <label>Colour</label>
            <select name="colour" class="form-control" required>
                <?php
                    if(is_array($colour_list) && count($colour_list) > 0){
                        foreach($colour_list as $result){
                            echo "<option value=\"{$result['colour_id']}\">{$result['colour_name']}</option>";
                        }
                    }
                ?>
            </select>
            <?php if(isset($error)) echo $error?>
          </div>
          <div class="form-group">
            <label>Price</label>
            <input name="price" type="text" value="<?=$item['price']?>" class="form-control" maxlength="75" required>
          </div>
          <button type="submit" name="submit" value="add" class="btn btn-md btn-primary">&nbspSave&nbsp</button>
          &nbsp
          <button type="cancel" class="btn btn-md btn-primary" onclick="window.location='admin_item.html';return false;">Cancel</button>
        </div>
      </form> 
    </div>
  </div>

          