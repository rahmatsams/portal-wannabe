	<?php
        $category = '';
        $vendor = '';
        foreach($meal_category as $c){
            $category .= "
                                    <option value='{$c['meal_category_id']}'>{$c['meal_category_name']}</option>";
        }
        foreach($meal_vendor as $c){
            $vendor .= "
                                    <option value='{$c['vendor_id']}'>{$c['vendor_name']}</option>";
        }
        
    ?>
    
    <div class="row">
		<div class="col-lg-3">
			<a href="#"><strong><i class="glyphicon glyphicon-calendar"></i> Admin Menu</strong></a>
			<hr>

			<?=$menu?>

		</div>
		<div class="col-lg-9">
			<h2 class="sub-header"><?=$page_name?></h2>
			<ol class="breadcrumb">
				<li><a href="admin.html">Admin Page</a></li>
				<li class="active"><a href="#"><?=$page_name?></a></li>
			</ol>
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>#</th>
                            <th>Name</th>
                            <th>Vendor</th>
							<th>Category</th>
							<th>Price</th>
							<th>Status</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody id="listTable">
						<?php
							if(is_array($meal) && !empty($meal)){
								$num = (($page-1)*10)+1;
								foreach($meal as $result){
									switch($result['meal_status']){
										case 1:
											$status = 'Active';
											break;
										default:
											$status = 'Disabled';
									}
									echo "
						<tr>
							<td>{$num} </td>
                            <td>{$result['meal_name']}</td>
                            <td>{$result['vendor_name']}</td>
							<td>{$result['meal_category_name']}</td>
							<td>{$result['meal_price']}</td>	
							<td>{$status}</td>
							<td><a href=\"#edit{$num}\" id='edit{$num}' data-id='{$result['meal_id']}' class='edit-users btn btn-primary btn-sm edit-meal'>Edit</a></td>
							<td><a href=\"admin_meal_delete_{$result['meal_id']}.html\" id='delete{$num}' onClick=\"return doconfirm();\" class='btn btn-danger btn-sm'>Delete</a></td>
						</tr>";
									$num++;
								}
							} else {
						?>
						<tr>
							<td colspan="4">There's no result found</td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
				
				<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newMealModal">New Meal</a>

			</div>
		</div>
	</div>
	
<!-- 	onClick=\"return doconfirm();\"-->	
	<div id="newMealModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel">Create New Meal</h4>

					</div>
					<div class="modal-body">
						<div class="row">
							<form id="newMeal" method="POST" action="submit_new_meal.html">
								<div class="col-lg-12">
                                    <div class="form-group ">
										<label>Vendor</label>
										<select name="meal_vendor" class="form-control" required>
											<option value=''>- Vendor -</option><?=$vendor?>
                                        </select>
									</div>
									<div class="form-group ">
										<label>Category</label>
										<select name="meal_category" class="form-control" required>
											<option value=''>- Category -</option><?=$category?>
                                        </select>
									</div>
									<div class="form-group ">
										<label>Catering Name</label>
										<input name="meal_name" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
									</div>
									<div class="form-group ">
										<label>Price</label>
										<input name="meal_price" type="number" placeholder="Type here.." class="form-control" maxlength="12"  required>
									</div>

									<button type="submit" name="submit" value="create" class="btn btn-md btn-primary">Create Meal</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div id="editMealModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel2">Edit Meal</h4>

					</div>
					<div class="modal-body">
						<div class="row">
							<form id="editMeal" method="POST" action="submit_edit_meal.html">
								<div class="col-lg-12">
                                    <div class="form-group ">
										<label>Vendor</label>
										<select name="meal_vendor" id="eMealVendor" class="form-control" required><?=$vendor?>
                                        </select>
									</div>
                                    <div class="form-group ">
										<label>Category</label>
										<select name="meal_category" id="eMealCategory" class="form-control" required><?=$category?>
                                        </select>
									</div>
									<div class="form-group ">
										<label>Catering Name</label>
										<input name="meal_name" type="text" id="eMealName" placeholder="Type here.." class="form-control" maxlength="50" required>
                                        <input name="meal_id" type="hidden" id="eMealID" required>
									</div>
									<div class="form-group ">
										<label>Price</label>
										<input name="meal_price" type="number" id="eMealPrice" placeholder="Type here.." class="form-control" maxlength="50" required>
									</div>
                                    <div class="form-group ">
										<label>Status</label>
										<select name="meal_status" id="eMealStatus" class="form-control" required>
                                            <option value="0">Disabled</option>
                                            <option value="1">Enabled</option>
                                        </select>
									</div>
									<button type="submit" name="submit" value="edit" class="btn btn-md btn-primary">Edit Meal</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<script>
		function doconfirm()
		{
			job=confirm("Are you sure to delete?");
			if (job!=true) 
			{
				return false;
			}
		}
	</script>