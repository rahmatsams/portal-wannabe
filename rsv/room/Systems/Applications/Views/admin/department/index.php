	<div class="row">
		<div class="col-lg-3">
			<a href="#"><strong><i class="glyphicon glyphicon-folder-open"></i> Admin Menu</strong></a>
			<hr>

			<?=$menu?>

		</div>
		<div class="col-lg-9">
			<h2 class="sub-header">Manage Department</h2>
			<ol class="breadcrumb">
				<li><a href="admin.html">Admin Page</a></li>
				<li class="active"><a href="#">Manage Department</a></li>
			</ol>
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Department</th>
							<th>General Manager</th>
							<th>GM Name</th>
							<th>GM Email</th>
							<th>Status</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody id="listTable">
						<?php
							if(is_array($department) && !empty($department)){
								$num = (($page-1)*10)+1;
								foreach($department as $result){
									switch($result['department_status']){
										case "1":
											$status = 'Active';
										break;
										default:
											$status = 'Disabled';
									}
									echo "
						<tr>
							<td>{$num} </td>
							<td>{$result['department_name']}</td>
							<td>{$result['gm_position']}</td>
							<td>{$result['gm_name']}</td>
							<td>{$result['gm_email']}</td>
							<td>{$status}</td>
							<td><a href=\"#edit{$num}\" id='edit{$num}' data-id='{$result['department_id']}' class='edit-users btn btn-primary btn-sm editDepartment'>Edit</a>
							</td>
							<td><a href=\"admin_department_delete_{$result['department_id']}.html\" id='delete{$num}' onClick=\"return doconfirm();\" class='btn btn-danger btn-sm'>Delete</a></td>
						</tr>";
									$num++;
								}
							} else {
						?>
						<tr>
							<td colspan="4">There's no result found</td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
				<?php
				$pages = ceil($total/$max_result);
					if($pages > 1){
						echo '
					<nav>
						<ul class="pagination pagination-sm">
							<li>
								<a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
							</li>';
						for($i=1;$i <= $pages;$i++){
							echo "<li ". ($page == $i ? 'class="active"' : '') ."><a href='admin_user_{$i}.html'>{$i}</a></li>";
						}
						echo '
						<li>
							<a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
						</li>
						</ul>
					</nav>';
					}
				?>
				
			<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newDepartmentModal">New Department</a>

		</div>
	</div>

<div id="newDepartmentModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
						 <h4 class="modal-title" id="myModalLabel">Create New Department</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<form id="newDepartment" method="POST" action="submit_new_department.html">
								<div class="col-lg-12">
									<div class="form-group">
										<label>Department Name</label>
											<input name="department_name" type="text" placeholder="Type here.." class="form-control" maxlength="50" required>
									</div>
									<div class="form-group">
										<label>GM Position</label>
											<select name="gm_position" class="form-control" required>
												<option value=''>- GM Position -</option>
												<option value='GM Operation'>GM Operation</option>
												<option value='GM People&Inventory Management'>GM People&Inventory Management</option>
												<option value='GM Admin'>GM Admin</option>
												<option value='AGM Operation TS&SK'>AGM Operation TS&SK</option>
												<option value='AGM Operation ST'>AGM Operation ST</option>
												<option value='AGM Inventory Management'>AGM Inventory Management</option>
	                                        </select>
									</div>
									<div class="form-group">
										<label>GM Name</label>
											<input name="gm_name" type="text" placeholder="Type here.." class="form-control" maxlength="50" required>
									</div>
									<div class="form-group">
										<label>GM Email</label>
											<input name="gm_email" type="email" placeholder="Type here.." class="form-control" maxlength="50" required>
									</div>

									<button type="submit" value="create" class="btn btn-sm btn-primary">Create Department</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--EDIT DEPARTMENT -->
	<div id="editDepartmentModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
						 <h4 class="modal-title" id="myModalLabel2">Edit Department</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<form id="editDepartment" method="POST" action="submit_edit_department.html">
								<div class="col-lg-12">
									<div class="form-group ">
										<label>Department Name</label>
										<input name="department_name" type="text" id="eDepartmenName" placeholder="Type here.." class="form-control" maxlength="50" required>
										<input name="department_id" type="hidden" id="eDepartmentID" required>
									</div>
									<div class="form-group ">
										<label>General Manager</label>
										<select name="gm_position" id="eGeneralManager" class="form-control" required>
												<option value='GM Operation'>GM Operation</option>
												<option value='GM People&Inventory Management'>GM People&Inventory Management</option>
												<option value='GM Admin'>GM Admin</option>
												<option value='AGM Operation TS&SK'>AGM Operation TS&SK</option>
												<option value='AGM Operation ST'>AGM Operation ST</option>
												<option value='AGM Inventory Management'>AGM Inventory Management</option>
												
                                        </select>
									</div>
									<div class="form-group ">
										<label>GM Name</label>
										<input name="gm_name" type="text" id="eGeneralManagerName" placeholder="Type here.." class="form-control" maxlength="50" required>
										
									</div>
									<div class="form-group ">
										<label>GM Email</label>
										<input name="gm_email" type="text" id="eGeneralManagerEmail" placeholder="Type here.." class="form-control" maxlength="50" required>
										
									</div>
									<div class="form-group ">
										<label>Department Status</label>
										<select name="department_status" id="eDepartmentStatus" class="form-control" required>
                                            <option value="0">Disable</option>
                                            <option value="1">Enable</option>
                                        </select>
									</div>
									
									
									<button type="submit" name="submit" value="edit" class="btn btn-md btn-primary">Edit Department</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

			<script>
		function doconfirm()
		{
			job=confirm("Are you sure to delete?");
			if (job!=true) 
			{
				return false;
			}
		}
	</script>
	