    <?php
                $et = ''; #Event Type
                $ps = ''; #Provider Snack
                $pl = ''; #Provider Lunch
                $sl = ''; #Snack List
                $ll = ''; #Lunch List   

                #Set list Event type
                foreach ($event_type as $t) {
                    $et .= "
                            <option value=\"{$t['event_type_id']}\">{$t['event_type_name']}</option>";
                }          
                #Set List Provider Snack
                foreach ($vendor_snack as $k) {
                    $ps .= "
                            <option value=\"{$k['vendor_id']}\">{$k['vendor_name']}</option>";
                }
                #Set List Provider Lunch
                foreach ($vendor_lunch as $k) {
                    $pl .= "
                            <option value=\"{$k['vendor_id']}\">{$k['vendor_name']}</option>";
                }
                #Set Snack List
                foreach ($meal_snack as $w) {
                    $sl .= "
                            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
                }
                #Set Lunch List
                foreach ($meal_lunch as $w) {
                    $ll .= "
                            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
                }
                
            ?>
    <div class="row">
        <div class="col-lg-3">
            <a href="#"><strong><i class="glyphicon glyphicon-folder-open"></i> Admin Menu</strong></a>
            <hr>

            <?=$menu?>

        </div>
        <div class="col-lg-9">
            <h2 class="sub-header"><?=$page_name?></h2>
            <ol class="breadcrumb">
                <li><a href="admin.html">Admin Page</a></li>
                <li class="active"><a href="#"><?=$page_name?></a></li>
            </ol>
            
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#futureActivity"><b>Future Activity</b></a></li>
                <li><a data-toggle="tab" href="#pastActivity"><b>Past Activity</b></a></li>
                <li><a data-toggle="tab" href="#exportData"><b>Report</b></a></li>
            </ul>

            <div class="tab-content">
                <div id="futureActivity" class="tab-pane fade in active">
                    <div  style="margin-top: 10px;">
                        <table class="table table-striped table-bordered" id="listFuture">
                            <thead>
                                <tr>
                                <th data-orderable="false">Activity Name</th>
                                <th data-orderable="false">User</th>
                                <th data-orderable="false">Room Name</th>
                                <th data-orderable="false">PAX</th>
                                <!-- <th data-orderable="false">Date</th> -->
                                <th data-orderable="false">Start</th>
                                <th data-orderable="false">End</th>
                                <th data-orderable="false">Status</th>
                                <th data-orderable="false">Action</th>
                                <th data-orderable="false"></th>
                            </tr>
                            </thead>
                            <tbody>

                        </tbody>
                        </table>
                        
                    </div>
                </div>

                <div id="pastActivity" class="tab-pane fade">
                    <div  style="margin-top: 10px;">
                        <table class="table table-striped table-bordered" id="listPast">
                            <thead>
                                <tr>
                                    <th data-orderable="false">Activity Name</th>
                                    <th data-orderable="false">User</th>
                                    <th data-orderable="false">Room Name</th>
                                    <th data-orderable="false">PAX</th>
                                    <!-- <th data-orderable="false">Date</th> -->
                                    <th data-orderable="false">Start</th>
                                    <th data-orderable="false">End</th>
                                    <th data-orderable="false">Status</th>
                                    <th data-orderable="false">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                
                    </div>
                </div>
                <div id="exportData" class="tab-pane fade">
                    
                    <div style="margin-top: 10px;">
                        <form id="exportCSV" method="POST" action="export.html">
                            <div class="col-md-12">
                                <div class="row">
                                   <div class="col-sm-3 form-group">
                                        <label>Activity Name</label>
                                        <input id="ticketPage" name="page" type="hidden" value="1">
                                        <input id="ticketTitle" name="title" type="text" placeholder="Type the name.." class="form-control" maxlength="25">
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label>User</label>
                                        <select name="store_name"  id="storeInput" class="form-control">
                                            <option></option>
                                            <?php
                                    foreach($users as $u)
                                                {
                                        echo "
                                            <option value=\"{$u['user_id']}\">{$u['display_name']}</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 form-group">
                                        <label>Activity Date From</label>
                                        <input type="text" name="date_first" class="form-control" id="datepickerA" autocomplete="off" maxlength="10">
                                        <!-- <input name="date_first" type="date" class="form-control" maxlength="10"> -->
                                    </div>                            
                                    <div class="col-sm-3 form-group">
                                        <label>to Date</label>
                                        <input type="text" name="date_last" class="form-control" id="datepickerB" autocomplete="off" maxlength="10">
                                        <!-- <input name="date_last" type="date" class="form-control" maxlength="10"> -->
                                    </div>
                                </div>                
                                <button class="btn btn-sm" id="exportCSV">Export</button>
                            </div>
                        </form>
                      <!--  <button class="btn btn-lg btn-link d-none" id="exportCSV"><a href="export.html">Export</a></button> -->               
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    
    <div id="editActivityModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                    
                         <h4 class="modal-title" id="myModalLabel2">Edit Activity</h4>

                    </div>
                    <div class="modal-body">
                        <form id="editEvent">
                            <div class="row" id="bookForm">
                                <div class="col-lg-12">

                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>User Name</label>
                                            <input name="user_name" type="text" class="form-control" id="iUsName" readonly>
                                        </div>
                                        <div class="col-sm-6 form-group ">
                                            <label>Choose Room</label>
                                            <select name="room" class="form-control" id="roomList" required>
                                                <option>-</option>
                                                <?php
                                                    foreach($room as $r){
                                                        echo "
                                                            <option value=\"{$r['room_id']}\">{$r['room_name']}</option>";
                                                    }
                                                    #var_dump($room);
                                                ?>
                                            </select>

                                            <input type="hidden" name="event_id" id="evid">
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-8 form-group">
                                            <label>From</label>
                                            <!-- <input type="text" name="start_date" class="form-control datepicker_from_edit" id="timeFrom" style="position: relative; z-index: 100000;" autocomplete="off" required> -->
                                            <input name="start_date" id="timeFrom" type="date" class="form-control timepicker" style="position: relative; z-index: 100000;" required>
                                        </div>
                                            <div class="col-sm-2 form-group" style="padding: 0 3px 0 6px;">
                                            <label>&nbsp;</label>
                                            <select name="start_hour" id="start_hour" class="form-control" style="padding: 0px;" required>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                            </select>
                                            </div>
                                            <div class="col-sm-2 form-group" style="padding-left: 3px; margin: 0;">
                                                <label>&nbsp;</label>
                                                    <select name="start_minute" id="start_minute" class="form-control" style="padding: 0px;" required>
                                                        <option value="00">00</option>
                                                        <option value="30">30</option>
                                                    </select>
                                            </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8 form-group">
                                            <label>Until</label>
                                            <!-- <input type="text" name="end_date" class="form-control datepicker_until_edit" id="timeUntil" style="position: relative; z-index: 100000;" autocomplete="off" required> -->
                                            <input name="end_date" id="timeUntil" type="date" class="form-control timepicker" style="position: relative; z-index: 99999;" required>
                                        </div>
                                        <div class="col-sm-2 form-group" style="padding: 0 3px 0 6px;">
                                            <label>&nbsp;</label>
                                            <select name="end_hour" id="end_hour" class="form-control" style="padding: 0px;" required>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                            </select>
                                            </div>
                                            <div class="col-sm-2 form-group" style="padding-left: 3px; margin: 0;">
                                                <label>&nbsp;</label>
                                                    <select name="end_minute" id="end_minute" class="form-control" style="padding: 0px;" required>
                                                        <option value="00">00</option>
                                                        <option value="30">30</option>
                                                    </select>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 form-group ">
                                            <label>Activity Type</label>
                                            <select name="event_type" class="form-control" id="mEventType1" required>
                                            <option>-</option><?=$et?>
                                        </select>
                                        </div>
                                        <div class="col-sm-4 form-group ">
                                            <label>Activity Name</label>
                                                <input name="event_name" id="mEventName" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
                                            </div>
                                        <div class="col-sm-4 form-group">
                                            <label>PAX</label>
                                            <div class="input-group">
                                                <input name="room_pax" id="pax" type="number" placeholder="Person.." class="form-control" max="9999"  required aria-describedby="person-addon">
                                                <span class="input-group-addon" id="personAddon"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                         <div class="col-sm-4 form-group ">
                                            <label>Snack</label>
                                        <select name="vendor_snack"class="form-control" id="iVendorSnack1">
                                            <option>-</option><?=$ps?>
                                             
                                        </select>
                                        </div>
                                        <div class="col-sm-4 form-group ">
                                            <label>&nbsp</label>
                                            <select name="meal_snack" class="form-control" id="iSnack11">
                                                <option>-</option><?=$sl?>
                                                
                                            </select>
                                        </div>
                                        <div class='col-sm-4 form-group1'>
                                            <label>Total</label>
                                            <input id="iTotalSnack1" type="text" class="form-control" readonly>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class='col-sm-4 form-group1'>
                                            <label>Lunch</label>
                                                <select class="form-control" name="vendor_lunch" id="iVendorLunch11">
                                                <option>-</option><?=$pl?>
                                            </select>
                                        </div>
                                        <div class='col-sm-4 form-group1'>
                                            <label>&nbsp</label>
                                            <select name="meal_lunch" class="form-control" id="iLunch1">
                                                <option>-</option><?=$ll?>
                                            </select>
                                        </div>
                                        <div class='col-sm-4 form-group1'>
                                            <label>Total</label>
                                            <input id="iTotalLunch1" type="text" class="form-control" readonly>
                                        </div>
                                    </div>
                                    
                                    <div class="row form-inline" id="optionList">
                                        <?php
                                            if(count($equipment) > 0){
                                                echo '
                                        <div class="col-lg-4">
                                            <label><center>Equipment</center></label>
                                        ';
                                                foreach($equipment as $data){
                                                    echo "
                                            <div class=\"form-inline\">
                                                <div class='form-group checkbox'>
                                                    <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                                </div>
                                            </div>";
                                                }
                                                echo "
                                        </div>";
                                            }
                                            
                                            if(count($layout) > 0){
                                                echo '
                                        <div class="col-lg-4">
                                            <label>Layout</label>
                                        ';
                                                foreach($layout as $data){
                                                    echo "
                                            <div class=\"form-inline\">
                                                <div class='form-group checkbox'>
                                                    <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                                </div>
                                            </div>";
                                                }
                                                echo "
                                        </div>";
                                            }
                                        ?>
                                    </div>
                                    <br>
                                    <div class="row form-inline">
                                        
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button type="submit" name="submit" value="create" class="btn btn-sm btn-primary">Approve</button>
                                        </div>
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button type="button" data-dismiss="modal" class="btn btn-sm btn-warning">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    <div id="declineModal" class="modal fade" tabindex="-1" role="confirmation" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <form id="submitDecline">
                        <div class="modal-header bg-danger">
                        
                             <h4 class="modal-title">Decline</h4>

                        </div>
                        <div class="modal-body text-danger">
                            <p>Please type the reason.</p>
                            
                            <input type="hidden" name="event_id" required>
                            <textarea name="decline_reason" cols="50" rows="5" required></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Decline</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div id="cancelModal" class="modal fade" tabindex="-1" role="confirmation" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <form id="submitCancel">
                        <div class="modal-header bg-danger">
                        
                             <h4 class="modal-title">Cancel</h4>

                        </div>
                        <div class="modal-body text-danger">
                            <p><b>You can cancel the event AT LEAST 1 DAYS BEFORE D-DAY.</b></p>
                            
                            <input type="hidden" name="event_id" required>
                            <textarea name="cancel_reason" class="form-control" cols="50" rows="5" placeholder="Please type the reason.." required></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="paidModal" class="modal fade" tabindex="-1" role="confirmation" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <form id="submitPaid">
                        <div class="modal-header bg-info">
                        
                             <h4 class="modal-title">Paid</h4>

                        </div>
                        <div class="modal-body text-info">
                            <p><b>Paid Confirmation</b></p>
                            
                            <input type="hidden" name="event_id" required>
                            <textarea name="paid_reason" class="form-control" cols="50" rows="5" placeholder="Please type the reason.." required></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info">Paid Confirm</button>
                            <button class="btn btn-secondary">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="infoActivityModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel2">Activity Info</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="bookForm">
                            <div class="col-lg-12">
                                <div class="form-group ">
                                    <label>User Name :</label>
                                    <span id="iUserName"></span>
                                </div>
                                <div class="form-group ">
                                    <label>Room Name :</label>
                                    <span id="iRoomName"></span>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Book Date :</label>
                                        <div class="input-group">
                                            <span id="iBookDate"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>From :</label>
                                        <div class="input-group">
                                            <span id="iTimeFrom"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Until :</label>
                                        <div class="input-group">
                                            <span id="iTimeUntil"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Activity Type :</label>
                                        <div class="input-group">
                                            <span id="iEventType"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Activity Name :</label>
                                        <div class="input-group">
                                            <span id="iEventName"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>PAX :</label>
                                        <div class="input-group">
                                            <span id="iPax"></span> Person
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Snack :</label>
                                        <div class="input-group">
                                            <span id="iVendorSnack"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>&nbsp</label>
                                        <div class="input-group">
                                            <span id="iSnack"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Total :</label>
                                        <div class="input-group">
                                            <strong>Rp &nbsp</strong>
                                            <span id="iTotalSnack2"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label>Lunch :</label>
                                        <div class="input-group">
                                            <span id="iVendorLunch"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>&nbsp</label>
                                        <div class="input-group">
                                            <span id="iLunch"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label>Total :</label>
                                        <div class="input-group">
                                            <strong>Rp &nbsp</strong>
                                            <span id="iTotalLunch2"></span>
                                        </div>
                                    </div>
                                </div>
                               
                                
                                <div class="row" id="iOptionList">
                                <?php
                                    if(count($meal) > 0){
                                        echo '
                                    <div class="col-lg-4">
                                        <label>Meal</label>
                                    ';
                                            foreach($meal as $data){
                                                echo "
                                        <div class=\"form-inline\">
                                            <div class='form-group checkbox'>
                                                <label><input name='option[]' type='checkbox' value='{$data['option_id']}'>{$data['option_name']}</label>
                                            </div>
                                        </div>";
                                            }
                                            echo "
                                    </div>";
                                    }
                                    if(count($equipment) > 0){
                                        echo '
                                <div class="col-lg-4">
                                    <label>Equipment</label>
                                ';
                                        foreach($equipment as $data){
                                            echo "
                                    <div class=\"form-inline\">
                                        <div class='form-group checkbox'>
                                            <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                        </div>
                                    </div>";
                                        }
                                        echo "
                                </div>";
                                    }
                                    
                                    if(count($layout) > 0){
                                        echo '
                                <div class="col-lg-4">
                                    <label>Layout</label>
                                ';
                                        foreach($layout as $data){
                                            echo "
                                    <div class=\"form-inline\">
                                        <div class='form-group checkbox'>
                                            <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                        </div>
                                    </div>";
                                        }
                                        echo "
                                </div>";
                                    }
                                ?>                                    
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
