            <?php
                $date = date("Y-m-d");
                $et = ''; #Event Type
                $ps = ''; #Provider Snack
                $sl = ''; #Snack List
                $ll = ''; #Lunch List
                $rl = ''; #Room List
                foreach($room as $r){
                    $rl .=  "
                            <option value=\"{$r['room_id']}\">{$r['room_name']}</option>";
                }
                #Set list Event type
                foreach ($event_type as $t) {
                    $et .= "
                            <option value=\"{$t['event_type_id']}\">{$t['event_type_name']}</option>";
                }
                #Set List Provider
                foreach ($provider_snack as $k) {
                    $ps .= "
                            <option value=\"{$k['vendor_id']}\">{$k['vendor_name']}</option>";
                }
                #Set Snack List
                foreach ($snack as $w) {
                    $sl .= "
                            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
                }
                #Set Lunch List
                foreach ($lunch as $w) {
                    $ll .= "
                            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
                }
                
            ?>
            <div class="row">
                <div class="col-lg-4">
                    <a href="#" class="dropdown" data-toggle="collapse" data-target="#collapsingMenu"><strong><i class="glyphicon glyphicon-calendar"></i> Calendar</strong></a>
                    <div class="collapse" id="collapsingMenu">
                        <hr>
                        <ul class="nav nav-stacked" id="generatedEvent"></ul>
                    </div>
                        <hr>
                    <a href="#" class="dropdown block" data-toggle="collapse" data-target="#bookForm"><strong><i class="glyphicon glyphicon-envelope"></i> Reserve Room</strong></a>
                    <hr>
                    <div class="row nav nav-stacked collapse" id="bookForm">
                        <form id="newBook">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-5 form-group">
                                        <label>Type</label>
                                        <select name="event_type" class="form-control" required>
                                            <?=$et?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>Room</label>
                                        <select name="room" class="form-control" id="roomList" required>
                                            <?=$rl?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>&nbsp;</label>
                                            <button id="mapView" class="btn btn-md btn-success form-control mapEvent" data-id="0">View</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5 form-group">
                                        <label>Date</label>
                                        <input name="start_date" id="dateFrom" type="date" min="<?=$date?>" value="<?=$date?>" class="form-control" required>
                                    </div>
                                    <div class="col-lg-7 form-group">
                                        <label>Activity</label>
                                        <input name="event_name" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 form-group">
                                        <label>From</label>
                                        <select name="start_hour" class="form-control" required>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 form-group">
                                        <label>&nbsp;</label>
                                        <select name="start_minute" class="form-control" required>
                                            <option value="00">00</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 form-group">
                                        <label>Until</label>
                                        <select name="end_hour" class="form-control" required>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 form-group">
                                        <label>&nbsp;</label>
                                        <select name="end_minute" class="form-control" required>
                                            <option value="00">00</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>Capacity</label>
                                        <div class="input-group">
                                            <input name="room_pax" id="pax" type="number" placeholder="Pax.." class="form-control" max="9999"  required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <?php
                                    if(count($meal) > 0){
                                        echo '
                                    <div class="col-lg-3">
                                        <label>Meal</label>
                                    ';
                                            foreach($meal as $data){
                                                echo "
                                        <div class=\"form-inline\" >
                                            <div class='form-group checkbox'>
                                                <label class=\"checkbox-inline spasi\"><input name='option[]' type='checkbox' value='{$data['option_id']}'>{$data['option_name']}</label>
                                            </div>
                                        </div>
                                        ";
                                            }
                                            echo "
                                    </div>";
                                    }
                                ?>
                                <div class="row ">
                                    <div class="col-lg-4 form-group">
                                        <label>&nbsp;</label>
                                        <select name="provider_snack" class="form-control" id="" required>
                                            <option value="0">- Vendor -</option><?=$ps?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>&nbsp;</label>
                                        <select name="meal_snack" class="form-control" required>
                                            <option value="0">- Snack -</option><?=$sl?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>&nbsp;</label>
                                        <select name="provider_lunch" class="form-control" required>
                                            <option value="0">- Vendor -</option><?=$ps?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>&nbsp;</label>
                                        <select name="meal_lunch" class="form-control" required>
                                            <option value="0">- Lunch -</option><?=$ll?>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                    <?php 
                                    if(count($equipment) > 0){
                                        echo '
                                <div class="col-lg-4">
                                    <label>Equipment</label>
                                ';
                                        foreach($equipment as $data){
                                            echo "
                                    <div class=\"form-inline\">
                                        <div class='form-group checkbox'>
                                            <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                        </div>
                                    </div>";
                                        }
                                        echo "
                                </div>";
                                    }
                                    
                                    if(count($layout) > 0){
                                        echo '
                                <div class="col-lg-4">
                                    <label>Layout</label>
                                ';
                                        foreach($layout as $data){
                                            echo "
                                    <div class=\"form-inline\">
                                        <div class='form-group checkbox'>
                                            <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                        </div>
                                    </div>";
                                        }
                                        echo "
                                </div>";
                                    }

                                     ?>
                                </div>
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" name="submit" value="create" class="btn btn-md btn-primary form-control">Book Request</button>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="col-lg-12 page-header">
                        <div class="pull-right form-inline">
                            <div class="btn-group">
                                <button class="btn btn-primary" data-calendar-nav="prev">Prev</button>
                                <button class="btn btn-default" data-calendar-nav="today">Today</button>
                                <button class="btn btn-primary" data-calendar-nav="next">Next</button>
                            </div>
                            <div class="btn-group">
                                <button class="btn btn-warning" data-calendar-view="year">Year</button>
                                <button class="btn btn-warning active" data-calendar-view="month">Month</button>
                                <button class="btn btn-warning" data-calendar-view="week">Week</button>
                                <button class="btn btn-warning" data-calendar-view="day">Day</button>
                            </div>
                        </div>
                        <h3></h3>
                    </div>
                    <div class="col-lg-12">     
                        <div id="calendar" class="center-block"></div>
                    </div><!-- /.row -->
                </div>
            </div>
            <div id="mapModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content">
                            <div class="modal-header">
                                 <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-6" id="mapImage">
                                    </div>
                                    <div class="col-lg-6" id="roomImage">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="infoActivityModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content">
                            <div class="modal-header">
                                 <h4 class="modal-title" id="myModalLabel2">Activity Info</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row" id="bookForm">
                                    <div class="col-lg-12">
                                        <div class="form-group ">
                                            <label>Room Name :</label>
                                            <span id="iRoomName"></span>
                                        </div>
                                        <div class="form-group ">
                                            <label>Book Date :</label>
                                            <span id="iBookDate"></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <label>From :</label>
                                                <span id="iTimeFrom"></span>
                                            </div>
                                            
                                            <div class="col-sm-6 form-group">
                                                <label>Until :</label>
                                                <span id="iTimeUntil"></span>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label>Activity Name :</label>
                                            <span id="iEventName"></span>
                                        </div>s
                                        <div class="form-group">
                                            <label>PAX :</label>
                                            <div class="input-group">
                                                <span id="iPax"></span> Person
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="events-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3>Event</h3>
                        </div>
                        <div class="modal-body" style="height: 400px">
                        </div>
                        <div class="modal-footer">
                            <a href="#" data-dismiss="modal" class="btn">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            
            