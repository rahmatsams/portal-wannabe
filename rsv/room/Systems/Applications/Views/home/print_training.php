<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!-- TemplateBeginEditable name="doctitle" -->
<title><?=$web['title']?></title>
<style>
  .table1{
      border-width: 10%;
      font-size: 82%;
      font-family: Arial;
      /* border-right: 0px; */
  }
  .table2{
      border-width: 10%;
      font-size: 82%;
      font-family: Arial;
  }
  .table3{
      border-width: 10%;
      font-size: 57%;
      font-family: Arial;
  }
  .td1{
    border-right: 0px;
  }
  .td2
  {
    border-right: 0px;
    border-left: 0px;
  }
  .td3
  {
    border-left: 0px;
  }
  .table4
  {
    font-family: Tahoma;
    font-size: 80%;
  }
  table 
  {   
      padding-bottom: 2px;
      padding-top: 2px; 
      border-collapse: collapse;
  }

</style>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
	<script type="text/css">
		
	</script>
</head>
      <body>
                        <div class="a"><br><br>
                            <center>
                              <table class="table4" width="600" border="0">
                      <tbody>
                        <tr>
                          <td width="100"><img src="Resources/images/LogoBig.jpg" alt='' width="50"/></td>
                          <center><td>
                            <center>
                              <b>
                                P T. S U S H I - T E I  &nbsp I N D O N E S I A  
                             </b><br>
                              SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                              Grand Wijaya Center Blok E No. 18-19 <br>
                              Jl. Wijaya II - Kebayoran Baru <br>
                              Jakarta Selatan 12160 <br>
                              Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                              </center>       
                            </td></center>
                        </tr>
                      </tbody>
                    </table></center>
                        <br>
                        <center>
                          <h3>
                            
                              <font face="arial" size="2">
                                  FORM PERMOHONAN TRAINING
                              </font>
                            
                          </h3>
                        </center>
                            <div class="modal-body">
                                <center><table width="600" border="0" class="table1">
                                  <tbody>
                                    <tr>
                                      <td height="25">
                                        <b>
                                            A. Data Diri Pemohon 
                                        </b>
                                      </td>
                                    </tr>
                                  </tbody>
                            </table>
                          </center>
                                
                                <center>
                                  <table width="600" border="1" class="table1">
                                  <tbody>
                                    <tr>
                                      <td width="150" class="td1">&nbsp Nama</td>
                                      <td width="450" class="td2">:&nbsp &nbsp <?=$data['display_name']?></td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Jabatan</td>
                                      <td class="td2">:</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </center>
                            
                                <center>
                                  <table width="600" border="0" class="table1">
                                  <tbody><br>
                                    <tr>
                                      <td height="25"><b>B. Informasi Pelaksaan Training</b></td>
                                    </tr>
                                  </tbody>
                                  </table>
                                </center>

                            <center>
                                <table width="600" border="1" class="table1">
                                  <tbody>
                                    <tr>
                                      <td width="150" class="td1">&nbsp Nama Penyelenggara</td>
                                      <td width="450" class="td2" colspan="2">:</td>
                                    </tr>
                                    <tr>
                                      <td class="td1">
                                      &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp     
                                      a. Internal</td>
                                      <td class="td2" colspan="2">:</td>
                                    </tr>
                                    <tr>
                                      <td class="td1">
                                      &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp    
                                      b. Eksternal</td>
                                      <td class="td2" colspan="2">:</td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Judul Training</td>
                                      <td class="td2" colspan="2">:&nbsp &nbsp <?=$data['event_name']?></td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Tempat</td>
                                      <td class="td2" colspan="2">:&nbsp &nbsp <?=$data['room_name']?></td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Tanggal</td>
                                      <td class="td2" colspan="2">:&nbsp &nbsp 
                                        Mulai &nbsp &nbsp 
                                        <?=date("d-m-Y", strtotime($data['date_from']))?>
                                        &nbsp &nbsp &nbsp &nbsp &nbsp  
                                        Selesai  &nbsp &nbsp 
                                        <?=date("d-m-Y", strtotime($data['date_until']))?> 
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Waktu</td>
                                      <td class="td2" colspan="2">:&nbsp &nbsp 
                                        Mulai &nbsp &nbsp  <?=date("H:i", strtotime($data['time_from']))?> 
                                          &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                                          Selesai &nbsp &nbsp  <?=date("H:i", strtotime($data['time_until']))?>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Peserta</td>
                                      <td class="td2" colspan="2">:</td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Jumlah Peserta</td>
                                      <td class="td2" colspan="2">:&nbsp &nbsp <?=$data['room_pax']?> &nbsp orang</td>
                                    </tr>
                                    <tr>
                                      <td class="td1">&nbsp Biaya <br>
                                        <font size="1">&nbsp &nbsp (* Lingkari yang diperlukan)</font>
                                        <br><br></td>
                                      <td class="td2" width="100">:
                                      &nbsp  
                                      a. Snack <br>
                                      &nbsp &nbsp 
                                      b. Lunch <br>
                                      &nbsp &nbsp 
                                      c. Lain-lain
                                      </td>
                                      <td class="td3" width="350">:
                                            &nbsp <?=$data['vendor_snack']?> 
                                            <?=($data['snack'] != null ? '-' : ' ')?>
                                            <?=$data['snack']?>
                                            &nbsp <?=($data['snack'] != null ? 'Rp' : ' ')?> &nbsp 
                                            <?=($data['snack'] != null ? $data['snack_price']*$data['room_pax'] : ' ')?>
                                            <br>:
                                            &nbsp <?=$data['vendor_lunch']?> 
                                            <?=($data['lunch'] != null ? '-' : ' ')?>
                                            <?=$data['lunch']?>
                                            &nbsp <?=($data['lunch'] != null ? 'Rp' : ' ')?> &nbsp 
                                            <?=($data['lunch'] != null ? $data['lunch_price']*$data['room_pax'] : ' ')?>
                                            <br>:
                                            <br>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                            </center>
                                
                          <center>
                            <table class="table1" width="600" border="0">
                                  <tbody><br>
                                    <tr>
                                      <td height="25"><b>C. Proses Persetujuan<b></td>
                                    </tr>
                                  </tbody>
                            </table>
                          </center>
                                
                                <center>
                                  <table width="600" border="1" class="table2">
                                  <tbody>
                                    <tr>
                                      <td width="210">&nbsp;</td>
                                      <td width="130"><center><b>Nama</b></center></td>
                                      <td width="130"><center><b>Jabatan</b></center></td>
                                      <td width="130"><center><b>Tanggal</b></center></td>
                                    </tr>
                                    <tr>
                                      <td height="35">Diajukan oleh :<br>
                                        <b>Pemohon</b>
                                        </td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td height="35">Disetujui oleh :<br>
                                        <b>Head Dept/BOM/BOD</b>
                                        </td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td height="35">Diproses oleh :<br>
                                        <b>Training Dept</b>
                                        </td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table></center>
                                <br><br><br><br><br><br><br>
                                <center><table width="600" border="0" class="table3">
                                    <tbody>
                                        <tr>
                                            <td width="100">No Dokumen</td>
                                            <td width="10">:</td>
                                            <td>FRM/TRG/01/01</td>
                                        </tr>
                                        <tr>
                                            <td>No Revisi</td>
                                            <td width="10">:</td>
                                            <td>3</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Efektif</td>
                                            <td width="10">:</td>
                                            <td>14 November 2016</td>
                                        </tr>
                                    </tbody>
                                </table>
                 </center>
              </div>
          </div>
    </body>	
</html>
