<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!-- TemplateBeginEditable name="doctitle" -->
<title><?=$web['title']?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="Resources/assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="Resources/assets/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
<style>
  .table1{
    font-size: 89%;
    font-family: Arial;
    border: 0;
  }
  .border-bottom-table{
      border-bottom: 1px solid #000;
  }
  .border-top-table{
      border-top: 1px solid #000;
  }
  @media print {
  .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
    float: left;
  }
  .col-sm-12 {
    width: 100%;
  }
  .col-sm-11 {
    width: 91.66666667%;
  }
  .col-sm-10 {
    width: 83.33333333%;
  }
  .col-sm-9 {
    width: 75%;
  }
  .col-sm-8 {
    width: 66.66666667%;
  }
  .col-sm-7 {
    width: 58.33333333%;
  }
  .col-sm-6 {
    width: 50%;
  }
  .col-sm-5 {
    width: 41.66666667%;
  }
  .col-sm-4 {
    width: 33.33333333%;
  }
  .col-sm-3 {
    width: 25%;
  }
  .col-sm-2 {
    width: 16.66666667%;
  }
  .col-sm-1 {
    width: 8.33333333%;
  }
  .col-sm-pull-12 {
    right: 100%;
  }
  .col-sm-pull-11 {
    right: 91.66666667%;
  }
  .col-sm-pull-10 {
    right: 83.33333333%;
  }
  .col-sm-pull-9 {
    right: 75%;
  }
  .col-sm-pull-8 {
    right: 66.66666667%;
  }
  .col-sm-pull-7 {
    right: 58.33333333%;
  }
  .col-sm-pull-6 {
    right: 50%;
  }
  .col-sm-pull-5 {
    right: 41.66666667%;
  }
  .col-sm-pull-4 {
    right: 33.33333333%;
  }
  .col-sm-pull-3 {
    right: 25%;
  }
  .col-sm-pull-2 {
    right: 16.66666667%;
  }
  .col-sm-pull-1 {
    right: 8.33333333%;
  }
  .col-sm-pull-0 {
    right: auto;
  }
  .col-sm-push-12 {
    left: 100%;
  }
  .col-sm-push-11 {
    left: 91.66666667%;
  }
  .col-sm-push-10 {
    left: 83.33333333%;
  }
  .col-sm-push-9 {
    left: 75%;
  }
  .col-sm-push-8 {
    left: 66.66666667%;
  }
  .col-sm-push-7 {
    left: 58.33333333%;
  }
  .col-sm-push-6 {
    left: 50%;
  }
  .col-sm-push-5 {
    left: 41.66666667%;
  }
  .col-sm-push-4 {
    left: 33.33333333%;
  }
  .col-sm-push-3 {
    left: 25%;
  }
  .col-sm-push-2 {
    left: 16.66666667%;
  }
  .col-sm-push-1 {
    left: 8.33333333%;
  }
  .col-sm-push-0 {
    left: auto;
  }
  .col-sm-offset-12 {
    margin-left: 100%;
  }
  .col-sm-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-sm-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-sm-offset-9 {
    margin-left: 75%;
  }
  .col-sm-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-sm-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-sm-offset-6 {
    margin-left: 50%;
  }
  .col-sm-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-sm-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-sm-offset-3 {
    margin-left: 25%;
  }
  .col-sm-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-sm-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-sm-offset-0 {
    margin-left: 0%;
  }
  .visible-xs {
    display: none !important;
  }
  .hidden-xs {
    display: block !important;
  }
  table.hidden-xs {
    display: table;
  }
  tr.hidden-xs {
    display: table-row !important;
  }
  th.hidden-xs,
  td.hidden-xs {
    display: table-cell !important;
  }
  .hidden-xs.hidden-print {
    display: none !important;
  }
  .hidden-sm {
    display: none !important;
  }
  .visible-sm {
    display: block !important;
  }
  table.visible-sm {
    display: table;
  }
  tr.visible-sm {
    display: table-row !important;
  }
  th.visible-sm,
  td.visible-sm {
    display: table-cell !important;
  }
}
</style>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
</head>

    <body style="max-width: 650px; margin: 40px auto;">
        <div>
            <table class="table4" border='0' width="600">
                <tbody>
                    <tr>
                        <td width="100"><img src="Resources/images/LogoBig.jpg" width="50"/></td>
                        <td style="text-align:center;">
                          <b>P T. S U S H I - T E I  &nbsp I N D O N E S I A </b><br>
                          SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
                          Grand Wijaya Center Blok E No. 18-19 <br>
                          Jl. Wijaya II - Kebayoran Baru <br>
                          Jakarta Selatan 12160 <br>
                          Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
                        </td>
                    </tr>
                </tbody>
            </table>
            <h3 style="text-align:center; font: arial; font-size: 110%; font-weight: bold;">FORMULIR PERMOHONAN MEETING</h3>
            <div style="font-size: 85%;">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">1.</div>
                        <div class="col-sm-3">Nama Pemohon</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7 border-bottom-table"><?=$data['display_name']?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">2.</div>
                        <div class="col-sm-3">Jabatan Pemohon</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7 border-bottom-table">&nbsp;</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">3.</div>
                        <div class="col-sm-3">Tanggal Meeting</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7 border-bottom-table"><?=date("d-m-Y", strtotime($data['date_from']))?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">4.</div>
                        <div class="col-sm-3">Waktu Meeting</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7 border-bottom-table"><?=date("H:i", strtotime($data['time_from']))?> &nbsp &nbsp s/d &nbsp &nbsp <?=date("H:i", strtotime($data['time_until']))?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">5.</div>
                        <div class="col-sm-3">Agenda Meeting</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7 border-bottom-table"><?=$data['event_name']?></div>
                    </div>
                </div>
                
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">6.</div>
                        <div class="col-sm-3">Tempat Meeting</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7 border-bottom-table"><?=$data['room_name']?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">7.</div>
                        <div class="col-sm-3">Jumlah Peserta Meeting</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7 border-bottom-table"><?=$data['room_pax']?> Orang</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">8.</div>
                        <div class="col-sm-3">Perkiraan Biaya</div>
                        <div class="col-sm-8">:</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-3" style="text-align: right;">a.</div>
                        <div class="col-sm-9 border-bottom-table"><?=($data['snack'] != null ? "Snack : {$data['vendor_snack']}  Rp. ".$data['snack_price']*$data['room_pax'] : '&nbsp;')?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-3" style="text-align: right;">b.</div>
                        <div class="col-sm-9 border-bottom-table"><?=($data['lunch'] != null ? "Lunch : {$data['vendor_lunch']}  -  Rp. ".$data['lunch_price']*$data['room_pax'] : '&nbsp;')?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-3" style="text-align: right;">c.</div>
                        <div class="col-sm-9 border-bottom-table">&nbsp;</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-3" style="text-align: right;">d.</div>
                        <div class="col-sm-9 border-bottom-table">&nbsp;</div>
                    </div>
                </div>
                <div class="col-lg-12" style="margin-top: 10px;">
                    <div class="row">
                        <div class="col-sm-1">9.</div>
                        <div class="col-sm-3">Sifat Meeting</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7">a. Wajib, mendesak, dan harus dilaksanakan</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-5">&nbsp;</div>
                        <div class="col-sm-7">b. Wajib dan tidak mendesak</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-5">&nbsp;</div>
                        <div class="col-sm-7">c. Sebagai pelengkap</div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">10.</div>
                        <div class="col-sm-3">Equipment</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7"><?=($data['projector'] == 1 ? 'Projector' : '-')?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-5">&nbsp;</div>
                        <div class="col-sm-7"><?=($data['flipchart'] == 1 ? 'Flipchart' : '-')?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-1">11.</div>
                        <div class="col-sm-3">Layout</div>
                        <div class="col-sm-1">:</div>
                        <div class="col-sm-7"><?=($data['round_seat'] == 1 ? 'Round Seating' : '-')?></div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-5">&nbsp;</div>
                        <div class="col-sm-7"><?=($data['column_seat'] == 1 ? 'Column Seating' : '-')?></div>
                    </div>
                </div>
                <div class="col-lg-12" style="margin-top: 30px;">
                    <div class="row">
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3 text-center">Pemohon</div>
                        <div class="col-sm-5">&nbsp;</div>
                        <div class="col-sm-3 text-center">Menyetujui Head Department :</div>
                    </div>
                </div>
                <div class="col-lg-12" style="margin-top: 70px;">
                    <div class="row">
                        <div class="col-sm-1">&nbsp;</div>
                        <div class="col-sm-3 text-center border-top-table">TTD Tanggal</div>
                        <div class="col-sm-5">&nbsp;</div>
                        <div class="col-sm-3 text-center border-top-table">TTD Tanggal</div>
                    </div>
                </div>
                <div class="col-lg-12" style="margin-top: 30px;">
                    <div class="row">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-sm-4 text-center">Mengetahui GM :</div>
                        <div class="col-sm-4">&nbsp;</div>
                    </div>
                </div>
                <div class="col-lg-12" style="margin-top: 90px;">
                    <div class="row">
                        <div class="col-sm-4">&nbsp;</div>
                        <div class="col-sm-4 text-center border-top-table">TTD Tanggal</div>
                        <div class="col-sm-4">&nbsp;</div>
                    </div>
                </div>
            </div>
            
            <table width="600" border="0">
                <tbody>
                    <tr>
                        <td width="100"><font face="arial" size="1">No Dokumen</font></td>
                        <td width="10"><font face="arial" size="1">:</font></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><font face="arial" size="1">No Revisi</font></td>
                        <td width="10"><font face="arial" size="1">:</font></td>
                        <td><font face="arial" size="1">0</font></td>
                    </tr>
                    <tr>
                        <td><font face="arial" size="1">Tanggal Efektif</font></td>
                        <td width="10"><font face="arial" size="1">:</font></td>
                        <td><font face="arial" size="1">12 Juli 2018</font></td>
                    </tr>
                </tbody>
            </table>
        <script src="Resources/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    </body>
</html>
