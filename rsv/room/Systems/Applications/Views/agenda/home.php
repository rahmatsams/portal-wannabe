            <?php
                $date = date("Y-m-d");
                $et = ''; #EVENT TYPE
                $ps = ''; #PROVIDER SNACK
                $pl = ''; #PROVIDER LUNCH
                $sl = ''; #SNACK LIST
                $ll = ''; #LUNCH LIST
                $rl = ''; #ROOM LIST
                foreach($room as $r){
                    $rl .=  "
                            <option value=\"{$r['room_id']}\">{$r['room_name']}</option>";
                }
                #SET LIST EVENT TYPE
                foreach ($event_type as $t) {
                    $et .= "
                            <option value=\"{$t['event_type_id']}\">{$t['event_type_name']}</option>";
                }
                #SET LIST PROVIDER SNACK
                foreach ($provider_snack as $k) {
                    $ps .= "
                            <option value=\"{$k['vendor_id']}\">{$k['vendor_name']}</option>";
                }
                #SET LIST PROVIDER LUNCH
                foreach ($provider_lunch as $k) {
                    $pl .= "
                            <option value=\"{$k['vendor_id']}\">{$k['vendor_name']}</option>";
                }
                #SET LIST SNACK LIST
                foreach ($snack as $w) {
                    $sl .= "
                            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
                }
                #SET LIST LUNCH LIST
                foreach ($lunch as $w) {
                    $ll .= "
                            <option value=\"{$w['meal_id']}\">{$w['meal_name']}</option>";
                }
                
            ?>

            <div class="row">
                <div class="col-lg-4">
                    <div class="nav nav-stacked" id="bookForm">
                        <form id="newBook" method="post">
                            <div class="col-lg-12">
                            
                                <div class="row">
                                    <div class="col-lg-4 form-group">
                                        <label>Type</label>
                                        <select name="event_type" class="form-control" required>
                                            <option value="">- Select -</option>
                                            <?=$et?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>Room</label>
                                        <select name="room" class="form-control" id="roomList" required>
                                            <option value="">- Select -</option>
                                            <?=$rl?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>&nbsp;</label>
                                            <button id="mapView" class="btn btn-md btn-success form-control mapEvent" data-id="0">View</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8 form-group">
                                        <label class="">From</label>
                                        <input name="start_date" id="datepicker_from" type="text" class="form-control pull-right datepicker" placeholder="dd/mm/yyyy" value="" autocomplete="off" required>
                                    </div>
                                    <div class="col-sm-2 form-group" style="padding: 0 3px 0 6px;">
                                        <label>&nbsp;</label>
                                            <select name="start_hour" id="start_hour" class="form-control" style="padding: 0px;" required>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                            </select>
                                    </div>
                                    <div class="col-sm-2 form-group" style="padding-left: 3px; margin: 0;">
                                        <label>&nbsp;</label>
                                            <select name="start_minute" id="start_minute" class="form-control" style="padding: 0px;" required>
                                                <option value="00">00</option>
                                                <option value="30">30</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8 form-group">
                                        <label class="">Until</label>
                                      <input name="end_date" type="text" id="datepicker_until" class="form-control pull-right datepicker" placeholder="dd/mm/yyyy" autocomplete="off">
                                    </div>
                                    <div class="col-sm-2 form-group" style="padding: 0 3px 0 6px;">
                                                <label>&nbsp</label>
                                                <select name="end_hour" id="end_hour" class="form-control" style="padding: 0px;" required>
                                                    <option value="09">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2 form-group" style="padding-left: 3px;">
                                                <label>&nbsp;</label>
                                                <select name="end_minute" id="end_minute" class="form-control" style="padding: 0px;" required>
                                                    <option value="00">00</option>
                                                    <option value="30">30</option>
                                                </select>
                                            </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 form-group">
                                        <label>Activity</label>
                                        <input name="event_name" type="text" placeholder="Type here.." class="form-control" maxlength="50" autocomplete="off" required>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>Capacity</label>
                                        <div class="input-group">
                                            <input name="room_pax" id="pax" type="number" placeholder="Pax.." class="form-control" max="9999" required>
                                            <span class="input-group-addon" id="personAddon"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label><input id='checkboxSnack' name='option[]' type='checkbox' value='1'>  Snack</label>
                                    </div>
                                    <div class="col-lg-5 form-group" id="vendor_snack">
                                        <select name="provider_snack" class="form-control" id="vendorSnack" required disabled>
                                            <option value="0">- Vendor -</option><?=$ps?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <select name="meal_snack" class="form-control" id="mealSnack" disabled>
                                            <option value="0">- Select -</option><?=$sl?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" id="vendor_meal">
                                    <div class="col-lg-3 form-group">
                                        <label><input id='checkboxLunch' name='option[]' type='checkbox' value='7'>  Lunch</label>
                                    </div>
                                    <div class="col-lg-5 form-group">
                                        <select name="provider_lunch" class="form-control" id="vendorLunch" disabled>
                                            <option value="0">- Vendor -</option><?=$pl?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <select name="meal_lunch" class="form-control" id="mealLunch" disabled>
                                            <option value="0">- Select -</option><?=$ll?>
                                        </select>
                                    </div>
                                   
                                </div>
                                <div class="row">
                                    <?php 
                                    if(count($equipment) > 0){
                                        echo '
                                <div class="col-lg-3">
                                    <label>Equipment</label>
                                ';
                                        foreach($equipment as $data){
                                            echo "
                                    <div class=\"form-inline\">
                                        <div class='form-group checkbox'>
                                            <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                        </div>
                                    </div>";
                                        }
                                        echo "
                                </div>";
                                    }
                                    
                                    if(count($layout) > 0){
                                        echo '
                                <div class="col-lg-5">
                                    <label>Layout</label>
                                ';
                                        foreach($layout as $data){
                                            echo "
                                    <div class=\"form-inline\">
                                        <div class='form-group checkbox'>
                                            <label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
                                        </div>
                                    </div>";
                                        }
                                        echo "
                                </div>";
                                    }

                                     ?>
                                     <div class="col-lg-4 form-group">
                                        <label>&nbsp;</label>
                                        <button type="submit" name="submit" value="create" class="btn btn-primary btn-lg btn-block">Book Now</button>
                                    </div>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="col-md-12 page-header" style="margin: 0px;">
                        <div class="pull-right form-inline">
                            <div class="btn-group">
                                <button class="btn btn-primary" data-calendar-nav="prev">Prev</button>
                                <button class="btn btn-default" data-calendar-nav="today">Today</button>
                                <button class="btn btn-primary" data-calendar-nav="next">Next</button>
                            </div>
                            <div class="btn-group">
                                <button class="btn btn-warning" data-calendar-view="year">Year</button>
                                <button class="btn btn-warning active" data-calendar-view="month">Month</button>
                                <button class="btn btn-warning" data-calendar-view="week">Week</button>
                                <button class="btn btn-warning" data-calendar-view="day">Day</button>
                            </div>
                        </div>
                        <h3></h3>
                    </div>
                    <div class="col-md-12">     
                        <div id="calendar" class="center-block"></div>
                    </div><!-- /.row -->
                    <div id="txt" align="right"></div>
                </div>
            </div>
            <div id="mapModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content" style="width: 760px">
                            <div class="modal-header">
                                 <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-4" id="mapImage" style="width: 300px">
                                    </div>
                                    <div class="col-lg-8" id="roomImage" style="width: 450px">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="infoActivityModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content">
                            <div class="modal-header">
                                 <h4 class="modal-title" id="myModalLabel2">Activity Info</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row" id="bookForm">
                                    <div class="col-lg-12">
                                        <div class="form-group ">
                                            <label>Room Name :</label>
                                            <span id="iRoomName"></span>
                                        </div>
                                        <div class="form-group ">
                                            <label>Book Date :</label>
                                            <span id="iBookDate"></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <label>From :</label>
                                                <span id="iTimeFrom"></span>
                                            </div>
                                            
                                            <div class="col-sm-6 form-group">
                                                <label>Until :</label>
                                                <span id="iTimeUntil"></span>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label>Activity Name :</label>
                                            <span id="iEventName"></span>
                                        </div>s
                                        <div class="form-group">
                                            <label>PAX :</label>
                                            <div class="input-group">
                                                <span id="iPax"></span> Person
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="events-modal">
                <div class="modal-dialog">
                    <div class="modal-content" style="height: 650px; width: 800px">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3>Event</h3>
                        </div>
                        <div class="modal-body" style="height: 500px">
                        </div>
                        <div class="modal-footer">
                            <!-- MODAL KLIK PER EVENT -->
            
                           <!--  <button id="btnPrint" type="button" class="btn btn-default">Print</button> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="events-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div id="events-modal-klik" class="modal-body" style="height: 500px">
                        </div>
                        <div class="modal-footer">
                            <!-- MODAL KLIK PER EVENT -->
                           <!--  <button id="btnPrint" type="button" class="btn btn-default">Print</button> -->
                        </div>
                    </div>
                </div>
            </div>