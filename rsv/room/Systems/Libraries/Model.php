<?php

/**

* This Class Extends PDO Class from PHP for easy, time saving declaration MySql Queries

*/

class Model extends PDO

{

    /* Make a connection */

    public $_last_result;

    protected $_count;

    public $_paging_from;

    public $_query;

    

    public function __construct($config = array())

    {

        $host_name = (isset($config['host']) ? $config['host'] : getConfig('host'));

        $db_name = (isset($config['db_name']) ? $config['db_name'] : getConfig('db_name'));

        $user_name = (isset($config['user_name']) ? $config['user_name'] : getConfig('user_name'));

        $password = (isset($config['password']) ? $config['password'] : getConfig('password'));
        
        $port = (isset($config['port']) ? ";port={$config['port']}" : '');

        try {
            
            if(isset($config['db']) && $config['db'] == 'MSSQL'){
                
                parent::__construct("sqlsrv:Server={$host_name}{$port};Database={$db_name};", $user_name, $password);
                
            }else{
                
                parent::__construct("mysql:dbname={$db_name}{$port};host={$host_name}", $user_name, $password);
                
            }
            
            
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 

        } catch (PDOException $e) {
			if(__DEV_MODE == 1){
				die ($e->getMessage());
			}else{
				return 'Cannot Connect to Database';
			}

        }

    }

    

    protected function doQuery($query_string, $query_value = null)

    {

        try {

            $query = $this->prepare($query_string);

            $this->_query = (!empty($query_value) ? $query->execute($query_value) : $query->execute());

            return $this->_query;

        } catch (PDOException $e) {
			
			if(__DEV_MODE == 1){
				
				die ("Query Error : {$query_string}". $e->getMessage());
				
			}else{
				
				return 'Unexpected query error';
				
			}
			
        }

    }



    protected function fetchAllQuery($query_string, $query_value = null)

    {

        try {

            $query = $this->prepare($query_string);

            (!empty($query_value) ? $query->execute($query_value) : $query->execute());

            $this->_last_result = $query->fetchAll(PDO::FETCH_ASSOC);

            return $this->_last_result;

        } catch (PDOException $e) {
			
            if(__DEV_MODE == 1){
				die ("Query Error : {$query_string}". $e->getMessage());
				
			}else{
				die ('Unexpected query error');
				
			}

        }

    }

    

    protected function fetchSingleQuery($query_string, $query_value = null)

    {

        try {

            $query = $this->prepare($query_string);

            (!empty($query_value) ? $query->execute($query_value) : $query->execute());

            $this->_last_result = $query->fetch(PDO::FETCH_ASSOC);

            return $this->_last_result;

        } catch (PDOException $e) {

            if(__DEV_MODE == 1){
				
				die ("Query Error : {$query_string}". $e->getMessage());
				
			}else{
				
				die ('Unexpected query error');
				
			}

        }

    }

    

    protected function insertQuery($table_name, $query_value)

    {

        try{

            $query_string = 'INSERT INTO '.$table_name.' ';
           
            $field = '(';

            $field_value = ') VALUES (';

            $number_array = count($query_value);
            $i = 1;

            foreach($query_value as $key => $value){

                if($i++ == $number_array){

                    $field .= $key;

                    $field_value .= ':'.$key;

                }else{

                    $field .= $key.',';

                    $field_value .= ':'.$key.',';

                }

            }

            $query_string .= $field.$field_value.')';

            $query = $this->prepare($query_string);

            if($query->execute($query_value)){

                return 1;

            }else{

                return 0;

            }

        } catch (PDOException $e) {

            if(__DEV_MODE == 1){
				
				die ("Query Error : {$query_string}". $e->getMessage());
				
			}else{
				
				die ('Unexpected query error');
				
			}

        }

    }

    

    protected function editQuery($table_name, $query_value, $where)

    {

        try{

            $query_string = 'UPDATE '.$table_name.' SET ';

            $where_query = ' WHERE ';

            $field = null;

            $number_array = count($query_value);

            $i = 1;

            foreach($query_value as $key => $value){

                if($i++ == $number_array){

                    $field .= $key.'=:'.$key;

                }else{

                    $field .= $key.'=:'.$key.',';

                }

            }

            $where_array = count($where);

            $d = 1;

            foreach($where as $key => $value){

                if($d++ == $where_array){

                    $where_query .= $key.'=:where_'.$key;

                }else{

                    $where_query .= $key.'=:where_'.$key.' AND ';

                }

                $query_value['where_'.$key] = $value;

            }

            $query_string .= $field.$where_query;

            $query = $this->prepare($query_string);
            
            if($query->execute($query_value)){

                return 1;

            }else{

                return 0;

            }

        }catch (PDOException $e) {

            if(__DEV_MODE == 1){
				
				die ("Query Error : {$query_string}". $e->getMessage());
				
			}else{
				
				die ('Unexpected query error');
				
			}

        }

    }

    

    protected function deleteQuery($table_name, $query_value)

    {

        try{

            $query_string = 'DELETE FROM '.$table_name;

            $where_query = ' WHERE ';

            $number_array = count($query_value);

            $d = 1;

            foreach($query_value as $key => $value) {

                if($d++ == $number_array){

                    $where_query .= $key.'=:where_'.$key;

                }else{

                    $where_query .= $key.'=:where_'.$key.' AND ';

                }

                $query_value['where_'.$key] = $value;

            }

            $query_string .= $field_value;

            $query = $this->prepare($query_string);

            if($query->execute($query_value)){

                return 1;

            }else{

                return 0;

            }

        } catch (PDOException $e) {

           if(__DEV_MODE == 1){
				
				die ("Query Error : {$query_string}". $e->getMessage());
				
			}else{
				
				die ('Unexpected query error');
				
			}

        }

    }

    

    protected function pagingQuery($query_string, $count_query, $option = array())

    {

        $page_open = (isset($option['page']) && $option['page'] > 0 ? $option['page'] : 1);

        $max_result = (isset($option['result']) && $option['result'] > 0 ? $option['result'] : getConfig('max_result'));

        $start_from = ($page_open > 1 ? $max_result*($page_open-1) : 0);

        $this->_paging_from = $start_from;

        $query_string .= (isset($option['order_by']) ? ' ORDER BY '.$option['order_by'] : '') .

                        (isset($option['order_by']) && isset($option['order']) ? ' '. $option['order'] : '') .

                        ' LIMIT '.$start_from.','.$max_result;

        try {

            (is_array($count_query) ? $this->countPagingQuery($count_query['query'],$count_query['value']) : $this->countPagingQuery($count_query));

            $query = $this->prepare($query_string);

            

            (!empty($query_value) ? $query->execute($query_value) : $query->execute());

            $this->_last_result = $query->fetchALL(PDO::FETCH_ASSOC);

            return $this->_last_result;

        } catch (PDOException $e) {

            if(__DEV_MODE == 1){
				
				die ("Query Error : {$query_string}". $e->getMessage());
				
			}else{
				
				die ('Unexpected query error');
				
			}

        }

    }

    

    public function countPagingQuery($query_string, $query_value = null)

    {

        if($this->_count > 0) {

            return $this->_count;

        } else {

            try {

                $result = (!empty($query_value) ? $this->fetchSingleQuery($query_string, $query_value) : $this->fetchSingleQuery($query_string));

                $result = $result['row_total'];

                $this->_count = $result;

            } catch (PDOException $e) {

                if(__DEV_MODE == 1){
				
					die ("Query Error : {$query_string}". $e->getMessage());
					
				}else{
					
					die ('Unexpected query error');
					
				}

            }

        }

    }

    
    public function getCountResult()
	{

        return $this->_count;

    }
	
}

?>