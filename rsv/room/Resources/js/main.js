function loginForm(me, forms)
{
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_login.html",
		dataType: "json",
        data: forms.serialize(),
        success: function(resp_data){
            if(resp_data.success == 1){
                var url = resp_data.last_url;
                if(resp_data.last_url == "http://room.sushitei.co.id/ajax_register.html"){
                    url = 'index.html';
                }
                if(resp_data.last_url == "http://room.sushitei.co.id/ajax_register.html"){
                    url = 'index.html';
                }
                
                window.location = url;
            } else if(resp_data.success == 0) {
                $("#userName").addClass("has-error");
                $("#loginLabel").html(resp_data.message);
                if(resp_data.login_attempt <= 5){
                    alert(resp_data.message);
                }else{
                    var hide_status = $( "#recaptchaDiv" ).hasClass( "hidden" );
                    if(hide_status == 1){
                        $("#recaptchaDiv").toggleClass('hidden');
                    }
                    grecaptcha.reset();
                    $("#loginForm button[name='login']").attr("disabled", "disabled");
                }
            } else {
                alert('Unknown error.');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}

function doRegister(me, form){
    
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_register.html",
		dataType: "json",
        data: form.serialize(),
        success: function(resp_data){
            if(resp_data.success == 1){
                alert('Registration Success');
                me.data('requestRunning', false);
                window.location.assign("login.html");
            } else if(resp_data.success == 0) {
                $("#userNameReg").removeClass('has-error');
                $("#emailReg").removeClass('has-error');
                if(resp_data.error.user_name != null){
                    $("#userNameReg").addClass('has-error');
                    $("#userNameReg").append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    $("#userNameReg input[name='user_name']").focus();
                    alert(resp_data.error.user_name);
                }
                if(resp_data.error.email != null){
                    $("#emailReg").addClass('has-error');
                    $("#emailReg").append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    $("#emailReg input[name='email']").focus();
                    alert(resp_data.error.user_name);
                }
            } else {
                alert('Unknown error.');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}
function doForgot(me, form){
    
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_forgot.html",
		dataType: "json",
        data: form.serialize(),
        success: function(resp_data){
            if(resp_data.success == 1){
                alert('Please Check your email.');
                me.data('requestRunning', false);
                window.location.assign("login.html");
            } else if(resp_data.success == 0) {
                if(resp_data.err_code == 1 || resp_data.err_code == 2){
                    alert(resp_data.message);
                }else{
                    alert('Wrong username input format');
                }
            } else {
                alert('Unknown error.');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}