var holidays2017 = {
	// January 1
	'01-01': "Tahun Baru 2017",
	'28-01': "Tahun Baru Imlek 256",
	'28-03': "Hari Raya Nyepi Tahun Baru Saka 1939",
	'14-04': "Wafat Isa Al Masih",
	'24-04': "Isra Miraj Nabi Muhammad SAW",
	'01-05': "Hari Buruh Internasional",
	'11-05': "Hari Raya Waisak 2561",
	'25-15': "Kenaikan Isa Al Masih",
	'01-06': "Hari Lahir Pancasila",
	'25-06': "Hari Raya Idul Fitri 1438 Hijriah",
	'26-06': "Hari Raya Idul Fitri 1438 Hijriah",
	'17-08': "Hari Kemerdekaan Indonesia",
	'14-04': "Wafat Isa Al Masih",
	'01-09': "Hari Raya Idul Adha 1438 Hijriah",
	'21-09': "Tahun Baru Islam 1439 Hijriah",
	'01-12': "Maulid Nabi Muhammad SAW",
	'25-12': "Hari Raya Natal",
};
var last_room = null;

$(document).ready(function(){
    var max_pax = 0;
	
    $("#roomList").change(function(){
        var me = $(this);
        var room = $("#roomList").val();
        $("#mapView").data('id', room);
        if(room > 0){
            getRoomData(me, room);
        }
    })
	
	$("#newBook").submit(function(event){
		event.preventDefault();
        $("#disclaimerModal").modal('show');
		
	});
    $("#bookDisclaimer").click(function(event){
		event.preventDefault();
		var me = $(this);
		var form = $("#newBook");
		bookRoom(me, form);
	});
    $(".mapEvent").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
        if(room != "0"){
            showMapModal(me, room);
        }
	});
    
    $("#submitCancel").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $('#submitCancel');
		submitCancelEvent(me, room);
	});

    $("#submitEventPerson").submit(function(event){
        event.preventDefault();
        var me = $(this);
        var room = $('#submitEventPerson');
        submitEventPerson(me, room);
    });
    
    $(document).on('click', '.manageEvent' , function(event) {
        event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		if(last_room == room){
            $('#editActivityModal').modal('show');
        }else{
            eventData(me, room, 1);
        }
    });
    $(document).on('click', '.infoEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
        if(last_room == room){
            $('#infoActivityModal').modal('show');
        }else{
            eventData(me, room, 2);
        }
	});
	$(document).on('click', '.eventPerson' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id').split("#");
		showEventPersonModal(me, room[0], room[1], room[2]);
	});

	$(document).on('click', '.declineEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		showDeclineModal(me, room);
	});
    $(document).on('click', '.cancelEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		showCancelModal(me, room);
	});

    $('#listFuture').DataTable( {
        serverSide: true,
        searching : false,
        ajax: {
            url: 'event_user_2.html',
            type: 'POST'
        },
        columns: [
            { "data": "event_name" },
            { "data": "display_name" },
            { "data": "room_name" },
            { "data": "book_date" },
            { "data": "start" },
            { "data": "end" },
            { "data": "status_name" },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-success btn-sm btn-block infoEvent" data-id="'+data+'">Info</a>';
                }
            },
            { 
                //"data": "event_id",
                "data": function (data, type, dataToSet) {
                    return data.event_id + "#" + data.start + "#" + data.end;
                },
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-info btn-sm eventPerson" data-id="'+data+'">+Peserta</a>';
                }
            }
        ]
    } );
    $('#listPast').DataTable( {
        serverSide: true,
        searching : false,
        ajax: {
            url: 'event_user_3.html',
            type: 'POST'
        },
        columns: [
            { "data": "event_name" },
            { "data": "display_name" },
            { "data": "room_name" },
            { "data": "room_capacity" },
            { "data": "book_date" },
            { "data": "start" },
            { "data": "end" },
            { "data": "status_name" },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-success btn-sm btn-block infoEvent" data-id="'+data+'">Info</a>';
                }
            },
        ]
    } );
    
});

function loadOption(){
	$.ajax({
		type: "POST",
		url: "option_data.html",
		dataType: "json",
		success: function(resp){
			if(resp.success == 1){
				$.each(resp.data, function(){
                    var html = "<div class='col-sm-6 form-group checkbox'>"+
                                        "<label><input name='option[]' type='checkbox' value='"+this.option_id+"' > "+this.option_name+"</label>"+
                                    "</div>";
                    $("#optionList").append(html);
                });
			}
		},
	});
	
}

function showMapModal(me, room)
{
	
    $("#mapModal").modal('show');
    var map;
    var place;
    $.ajax({
        url:'Resources/images/map/'+room+'.jpg',
        type:'HEAD',
        error: function()
        {
            map = 'Resources/images/map/not_found.jpg';
            $("#mapImage").html('<a href="'+map+'"><img src="'+map+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        },
        success: function()
        {
            map = 'Resources/images/map/'+room+'.jpg';
            $("#mapImage").html('<a href="'+map+'"><img src="'+map+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        }
    });
    $.ajax({
        url:'Resources/images/room/'+room+'.jpg',
        type:'HEAD',
        error: function()
        {
            place = 'Resources/images/room/not_found.jpg';
            $("#roomImage").html('<a href="'+place+'"><img src="'+place+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        },
        success: function()
        {
            place = 'Resources/images/room/'+room+'.jpg';
            $("#roomImage").html('<a href="'+place+'"><img src="'+place+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        }
    });
    
}

function getRoomData(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "room_data.html",
		dataType: "json",
		data: 'room_id='+room,
		success: function(resp){
			if(resp.success == 1){
				$("#personAddon").html('/ '+resp.data.room_capacity);
                $("#mapModal .modal-title").html(resp.data.room_name);
                max_pax = resp.data.room_capacity;
			} else if(resp.success == 0){
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function bookRoom(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_book.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
                var message = 'Book Request has been sent to Admin for moderation.';
                $("#successModal .modal-body").html(message);
                $("#successModal").modal('toggle');
			} else if(resp.success == 0)    {
				if(resp.data != null){
                    var l = resp.data;
					var message = 'Room already used by '+ l.user_name +' for '+ l.current_event + ' from ' + l.event_start + ' to ' + l.event_end;
					
					$("#failedModal .modal-body").html(message);
					$("#failedModal").modal('toggle');
				}
			}else{
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function eventData(me) //activity info d my request admininstrator
{
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	var eid;
	$.ajax({
		type: "POST",
		url: "event_data.html",
		dataType: "json",
		data: 'event_id='+me.data('id'),
		success: function(resp){
			if(resp.success == 1)    {
                eid = resp.data.event_id;
                var date = new Date(resp.data.event_date);
                $("#iUserName").html(resp.data.display_name);                
                $("#iRoomName").html(resp.data.room_name);
                $("#iEventType").html(resp.data.event_type);
                $("#iEventName").html(resp.data.event_name);
                $("#personAddon").html(resp.data.room_capacity);
                $("#iBookDate").html(date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear());
                $("#iTimeFrom").html(resp.data.event_start);
                $("#iTimeUntil").html(resp.data.event_end);
                $("#iPax").html(resp.data.room_pax);
                $("#iVendorSnack").html(resp.data.vendor_snack);
				$("#iSnack").html(resp.data.snack);
				$("#iTotalSnack2").html(resp.data.jumlah);
				$("#iVendorLunch").html(resp.data.vendor_lunch);
                $("#iLunch").html(resp.data.lunch);
				$("#iTotalLunch2").html(resp.data.total);
                $('#infoActivityModal').modal('show');
                $('#iOptionList :checkbox').prop('checked',false);
                $.each(resp.data.opt, function(i, obj) {
                    if(obj.euser_value > 0){
                        $( "#iOptionList :checkbox[value="+obj.option_id+"]" ).prop("checked",true);
                    }
                });
			} else if(resp.success == 0)    {
				alert('Could not fetch data.');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
            me.data('requestRunning', false);
		},
	});
	return false;
}


function showCancelModal(me, room)
{
    $("#submitCancel input[name='event_id']").val(room);
    $("#submitCancel textarea").html('');
    $("#cancelModal .modal-title").html('Cancel Confirmation');
    $("#cancelModal").modal('show');  
}

function submitCancelEvent(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "user_cancel.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
                var message = 'Book Successfully Cancelled.';
                $("#successModal .modal-title").html('Cancel Book');
                $("#successModal .modal-body").html(message);
                $("#cancelModal").modal('hide');
                $("#successModal").modal('toggle');
			} else if(resp.success == 0)    {
				alert(resp.error);
			}else{
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

// function showEventPersonModal(me, room, tglstart, tglend) Pilih tanggal
// {	   
//     var tgl = "";
//     /*let start = new Date(tglstart),
//         duration = 'P1D',
//         end = new Date(tglend),
//         period;
//     for (let date of Object.keys(period)) {
//         tgl += "<option value='"+date+"'>"+date+"</option>"
//     }*/
//     tglend = tglend.substring(3,5) + "-" + tglend.substring(0,2) + "-" + tglend.substring(6,10);
//     var endloop = new Date(tglend);
//     tglstart = tglstart.substring(3,5) + "-" + tglstart.substring(0,2) + "-" + tglstart.substring(6,10);
//     var d = new Date(tglstart);
//     const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
//     for (var d = new Date(tglstart); d <= endloop; d.setDate(d.getDate() + 1)) {
//         var month = d.getMonth() + 1;
//         var fullmonth = monthNames[d.getMonth()];
//         var day = d.getDate();
//         var year = d.getFullYear();
//         tgl += "<option value='"+year+"-"+month+"-"+day+"'>"+day+" "+fullmonth+" "+year+"</option>";
//     }

//     $("#submitEventPerson input[name='event_id']").val(room);
//     $("#submitEventPerson textarea").html('');
//     $("#submitEventPerson select").html(tgl);
//     $("#eventPersonModal .modal-title").html('Submit Daftar Hadir');
//     $("#eventPersonModal").modal('show');  
// }

function showEventPersonModal(me, room)
{      
    $("#submitEventPerson input[name='event_id']").val(room);
    $("#submitEventPerson textarea").html('');
    $("#eventPersonModal .modal-title").html('Submit Daftar Hadir');
    $("#eventPersonModal").modal('show');  
}

function submitEventPerson(me, room){
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    
    $.ajax({
        type: "POST",
        url: "event_person.html",
        dataType: "json",
        data: room.serialize(),
        success: function(resp){
            if(resp.success == 1)    {
                var message = 'Successfully Added..';
                $("#successModal .modal-title").html('Daftar Hadir');
                $("#successModal .modal-body").html(message);
                $("#eventPersonModal").modal('hide');
                $("#successModal").modal('toggle'); 
            } else if(resp.success == 0)    {
                alert(resp.error);
            }else{
                alert('Unknown Error');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}