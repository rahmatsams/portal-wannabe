var holidays2017 = {
	// January 1
	'01-01': "Tahun Baru 2017",
	'28-01': "Tahun Baru Imlek 2568ongzili",
	'28-03': "Hari Raya Nyepi Tahun Baru Saka 1939",
	'14-04': "Wafat Isa Al Masih",
	'24-04': "Isra Miraj Nabi Muhammad SAW",
	'01-05': "Hari Buruh Internasional",
	'11-05': "Hari Raya Waisak 2561",
	'25-15': "Kenaikan Isa Al Masih",
	'01-06': "Hari Lahir Pancasila",
	'25-06': "Hari Raya Idul Fitri 1438 Hijriah",
	'26-06': "Hari Raya Idul Fitri 1438 Hijriah",
	'17-08': "Hari Kemerdekaan Indonesia",
	'14-04': "Wafat Isa Al Masih",
	'01-09': "Hari Raya Idul Adha 1438 Hijriah",
	'21-09': "Tahun Baru Islam 1439 Hijriah",
	'01-12': "Maulid Nabi Muhammad SAW",
	'25-12': "Hari Raya Natal",
};
var last_room = null;
(function($) {

	"use strict";
	var datef = new Date().toISOString().slice(0,10);
	
	var options = {
		events_source: 'events.html',
		view: 'month',
		tmpl_path: 'Resources/tmpls/',
        modal: '#events-modal',
        modal_type: 'ajax',
		tmpl_cache: false,
		day: datef,
		first_day: 1,
		time_start: '08:00',
		time_end: '18:00',
		width: '100%',
		holidays: holidays2017,
		
		onAfterViewLoad: function(view) {
			$('.page-header h3').text(this.getTitle());
			$('.btn-group button').removeClass('active');
			$('button[data-calendar-view="' + view + '"]').addClass('active');
		},
		classes: {
			months: {
				general: 'label'
			}
		}
	};

	var calendar = $('#calendar').calendar(options);

	$('.btn-group button[data-calendar-nav]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.navigate($this.data('calendar-nav'));
		});
	});

	$('.btn-group button[data-calendar-view]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.view($this.data('calendar-view'));
		});
	});

	$('#first_day').change(function(){
		var value = $(this).val();
		value = value.length ? parseInt(value) : null;
		calendar.setOptions({first_day: value});
		calendar.view();
	});

	$('#language').change(function(){
		calendar.setLanguage($(this).val());
		calendar.view();
	});

	$('#events-in-modal').change(function(){
		var val = $(this).is(':checked') ? $(this).val() : null;
		calendar.setOptions({modal: val});
	});
	$('#format-12-hours').change(function(){
		var val = $(this).is(':checked') ? true : false;
		calendar.setOptions({format12: val});
		calendar.view();
	});
	$('#show_wbn').change(function(){
		var val = $(this).is(':checked') ? true : false;
		calendar.setOptions({display_week_numbers: val});
		calendar.view();
	});
	$('#show_wb').change(function(){
		var val = $(this).is(':checked') ? true : false;
		calendar.setOptions({weekbox: val});
		calendar.view();
	});
	$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
		//e.preventDefault();
		//e.stopPropagation();
	});
	
	function wait(ms){
	   var start = new Date().getTime();
	   var end = start;
	   while(end < start + ms) {
		 end = new Date().getTime();
	  }
	}
	$("#loadingModal").on("hide.bs.modal", function () {
		$('#loadingModal .modal-content').html('<div class="modal-header"><h4 class="modal-title" id="myModalLabel">Loading</h4></div><div class="modal-body"><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div></div>');
	});
    
}(jQuery));


$(document).ready(function(){
    var max_pax = 0;
	$("#vendorSnack").change(function(event)
	{
		event.preventDefault();
		changeValueSub();
	});

	$("#vendorLunch").change(function(event)
	{
		event.preventDefault();
		changeValueSubLunch();
	});
    
    $("#roomList").change(function(){
        var me = $(this);
        var room = $("#roomList").val();
        $("#mapView").data('id', room);
        if(room > 0){
            getRoomData(me, room);
        }
    });
    
    $("input[name='option[]']").click(function(){
    	var start = (parseInt($('#start_hour').val())*60) + parseInt($('#start_minute').val());
    	var end = (parseInt($('#end_hour').val())*60) + parseInt($('#end_minute').val());
        if($(this).val() == 1){
            if($(this).is(':checked')){
            	if ((end - start) > (2*60)) {
            		$('#vendorSnack').attr('disabled', false);
            	}else{
            		alert("Durasi acara <=2 jam tidak dapat melakukan pemesanan SNACK.");
            		$(this).attr('checked', false);
            	}
            }else{
                $('#vendorSnack').attr('disabled', true);
                $('#mealSnack').attr('disabled', true);
            }
        }else if($(this).val() == 7){
            if($(this).is(':checked')){
            	if (start < (12*60) && end > (12*60)) { //waktu mulai >= jam 12 dan waktu selesai <=jam 12 can't pesan lunch  
            		$('#vendorLunch').attr('disabled', false);
            	}else{
            		alert("Selesai acara kurang dari jam 12 atau Mulai acara jam 12 dan setelahnya tidak dapat LUNCH.");
            		$(this).attr('checked', false);
            	}
            }else{
                $('#vendorLunch').attr('disabled', true);
                $('#mealLunch').attr('disabled', true);
            }   
        }
    });
    
	$("#newBook").submit(function(event){ 
	    	event.preventDefault();
	    	var snackChecked = false;
	    	var lunchChecked = false;
	    	var snackComplete = 0;
	    	var lunchComplete = 0;
		    $("input[name='option[]']:checked").each(function(){
		    	if($(this).val() == 1){ // SNACK
		    		snackChecked = true;
		    		if($("#vendorSnack").val() == "0")
					{
			    		window.alert("Anda belum memilih Vendor Snack"); //snack 'checked', belum milih vendor snack
				    }
				    else if($("#mealSnack").val() == "0")
				    {
				    	window.alert("Anda belum memilih Menu Snack"); //snack 'checked', belum milih menu snack
				    }else{
				    	snackComplete = 1;
				    }
		    	}

		    	if($(this).val() == 7){ //LUNCH
		    		lunchChecked = true;
		    		if($("#vendorLunch").val() == "0")
				    {
				    	window.alert("Anda belum memilih Vendor Lunch"); //lunch 'checked', belum milih vendor lunch
				    }
				    else if($("#mealLunch").val() == "0")
				    {
				    	window.alert("Anda belum memilih Menu Lunch"); //lunch 'checked', belum milih menu lunch
				    }else{
				    	lunchComplete = 1;
				    }
		    	}
		    });

		    if(snackChecked == true){
		    	if(lunchChecked == true){
		    		if(snackComplete == 1 && lunchComplete == 1){
		    			$("#disclaimerModal").modal('show');		
		    		}
		    	}else{
		    		if (snackComplete == 1) {
		    			$("#disclaimerModal").modal('show');	
		    		}
		    	}
		    }else if (lunchChecked == true){
		    	if (lunchComplete == 1) {
		    		$("#disclaimerModal").modal('show');	
		    	}
		    }else{
		    	$("#disclaimerModal").modal('show');
		    }
	});
	
    $("#bookDisclaimer").click(function(event){
		event.preventDefault();
		var me = $(this);
		var form = $("#newBook");
		bookRoom(me, form);
	});
    $(".mapEvent").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
        if(room != "0"){
            showMapModal(me, room);
        }
	}); 
});

function loadOption(){
	$.ajax({
		type: "POST",
		url: "option_data.html",
		dataType: "json",
		success: function(resp){
			if(resp.success == 1){
				$.each(resp.data, function(){
                    var html = "<div class='col-sm-6 form-group checkbox'>"+
                                        "<label><input name='option[]' type='checkbox' value='"+this.option_id+"' > "+this.option_name+"</label>"+
                                    "</div>";
                    $("#optionList").append(html);
                });
			}
		},
	});
	
}

function showMapModal(me, room)
{
	
    $("#mapModal").modal('show');
    var map;
    var place;
    $.ajax({
        url:'Resources/images/map/'+room+'.jpg',
        type:'HEAD',
        error: function()
        {
            map = 'Resources/images/map/not_found.jpg';
            $("#mapImage").html('<a href="'+map+'"><img src="'+map+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        },
        success: function()
        {
            map = 'Resources/images/map/'+room+'.jpg';
            $("#mapImage").html('<a href="'+map+'"><img src="'+map+'" alt="" style="width: 450px" class="map-images img-responsive img-thumbnail"></a>');
        }
    });
    $.ajax({
        url:'Resources/images/room/'+room+'.jpg',
        type:'HEAD',
        error: function()
        {
            place = 'Resources/images/room/not_found.jpg';
            $("#roomImage").html('<a href="'+place+'"><img src="'+place+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        },
        success: function()
        {
            place = 'Resources/images/room/'+room+'.jpg';
            $("#roomImage").html('<a href="'+place+'"><img src="'+place+'" alt="" style="width: 450px" class="map-images img-responsive img-thumbnail"></a>');
        }
    });
    
}

function getRoomData(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "room_data.html",
		dataType: "json",
		data: 'room_id='+room,
		success: function(resp){
			if(resp.success == 1){
				$("#personAddon").html('/ '+resp.data.room_capacity);
                $("#mapModal .modal-title").html(resp.data.room_name);
                max_pax = resp.data.room_capacity;
			} else if(resp.success == 0){
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function bookRoom(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	$('#loadingModal').modal('show');
	$.ajax({
		type: "POST",
		url: "submit_book.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			console.log(resp);
			if(resp.success == 1)    
			{
				console.log("Bisa");
                var message = 'Book Request has been sent to Admin for moderation.'; 
                $("#successModal .modal-body").html(message);
                $("#successModal").modal('toggle');
			} else if(resp.success == 0){ 
			console.log(resp);

				if(resp.data != null){
                    var l = resp.data;
					var message = 'Room already used by '+ l.user_name +' for '+ l.current_event + ' from ' + l.event_start + ' to ' + l.event_end;
					$("#failedModal .modal-body").html(message);
					$("#failedModal").modal('toggle');
				}else{
                    alert('Error');
                }
			}
		},
		complete: function() {
            $("#loadingModal").modal('hide');
			me.data('requestRunning', false);
		},
	});
	return false;
}
function eventData(me)
{
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	var eid;
	$.ajax({
		type: "POST",
		url: "event_data.html",
		dataType: "json",
		data: 'event_id='+me.data('id'),
		success: function(resp){
			if(resp.success == 1)    {
                eid = resp.data.event_id;
                var date = new Date(resp.data.event_date);
                $("#iRoomName").html(resp.data.room_name);
                $("#iEventName").html(resp.data.event_name);
                $("#personAddon").html(resp.data.room_capacity);
                $("#iBookDate").html(date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear());
                $("#iTimeFrom").html(resp.data.event_start);
                $("#iTimeUntil").html(resp.data.event_end);
                $("#iPax").html(resp.data.room_pax);
                $('#infoActivityModal').modal('show');
                alert('Success');
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
            last_room = room;
            me.data('requestRunning', false);
		},
	});
	return false;
}

function changeValueSub()
{
	$vendor_id = $("#vendorSnack").val();
    $.ajax({
      type: "POST",
      url: "meal_snack_Vendor.html",
      data: "vendor_id="+$vendor_id,
      success: function(res) {
        $('#mealSnack').html(res);
        $('#mealSnack').removeAttr('disabled');
      }
    });
}

function changeValueSubLunch()
{
	$vendor_id = $("#vendorLunch").val();
	$.ajax({
		type: "POST",
		url: "meal_lunch_Vendor.html",
		data: "vendor_id="+$vendor_id,
		success: function(res){
			$('#mealLunch').html(res);
            $('#mealLunch').removeAttr('disabled');
		}
	});
}
                    