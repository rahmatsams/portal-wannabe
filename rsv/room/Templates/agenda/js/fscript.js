$(document).ready(function(){
	$("#newLunch").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#newLunch");
		submitNewLunch(me, room);
	});
	$("#editLunch").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#editLunch");
		submitEditLunch(me, room);
	});
	$(".editLunch").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		lunchData(me, room);
	});
	$(".deleteLunch").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		submitDeleteLunch(me, room);
	});
});
function submitNewLunch(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_new_lunch.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				$("#failedModal").toggle('modal');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function submitEditLunch(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_edit_lunch.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function lunchData(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "lunch_data.html",
		dataType: "json",
		data: 'lunch_id='+room,
		success: function(resp){
			if(resp.success == 1)    {
				$("#eLunchName").val(resp.data.vendor_name);
				$("#eLunchAddress").val(resp.data.vendor_address);
				$("#eLunchPhone").val(resp.data.vendor_phone);
				$("#eLunchID").val(resp.data.vendor_id);
				$('#editLunchModal').modal('show');
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
