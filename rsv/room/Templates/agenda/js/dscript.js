$(document).ready(function(){
	$("#newOption").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var options = $("#newOption");
		submitNewOption(me, options);
	});
	$("#editOption").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#editOption");
		submitEditOption(me, room);
	});
	$(".editOption").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		optionData(me, room);
	});
	$(".deleteOption").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		submitDeleteOption(me, room);
	});
});
function submitNewOption(me, options){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_new_option.html",
		dataType: "json",
		data: options.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				$("#failedModal").toggle('modal');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function submitEditOption(me, options){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_edit_option.html",
		dataType: "json",
		data: options.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function optionData(me, options){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "admin_option_data.html",
		dataType: "json",
		data: 'option_id='+options,
		success: function(resp){
			if(resp.success == 1)    {
				$("#eOptionName").val(resp.data.option_name);
                $("#eOptionStatus").val(resp.data.option_status);
                $("#eOptionCategory").val(resp.data.option_category);
				$("#eOptionID").val(resp.data.option_id);
				$('#editOptionModal').modal('show');
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
