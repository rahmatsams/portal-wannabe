
$(document).ready(function(){
	$("#newMeal").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var meal = $("#newMeal");
		submitNewMeal(me, meal);
	});
	$("#editMeal").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var meal = $("#editMeal");
		submitEditMeal(me, meal);
	});
	$(".edit-meal").click(function(event){
		event.preventDefault();
		var me = $(this);
		var meal = $(this).data('id');
		mealData(me, meal);
	});
	$(".deleteMeal").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		submitDeleteSnack(me, room);
	});
});


function submitNewMeal(me, meal){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "ajax_new_meal.html",
		dataType: "json",
		data: meal.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				$("#failedModal").toggle('modal');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}


function submitEditMeal(me, meal){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "ajax_edit_meal.html",
		dataType: "json",
		data: meal.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

function mealData(me, meal){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "admin_meal_data.html",
		dataType: "json",
		data: 'meal_id='+meal,
		success: function(resp){
			if(resp.success == 1)    {
				$("#eMealName").val(resp.data.meal_name);
				$("#eMealVendor").val(resp.data.meal_vendor);
				$("#eMealCategory").val(resp.data.meal_category);
				$("#eMealID").val(resp.data.meal_id);
                $("#eMealStatus").val(resp.data.meal_status);
                $("#eMealPrice").val(resp.data.meal_price);
				$('#editMealModal').modal('show');
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
