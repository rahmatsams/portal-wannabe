var last_room = null;
$(document).ready(function(){
    var room_info = null;
	$("#newCatering").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#newCatering");
		submitNewCatering(me, room);
	});
	$("#editEvent").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#editEvent");
		submitEditEvent(me, room);
	});
    $(document).on('click', '.manageEvent' , function(event) {
        event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		if(last_room == room){
            $('#editActivityModal').modal('show');
        }else{
            eventData(me, room, 1);
        }
    });
    $(document).on('click', '.infoEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
        if(last_room == room){
            $('#infoActivityModal').modal('show');
        }else{
            eventData(me, room, 2);
        }
	});

	$(document).on('click', '.declineEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		showDeclineModal(me, room);
	});
    $(document).on('click', '.cancelEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		showCancelModal(me, room);
	});
    $("#submitDecline").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $('#submitDecline');
		submitDeclineEvent(me, room);
	});
    $("#submitCancel").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $('#submitCancel');
		submitCancelEvent(me, room);
	});
    //button cancelsubmit pada modal editevent(evendata)
    $("#cancelSubmit").click(function(event){
		event.preventDefault();
		$('#editActivityModal').modal('hide');
	});
    
    $('#listFuture').DataTable( {
        serverSide: true,
        searching : false,
        ajax: {
            url: 'event_list_2.html',
            type: 'POST'
        },
        columns: [
            { "data": "event_name" },
            { "data": "display_name" },
            { "data": "room_name" },
            { "data": "room_capacity" },
            /*{ 
                "data": "book_date",
                "filtering" : true,
                "sort" : true,
                "render": function ( data, type, row, meta ) {
                    return data;
                }
            },*/
            { "data": "start" },
            { "data": "end" },
            { "data": "status_name" },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-danger btn-sm cancelEvent" data-id="'+data+'">Cancel</a>';
                }
            },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-success btn-sm manageEvent" data-id="'+data+'">Edit</a>';
                }
            }
        ]
    } );
    $('#listPast').DataTable( {
        serverSide: true,
        searching : false,
        ajax: {
            url: 'event_list_3.html',
            type: 'POST'
        },
        columns: [
            { "data": "event_name" },
            { "data": "display_name" },
            { "data": "room_name" },
            { "data": "room_capacity" },
            //{ "data": "book_date" },
            { "data": "start" },
            { "data": "end" },
            { "data": "status_name" },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-success btn-sm btn-block infoEvent" data-id="'+data+'">Info</a>';
                }
            },
        ]
    } );
});

function showDeclineModal(me, room)
{   
    $("#submitDecline input[name='event_id']").val(room);
    $("#submitDecline textarea").html('');
    $("#declineModal .modal-title").html('Decline Confirmation');
    $("#declineModal").modal('show');
   
}

function showCancelModal(me, room)
{
	   
    $("#submitCancel input[name='event_id']").val(room);
    $("#submitCancel textarea").html('');
    $("#cancelModal .modal-title").html('Cancel Confirmation');
    $("#cancelModal").modal('show');
   
}

function submitDeclineEvent(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "event_decline.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
                var message = 'Book declined successfully.';
                $("#successModal .modal-title").html('Decline Book');
                $("#successModal .modal-body").html(message);
                $("#declineModal").modal('hide');
                $("#successModal").modal('toggle');
			} else if(resp.success == 0)    {
				alert('Decline Failed');
			}else{
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

function submitCancelEvent(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "event_cancel.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
                var message = 'Book Successfully Cancelled.';
                $("#successModal .modal-title").html('Cancel Book');
                $("#successModal .modal-body").html(message);
                $("#cancelModal").modal('hide');
                $("#successModal").modal('toggle');
			} else if(resp.success == 0)    {
				alert('Cancel Failed');
			}else{
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

function submitEditEvent(me, room) //modal edit Event
{
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_edit_event.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Approved');
				//location.reload();
			} else if(resp.success == 0 && resp.message == 1)    {
				if(resp.data != null){
					var message = "" ;
					
                    message = 'Room already used by '+ resp.data.display_name +' for '+ resp.data.event_name + ' from ' + resp.data.time_from + ' to ' + resp.data.time_until;
					
					$("#failedModal .modal-body").html(message);
					$("#failedModal").modal('toggle');
				}
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

function eventData(me, room, type)
{
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	var eid;
	$.ajax({
		type: "POST",
		url: "event_data.html",
		dataType: "json",
		data: 'event_id='+room, 
		success: function(resp){
			if(resp.success == 1)    { //info edit future list data
                if(type == 1){
                    eid = resp.data.event_id;
                    $("#iUsName").val(resp.data.display_name);
                    $("#roomList").val(resp.data.room_id);
                    $("#dateFrom").val(resp.data.provider_name);
                    $("#mEventType1").val(resp.data.event_type_id);
                    $("#mEventName").val(resp.data.event_name);
                    $("#personAddon").html(resp.data.room_capacity);
                    $("#dateFrom").val(resp.data.event_date);
                    $("#timeFrom").val(resp.data.event_start);
                    $("#timeUntil").val(resp.data.event_end);
                    $("#start_hour").val(resp.data.time_from);
                    $("#start_minute").val(resp.data.time_from_2);
                    $("#end_hour").val(resp.data.time_until);
                    $("#end_minute").val(resp.data.time_until_2);
                    $("#eventType").val(resp.data.event_type_name);
                    $("#evid").val(resp.data.event_id);
                    $("#pax").val(resp.data.room_pax);
                    //snack
                    $("#iVendorSnack1").val(resp.data.vendor_id_snack);
					$("#iSnack11").val(resp.data.meal_id_snack);
					$("#iTotalSnack1").val(resp.data.jumlah);
                    //lunch
                    $("#iVendorLunch11").val(resp.data.vendor_id_lunch);
                    $("#iLunch1").val(resp.data.meal_id_lunch);
					$("#iTotalLunch1").val(resp.data.total);

                    $('#editActivityModal').modal('show');
                    $.each(resp.data.opt, function(i, obj) {
                        if(obj.euser_value > 0){
                            $( "#optionList :checkbox[value="+obj.option_id+"]" ).prop("checked",true);
                            $( "#optionList1 :checkbox[value="+obj.option_id+"]" ).prop("checked",true);
                        }
                    });
                    
                }else{
                    eid = resp.data.event_id; //info past list data
                    var date = new Date(resp.data.event_date);
                    
                    $("#iUserName").html(resp.data.display_name);
                    $("#iRoomName").html(resp.data.room_name);
                    $("#iEventType").html(resp.data.event_type);
                    $("#iEventName").html(resp.data.event_name);
                    $("#personAddon").html(resp.data.room_capacity);
                    $("#iBookDate").html(date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear());
                    $("#iTimeFrom").html(resp.data.event_start);
                    $("#iTimeUntil").html(resp.data.event_end);
                    $("#iPax").html(resp.data.room_pax);
                    $("#iVendorSnack").html(resp.data.vendor_snack);
					$("#iSnack").html(resp.data.snack);
					$("#iTotalSnack2").html(resp.data.jumlah);
					$("#iVendorLunch").html(resp.data.vendor_lunch);
                    $("#iLunch").html(resp.data.lunch);
					$("#iTotalLunch2").html(resp.data.total);
         
                    $('#infoActivityModal').modal('show');
                    $.each(resp.data.opt, function(i, obj) {
                        if(obj.euser_value > 0){
                            $( "#iOptionList :checkbox[value="+obj.option_id+"]" ).prop("checked",true);
                        }
                    });
                    
                }
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
            last_room = room;
		},
	});
	return false;
}

$(document).ready(function() //untuk dropdown snack page edit actv
{
    document.getElementById("iSnack11").disabled = false; 
    $("#iVendorSnack1").change(function(event)
    {
        event.preventDefault();
        changeValueSub();
    });
});

function changeValueSub()
{
    document.getElementById("iSnack11").disabled = false; 
    var vendor_id = $("#iVendorSnack1").val();
    if(vendor_id != '-'){
        $.ajax({
          type: "POST",
          url: "meal_snack_Vendor.html",
          data: "vendor_id="+ vendor_id,
          success: function(res) {
            $('#iSnack11').html(res);
            console.log('test jalan');
                console.log(res);
          }
        });
    }
}

$(document).ready(function()
{
    document.getElementById("iLunch1").disabled = false; 
    $("#iVendorLunch11").change(function(event)
    {
        event.preventDefault();
        changeValueSubLunch();
    });
});

function changeValueSubLunch()
{
    document.getElementById("iLunch1").disabled = false; 
    var vendor_id = $("#iVendorLunch11").val();
    if(vendor_id != '-'){
        $.ajax({
            type: "POST",
            url: "meal_lunch_Vendor.html",
            data: "vendor_id="+$vendor_id,
            success: function(res){
                $('#iLunch1').html(res);
            }
        });
    }
}
