<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!-- TemplateBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
	<script type="text/css">
		
	</script>
</head>

<body>
<div class="modal-body bg-primary">
	<center><table width="600" border="0">
  <tbody>
    <tr>
      <td width="100"><img src="../Resources/images/LogoBig.jpg" alt="Sushitei" width="50"/></td>
      <center><td>
		<center>
		  <b>
		 	PT. SUSHI-TEI INDONESIA  
		 </b><br>
		  SUSHI-TEI <i>Master Franchisee for Indonesia</i><br><br>
		  Grand Wijaya Center Blok E No. 18-19 <br>
		  Jl. Wijaya II - Kebayoran Baru <br>
		  Jakarta Selatan 12160 <br>
		  Tel: (62-21) 7280-1149 &nbsp; &nbsp; &nbsp; Fax: (62-21) 7280-1150
		  </center>		  
		</td></center>
    </tr>
  </tbody>
</table></center>

	<br>
	<br>
	<center><h3>Formulir Permohonan Meeting</h3></center>
	<div class=modal-body>
		<center><table width="600" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
	  <td width="50px">1.</td>
      <td width="200px">Nama Pemohon</td>
      <td>:</td>
      <td width="300px">username</td>
	  <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px">2.</td>
      <td width="200px">Jabatan Pemohon</td>
      <td>:</td>
      <td width="300px"></td>
	  <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px">3.</td>
      <td width="200px">Tanggal Agenda</td>
      <td>:</td>
      <td width="300px">book_date</td>
	  <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px">4.</td>
      <td width="200px">Waktu Agenda</td>
      <td>:</td>
      <td width="300px">from</td>
	  <td width="100px">s/d</td>
    </tr>
    <tr>
		<td width="50px"></td>
      <td width="200px"></td>
      <td></td>
      <td width="300px">until</td>
	  <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px">5.</td>
      <td width="200px">Jenis Agenda</td>
      <td>:</td>
      <td width="300px">event_type</td>
      <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px">6.</td>
      <td width="200px">Nama Agenda</td>
      <td>:</td>
      <td width="300px">event_name</td>
	  <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px">7.</td>
      <td width="200px">Tempat Agenda</td>
      <td>:</td>
      <td width="300px">room_name</td>
	  <td width="100px"></td>
    </tr>
	<tr>
		<td width="50px">8.</td>
      <td width="200px">Jumlah Peserta</td>
      <td>:</td>
      <td width="300px">pax</td>
	  <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px">9.</td>
      <td width="200px">Perkiraan Biaya</td>
      <td>:</td>
      <td width="300px"></td>
	  <td width="100px"></td>
    </tr>
    <tr>
		<td width="50px"></td>
      <td width="200px">a.</td>
      <td>:</td>
      <td width="300px">vendor</td>
		<td width="100px"></td>
    </tr>
    <tr>
		<td width="50px"></td>
      <td width="200px">b.</td>
      <td>:</td>
      <td width="300px">snack</td>
		<td width="100px">total</td>
    </tr>
    <tr>
		<td width="50px"></td>
      <td width="200px">c.</td>
      <td>:</td>
      <td width="300px">vendor</td>
		<td width="100px"></td>
    </tr>
	<tr>
		<td width="50px"></td>
      <td width="200px">d.</td>
      <td>:</td>
      <td width="300px">lunch</td>
		<td width="100px">total</td>
    </tr>
	<tr>
		<td width="50px">10.</td>
      <td width="200px">Sifat Agenda</td>
      <td>:</td>
      <td width="300px">a. Wajib, mendesak & harus dilaksanakan</td>
		<td width="100px"></td>
    </tr>
	<tr>
		<td width="50px"></td>
      <td width="200px"></td>
      <td></td>
      <td width="300px">b. Wajib dan tidak mendesak</td>
		<td width="100px"></td>
    </tr>
	<tr>
		<td width="50px"></td>
      <td width="200px"></td>
      <td></td>
      <td width="300px">c. Sebagai Pelengkap</td>
	  <td width="100px"></td>
    </tr>
	<tr>
		<td width="50px">13.</td>
      <td width="200px">Equipment</td>
      <td>:</td>
      <td width="300px">Projector</td>
	  <td width="100px">Y/N</td>
    </tr>
	<tr>
		<td width="50px"></td>
      <td width="200px"></td>
      <td>:</td>
      <td width="300px">Flipchart</td>
	  <td width="100px">Y/N</td>
    </tr>
	<tr>
		<td width="50px">14.</td>
      <td width="200px">Layout</td>
      <td>:</td>
      <td width="300px">Round Seating</td>
	  <td width="100px">Y/N</td>
    </tr>
	<tr>
		<td width="50px"></td>
      <td width="200px"></td>
      <td>:</td>
      <td width="300px">Column Seating</td>
	  <td width="100px">Y/N</td>
    </tr>
  </tbody>
</table>
<br>
<table width="650" border="0">
  <tbody>
    <tr>
      <td width="200"><center>Pemohon:</center></td>
      <td width="200">&nbsp;</td>
      <td width="250"><center>Menyetujui Head Department:</center></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><hr></td>
      <td>&nbsp;</td>
      <td><hr></td>
    </tr>
    <tr>
      <td><center>Ttd Tanggal</center></td>
      <td>&nbsp;</td>
      <td><center>Ttd Tanggal</center></td>
    </tr>
	<tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><center>Mengetahui GM:</center></td>
      <td>&nbsp;</td>
    </tr>
	<tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	<tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
	  <tr>
      <td>&nbsp;</td>
      <td><hr></td>
      <td>&nbsp;</td>
    </tr>
	<tr>
      <td>&nbsp;</td>
      <td><center>Ttd Tanggal</center></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>

</center>
			
	</div>
	
</div>
	
</body>
	
	
</html>
