function loginForm(me, forms)
{
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_login.html",
		dataType: "json",
        data: forms.serialize(),
        success: function(resp_data){
            if(resp_data.success == 1){
                if(resp_data.last_url != '' && resp_data.last_url != 'ajax_login.html'){
                    window.location.assign(resp_data.last_url);
                }else{
                    window.location.assign("index.html");
                }
            } else if(resp_data.success == 0) {
                $("#userName").addClass("has-error");
                $("#loginLabel").html(resp_data.message);
                if(resp_data.login_attempt <= 5){
                    alert(resp_data.message);
                }else{
                    var hide_status = $( "#recaptchaDiv" ).hasClass( "hidden" );
                    if(hide_status == 1){
                        $("#recaptchaDiv").toggleClass('hidden');
                    }
                    grecaptcha.reset();
                    $("#loginForm button[name='login']").attr("disabled", "disabled");
                }
            } else {
                alert('Unknown error.');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}

function doRegister(me, form){
    
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_register.html",
		dataType: "json",
        data: form.serialize(),
        success: function(resp_data){
            if(resp_data.success == 1){
                alert('Registration Success');
                me.data('requestRunning', false);
                window.location.assign("login.html");
            } else if(resp_data.success == 0) {
                if(resp_data.err_code == 1 || resp_data.err_code == 2){
                    alert(resp_data.message);
                }else{
                    alert(resp_data.error);
                }
            } else {
                alert('Unknown error.');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}
function doForgot(me, form){
    
    if ( me.data('requestRunning') ) {
        return;
    }
    me.data('requestRunning', true);
    $.ajax({
        type: "POST",
        url: "ajax_forgot.html",
		dataType: "json",
        data: form.serialize(),
        success: function(resp_data){
            if(resp_data.success == 1){
                alert('Please Check your email.');
                me.data('requestRunning', false);
                window.location.assign("login.html");
            } else if(resp_data.success == 0) {
                if(resp_data.err_code == 1 || resp_data.err_code == 2){
                    alert(resp_data.message);
                }else{
                    alert(resp_data.error);
                }
            } else {
                alert('Unknown error.');
            }
        },
        complete: function() {
            me.data('requestRunning', false);
        },
    });
    return false;
}