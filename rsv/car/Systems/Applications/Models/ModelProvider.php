<?php
/*
* Model for Ticketdesk
*/
class ModelProvider extends Model
{
	
    function getAllActiveProvider()
    {
        $query_string = "SELECT * FROM catering_provider WHERE provider_status=1";
		
		$result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function newProvider($input)
    {
        $result = $this->insertQuery('catering_provider', $input);
        return $result;
    }
	
	function getProviderByID($input)
    {
        $query_string = "SELECT * FROM catering_provider WHERE provider_id=:provider_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    function getAllProvider()
    {
        $query_string = "SELECT * FROM catering_provider ";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    
    function getProviderBy($input)
    {
        $query_string = "SELECT * from catering_provider ";
        $query_string .= "WHERE provider_id=:provider_id";            
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }
    
    
    function editProvider($form = array(), $where = array()){
        $table = 'catering_provider';
        $result = $this->editQuery($table, $form, $where);
        return $result;
    }
    
    function deleteProvider($id){
        
        $result = $this->deleteQuery('catering_provider' ,$id);
        return $result;
        
    }
    
}