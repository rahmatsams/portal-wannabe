<?php
/*
* Model for Ticketdesk
*/
class ModelUserData extends Model
{
	function __construct()
    {
        $config = array('db' => 'MySQL','host' => 'localhost','db_name' => 'sushitei_portal','user_name' => 'root','password' => '');
        #$config = array('db' => 'MySQL','host' => 'localhost','db_name' => 'sushitei_ticketdesk','user_name' => 'h71332_ticket','password' => 'a)_1qjbW!0-5');
        parent::__construct($config);
    }
	function increaseDataValue($input)
    {
        $this->countPagingQuery('SELECT COUNT(*) AS row_total FROM user_data WHERE data_user=:data_user AND data_site=:data_site AND data_name=:data_name', $input);
        if($this->getCountResult() > 0){
            $this->doQuery('UPDATE user_data SET data_value=data_value+1 WHERE data_user=:data_user AND data_site=:data_site AND data_name=:data_name', $input);
            return 1;
        }else{
            $input['data_value'] = 1;
            $this->insertQuery('user_data', $input);
            return 2;
        }
	}
}