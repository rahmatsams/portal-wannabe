<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class ModelCore extends Model{
		function getPageController($controllername,$actionname){
			$query_string = "SELECT controller_name,action,action_type,action_condition FROM controller_details LEFT JOIN controllers USING (controller_id) WHERE controller_name=:controller AND action=:action";
			$query_input = array(
				'controller'=>$controllername,
				'action'=>$actionname
			);
			$result = $this->fetchSingleQuery($query_string,$query_input,array('order'=>'DESC'));
			return $result;
		}
	}
?>