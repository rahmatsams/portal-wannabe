<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class ModelUsers extends Model{
		public $_result = array();
		private $table_field = array('user_id','user_name','user_password','group_id');
		
		function doLogin($input){
			$query = "SELECT user_id, display_name, group_name, email, level, store_id FROM users LEFT JOIN user_groups USING (group_id) WHERE user_name=:user_name AND user_password=:user_password";
            $result = $this->fetchSingleQuery($query, $input);
			return $result;
		}
        
        function checkForgotRequirement($input){
			$query = "SELECT * FROM users WHERE user_name=:user_name AND security_question=:security_question AND security_answer=:security_answer";
            $result = $this->fetchSingleQuery($query, $input);
			return $result;
		}
        
        function newUser($input)
        {
            
            try{
                $this->insertQuery('users', $input);
                return 1;
            } catch(Exception $e){
                if(getConfig('development') == 1){
                    echo $e;
                    exit;
                }
                return 0;
            }
        }
        
        function newTicketUser($input)
        {
            $result = $this->insertQuery('users', $input['new_user']);
            return $result;
        }
        
        function editTicketUser($input = array()){
            $result = $this->editQuery('users', $input['users'], $input['where']);
            return $result;

        }
		
        
        function checkUserName($input){
			$query = $this->prepare("SELECT user_name FROM users WHERE user_name=:username");
			$query->bindParam(':username',$input);
			$query->execute();
			if($query->rowCount() > 0){
				return 0;
			}else{
				return 1;
			}
        }
        function getAllNotAdmin(){
			$query_string = "SELECT user_id,display_name FROM users WHERE group_id=4";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        function getAllUser2(){
			$query_string = "SELECT user_id,display_name FROM users";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllNotUser(){
			$query_string = "SELECT user_id,display_name FROM users WHERE group_id NOT IN ('4')";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllEngineer(){
			$query_string = "SELECT user_id,display_name FROM users WHERE group_id=3";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        
        function getAllUser($query_option)
        {
            $query_string = "SELECT  user_id, user_name, display_name, email, user_groups.group_name, user_status ";
            $query_string .= "FROM users LEFT JOIN user_groups ON users.group_id=user_groups.group_id";
            $count_query = "SELECT COUNT(*) AS row_total FROM users";
            
            $result = $this->pagingQuery($query_string, $count_query, $query_option);
            return $result;
        }
        
        function countAllUser()
        {
            $query_string = "SELECT COUNT(*) AS total FROM users";
            
            $result = $this->doQuery($query_string);
            return $result['total'];
        }
        
		function getUserBy($input)
        {
            $query_string = "SELECT  user_id, user_name, user_password, display_name, store_id, email, user_groups.group_name, users.group_id, user_status, level ";
            $query_string .= "FROM users LEFT JOIN user_groups ON users.group_id=user_groups.group_id ";
            $query_string .= "WHERE user_id=:user_id";            
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }
        
        function editUser($form = array(), $where = array()){
            $table = 'users';
            try{
                $this->editQuery($table, $form, $where);
                return 1;
            } catch(Exception $e){
                return $e;
            }
                
        }
		
		function deleteUser($id){
			
			$result = $this->deleteQuery('users' ,$id);
			return $result;
			
		}
        
        function getAllGroups(){
			$query_string = "SELECT group_id, group_name FROM user_groups";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
		function getAllActiveStore()
        {
            $query_string = "SELECT * FROM store WHERE store_status=1";
            
            $result = $this->fetchAllQuery($query_string);
            return $result;
        }
	}
?>