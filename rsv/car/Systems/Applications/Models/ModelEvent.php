<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelEvent extends Model
	{
		
		public function getEventRangeUser($input)
		{
			$q = "SELECT event_id,event_name,room.room_name,display_name,room.room_color,start_time,end_time,event_status FROM events LEFT JOIN room ON events.room_id=room.room_id LEFT JOIN sushitei_portal.portal_user ON event_creator=sushitei_portal.portal_user.user_id WHERE event_status=1 AND (start_time BETWEEN :st1 AND :st2 OR end_time BETWEEN :ed1 AND :ed2)";
			
			return $this->fetchAllQuery($q, $input);
		}
		
		public function getOverlapDate($i)
		{
			$q = "SELECT event_id,event_name,room.room_name,display_name,room.room_color,start_time,end_time FROM events LEFT JOIN room ON events.room_id=room.room_id LEFT JOIN sushitei_portal.portal_user ON event_creator=sushitei_portal.portal_user.user_id WHERE event_status=1 AND events.room_id=:room AND (start_time BETWEEN :start_time AND :end_time OR end_time BETWEEN  :start_time AND :end_time OR :start_time BETWEEN start_time AND end_time OR :end_time BETWEEN start_time AND end_time)";
			
			return $this->fetchAllQuery($q, $i);
		}
		
		public function newEvent($input)
        {
			$t = 'events';
			return $this->insertQuery($t, $input);
		}
        
        public function getUnconfirmedEvent($f, $opt)
		{
			$q = "SELECT event_id,event_name,status_name,display_name,room_name,room_capacity,date_format(start_time,'%d-%m-%Y') AS book_date,date_format(start_time,'%H:%i') AS start,date_format(end_time,'%H:%i') AS end FROM view_events WHERE start_time >= CURDATE() AND event_status=2 ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE start_time >= CURDATE() AND event_status=2 ";
            foreach($f as $key => $value){
                $q .=  " AND {$key} LIKE '%{$value}%' ";
                $c .= " AND {$key} LIKE '%{$value}%' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getFutureEvent($f, $opt)
		{
			
			$q = "SELECT event_id,event_name,status_name,display_name,room_name,room_capacity,date_format(start_time,'%d-%m-%Y') AS book_date,date_format(start_time,'%H:%i') AS start,date_format(end_time,'%H:%i') AS end FROM view_events WHERE start_time >= CURDATE() AND event_status=1 ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE start_time >= CURDATE() AND event_status=1 ";
            foreach($f as $key => $value){
                $q .=  " AND {$key} LIKE '%{$value}%' ";
                $c .= " AND {$key} LIKE '%{$value}%' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getPastEvent($f, $opt)
		{
            $q = "SELECT event_id,event_name,status_name,display_name,room_name,room_capacity,date_format(start_time,'%d-%m-%Y') AS book_date,date_format(start_time,'%H:%i') AS start,date_format(end_time,'%H:%i') AS end FROM view_events WHERE start_time < CURDATE() ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE start_time < CURDATE() ";
            foreach($f as $key => $value){
                $q .=  " AND {$key} LIKE '%{$value}%' ";
                $c .= " AND {$key} LIKE '%{$value}%' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getEventBy($input)
        {
            $q = "SELECT * FROM events LEFT JOIN room ON room.room_id=events.room_id LEFT JOIN sushitei_portal.portal_user ON sushitei_portal.portal_user.user_id=events.event_creator WHERE event_id=:event_id";
            return $this->fetchSingleQuery($q, $input);
        }
        
        public function getMyEvent($input)
        {
            $q = "SELECT event_id,event_name,event_status,display_name,room.room_name,room.room_capacity,start_time,end_time FROM events LEFT JOIN room ON events.room_id=room.room_id LEFT JOIN sushitei_portal.portal_user ON sushitei_portal.portal_user.user_id=events.event_creator WHERE event_creator=:event_creator";
            return $this->fetchAllQuery($q, $input);
        }
		
		function editEvent($form = array(), $where = array())
		{
			$table = 'events';
			$result = $this->editQuery($table, $form, $where);
			return $result;
		}
		
		function getAdminAddress()
        {
            $query_string = 'SELECT pu.email,pu.display_name FROM sushitei_portal.portal_user pu ';
            $query_string .= 'JOIN sushitei_portal.role_permission rp ON rp.role=pu.role_id ';
            $query_string .= 'JOIN sushitei_portal.portal_permission pp ON pp.permission_id=rp.permission ';
            $query_string .= 'WHERE pu.user_status=1 AND pp.permission_code="receiveEmail" AND pp.permission_site='.getConfig('site_number').' GROUP BY pu.email';
            
            $result = $this->fetchAllQuery($query_string);
            return $result;
        }
        
        public function getUnconfirmedEventUser($f, $opt)
		{
			$q = "SELECT event_id,event_name,status_name,display_name,room_name,room_capacity,date_format(start_time,'%d-%m-%Y') AS book_date,date_format(start_time,'%H:%i') AS start,date_format(end_time,'%H:%i') AS end FROM view_events WHERE start_time >= CURDATE() AND event_status=2 ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE start_time >= CURDATE() AND event_status=2 ";
            foreach($f as $key => $value){
                $q .=  " AND {$key}='{$value}' ";
                $c .= " AND {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getFutureEventUser($f, $opt)
		{
			
			$q = "SELECT event_id,event_name,status_name,display_name,room_name,room_capacity,date_format(start_time,'%d-%m-%Y') AS book_date,date_format(start_time,'%H:%i') AS start,date_format(end_time,'%H:%i') AS end FROM view_events WHERE start_time >= CURDATE() AND event_status=1 ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE start_time >= CURDATE() AND event_status=1 ";
            foreach($f as $key => $value){
                $q .=  " AND {$key}='{$value}' ";
                $c .= " AND {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getPastEventUser($f, $opt)
		{
            $q = "SELECT event_id,event_name,status_name,display_name,room_name,room_capacity,date_format(start_time,'%d-%m-%Y') AS book_date,date_format(start_time,'%H:%i') AS start,date_format(end_time,'%H:%i') AS end FROM view_events WHERE start_time < CURDATE() ";
            $c = "SELECT COUNT(*) row_total FROM view_events WHERE start_time < CURDATE() ";
            foreach($f as $key => $value){
                $q .=  " AND {$key}='{$value}' ";
                $c .= " AND {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getAllEventUser($f, $opt)
		{
            $q = "SELECT event_id,event_name,status_name,display_name,room_name,room_capacity,date_format(start_time,'%d-%m-%Y') AS book_date,date_format(start_time,'%H:%i') AS start,date_format(end_time,'%H:%i') AS end FROM view_events ";
            $c = "SELECT COUNT(*) row_total FROM view_events ";
            foreach($f as $key => $value){
                $q .=  " WHERE {$key}='{$value}' ";
                $c .= " WHERE {$key}='{$value}' ";
            }
            
            return $this->pagingQuery($q, $c, $opt);
		}
        
        public function getEventList($f, $opt)
        {
            $q = "SELECT * FROM view_events ";
            $c = "SELECT COUNT(*) row_total FROM view_events ";
            $n = 1;
            foreach($f as $key => $value){
                if($n > 1){
                    
                    $q .= "AND {$key} LIKE '%{$value}%' ";
                    $c .= "AND {$key} LIKE '%{$value}%' ";
                    
                } else {
                   
                    $q .=  "WHERE {$key} LIKE '%{$value}%' ";
                    $c .= "WHERE {$key} LIKE '%{$value}%' ";
                   
                }
                $n++;
            }
            
            $result = $this->pagingQuery($q, $c, $opt);
            return $result;
        }
	}