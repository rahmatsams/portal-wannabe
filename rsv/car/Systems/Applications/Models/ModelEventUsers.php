<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelEventUsers extends Model
	{
		
		public function getEUbyEvent($o)
		{
			$q = "SELECT * FROM event_user WHERE event_id=:event_id";
			
			return $this->fetchAllQuery($q, $o);
		}
		
		
		public function insertEUserOption($input){
			$t = 'event_user';
			return $this->insertQuery($t, $input);
		}
	}