<?php 
/*
* Class Model Categories for Category Controller
*/
class ModelConfig extends Model
{
    function getConfigValue($key_name)
    {
        $query_string = 'SELECT config_value FROM wl_config WHERE config_name=:key';
        $query_value = array(
            'key' => $key_name
        );
        $result = $this->doQuery($query_string, $query_value);
        return $result;
    }
	
    function getConfig()
    {
        $query_string = 'SELECT * FROM wl_config';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
}
