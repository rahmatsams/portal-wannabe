            
			<div class="row">
				<div class="col-lg-3">
					<a href="#"><strong><i class="glyphicon glyphicon-calendar"></i> Book Room</strong></a>
					<hr>

					<div class="row">
						<form id="newRoom">
							<div class="col-lg-12">
								<div class="form-group ">
									<label>Choose Room</label>
									<select name="room" class="form-control" id="roomList">
										<option>-</option>
									</select>
								</div>
								<div class="form-group ">
                                    <label>Book Date</label>
                                    <input name="start_time" id="timeFrom" type="text" placeholder="yyyy-mm-dd" class="form-control" style="position: relative; z-index: 100000;" required>
								</div>
                                <div class="row">
                                    <div class="col-sm-3 form-group">
                                        <label>From</label>
                                        <select name="hour_start" class="form-control" required>
                                            <option value="00">00</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label>&nbsp;</label>
                                        <select name="minute_start" class="form-control" required>
                                            <option value="00">00</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label>Until</label>
                                        <select name="hour_until" class="form-control" required>
                                            <option value="01">01</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label>&nbsp;</label>
                                        <select name="minute_until" class="form-control" required>
                                            <option value="00">00</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div>
                                </div>
								<div class="form-group ">
									<label>Activity Name</label>
									<input name="room_name" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
								</div>
								<div class="form-group">
									<label>PAX</label>
									<div class="input-group">
										<input name="room_capacity" type="text" placeholder="Number only.." class="form-control" maxlength="4"  required aria-describedby="person-addon">
										<span class="input-group-addon" id="person-addon">Person</span>
									</div>
								</div>
								<div class="form-group checkbox">
                                    <label><input name="use_catering" type="checkbox" > Catering</label>
                                    
								</div>
								
								
								
								<button type="submit" name="submit" value="create" class="btn btn-lg btn-primary">Book Now</button>
								
							</div>
						</form> 
					</div>

				</div>
				<div class="col-lg-9">
					<div class="col-lg-12 page-header">

						<div class="pull-right form-inline">
							<div class="btn-group">
								<button class="btn btn-primary" data-calendar-nav="prev">Prev</button>
								<button class="btn btn-default" data-calendar-nav="today">Today</button>
								<button class="btn btn-primary" data-calendar-nav="next">Next</button>
							</div>
							<div class="btn-group">
								<button class="btn btn-warning" data-calendar-view="year">Year</button>
								<button class="btn btn-warning active" data-calendar-view="month">Month</button>
								<button class="btn btn-warning" data-calendar-view="week">Week</button>
								<button class="btn btn-warning" data-calendar-view="day">Day</button>
							</div>
						</div>

						<h3></h3>
					</div>
					<div class="col-lg-12">
							
						<div id="calendar"></div>
						
					</div><!-- /.row -->
				</div>
			</div>