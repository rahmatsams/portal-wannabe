            
    <div class="row">
        <div class="col-lg-4">
            <a href="#" class="dropdown" data-toggle="collapse" data-target="#collapsingMenu"><strong><i class="glyphicon glyphicon-calendar"></i> Calendar</strong></a>
            <div class="collapse" id="collapsingMenu">
                <hr>

                <ul class="nav nav-stacked" id="generatedEvent"></ul>
            </div>
            <hr>
            
            <a href="#" class="dropdown block" data-toggle="collapse" data-target="#bookForm"><strong><i class="glyphicon glyphicon-plane"></i> Book a Car</strong></a>
            <hr>

            <div class="row nav nav-stacked collapse" id="bookForm">
                <form id="newBook">
                    <div class="col-lg-12">
                        <div class="form-group ">
                            <label>Driver</label>
                            <div class="row">
                                <div class="col-lg-8">
                                    <select name="room" class="form-control" id="roomList" required>
                                        <option value="0">-</option>
                            <?php
                                foreach($room as $r){
                                    echo "
                                        <option value=\"{$r['room_id']}\">{$r['moderator_name']}</option>";
                                }
                            ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <button id="mapView" class="btn btn-md btn-success form-control mapEvent" data-id="0">Driver & Car</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label>Date</label>
                            <input name="start_date" id="dateFrom" type="date" placeholder="yyyy-mm-dd" class="form-control" required>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 form-group">
                                <label>From</label>
                                <select name="start_hour" class="form-control" required>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                </select>
                            </div>
                            <div class="col-lg-3 form-group">
                                <label>&nbsp;</label>
                                <select name="start_minute" class="form-control" required>
                                    <option value="00">00</option>
                                    <option value="30">30</option>
                                </select>
                            </div>
                            <div class="col-lg-3 form-group">
                                <label>Until</label>
                                <select name="end_hour" class="form-control" required>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                </select>
                            </div>
                            <div class="col-lg-3 form-group">
                                <label>&nbsp;</label>
                                <select name="end_minute" class="form-control" required>
                                    <option value="00">00</option>
                                    <option value="30">30</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label>Purpose</label>
                                <input name="event_name" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label>Destination</label>
                                <input name="destination" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
                            </div>
                        </div>
                        <div class="row">
                        <?php
                            if(count($trip) > 0){
                                echo '
                            <div class="col-lg-4">
                                <label>Trip Option</label>
                            ';
                                    foreach($trip as $data){
                                        echo "
                                <div class=\"form-inline\">
                                    <div class='form-group checkbox'>
                                        <label><input name='option[]' type='checkbox' value='{$data['option_id']}'>{$data['option_name']}</label>
                                    </div>
                                </div>";
                                    }
                                    echo "
                            </div>";
                            }
                        ?>
                        </div>
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="submit" name="submit" value="create" class="btn btn-md btn-primary form-control">Request</button>
                        </div>
                    </div>
                </form> 
            </div>
           
            

        </div>
        <div class="col-lg-8">
			<h2 class="sub-header"><?=$page_name?></h2>
            <ul class="nav nav-tabs">

                <li class="active"><a data-toggle="tab" href="#menu1"><b>Future Activity</b></a></li>
                <li><a data-toggle="tab" href="#menu2"><b>All Activity</b></a></li>
            </ul>

            <div class="tab-content">
                
                <div id="menu1" class="tab-pane fade in active">
                    <div  style="margin-top: 10px;">
                        <table class="table table-striped table-bordered" id="listFuture">
                            <thead>
                                <tr>
                                <th data-orderable="false">Purpose</th>
                                <th data-orderable="false">Car Plate</th>
                                <th data-orderable="false">Date</th>
                                <th data-orderable="false">Start</th>
                                <th data-orderable="false">End</th>
                                <th data-orderable="false">Status</th>
                                <th data-orderable="false">Action</th>
                                <th data-orderable="false"></th>
                            </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                        
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    
                    <div style="margin-top: 10px;">
                        <table class="table table-striped table-responsive table-bordered" id="listPast">
                            <thead>
                                <tr>
                                    <th data-orderable="false">Purpose<</th>
                                    <th data-orderable="false">Car Plate</th>
                                    <th data-orderable="false">Destination</th>
                                    <th data-orderable="false">Date</th>
                                    <th data-orderable="false">Start</th>
                                    <th data-orderable="false">End</th>
                                    <th data-orderable="false">Status</th>
                                    <th data-orderable="false">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
				
                    </div>
                </div>
            </div>
		</div>
        
	</div>
    
    <div id="cancelModal" class="modal fade" tabindex="-1" role="confirmation" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <form id="submitCancel">
                        <div class="modal-header bg-danger">
                        
                             <h4 class="modal-title">Cancel</h4>

                        </div>
                        <div class="modal-body text-danger">
                            <p>Please type the reason.</p>
                            
                            <input type="hidden" name="event_id" required>
                            <textarea name="cancel_reason" cols="50" rows="5" required></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Cancel Request</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="infoActivityModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel2">Activity Info</h4>

					</div>
					<div class="modal-body">
                        <div class="row" id="bookForm">
                            <div class="col-lg-12">
                                
                                <div class="form-group ">
                                    <label>Car Plate :</label>
                                    <span id="iRoomName"></span>
                                </div>
                                <div class="form-group ">
                                    <label>Book Date :</label>
                                    <span id="iBookDate"></span>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>From :</label>
                                        <span id="iTimeFrom"></span>
                                    </div>
                                    
                                    <div class="col-sm-6 form-group">
                                        <label>Until :</label>
                                        <span id="iTimeUntil"></span>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label>Activity Name :</label>
                                    <span id="iEventName"></span>
                                </div>
                                <div class="form-group">
                                    <label>PAX :</label>
                                    <div class="input-group">
                                        <span id="iPax"></span> Person
                                    </div>
                                </div>
                                
                                <div class="row" id="iOptionList">
								<?php
									if(count($trip) > 0){
										echo '
                                    <div class="col-lg-4">
                                        <label>Trip Option</label>
                                    ';
                                            foreach($trip as $data){
                                                echo "
                                        <div class=\"form-inline\">
                                            <div class='form-group checkbox'>
                                                <label><input name='option[]' type='checkbox' value='{$data['option_id']}'>{$data['option_name']}</label>
                                            </div>
                                        </div>";
                                            }
                                            echo "
                                    </div>";
									}
									
								?>                                    
                                </div>
                                <br>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
    <div id="mapModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                    
                         <h4 class="modal-title"></h4>

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6" id="mapImage">
                            </div>
                            <div class="col-lg-6" id="roomImage">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
