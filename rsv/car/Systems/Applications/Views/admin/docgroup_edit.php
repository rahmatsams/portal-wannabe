<div class="content-right">
  <h3 class="well">Edit Document's Group</h3>
  <div class="row">
    <form id="editDocGroup" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
      <div class="col-md-12">
        <div class="form-group">
          <?=(isset($error) && isset($error['group_name']) ? "<div class=\"alert alert-danger\"><strong>Document's Group Name </strong>{$error['group_name']}</div>" : '<label>Document\'s Group Name</label>')?>
          <input name="group_name" type="text" placeholder="Type group name.." value="<?=$docgroup['name']?>" class="form-control" maxlength="25" required>
        </div>
        <button type="submit" name="submit" value="edit_group" class="btn btn-lg btn-primary">&nbspSave&nbsp</button>
        &nbsp
        <button type="cancel" class="btn btn-lg btn-primary" onclick="window.location='admin_docgroup.html';return false;">Cancel</button>
      </div>
    </form>
  </div>
</div>