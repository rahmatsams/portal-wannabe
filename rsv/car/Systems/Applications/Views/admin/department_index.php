<div class="container-fluid">
  <h2 class="sub-header">Manage Department</h2>
  <table>
    <tr width='100%'>
      <td style="height:40px">
        <a href="admin_departmentadd.html" class="btn btn-primary btn-sm">Create New Department</a>
        <br>
      </td>
    </tr>
  </table>
  <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>#</th>
          <th>Department Name</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody id="listTable">
        <?php
        if(is_array($department) && !empty($department))
        {
          $num = (($page-1)*10)+1;
          foreach($department as $result)
          {
            echo "
                <tr>
                    <td width=\"100px\">{$num} </td>
                    <td>{$result['name']}</td>
                    <td width=\"70px\"><a href=\"admin_departmentedit_{$result['name']}.html\" class='edit-users btn btn-primary btn-sm'>Edit</a></td>
                    <td width=\"70px\"><a href=\"admin_departmentdelete_{$result['name']}.html\" class='delete-user btn btn-danger btn-sm'>Delete</a></td>
                </tr>";
            $num++;
          }
        } 
        else 
        {
          ?>
          <tr>
            <td colspan="4">There's no result found</td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
    <?php
    $pages = ceil($total/$max_result);
    if($pages > 1)
    {
      echo '
          <nav>
            <ul class="pagination pagination-sm">
              <li>
                <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
              </li>';
      for($i=1;$i <= $pages;$i++)
      {
        echo "<li ". ($page == $i ? 'class="active"' : '') ."><a href='admin_user_{$i}.html'>{$i}</a></li>";
      }
      echo '
              <li>
              <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
              </li>
            </ul>
          </nav>';
    }
    ?>
    <a href="admin_departmentadd.html" class="btn btn-primary btn-sm">Create New Department</a>
  </div>
</div>