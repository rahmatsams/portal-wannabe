<div class="container-fluid">
  <h2 class="sub-header">Manage Document's Group</h2>
  <table>
    <tr width='100%'>
      <td style="height:40px">
        <a href="admin_docgroupcreate.html" class="btn btn-primary btn-sm">Create New Documents Group</a>
        <br>
      </td>
    </tr>
  </table>
  <div class="table-responsive">
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="float: right; margin-bottom: 10px;">
      Advanced Search
    </a>
    <div class="col-lg-12 well collapse collapse-well pull-right clearleftright" id="collapseExample">
      <div class="row">
        <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-4 form-group">
                <label>Group Name</label>
                <input name="group_name" type="text" class="form-control" maxlength="50" value="<?php if(isset($criteria_group_name)) echo $criteria_group_name; ?>">
              </div>
            </div>
            <div class="row">
                <label><?php if(isset($error)) print_r($error);?></label><br>
            </div>  
            <button type="submit" name="submit" value="search" class="btn btn-lg btn-primary">Search</button>
          </div>
        </form> 
      </div>
    </div>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>#</th>
          <th>Group Name</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody id="listTable">
        <?php
        if(is_array($docgroups) && !empty($docgroups))
        {
          $num = (($page-1)*10)+1;
          foreach($docgroups as $result)
          {
            echo "
                <tr>
                  <td>{$num} </td>
                  <td>{$result['name']}</td>
                  <td><a href=\"admin_docgroupedit_{$result['id']}.html\" class='edit-users btn btn-primary btn-sm'>Edit</a></td>
                  <td><a href=\"admin_docgroupdelete_{$result['id']}.html\" class='delete-user btn btn-danger btn-sm'>Delete</a></td>
                </tr>";
            $num++;
          }
        } 
        else 
        {
          ?>
          <tr>
              <td colspan="4">There's no result found</td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
    <?php
    $pages = ceil($total/$max_result);
    if($pages > 1)
    {
      echo '
          <nav>
            <ul class="pagination pagination-sm">
              <li>
                <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
              </li>';
      for($i=1;$i <= $pages;$i++)
      {
        echo "<li ". ($page == $i ? 'class="active"' : '') ."><a href='admin_docgroup_{$i}.html'>{$i}</a></li>";
      }
      echo '
              <li>
                <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
              </li>
            </ul>
          </nav>';
    }
    ?>
    <a href="admin_docgroupcreate.html" class="btn btn-primary btn-sm">Create New Documents Group</a>
  </div>
</div>