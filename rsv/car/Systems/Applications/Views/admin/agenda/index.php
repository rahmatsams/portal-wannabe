	<div class="row">
		<div class="col-lg-3">
			<a href="#"><strong><i class="glyphicon glyphicon-calendar"></i> Admin Menu</strong></a>
			<hr>

			<?=$menu?>

		</div>
		<div class="col-lg-9">
			<h2 class="sub-header"><?=$page_name?></h2>
			<ol class="breadcrumb">
				<li><a href="admin.html">Admin Page</a></li>
				<li class="active"><a href="#"><?=$page_name?></a></li>
			</ol>
            
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#menu1"><b>Future Activity</b></a></li>
                <li><a data-toggle="tab" href="#menu2"><b>Past Activity</b></a></li>
                <li><a data-toggle="tab" href="#menu3"><b>Export</b></a></li>
            </ul>

            <div class="tab-content">
                <div id="menu1" class="tab-pane fade in active">
                    <div  style="margin-top: 10px;">
                        <table class="table table-striped table-bordered" id="listFuture">
                            <thead>
                                <tr>
                                <th data-orderable="false">Activity Name</th>
                                <th data-orderable="false">User</th>
                                <th data-orderable="false">Car Plate</th>
                                <th data-orderable="false">Date</th>
                                <th data-orderable="false">Start</th>
                                <th data-orderable="false">End</th>
                                <th data-orderable="false">Status</th>
                                <th data-orderable="false">Action</th>
                                <th data-orderable="false"></th>
                            </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                        
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade">
                    
                    <div style="margin-top: 10px;">
                        <table class="table table-striped table-responsive table-bordered" id="listPast">
                            <thead>
                                <tr>
                                    <th data-orderable="false">Activity Name</th>
                                    <th data-orderable="false">User</th>
                                    <th data-orderable="false">Room Name</th>
                                    <th data-orderable="false">PAX</th>
                                    <th data-orderable="false">Date</th>
                                    <th data-orderable="false">Start</th>
                                    <th data-orderable="false">End</th>
                                    <th data-orderable="false">Status</th>
                                    <th data-orderable="false">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
				
                    </div>
                </div>
                <div id="menu3" class="tab-pane fade">
                    
                    <div style="margin-top: 10px;">
                        <button class="btn btn-lg btn-link" id="exportCSV"><a href="exportCSV.html">Export to CSV</a></button>
                    </div>
                </div>
            </div>
		</div>
        
	</div>
	
	
	<div id="editActivityModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel2">Edit Activity</h4>

					</div>
					<div class="modal-body">
						<form id="editEvent">
							<div class="row" id="bookForm">
								<div class="col-lg-12">
									<div class="form-group ">
										<label>Choose Driver</label>
										<select name="room" class="form-control" id="roomList" required>
											<option>-</option>
								<?php
									foreach($room as $r){
										echo "
											<option value=\"{$r['room_id']}\">{$r['moderator_name']}</option>";
									}
								?>
										</select>
										<input type="hidden" name="event_id" id="evid">
									</div>
									<div class="form-group ">
										<label>Book Date</label>
										<input name="start_date" id="dateFrom" type="date" placeholder="yyyy-mm-dd" class="form-control" style="position: relative; z-index: 100000;" required>
									</div>
									<div class="row">
										<div class="col-sm-6 form-group">
											<label>From</label>
											<input name="start_time" id="timeFrom" type="time" class="form-control timepicker" style="position: relative; z-index: 100000;" required>
										</div>
										
										<div class="col-sm-6 form-group">
											<label>Until</label>
											<input name="end_time" id="timeUntil" type="time" class="form-control timepicker" style="position: relative; z-index: 99999;" required>
										</div>
									</div>
									<div class="form-group ">
										<label>Purpose</label>
										<input name="event_name" id="mEventName" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
									</div>
									<div class="form-group">
										<label>Destination</label>
										<div class="input-group">
											<input name="destination" id="destination" type="text" placeholder="Person.." class="form-control" maxlength="50" required>
										</div>
									</div>
									
									<div class="row form-inline" id="optionList">
										<?php
                                            if(count($trip) > 0){
                                                echo '
                                            <div class="col-lg-4">
                                                <label>Trip Option</label>
                                            ';
                                                    foreach($trip as $data){
                                                        echo "
                                                <div class=\"form-inline\">
                                                    <div class='form-group checkbox'>
                                                        <label><input name='option[]' type='checkbox' value='{$data['option_id']}'>{$data['option_name']}</label>
                                                    </div>
                                                </div>";
                                                    }
                                                    echo "
                                            </div>";
                                            }
                                        ?>
									</div>
									<br>
									<div class="row form-inline">
										
										<div class="form-group">
											<label>&nbsp;</label>
											<button type="submit" name="submit" value="create" class="btn btn-sm btn-primary">Approve</button>
										</div>
										<div class="form-group">
											<label>&nbsp;</label>
											<button id="cancelSubmit" class="btn btn-sm btn-warning">Close</button>
										</div>
									</div>
								</div>
							</div>
						</form> 
					</div>
				</div>
			</div>
		</div>
		
	</div>
    
    <div id="declineModal" class="modal fade" tabindex="-1" role="confirmation" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <form id="submitDecline">
                        <div class="modal-header bg-danger">
                        
                             <h4 class="modal-title">Decline</h4>

                        </div>
                        <div class="modal-body text-danger">
                            <p>Please type the reason.</p>
                            
                            <input type="hidden" name="event_id" required>
                            <textarea name="decline_reason" cols="50" rows="5" required></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Decline</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div id="cancelModal" class="modal fade" tabindex="-1" role="confirmation" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <form id="submitCancel">
                        <div class="modal-header bg-danger">
                        
                             <h4 class="modal-title">Cancel</h4>

                        </div>
                        <div class="modal-body text-danger">
                            <p>Please type the reason.</p>
                            
                            <input type="hidden" name="event_id" required>
                            <textarea name="cancel_reason" cols="50" rows="5" required></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="infoActivityModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel2">Activity Info</h4>

					</div>
					<div class="modal-body">
                        <div class="row" id="bookForm">
                            <div class="col-lg-12">
                                <div class="form-group ">
                                    <label>User Name :</label>
                                    <span id="iUserName"></span>
                                </div>
                                <div class="form-group ">
                                    <label>Car Plate Number :</label>
                                    <span id="iRoomName"></span>
                                </div>
                                <div class="form-group ">
                                    <label>Book Date :</label>
                                    <span id="iBookDate"></span>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <label>From :</label>
                                        <span id="iTimeFrom"></span>
                                    </div>
                                    
                                    <div class="col-sm-6 form-group">
                                        <label>Until :</label>
                                        <span id="iTimeUntil"></span>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label>Reason :</label>
                                    <span id="iEventName"></span>
                                </div>
                                <div class="form-group">
                                    <label>Passenger :</label>
                                    <div class="input-group">
                                        <span id="iPax"></span> Person
                                    </div>
                                </div>
                                
                                <div class="row" id="iOptionList">
								<?php
								
                                    if(count($trip) > 0){
                                        echo '
                                    <div class="col-lg-4">
                                        <label>Trip Option</label>
                                    ';
                                            foreach($trip as $data){
                                                echo "
                                        <div class=\"form-inline\">
                                            <div class='form-group checkbox'>
                                                <label><input name='option[]' type='checkbox' value='{$data['option_id']}'>{$data['option_name']}</label>
                                            </div>
                                        </div>";
                                            }
                                            echo "
                                    </div>";
                                    }
                                
								?>                                    
                                </div>
                                <br>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
