<div class="content-right">
  <h3 class="well">Delete Location</h3>
  <div class="col-lg-12 well">
    <div class="row">
      <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
        <div class="col-sm-12">
          <div class="form-group">
            <?php if(isset($error)) echo $error?>
            <label>Are you sure want to delete location : <?=$location['location_name']?> ?</label>
          </div>
          <button type="submit" name="submit" value="delete" class="btn btn-lg btn-primary">&nbsp&nbspYES&nbsp&nbsp</button>
          &nbsp
          <button type="cancel" class="btn btn-lg btn-primary" onclick="window.location='admin_location.html';return false;">&nbsp&nbsp&nbspNO&nbsp&nbsp&nbsp</button>
        </div>
      </form> 
    </div>
  </div>
</div>
					