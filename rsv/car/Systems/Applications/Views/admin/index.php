	<div class="containerx">
        <h2 class="sub-header">Administrator Page</h2>
        <hr>
        <br>
		<div class="row">
			<div class="col-lg-2 col-md-6">
				<div class="panel text-center">
                    <a href="admin_calendar.html">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <i style="font-size:10em;" class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
                                </div>
                                
                            </div>
                        </div>
					
						<div class="panel-footer">
							<span class="pull-left">Manage Calendar</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>

			<div class="col-lg-2 col-md-6">
				<div class="panel text-center">
                    <a href="admin_room.html">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <i style="font-size:10em;" class="glyphicon glyphicon-th-large" aria-hidden="true"></i>
                                </div>
                                
                            </div>
                        </div>
						<div class="panel-footer">
							<span class="pull-left">Manage Car</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-6">
				<div class="panel text-center">
                    <a href="admin_user.html">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <i style="font-size:10em;" class="glyphicon glyphicon-user" aria-hidden="true"></i>
                                </div>
                                
                            </div>
                        </div>
					
						<div class="panel-footer">
							<span class="pull-left">Manage User</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-6">
				<div class="panel text-center">
                    <a href="admin_option.html">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <i style="font-size:10em;" class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></i>
                                </div>
                                
                            </div>
                        </div>
						<div class="panel-footer">
							<span class="pull-left">Manage Option</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>