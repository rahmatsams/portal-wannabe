					<div class="content-right">
						<h3 class="well">Create New User</h3>
						<div class="row">
                            <form id="addcategory" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Username </label>
                                            <?=((isset($error) && isset($error['user_name'])) ? "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> {$error['user_name']}</div>" : '')?>
                                            <input id="adduserid" name="user_id" type="hidden" value="" class="form-control" maxlength="25" required>
                                            <input id="addusername" name="user_name" type="text" value="<?=(isset($last_input['user_name']) ? $last_input['user_name'] : '')?>" class="form-control" maxlength="25" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="adddisplayname">Display Name</label>
                                            <?=((isset($error) && isset($error['display_name'])) ? "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> {$error['display_name']}</div>" : '')?>
                                            <input id="adddisplayname" name="display_name" type="text" value="<?=(isset($last_input['display_name']) ? $last_input['display_name'] : '')?>" class="form-control" maxlength="50" required>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 form-group">
                                                <label for="addpass">Password</label>
                                                <input id="addpass" name="new_password" type="password" value="" class="form-control" maxlength="25">
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <label for="addpasskonfirm">Confirm Password</label>
                                                <input id="addpasskonfirm" name="confirm_password" type="password" value="" class="form-control" maxlength="25">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="adddemail">Email</label>
                                            <?=((isset($error) && isset($error['email'])) ? "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> {$error['email']}</div>" : '')?>
                                            <input id="addemail" name="email" type="email" value="<?=(isset($last_input['email']) ? $last_input['email'] : '')?>" class="form-control" maxlength="50" required>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <label for="addgroupid">User Type</label>
                                                <select id="addgroupid" name="group_id" class="form-control">
                                                    <?php
                                                    foreach($groups as $user_groups){
                                                        echo "\n
                                                        <option value=\"{$user_groups['group_id']}\">{$user_groups['group_name']}</option>\n";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <label for="adduserstatus">Status</label>
                                                <select id="adduserstatus" name="user_status" class="form-control">
                                                    <option value="0">Disabled</option>
                                                    <option value="1">Active</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4 form-group">
                                                <label>Store</label>
                                                <select name="store_id" class="form-control">
                                                    <?php
                                                    foreach($outlet_list as $outlet){
                                                        echo "\n
                                                        <option value=\"{$outlet['store_id']}\">{$outlet['store_name']}</option>\n";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <button type="submit" name="submit" value="new_user" class="btn btn-primary">Create</button>
                                    </div>
                            </form>
                         </div>
					</div>
					