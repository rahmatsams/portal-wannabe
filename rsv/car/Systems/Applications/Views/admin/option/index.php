	<div class="row">
		<div class="col-lg-3">
			<a href="#"><strong><i class="glyphicon glyphicon-calendar"></i> Admin Menu</strong></a>
			<hr>

			<?=$menu?>

		</div>
		<div class="col-lg-9">
			<h2 class="sub-header"><?=$page_name?></h2>
			<ol class="breadcrumb">
				<li><a href="admin.html">Admin Page</a></li>
				<li class="active"><a href="#"><?=$page_name?></a></li>
			</ol>
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Option Name</th>
							<th>Category</th>
							<th>Status</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody id="listTable">
						<?php
							if(is_array($catering) && !empty($catering)){
								$num = (($page-1)*10)+1;
								foreach($catering as $result){
									switch($result['option_status']){
										case 1:
											$status = 'Active';
											break;
										default:
											$status = 'Disabled';
									}
									echo "
						<tr>
							<td>{$num} </td>
							<td>{$result['option_name']}</td>
							<td>{$result['category_name']}</td>
							<td>{$status}</td>
							<td><a href=\"#edit{$num}\" id='edit{$num}' data-id='{$result['option_id']}' class='edit-users btn btn-primary btn-sm editOption'>Edit</a></td>
							<td><a href=\"#delete{$num}\" id='delete{$num}' class='btn btn-danger btn-sm deleteOption'>Delete</a></td>
						</tr>";
									$num++;
								}
							} else {
						?>
						<tr>
							<td colspan="4">There's no result found</td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
				
				<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newCateringModal">New Option</a>

			</div>
		</div>
	</div>
	
	<div id="newCateringModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel">Create Option</h4>

					</div>
					<div class="modal-body">
						<div class="row">
							<form id="newOption">
								<div class="col-lg-12">
									<div class="form-group ">
										<label>Option Name</label>
										<input name="option_name" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
									</div>
									<div class="form-group ">
										<label>Option Category</label>
										<select name="option_category" class="form-control" required>
                                            <?php
                                                foreach($option_category as $c){
                                                    echo "
                                            <option value='{$c['category_id']}'>{$c['category_name']}</option>";
                                                }
                                            ?>
                                        </select>
									</div>
									
									<button type="submit" name="submit" value="create" class="btn btn-md btn-primary">Create Option</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div id="editOptionModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel2">Edit Option</h4>

					</div>
					<div class="modal-body">
						<div class="row">
							<form id="editOption">
								<div class="col-lg-12">
									<div class="form-group ">
										<label>Option Name</label>
										<input name="option_name" type="text" id="eOptionName" placeholder="Type here.." class="form-control" maxlength="50" required>
                                        <input name="option_id" type="hidden" id="eOptionID" required>
									</div>
                                    <div class="form-group ">
										<label>Option Category</label>
										<select name="option_category" id="eOptionCategory" class="form-control" required>
                                            <?php
                                                foreach($option_category as $c){
                                                    echo "
                                            <option value='{$c['category_id']}'>{$c['category_name']}</option>";
                                                }
                                            ?>
                                        </select>
									</div>
									<div class="form-group ">
										<label>Option Status</label>
										<select name="option_status" id="eOptionStatus" class="form-control" required>
                                            <option value="0">Disable</option>
                                            <option value="1">Enable</option>
                                        </select>
									</div>
									
									<button type="submit" name="submit" value="edit" class="btn btn-md btn-primary">Edit Option</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>