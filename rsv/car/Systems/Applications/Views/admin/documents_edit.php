<div class="content-right">
  <h3 class="well">Edit Document</h3>
  <div class="col-lg-12 well">
    <div class="row">
      <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
        <div class="col-sm-12">
          <div class="form-group">
            <label>Document's Title</label>
            <input name="doc_id" type="hidden" value="<?=$document['id']?>">
            <input name="last_file_name" type="hidden" value="<?=$document['filename']?>">
            <input name="title" type="text" value="<?=$document['name']?>" class="form-control" maxlength="250" required>
          </div>
          <div class="form-group">
            <label>File</label>
            <input name="image_upload" type="file" class="form-control" accept="application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword">
            current file name : <?=$document['filename']?>
          </div>
          <div class="row">
            <div class="col-sm-4 form-group">
              <label>Department</label>
              <select name="department" class="form-control" required>
                <option value="" default></option>
                <?php
                foreach($department as $dep_list)
                {
                  echo "\n
                  <option value=\"{$dep_list['id']}\" ".($dep_list['id'] == $document['dept_id'] ? 'selected' : '').">{$dep_list['name']}</option>\n";
                }
                ?>
              </select>
            </div>
            <div class="col-sm-4 form-group">
                <label>Revision</label>
                <input name="rev" type="text" value="<?=$document['rev']?>" class="form-control" maxlength="3" required>
            </div>
            <div class="col-sm-4 form-group">
              <label>Printable</label>
              <select name="printable" class="form-control" required>
                <option value="0" <?=($document['printable'] == 0 ? 'selected' : '')?>>No</option>
                <option value="1" <?=($document['printable'] == 1 ? 'selected' : '')?>>Yes</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-8 form-group">
              <label>Number</label>
              <input name="number" type="text" value="<?=$document['number']?>" class="form-control" maxlength="50" required>
            </div>
            <div class="col-sm-4 form-group">
              <label>Status</label>
              <select name="doc_status" class="form-control" required>
                <option value="Active" <?=($document['status'] == 1 ? 'selected' : '')?>>Active</option>
                <option value="Obsolete" <?=($document['status'] == 0 ? 'selected' : '')?>>Obsolete</option>
              </select>
            </div>
          </div>
          <!--
          <div class="form-group">
            <label>Document's Description</label>
            <textarea name="desc" placeholder="Type documents detail.." class="form-control" rows="3"><?=$document['description']?></textarea>
          </div>
          -->
          <div class="col-sm-12 alert alert-info" >
            <strong>Accessable By :</strong>
          </div>
          <div class="row">
            <?php
            $doc_id = array();
            foreach($allowed_group as $doc_id_only)
            {
              array_push($doc_id, $doc_id_only['doc_groups_id']);
            }
           
            foreach($docgroups as $groups)
            {
              echo "
                  <div class=\"col-sm-4 form-group checkbox\">
                    <label>
                      <input type=\"checkbox\" name=\"doc_groups[]\" value=\"{$groups['id']}\" ".(in_array($groups['id'], $doc_id) ? 'checked' : '').">{$groups['name']}
                    </label>
                  </div>
                  ";
            }
            ?>
          </div>
          <button type="submit" name="submit" value="edit" class="btn btn-lg btn-primary">&nbspSave&nbsp</button>
          &nbsp
          <button type="cancel" class="btn btn-lg btn-primary" onclick="window.location='admin_documents.html';return false;">Cancel</button>
        </div>
      </form> 
    </div>
  </div>
</div>
					