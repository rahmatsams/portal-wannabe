<div class="content-right">
  <h3 class="well">Delete User</h3>
  <div class="col-lg-12 well">
    <div class="row">
      <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
        <div class="col-sm-12">
          <div class="form-group">
            <?php if(isset($error)) echo $error['undefined'];?>
            <label>Are you sure want to delete user : <?=$user['user_name']?> ?</label>
          </div>
          <button type="submit" name="submit" value="delete_user" class="btn btn-primary">&nbsp&nbspYES&nbsp&nbsp</button>
          &nbsp
          <button type="cancel" class="btn btn-primary" onclick="window.location='admin_user.html';return false;">&nbsp&nbsp&nbspNO&nbsp&nbsp&nbsp</button>
        </div>
      </form> 
    </div>
  </div>
</div>
					