	<div class="row">
		<div class="col-lg-3">
			<a href="#"><strong><i class="glyphicon glyphicon-calendar"></i> Admin Menu</strong></a>
			<hr>

			<?=$menu?>

		</div>
		<div class="col-lg-9">
			<h2 class="sub-header"><?=$page_name?></h2>
			<ol class="breadcrumb">
				<li><a href="admin.html">Admin Page</a></li>
				<li class="active"><a href="#"><?=$page_name?></a></li>
			</ol>
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Driver Name</th>
							<th>Plate</th>
							<th>Car Color</th>
							<th>Status</th>
							<th colspan="2">Action</th>
						</tr>
					</thead>
					<tbody id="listTable">
						<?php
							if(is_array($room) && !empty($room)){
								$num = (($page-1)*10)+1;
								foreach($room as $result){
									switch($result['room_status']){
										case 1:
											$status = 'Active';
											break;
										default:
											$status = 'Disabled';
									}
									echo "
						<tr>
							<td>{$num} </td>
							<td>{$result['moderator_name']}</td>
							<td>{$result['room_name']}</td>
							<td class='{$result['room_color']}'>Color</td>
							<td>{$status}</td>
							<td><a href=\"#edit{$num}\" id='edit{$num}' data-id='{$result['room_id']}' class='edit-users btn btn-primary btn-sm editRoom'>Edit</a></td>
							<td><a href=\"#delete{$num}\" id='delete{$num}' class='btn btn-danger btn-sm deleteRoom'>Delete</a></td>
						</tr>";
									$num++;
								}
							} else {
						?>
						<tr>
							<td colspan="4">There's no result found</td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
				
				<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newRoomModal">Add Driver</a>

			</div>
		</div>
	</div>
	
	<div id="newRoomModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel">Add New Car</h4>

					</div>
					<div class="modal-body">
						<div class="row">
							<form id="newRoom">
								<div class="col-lg-12">
									<div class="form-group ">
										<label>Car Plate Number</label>
										<input name="room_name" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
									</div>
									<div class="form-group ">
										<label>Driver</label>
                                        <input name="driver_name" type="text" placeholder="Driver Name.." class="form-control" maxlength="50"  required>
									</div>
                                    <div class="form-group ">
										<label>Car Capacity</label>
										<div class="input-group">
											<input name="room_capacity" type="text" placeholder="Number only.." class="form-control" maxlength="4"  required aria-describedby="person-addon">
											<span class="input-group-addon" id="person-addon">Person</span>
										</div>
									</div>
									<div class="form-group " id="radioColor">
										<label>Car Color</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-info" required> <div class="event event-info"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-warning"> <div class="event event-warning"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-inverse"> <div class="event event-inverse"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-success"> <div class="event event-success"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-special"> <div class="event event-special"></div>
										</label>
                                        <label class="radio-inline">
											<input type="radio" name="room_color" value="event-orange"> <div class="event event-orange"></div>
										</label>
                                        <label class="radio-inline">
											<input type="radio" name="room_color" value="event-red"> <div class="event event-red"></div>
										</label>
                                        <label class="radio-inline">
											<input type="radio" name="room_color" value="event-green"> <div class="event event-green"></div>
									</div>
									
									
									<button type="submit" name="submit" value="create" class="btn btn-lg btn-primary">Create Room</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div id="editRoomModal" class="modal fade" tabindex="-1" role="room-modal" data-keyboard="false">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
					
						 <h4 class="modal-title" id="myModalLabel2">Edit Car Detail</h4>

					</div>
					<div class="modal-body">
						<div class="row">
							<form id="editRoom">
								<div class="col-lg-12">
									<div class="form-group ">
										<label>Car Plate Number</label>
										<input name="room_name" type="text" id="eRoomName" placeholder="Type here.." class="form-control" maxlength="50" required>
										<input name="room_id" type="hidden" id="eRoomID" required>
									</div>
									<div class="form-group ">
										<label>Driver Name</label>
                                        <input name="moderator_name" id="eDriverName" type="text" placeholder="Number only.." class="form-control" maxlength="50"  required aria-describedby="person-addon">
									</div>
                                    <div class="form-group ">
										<label>Car Capacity</label>
										<div class="input-group">
											<input name="room_capacity" id="eRoomCapacity" type="text" placeholder="Number only.." class="form-control" maxlength="4"  required aria-describedby="person-addon">
											<span class="input-group-addon" id="person-addon">Person</span>
										</div>
									</div>
									<div class="form-group ">
										<label>Car Color</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-info" required> <div class="event event-info"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-warning"> <div class="event event-warning"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-inverse"> <div class="event event-inverse"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-success"> <div class="event event-success"></div>
										</label>
										<label class="radio-inline">
											<input type="radio" name="room_color" value="event-special"> <div class="event event-special"></div>
										</label>
                                        <label class="radio-inline">
											<input type="radio" name="room_color" value="event-orange"> <div class="event event-orange"></div>
										</label>
                                        <label class="radio-inline">
											<input type="radio" name="room_color" value="event-red"> <div class="event event-red"></div>
										</label>
                                        <label class="radio-inline">
											<input type="radio" name="room_color" value="event-green"> <div class="event event-green"></div>
									</div>
									
									
									<button type="submit" name="submit" value="edit" class="btn btn-lg btn-primary">Edit Car</button>
									
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>