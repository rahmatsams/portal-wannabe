<div class="content-right">
  <h3 class="well">Create New Document</h3>
  <div class="col-lg-12 well">
    <div class="row">
      <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
        <div class="col-sm-12">
          <div class="form-group">
            <label>Title</label>
            <input name="title" type="text" placeholder="Type the document's title.." class="form-control" maxlength="75" required>
          </div>
          <div class="form-group">
            <label>File</label>
            <input name="image_upload" type="file" class="form-control" accept="application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword" required>
          </div>
          <div class="row">
            <div class="col-sm-4 form-group">
              <label>Department</label>
              <select name="department" class="form-control" required>
                <option value="" default></option>
                <?php
                foreach($department as $dep_list)
                {
                  echo "\n <option value=\"{$dep_list['id']}\">{$dep_list['name']}</option>\n";
                }
                ?>
              </select>
            </div>
            <div class="col-sm-4 form-group">
              <label>Revision</label>
              <input name="rev" type="text" placeholder="Number of Revision.." class="form-control" maxlength="3" required>
            </div>
            <div class="col-sm-4 form-group">
              <label>Printable</label>
              <select name="printable" class="form-control" required>
                <option value="0" selected>No</option>
                <option value="1">Yes</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-8 form-group">
              <label>Number</label>
              <input name="number" type="text" placeholder="Type the document's Number.." class="form-control" maxlength="50" required>
            </div>
            <div class="col-sm-4 form-group">
              <label>Status</label>
              <select name="doc_status" class="form-control" required>
                <option value="Active" selected>Active</option>
                <option value="Obsolete">Obsolete</option>
              </select>
            </div>
          </div>
          <!--
          <div class="form-group">
            <label>Description</label>
            <textarea name="desc" placeholder="Type the document's detail.." class="form-control" rows="3"></textarea>
          </div>
          -->
          <div class="col-sm-12 alert alert-info" >
            <strong>Accessable By :</strong>
          </div>
          <div class="row">
            <?php
            foreach($docgroups as $groups)
            {
              echo "
                  <div class=\"col-sm-4 form-group checkbox\">
                    <label>
                      <input type=\"checkbox\" name=\"doc_groups[]\" value=\"{$groups['id']}\">{$groups['name']}
                    </label>
                  </div>
                  ";
                  }
            ?>
          </div>              
          <button type="submit" name="submit" value="create" class="btn btn-lg btn-primary">&nbspSave&nbsp</button>
          &nbsp
          <button type="cancel" class="btn btn-lg btn-primary" onclick="window.location='admin_documents.html';return false;">Cancel</button>
        </div>
      </form> 
    </div>
  </div>
</div>