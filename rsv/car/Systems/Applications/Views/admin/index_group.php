	<div class="container-fluid">
        <h2 class="sub-header">Manage Group</h2>
        <ol class="breadcrumb">
            <li><a href="ticket_admin.html">Administrator Page</a></li>
            <li class="active"><a href="#">Manage Group</a></li>
        </ol>
		<div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Group Name</th>
                        <th>Level</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($groups) && !empty($groups)){
                            $num = (($page-1)*10)+1;
                            foreach($groups as $result){
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['group_name']}</td>
                        <td>{$result['level']}</td>
                        <td><a href=\"admin_edit_group_{$result['group_id']}.html\" class='edit-users btn btn-primary btn-sm'>Edit</a></td>
                        <td><a href=\"admin_delete_group_{$result['group_id']}.html\" class='delete-user btn btn-danger btn-sm'>Delete</a></td>
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="4">There's no result found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
            $pages = ceil($total/$max_result);
                if($pages > 1){
                    echo '
                <nav>
                    <ul class="pagination pagination-sm">
                        <li>
                            <a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
                        </li>';
                    for($i=1;$i <= $pages;$i++){
                        echo "<li ". ($page == $i ? 'class="active"' : '') ."><a href='admin_user_{$i}.html'>{$i}</a></li>";
                    }
                    echo '
                    <li>
                        <a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                    </li>
                    </ul>
                </nav>';
                }
            ?>
            <a href="admin_create_group.html" class="btn btn-primary btn-sm">Create New Group</a>

        </div>
    </div>