        <div class="row">
            <div class="col-lg-3">
                <a href="#"><strong><i class="glyphicon glyphicon-calendar"></i> Admin Menu</strong></a>
                <hr>

                <?=$menu?>

            </div>
            <div class="col-lg-9">
                <h3 class="well">Edit User</h3>
                <ol class="breadcrumb">
                    <li class="active"><a href="admin.html">Admin</a></li>
                    <li class="active"><a href="admin_user.html">Manage User</a></li>
                    <li class="active"><?=$user['display_name']?></li>
                </ol>
                <p class="bg-success <?=(isset($success) && $success == 1 ? '' : 'hidden')?>" style="padding: 15px;">Edit Success</p>
                <p class="bg-danger <?=(isset($success) && $success == 0 ? '' : 'hidden')?>" style="padding: 15px;">Edit Failed</p>
                <div class="row">
                    <form id="addcategory" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                        <div class="col-md-12">
                                <div class="form-group">
                                    <label>Username</label>
                                    <?=((isset($error) && isset($error['user_name'])) ? "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> {$error['user_name']}</div>" : '')?>
                                    <input name="user_id" type="hidden" value="<?=$user['user_id']?>" class="form-control" maxlength="25" required>
                                    <input name="user_name" type="text" value="<?=$user['user_name']?>" class="form-control" maxlength="25">
                                </div>
                                <div class="form-group">
                                    <label for="adddisplayname">Display Name</label>
                                    <?=((isset($error) && isset($error['display_name'])) ? "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> {$error['display_name']}</div>" : '')?>
                                    <input name="display_name" type="text" value="<?=$user['display_name']?>" class="form-control" maxlength="25" required>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="addpass">Password</label>
                                        <input id="addpass" name="new_password" type="password" class="form-control" maxlength="25">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label>Confirm Password</label>
                                        <input name="confirm_password" type="password" class="form-control" maxlength="25">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <?=((isset($error) && isset($error['email'])) ? "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> {$error['email']}</div>" : '')?>
                                    <input name="email" type="email" value="<?=$user['email']?>" class="form-control" maxlength="50" required>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label>User Type</label>
                                        <select name="group_id" class="form-control">
                                            <option value="6"<?=($user['group_id'] == 6 ? ' selected' : '')?>>Administrator</option>
                                            <option value="7"<?=($user['group_id'] == 7 ? ' selected' : '')?>>User</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Status</label>
                                        <select name="user_status" class="form-control">
                                            <option value="0" <?=($user['user_status'] == 0 ? 'selected' : '')?>>Disabled</option>
                                            <option value="1" <?=($user['user_status'] == 1 ? 'selected' : '')?>>Active</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label>Store</label>
                                        <select name="store_id" class="form-control">
                                            <?php
                                            foreach($outlet_list as $outlet){
                                                echo "\n
                                                <option value=\"{$outlet['store_id']}\" " .($user['store_id'] == $outlet['store_id'] ? 'selected' : ''). ">{$outlet['store_name']}</option>\n";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" name="submit" value="edit_user" class="btn btn-primary">Edit</button>
                            </div>
                    </form>
                 </div>
        </div>
					