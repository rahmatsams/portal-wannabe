<div class="container-fluid">
  <h2 class="sub-header">Manage Documents</h2>
  <table>
    <tr width='100%'>
      <td style="height:40px">
        <a href="admin_documentscreate.html" class="btn btn-primary btn-sm">Create New Document</a>
        <br>
      </td>
    </tr>
  </table>
  <div class="table-responsive">
    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="float: right; margin-bottom: 10px;">
      Advanced Search
    </a>
    <div class="col-lg-12 well collapse collapse-well pull-right clearleftright" id="collapseExample">
      <div class="row">
        <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-6 form-group">
                <label>Document Name</label>
                <input name="doc_name" type="text" class="form-control" maxlength="50" value="<?php if(isset($criteria_doc_name)) echo $criteria_doc_name; ?>">
              </div>
              <div class="col-sm-6 form-group">
                <label>Department Name</label>
                <input name="dept_name" type="text" class="form-control" maxlength="20" value="<?php if(isset($criteria_dept_name)) echo $criteria_dept_name; ?>">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-3 form-group">
                <label>From Date</label><br>
                <input name="from_date" type="date" class="form-control" value="<?php if(isset($criteria_from_date)) echo $criteria_from_date; ?>">
              </div>
              <div class="col-sm-3 form-group">
                <label>To Date</label><br>
                <input name="to_date" type="date" class="form-control" value="<?php if(isset($criteria_to_date)) echo $criteria_to_date; ?>">
              </div>
              <div class="col-sm-3 form-group">
                <label>Number</label>
                <input name="number" type="text" class="form-control" maxlength="20" value="<?php if(isset($criteria_number)) echo $criteria_number; ?>">
              </div>
              <div class="col-sm-3 form-group">
                <label>Status</label><br>
                <select name="doc_status" class="form-control">
                  <option value="All" <?=(isset($criteria_status) ? ($criteria_status == 'ALL' ? 'selected' : '') : '')?>>All</option>
                  <option value="Active" <?=(isset($criteria_status) ? ($criteria_status == 'Active' ? 'selected' : '') : '')?>>Active</option>
                  <option value="Obsolete" <?=(isset($criteria_status) ? ($criteria_status == 'Obsolete' ? 'selected' : '') : '')?>>Obsolete</option>
                </select>
              </div>
            </div>	
            <div class="row">
                <label><?php if(isset($error)) print_r($error);?></label><br>
            </div>	
            <button type="submit" name="submit" value="search" class="btn btn-lg btn-primary">Search</button>
          </div>
        </form> 
			</div>
    </div>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
					<th>No</th>
					<th width="170px">Doc. Number</th>
					<th>Name</th>
					<th>Rev</th>
					<th>Department</th>
					<th width="170px">Date</th>
					<th width="70px">Status</th>
					<th>Printable</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody id="listTable">
        <?php
        if(is_array($docs) && !empty($docs))
        {
          $num = (($page-1)*10)+1;
          
          foreach($docs as $result)
          {
            switch($result['printable'])
            {
              case 1:
                  $status = 'YES';
                  break;
              default:
                  $status = 'NO';
            }
            if($result['status']==1)
              $doc_status='Active';
            elseif($result['status']==0)
              $doc_status='Obsolete';
            
            echo "
                <tr>
                  <td>{$num} </td>
                  <td>{$result['number']}</td>
                  <td>{$result['doc_name']}</td>
                  <td>{$result['rev']}</td>
                  <td>{$result['dept_name']}</td>
                  <td>{$result['upload_date']}</td>
                  <td>{$doc_status}</td>
                  <td>{$status}</td>
                  <td><a href=\"admin_documentsedit_{$result['id']}.html\" class='edit-users btn btn-primary btn-sm'>Edit</a></td>
                  <td><a href=\"admin_documentsdelete_{$result['id']}.html\" class='delete-user btn btn-danger btn-sm'>Delete</a></td>
                </tr>";
            $num++;
          }
        } 
        else 
        {
          ?>
          <tr>
              <td colspan="9">There's no result found</td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
    <?php
    $pages = ceil($total/$max_result);
    if($pages > 1)
    {
      echo '
          <nav>
            <ul class="pagination pagination-sm">
              <li>
                <a href="admin_documents_1.html" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
              </li>';
      $page_start = 1;
      $page_end = $pages;
      if($pages > 9 && $page > 9)
      {
        $page_start = (($page-4) < 1 ? 1 : $page-4);
        $page_end = (($page+4) > $pages ? $pages : ($page+4));
      }
      if($page <= 9)
      {
        $page_start = (($page-4) < 1 ? 1 : $page-4);
        $page_end = (($page+4) > 9 ? ($page+4) : 9);
      }
      for($i=$page_start;$i <= $page_end;$i++)
      {
        echo "<li ". ($page == $i ? 'class="active"' : '') ."><a href='admin_documents_{$i}.html'>{$i}</a></li>";
      }
      echo "
              <li>
              <a href=\"admin_documents_{$pages}.html\" aria-label=\"Next\"><span aria-hidden=\"true\">&raquo;</span></a>
              </li>
            </ul>
          </nav>";
    }
    ?>
    <a href="admin_documentscreate.html" class="btn btn-primary btn-sm">Create New Document</a>
  </div>
</div>