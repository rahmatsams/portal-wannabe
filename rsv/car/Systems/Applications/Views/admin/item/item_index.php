<a href="#"><strong><i class="glyphicon glyphicon-dashboard"></i> Manage Item</strong></a>
<hr/>
    <div class="col-lg-12">
        <div class="row">
            <form action="admin_item.html" method="POST">
                <div class="form-group col-lg-12">
                    <label class="control-label col-lg-12">Location</label>
                    <div class="col-lg-8">
                        <select name="location" class="validate[required] form-control"><?=$location_list?>
                        
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <label class="control-label"></label>
                    <div class=" col-lg-4">
                        <input class="form-control btn btn-primary btn-sm" type="submit" name="submit" value="Show">
                    </div>
                    <div class=" col-lg-4">
                        <a href="admin_item_add.html" class="form-control btn btn-primary btn-sm">Add Item</a>
                    </div>
                </div>
          </form>
        </div>
    </div>
  <div class="col-lg-12">
      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Code</th>
              <th>Name</th>
              <th>Colour</th>
              <th>Price</th>
              <th>Status</th>
              <th colspan="2">Action</th>
            </tr>
          </thead>
          <tbody id="listTable">
            <?php
            if(is_array($item_list) && !empty($item_list))
            {
              $num = 1;
              foreach($item_list as $result)
              {
                echo "
                    <tr>
                        <td width=\"100px\">{$num} </td>
                        <td>{$result['item_code']}</td>
                        <td>{$result['item_name']}</td>
                        <td>{$result['colour_name']}</td>
                        <td>{$result['item_price']}</td>
                        <td>".($result['item_status'] == 1 ? 'Active' : 'Disabled')."</td>
                        <td width=\"70px\"><a href=\"admin_item_edit_{$result['item_id']}.html\" class='edit-users btn btn-primary btn-sm'>Edit</a></td>
                        <td width=\"70px\"><a href=\"admin_item_delete_{$result['item_id']}.html\" class='delete-user btn btn-danger btn-sm'>Delete</a></td>
                    </tr>";
                $num++;
              }
            } 
            else 
            {
              ?>
              <tr>
                <td colspan="7">There's no result found</td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
    </div>