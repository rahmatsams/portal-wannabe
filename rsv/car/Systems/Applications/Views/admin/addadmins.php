			
			<div class="maincontent">
				<div class="content">
					<h2>Add Administrator</h2>
					<?php
					$data = (count($this->getFlashData()) > 0) ? $this->getFlashData() : array();
					if(isset($data['view']) && isset($data['view']['ok']) && $data['view']['ok'] == 1){
						echo 'A new administrator has been made';
					}
					?>
					<form method="POST" action="<?php echo controllerUrl('admin','addadmins'); ?>" enctype="multipart/form-data">
						<div id="quotation-form">
								<div class="formline">
									<div class="formcolumn">User Name</div>
									<div class="formcolumn">:</div>
									<div class="formcolumn"><input type="text" name="userName" id="userName" value="<?php if(isset($last_input) && !empty($last_input['userName'])){ echo $last_input['userName']; } ?>" /></div>
								</div>
								<div class="formline">
									<div class="formcolumn">Password</div>
									<div class="formcolumn">:</div>
									<div class="formcolumn"><input type="password" name="firstPassword" id="firstPassword" value="<?php if(isset($last_input) && !empty($last_input['firstPassword'])){ echo $last_input['firstPassword']; } ?>" /></div>
								</div>
								<div class="formline">
									<div class="formcolumn">Confirm Password</div>
									<div class="formcolumn">:</div>
									<div class="formcolumn"><input type="password" name="secondPassword" id="secondPassword" value="<?php if(isset($last_input) && !empty($last_input['secondPassword'])){ echo $last_input['secondPassword']; } ?>" /></div>
								</div>
								<div class="formline">
									<div class="formcolumn"><input type="submit" name="add" value="Add"></div>
								</div>
							</div>
					</form>
				</div>
			</div>