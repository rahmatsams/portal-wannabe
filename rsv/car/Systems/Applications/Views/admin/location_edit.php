<div class="content-right">
  <h3 class="well">Edit Location</h3>
  <div class="col-lg-12 well">
    <div class="row">
      <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
        <div class="col-sm-12">
          <div class="form-group">
            <label>Location Name</label>
            <input name="loc_name" type="text" value="<?=$location['location_name']?>" class="form-control" maxlength="75" required>
            <?php if(isset($error)) echo $error?>
          </div>
          <button type="submit" name="submit" value="edit" class="btn btn-lg btn-primary">&nbspSave&nbsp</button>
          &nbsp
          <button type="cancel" class="btn btn-lg btn-primary" onclick="window.location='admin_location.html';return false;">Cancel</button>
        </div>
      </form> 
    </div>
  </div>
</div>
					