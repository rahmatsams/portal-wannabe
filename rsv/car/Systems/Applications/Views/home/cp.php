            <a href="#"><strong><i class="glyphicon glyphicon-dashboard"></i> Color Plate list</strong></a>
            <hr>
            <div class="row">
              <div class="col-lg-12">
                <div class="box">
                  <form class="form-horizontal" id="listCP">
                      <div id="collapse2" class="body collapse in">
                        
                          <div class="form-group">
                            <label class="control-label col-lg-4">Outlet</label>
                            <div class="col-lg-4">
                              <select name="store" id="store" class="validate[required] form-control">
                                <?=$store?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-lg-4">Tanggal</label>
                            <div class=" col-lg-4">
                              <input class="form-control" type="date" name="date" id="date3" value="<?=date("Y-m-d")?>"/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-lg-4"></label>
                            <div class=" col-lg-4">
                              <button id="saveButton" class="btn btn-success form-control" type="submit">Save</button>
                            </div>
                          </div>
                      </div>
                      <div>
                        <p id="exportStatus" class="export-nav">&nbsp;</p>
                      </div>
                      <div class="table-responsive center-block">
                        <table class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>Code</th>
                              <th>Name</th>
                              <th>Colour</th>
                              <th>Up QTY</th>
                              <th>Down QTY</th>
                              <th>Sales QTY</th>
                            </tr>
                          </thead>
                          <tbody id="listTable">
                          </tbody>
                        </table>

                      </div>
                  </form>
                </div>
              </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->