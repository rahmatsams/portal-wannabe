            
			<div class="row">
				<div class="col-lg-4">
					<a href="#" class="dropdown" data-toggle="collapse" data-target="#collapsingMenu"><strong><i class="glyphicon glyphicon-calendar"></i> Calendar</strong></a>
                    <div class="collapse" id="collapsingMenu">
                        <hr>

                        <ul class="nav nav-stacked" id="generatedEvent"></ul>
                    </div>
                    <hr>
                    <a href="#" class="dropdown" data-toggle="collapse" data-target="#siteMap"><strong><i class="glyphicon glyphicon-map-marker"></i> Room Map</strong></a>
                    <div class="collapse" id="siteMap">
                        <hr>
                        <ul class="nav nav-stacked" id="roomMap">
                            <?php
                                foreach($room as $r){
                                    echo "
                            <li data-id=\"{$r['room_id']}\" class=\"mapEvent\"><a href=\"#\">{$r['room_name']}</a></li>";
                                }
                            ?>
                        </ul>
                    </div>
					<hr>
                    <a href="#" class="dropdown" data-toggle="collapse" data-target="#bookForm"><strong><i class="glyphicon glyphicon-envelope"></i> Reserve Room</strong></a>
					<hr>

					<div class="row nav nav-stacked collapse" id="bookForm">
						<form id="newBook">
							<div class="col-lg-12">
								<div class="form-group ">
									<label>Room</label>
									<select name="room" class="form-control" id="roomList" required>
										<option>-</option>
                            <?php
                                foreach($room as $r){
                                    echo "
                                        <option value=\"{$r['room_id']}\">{$r['room_name']}</option>";
                                }
                            ?>
									</select>
								</div>
								<div class="form-group ">
                                    <label>Date</label>
                                    <input name="start_date" id="dateFrom" type="date" placeholder="yyyy-mm-dd" class="form-control" required>
								</div>
                                <div class="row">
                                    <div class="col-lg-3 form-group">
                                        <label>From</label>
                                        <select name="start_hour" class="form-control" required>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
										</select>
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>&nbsp;</label>
										<select name="start_minute" class="form-control" required>
											<option value="00">00</option>
											<option value="30">30</option>
										</select>
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Until</label>
                                        <select name="end_hour" class="form-control" required>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
										</select>
                                    </div>
									<div class="col-lg-3 form-group">
                                        <label>&nbsp;</label>
										<select name="end_minute" class="form-control" required>
											<option value="00">00</option>
											<option value="30">30</option>
										</select>
                                    </div>
                                </div>
								<div class="form-group ">
									<label>Activity Name</label>
									<input name="event_name" type="text" placeholder="Type here.." class="form-control" maxlength="50"  required>
								</div>
								<div class="form-group">
									<label>Capacity</label>
									<div class="input-group">
										<input name="room_pax" id="pax" type="number" placeholder="Person.." class="form-control" max="9999"  required aria-describedby="person-addon">
										<span class="input-group-addon" id="personAddon"></span>
									</div>
								</div>
								<div class="row">
								</div>
								<?php
									if(count($meal) > 0){
										echo '
										
                                <div class="form-group">
									<label>Meal</label>
								</div>';
										foreach($meal as $data){
											echo "
                                <div class=\"row form-inline\">
                                    <div class='col-sm-6 form-group checkbox'>
										<label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
									</div>
								</div>";
										}
										echo "
								<br>";
									}
									if(count($equipment) > 0){
										echo '
                                <div class="form-group">
									<label>Equipment</label>
								</div>';
										foreach($equipment as $data){
											echo "
                                <div class=\"row form-inline\">
                                    <div class='col-sm-6 form-group checkbox'>
										<label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
									</div>
								</div>";
										}
										echo "
								<br>";
									}
									
									if(count($layout) > 0){
										echo '
                                <div class="form-group">
									<label>Layout</label>
								</div>';
										foreach($layout as $data){
											echo "
                                <div class=\"row form-inline\">
                                    <div class='col-sm-6 form-group checkbox'>
										<label><input name='option[]' type='checkbox' value='{$data['option_id']}' >{$data['option_name']}</label>
									</div>
								</div>";
										}
										echo "
								<br>";
									}
								?>
								<br>
								<div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" name="submit" value="create" class="btn btn-md btn-primary">Book Request</button>
								</div>
							</div>
						</form> 
					</div>
                   
					

				</div>
				<div class="col-lg-8">
					<div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">My Account</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <p class="bg-success <?=(isset($success) && $success == 1 ? '' : 'hidden')?>" style="padding: 15px;">Edit Success</p>
                    <p class="bg-danger <?=(isset($success) && $success == 0 ? '' : 'hidden')?>" style="padding: 15px;">Edit Failed</p>
                    <div class="well">
                        <div class="row">
                            <form id="register" method="POST" action="account.html">
                                <div class="col-sm-12">
                                    <div class="form-group has-feedback <?=(isset($error['display_name']) ? 'has-error' : '')?>">
                                        <label for="displayName">Display Name</label>
                                        <label class="control-label <?=isset($error['display_name']) ? '' : 'sr-only'?>" for="displayName"><?=isset($error['display_name']) ? $error['display_name'] : ''?></label>
                                        <input id="displayName" name="display_name" type="text" placeholder="Cth: Leonardo Da Vinci" class="form-control" maxlength="25" value="<?=$account['display_name']?>" required>
                                        <?php
                                            if(isset($error['display_name_char']) || isset($error['display_name_min'])){
                                                echo '
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                        <span id="inputError2Status" class="sr-only">(error)</span>';
                                            }
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <label class="control-label <?=isset($error['email']) ? '' : 'sr-only'?>" for="eMail"><?=isset($flash_data['email']) ? $error['email'] : ''?></label>
                                        <input id="eMail" name="email" type="email" placeholder="Masukkan alamat email.." class="form-control" value="<?=$account['email']?>" required>
                                    </div>	
                                    <div class="form-group has-feedback <?=(isset($error['old_password']) ? 'has-error' : '')?>"">
                                        <label class="control-label <?=isset($error['old_password']) ? '' : 'sr-only'?>" for="oldpasswordinput"><?=isset($error['old_password']) ? $error['old_password'] : ''?></label>
                                        <label for="oldpasswordinput">Current Password</label>
                                        <input id="oldpasswordinput" name="old_password" type="password" class="form-control" maxlength="25"  placeholder="Type old password..">
                                        <?php
                                            if(isset($error['old_password'])){
                                                echo '
                                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                        <span id="inputError2Status" class="sr-only">(error)</span>';
                                            }
                                        ?>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-6 form-group has-feedback <?=(isset($error['new_password']) ? 'has-error' : '')?>" id="passwordForm">
                                            <label>New Password</label>
                                            <label class="control-label <?=isset($error['new_password']) ? '' : 'sr-only'?>" for="passwordInput"><?=isset($error['new_password']) ? $error['new_password'] : ''?></label>
                                            <input id="newpasswordinput" name="new_password" type="password" placeholder="Type new password.." class="form-control" aria-describedby="inputError2Status">
                                            <?php
                                            if(isset($error['new_password_min'])){
                                                echo '
                                            <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                            <span id="inputError2Status" class="sr-only">(error)</span>';
                                            }
                                            ?>
                                            
                                        </div>	
                                        <div class="col-sm-6 form-group has-feedback <?=(isset($error['password_confirm']) ? 'has-error' : '')?>" id="passwordConfirmForm">
                                            <label>Confirm new password</label>
                                            <label class="control-label <?=isset($error['password_confirm']) ? '' : 'sr-only'?>" for="passwordConfirm"><?=isset($error['password_confirm']) ? $error['password_confirm'] : ''?></label>
                                            <input id="passwordConfirm" name="password_confirm" type="password" placeholder="Type password confirmation.." class="form-control">
                                            <?php
                                            if(isset($error['password_confirm'])){
                                                echo '
                                            <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                            <span id="inputError2Status" class="sr-only">(error)</span>';
                                            }
                                            ?>
                                        </div>	
                                    </div>
                                    	
                                    <button class="btn btn-lg btn-primary" type="submit" name="register" value="Edit">Edit</button>
                                </div>
                            </form> 
                        </div>
				</div>
			</div>
            <div id="mapModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content">
                            <div class="modal-header">
                            
                                 <h4 class="modal-title"></h4>

                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-6" id="mapImage">
                                    </div>
                                    <div class="col-lg-6" id="roomImage">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="eventModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content">
                            <div class="modal-header">
                            
                                 <h4 class="modal-title"></h4>

                            </div>
                            <div class="modal-body">
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>