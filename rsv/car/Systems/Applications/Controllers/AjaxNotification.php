<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxNotification extends Controller
{
	public function accessRules()
	{
		return array(
            array('Allow', 
				'actions'=>array('viewNotificationData'),
				'groups'=>array('*'),
			),
			
			array('Deny', 
				'actions'=>array('viewEventList', 'getUserList', 'getTicketData', 'getCategoryEdit', 'editTicket', 'getUserDetail', 'getTotalPage'),
				'groups'=>array('Guest'),
			),
		);
	}
	
	public function viewNotificationData()
	{
		
        
        if($this->_mySession['group'] == 'Administrator' || $this->_mySession['group'] == 'Super Admin'){
            $m = $this->load->model('Event');
            $data = $m->getFutureEvent();
            
            echo json_encode(array('success'=>1, 'data' => $data['total']));
            
        }else{
            $m = $this->load->model('Notification');
            $data = $m->getNotification(array('user_id'=>$this->_mySession['userid']));
            
            echo json_encode(array('success'=>1, 'data' => $data['total']));
            
        }
	}
    
}
/*
* End Home Class
*/