<?php
class Admin extends Controller
{
	private function _adminMenu()
	{
		$menu = '
		<ul class="nav nav-stacked">
			<li class="active"><a href="index.html"><i class="glyphicon glyphicon-list-alt"></i> Front Page Home</a></li>
			<li role="separator" class="divider"></li>
			<li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu">Admin <i class="glyphicon glyphicon-chevron-down"></i></a>
				<ul class="nav nav-stacked collapsed" id="userMenu">
					<li class="active"><a href="admin_room.html"><i class="glyphicon glyphicon-envelope"></i> Manage Car</a></li>
					<li><a href="admin_calendar.html"><i class="glyphicon glyphicon-exclamation-sign"></i> Manage Calendar</a></li>
					<li><a href="admin_user.html"><i class="glyphicon glyphicon-user"></i> Manage User</a></li>
					<li><a href="admin_option.html"><i class="glyphicon glyphicon-comment"></i> Manage Option</a></li>
					<li><a href="logout.html"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
				</ul>
			</li>
			
		
		</ul>
		';
		return $menu;
	}
	#Index Halaman Admin
	
    public function viewIndex()
    {
        $data = array(
            'admin' => $this->isAdmin()
		);
		$option = array(
            'page_name' => 'Admin Page',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
            'admin' => $this->isAdmin()
        );
        $this->load->template('admin/index', $data, $option);
    }
  
    #Index Halaman Manage Room
	
	public function viewRoomIndex()
	{
		$m = $this->load->model('Room');
		$data = array(
			'page_name' => 'Manage Car',
			'menu' => $this->_adminMenu(),
			'room' => $m->getAllRoom(),
			'page' => 1,
		);
		$option = array(
			'page_name' => 'Manage Car',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'style' => '
			<link rel="stylesheet" href="Resources/css/calendar.css">',
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/ascript.js'></script>
			",
            'admin' => $this->isAdmin()
			
        );
        $view = $this->load->template('admin/room/index', $data, $option);
	}
    
	#Index Halaman Manage User
	public function viewUserIndex()
	{
        $m_users = $this->load->model('Users');
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'user_name',
            'order' => 'ASC',
        );
        $data = array(
            '_mySession' => $this->_mySession,
            'users' => $m_users->getAllUser($query_option),
            'total' => $m_users->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
            'groups' => $m_users->getAllGroups(),
			'menu' => $this->_adminMenu(),
            'admin' => $this->isAdmin()
        );
        $option = array(
            'page_name' => 'Manage User',
            'session' => $this->_mySession,
            'admin' => $this->isAdmin()
        );
         
        $this->load->template('admin/user/index', $data, $option);
    }
	
	#Index Halaman Manage Catering
	public function viewProviderIndex()
	{
		$m = $this->load->model('Provider');
		$data = array(
			'page_name' => 'Manage Catering',
			'menu' => $this->_adminMenu(),
			'catering' => $m->getAllProvider(),
			'page' => 1,
		);
		$option = array(
			'page_name' => 'Manage Catering',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/bscript.js'></script>
			",
            'admin' => $this->isAdmin()
			
        );
        $view = $this->load->template('admin/catering/index', $data, $option);
	}
    
    #Index Halaman Manage Agenda
	public function viewAgendaIndex()
	{
		$m = $this->load->model('Event');
		$mr = $this->load->model('Room');
        $moption = $this->load->model('EventOptions');
		$data = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'Manage Calendar',
			'menu' => $this->_adminMenu(),
			'room' => $mr->getallActiveRoom(),
            'trip' => $moption->getOptionType('1'),
			#'page' => 1,
		);
		$option = array(
			'page_name' => 'Manage Calendar',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
            'style' => "
            <link rel='stylesheet' href='Resources/css/jquery.dataTables.min.css'>
            ",
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/cscript.js'></script>
            <script type='text/javascript' src='Resources/js/jquery.dataTables.min.js'></script>
			",
			'admin' => $this->isAdmin()
        );
        $view = $this->load->template('admin/agenda/index', $data, $option);
	}
    
    #Index Halaman Manage Option
	public function viewOptionIndex()
	{
		$m = $this->load->model('EventOptions');
		$data = array(
			'page_name' => 'Manage Option',
			'menu' => $this->_adminMenu(),
			'catering' => $m->getAllOption(),
            'option_category' => $m->getAllCategory(),
			'page' => 1,
            'admin' => $this->isAdmin()
		);
		$option = array(
			'page_name' => 'Manage Option',            
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'scripts' => "
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/dscript.js'></script>
			",
			
        );
        $view = $this->load->template('admin/option/index', $data, $option);
	}
}