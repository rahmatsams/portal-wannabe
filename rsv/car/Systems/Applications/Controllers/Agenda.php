<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Agenda extends Controller
{
    
    public function accessRules()
    {
		return array(
            array('Allow', 
				'actions'=>array('viewIndex', 'viewBook'),
				'groups'=>array('*'),
			),
			array('Deny', 
				'actions'=>array('viewIndex'),
				'groups'=>array('Guest'),
			),
		);
    }
    
    
    public function viewIndex()
    {
        $mroom = $this->load->model('Room');
		$moption = $this->load->model('EventOptions');
        $data = array(
            '_mySession' => $this->_mySession,
            'room' => $mroom->getAllActiveRoom(),
			'trip' => $moption->getOptionType('1'),
            'admin' => $this->isAdmin(),
        );
		
        $option = array(
            'page_name' => 'Dashboard',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
            'admin' => $this->isAdmin(),
			'style' => '
			<link rel="stylesheet" href="Resources/css/calendar.css">',
			'scripts' => "
			<script type='text/javascript' src='Resources/js/underscore-min.js'></script>
			<script type='text/javascript' src='Resources/js/calendar.js'></script>
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/myscript.js'></script>
			",
			
        );
        $this->load->Template('agenda/home', $data, $option);
        
    }
	
	public function viewBook()
    {
        
        $data = array(
            '_mySession' => $this->_mySession,
            'admin' => $this->isAdmin()
        );
        $option = array(
            'admin' => $this->isAdmin(),
            'page_name' => 'Book Room',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
			'style' => '
			<link rel="stylesheet" href="Resources/css/calendar.css">
            <link rel="stylesheet" href="Resources/css/jquery-ui.css">
            ',
			'scripts' => "
            <script type='text/javascript' src='Resources/js/jquery-ui.min.js'></script>
			<script type='text/javascript' src='Resources/js/underscore-min.js'></script>
			<script type='text/javascript' src='Resources/js/calendar.js'></script>
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/myscript.js'></script>
            <script type='text/javascript' src='Templates/".getConfig('default_template')."/js/bookscript.js'></script>
			",
			
        );
        $this->load->Template('agenda/book', $data, $option);
        
    }
    
}
/*
* End Home Class
*/