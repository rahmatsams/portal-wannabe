<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxAgenda extends Controller
{
	private $message;
	
	public function accessRules()
	{
		return array(
            array('Allow', 
				'actions'=>array('viewEventList', 'viewNewEvent', 'viewActiveOptionData', 'viewEventData', 'viewEventCancel', 'viewEventListUser', 'viewUserCancel', 'viewUserCancel', 'viewEventDetail'),
				'groups'=>array('*'),
			),
            array('Allow', 
				'actions'=>array('viewEventDecline' ,'viewEditEvent', 'viewEventListAdmin'),
				'groups'=>array('Super Admin', 'Administrator', 'Admin GA'),
			),
			array('Deny', 
				'actions'=>array('viewEventList', 'viewActiveOptionData', 'viewEventData'),
				'groups'=>array('Guest'),
			),
		);
	}
	
    protected function eventModel()
    {
        return $this->load->model('Event');
    }
	
	public function viewEventList()
	{
		if(isset($_GET['from']) && isset($_GET['to'])){
			$input = $this->load->lib('Input');
			$input->addValidation('from_format', $_GET['from'], 'numeric', 'Please check your input.');
			$input->addValidation('from_req', $_GET['from'], 'min=1', 'Please check your input.');
			$input->addValidation('from_length', $_GET['from'], 'max=15', 'Please check your input.');
			$input->addValidation('to_format', $_GET['to'], 'numeric', 'Please check your input.');
			$input->addValidation('to_req', $_GET['to'], 'min=1', 'Please check your input.');
			$input->addValidation('to_length', $_GET['to'], 'max=15', 'Please check your input.');
			if($input->validate()){
				$m = $this->load->model('Event');
				$data = array(
					'st1' => date('Y-m-d', $_GET['from']/1000),
					'st2' => date('Y-m-d', $_GET['to']/1000),
					'ed1' => date('Y-m-d', $_GET['from']/1000),
					'ed2' => date('Y-m-d', $_GET['to']/1000)
				);
				$edata = $m->getEventRangeUser($data);
				if(is_array($edata) && count($edata) > 0){
					$r = array(
						'success' => 1,
						'result' => array()
					);
					$i = 0;
					foreach($edata as $cdata){
						if(isset($cdata['event_id'])){
							$r['result'][$i]['id'] = $cdata['event_id'];
							$r['result'][$i]['title'] = "{$cdata['room_name']} - {$cdata['event_name']}";
							$r['result'][$i]['url'] = "event_detail_{$cdata['event_id']}.html";
							$r['result'][$i]['class'] = $cdata['room_color'];
							$r['result'][$i]['time_start'] = date("H:i", strtotime($cdata['start_time']));
							$r['result'][$i]['time_end'] = date("H:i", strtotime($cdata['end_time']));
							$r['result'][$i]['start'] = strtotime($cdata['start_time']).'000';
							$r['result'][$i]['end'] = strtotime($cdata['end_time']).'000';
							$i++;
						}

					}
				}else{
					$r = array(
						'success' => 1,
						'result' => array()
					);
				}
				echo json_encode($r);
			}else{
				echo json_encode($input->_error);
			}
		}
	}
    
    public function viewEventListAdmin()
	{
        $m = $this->eventModel();
        $data = 0;
        $f = array();

        $o = array(
            'page' => ($_POST['start']/$_POST['length'] > 0 ? ($_POST['start']/$_POST['length']+1) : 1),
            'result' => $_POST['length'],
            'order_by' => 'start_time',
            'order' => 'ASC'
        );
        switch($_GET['id']){
            case 1:
                $data = $m->getUnconfirmedEvent($f, $o);
            break;
            case 2:
                $o['order_by'] = 'start_time,room_name';
                $data = $m->getFutureEvent($f, $o);
            break;
            default:
                $o['order'] = 'DESC';
                $data = $m->getPastEvent($f, $o);
        }
        $result = array(
            'draw' => $_POST['draw'],
            'data' => $data,
            'recordsTotal' => $m->getCountResult(),
            'recordsFiltered' => $m->getCountResult(),
        );
        
        echo json_encode($result);
 
	}
	public function viewEventListUser()
	{
        $m = $this->eventModel();
        $data = 0;
        $f = array(
            'user_id' => $this->_mySession['user_id'],
        );

        $o = array(
            'page' => ($_POST['start']/$_POST['length'] > 0 ? ($_POST['start']/$_POST['length']+1) : 1),
            'result' => $_POST['length'],
            'order_by' => 'start_time',
            'order' => 'ASC'
        );
        switch($_GET['id']){
            case 1:
                $data = $m->getUnconfirmedEventUser($f, $o);
            break;
            case 2:
                $o['order_by'] = 'start_time,room_name';
                $data = $m->getFutureEventUser($f, $o);
            break;
            default:
                $o['order'] = 'DESC';
                $data = $m->getAllEventUser($f, $o);
        }
        $result = array(
            'draw' => $_POST['draw'],
            'data' => $data,
            'recordsTotal' => $m->getCountResult(),
            'recordsFiltered' => $m->getCountResult(),
        );
        
        echo json_encode($result);
 
	}
    
	public function viewNewEvent()
    
	{
		if(isset($_POST['room']) && isset($_POST['event_name']) && isset($_POST['start_hour']) && isset($_POST['start_minute']) && isset($_POST['end_hour']) && isset($_POST['end_minute']) && isset($_POST['destination'])){
			
			$input = $this->load->lib('Input');
            $input->addValidation('room_required', $_POST['room'], 'min=1', 'Room is required');
            $input->addValidation('room_format', $_POST['room'], 'numeric', 'Please check your room input');
            $input->addValidation('event_required', $_POST['event_name'], 'min=1', 'Event name should be filled');
            $input->addValidation('event_format', $_POST['event_name'], 'alpha_numeric_sc', 'Please check your room input');
            $input->addValidation('start_required', $_POST['start_hour'].':'.$_POST['start_minute'], 'min=1', 'Book starting time is empty.');
            $input->addValidation('start_format', $_POST['start_hour'].':'.$_POST['start_minute'], 'hour', 'Please check your start time input');
            $input->addValidation('end_required', $_POST['end_hour'].':'.$_POST['end_minute'], 'min=1', 'Book ending time is empty.');
            $input->addValidation('end_format', $_POST['end_hour'].':'.$_POST['end_minute'], 'hour', 'Please check your end time input');
            $input->addValidation('destination_required', $_POST['destination'], 'min=1', 'Destination is required');
            $input->addValidation('destination_format', $_POST['destination'], 'alpha_numeric_sc', 'Please check your destination input');
            if($input->validate()){
                $m = $this->load->model('Event');
                $mroom = $this->load->model('Room');
                $st_time = $_POST['start_date'].' '.$_POST['start_hour'].':'.$_POST['start_minute'].':00';
                $ed_time = $_POST['start_date'].' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00';
                if($ed_time < $st_time){
                    $overlap = array(
                        'start_time' => $_POST['start_date'].' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00',
                        'end_time' => $_POST['start_date'].' '.$_POST['start_hour'].':'.$_POST['start_minute'].':00',
                        'room' => $_POST['room']
                    );
                }else{
                    $overlap = array(
                        'start_time' => $_POST['start_date'].' '.$_POST['start_hour'].':'.$_POST['start_minute'].':00',
                        'end_time' => $_POST['start_date'].' '.$_POST['end_hour'].':'.$_POST['end_minute'].':00',
                        'room' => $_POST['room']
                    );
                }
                $data = $m->getOverlapDate($overlap);
                if(is_array($data) && count($data) > 0){
                    $output = array(
                        'user_name' => $data[0]['display_name'],
                        'current_event' => $data[0]['event_name'],
                        'event_start' => date("H:i", strtotime($data[0]['start_time'])),
                        'event_end' => date("H:i", strtotime($data[0]['end_time']))
                    );
                    $return = array(
                        'success' => 0,
                        'message' => 1,
                        'data' => $output
                    );
                    echo json_encode($return);
                }else{
                    $submit = array(
                        'event_name' => $_POST['event_name'],
                        'room_id' => $_POST['room'],
                        'destination' => $_POST['destination'],
                        'event_creator' => $this->_mySession['user_id'],
                        'submit_time' => date("Y-m-d H:i:s"),
                        'start_time' => $overlap['start_time'],
                        'end_time' => $overlap['end_time'],
                        'event_status' => 1,
                    );
                    if($m->newEvent($submit)){
                        $eo = $this->load->model('EventOptions');
                        $i = 1;
                        if(isset($_POST['option'])){
                            foreach($_POST['option'] as $option){
                                $data = array(
                                    'event_id' => $m->lastInsertID(),
                                    'option_id' => $option,
                                    'euser_value' => 1
                                );
                                $eo->insertEUserOption($data);
                            }
                        }
                        $mail_data = array(
                            'title' => 'Reservation #'.$m->lastInsertID().' (NEW)',
                            'view' => 'email/new',
                            'data' => array(
                                'detail' => $m->getEventBy(array('event_id' => $m->lastInsertID())),
                                'content' => 'You\'ve successfully booked a Car with detail:',
                                'header' => ''
                            ),
                            'recipient' => array(
                                'address' => $this->_mySession['email'],
                                'name' => $this->_mySession['user_name'],
                                
                            ),
                            
                        );
                        $return = array(
                            'success' => 1,
                            'email' => 0,
                        );
                        
                        if($this->send_email($mail_data)){
                            $return['email'] = 1;
                        }
                        $return['message'] = $this->message;
                    }
                }
            }else{
                $return = array(
                    'success' => 0,
                    'email' => 0,
                    'error' => $input->_error
                );
            }
            echo json_encode($return);
		}else{
			echo json_encode(array('success'=>0));
		}
	}
	
    public function viewActiveOptionData()
	{
		
		$m = $this->load->model('EventOptions');
		$data = $m->getActiveOption();
		if(count($data) > 0){
			echo json_encode(array('success'=>1, 'data' => $data));
		}else{
			echo json_encode(array('success'=>0));
		}
	}
	
	
	public function viewEventData()
	{
		
            $m = $this->load->model('Event');
            $meo = $this->load->model('EventOptions');
            $meu = $this->load->model('EventUsers');
            $data = $m->getEventBy($_POST);
            $opt = $meo->getActiveOption();
            $optuser = $meu->getEUbyEvent($_POST);
            $m = null;
            if(count($data) > 0){
                foreach($opt as $key => $val){
                    $m .= "<div class='col-sm-6 form-group checkbox'>";
                    $m .= "<label><input name='option[]' type='checkbox' value='{$val['option_id']}'";
                    foreach($optuser as $key2 => $val2){
                        if($val['option_id'] == $val2['option_id'] && $val2['euser_value'] == 1){
                            $m .= " checked";
                        }
                    }
                     $m .= "> {$val['option_name']}</label></div>";
                }
				$output = array(
					'room_id' => $data['room_id'],
                    'display_name' => $data['display_name'],
                    'room_name' => $data['room_name'],
					'event_id' => $data['event_id'],
					'event_name' => $data['event_name'],
					'event_date' => date('Y-m-d', strtotime($data['start_time'])),
					'event_start' => date('H:i:s', strtotime($data['start_time'])),
					'event_end' => date('H:i:s', strtotime($data['end_time'])),
					'destination' => $data['destination'],
					'room_capacity' => $data['room_capacity'],
                    'opt' => $optuser
				);
                echo json_encode(array('success'=>1, 'data' => $output));
            }else{
                echo json_encode(array('success'=>0));
            }
	}
	
	public function viewEventDecline()
	{
		if(isset($_POST['event_id']) && isset($_POST['decline_reason'])){
			$input = $this->load->lib('Input');
			$input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
			$input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->load->model('Event');
				
                $i = array(
                    'status_desc' => $_POST['decline_reason'],
                    'event_status' => 0,
                );
                $w = array(
                    'event_id' => $_POST['event_id']
                );
                
                if($m->editEvent($i, $w)){
                    $data = $m->getEventBy(array('event_id' => $_POST['event_id']));
                    $n = $this->load->model('Notification');
                    $n_data = array(
                        'notification_user' => $data['event_creator'],
                        'notification_type' => 2,#Decline
                        'notification_status'=>0, #Unread
                    );
                    $n->newNotification($n_data);
                    $mail_data = array(
                        'title' => "Reservation #{$data['event_id']}.' (Declined)",
                        'view' => 'email/decline',
						'data' => array(
							'detail' => $data,
							'content' => 'Your Car booking with detail:',
							'header' => 'Are Declined'
						),
						'recipient' => array(
							'address' => $data['email'],
							'name' => $data['display_name'],
						),
						
					);
					$return = array(
						'success' => 1,
						'email' => 0,
					);
					
					if($this->send_email($mail_data)){
						$return['email'] = 1;
					}
                    echo json_encode($return);
                }else{
                    echo json_encode(array('success'=>0));
                }
            
				
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
            print_r($_POST);
			echo json_encode(array('success'=>0));
		}
	}
	
    public function viewEventCancel()
	{
		if(isset($_POST['event_id']) && isset($_POST['cancel_reason'])){
			$input = $this->load->lib('Input');
			$input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
			$input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->eventModel();
				
                $i = array(
                    'status_desc' => $_POST['cancel_reason'],
                    'event_status' => 3,
                );
                $w = array(
                    'event_id' => $_POST['event_id']
                );
                
                if($m->editEvent($i, $w)){
                    $data = $m->getEventBy(array('event_id' => $_POST['event_id']));
                    $n = $this->load->model('Notification');
                    $n_data = array(
                        'notification_user' => $data['event_creator'],
                        'notification_type' => 3,#Decline
                        'notification_status'=>0, #Unread
                    );
                    $n->newNotification($n_data);
                    $mail_data = array(
                        'title' => "Reservation #{$data['event_id']}.' (Cancelled)",
                        'view' => 'email/cancel',
						'data' => array(
							'detail' => $data,
							'content' => 'Permintaan booking ruangan anda dengan detail:',
							'header' => 'Permintaan Anda Dibatalkan (Cancelled)'
						),
						'recipient' => array(
							'address' => $data['email'],
							'name' => $data['display_name'],
						),
						
					);
					$return = array(
						'success' => 1,
						'email' => 0,
					);
					
					if($this->send_email($mail_data)){
						$return['email'] = 1;
					}
                    echo json_encode($return);
                }else{
                    echo json_encode(array('success'=>0));
                }
            
				
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
            print_r($_POST);
			echo json_encode(array('success'=>0));
		}
	}
    
    public function viewUserCancel()
	{
		if(isset($_POST['event_id']) && isset($_POST['cancel_reason'])){
			$input = $this->load->lib('Input');
			$input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
			$input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->eventModel();
				
                $i = array(
                    'status_desc' => $_POST['cancel_reason'],
                    'event_status' => 5,
                );
                $w = array(
                    'event_id' => $_POST['event_id']
                );
                
                $return = array('success' => 0);
                $data = $m->getEventBy(array('event_id' => $_POST['event_id']));
                
                if(date('Y-m-d') <= date('Y-m-d', strtotime($data['start_time']."-1 days"))){
                    if($m->editEvent($i, $w)){
                        $n = $this->load->model('Notification');
                        $n_data = array(
                            'notification_user' => $data['event_creator'],
                            'notification_type' => 5,#Decline
                            'notification_status'=>0, #Unread
                        );
                        $n->newNotification($n_data);
                        $mail_data = array(
                            'title' => "Reservation #{$data['event_id']}.' (Cancelled)",
                            'view' => 'email/cancel',
                            'data' => array(
                                'detail' => $data,
                                'content' => 'Permintaan booking ruangan anda dengan detail:',
                                'header' => 'Anda membatalkan request'
                            ),
                            'recipient' => array(
                                'address' => $data['email'],
                                'name' => $data['display_name'],
                            ),
                            
                        );
                        $return = array(
                            'success' => 1,
                            'email' => 0,
                        );
                        
                        if($this->send_email($mail_data)){
                            $return['email'] = 1;
                        }
                        
                    }
                }else{
                    $return['error'] = 'Pembatalan booking maksimal h-1';
                }
                echo json_encode($return);
            
				
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
            print_r($_POST);
			echo json_encode(array('success'=>0));
		}
	}
    
    public function viewEditEvent()
	{
		if(isset($_POST['event_id'])){
			$input = $this->load->lib('Input');
			$input->addValidation('event_id_format',$_POST['event_id'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('event_id_required',$_POST['event_id'],'min=1', 'Is Required');
			$input->addValidation('event_id_max',$_POST['event_id'],'max=4', 'Max capacity is 9999');
			if($input->validate()){
				$m = $this->load->model('Event');
				$st_time = $_POST['start_date'].' '.$_POST['start_time'];
				$ed_time = $_POST['start_date'].' '.$_POST['end_time'];
				if($ed_time < $st_time){
					$overlap = array(
						'start_time' => $_POST['start_date'].' '.$_POST['end_time'],
						'end_time' => $_POST['start_date'].' '.$_POST['start_time'],
						'room' => $_POST['room']
					);
				}else{
					$overlap = array(
						'start_time' => $_POST['start_date'].' '.$_POST['start_time'],
						'end_time' => $_POST['start_date'].' '.$_POST['end_time'],
						'room' => $_POST['room']
					);
				}
				$data = $m->getOverlapDate($overlap);
				if(is_array($data) && count($data) > 0 && $_POST['event_id'] != $data[0]['event_id']){
					$return = array(
						'success' => 0,
						'message' => 1,
						'data' => $data,
					);
					echo json_encode($return);
				}else{
					$i = array(
						'room_id' => $_POST['room'],
						'start_time' => $overlap['start_time'],
						'end_time' => $overlap['end_time'],
						'event_name' => $_POST['event_name'],
						'destination' => $_POST['destination'],
						'event_status' => 1,
					);
					$w = array(
						'event_id' => $_POST['event_id']
					);
					
					if($m->editEvent($i, $w)){
						$eo = $this->load->model('EventOptions');
                        $dm = $m->getEventBy(array('event_id' => $_POST['event_id']));
						$eo->deleteEUserOption(array('event_id' => $_POST['event_id']));
						foreach($_POST['option'] as $option){
							$data = array(
								'event_id' => $_POST['event_id'],
								'option_id' => $option,
								'euser_value' => 1
							);
							$eo->insertEUserOption($data);
						}
                        $ed = $m->getEventBy(array('event_id' => $_POST['event_id']));
                        $n = $this->load->model('Notification');
                        $n_data = array(
                            'notification_user' => $ed['event_creator'],
                            'notification_type' => 1,#Approve
                            'notification_status' =>0, #Unread
                        );
                        $n->newNotification($n_data);
                        $mail_data = array(
                            'title' => "Reservation #{$data['event_id']}.' (Approved)",
                            'view' => 'email/approve',
                            'data' => array(
                                'detail' => $dm,
                                'content' => 'Permintaan booking ruangan anda dengan detail:',
                                'header' => 'Permintaan Anda Diterima (Approved)'
                            ),
                            'recipient' => array(
                                'address' => $dm['email'],
                                'name' => $dm['display_name'],
                            ),
                            
                        );
                        $return = array(
                            'success' => 1,
                            'email' => 0,
                        );
                        
                        if($this->send_email($mail_data)){
                            $return['email'] = 1;
                        }
                        echo json_encode($return);
					}else{
						echo json_encode(array('success'=>0));
					}
				}
				
			}else{
				echo json_encode(array('success'=>0, 'error' => $input->_error));
			}
		}else{
			echo json_encode(array('success'=>0));
		}
	}
    
public function viewEventDetail()
	{
		if(isset($_GET['eid'])){
			$input = $this->load->lib('Input');
			$input->addValidation('event_id_format',$_GET['eid'],'numeric', 'Room Name only accept A-z 0-9 and space as input');
			$input->addValidation('event_id_required',$_GET['eid'],'min=1', 'Is Required');
			$input->addValidation('event_id_max',$_GET['eid'],'max=4', 'Max capacity is 9999');
            if($input->validate()){
                $m = $this->load->model('Event');
                $s = array(
                    'event_id' => $_GET['eid']
                );
                $data = $m->getEventBy($s);
                $m = null;
                if(count($data) > 0){
                    #print_r($data);
                    echo "
                    <div class='modal-body'>
                        <div class='row'>
                            <div class='col-lg-12'>
                                <div class='form-group '>
                                    <label>User Name :</label>
                                    <span id='iuser_name'>{$data['display_name']}</span>
                                </div>
                                <div class='form-group '>
                                    <label>Car Number :</label>
                                    <span id='iRoomName'>{$data['room_name']}</span>
                                </div>
                                <div class='form-group '>
                                    <label>Book Date :</label>
                                    <span id='iBookDate'>".date("d-m-Y", strtotime($data['start_time']))."</span>
                                </div>
                                <div class='row'>
                                    <div class='col-sm-6 form-group'>
                                        <label>From :</label>
                                        <span id='iTimeFrom'>".date("H:i", strtotime($data['start_time']))."</span>
                                    </div>
                                    
                                    <div class='col-sm-6 form-group'>
                                        <label>Until :</label>
                                        <span id='iTimeUntil'>".date("H:i", strtotime($data['end_time']))."</span>
                                    </div>
                                </div>
                                <div class='form-group '>
                                    <label>Purpose :</label>
                                    <span id='iEventName'>{$data['event_name']}</span>
                                </div>
                                <div class='form-group'>
                                    <label>Destination :</label>
                                    <div class='input-group'>
                                        <span id='iPax'>{$data['destination']}</span>
                                    </div>
                                </div>
                                
                                <div class='row' id='iOptionList'>
                                </div>
                                <br>
                            </div>";
                }else{
                    echo json_encode(array('success'=>0));
                }
            }
        }
	}
    
	/* Mailing */
	
	private function send_email($option){
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        #$stmail = $this->load->lib('STMail');
		try
		{
			$stmail->IsSMTP(); // telling the class to use SMTP
			$stmail->Host       = "mail.sushitei.co.id"; // SMTP server
			$stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
													   // 1 = errors and messages
													   // 2 = messages only
			$stmail->SMTPAuth   = true;                  // enable SMTP authentication
			$stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
			$stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
			$stmail->Username   = "ga.helpdesk@sushitei.co.id"; // SMTP account user_name
			$stmail->Password   = "St123456";        // SMTP account password

			$stmail->SetFrom('ga.helpdesk@sushitei.co.id', 'Car Booking');

			$stmail->AddReplyTo('ga.helpdesk@sushitei.co.id', 'Car Booking');
			
			$stmail->isHTML('true');

			$stmail->Subject    = $option['title'];
			
            ob_start();
            $this->load->view($option['view'], $option['data']);
            $returned = ob_get_contents();
            ob_end_clean();
            
			$stmail->MsgHTML($returned);
			
			$stmail->AddAddress($option['recipient']['address'], $option['recipient']['name']);
			
			$m = $this->eventModel();
			foreach($m->getAdminAddress() as $recipient){
				$stmail->AddBCC($recipient['email'], $recipient['display_name']);
			}
			return ($stmail->Send() ? 1 : 0);
		} catch (phpmailerException $e) {
		    $this->message = $e->errorMessage();
			return 0;
		    
		} catch (Exception $e) {
			$this->message = $e->getMessage();
			return 0;
			
		}
        
        
    }
	
}
/*
* End Home Class
*/