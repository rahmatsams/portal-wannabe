<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* User Controller
*/
class User extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('viewIndex', 'insert_category','my_account', 'edit_user'),
                'groups'=>array('Guest'),
            ),
            array('Allow', 
                'actions'=>array('user_login', 'new_user'),
                'groups'=>array('Super Admin', 'Administrator'),
            ),
            array('Allow', 
                'actions'=>array('viewIndex', 'viewEditUser', 'viewNewUser', 'getUser'),
                'groups'=>array('Super Admin', 'Administrator'),
            ),
            array('Allow', 
                'actions'=>array('text', 'register_ok' , 'viewMyAccount', 'viewLogin', 'viewLogout', 'user_login', 'viewMyHistory'),
                'groups'=>array('*'),
            ),
        );
    }
    
	private function _adminMenu()
	{
		$menu = '
		<ul class="nav nav-stacked">
			<li class="active"><a href="index.html"><i class="glyphicon glyphicon-list-alt"></i> Front Page Home</a></li>
			<li role="separator" class="divider"></li>
			<li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu">Admin <i class="glyphicon glyphicon-chevron-down"></i></a>
				<ul class="nav nav-stacked collapsed" id="userMenu">
					<li class="active"><a href="admin_room.html"><i class="glyphicon glyphicon-envelope"></i> Manage Car</a></li>
					<li><a href="admin_calendar.html"><i class="glyphicon glyphicon-exclamation-sign"></i> Manage Calendar</a></li>
					<li class="active"><a href="admin_user.html"><i class="glyphicon glyphicon-user"></i> Manage User</a></li>
					<li><a href="admin_option.html"><i class="glyphicon glyphicon-comment"></i> Manage Option</a></li>
					<li><a href="logout.html"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
				</ul>
			</li>
			
		
		</ul>
		';
		return $menu;
	}
    
    public function load_model(){
		$model = $this->load->model('Users');
		return $model;
	}
    
    public function viewLogin()
    {
        if($this->isGuest()){
            $store = $this->load->model('Store');
            $data = array(
                '_mySession' => $this->_mySession,
                '_template_path' => 'Templates/'.getConfig('default_template'),
                'store' => $store->getAllActiveStore()
            ) ;
            $this->load->View('users/login', $data);
        }else{
            header("Location: index.php");
        }
    }
    
    public function viewLogout()
    {        
        $this->unsetSession('user_id');
        $this->unsetSession('outlet');
        $this->setSession('username','Guest');
        $this->setSession('group','Guest');
        $this->setSession('attempt',0);
        $this->setSession('role',1);
        header("Location: index.html");
    }
    
    
    public function viewMyAccount(){
        $data = array(
            '_mySession' => $this->_mySession,
        );
        $option = array(
            'session' => $this->_mySession,
        );
        $account = $this->load->model('Users');
        $store = $this->load->model('Store');
        $data['store'] = $store->getAllActiveStore();
        if(isset($_POST['register']) && $_POST['register'] == 'Edit' ){
            $input = $this->load->lib('Input');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc','Cek kembali input anda');
            $input->addValidation('email',$_POST['email'],'email','Cek kembali input anda');
            if(!empty($_POST['new_password'])){
                $input->addValidation('new_password', $_POST['new_password'], 'min=6', 'Minimal length of password is 6 Character');
                $input->addValidation('password_confirm', $_POST['password_confirm'], 'like=' . $_POST['new_password'], 'Password does not match');
            }
            if($input->validate()){
                $input_data = array(
                    'display_name' => $_POST['display_name'],
                    'email' => $_POST['email']
                );
                $where = array('user_id' => $this->_mySession['userid']);
                if(!empty($_POST['new_password'])){
                    $input_data['user_password'] = md5($_POST['new_password'].getConfig('salt'));
                    $where['user_password'] = md5($_POST['old_password'].getConfig('salt'));
                }
                
                
                if($account->editUser($input_data, $where)){
                    $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['userid']));
                    $data['success'] = 1;
                    $this->load->template('users/my_account', $data, $option);
                }else{
                    $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['userid']));
                    $data['success'] = 0;
                    $this->load->template('users/my_account', $data, $option);
                }
            }else{
                $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['userid']));
                $data['success'] = 0;
                $data['error'] = $input->_error;
                $this->load->template('users/my_account', $data, $option);
            }
            
        }else{
            $data['account'] = $account->getUserBy(array('user_id' => $this->_mySession['userid']));
            if(is_array($data['account']) && count($data['account']) > 1){
                $this->load->template('users/my_account', $data, $option);
            }else{
                $this->showError(2);
            }
        }
    }
    
    public function viewEditUser(){
        $account = $this->load->model('Users');
        $data = array(
            '_mySession' => $this->_mySession,
            'groups' => $account->getAllGroups(),
            'outlet_list' => $account->getAllActiveStore(),
            'page_name' => 'Manage Room',
			'menu' => $this->_adminMenu(),
        );
        $option = array(
			'page_name' => 'Manage Room',            
            'session' => $this->_mySession,
			
        );
        $input = $this->load->lib('Input');
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_user' ){
            $input->addValidation('user_id',$_POST['user_id'], 'numeric', 'Cek kembali input anda');
            $input->addValidation('user_id',$_POST['user_id'], 'max=4', 'Cek kembali input anda');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Please check your input');
            $input->addValidation('user_name',$_POST['user_name'],'min=1', 'cannot be blank');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Only Accept numeric and t');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Cannot be blank');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('store_id', $_POST['store_id'],'min=1', 'Cannot be blank');
            $input->addValidation('store_id', $_POST['store_id'],'numeric', 'Please check your input');
            $input->addValidation('group_id', $_POST['group_id'],'min=1', 'Cannot be blank');
            $input->addValidation('group_id', $_POST['group_id'],'numeric', 'Please check your input');
            if(!empty($_POST['new_password'])){
                $input->addValidation('new_password', $_POST['new_password'], 'min=6', 'Password minimal 6 huruf');
                $input->addValidation('confirm_password', $_POST['confirm_password'], 'like=' . $_POST['new_password'], 'Password konfirmasi harus sama dengan password awal');
            }
            $data['user'] = $account->getUserBy(array('user_id' => $_POST['user_id']));
            if($input->validate()){
                $input_data = array(
                    'users' => array(
                        'display_name' => $_POST['display_name'],
                        'group_id' => $_POST['group_id'],
                        'store_id' => $_POST['store_id'],
                        'email' => $_POST['email'],
                        'user_status' => $_POST['user_status'],
                    ),
                    'where' => array(
                        'user_id' => $_POST['user_id']
                    ),
                );
                if(!empty($_POST['new_password'])){
                    $input_data['users']['user_password'] = md5($_POST['new_password'].getConfig('salt'));
                }
                
                if($account->editTicketUser($input_data)){
                    $data['user'] = $account->getUserBy(array('user_id' => $_POST['user_id']));
                    $data['success'] = 1;
                }else{
                    $data['success'] = 0;
                }
            }else{
                $data['success'] = 0;
                $data['error'] = $input->_error;
            }
            
            $this->load->template('admin/user/edit', $data, $option);
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'User not found');
                $input->addValidation('id', $_GET['id'], 'max=6', 'User not found');
                if($input->validate()){
                    $data['user'] = $account->getUserBy(array('user_id' => $_GET['id']));
                    if($data['user']['level'] > $this->_mySession['role']){
                        exit('you dont have enough privilege');
                    }else{
                        $this->load->template('admin/user/edit', $data, $option);
                    }
                }
            }
            
        }
    }
	
    public function viewNewUser()
    {
        $m_users = $this->load->model('Users');
        $data = array(
            '_mySession' => $this->_mySession,
            'groups' => $m_users->getAllGroups(),
            'outlet_list' => $m_users->getAllActiveStore(),
            'menu' => $this->_adminMenu(),
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'new_user'){
            $input = $this->load->lib('Input');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Please check your input');
            $input->addValidation('user_name',$_POST['user_name'],'min=1', 'cannot be blank');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Please check your input');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Cannot be blank');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('new_password', $_POST['new_password'],'min=6', 'Password lenght should be greater than 6 character');
            $input->addValidation('confirm_password', $_POST['confirm_password'],'like='.$_POST['new_password'], 'Password confirmation is different');
            $input->addValidation('store_id', $_POST['store_id'],'min=1', 'Cannot be blank');
            $input->addValidation('store_id', $_POST['store_id'],'numeric', 'Please check your input');
            $input->addValidation('group_id', $_POST['group_id'],'min=1', 'Cannot be blank');
            $input->addValidation('group_id', $_POST['group_id'],'numeric', 'Please check your input');
            if($input->validate()){
                $insert_value = array(
                    'new_user' => array(
                        'user_name' => $_POST['user_name'],
                        'user_password' => MD5($_POST['new_password'].getConfig('salt')),
                        'display_name' => $_POST['display_name'],
                        'email' => $_POST['email'],
                        'store_id' => $_POST['store_id'],
                        'group_id' => $_POST['group_id'],
                        'user_status' => $_POST['user_status']
                    )
                );
                if($m_users->checkUserName($_POST['user_name'])){
                    if($m_users->newTicketUser($insert_value)){
                        header("Location: admin_user.html");
                        
                    } else {
                        exit('Unexpected Error');
                    }
                }else{
                    $data['error']['user_name'] = 'is already used';
                }
            } else {
                $data['error'] = $input->_error;
            }
            $data['last_input'] = $_POST;
            
        }
        
        $this->load->template('admin/user/add', $data);
        
    }
    
    
    
	public function delete_user(){
		if(isset($_POST['rem_user'])){
			$input = $this->load->lib('Input');
			$input->addValidation('id',$_POST['id'], 'numeric', 'Periksa kembali input anda');
			if($input->validate()){
				if($this->load_model->deleteUser($_POST)){
					echo 'sukses';
				}
			}else{
				print_r($input->_error);
			}
		}
	}
    
    public function getUser(){
		$list_users = $this->load_model->getAllUser2();
		echo json_encode($list_users);
	}
    
    
    
    public function viewMyHistory()
	{
		$m = $this->load->model('Event');
        $mn = $this->load->model('Notification');
        $moption = $this->load->model('EventOptions');
        $mroom = $this->load->model('Room');
		$data = array(
			'page_name' => 'My History',
            'room' => $mroom->getAllActiveRoom(),
            'trip' => $moption->getOptionType('1'),
            'admin' => $this->isAdmin()
			#'page' => 1,
		);
		$option = array(
            'admin' => $this->isAdmin(),
			'page_name' => 'My History',
            'config' => $this->load->model('Config'),
            'session' => $this->_mySession,
            'style' => "
            <link rel='stylesheet' href='Resources/css/jquery.dataTables.min.css'>
            ",
			'scripts' => "
            <script type='text/javascript' src='Resources/js/jquery.dataTables.min.js'></script>
			<script type='text/javascript' src='Templates/".getConfig('default_template')."/js/scripts2.js'></script>
			",
			
        );
        $view = $this->load->template('agenda/my_agenda', $data, $option);
        $mn->clearNotification(array('notification_user' => $this->_mySession['user_id']));
	}
}

/*
End User Controller
*/