var holidays2017 = {
	// January 1
	'01-01': "Tahun Baru 2017",
	'01-05': "Hari Buruh Internasional",
	'11-05': "Hari Raya Waisak 2561",
	'01-06': "Hari Lahir Pancasila",
	'17-08': "Hari Kemerdekaan Indonesia",
	'25-12': "Hari Raya Natal",
};

var last_room = null;
(function($) {

	"use strict";
	var datef = new Date().toISOString().slice(0,10);
	
	var options = {
		events_source: 'events.html',
		view: 'month',
		tmpl_path: 'Resources/tmpls/',
        modal: '#events-modal',
		tmpl_cache: false,
		day: datef,
		first_day: 1,
		time_start: '08:00',
		time_end: '18:00',
		width: '100%',
		holidays: holidays2017,
		onAfterEventsLoad: function(events) {
			if(!events) {
				return;
			}
			var list = $('#generatedEvent');
			list.html('');

			$.each(events, function(key, val) {
				$(document.createElement('li'))
					.html('<a href="' + val.url + '">' + val.title + '</a>')
					.appendTo(list);
			});
		},
		onAfterViewLoad: function(view) {
			$('.page-header h3').text(this.getTitle());
			$('.btn-group button').removeClass('active');
			$('button[data-calendar-view="' + view + '"]').addClass('active');
		},
		classes: {
			months: {
				general: 'label'
			}
		}
	};

	var calendar = $('#calendar').calendar(options);

	$('.btn-group button[data-calendar-nav]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.navigate($this.data('calendar-nav'));
		});
	});

	$('.btn-group button[data-calendar-view]').each(function() {
		var $this = $(this);
		$this.click(function() {
			calendar.view($this.data('calendar-view'));
		});
	});

	$('#first_day').change(function(){
		var value = $(this).val();
		value = value.length ? parseInt(value) : null;
		calendar.setOptions({first_day: value});
		calendar.view();
	});

	$('#language').change(function(){
		calendar.setLanguage($(this).val());
		calendar.view();
	});

	$('#events-in-modal').change(function(){
		var val = $(this).is(':checked') ? $(this).val() : null;
		calendar.setOptions({modal: val});
	});
	$('#format-12-hours').change(function(){
		var val = $(this).is(':checked') ? true : false;
		calendar.setOptions({format12: val});
		calendar.view();
	});
	$('#show_wbn').change(function(){
		var val = $(this).is(':checked') ? true : false;
		calendar.setOptions({display_week_numbers: val});
		calendar.view();
	});
	$('#show_wb').change(function(){
		var val = $(this).is(':checked') ? true : false;
		calendar.setOptions({weekbox: val});
		calendar.view();
	});
	$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
		//e.preventDefault();
		//e.stopPropagation();
	});
	
	function wait(ms){
	   var start = new Date().getTime();
	   var end = start;
	   while(end < start + ms) {
		 end = new Date().getTime();
	  }
	}
	$("#loadingModal").on("hide.bs.modal", function () {
		$('#loadingModal .modal-content').html('<div class="modal-header"><h4 class="modal-title" id="myModalLabel">Loading</h4></div><div class="modal-body"><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div></div>');
	});
    
}(jQuery));


$(document).ready(function(){
    var max_pax = 0;
	
    $("#roomList").change(function(){
        var me = $(this);
        var room = $("#roomList").val();
        $("#mapView").data('id', room);
        if(room > 0){
            getRoomData(me, room);
        }
    });
	
	$("#newBook").submit(function(event){
		event.preventDefault();
        
        $("#disclaimerModal").modal('show');
		
	});
    $("#bookDisclaimer").click(function(event){
		event.preventDefault();
		var me = $(this);
		var form = $("#newBook");
		bookRoom(me, form);
	});
    $("#mapView").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
        if(room != "0"){
            showMapModal(me, room);
            checkBook(holidays2017);
        }
	});

});

function checkBook(data){
    var date = new Date($("#dateFrom").val());
    var month = (1 + date.getMonth()).toString();
    var day = date.getDay();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    
    var date2 = day+'-'+month;
    
    if(data[date2]){
        alert('ada');
    }else if(day == 6 || day == 0){
        alert('minggu');
    }
}

function loadOption(){
	$.ajax({
		type: "POST",
		url: "option_data.html",
		dataType: "json",
		success: function(resp){
			if(resp.success == 1){
				$.each(resp.data, function(){
                    var html = "<div class='col-sm-6 form-group checkbox'>"+
                                        "<label><input name='option[]' type='checkbox' value='"+this.option_id+"' > "+this.option_name+"</label>"+
                                    "</div>";
                    $("#optionList").append(html);
                });
			}
		},
	});
	
}

function showMapModal(me, room)
{
	
    $("#mapModal").modal('show');
    var place;
    
    $.ajax({
        url:'Resources/images/room/'+room+'.jpg',
        type:'HEAD',
        error: function()
        {
            place = 'Resources/images/room/not_found.jpg';
            $("#roomImage").html('<a href="'+place+'"><img src="'+place+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        },
        success: function()
        {
            place = 'Resources/images/room/'+room+'.jpg';
            $("#roomImage").html('<a href="'+place+'"><img src="'+place+'" alt="" class="map-images img-responsive img-thumbnail"></a>');
        }
    });
    
}

function getRoomData(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "room_data.html",
		dataType: "json",
		data: 'room_id='+room,
		success: function(resp){
			if(resp.success == 1){
				$("#personAddon").html('/ '+resp.data.room_capacity);
                $("#mapModal .modal-title").html(resp.data.room_name);
                max_pax = resp.data.room_capacity;
			} else if(resp.success == 0){
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function bookRoom(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_book.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
                var message = 'You\'ve successfully booked a Car.';
                $("#successModal .modal-body").html(message);
                $("#successModal").modal('toggle');
			} else if(resp.success == 0)    {
				if(resp.data != null){
                    var l = resp.data;
					var message = 'Car already used by '+ l.user_name +' for '+ l.current_event + ' from ' + l.event_start + ' to ' + l.event_end;
					
					$("#failedModal .modal-body").html(message);
					$("#failedModal").modal('toggle');
				}
			}else{
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function eventData(me)
{
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	var eid;
	$.ajax({
		type: "POST",
		url: "event_data.html",
		dataType: "json",
		data: 'event_id='+me.data('id'),
		success: function(resp){
			if(resp.success == 1)    {
                eid = resp.data.event_id;
                var date = new Date(resp.data.event_date);
                $("#iRoomName").html(resp.data.room_name);
                $("#iEventName").html(resp.data.event_name);
                $("#personAddon").html(resp.data.room_capacity);
                $("#iBookDate").html(date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear());
                $("#iTimeFrom").html(resp.data.event_start);
                $("#iTimeUntil").html(resp.data.event_end);
                $("#iPax").html(resp.data.room_pax);
                $('#infoActivityModal').modal('show');
                alert('Success');
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
            last_room = room;
            me.data('requestRunning', false);
		},
	});
	return false;
}
