var last_room = null;
$(document).ready(function(){
    var room_info = null;
	$("#newCatering").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#newCatering");
		submitNewCatering(me, room);
	});
	$("#editEvent").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#editEvent");
		submitEditEvent(me, room);
	});
    $(document).on('click', '.manageEvent' , function(event) {
        event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		if(last_room == room){
            $('#editActivityModal').modal('show');
        }else{
            eventData(me, room, 1);
        }
    });
    $(document).on('click', '.infoEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
        if(last_room == room){
            $('#infoActivityModal').modal('show');
        }else{
            eventData(me, room, 2);
        }
	});

	$(document).on('click', '.declineEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		showDeclineModal(me, room);
	});
    $(document).on('click', '.cancelEvent' ,function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		showCancelModal(me, room);
	});
    $("#submitDecline").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $('#submitDecline');
		submitDeclineEvent(me, room);
	});
    $("#submitCancel").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $('#submitCancel');
		submitCancelEvent(me, room);
	});
    $("#cancelSubmit").click(function(event){
		event.preventDefault();
		$('#editActivityModal').modal('hide');
	});
    
    $('#listFuture').DataTable( {
        serverSide: true,
        searching : false,
        ajax: {
            url: 'event_list_2.html',
            type: 'POST'
        },
        columns: [
            { "data": "event_name" },
            { "data": "display_name" },
            { "data": "room_name" },
            { "data": "book_date" },
            { "data": "start" },
            { "data": "end" },
            { "data": "status_name" },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-danger btn-sm cancelEvent" data-id="'+data+'">Cancel</a>';
                }
            },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-success btn-sm manageEvent" data-id="'+data+'">Manage</a>';
                }
            },
        ]
    } );
    $('#listPast').DataTable( {
        serverSide: true,
        searching : false,
        ajax: {
            url: 'event_list_3.html',
            type: 'POST'
        },
        columns: [
            { "data": "event_name" },
            { "data": "display_name" },
            { "data": "room_name" },
            { "data": "room_capacity" },
            { "data": "book_date" },
            { "data": "start" },
            { "data": "end" },
            { "data": "status_name" },
            { 
                "data": "event_id",
                "filtering" : false,
                "sort" : false,
                "render": function ( data, type, row, meta ) {
                    return '<button class="btn btn-success btn-sm infoEvent" data-id="'+data+'">Info</a>';
                }
            },
        ]
    } );
});

function showDeclineModal(me, room)
{
	
    
    $("#submitDecline input[name='event_id']").val(room);
    $("#submitDecline textarea").html('');
    $("#declineModal .modal-title").html('Decline Confirmation');
    $("#declineModal").modal('show');
   
}

function showCancelModal(me, room)
{
	
    
    $("#submitCancel input[name='event_id']").val(room);
    $("#submitCancel textarea").html('');
    $("#cancelModal .modal-title").html('Cancel Confirmation');
    $("#cancelModal").modal('show');
   
}

function submitDeclineEvent(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "event_decline.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
                var message = 'Book declined successfully.';
                $("#successModal .modal-title").html('Decline Book');
                $("#successModal .modal-body").html(message);
                $("#declineModal").modal('hide');
                $("#successModal").modal('toggle');
			} else if(resp.success == 0)    {
				alert('Decline Failed');
			}else{
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

function submitCancelEvent(me, room){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "event_cancel.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
                var message = 'Book Successfully Cancelled.';
                $("#successModal .modal-title").html('Cancel Book');
                $("#successModal .modal-body").html(message);
                $("#cancelModal").modal('hide');
                $("#successModal").modal('toggle');
			} else if(resp.success == 0)    {
				alert('Cancel Failed');
			}else{
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

function submitEditEvent(me, room)
{
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_edit_event.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Approved');
				location.reload();
			} else if(resp.success == 0 && resp.message == 1)    {
				if(resp.data != null){
					var message = "" ;
					$.each( resp.data, function( i, l ){
						message = 'Room already used by '+ l.display_name +' for '+ l.event_name + ' from ' + l.start_time + ' to ' + l.end_time;
					});
					$("#failedModal .modal-body").html(message);
					$("#failedModal").modal('toggle');
				}
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}

function eventData(me, room, type)
{
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	var eid;
	$.ajax({
		type: "POST",
		url: "event_data.html",
		dataType: "json",
		data: 'event_id='+room,
		success: function(resp){
			if(resp.success == 1)    {
                if(type == 1){
                    eid = resp.data.event_id;
                    $("#roomList").val(resp.data.room_id);
                    $("#dateFrom").val(resp.data.provider_name);
                    $("#mEventName").val(resp.data.event_name);
                    $("#personAddon").html(resp.data.room_capacity);
                    $("#dateFrom").val(resp.data.event_date);
                    $("#timeFrom").val(resp.data.event_start);
                    $("#timeUntil").val(resp.data.event_end);
                    $("#evid").val(resp.data.event_id);
                    $("#destination").val(resp.data.destination);
                    $('#editActivityModal').modal('show');
                    $.each(resp.data.opt, function(i, obj) {
                        if(obj.euser_value > 0){
                            $( "#optionList :checkbox[value="+obj.option_id+"]" ).prop("checked",true);
                        }
                    });
                    
                }else{
                    eid = resp.data.event_id;
                    var date = new Date(resp.data.event_date);
                    $("#iUserName").html(resp.data.display_name);
                    $("#iRoomName").html(resp.data.room_name);
                    $("#iEventName").html(resp.data.event_name);
                    $("#personAddon").html(resp.data.room_capacity);
                    $("#iBookDate").html(date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear());
                    $("#iTimeFrom").html(resp.data.event_start);
                    $("#iTimeUntil").html(resp.data.event_end);
                    $("#iPax").html(resp.data.room_pax);
                    $('#infoActivityModal').modal('show');
                    $.each(resp.data.opt, function(i, obj) {
                        if(obj.euser_value > 0){
                            $( "#iOptionList :checkbox[value="+obj.option_id+"]" ).prop("checked",true);
                        }
                    });
                    
                }
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
            last_room = room;
		},
	});
	return false;
}
