$(document).ready(function(){
	$("#newRoom").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#newRoom");
		submitNewRoom(me, room);
	});
	$("#editRoom").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#editRoom");
		submitEditRoom(me, room);
	});
	$(".editRoom").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		roomData(me, room);
	});
	$(".deleteRoom").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		submitDeleteRoom(me, room);
	});
});
function submitNewRoom(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_new_room.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				$("#failedModal").toggle('modal');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function submitEditRoom(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_edit_room.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function roomData(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "room_data.html",
		dataType: "json",
		data: 'room_id='+room,
		success: function(resp){
			if(resp.success == 1)    {
				$("#eRoomName").val(resp.data.room_name);
				$("#eRoomID").val(resp.data.room_id);
				$("#eDriverName").val(resp.data.moderator_name);
				$("#eRoomCapacity").val(resp.data.room_capacity);
				$("input[type=radio]").each(function(){
				   if($(this).val() === resp.data.room_color){
					  $(this).prop('checked', true);
				   }
				});
				$('#editRoomModal').modal('show');
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
