/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - sushitei_pestcontrol
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_pestcontrol` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sushitei_pestcontrol`;

/*Table structure for table `ticket_category` */

DROP TABLE IF EXISTS `ticket_category`;

CREATE TABLE `ticket_category` (
  `category_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` char(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_parent_id` tinyint(2) unsigned DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `work_time` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `category_parent_id` (`category_parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_category` */

insert  into `ticket_category`(`category_id`,`category_name`,`category_parent_id`,`description`,`work_time`) values 
(1,'All',0,'Kategori Utama',0),
(2,'Terlihat Customer',1,'Terlihat customer',0),
(3,'Terlihat Karyawan',1,'Terlihat karyawan',0),
(4,'Permintaan QA',1,'Permintaan QA',0),
(5,'Pest tertangkap',2,'Pest tertangkap',0),
(6,'Pest tidak tertangkap',2,'Pest tidak tertangkap',0),
(7,'Pest tertangkap',3,'Pest tertangkap',0),
(8,'Pest tidak tertangkap',3,'Pest tidak tertangkap',0),
(9,'-',4,'-',0);

/*Table structure for table `ticket_log` */

DROP TABLE IF EXISTS `ticket_log`;

CREATE TABLE `ticket_log` (
  `ticketlog_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticketlog_title` text NOT NULL,
  `ticket_id` mediumint(11) unsigned DEFAULT NULL,
  `user_id` smallint(3) unsigned NOT NULL,
  `ticketlog_type` varchar(20) NOT NULL,
  `ticketlog_content` text NOT NULL,
  `ticketlog_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticketlog_show` tinyint(1) NOT NULL,
  PRIMARY KEY (`ticketlog_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_log` */

insert  into `ticket_log`(`ticketlog_id`,`ticketlog_title`,`ticket_id`,`user_id`,`ticketlog_type`,`ticketlog_content`,`ticketlog_time`,`ticketlog_show`) values 
(10,'Admin IT Test Created a new Ticket',6,6797,'Submit','Hasil service: Ok\r\nRekomendasi: tidak ada\r\nNomor ticket desk MRP: 009\r\nNama pendamping outlet: siapa ya\r\nNama pendamping teknisi: siapa kek                               ','2020-02-18 13:42:58',1),
(11,'Fina Alfiatul Jannah Edited the ticket detail.',6,6672,'Edit Detail','test edit','2020-02-18 13:55:39',1),
(12,'Fina Alfiatul Jannah Edited the ticket detail.',6,6672,'Edit Detail','test edit','2020-02-18 13:56:11',1),
(13,'Fina Alfiatul Jannah Edited the ticket detail.',6,6672,'Edit Detail','test edit','2020-02-18 13:59:46',1),
(14,'Fina Alfiatul Jannah Commented this ticket',6,6672,'Comment','test comment','2020-02-18 14:00:41',1),
(15,'Fina Alfiatul Jannah Assigned Yussy',6,6672,'Assign','tes assign','2020-02-18 14:00:53',1),
(16,'Fina Alfiatul Jannah Taking over this ticket',6,6672,'Take Over','tes take over','2020-02-18 14:02:53',1),
(17,'Admin IT Test Edited the ticket detail.',6,6797,'Edit Detail','tes edit user','2020-02-18 14:29:34',1),
(18,'Admin IT Test Commented this ticket',6,6797,'Comment','tes user comment','2020-02-18 14:29:50',1),
(19,'Fina Alfiatul Jannah Edited the ticket detail.',6,6672,'Edit Detail','tes','2020-02-18 14:31:36',1),
(20,'Fina Alfiatul Jannah Commented this ticket',6,6672,'Comment','tesss','2020-02-18 14:31:40',1),
(21,'Admin IT Test Edited the ticket detail.',6,6797,'Edit Detail','tess','2020-02-18 14:31:46',1),
(22,'Admin IT Test Commented this ticket',6,6797,'Comment','iya','2020-02-18 14:31:51',1),
(23,'Admin IT Test Resolve this ticket.',6,6797,'Solved','tes solved','2020-02-18 14:50:20',1),
(24,'Fina Alfiatul Jannah Resolved this ticket',6,6672,'Closed','tes close','2020-02-18 15:06:35',1),
(25,'Fina Alfiatul Jannah Resolved this ticket',6,6672,'Closed','close','2020-02-18 15:11:07',1),
(26,'Admin IT Test Created a new Ticket',7,6797,'Submit','Hasil service: hasil ada tertangkap\r\nRekomendasi: tdk ada\r\nNomor ticket desk MRP: 088\r\nNama pendamping outlet: fina\r\nNama pendamping teknisi: teknisi 1                                           ','2020-02-18 16:21:27',1),
(27,'Fina Alfiatul Jannah Edited the ticket detail.',7,6672,'Edit Detail','edit tes','2020-02-18 16:36:01',1),
(28,'Fina Alfiatul Jannah Edited the ticket detail.',7,6672,'Edit Detail','data','2020-02-18 16:37:51',1),
(29,'Fina Alfiatul Jannah Taking over this ticket',7,6672,'Take Over','tes','2020-02-18 16:39:00',1),
(30,'Admin IT Test Resolve this ticket.',7,6797,'Solved','done','2020-02-18 16:39:48',1),
(31,'Admin IT Test Resolve this ticket.',6,6797,'Solved','sudah done','2020-02-18 16:48:38',1),
(32,'Admin IT Test Created a new Ticket',8,6797,'Submit','Hasil service:ok\r\nRekomendasi:boleh juga\r\nNomor ticket desk MRP:0213\r\nNama pendamping outlet: si anu\r\nNama pendamping teknisi: si ini                                   ','2020-02-18 16:50:16',1),
(33,'Admin IT Test Resolve this ticket.',8,6797,'Solved','sudah done ya.','2020-02-18 16:50:43',1),
(34,'Fina Alfiatul Jannah Taking over this ticket',8,6672,'Take Over','take over','2020-02-18 16:52:05',1);

/*Table structure for table `ticket_main` */

DROP TABLE IF EXISTS `ticket_main`;

CREATE TABLE `ticket_main` (
  `ticket_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_title` datetime NOT NULL,
  `ticket_type` tinyint(2) unsigned NOT NULL,
  `department_id` smallint(3) unsigned NOT NULL,
  `category` smallint(3) unsigned NOT NULL,
  `priority` smallint(3) unsigned NOT NULL,
  `impact` tinyint(1) unsigned NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `ticket_user` smallint(4) unsigned DEFAULT NULL,
  `ticket_creator` smallint(4) unsigned NOT NULL,
  `assigned_staff` smallint(4) unsigned DEFAULT NULL,
  `submit_date` datetime NOT NULL,
  `spare_part` varchar(50) NOT NULL,
  `problem_source` text NOT NULL,
  `resolved_date` datetime NOT NULL,
  `resolved_solution` text NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  PRIMARY KEY (`ticket_id`),
  KEY `ticketType` (`ticket_type`),
  KEY `ticketCat` (`category`),
  KEY `assigned_staff` (`assigned_staff`),
  KEY `ticket_user` (`ticket_user`),
  KEY `priority` (`priority`),
  KEY `ticket_type` (`ticket_type`),
  KEY `department_id` (`department_id`),
  KEY `ticket_creator` (`ticket_creator`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_main` */

insert  into `ticket_main`(`ticket_id`,`ticket_title`,`ticket_type`,`department_id`,`category`,`priority`,`impact`,`content`,`status`,`ticket_user`,`ticket_creator`,`assigned_staff`,`submit_date`,`spare_part`,`problem_source`,`resolved_date`,`resolved_solution`,`last_update`,`start_time`) values 
(6,'2020-02-17 00:00:00',2,0,9,0,1,'Hasil service: ok\r\nRekomendasi: tidak ada\r\nNomor ticket desk MRP: 009\r\nNama pendamping outlet: siapa ya\r\nNama pendamping teknisi: siapa kek    ',3,6797,6797,6672,'2020-02-18 13:42:58','','','2020-02-18 15:11:07','close','2020-02-18 16:48:38','2020-02-18 00:00:00'),
(7,'2020-02-14 00:00:00',2,0,5,0,1,'Hasil service: hasil ada tertangkap\r\nRekomendasi: tdk ada\r\nNomor ticket desk MRP: 088\r\nNama pendamping outlet: fina\r\nNama pendamping teknisi: teknisi 1                                           ',3,6797,6797,6672,'2020-02-18 16:21:27','','','0000-00-00 00:00:00','','2020-02-18 16:39:48','2020-02-18 00:00:00'),
(8,'2020-02-15 00:00:00',2,0,5,0,0,'Hasil service:ok\r\nRekomendasi:boleh juga\r\nNomor ticket desk MRP:0213\r\nNama pendamping outlet: si anu\r\nNama pendamping teknisi: si ini                                   ',1,6797,6797,0,'2020-02-18 16:50:16','','','0000-00-00 00:00:00','','2020-02-18 16:52:05','2020-02-18 00:00:00');

/*Table structure for table `ticket_status` */

DROP TABLE IF EXISTS `ticket_status`;

CREATE TABLE `ticket_status` (
  `status_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(25) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_status` */

insert  into `ticket_status`(`status_id`,`status_name`) values 
(1,'Open'),
(2,'On Progress'),
(3,'Solved'),
(4,'Closed'),
(5,'Re-Open'),
(6,'Force Closed');

/*Table structure for table `ticket_type` */

DROP TABLE IF EXISTS `ticket_type`;

CREATE TABLE `ticket_type` (
  `type_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(25) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_id` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_type` */

insert  into `ticket_type`(`type_id`,`type_name`) values 
(1,'Service Reguler'),
(2,'Red Alert');

/*Table structure for table `ticket_upload` */

DROP TABLE IF EXISTS `ticket_upload`;

CREATE TABLE `ticket_upload` (
  `upload_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `upload_name` varchar(50) NOT NULL,
  `upload_location` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_upload` */

insert  into `ticket_upload`(`upload_id`,`upload_name`,`upload_location`,`upload_time`,`ticket_id`) values 
(1,'2020-02-17_1445120.jpg','Resources/images/Ticket\\','2020-02-17 14:45:17',1),
(2,'2020-02-18_1114530.jpg','Resources/images/Ticket\\','2020-02-18 11:14:59',5),
(3,'2020-02-18_1114591.jpg','Resources/images/Ticket\\','2020-02-18 11:15:04',5),
(4,'2020-02-18_1342380.jpg','Resources/images/Ticket\\','2020-02-18 13:42:45',6),
(5,'2020-02-18_1342451.jpg','Resources/images/Ticket\\','2020-02-18 13:42:52',6),
(6,'2020-02-18_1342522.jpg','Resources/images/Ticket\\','2020-02-18 13:42:58',6);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
