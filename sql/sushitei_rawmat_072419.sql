/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - sushitei_portal
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_portal` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sushitei_portal`;

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `location_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `location_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `location_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Data for the table `location` */

insert  into `location`(`location_id`,`location_name`,`location_status`) values 
(1,'Jakarta',1),
(2,'Surabaya',1),
(3,'Makasar',1),
(4,'Palembang',1),
(5,'Bali',1),
(6,'Bandung',1),
(7,'Jogja',1),
(8,'Medan',1),
(9,'Pekan Baru',1),
(10,'Batam',1),
(11,'Bengkulu',1);

/*Table structure for table `login_token` */

DROP TABLE IF EXISTS `login_token`;

CREATE TABLE `login_token` (
  `token_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `token_name` varchar(100) DEFAULT NULL,
  `token_password` varchar(100) DEFAULT NULL,
  `token_user` smallint(5) unsigned NOT NULL,
  `token_expired_time` datetime DEFAULT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `login_token` */

/*Table structure for table `portal_permission` */

DROP TABLE IF EXISTS `portal_permission`;

CREATE TABLE `portal_permission` (
  `permission_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `permission_site` mediumint(6) unsigned NOT NULL,
  `permission_controller` varchar(50) DEFAULT NULL,
  `permission_method` varchar(50) DEFAULT NULL,
  `permission_name` varchar(50) DEFAULT NULL,
  `permission_type` tinyint(1) DEFAULT NULL COMMENT '0=url access;1=other access',
  `permission_url` varchar(250) DEFAULT NULL,
  `permission_description` varchar(50) DEFAULT NULL,
  `permission_icon` varchar(50) DEFAULT NULL,
  `permission_code` varchar(30) DEFAULT NULL,
  `permission_order` tinyint(3) unsigned DEFAULT '0',
  `show_on_menu` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

/*Data for the table `portal_permission` */

insert  into `portal_permission`(`permission_id`,`permission_site`,`permission_controller`,`permission_method`,`permission_name`,`permission_type`,`permission_url`,`permission_description`,`permission_icon`,`permission_code`,`permission_order`,`show_on_menu`) values 
(1,0,NULL,NULL,'Portal',1,NULL,'Access to Portal',NULL,'siteAccess',0,1),
(2,2,NULL,NULL,'Administrator',1,'administrator/home','Access to Administrator',NULL,'siteAccess',0,1),
(3,2,NULL,NULL,'Dashboard',0,'administrator/home','Dashboard',NULL,'accessAdminDashboard',1,1),
(4,2,NULL,NULL,'Manage User',0,'administrator/user','Manage User',NULL,'accessManageUser',1,1),
(6,3,NULL,NULL,'IT Helpdesk',1,'hd/it','Access to IT Helpdesk',NULL,'siteAccess',0,1),
(7,2,NULL,NULL,'Manage Role',0,'administrator/role','Manage Role',NULL,'accessManageRole',1,1),
(8,4,NULL,NULL,'Room Booking',1,'rsv/room','Access to Room Booking',NULL,'siteAccess',0,1),
(12,2,NULL,NULL,'Manage Site',0,'administrator/site','Manage Site',NULL,'accessManageSite',1,1),
(13,4,NULL,NULL,'Room Booking',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(14,5,NULL,NULL,'MRP Helpdesk',1,'hd/mrp','Access to MRP Helpdesk',NULL,'siteAccess',0,1),
(15,6,NULL,NULL,'GA Helpdesk',1,'hd/ga','Access to GA Helpdesk',NULL,'siteAccess',0,1),
(16,9,NULL,NULL,'PRP',1,'mt/prp','Access to PRP',NULL,'siteAccess',0,1),
(17,10,NULL,NULL,'Car Booking',1,'rsv/car','Access to Car Booking',NULL,'siteAccess',0,1),
(18,5,NULL,NULL,'MRP Helpdesk',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(20,4,NULL,NULL,'Room Booking',1,NULL,'Receive Email',NULL,'receiveEmail',3,0),
(21,3,NULL,NULL,'IT Helpdesk',1,NULL,'Receive Email',NULL,'receiveEmail',3,0),
(22,6,NULL,NULL,'GA Helpdesk',1,NULL,'Receive Email',NULL,'receiveEmail',3,0),
(23,3,NULL,NULL,'IT Helpdesk',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(24,6,NULL,NULL,'GA Helpdesk',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(25,5,NULL,NULL,'MRP Helpdesk',1,NULL,'Receive Email',NULL,'receiveEmail',3,0),
(26,9,NULL,NULL,'PRP',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(27,10,NULL,NULL,'Car Booking',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(28,10,NULL,NULL,'Car Booking',1,NULL,'Receive Email',NULL,'receiveEmail',3,0),
(29,7,NULL,NULL,'PQNC',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(30,8,NULL,NULL,'SLNC',1,NULL,'Administrator',NULL,'isAdmin',2,0),
(31,7,NULL,NULL,'PQNC',1,'hd/pqnc','Access to PQNC',NULL,'siteAccess',0,1),
(32,8,NULL,NULL,'SLNC',1,'hd/slnc','Access to SLNC',NULL,'siteAccess',0,1),
(33,11,NULL,NULL,'Rawmat',1,'hd/rawmat','Access to Rawmat',NULL,'siteAccess',0,1),
(34,11,NULL,NULL,'Rawmat',1,NULL,'Administrator',NULL,'isAdmin',2,0);

/*Table structure for table `portal_sites` */

DROP TABLE IF EXISTS `portal_sites`;

CREATE TABLE `portal_sites` (
  `site_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_name` varchar(50) DEFAULT NULL,
  `site_status` tinyint(1) DEFAULT NULL,
  `site_color` varchar(15) DEFAULT NULL,
  `site_icon` varchar(15) DEFAULT NULL,
  `site_group` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Data for the table `portal_sites` */

insert  into `portal_sites`(`site_id`,`site_name`,`site_status`,`site_color`,`site_icon`,`site_group`) values 
(1,'Portals',1,'bg-primary','fa-tachometer',1),
(2,'Administrator',1,NULL,'fa-cog',1),
(3,'IT Helpdesk',1,'bg-warning','fa-magic',2),
(4,'Room Reservation',1,'bg-success','fa-bell',5),
(5,'MRP Helpdesk',1,'bg-info','fa-cog',2),
(6,'GA Helpdesk',1,'bg-success','fa-book',2),
(7,'PQNC',1,'bg-success','fa-check-square',3),
(8,'SLNC',1,'bg-danger','fa-check-circle',3),
(9,'PRP',1,'bg-warning','fa-check-square',4),
(10,'Car Reservation',1,'bg-info','fa-car',5),
(11,'Rawmat',1,'bg-success','fa-check',4);

/*Table structure for table `portal_store` */

DROP TABLE IF EXISTS `portal_store`;

CREATE TABLE `portal_store` (
  `store_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `store_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `store_location` smallint(3) unsigned NOT NULL,
  `store_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `store_phone` varchar(18) CHARACTER SET utf8 DEFAULT NULL,
  `store_email` varchar(50) DEFAULT NULL,
  `store_status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`store_id`),
  KEY `location_id` (`store_location`),
  CONSTRAINT `storeLink` FOREIGN KEY (`store_location`) REFERENCES `store_location` (`location_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8mb4;

/*Data for the table `portal_store` */

insert  into `portal_store`(`store_id`,`store_name`,`store_location`,`store_address`,`store_phone`,`store_email`,`store_status`) values 
(1,'Plaza Indonesia',1,NULL,NULL,NULL,1),
(2,'Plaza Senayan',1,NULL,NULL,NULL,1),
(3,'Pondok Indah Mall 2',1,NULL,NULL,NULL,1),
(4,'Senayan City',1,NULL,NULL,NULL,1),
(5,'Mall Kelapa Gading',1,NULL,NULL,NULL,1),
(6,'Emporium Pluit',1,NULL,NULL,NULL,1),
(7,'Central Park',1,NULL,NULL,NULL,1),
(8,'Gandaria City',1,NULL,NULL,NULL,1),
(9,'Flavour Bliss',1,NULL,NULL,NULL,1),
(10,'Supermall Karawaci',1,NULL,NULL,NULL,1),
(11,'Grand Indonesia',1,NULL,NULL,NULL,1),
(12,'Lotte Avenue',1,NULL,NULL,NULL,1),
(13,'Kota Kasablanka',1,NULL,NULL,NULL,1),
(14,'Summarecon Bekasi',1,NULL,NULL,NULL,1),
(15,'Bintaro Xchange',1,NULL,NULL,NULL,1),
(16,'The Breeze',1,NULL,NULL,NULL,1),
(17,'Puri Mall',1,NULL,NULL,NULL,1),
(18,'Summarecon Serpong',1,NULL,NULL,NULL,1),
(99,'HO',1,NULL,NULL,NULL,1),
(100,'Sushi Kiosk Puri',1,NULL,NULL,NULL,1),
(101,'Sushi Kiosk Sentul',1,NULL,NULL,NULL,1),
(102,'Sushi Kiosk Botani',1,NULL,NULL,NULL,1),
(103,'Sushi Kiosk Grand Indones',1,NULL,NULL,NULL,1),
(104,'Sushi Kiosk WTC 2',1,NULL,NULL,NULL,1),
(105,'Central Kitchen',1,NULL,NULL,NULL,1),
(106,'ST BBX',1,NULL,NULL,NULL,0),
(107,'Warehouse',1,NULL,NULL,NULL,1),
(108,'Kemang Village',1,NULL,NULL,NULL,1),
(109,'Margo City',1,NULL,NULL,NULL,1),
(110,'Sushitei Palembang',4,NULL,NULL,NULL,1),
(111,'Sushi Tei Bali',5,NULL,NULL,NULL,1),
(112,'Citywalk',1,NULL,NULL,NULL,1),
(113,'City Walk Sudirman',1,NULL,NULL,NULL,1),
(114,'Ciputra Cibubur',1,NULL,NULL,NULL,1),
(115,'Sushi Tei Pakuwon',2,NULL,NULL,NULL,1),
(116,'Trans Studio Bandung',6,NULL,NULL,NULL,1),
(117,'Tomo Sushi Grand Indonesi',1,NULL,NULL,NULL,1),
(118,'Tom Sushi MI',1,NULL,NULL,NULL,1);

/*Table structure for table `portal_user` */

DROP TABLE IF EXISTS `portal_user`;

CREATE TABLE `portal_user` (
  `user_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `store_id` smallint(3) unsigned NOT NULL,
  `department_id` smallint(3) unsigned NOT NULL,
  `role_id` smallint(2) unsigned NOT NULL,
  `employee_id` mediumint(8) unsigned zerofill NOT NULL,
  `security_question` tinyint(1) unsigned NOT NULL,
  `security_answer` varchar(40) NOT NULL,
  `user_status` tinyint(1) unsigned NOT NULL,
  `password_md5` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email` (`email`),
  KEY `group_id` (`role_id`),
  KEY `employee_id` (`employee_id`),
  KEY `store_id` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6780 DEFAULT CHARSET=utf8;

/*Data for the table `portal_user` */

insert  into `portal_user`(`user_id`,`user_name`,`user_password`,`display_name`,`email`,`store_id`,`department_id`,`role_id`,`employee_id`,`security_question`,`security_answer`,`user_status`,`password_md5`) values 
(6610,'adminhrd1.jkt@sushitei.co.id','','adminhrd1.jkt@sushitei.co.id','adminhrd1.jkt@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6611,'creative.spv@sushitei.co.id','','creative.spv@sushitei.co.id','creative.spv@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6612,'dodi@sushitei.co.id','','dodi@sushitei.co.id','dodi@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6613,'maria@sushitei.co.id','','maria@sushitei.co.id','maria@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6614,'finance3.jkt@sushitei.co.id','','finance3.jkt@sushitei.co.id','finance3.jkt@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6615,'finance4.jkt@sushitei.co.id','','finance4.jkt@sushitei.co.id','finance4.jkt@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6616,'ismail@sushitei.co.id','','ismail@sushitei.co.id','ismail@sushitei.co.id',10000,0,2,00000000,0,'',0,NULL),
(6617,'it5.jkt@sushitei.co.id','','it5.jkt@sushitei.co.id','it5.jkt@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6618,'operationst.jkt@sushitei.co.id','','operationst.jkt@sushitei.co.id','operationst.jkt@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6619,'rika@sushitei.co.id','','rika@sushitei.co.id','rika@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6620,'training1.jkt@sushitei.co.id','','training1.jkt@sushitei.co.id','training1.jkt@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6621,'yennita@sushitei.co.id','','yennita@sushitei.co.id','yennita@sushitei.co.id',10000,0,2,00000000,0,'',1,NULL),
(6641,'lila','$2y$09$GtHLe4wUcdbtmln/538m../7oLMcyLZmDHBWRGQDv/KX0KfVUuuye','lila rahma','adminfb@sushitei.co.id',99,0,4,00000000,3,'zulviarty usman',1,NULL),
(6642,'dede','$2y$09$D./TZiUHyiojvytsgbC9hetJqkncPBC94sP5PMq4uwSpyxERQwGdS','Dede Mardinah','adminfbspv@sushitei.co.id',99,0,7,00000000,1,'PI',1,NULL),
(6643,'cnb','$2y$09$SM63XQ9nMBbCI91HDK5fBOElx3FizItiSrzVxqhq5HXep8.NETKtG','cnb','adminhrd4.jkt@sushitei.co.id',99,0,4,00000000,1,'HO',1,NULL),
(6644,'dhani','$2y$09$XG/.RJOHRUqEZrBZhv9CV.cdWXsKxrSoqfr1O8iRSZzokonqHiBJ2','Dhani','adminhrd5.jkt@sushitei.co.id',99,0,4,00000000,0,'',1,NULL),
(6645,'alfin','$2y$09$SZP1TAD08F99rUzXOc8Gp./KvFaYIegRr/eADvpJxbMlKOgc4CeSi','Alfin','alfin@sushitei.co.id',99,0,5,00000000,1,'Plaza Indonesia',1,NULL),
(6646,'any.puspa','$2y$09$N5bH9VLDBYn3g7mcLaqhjecL.AkaeNLQ5wShTi7HKnW0juFtMFR6C','Any Puspa','any@sushitei.co.id',99,0,4,00000000,1,'HO',1,NULL),
(6647,'st.bandung','$2y$09$hO4AhCxThZqDgz4ba8c2.ehIqu/caQBSReZid2wL7nXVD0KAgNrmu','Sushi Tei Bandung','budi@sushitei.co.id',116,0,4,00000000,0,'',1,NULL),
(6648,'hck.pqnc','$2y$09$xnB5rp9atoit9qSaOhLUbOsJtu8jbaFP9AwPQFJw87/PgblXQMfRq','HCK','central.kitchen@sushitei.co.id',105,0,13,00000000,0,'',1,NULL),
(6649,'febriarbi','$2y$09$TX49Vw816BGrzA4jpo4fFuq2ORtoMcoyl2HMvOCK.aUrGatQIMhAy','Febriansyah Arbi','costcontrol3.jkt@sushitei.co.id',99,0,4,00000000,1,'HO',1,NULL),
(6650,'dcr1.jkt','$2y$09$FlAmJBB6UlpME1ZxCzMpd.duYRSXbRT2OaIEo0SA7CrYod5qris.y','DCR','dcr1.jkt@sushitei.co.id',99,0,8,00000000,0,'',1,NULL),
(6651,'devi','$2y$09$qS9oDNzO9LmxkEIaRCdvOeamGL8jZ2Btl1959/8MMIi94IdqErDbu','Raden Roro Devi Suwanti','devi@sushitei.co.id',99,0,7,00000000,0,'',1,NULL),
(6652,'dwi.purnomo','$2y$09$QKKwYadMzMzs35sUQ1/1iOmk7xqfbnVdAH9yrDGHWeBKCiUKsRiAS','Dwi Purnomo','dwi.purnomo@sushitei.co.id',99,0,7,00000000,1,'MKG',1,NULL),
(6653,'ga','$2y$09$TSZuQqxedyHf/vVKTjg1seXrLqsiMQFCqLWZPMbW4I/AwOpYRy6dC','Rijal','ga1.jkt@sushitei.co.id',99,0,11,00000000,1,'123',1,NULL),
(6654,'tami','$2y$09$ntHFIWqPWYS2zIPLZB10p.guNf4hUWPDrEugeiZQKWGH7zp0SIltq','Tami Fitria','ga2.jkt@sushitei.co.id',99,0,11,00000000,0,'',1,NULL),
(6655,'hadi.chandra','$2y$09$RPYUmnTSIN3XfKXjsWT05OMhx5eAYJi/0f5PbBV2fuRl0NJVLpOhK','Hadi Chandra','hadi.chandra@sushitei.co.id',99,0,5,00000000,4,'ayam',1,NULL),
(6656,'happy.nurlia','$2y$09$ME9HdoitfTeiNP73YRFsN.8sgMHfvdhc4mVrmkR3iKVT6uf34NT9S','Happy Nurlia','happy.nurlia@sushitei.co.id',99,0,4,00000000,1,'marketing',1,NULL),
(6657,'headsk1','$2y$09$q7uQyaQMPp5L/yoJo2xn5.1iiwYXTmJuBnXmveil9GfALd/ZeuSn6','Andhika Kusuma','headsk1@sushitei.co.id',99,0,12,00000000,0,'',1,NULL),
(6658,'headsk2','$2y$09$nVPvlIwqFQcrXxPLNPh3LuTMU56A3cV0ZMBbPB7PqVu44Fx7hcNT6','Dwi Kristanto','headsk2@sushitei.co.id',99,0,12,00000000,0,'',1,NULL),
(6659,'headsk3','$2y$09$sV47wovk3YuB/gZ4vjfHAuPA.IkbpTDiLBe/Ev0cfvYx5Jl75vG1G','Fadilah Hakim','headsk3@sushitei.co.id',99,0,12,00000000,0,'',1,NULL),
(6660,'headsk4','$2y$09$wpgfAenfn00NVCpjT8mdAO6vosMrmigohy/HbY23U2X/knwP4bv5S','Heri Kiswanto','headsk4@sushitei.co.id',99,0,12,00000000,0,'',1,NULL),
(6661,'headsk5','$2y$09$lTCJWjC3ATu9D4Y1vqQr5eoMQkw9RoMXc7dz6WpEpu2XpA.wslpIa','Slamet Asroi','headsk5@sushitei.co.id',99,0,12,00000000,1,'HO',1,NULL),
(6662,'indra','$2y$09$uoOQoXfP0hKUAg9qWmwqN.IessG/WNmCc/4bLtsbtt4HQ2pPnuVYC','Indra','indra@sushitei.co.id',99,0,7,00000000,0,'',1,NULL),
(6663,'daniellintong','$2y$09$naXUUv9SmORZmFHwiO49yeUG52s4I803T7qH3NZharqCqq6Sb0A5m','Daniel Lintong','intaudit1.jkt@sushitei.co.id',99,0,4,00000000,1,'HO',1,NULL),
(6664,'Arifuzaki','$2y$09$yhevAOcRvXOhQXogiEhvNeJSiPuBl65c1QoZDhigEUIBbr10v07gy','Arifuzaki','intauditspv2.jkt@sushitei.co.id',99,0,4,00000000,1,'HO',1,NULL),
(6665,'isapp','$2y$09$VtnqbpMCxtVWUPGF0x.K0urwLKoKIkQbtpkanQgXTPnPVhHDd4Wmi','ISA PP','isa.pp@sushitei.co.id',123,0,4,00000000,1,'pasific place',1,NULL),
(6666,'admin.isa','$2y$09$.ItpUo2ktsgfc5rwu/z3au.nIAmLO9ahvoDuhrN3iGdEV9Y7TL/LO','Admin ISA','isa2.jkt@sushitei.co.id',99,0,4,00000000,0,'',1,NULL),
(6667,'it.st.plg','$2y$09$Jk/YJnkvXLyUuKR3DazpkuCxLQFpHHePId.uc.KnWflaC0uuXyF6m','Sushi Tei Palembang','it.st.plg@sushitei.co.id',110,0,4,00000000,0,'',1,NULL),
(6668,'rahmats','$2y$09$PJAwt6fANw4SRaJNP/p7IuvCn5RARQF4w9KJDNQTww1RzTzhHZ55K','Rahmat Samsudin','it1.jkt@sushitei.co.id',99,0,1,00009311,2,'rocky',1,NULL),
(6669,'dessy','$2y$09$65zrNXW5Ymu5DPgZtRKiAO1hmRbQ6aZscUChl3vQ7KJHX4zcFT4fC','Dessy Anggoro Putri','it2.jkt@sushitei.co.id',99,0,10,00000000,0,'',1,NULL),
(6670,'it3.jkt','$2y$09$5kkHVmVPHWJL96m78pspY.4O.n30Lzuzz8fN.iX//pKUkG8cQq73m','Hendra','it3.jkt@sushitei.co.id',99,0,1,00000000,0,'',1,NULL),
(6671,'eka.nuari','$2y$09$FB/8TeA/2S9OLdEEZyh3a.fL/kH6uG1mgBUEEKEf794oFCRbaJAiK','Eka Nuari','it4.jkt@sushitei.co.id',99,0,10,00000000,0,'',1,NULL),
(6672,'it6.jkt@sushitei.co.id','$2y$09$SLbawdvxHKBRbq812e4DBu8dbq.jEnnnmcpXFasxR.Xny5k6sBYtm','Fina Alfiatul J','it6.jkt@sushitei.co.id',99,0,10,00000000,1,'HO',1,NULL),
(6673,'herusartono','$2y$09$neKraZYzbGT4ZP5DjByZZe3rfdffFP0QPb61/M6/sB5bk3UYAMtoi','Heru Sartono','it7.jkt@sushitei.co.id',99,0,10,00000000,2,'heru',1,NULL),
(6674,'wisnu','$2y$09$uCtiyRljITne5aEDMWIGwechRQzGzDSUSv4TU4SKEtoT081kKyRpe','Wisnu Setiawan','it8.jkt@sushitei.co.id',99,0,10,00000000,2,'wisnu ganteng',1,NULL),
(6675,'prabu','$2y$09$eyysozk51M3OUbVme2.jKOAXcP7qg0YNiHh01iogrPRCxfWfPzHyi','Prabu Fajar','it9.jkt@sushitei.co.id',99,0,10,00000000,2,'spiderman',1,NULL),
(6676,'eva.kopber','$2y$09$li5XdU3A8MCejs/qqAGgsuHIoNtRt/MGsI2LPwtUbbVVXjMehhLpu','Eva Koperasi Berkat','ketua.kopber@gmail.com',99,0,4,00000000,1,'HO',1,NULL),
(6677,'kriswahyuda','$2y$09$TqeSD65fQL74KfIa6rF4FOJllFYif8kkDDcVmkIOM7WB.IfIZ0nYa','Kris Wahyuda','kris@sushitei.co.id',99,0,4,00000000,3,'agnes',1,NULL),
(6678,'kris.h','$2y$09$63FEczKhtlLECqz4bZfTr.2NYHrkxSNf/WXW8aZ9ipndjr0lgK1Vq','Krishandaya','krishandaya@sushitei.co.id',99,0,1,00000000,0,'',1,NULL),
(6679,'legal.jkt','$2y$09$XaV.8lyssNvJLkkMOwVI3OPx21a2.jy8ncEfESbQRoFSUK/zUpZbW','Setyorini','legal.jkt@sushitei.co.id',99,0,4,00000000,0,'',1,NULL),
(6680,'qa.spv3','$2y$09$k1JO1Fe4w3xRXekIX5/3UugkzaWxkGnz.HwKncHVvdy3j7kWenRUm','Levi','levi@sushitei.co.id',99,0,7,00000000,0,'',1,NULL),
(6681,'luciana','$2y$09$fOUOGulcJ//evoQgr1Bn7.K5s2ZJ7t2qXZk9RgUcr4SFgZWzm4/ue','Luciana','luciana@sushitei.co.id',99,0,4,00000000,1,'fat',1,NULL),
(6682,'michael','$2y$09$K5hDAeDSEXV.5YmTzVK3TOp/4Fon1i0bbp0.rfVUT9IxgwUOBJ6Fy','michael','m.chandra@sushitei.co.id',99,0,8,00000000,1,'PI',1,NULL),
(6683,'manager.bx','$2y$09$J9IydqWcCYxalP25N1deR.T7BUIJy0RIUV/zNttOVvSVY5.tozoze','Sushitei Bintaro Xchange','manager.bx@sushitei.co.id',15,0,15,00000000,0,'',1,NULL),
(6684,'manager.cp','$2y$09$pzwgMHGozB/Y2Kr19OIxtudpE2pKGzFb2PY75lk/VxKnv2udj4WXS','Sushitei Central Park','manager.cp@sushitei.co.id',7,0,15,00000000,0,'',1,NULL),
(6685,'manager.cwk','$2y$09$esX7OyhP4utgEKYebTBa1edty64uBPOJHGAOCYxO4QiKiCBv3vlnO','Sushitei Citywalk','manager.cwk@sushitei.co.id',112,0,15,00000000,0,'',1,NULL),
(6686,'manager.ep','$2y$09$CRCAtdkYWNcNXNFyFackiuPZ1J5ykya7FqcFQjyAcmUx8pLWOUkWO','Sushitei Emporium Pluit','manager.ep@sushitei.co.id',6,0,15,00000000,0,'',1,NULL),
(6687,'manager.fs','$2y$09$On7OEAK7pUWoEwHwwQToD.lEXBHukO13.A/98wJytMTg7NEu1odyq','Sushitei Flavour Bliss','manager.fs@sushitei.co.id',9,0,15,00000000,0,'',1,NULL),
(6688,'manager.gc','$2y$09$mMw6Ip0bixOJlvTqv2H.rufQTB94etxpW9Nx.UJPO0X7hf8sT//FS','Sushitei Gandaria City','manager.gc@sushitei.co.id',8,0,15,00000000,0,'',1,NULL),
(6689,'manager.ggp','$2y$09$M2NmE1PviO39armOV9OW2OOunOrJ/dpMp4DZ/ymMRzqV2QYfadmZq','Tom Sushi GGP','manager.ggp@tomsushi.co.id',124,0,15,00000000,0,'',1,NULL),
(6690,'manager.gi','$2y$09$43uEzNKE2wJC7AoVKnhwAOt4dPXL/tqlzuHPrrVOgW4rIPmKw5gA2','Sushitei Grand Indonesia','manager.gi@sushitei.co.id',11,0,15,00000000,0,'',1,NULL),
(6691,'tomsushi.gi','$2y$09$e88Q1MNUp7jvWGURjmNFh.5szns2tTQst58op88CLn1shFdQDGg7u','Manager Tom Sushi GI','manager.gi@tomsushi.co.id',117,0,15,00000000,0,'',1,NULL),
(6692,'manager.kw','$2y$09$xlEAoNCNB6TiMysxsMlB8eLhWGkdsVfbQh4nFBWRPIgYRMkU1cfi.','Sushitei Karawaci','manager.karawaci@sushitei.co.id',10,0,15,00000000,0,'',1,NULL),
(6693,'manager.kk','$2y$09$NaO2I5KFsZE3fEQMss0nBu0cD1oo2svUBGxMyrJDNV.Ob0xuLOQ62','Sushitei Kota Kasablanka','manager.kk@sushitei.co.id',13,0,15,00000000,0,'',1,NULL),
(6694,'manager.kv','$2y$09$WbT5aZ7WEnEjko5xzfx/TeOWeBxr0WpuYU.jEWGp6/2upmzmGhKe.','Sushitei Kemang Village','manager.kv@sushitei.co.id',108,0,15,00000000,0,'',1,NULL),
(6695,'manager.la','$2y$09$M.KX3LVvtJqqYWaDWlM0WunHUpc1.vShkR7uhx2Phz2qusK1IwdhO','Sushitei Lotte Avenue','manager.la@sushitei.co.id',12,0,15,00000000,0,'',1,NULL),
(6696,'manager.mc','$2y$09$kN0yN.GObaVr0xHS0DcGueXSCVcX0d/ws0z/ntx1mQzifR09JHqCy','Sushi Tei Margo City','manager.mc@sushitei.co.id',109,0,15,00000000,0,'',1,NULL),
(6697,'manager.mcc','$2y$09$syJI7BR7HqE6Hzbyqj/JJ.Lg.RmYQ6zfAfC3INwIMw8xBs4o4LGOa','Sushitei Ciputra Cibubur','manager.mcc@sushitei.co.id',114,0,15,00000000,0,'',1,NULL),
(6698,'manager.mkg','$2y$09$1AcTlj/PqrXs/VQXzgag4uDJ5ZwqjMe8boM.qCI3Us/7y9ttwjYrG','Sushitei Kelapa Gading','manager.mkg@sushitei.co.id',5,0,15,00000000,0,'',1,NULL),
(6699,'manager.moi','$2y$09$qRtQaTALZFCM.DKpip8XQ.1cong4pkmaza2HgQr6NrKWitNjE0DtK','Tom Sushi MOI','manager.moi@tomsushi.co.id',118,0,15,00000000,0,'',1,NULL),
(6700,'manager.pi','$2y$09$P5tMbU/tkWfNSJGJrAVhkOQbQTXVAAxH4StG7HFeebQm1krmU/DF6','Sushitei Plaza Indonesia','manager.pi@sushitei.co.id',1,0,15,00000000,0,'',1,NULL),
(6701,'manager.pim','$2y$09$SYLHQ.5aMrNAmpxCnTErHuj9hINBxpmYDCdBRDHTcdGubFKgfXyuW','Sushitei PIM','manager.pim@sushitei.co.id',3,0,15,00000000,0,'',1,NULL),
(6702,'manager.pm','$2y$09$8Fa46GKCeWASYx3SMQN2PuKxqhPtR3rvq6Wpwe.cRMEfeanXfeBHm','Sushitei Puri Mall','manager.pm@sushitei.co.id',17,0,15,00000000,0,'',1,NULL),
(6703,'manager.pp','$2y$09$HlmIIEpN2t9xUAYSRoiDUebnpB2xwlUCnnyLLheT9Qt.pQON5zuZm','Sushitei Pacific Place','manager.pp@sushitei.co.id',123,0,15,00000000,0,'',1,NULL),
(6704,'manager.ps','$2y$09$i49lFhiqQLwWYn8Nhxlpfex15nQTa0EcPUbaKoix1FF5AEvsZOkvS','Sushitei Plasa Senayan','manager.ps@sushitei.co.id',2,0,15,00000000,0,'',1,NULL),
(6705,'tspv','$2y$09$Q724JBsHqeCObsUVuVn6QOOZRqdTZVP.LoYL/2ykIV2RDAMv0ExTm','Tom Sushi Pluit Village','manager.pv@tomsushi.co.id',127,0,15,00000000,0,'',1,NULL),
(6706,'manager.smb','$2y$09$08d497BaJBwhzzLIvDWBJuP3RbtI6fVewXvDlgZ3wrN8oBc3EaaYm','Sushitei Summarecon Bekasi','manager.sb@sushitei.co.id',14,0,15,00000000,0,'',1,NULL),
(6707,'manager.sency','$2y$09$6foop3T9.Yy9Z3BRWw4XUeBN5q6DWO2C9Bj995HEAnLLnLGBuY0Je','Sushitei Senayan City','manager.senci@sushitei.co.id',4,0,15,00000000,0,'',1,NULL),
(6708,'manager.sms','$2y$09$Tq6J11b5eKcVDoxuQonAYO2Qq/A30b3BZtDZGzym1ac3eS9pqKMQG','Sushitei Summarecon Serpong','manager.sms@sushitei.co.id',18,0,15,00000000,0,'',1,NULL),
(6709,'manager.tb','$2y$09$jJuRznizjcVfdLfh9jN0lOeU1jhUmxvCBamLdmSOHYykbJudFGIJ2','Sushitei The Breeze','manager.tb@sushitei.co.id',16,0,15,00000000,0,'',1,NULL),
(6710,'manager.tsc','$2y$09$xxAuX1AHmwOkLY72JuaM0u7PHaVmQyUIbpDASqCNG.zollGy7GTB2','Sushi Tei TSC','manager.tsc@sushitei.co.id',125,0,15,00000000,0,'',1,NULL),
(6711,'mega','$2y$09$1wKv7S9l3x0Z1toEYY.mXeoK/wmI6nIZNIz9A5OlItZDK5i2C2bHm','mega','mega@sushitei.co.id',99,0,4,00000000,1,'PI',1,NULL),
(6712,'RifqiAlwafi','$2y$09$E47gtSyQ31d8HTpSHNtI/eD4dJFi2QJDJwz5WagcEA6WO/8fQ7oLK','Rifqi Alwafi','mkt_sk@sushi-kiosk.com',99,0,4,00000000,3,'Budi',1,NULL),
(6713,'heriherman','$2y$09$qo8B7qYIN.9x0D.vPAQNlOz7fn0j4wYXYc06UXKVTjT0mb3/Hnq.O','Heri Hermansyah','mrp.helpdesk@sushitei.co.id',99,0,16,00000000,0,'',1,NULL),
(6714,'MRP.SPV','$2y$09$VwMR6BNp4qmojCWXu.7/AOEWOPbqhl8zL6CocRIEZMt0foJh4xBhe','Michael Bruno','mrp.spv@sushitei.co.id',99,0,5,00000000,0,'',0,NULL),
(6715,'admin.mrp1','$2y$09$UuMIlJcfxWd8ElhUt5PG0.t2v3fnNaiatCHE4bZ6d0CkqOV8Xo0hy','Admin MRP','mrp1.jkt@sushitei.co.id',99,0,5,00000000,0,'',1,NULL),
(6716,'admin.mrp2','$2y$09$weMFGyBCb7FMY/rUl6jiz.QmGL0.ooGIC4ppGGFnrQuYrR9vZt7iO','Rudi (MRP-2)','mrp2.jkt@sushitei.co.id',99,0,5,00000000,0,'',1,NULL),
(6717,'venda','$2y$09$jAyYi5vLYmiSr2lpqbeVhemzYjSnuwNjBGEoFQjyzXHD1Szy.c0EG','Nurvenda Lafian','ppfbspv2@sushitei.co.id',99,0,12,00000000,3,'nunung',1,NULL),
(6718,'fahmi','$2y$09$QVCzlwdHZPc1G15ut8w.x.NU3JJ2JuVW5yOe5BOdRSVG8sE/yk14a','fahmi','ppfbspv@sushitei.co.id',99,0,14,00000000,2,'amy',1,NULL),
(6719,'purchasing.slnc','$2y$09$Fyt21vyiZP.I2XY5zRIrA.e.q5euT1KMxjdn1YYZBmIZbXT2NBqse','Purchasing Team','purchteam.jkt@sushitei.co.id',99,0,13,00000000,0,'',1,NULL),
(6720,'sk.puri','$2y$09$a3yy9l/aUVWEiobzzCRvUOYpNM7VPi//lH0IwQxrpGh2oguswTsyW','Sushi Kiosk Puri','puriindahmall@sushi-kiosk.com',100,0,4,00000000,0,'',1,NULL),
(6721,'qa.spv4','$2y$09$l88loFG5ilCNbWEZDSslDODFuSGaeJZW5XF2lR5W2C0ERsezboMFK','Dhini','qa.spv4@sushitei.co.id',99,0,8,00000000,0,'',1,NULL),
(6722,'qa.spv','$2y$09$nrSoQb4dtIJLe5TnBKV5huP6egDF2OST7llUWZ3XjORuF9gZKQFhe','Firga','qa.spv@sushitei.co.id',99,0,8,00000000,0,'',1,NULL),
(6723,'qa1.jkt','$2y$09$nN5Zofs66v9QeSHfwqB/le.JFnQOvPkSwWZ4FsP5C27x7cIgEKZ4C','Cholil','qa1.jkt@sushitei.co.id',99,0,8,00000000,0,'',1,NULL),
(6724,'qa4.jkt','$2y$09$vlEvxq3L2aKWaNq7tTBxMux41UQLGF2tK7HgcqzJgPMqKUdGUM2.i','Yonas','qa4.jkt@sushitei.co.id',99,0,8,00000000,0,'',1,NULL),
(6725,'qa6','$2y$09$VwtRmnM7wixx0C78JX3Cfeo64DYWhxcojj3W7DE9h.oMchnooUDDW','Audie','qa6.jkt@sushitei.co.id',99,0,8,00000000,1,'HO',1,NULL),
(6726,'qa7','$2y$09$vTxddy.YeRqpE.DWKwF8/uUSea0RuqlgyTiqHVwiNEFMeNWtRsOXG','Yussy','qa7.jkt@sushitei.co.id',99,0,8,00000000,0,'',1,NULL),
(6727,'rizal','$2y$09$uhpdwbl8TDRnvgBWtIhu3eBaC9BhQWv/cXElBsKvd7P1Bd9wQvvbG','Rizal','rizal@sushitei.co.id',99,0,7,00000000,0,'',1,NULL),
(6728,'stbali','$2y$09$MJ4YPvT8bg3tMl1npkOMzuBUC.Dyw8PfoXkL6VOe9rS8i1XybHcX6','Sushi Tei Bali','spv.it.bali@sushitei.co.id',111,0,4,00000000,1,'bali',1,NULL),
(6729,'st.bx','$2y$09$aHnIf/pgSwXhovWwYMrK6OQ0hFxebk9Bb3HaU9xDZGb2a73a28ny6','ST BX','st.bx@sushitei.co.id',15,0,4,00000000,0,'',1,NULL),
(6730,'st.cp','$2y$09$wv3Tr0bEe2DeH05EPmN0IOQRZIuB9ZfQQ/yMIEgvUoPyH/CxLHNhi','ST CP','st.cp@sushitei.co.id',7,0,4,00000000,0,'',1,NULL),
(6731,'st.cwk','$2y$09$0vKRPtDmn4MtsbVooz/Evu.1goBUI/Q43KTvmM3JWNmLI.99FcgRq','ST CWK','st.cwk@sushitei.co.id',113,0,4,00000000,0,'',1,NULL),
(6732,'st.ep','$2y$09$cR3jfienqG8SDPpcGjwL2uxfic8MtnmjY0X7aEcpYZdjnDQF6jWHO','ST EP ','st.ep@sushitei.co.id',6,0,4,00000000,0,'',1,NULL),
(6733,'st.fs','$2y$09$KEvNqNhpOJvH7wKxjfTJB.jzebJSf7mquNd9vxrTXkLhomBOwtPPS','ST FS','st.fs@sushitei.co.id',9,0,4,00000000,0,'',1,NULL),
(6734,'st.gc','$2y$09$06RPWYlavbwZ5lc2HNq61uguLWrdLdXTpbDAU1Kc9/s1aIPXEl95G','ST GC','st.gc@sushitei.co.id',8,0,4,00000000,0,'',1,NULL),
(6735,'st.gi','$2y$09$xnzTScXdEqXvFf2gVHju4ORJ.EPMpoKSXBmsTKIkPULs1lxghiNbW','ST GI','st.gi@sushitei.co.id',11,0,4,00000000,0,'',1,NULL),
(6736,'st.karawaci','$2y$09$8FNx24UKEZ.X5bi35eHDcOdmijmfyNYqfoFP.xFfVdjFrQ9R1KJyO','ST Karawaci','st.karawaci@sushitei.co.id',10,0,4,00000000,0,'',1,NULL),
(6737,'st.kk','$2y$09$8hW.u0Reu7zzEoZuRAjw8.sTkb57LceqA76/FvJnDCQh1fLygBDAe','ST KK','st.kk@sushitei.co.id',13,0,4,00000000,0,'',1,NULL),
(6738,'st.kv','$2y$09$bEmtWJKMAgjgYz4jXegOouxdgNl1dFmyVKgpxZmb5/toM8OqVzZ5e','ST KV','st.kv@sushitei.co.id',108,0,4,00000000,0,'',1,NULL),
(6739,'st.la','$2y$09$1GcvqPOnYJt6dxR3QEKZmOGmvGjTYKuMnwIlEZTi99ojJMzDC.Y0C','ST LA','st.la@sushitei.co.id',12,0,4,00000000,0,'',1,NULL),
(6740,'st.mc','$2y$09$CrDLI1Jh8z4UIx0ydViTQeoFxXekWeXMvocG8.i6q58Aa6Exg4iWG','ST MC','st.mc@sushitei.co.id',109,0,4,00000000,0,'',1,NULL),
(6741,'st.mcc','$2y$09$C0jXnkBUgfx83YUS6s8iGuZJniT0XyCvS42dSTN4fEUzu7WmzjiuG','ST MCC','st.mcc@sushitei.co.id',114,0,4,00000000,0,'',1,NULL),
(6742,'st.mkg','$2y$09$MmIJAhYgGB0i1PUYGSnY9e0kSxi1FRMYysVVZC26.LZhOGgDKThIa','ST MKG','st.mkg@sushitei.co.id',5,0,4,00000000,0,'',1,NULL),
(6743,'st.pi','$2y$09$DHsn1.Xh1KmkZs1ZSvgKRevYjP/wrKFvNLWdDUDI0e7ERJRnC8H52','ST PI ','st.pi@sushitei.co.id',1,0,4,00000000,0,'',1,NULL),
(6744,'st.pim','$2y$09$bWWYnqdCsByejw/Svc/V5ehKmgFYk3A9Y9LWGT8ZFy4ScdYORcHsW','ST PIM','st.pim@sushitei.co.id',3,0,4,00000000,0,'',1,NULL),
(6745,'st.pm','$2y$09$JREa7yxm2lP5QUC4SLb5a.7VDEnslKa0h/6JnHpFP6n2lDN9Qexde','ST PM','st.pm@sushitei.co.id',17,0,4,00000000,0,'',1,NULL),
(6746,'st.pp','$2y$09$DhBkjlNBdOQ.lPA4ybgzBenU.q8qp/8bldz4TYt.Hk5NigJP2m/j.','ST Pacific Place','st.pp@sushitei.co.id',123,0,4,00000000,0,'',1,NULL),
(6747,'st.ps','$2y$09$/obqILi2dvQIs3gydco3OO6yj8KClZ0r2ZyYg8ElV9QdlHkCFopnC','ST PS','st.ps@sushitei.co.id',2,0,4,00000000,0,'',1,NULL),
(6748,'st.sb','$2y$09$8BnJqNy5MtGWfhX9S9x0/eVLo9GDE0TQA5JnVNlvU6BGr/iP0fJ.O','ST SMB','st.sb@sushitei.co.id',14,0,4,00000000,0,'',1,NULL),
(6749,'st.senci','$2y$09$bCFM8f0Um1r8/ao6dpbAQu9G4cgHedn/i6kSjkVR9hckAF1nlaTUq','ST SC','st.senci@sushitei.co.id',4,0,4,00000000,0,'',1,NULL),
(6750,'st.sms','$2y$09$Q5A6rdYk5KAt5vtthmbORe2oqaSJXVng5b1tliTrXh5y00gwvU2qu','ST SMS','st.sms@sushitei.co.id',18,0,4,00000000,0,'',1,NULL),
(6751,'st.tb','$2y$09$VerPdU/Izn7NNG4uPIJnMuFjSGEvPWJq1hLzT9YK82b8vS4DuTKR6','ST TB','st.tb@sushitei.co.id',16,0,4,00000000,0,'',1,NULL),
(6752,'st.tsc','$2y$09$3nL8S56FelLsiBVtq1..c.5EJ0wcu/Mw5m2AmnWabv8QZJSXoD6pG','Sushi Tei Trans Studio Cibubur','st.tsc@sushitei.co.id',125,0,4,00000000,0,'',1,NULL),
(6753,'theresia','$2y$09$179n5U1fTh2uKstlGW2ihu2NbANIM6RVbOFKXLvQgJ0.jyjHxJ/.K','theresia','theresia@sushitei.co.id',99,0,4,00000000,1,'tereaja',1,NULL),
(6754,'Toya','$2y$09$5fkO4ZOgSsVqPsay0J.ID.6EN9pytC4aC8VyNnW/gs1/3asrObey6','Toya San','toya@sushitei.co.id',99,0,4,00000000,1,'teh tarik',1,NULL),
(6755,'nandisetiadi','$2y$09$kqXPT.NW473VPJ3P.2RMgOkTzHJ.u3cLrSY5fzOEiLQSSKkMRv8ne','Nandi Setiadi','training2.jkt@sushitei.co.id',99,0,4,00000000,2,'nandi',1,NULL),
(6756,'admintsmi','$2y$09$mD9/QJ3.q9mwkJhxK2IhjeK8XMhxYLRon/EXMQ364vqXLejknW.eK','TS MOI','ts.moi@tomsushi.co.id',118,0,4,00000000,1,'EP',1,NULL),
(6757,'wahyuni','$2y$09$ngOR4s/8pxNBTQyUk5DuqeUBxU8W8UqpMF0hNMmRBqKgjUCIFTc1G','Wahyuni','wahyuni@sushitei.co.id',99,0,7,00000000,0,'',1,NULL),
(6758,'warehouse.pqnc','$2y$09$o5sHRmJ3U6ghQU3XXrhCle6.JbO6zFToMFTTrcso6rd4e6jP.w1gq','Warehouse','warehouse.jkt@sushitei.co.id',107,0,4,00000000,0,'',1,NULL),
(6759,'Yulis','$2y$09$6BTCvYHlh9mpD9vT26HDpO.VaP.IL81kDGskqn5uiWotahEfAhTv2',' Yulis Setialianny Eka','yulis@sushitei.co.id',99,0,7,00000000,3,'saripah',1,NULL),
(6768,'Novita','$2y$09$Dz.1EwrtFbcFk2CjnhzJaeoEyJwJFU.PY2B2Z386rYCaAma2o6SGy','Koperasi Berkat','adm.kopber@gmail.com',1,26,7,00000000,2,'mpie',1,NULL),
(6769,'ritasariatun','$2y$09$fFOgwWCuG1zbJKqxBNnwt.rQwWdwA9MKEaLN2OSa5aqcBgMtKzkJO','Rita Sari Atun','isa4.jkt@sushitei.co.id',1,11,7,00000000,3,'sutirah',1,NULL),
(6770,'michiko','$2y$09$kspg76ffy/KutFvyBVlgLuEBviUkFyj.GazUrTXsmnLLBY3rMv4RO','Michiko','michiko@sushitei.co.id',1,15,7,00000000,4,'sushi',1,NULL),
(6771,'purchteam','$2y$09$7feizRtRKThRfGguulIbkephBKR73acScSd/xsr//kxMpTBFdcdUC','puchteam','p.putra@sushitei.co.id',1,22,7,00000000,1,'HO',1,NULL),
(6772,'Meiwita','$2y$09$Al/O.SaWYhn9ln5PZ/kaOuOd6uMoKIWey8QfDFP64xA2g7bbSkCFG','Mei wita Panjaitan','personnel.spv@sushitei.co.id',1,9,7,00000000,3,'Merry Tampubolon',1,NULL),
(6773,'tika','$2y$09$PHfudv.YYxG26C9s.N9ywO2Bw8YDBS2WhthXtt01rhkPdvwvJyTem','Tika Zainaf karbelan','recruitment1.jkt@sushitei.co.id',1,25,7,00000000,1,'head office',1,NULL),
(6774,'rinta','$2y$09$8O2HSXebK7otwq5QIGpnvO8dp7PxuBtGAqyfH.jOrifC4c2K9ilv6','Rientha Aryanie','recruitment2.jkt@sushitei.co.id',1,25,7,00000000,4,'sushi',1,NULL),
(6775,'Latifa','$2y$09$aiNNECzspfLgeKoDg65UmuMt2wK3qD2cqJ/c1ni9C9h3mtLvk/O76','Latifa Dinar','recruitment3.jkt@sushitei.co.id',1,25,7,00000000,1,'training',1,NULL),
(6776,'sigit','$2y$09$5LUAWXzybnhppQ9cGm06De3jdVyuVH5lJSgJAepqgLwRS6jBa85Zi','sigit pebriantoro','sigit@sushitei.co.id',1,7,7,00000000,1,'Plaza Indonesia',1,NULL),
(6777,'yulia','$2y$09$tRx0RUZtW6LSensXm9ii8OVj96mq9wXvHbxQjZlucMCUSfsPg6Lg.','yulia','training3.jkt@sushitei.co.id',1,13,6,00000000,2,'lia',1,NULL),
(6778,'vera','$2y$09$4RX4.h9Vvstcit.GpJr1hOR.7LZ9QBjAt0PNURzy3vMzs1198YMeC','vera','vera.kusliawan@sushitei.co.id',1,19,7,00000000,1,'HO',1,NULL),
(6779,'Yuliana','$2y$09$.AzMyQMgUQ/1s28wTxACXeLmJnlIONLLqVnCT02rNflHSvapKqyH2','Yuliana Sutanto','yuliana@sushitei.co.id',1,25,7,00000000,1,'PS',1,NULL);

/*Table structure for table `role_permission` */

DROP TABLE IF EXISTS `role_permission`;

CREATE TABLE `role_permission` (
  `role` smallint(5) DEFAULT NULL,
  `permission` mediumint(6) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `role_permission` */

insert  into `role_permission`(`role`,`permission`,`last_update`) values 
(15,6,NULL),
(15,14,NULL),
(15,16,NULL),
(5,6,NULL),
(5,8,NULL),
(5,14,NULL),
(5,18,NULL),
(5,25,NULL),
(5,15,NULL),
(5,17,NULL),
(4,6,NULL),
(4,8,NULL),
(4,14,NULL),
(4,15,NULL),
(4,17,NULL),
(9,31,NULL),
(9,29,NULL),
(14,6,NULL),
(14,8,NULL),
(14,14,NULL),
(14,15,NULL),
(14,31,NULL),
(14,29,NULL),
(14,17,NULL),
(3,14,NULL),
(13,32,NULL),
(2,2,NULL),
(2,3,NULL),
(2,7,NULL),
(2,12,NULL),
(2,4,NULL),
(2,6,NULL),
(2,8,NULL),
(2,14,NULL),
(2,15,NULL),
(2,31,NULL),
(2,32,NULL),
(2,16,NULL),
(2,17,NULL),
(7,6,NULL),
(7,23,NULL),
(7,8,NULL),
(7,13,NULL),
(7,14,NULL),
(7,18,NULL),
(7,15,NULL),
(7,24,NULL),
(7,31,NULL),
(7,29,NULL),
(7,32,NULL),
(7,30,NULL),
(7,16,NULL),
(7,26,NULL),
(7,17,NULL),
(7,27,NULL),
(18,6,NULL),
(18,8,NULL),
(18,13,NULL),
(18,20,NULL),
(18,14,NULL),
(18,15,NULL),
(11,6,NULL),
(11,8,NULL),
(11,15,NULL),
(11,24,NULL),
(11,22,NULL),
(11,17,NULL),
(10,2,NULL),
(10,4,NULL),
(10,6,NULL),
(10,23,NULL),
(10,21,NULL),
(10,8,NULL),
(10,14,NULL),
(10,15,NULL),
(10,17,NULL),
(10,33,NULL),
(10,34,NULL),
(1,2,NULL),
(1,3,NULL),
(1,7,NULL),
(1,12,NULL),
(1,4,NULL),
(1,6,NULL),
(1,23,NULL),
(1,21,NULL),
(1,8,NULL),
(1,13,NULL),
(1,20,NULL),
(1,14,NULL),
(1,18,NULL),
(1,25,NULL),
(1,15,NULL),
(1,24,NULL),
(1,22,NULL),
(1,31,NULL),
(1,29,NULL),
(1,32,NULL),
(1,30,NULL),
(1,16,NULL),
(1,26,NULL),
(1,17,NULL),
(1,27,NULL),
(1,28,NULL),
(1,33,NULL),
(1,34,NULL);

/*Table structure for table `staff_department` */

DROP TABLE IF EXISTS `staff_department`;

CREATE TABLE `staff_department` (
  `department_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `gm_position` varchar(50) CHARACTER SET latin1 NOT NULL,
  `gm_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `gm_email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `department_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `staff_department` */

insert  into `staff_department`(`department_id`,`department_name`,`gm_position`,`gm_name`,`gm_email`,`department_status`) values 
(1,'IT Department ','GM Admin','Luciana','it6.jkts@sushitei.co.id',1),
(2,'Tom Sushi','AGM Operation TS&SK','Vera Kusliawan','test@sushitei.co.id',1),
(3,'Internal Audit','GM Admin','Luciana','it6.jkt@sushitei.co.id',1);

/*Table structure for table `store_location` */

DROP TABLE IF EXISTS `store_location`;

CREATE TABLE `store_location` (
  `location_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `location_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `location_status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Data for the table `store_location` */

insert  into `store_location`(`location_id`,`location_name`,`location_status`) values 
(1,'Jakarta',1),
(2,'Surabaya',1),
(3,'Makasar',1),
(4,'Palembang',1),
(5,'Bali',1),
(6,'Bandung',1),
(7,'Jogja',1),
(8,'Medan',1),
(9,'Pekan Baru',1),
(10,'Batam',1),
(11,'Bengkulu',1);

/*Table structure for table `user_data` */

DROP TABLE IF EXISTS `user_data`;

CREATE TABLE `user_data` (
  `data_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `data_user` smallint(5) unsigned NOT NULL,
  `data_type` smallint(4) DEFAULT NULL,
  `data_site` mediumint(6) NOT NULL,
  `data_value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_data` */

insert  into `user_data`(`data_id`,`data_user`,`data_type`,`data_site`,`data_value`) values 
(1,6665,1,3,'0'),
(2,6610,1,3,'14'),
(3,6611,1,3,'17'),
(4,6612,1,3,'81'),
(5,6613,1,3,'9'),
(6,6614,1,3,'13'),
(7,6615,1,3,'3'),
(8,6616,1,3,'158'),
(9,6617,1,3,'115'),
(10,6618,1,3,'16'),
(11,6619,1,3,'31'),
(12,6620,1,3,'39'),
(13,6621,1,3,'239'),
(14,6641,1,3,'27'),
(15,6642,1,3,'20'),
(16,6643,1,3,'4'),
(17,6644,1,3,'3'),
(18,6645,1,3,'9'),
(19,6646,1,3,'25'),
(20,6647,1,3,'38'),
(21,6648,1,3,'29'),
(22,6649,1,3,'9'),
(23,6650,1,3,'28'),
(24,6652,1,3,'147'),
(25,6653,1,3,'37'),
(26,6654,1,3,'19'),
(27,6655,1,3,'17'),
(28,6656,1,3,'57'),
(29,6658,1,3,'15'),
(30,6659,1,3,'15'),
(31,6660,1,3,'8'),
(32,6661,1,3,'1'),
(33,6662,1,3,'169'),
(34,6663,1,3,'2'),
(35,6664,1,3,'31'),
(36,6666,1,3,'1'),
(37,6667,1,3,'81'),
(38,6668,1,3,'69'),
(39,6669,1,3,'209'),
(40,6670,1,3,'52'),
(41,6671,1,3,'206'),
(42,6672,1,3,'38'),
(43,6673,1,3,'102'),
(44,6674,1,3,'22'),
(45,6675,1,3,'2'),
(46,6676,1,3,'27'),
(47,6677,1,3,'135'),
(48,6678,1,3,'13'),
(49,6679,1,3,'24'),
(50,6680,1,3,'45'),
(51,6681,1,3,'48'),
(52,6682,1,3,'190'),
(53,6683,1,3,'212'),
(54,6684,1,3,'258'),
(55,6685,1,3,'119'),
(56,6686,1,3,'204'),
(57,6687,1,3,'179'),
(58,6688,1,3,'141'),
(59,6689,1,3,'5'),
(60,6690,1,3,'191'),
(61,6691,1,3,'42'),
(62,6692,1,3,'177'),
(63,6693,1,3,'143'),
(64,6694,1,3,'170'),
(65,6695,1,3,'158'),
(66,6696,1,3,'61'),
(67,6697,1,3,'83'),
(68,6698,1,3,'161'),
(69,6699,1,3,'48'),
(70,6700,1,3,'170'),
(71,6701,1,3,'187'),
(72,6702,1,3,'197'),
(73,6703,1,3,'24'),
(74,6704,1,3,'189'),
(75,6705,1,3,'2'),
(76,6706,1,3,'152'),
(77,6707,1,3,'197'),
(78,6708,1,3,'140'),
(79,6709,1,3,'120'),
(80,6710,1,3,'4'),
(81,6711,1,3,'29'),
(82,6712,1,3,'11'),
(83,6714,1,3,'2'),
(84,6715,1,3,'36'),
(85,6716,1,3,'13'),
(86,6717,1,3,'51'),
(87,6718,1,3,'3'),
(88,6719,1,3,'74'),
(89,6720,1,3,'61'),
(90,6721,1,3,'30'),
(91,6722,1,3,'10'),
(92,6723,1,3,'1'),
(93,6724,1,3,'6'),
(94,6725,1,3,'7'),
(95,6726,1,3,'5'),
(96,6727,1,3,'101'),
(97,6728,1,3,'152'),
(98,6729,1,3,'18'),
(99,6730,1,3,'25'),
(100,6731,1,3,'10'),
(101,6732,1,3,'51'),
(102,6733,1,3,'36'),
(103,6734,1,3,'29'),
(104,6735,1,3,'38'),
(105,6736,1,3,'21'),
(106,6737,1,3,'46'),
(107,6738,1,3,'18'),
(108,6739,1,3,'44'),
(109,6740,1,3,'87'),
(110,6741,1,3,'8'),
(111,6742,1,3,'86'),
(112,6743,1,3,'34'),
(113,6744,1,3,'89'),
(114,6745,1,3,'33'),
(115,6747,1,3,'85'),
(116,6748,1,3,'74'),
(117,6749,1,3,'13'),
(118,6750,1,3,'25'),
(119,6751,1,3,'46'),
(120,6753,1,3,'35'),
(121,6754,1,3,'21'),
(122,6755,1,3,'27'),
(123,6756,1,3,'3'),
(124,6757,1,3,'42'),
(125,6758,1,3,'90'),
(126,6759,1,3,'17'),
(127,6768,1,3,'10'),
(128,6770,1,3,'27'),
(129,6772,1,3,'5'),
(130,6773,1,3,'59'),
(131,6774,1,3,'7'),
(132,6776,1,3,'3'),
(133,6778,1,3,'26'),
(134,6779,1,3,'33'),
(256,6670,1,5,'0'),
(257,6620,1,5,'6'),
(258,6648,1,5,'278'),
(259,6650,1,5,'1'),
(260,6653,1,5,'1'),
(261,6662,1,5,'2'),
(262,6668,1,5,'4'),
(263,6674,1,5,'1'),
(264,6683,1,5,'649'),
(265,6684,1,5,'850'),
(266,6685,1,5,'208'),
(267,6686,1,5,'766'),
(268,6687,1,5,'717'),
(269,6688,1,5,'1037'),
(270,6689,1,5,'57'),
(271,6690,1,5,'458'),
(272,6691,1,5,'122'),
(273,6692,1,5,'479'),
(274,6693,1,5,'523'),
(275,6694,1,5,'438'),
(276,6695,1,5,'480'),
(277,6696,1,5,'359'),
(278,6697,1,5,'182'),
(279,6698,1,5,'725'),
(280,6699,1,5,'132'),
(281,6700,1,5,'314'),
(282,6701,1,5,'839'),
(283,6702,1,5,'638'),
(284,6703,1,5,'74'),
(285,6704,1,5,'732'),
(286,6705,1,5,'4'),
(287,6706,1,5,'524'),
(288,6707,1,5,'832'),
(289,6708,1,5,'433'),
(290,6709,1,5,'703'),
(291,6710,1,5,'17'),
(292,6714,1,5,'1'),
(293,6717,1,5,'4'),
(294,6720,1,5,'355'),
(295,6721,1,5,'4'),
(296,6722,1,5,'1'),
(297,6735,1,5,'1'),
(298,6758,1,5,'104'),
(319,6643,1,6,'0'),
(320,6610,1,6,'4'),
(321,6611,1,6,'2'),
(322,6612,1,6,'9'),
(323,6613,1,6,'9'),
(324,6614,1,6,'7'),
(325,6615,1,6,'1'),
(326,6616,1,6,'1'),
(327,6617,1,6,'1'),
(328,6618,1,6,'2'),
(329,6619,1,6,'2'),
(330,6620,1,6,'6'),
(331,6621,1,6,'6'),
(332,6641,1,6,'5'),
(333,6648,1,6,'2'),
(334,6649,1,6,'2'),
(335,6650,1,6,'1'),
(336,6653,1,6,'173'),
(337,6654,1,6,'85'),
(338,6656,1,6,'3'),
(339,6664,1,6,'3'),
(340,6668,1,6,'4'),
(341,6670,1,6,'1'),
(342,6671,1,6,'1'),
(343,6677,1,6,'3'),
(344,6678,1,6,'5'),
(345,6680,1,6,'2'),
(346,6686,1,6,'1'),
(347,6701,1,6,'1'),
(348,6707,1,6,'1'),
(349,6715,1,6,'2'),
(350,6716,1,6,'1'),
(351,6720,1,6,'2'),
(352,6754,1,6,'2'),
(353,6755,1,6,'37'),
(354,6758,1,6,'1'),
(355,6768,1,6,'4'),
(356,6774,1,6,'1'),
(357,6776,1,6,'1'),
(382,6670,1,8,'0'),
(383,6648,1,8,'35'),
(384,6657,1,8,'1'),
(385,6665,1,8,'15'),
(386,6668,1,8,'2'),
(387,6685,1,8,'6'),
(388,6686,1,8,'4'),
(389,6689,1,8,'14'),
(390,6691,1,8,'15'),
(391,6696,1,8,'5'),
(392,6697,1,8,'1'),
(393,6699,1,8,'21'),
(394,6703,1,8,'3'),
(395,6708,1,8,'1'),
(396,6717,1,8,'1'),
(397,6720,1,8,'19'),
(398,6729,1,8,'93'),
(399,6730,1,8,'64'),
(400,6731,1,8,'22'),
(401,6732,1,8,'114'),
(402,6733,1,8,'135'),
(403,6734,1,8,'194'),
(404,6735,1,8,'130'),
(405,6736,1,8,'102'),
(406,6737,1,8,'162'),
(407,6738,1,8,'54'),
(408,6739,1,8,'38'),
(409,6740,1,8,'57'),
(410,6741,1,8,'31'),
(411,6742,1,8,'52'),
(412,6743,1,8,'67'),
(413,6744,1,8,'92'),
(414,6745,1,8,'59'),
(415,6746,1,8,'7'),
(416,6747,1,8,'66'),
(417,6748,1,8,'19'),
(418,6749,1,8,'35'),
(419,6750,1,8,'72'),
(420,6751,1,8,'122'),
(421,6752,1,8,'24'),
(422,6756,1,8,'16'),
(423,6758,1,8,'1216'),
(445,6670,1,7,'0'),
(446,6648,1,7,'305'),
(447,6650,1,7,'2'),
(448,6657,1,7,'2'),
(449,6662,1,7,'1'),
(450,6665,1,7,'15'),
(451,6668,1,7,'9'),
(452,6680,1,7,'1'),
(453,6683,1,7,'10'),
(454,6684,1,7,'2'),
(455,6685,1,7,'15'),
(456,6686,1,7,'1'),
(457,6689,1,7,'6'),
(458,6690,1,7,'4'),
(459,6691,1,7,'42'),
(460,6694,1,7,'2'),
(461,6695,1,7,'15'),
(462,6696,1,7,'11'),
(463,6697,1,7,'14'),
(464,6699,1,7,'18'),
(465,6700,1,7,'1'),
(466,6701,1,7,'9'),
(467,6702,1,7,'27'),
(468,6703,1,7,'7'),
(469,6705,1,7,'2'),
(470,6707,1,7,'1'),
(471,6708,1,7,'4'),
(472,6710,1,7,'1'),
(473,6720,1,7,'103'),
(474,6721,1,7,'3'),
(475,6722,1,7,'1'),
(476,6729,1,7,'197'),
(477,6730,1,7,'317'),
(478,6731,1,7,'154'),
(479,6732,1,7,'247'),
(480,6733,1,7,'266'),
(481,6734,1,7,'425'),
(482,6735,1,7,'248'),
(483,6736,1,7,'249'),
(484,6737,1,7,'227'),
(485,6738,1,7,'173'),
(486,6739,1,7,'273'),
(487,6740,1,7,'262'),
(488,6741,1,7,'86'),
(489,6742,1,7,'208'),
(490,6743,1,7,'248'),
(491,6744,1,7,'370'),
(492,6745,1,7,'206'),
(493,6746,1,7,'11'),
(494,6747,1,7,'417'),
(495,6748,1,7,'232'),
(496,6749,1,7,'227'),
(497,6750,1,7,'283'),
(498,6751,1,7,'254'),
(499,6752,1,7,'43'),
(500,6756,1,7,'12'),
(501,6758,1,7,'1376');

/*Table structure for table `user_data_type` */

DROP TABLE IF EXISTS `user_data_type`;

CREATE TABLE `user_data_type` (
  `type_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(25) DEFAULT NULL,
  `type_desc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_data_type` */

insert  into `user_data_type`(`type_id`,`type_name`,`type_desc`) values 
(1,'total_ticket','Total Ticket'),
(2,'open_ticket','Open Ticket'),
(3,'on_progress_ticket','On-Progress Ticket');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `role_id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_level` smallint(2) unsigned NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`role_id`,`role_name`,`role_level`) values 
(1,'Super Admin',99),
(2,'Administrator',98),
(3,'MRP Engineer',5),
(4,'Ticket Users',1),
(5,'Admin MRP',10),
(6,'Area Manager',90),
(7,'Admin View',91),
(8,'Admin QA',10),
(9,'PQNC Engineer',5),
(10,'Admin IT',10),
(11,'Admin GA',10),
(12,'Admin SLNC',10),
(13,'PIC SLNC',10),
(14,'Admin PQNC',10),
(15,'MOD Outlet',2),
(18,'Admin Booking Room',0);

/*Table structure for table `user_role_manager` */

DROP TABLE IF EXISTS `user_role_manager`;

CREATE TABLE `user_role_manager` (
  `role` smallint(2) DEFAULT NULL,
  `allow_role` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_role_manager` */

/*Table structure for table `user_sessions` */

DROP TABLE IF EXISTS `user_sessions`;

CREATE TABLE `user_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_sessions` */

insert  into `user_sessions`(`id`,`ip_address`,`timestamp`,`data`) values 
('5jir41ljjavp0erq8qnj02s5lbjneuep','127.0.0.1',1562228674,'__ci_last_regenerate|i:1562228639;attempt|i:1;user_id|s:4:\"1001\";display_name|s:15:\"Rahmat Samsudin\";group_id|s:1:\"1\";group|s:11:\"Super Admin\";username|s:22:\"it1.jkt@sushitei.co.id\";role|s:2:\"99\";store_id|s:2:\"99\";logged_in|b:1;');

/*Table structure for table `view_events` */

DROP TABLE IF EXISTS `view_events`;

/*!50001 DROP VIEW IF EXISTS `view_events` */;
/*!50001 DROP TABLE IF EXISTS `view_events` */;

/*!50001 CREATE TABLE  `view_events`(
 `event_id` mediumint(8) unsigned ,
 `event_name` varchar(255) ,
 `event_status` tinyint(1) unsigned ,
 `status_name` varchar(25) ,
 `display_name` varchar(50) ,
 `room_name` varchar(30) ,
 `room_capacity` decimal(4,0) ,
 `start_time` datetime ,
 `end_time` datetime 
)*/;

/*View structure for view view_events */

/*!50001 DROP TABLE IF EXISTS `view_events` */;
/*!50001 DROP VIEW IF EXISTS `view_events` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_events` AS select `sce`.`event_id` AS `event_id`,`sce`.`event_name` AS `event_name`,`sce`.`event_status` AS `event_status`,`sces`.`status_name` AS `status_name`,`sppu`.`display_name` AS `display_name`,`scr`.`room_name` AS `room_name`,`scr`.`room_capacity` AS `room_capacity`,`sce`.`start_time` AS `start_time`,`sce`.`end_time` AS `end_time` from (((`sushitei_car`.`events` `sce` left join `sushitei_car`.`room` `scr` on((`sce`.`room_id` = `scr`.`room_id`))) left join `sushitei_portal`.`portal_user` `sppu` on((`sppu`.`user_id` = `sce`.`event_creator`))) left join `sushitei_car`.`event_status` `sces` on((`sces`.`status_id` = `sce`.`event_status`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
