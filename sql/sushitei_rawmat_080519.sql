/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - sushitei_rawmat
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_rawmat` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sushitei_rawmat`;

/*Table structure for table `request_type` */

DROP TABLE IF EXISTS `request_type`;

CREATE TABLE `request_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `request_type` */

insert  into `request_type`(`type_id`,`type_name`) values 
(1,'Promo Material'),
(2,'Alternatif Material'),
(3,'Pergantian Material');

/*Table structure for table `sample_log` */

DROP TABLE IF EXISTS `sample_log`;

CREATE TABLE `sample_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_date` datetime DEFAULT NULL,
  `log_content` text,
  `ticket_sample` int(11) unsigned NOT NULL COMMENT 'FK ticket_sample.sample_id',
  `sample_status` tinyint(3) unsigned NOT NULL COMMENT 'FK sample_status.status_id',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT 'FK user_type.type_id',
  `user` smallint(5) unsigned NOT NULL COMMENT 'FK sushitei_portal.portal_user.user_id',
  `registry_number` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_log` */

insert  into `sample_log`(`log_id`,`log_date`,`log_content`,`ticket_sample`,`sample_status`,`user_type`,`user`,`registry_number`) values 
(9,'2019-07-24 13:54:42','fnb release 8 ',8,2,1,6672,NULL),
(10,'2019-07-24 14:59:27','tes reject 12',12,3,1,6672,NULL),
(11,'2019-07-24 15:59:28','qa rilis',8,4,2,6672,NULL),
(12,'2019-07-24 16:36:48','fnb reject 11',11,3,1,6672,NULL),
(13,'2019-07-24 16:37:02','fnb release 9',9,2,1,6672,NULL),
(14,'2019-07-25 09:46:50','qa reject 9',9,5,2,6672,NULL),
(16,'2019-07-26 10:24:33','kahi rilis',8,6,3,6672,'FINA-12345-67'),
(17,'2019-08-02 11:09:38','rilis fnb 17',17,2,1,6672,NULL),
(18,'2019-08-02 11:20:50','rilis 18',18,2,1,6672,NULL);

/*Table structure for table `sample_status` */

DROP TABLE IF EXISTS `sample_status`;

CREATE TABLE `sample_status` (
  `status_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(15) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_status` */

insert  into `sample_status`(`status_id`,`status_name`) values 
(1,'Open'),
(2,'FNB Release'),
(3,'FNB Reject'),
(4,'QA Release'),
(5,'QA Reject'),
(6,'KAHI Release'),
(7,'KAHI Reject');

/*Table structure for table `ticket_document` */

DROP TABLE IF EXISTS `ticket_document`;

CREATE TABLE `ticket_document` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_sample` int(11) NOT NULL,
  `document_type` varchar(10) NOT NULL,
  `document_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `document_ext` varchar(10) CHARACTER SET latin1 NOT NULL,
  `document_location` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_document` */

insert  into `ticket_document`(`document_id`,`ticket_sample`,`document_type`,`document_name`,`document_ext`,`document_location`) values 
(12,17,'image','15646544690.jpg','.jpg','Resources/images/sample\\'),
(13,17,'pdf','15646544690.pdf','.pdf','Resources/cert/sample\\'),
(14,18,'image','15647179340.jpg','.jpg','Resources/images/sample\\'),
(15,18,'pdf','15644849900.pdf','.jpg','Resources/cert/sample\\'),
(16,19,'image','15649729030.jpg','.jpg','Resources/images/sample\\'),
(17,19,'pdf','15649728670.pdf','.pdf','Resources/cert/sample\\');

/*Table structure for table `ticket_link` */

DROP TABLE IF EXISTS `ticket_link`;

CREATE TABLE `ticket_link` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_link` */

insert  into `ticket_link`(`link_id`,`supplier_id`,`producer_id`) values 
(1,3,4),
(5,5,3);

/*Table structure for table `ticket_producer` */

DROP TABLE IF EXISTS `ticket_producer`;

CREATE TABLE `ticket_producer` (
  `producer_id` int(11) NOT NULL AUTO_INCREMENT,
  `producer_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `producer_address` text CHARACTER SET latin1 NOT NULL,
  `producer_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `producer_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_producer` */

insert  into `ticket_producer`(`producer_id`,`producer_name`,`producer_address`,`producer_phone`,`producer_pic`) values 
(3,'PT B','Jl Tes Coba No 1','021456789','Fina'),
(4,'PT A','Jl Halo No 2 Jakarta','021333444','Pak Halo'),
(5,'PT C','Jl test tes 3','021545678','tes pic 3');

/*Table structure for table `ticket_request` */

DROP TABLE IF EXISTS `ticket_request`;

CREATE TABLE `ticket_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `request_reason` tinyint(3) NOT NULL,
  `request_date` datetime NOT NULL,
  `deadline_date` datetime NOT NULL,
  `request_user` smallint(4) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_request` */

insert  into `ticket_request`(`request_id`,`request_name`,`request_reason`,`request_date`,`deadline_date`,`request_user`,`last_update`) values 
(11,'Dried Radishes',3,'2019-07-23 11:54:00','2019-08-17 00:00:00',6672,'2019-07-30 13:35:13'),
(12,'Fruit flavor',1,'2019-07-30 17:39:35','2019-09-30 00:00:00',6672,'2019-07-30 17:39:35'),
(13,'Ocha Japan Traditional Flavor',2,'2019-08-05 09:29:48','2019-10-31 00:00:00',6672,'2019-08-05 09:29:48');

/*Table structure for table `ticket_sample` */

DROP TABLE IF EXISTS `ticket_sample`;

CREATE TABLE `ticket_sample` (
  `sample_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `sample_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `link_id` int(11) NOT NULL,
  `sample_condition` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `sample_date` datetime DEFAULT NULL,
  `sample_status` tinyint(2) unsigned DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`sample_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_sample` */

insert  into `ticket_sample`(`sample_id`,`request_id`,`sample_name`,`link_id`,`sample_condition`,`sample_date`,`sample_status`,`last_update`) values 
(8,11,'Marukochan dried radish',1,'dry','2019-07-23 12:15:50',6,NULL),
(9,11,'Inuyasa dried radish',2,'dry','2019-07-23 14:13:15',5,NULL),
(11,11,'NARUTO dried dish',2,'dry','2019-07-23 16:29:48',3,NULL),
(12,11,'Shinchan Crayon',1,'dry','2019-07-23 16:43:47',3,NULL),
(17,12,'Mango flavor oil/liquid',5,'chill','2019-07-30 17:46:34',2,NULL),
(18,12,'Strawberry flavor oil/liquid',1,'chill','2019-08-02 10:52:15',2,NULL),
(19,13,'On The Way Ocha Traditional Flavor',5,'dry','2019-08-05 09:41:07',1,'2019-08-05 09:41:44');

/*Table structure for table `ticket_supplier` */

DROP TABLE IF EXISTS `ticket_supplier`;

CREATE TABLE `ticket_supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `supplier_address` text CHARACTER SET latin1 NOT NULL,
  `supplier_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `supplier_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_supplier` */

insert  into `ticket_supplier`(`supplier_id`,`supplier_name`,`supplier_address`,`supplier_phone`,`supplier_pic`) values 
(3,'Chibi Marukochan','Jl Chibi No 3, Jakarta','021666777','Chibi'),
(5,'Inuyasha tes','Jl Inuyasha No 8, Jakarta','021444555','Inuyasha');

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `user_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_type` */

insert  into `user_type`(`user_type_id`,`user_type_name`) values 
(1,'FNB'),
(2,'QA'),
(3,'KAHI'),
(4,'GM'),
(5,'Purchasing');

/*Table structure for table `view_rawmat` */

DROP TABLE IF EXISTS `view_rawmat`;

/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;
/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;

/*!50001 CREATE TABLE  `view_rawmat`(
 `request_id` int(11) ,
 `request_name` varchar(100) ,
 `request_date` datetime ,
 `deadline_date` datetime ,
 `sample_id` int(11) unsigned ,
 `sample_name` varchar(50) 
)*/;

/*View structure for view view_rawmat */

/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;
/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_rawmat` AS (select `ticket_request`.`request_id` AS `request_id`,`ticket_request`.`request_name` AS `request_name`,`ticket_request`.`request_date` AS `request_date`,`ticket_request`.`deadline_date` AS `deadline_date`,`ticket_sample`.`sample_id` AS `sample_id`,`ticket_sample`.`sample_name` AS `sample_name` from (`ticket_sample` left join `ticket_request` on((`ticket_sample`.`request_id` = `ticket_request`.`request_id`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
