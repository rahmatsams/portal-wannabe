/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - sushitei_ticket
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_ticket` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sushitei_ticket`;

/*Table structure for table `pa_category` */

DROP TABLE IF EXISTS `pa_category`;

CREATE TABLE `pa_category` (
  `category_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` char(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_parent_id` tinyint(2) unsigned DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `work_time` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `category_parent_id` (`category_parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;

/*Data for the table `pa_category` */

/*Table structure for table `pa_department` */

DROP TABLE IF EXISTS `pa_department`;

CREATE TABLE `pa_department` (
  `department_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(50) NOT NULL,
  `department_email` varchar(50) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pa_department` */

/*Table structure for table `pa_log` */

DROP TABLE IF EXISTS `pa_log`;

CREATE TABLE `pa_log` (
  `ticketlog_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticketlog_title` text NOT NULL,
  `ticket_id` mediumint(11) unsigned DEFAULT NULL,
  `user_id` smallint(3) unsigned NOT NULL,
  `ticketlog_type` varchar(20) NOT NULL,
  `ticketlog_content` text NOT NULL,
  `ticketlog_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticketlog_show` tinyint(1) NOT NULL,
  PRIMARY KEY (`ticketlog_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25150 DEFAULT CHARSET=utf8;

/*Data for the table `pa_log` */
/*Table structure for table `pa_main` */

DROP TABLE IF EXISTS `pa_main`;

CREATE TABLE `pa_main` (
  `ticket_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_title` varchar(50) NOT NULL,
  `ticket_type` tinyint(2) unsigned NOT NULL,
  `department_id` smallint(3) unsigned NOT NULL,
  `category` smallint(3) unsigned NOT NULL,
  `priority` smallint(3) unsigned NOT NULL,
  `impact` tinyint(1) unsigned NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `ticket_user` smallint(4) unsigned DEFAULT NULL,
  `ticket_creator` smallint(4) unsigned NOT NULL,
  `assigned_staff` smallint(4) unsigned DEFAULT NULL,
  `submit_date` datetime NOT NULL,
  `spare_part` varchar(50) NOT NULL,
  `problem_source` text NOT NULL,
  `resolved_date` datetime NOT NULL,
  `resolved_solution` text NOT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`ticket_id`),
  KEY `ticketType` (`ticket_type`),
  KEY `ticketCat` (`category`),
  KEY `assigned_staff` (`assigned_staff`),
  KEY `ticket_user` (`ticket_user`),
  KEY `priority` (`priority`),
  KEY `ticket_type` (`ticket_type`),
  KEY `department_id` (`department_id`),
  KEY `ticket_creator` (`ticket_creator`)
) ENGINE=InnoDB AUTO_INCREMENT=3720 DEFAULT CHARSET=utf8;

/*Data for the table `pa_main` */

/*Table structure for table `pa_priority` */

DROP TABLE IF EXISTS `pa_priority`;

CREATE TABLE `pa_priority` (
  `priority_id` smallint(13) unsigned NOT NULL AUTO_INCREMENT,
  `priority_name` varchar(25) NOT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pa_priority` */

/*Table structure for table `pa_status` */

DROP TABLE IF EXISTS `pa_status`;

CREATE TABLE `pa_status` (
  `status_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(25) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `pa_status` */

insert  into `pa_status`(`status_id`,`status_name`) values 
(1,'Open'),
(2,'On Progress'),
(3,'On-Hold'),
(5,'Solved'),
(6,'Re-Open'),
(7,'Closed'),
(8,'Forced-Close');

/*Table structure for table `pa_type` */

DROP TABLE IF EXISTS `pa_type`;

CREATE TABLE `pa_type` (
  `type_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(25) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_id` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `pa_type` */

insert  into `pa_type`(`type_id`,`type_name`) values 
(0,'Problem'),
(1,'Question'),
(3,'Request'),
(4,'Work Request');

CREATE TABLE `pa_upload` (
  `upload_id` SMALLINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `upload_name` VARCHAR(50) NOT NULL,
  `upload_location` VARCHAR(50) NOT NULL,
  `upload_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` MEDIUMINT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`upload_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
