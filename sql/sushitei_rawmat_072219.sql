/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - sushitei_rawmat
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_rawmat` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sushitei_rawmat`;

/*Table structure for table `request_type` */

DROP TABLE IF EXISTS `request_type`;

CREATE TABLE `request_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `request_type` */

insert  into `request_type`(`type_id`,`type_name`) values 
(1,'Promo Material'),
(2,'Alternatif Material'),
(3,'Pergantian Material');

/*Table structure for table `sample_log` */

DROP TABLE IF EXISTS `sample_log`;

CREATE TABLE `sample_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_date` datetime DEFAULT NULL,
  `log_content` text,
  `ticket_sample` int(11) unsigned NOT NULL COMMENT 'FK ticket_sample.sample_id',
  `sample_status` tinyint(3) unsigned NOT NULL COMMENT 'FK sample_status.status_id',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT 'FK user_type.type_id',
  `user` smallint(5) unsigned NOT NULL COMMENT 'FK sushitei_portal.portal_user.user_id',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_log` */

insert  into `sample_log`(`log_id`,`log_date`,`log_content`,`ticket_sample`,`sample_status`,`user_type`,`user`) values 
(1,'2019-07-22 17:12:55','reason 1',1,2,1,0);

/*Table structure for table `sample_status` */

DROP TABLE IF EXISTS `sample_status`;

CREATE TABLE `sample_status` (
  `status_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(15) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_status` */

insert  into `sample_status`(`status_id`,`status_name`) values 
(1,'Open'),
(2,'FNB Approve'),
(3,'FNB Reject'),
(4,'QA Approve'),
(5,'QA Reject'),
(6,'KAHI Release'),
(7,'KAHI Reject');

/*Table structure for table `ticket_document` */

DROP TABLE IF EXISTS `ticket_document`;

CREATE TABLE `ticket_document` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_id` int(11) NOT NULL,
  `document_type` tinyint(1) NOT NULL,
  `document_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `document_ext` varchar(10) CHARACTER SET latin1 NOT NULL,
  `document_location` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_document` */

/*Table structure for table `ticket_link` */

DROP TABLE IF EXISTS `ticket_link`;

CREATE TABLE `ticket_link` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_link` */

insert  into `ticket_link`(`link_id`,`supplier_id`,`producer_id`) values 
(1,1,4),
(2,2,5);

/*Table structure for table `ticket_producer` */

DROP TABLE IF EXISTS `ticket_producer`;

CREATE TABLE `ticket_producer` (
  `producer_id` int(11) NOT NULL AUTO_INCREMENT,
  `producer_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `producer_address` text CHARACTER SET latin1 NOT NULL,
  `producer_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `producer_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_producer` */

insert  into `ticket_producer`(`producer_id`,`producer_name`,`producer_address`,`producer_phone`,`producer_pic`) values 
(3,'PT Tes','Jl Tes Coba No 1','021456789','Fina'),
(4,'PT Halo','Jl Halo No 2 Jakarta','021333444','Pak Halo'),
(5,'PT Test Lagi 3','Jl test tes 3','021545678','tes pic 3');

/*Table structure for table `ticket_request` */

DROP TABLE IF EXISTS `ticket_request`;

CREATE TABLE `ticket_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `request_reason` varchar(50) CHARACTER SET latin1 NOT NULL,
  `request_date` datetime NOT NULL,
  `deadline_date` datetime NOT NULL,
  `request_user` smallint(4) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_request` */

insert  into `ticket_request`(`request_id`,`request_name`,`request_reason`,`request_date`,`deadline_date`,`request_user`,`last_update`) values 
(8,'test raw material request pertama','1','2019-07-09 00:00:00','2019-07-17 00:00:00',6520,NULL),
(9,'test','1','2019-07-09 14:10:22','2019-07-24 00:00:00',6520,NULL),
(10,'tes rahmat','1','2019-07-18 13:22:10','2019-07-23 00:00:00',6672,NULL);

/*Table structure for table `ticket_sample` */

DROP TABLE IF EXISTS `ticket_sample`;

CREATE TABLE `ticket_sample` (
  `sample_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `sample_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `link_id` int(11) NOT NULL,
  `sample_condition` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `sample_date` datetime DEFAULT NULL,
  `sample_status` tinyint(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`sample_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_sample` */

insert  into `ticket_sample`(`sample_id`,`request_id`,`sample_name`,`link_id`,`sample_condition`,`sample_date`,`sample_status`) values 
(1,8,'Blos #1',1,'dry','2019-07-18 11:43:55',1),
(2,8,'Sample 2',1,'chill','2019-07-18 12:03:02',2),
(3,10,'sample a',2,'chill','2019-07-18 13:22:22',1),
(4,10,'Sample 4',1,'chill','2019-07-18 13:22:32',5);

/*Table structure for table `ticket_supplier` */

DROP TABLE IF EXISTS `ticket_supplier`;

CREATE TABLE `ticket_supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `supplier_address` text CHARACTER SET latin1 NOT NULL,
  `supplier_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `supplier_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_supplier` */

insert  into `ticket_supplier`(`supplier_id`,`supplier_name`,`supplier_address`,`supplier_phone`,`supplier_pic`) values 
(1,'test sup','jl sup','0212222','tes ffina'),
(2,'test sup 2','Jl sup 2','021657986','sup 3');

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `user_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_type` */

insert  into `user_type`(`user_type_id`,`user_type_name`) values 
(1,'FNB'),
(2,'QA'),
(3,'KAHI'),
(4,'GM'),
(5,'Purchasing');

/*Table structure for table `view_rawmat` */

DROP TABLE IF EXISTS `view_rawmat`;

/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;
/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;

/*!50001 CREATE TABLE  `view_rawmat`(
 `request_id` int(11) ,
 `request_name` varchar(100) ,
 `request_date` datetime ,
 `deadline_date` datetime ,
 `sample_id` int(11) unsigned ,
 `sample_name` varchar(50) 
)*/;

/*View structure for view view_rawmat */

/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;
/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_rawmat` AS (select `ticket_request`.`request_id` AS `request_id`,`ticket_request`.`request_name` AS `request_name`,`ticket_request`.`request_date` AS `request_date`,`ticket_request`.`deadline_date` AS `deadline_date`,`ticket_sample`.`sample_id` AS `sample_id`,`ticket_sample`.`sample_name` AS `sample_name` from (`ticket_sample` left join `ticket_request` on((`ticket_sample`.`request_id` = `ticket_request`.`request_id`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
