/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - sushitei_rawmat
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_rawmat` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sushitei_rawmat`;

/*Table structure for table `document_link` */

DROP TABLE IF EXISTS `document_link`;

CREATE TABLE `document_link` (
  `link_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `list` smallint(5) unsigned DEFAULT NULL,
  `type` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `document_link` */

/*Table structure for table `document_type` */

DROP TABLE IF EXISTS `document_type`;

CREATE TABLE `document_type` (
  `type_id` smallint(2) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `document_type` */

/*Table structure for table `request_type` */

DROP TABLE IF EXISTS `request_type`;

CREATE TABLE `request_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `request_type` */

insert  into `request_type`(`type_id`,`type_name`) values 
(1,'Promo Material'),
(2,'Alternatif Material'),
(3,'Pergantian Material');

/*Table structure for table `sample_category` */

DROP TABLE IF EXISTS `sample_category`;

CREATE TABLE `sample_category` (
  `category_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  `list` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sample_category` */

/*Table structure for table `sample_condition` */

DROP TABLE IF EXISTS `sample_condition`;

CREATE TABLE `sample_condition` (
  `condition_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `condition_name` varchar(20) DEFAULT NULL,
  KEY `condition_id` (`condition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sample_condition` */

insert  into `sample_condition`(`condition_id`,`condition_name`) values 
(1,'Dry'),
(2,'Chill'),
(3,'Frozen');

/*Table structure for table `sample_list` */

DROP TABLE IF EXISTS `sample_list`;

CREATE TABLE `sample_list` (
  `list_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sample_list` */

/*Table structure for table `sample_log` */

DROP TABLE IF EXISTS `sample_log`;

CREATE TABLE `sample_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_date` datetime DEFAULT NULL,
  `log_content` text,
  `ticket_sample` int(11) unsigned NOT NULL COMMENT 'FK ticket_sample.sample_id',
  `sample_status` tinyint(3) unsigned NOT NULL COMMENT 'FK sample_status.status_id',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT 'FK user_type.type_id',
  `user` smallint(5) unsigned NOT NULL COMMENT 'FK sushitei_portal.portal_user.user_id',
  `registry_number` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_log` */

insert  into `sample_log`(`log_id`,`log_date`,`log_content`,`ticket_sample`,`sample_status`,`user_type`,`user`,`registry_number`) values 
(25,'2019-08-15 10:28:58','Lulus hasil uji oleh fnb ',27,2,1,6641,NULL),
(26,'2019-08-15 10:51:43','Hasil uji lolos oleh QA',27,4,2,6725,NULL),
(27,'2019-08-20 14:30:13','kahi release 27',27,6,3,6726,'TEST12345'),
(28,'2019-09-17 09:02:01','mango pertama rilis by fnb',29,2,1,6641,NULL),
(29,'2019-09-17 09:03:30','melamin chopstick reject by fnb',28,3,1,6641,NULL),
(30,'2019-09-17 09:07:30','mango release by QA',29,4,2,6725,NULL),
(31,'2019-09-17 09:10:58','seaweed whosale release by fnb 17 september 2019',26,2,1,6641,NULL),
(32,'2019-09-17 09:16:21','seaweed whosale reject by QA',26,5,2,6725,NULL),
(33,'2019-09-17 09:23:09','mango pertama release by kahi 170919',29,6,3,6726,'KAHI170919');

/*Table structure for table `sample_status` */

DROP TABLE IF EXISTS `sample_status`;

CREATE TABLE `sample_status` (
  `status_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(15) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_status` */

insert  into `sample_status`(`status_id`,`status_name`) values 
(1,'Open'),
(2,'FNB Release'),
(3,'FNB Reject'),
(4,'QA Release'),
(5,'QA Reject'),
(6,'KAHI Release'),
(7,'KAHI Reject');

/*Table structure for table `ticket_document` */

DROP TABLE IF EXISTS `ticket_document`;

CREATE TABLE `ticket_document` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_sample` int(11) NOT NULL,
  `document_type` varchar(10) NOT NULL,
  `document_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `document_ext` varchar(10) CHARACTER SET latin1 NOT NULL,
  `document_location` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_document` */

insert  into `ticket_document`(`document_id`,`ticket_sample`,`document_type`,`document_name`,`document_ext`,`document_location`) values 
(30,26,'image','15658390900.jpg','.jpg','Resources/images/sample\\'),
(31,26,'pdf','15658390910.pdf','.pdf','Resources/cert/sample\\'),
(32,27,'image','15658395360.jpg','.jpg','Resources/images/sample\\'),
(33,27,'pdf','15658395370.pdf','.pdf','Resources/cert/sample\\'),
(34,28,'image','15658404220.jpg','.jpg','Resources/images/sample\\'),
(35,28,'pdf','15658404230.pdf','.pdf','Resources/cert/sample\\'),
(36,29,'image','15686855040.jpg','.jpg','Resources/images/sample\\'),
(37,29,'pdf','15686855060.pdf','.pdf','Resources/cert/sample\\'),
(38,30,'image','15689640830.jpg','.jpg','Resources/images/sample\\');

/*Table structure for table `ticket_link` */

DROP TABLE IF EXISTS `ticket_link`;

CREATE TABLE `ticket_link` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_link` */

insert  into `ticket_link`(`link_id`,`supplier_id`,`producer_id`) values 
(1,3,4),
(5,5,3),
(6,6,5),
(7,7,6),
(8,8,7),
(9,9,7),
(10,10,8);

/*Table structure for table `ticket_producer` */

DROP TABLE IF EXISTS `ticket_producer`;

CREATE TABLE `ticket_producer` (
  `producer_id` int(11) NOT NULL AUTO_INCREMENT,
  `producer_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `producer_address` text CHARACTER SET latin1 NOT NULL,
  `producer_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `producer_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_producer` */

insert  into `ticket_producer`(`producer_id`,`producer_name`,`producer_address`,`producer_phone`,`producer_pic`) values 
(7,'Alibaba','Jakarta','021345667','Alibaba cs'),
(8,'Tokopedia','Jakarta','027767796','Tokopedia cs');

/*Table structure for table `ticket_request` */

DROP TABLE IF EXISTS `ticket_request`;

CREATE TABLE `ticket_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `request_type` tinyint(3) NOT NULL,
  `request_date` datetime NOT NULL,
  `deadline_date` datetime NOT NULL,
  `request_user` smallint(4) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_request` */

insert  into `ticket_request`(`request_id`,`request_name`,`request_type`,`request_date`,`deadline_date`,`request_user`,`last_update`) values 
(16,'Sushi Nori / seaweed',2,'2019-08-15 10:14:20','2019-09-02 00:00:00',6672,'2019-08-15 10:14:20'),
(17,'Chopstick Reusable Black',3,'2019-08-15 10:39:18','2019-09-27 00:00:00',6672,'2019-08-15 10:39:18'),
(18,'Fruit flavor Mango',3,'2019-09-16 09:19:25','2019-10-31 00:00:00',6672,'2019-09-16 09:19:25'),
(19,'Milk Tea Blueberry',1,'2019-09-16 09:20:56','2019-09-30 00:00:00',6672,'2019-09-16 09:20:56'),
(20,'Honey Flavor',2,'2019-09-16 10:15:57','2019-10-16 00:00:00',6672,'2019-09-16 10:15:57'),
(21,'Vegetarian Nugget',1,'2019-09-19 10:37:03','2019-11-14 00:00:00',6672,'2019-09-19 10:37:03'),
(22,'Papaya Milk Tea',1,'2019-09-19 10:37:28','2019-09-28 00:00:00',6672,'2019-09-19 10:37:28'),
(23,'Coconut Sirup',2,'2019-09-19 10:37:56','2019-10-02 00:00:00',6672,'2019-09-19 10:37:56'),
(24,'Matcha Jelly Pudding',2,'2019-09-19 10:40:03','2019-10-05 00:00:00',6672,'2019-09-19 10:40:03'),
(25,'Wasabi Mayonnaise',2,'2019-09-19 10:41:20','2019-10-31 00:00:00',6672,'2019-09-19 10:41:20'),
(26,'Tomato Ketchup',2,'2019-09-19 10:43:20','2019-09-28 00:00:00',6672,'2019-09-19 10:43:20');

/*Table structure for table `ticket_sample` */

DROP TABLE IF EXISTS `ticket_sample`;

CREATE TABLE `ticket_sample` (
  `sample_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `sample_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `link_id` int(11) NOT NULL,
  `sample_condition` tinyint(3) DEFAULT NULL,
  `sample_size` varchar(15) DEFAULT NULL,
  `sample_date` datetime DEFAULT NULL,
  `sample_status` tinyint(2) unsigned DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `category` smallint(5) unsigned DEFAULT NULL,
  `sample_price` decimal(12,0) DEFAULT NULL,
  PRIMARY KEY (`sample_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_sample` */

insert  into `ticket_sample`(`sample_id`,`request_id`,`sample_name`,`link_id`,`sample_condition`,`sample_size`,`sample_date`,`sample_status`,`last_update`,`category`,`sample_price`) values 
(26,16,'Roasted seaweed for whosale',8,1,'2 cartons','2019-08-15 10:18:11',5,NULL,NULL,NULL),
(27,16,'Roasted seaweed for yaki sushi nori',9,1,'1 carton','2019-08-15 10:25:37',6,NULL,NULL,NULL),
(28,17,'Melamine Chopstick Dinemate Hitam',10,1,'1 carton','2019-08-15 10:40:23',3,NULL,NULL,NULL),
(29,18,'Mango fruit flavor sample pertama',9,2,'2 kg','2019-09-17 08:58:26',6,'2019-09-17 08:59:00',NULL,NULL),
(30,18,'tes',10,3,'2 kg','2019-09-20 14:21:24',1,NULL,NULL,NULL);

/*Table structure for table `ticket_supplier` */

DROP TABLE IF EXISTS `ticket_supplier`;

CREATE TABLE `ticket_supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `supplier_address` text CHARACTER SET latin1 NOT NULL,
  `supplier_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `supplier_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_supplier` */

insert  into `ticket_supplier`(`supplier_id`,`supplier_name`,`supplier_address`,`supplier_phone`,`supplier_pic`) values 
(8,'Beijing Henin','Jakarta','0213445678','Beijing'),
(9,'Dalian Gaishi Food','Jakarta','0213334556','Dalian cs'),
(10,'Hanbao Jaya','Jakarta','0218848484','Hanbao cs');

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `user_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_type` */

insert  into `user_type`(`user_type_id`,`user_type_name`) values 
(1,'FNB'),
(2,'QA'),
(3,'KAHI'),
(4,'GM'),
(5,'Purchasing');

/*Table structure for table `view_rawmat` */

DROP TABLE IF EXISTS `view_rawmat`;

/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;
/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;

/*!50001 CREATE TABLE  `view_rawmat`(
 `request_id` int(11) ,
 `request_name` varchar(100) ,
 `request_date` datetime ,
 `deadline_date` datetime ,
 `sample_id` int(11) unsigned ,
 `sample_name` varchar(50) 
)*/;

/*View structure for view view_rawmat */

/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;
/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_rawmat` AS (select `ticket_request`.`request_id` AS `request_id`,`ticket_request`.`request_name` AS `request_name`,`ticket_request`.`request_date` AS `request_date`,`ticket_request`.`deadline_date` AS `deadline_date`,`ticket_sample`.`sample_id` AS `sample_id`,`ticket_sample`.`sample_name` AS `sample_name` from (`ticket_sample` left join `ticket_request` on((`ticket_sample`.`request_id` = `ticket_request`.`request_id`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
