/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.37-MariaDB : Database - sushitei_rawmat
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_rawmat` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sushitei_rawmat`;

/*Table structure for table `document_type` */

DROP TABLE IF EXISTS `document_type`;

CREATE TABLE `document_type` (
  `type_id` smallint(2) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `document_type` */

insert  into `document_type`(`type_id`,`type_name`) values 
(1,'SH'),
(2,'COA'),
(3,'FLOW'),
(4,'SPEC'),
(5,'FREE PORK'),
(6,'HALAL QUESTIONAIRE'),
(7,'ETC'),
(8,'Image');

/*Table structure for table `request_type` */

DROP TABLE IF EXISTS `request_type`;

CREATE TABLE `request_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `request_type` */

insert  into `request_type`(`type_id`,`type_name`) values 
(1,'Promo Material'),
(2,'Alternatif Material'),
(3,'Pergantian Material');

/*Table structure for table `sample_condition` */

DROP TABLE IF EXISTS `sample_condition`;

CREATE TABLE `sample_condition` (
  `condition_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `condition_name` varchar(20) DEFAULT NULL,
  KEY `condition_id` (`condition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sample_condition` */

insert  into `sample_condition`(`condition_id`,`condition_name`) values 
(1,'Dry'),
(2,'Chill'),
(3,'Frozen');

/*Table structure for table `sample_list` */

DROP TABLE IF EXISTS `sample_list`;

CREATE TABLE `sample_list` (
  `list_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sample_list` */

insert  into `sample_list`(`list_id`,`list_name`) values 
(1,'Positive'),
(2,'Non-positive'),
(3,'Tes List');

/*Table structure for table `sample_log` */

DROP TABLE IF EXISTS `sample_log`;

CREATE TABLE `sample_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_date` datetime DEFAULT NULL,
  `log_content` text,
  `ticket_sample` int(11) unsigned NOT NULL COMMENT 'FK ticket_sample.sample_id',
  `sample_status` tinyint(3) unsigned NOT NULL COMMENT 'FK sample_status.status_id',
  `user_type` tinyint(3) unsigned NOT NULL COMMENT 'FK user_type.type_id',
  `user` smallint(5) unsigned NOT NULL COMMENT 'FK sushitei_portal.portal_user.user_id',
  `registry_number` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_log` */

insert  into `sample_log`(`log_id`,`log_date`,`log_content`,`ticket_sample`,`sample_status`,`user_type`,`user`,`registry_number`) values 
(56,'2019-10-29 09:00:10','tes release fnb 37',37,2,1,6641,NULL);

/*Table structure for table `sample_status` */

DROP TABLE IF EXISTS `sample_status`;

CREATE TABLE `sample_status` (
  `status_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(15) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sample_status` */

insert  into `sample_status`(`status_id`,`status_name`) values 
(1,'Open'),
(2,'FNB Release'),
(3,'FNB Reject'),
(4,'QA Release'),
(5,'QA Reject'),
(6,'KAHI Release'),
(7,'KAHI Reject');

/*Table structure for table `ticket_document` */

DROP TABLE IF EXISTS `ticket_document`;

CREATE TABLE `ticket_document` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_sample` int(11) NOT NULL,
  `document_type` smallint(2) NOT NULL,
  `document_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `document_ext` varchar(10) CHARACTER SET latin1 NOT NULL,
  `document_location` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_document` */

insert  into `ticket_document`(`document_id`,`ticket_sample`,`document_type`,`document_name`,`document_ext`,`document_location`) values 
(60,37,8,'15722361050.jpg','jpg','Resources/images/sample\\'),
(61,37,1,'15723145870.pdf','pdf','Resources/cert/qa');

/*Table structure for table `ticket_producer` */

DROP TABLE IF EXISTS `ticket_producer`;

CREATE TABLE `ticket_producer` (
  `producer_id` int(11) NOT NULL AUTO_INCREMENT,
  `producer_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `producer_address` text CHARACTER SET latin1 NOT NULL,
  `producer_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `producer_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_producer` */

insert  into `ticket_producer`(`producer_id`,`producer_name`,`producer_address`,`producer_phone`,`producer_pic`) values 
(7,'Unilever','Jakarta','021345667','Unilever cs'),
(8,'Danone Group','Jakarta','027767796','Danone Group cs'),
(9,'NestlÃ©','Jakarta','0188888','NestlÃ© cs'),
(14,'PT. Indofood Sukses Makmur','Jakarta','021999884','Indofood cs'),
(15,'The Coca-Cola Company','Jakarta ','021333444','Coke cs');

/*Table structure for table `ticket_request` */

DROP TABLE IF EXISTS `ticket_request`;

CREATE TABLE `ticket_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `request_type` tinyint(3) NOT NULL,
  `request_date` datetime NOT NULL,
  `deadline_date` datetime NOT NULL,
  `request_user` smallint(4) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `request_closed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_request` */

insert  into `ticket_request`(`request_id`,`request_name`,`request_type`,`request_date`,`deadline_date`,`request_user`,`last_update`,`request_closed`) values 
(34,'Green Tea Flavor',2,'2019-10-28 11:12:54','2019-11-29 00:00:00',6672,'2019-10-28 11:12:54',NULL);

/*Table structure for table `ticket_sample` */

DROP TABLE IF EXISTS `ticket_sample`;

CREATE TABLE `ticket_sample` (
  `sample_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `sample_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `sample_condition` tinyint(3) DEFAULT NULL,
  `sample_size` varchar(15) DEFAULT NULL,
  `sample_date` datetime DEFAULT NULL,
  `sample_status` tinyint(2) unsigned DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `sample_price` decimal(12,0) DEFAULT NULL,
  `supplier` int(11) DEFAULT NULL,
  `producer` int(11) DEFAULT NULL,
  `sample_list` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`sample_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_sample` */

insert  into `ticket_sample`(`sample_id`,`request_id`,`sample_name`,`sample_condition`,`sample_size`,`sample_date`,`sample_status`,`last_update`,`sample_price`,`supplier`,`producer`,`sample_list`) values 
(37,34,'Green tea flavor with mint',1,'1 ton','2019-10-28 11:15:04',2,NULL,60000,21,7,1);

/*Table structure for table `ticket_supplier` */

DROP TABLE IF EXISTS `ticket_supplier`;

CREATE TABLE `ticket_supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `supplier_address` text CHARACTER SET latin1 NOT NULL,
  `supplier_phone` varchar(15) CHARACTER SET latin1 NOT NULL,
  `supplier_pic` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_supplier` */

insert  into `ticket_supplier`(`supplier_id`,`supplier_name`,`supplier_address`,`supplier_phone`,`supplier_pic`) values 
(19,'Tokopedia','Jakarta','0217766669','Tokopedia cs'),
(20,'Alfamidi','Jakarta','0218888855','Alfamidi cs'),
(21,'Indomaret','Jakarta','0217689541','Indomaret cs');

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `user_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user_type` */

insert  into `user_type`(`user_type_id`,`user_type_name`) values 
(1,'FNB'),
(2,'QA'),
(3,'KAHI'),
(4,'GM'),
(5,'Purchasing');

/*Table structure for table `view_rawmat` */

DROP TABLE IF EXISTS `view_rawmat`;

/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;
/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;

/*!50001 CREATE TABLE  `view_rawmat`(
 `request_id` int(11) ,
 `request_name` varchar(100) ,
 `request_date` datetime ,
 `deadline_date` datetime ,
 `sample_id` int(11) unsigned ,
 `sample_name` varchar(50) 
)*/;

/*View structure for view view_rawmat */

/*!50001 DROP TABLE IF EXISTS `view_rawmat` */;
/*!50001 DROP VIEW IF EXISTS `view_rawmat` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `view_rawmat` AS (select `ticket_request`.`request_id` AS `request_id`,`ticket_request`.`request_name` AS `request_name`,`ticket_request`.`request_date` AS `request_date`,`ticket_request`.`deadline_date` AS `deadline_date`,`ticket_sample`.`sample_id` AS `sample_id`,`ticket_sample`.`sample_name` AS `sample_name` from (`ticket_sample` left join `ticket_request` on((`ticket_sample`.`request_id` = `ticket_request`.`request_id`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
