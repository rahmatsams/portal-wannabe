-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Sep 2018 pada 09.59
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sushitei_prp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_email`
--

CREATE TABLE `ticket_email` (
  `email_id` smallint(5) UNSIGNED NOT NULL,
  `pic_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ticket_email`
--

INSERT INTO `ticket_email` (`email_id`, `pic_name`, `email`, `email_status`) VALUES
(1, 'Fina Alfiatul Jannah', 'it6.jkt@sushitei.co.id', 1),
(3, 'Oki Oktavianus', 'oktavianus@sushitei.co.id', 1),
(4, 'Reyza Eka Putra', 'reyza@sushitei.co.id', 1),
(5, 'Mike Chandra', 'm.chandra@sushitei.co.id', 1),
(6, 'Senior Bartender', 'senior.bartender@sushitei.co.id', 1),
(7, 'Nurvenda Lafian', 'ppfbspv2@sushitei.co.id', 1),
(8, 'Dede Mardinah', 'adminfbspv@sushitei.co.id', 1),
(9, 'Alam Suryana', 'alam@sushitei.co.id', 1),
(10, 'Thubagus', 'thubagus@sushitei.co.id', 1),
(11, 'Pepen Yulianto', 'fbspv2@sushitei.co.id', 1),
(12, 'Ade Gendawi', 'ade.gendawi@sushitei.co.id', 1),
(13, 'Wahyuni', 'wahyuni@sushitei.co.id', 1),
(14, 'Hamdi Layusa', 'hamdi@sushitei.co.id', 1),
(15, 'Yulis Setialianny Eka Azhari', 'yulis@sushitei.co.id', 1),
(16, 'Indra Oktapianus', 'indra@sushitei.co.id', 1),
(17, 'Dwi Purnomo', 'dwi.purnomo@sushitei.co.id', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ticket_email`
--
ALTER TABLE `ticket_email`
  ADD PRIMARY KEY (`email_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ticket_email`
--
ALTER TABLE `ticket_email`
  MODIFY `email_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
