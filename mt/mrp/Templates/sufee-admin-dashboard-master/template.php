<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=getConfig('site_name')?><?=(isset($page) ? " - {$page}" : '')?></title>
    <meta name="description" content="PRP, Sushi Tei">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/css/normalize.css">
    <link rel="stylesheet" href="./Resources/assets/bootstrap-4.0.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="./Resources/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/css/themify-icons.css">
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="./Templates/sufee-admin-dashboard-master/assets/scss/style.css">
    <link href="./Templates/sufee-admin-dashboard-master/assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <script src="./Templates/sufee-admin-dashboard-master/assets/js/vendor/jquery-2.1.4.min.js"></script>

</head>
<body>


        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand brand-logo" href="index.html">MRP</a>
                <a class="navbar-brand hidden" href="./">MRP</a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="<?=($site['root'] == 'home' ? 'active' : '')?>">
                        <a href="index.html"> <i class="menu-icon fa fa-dashboard" style="font-size:18px;color:LightGrey"></i>Dashboard </a>
                    </li>
                    <?php
                        if($session['group'] == 'Super Admin' || $session['group'] == 'Administrator' || $session['group'] == 'Admin MRP'){
                            echo '
                            <h3 class="menu-title">Admin</h3><!-- /.menu-title -->
                        
                                <li><a href="administrator.html"><i class="menu-icon fa fa-share-square-o" style="font-size:18px;color:LightGrey"></i>Administrator</a></li>
                            ';
                        }
                    ?>
                    <h3 class="menu-title">MRP</h3><!-- /.menu-title -->

                    <li>
                        <a href="submit_mrp.html"> <i class="menu-icon fa fa-edit" style="font-size:18px;color:LightGrey"></i>Submit MRP</a>
                    </li>
                    <li>
                        <a href="logout.html"> <i class="menu-icon ti-user" style="font-size:18px;color:LightGrey"></i>Logout </a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header" style="background:272c33; background-color:rgb(39, 44, 51);">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle nav-link text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Selamat Datang, <?=$session['username']?>
                    </a>
                </div>
                
            </div>

        </header><!-- /header -->
        <!-- Header-->

        <div class="content mt-3">

        <?php
            $this->view($_action, $_passed_var);
            $this->render();
        ?>

        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->

    <script src="./Templates/sufee-admin-dashboard-master/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="./Templates/sufee-admin-dashboard-master/assets/js/plugins.js"></script>
    <script src="./Templates/sufee-admin-dashboard-master/assets/js/main.js"></script>
<?php
    if(isset($option['injs']) && count($option['injs']) > 0){
        foreach($option['injs'] as $js){
            echo "
    <script src='{$_template_path}/{$js}'></script>";
        }
    }
    if(isset($option['exjs']) && count($option['exjs']) > 0){
        foreach($option['exjs'] as $js){
            echo "
    <script src='{$js}'></script>";
        }
    }
?>
</body>
</html>
