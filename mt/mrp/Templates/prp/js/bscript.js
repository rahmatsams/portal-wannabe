$(document).ready(function(){
	$("#newCatering").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#newCatering");
		submitNewCatering(me, room);
	});
	$("#editCatering").submit(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $("#editCatering");
		submitEditCatering(me, room);
	});
	$(".editCatering").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		cateringData(me, room);
	});
	$(".deleteRoom").click(function(event){
		event.preventDefault();
		var me = $(this);
		var room = $(this).data('id');
		submitDeleteRoom(me, room);
	});
});
function submitNewCatering(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_new_catering.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				$("#failedModal").toggle('modal');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function submitEditCatering(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "submit_edit_catering.html",
		dataType: "json",
		data: room.serialize(),
		success: function(resp){
			if(resp.success == 1)    {
				alert('Success');
				location.reload();
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
function cateringData(me, room){
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	
	$.ajax({
		type: "POST",
		url: "catering_data.html",
		dataType: "json",
		data: 'provider_id='+room,
		success: function(resp){
			if(resp.success == 1)    {
				$("#eCateringName").val(resp.data.provider_name);
				$("#eCateringAddress").val(resp.data.provider_address);
				$("#eCateringPhone").val(resp.data.provider_phone);
				$("#eCateringID").val(resp.data.provider_id);
				$('#editCateringModal').modal('show');
			} else if(resp.success == 0)    {
				alert('failed');
			}else {
				alert('Unknown Error');
			}
		},
		complete: function() {
			me.data('requestRunning', false);
		},
	});
	return false;
}
