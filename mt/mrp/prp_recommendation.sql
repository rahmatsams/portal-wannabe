/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.21-MariaDB : Database - sushitei_prp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `sushitei_prp`;

/*Table structure for table `ticket_recommendation` */

DROP TABLE IF EXISTS `ticket_recommendation`;

CREATE TABLE `ticket_recommendation` (
  `recommendation_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `recommendation_content` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_feedback` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_image` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_status` tinyint(1) DEFAULT NULL,
  `ticket_group` mediumint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`recommendation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_recommendation` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
