/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.21-MariaDB : Database - prp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`prp` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `prp`;

/*Table structure for table `area_category_link` */

DROP TABLE IF EXISTS `area_category_link`;

CREATE TABLE `area_category_link` (
  `link_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `category` smallint(5) unsigned DEFAULT NULL,
  `area` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8;

/*Data for the table `area_category_link` */

insert  into `area_category_link`(`link_id`,`category`,`area`) values 
(1,11,1),
(2,12,1),
(3,14,1),
(4,15,1),
(5,16,2),
(6,17,2),
(7,18,2),
(8,20,3),
(9,21,3),
(10,22,3),
(11,23,3),
(12,24,3),
(13,25,3),
(14,26,3),
(15,27,3),
(16,28,3),
(17,29,3),
(18,30,3),
(19,31,3),
(20,32,3),
(21,33,3),
(22,34,3),
(23,35,3),
(24,36,3),
(25,37,3),
(26,38,3),
(27,39,3),
(28,40,3),
(29,41,3),
(30,42,3),
(31,43,3),
(32,44,4),
(33,45,4),
(34,46,4),
(35,47,4),
(36,48,4),
(37,49,4),
(38,50,4),
(39,51,4),
(40,52,4),
(41,53,4),
(42,54,5),
(43,55,5),
(44,56,5),
(45,57,5),
(46,58,5),
(47,59,6),
(48,60,6),
(49,61,6),
(50,62,6),
(51,63,6),
(52,64,6),
(53,65,6),
(54,66,6),
(55,67,6),
(56,68,6),
(57,69,6),
(58,70,6),
(59,71,6),
(60,72,6),
(61,73,7),
(62,74,7),
(63,75,7),
(64,76,7),
(65,77,7),
(66,78,7),
(67,79,7),
(68,80,7),
(69,81,7),
(70,82,8),
(71,83,8),
(72,84,8),
(73,85,8),
(74,86,8),
(75,87,8),
(76,88,8),
(77,89,8),
(78,90,8),
(79,91,8),
(80,92,8),
(81,93,8),
(82,94,9),
(83,95,9),
(84,96,9),
(85,97,9),
(86,98,9),
(87,99,9),
(88,100,10),
(89,101,10),
(90,102,10),
(91,103,10),
(92,104,10),
(93,105,10),
(94,106,10),
(95,107,10),
(96,108,10),
(97,109,10),
(98,110,10),
(99,111,1),
(100,112,1),
(101,113,1),
(102,114,1),
(103,115,1),
(104,116,1),
(105,117,1),
(106,118,1),
(107,119,1),
(108,120,1),
(109,121,1),
(110,122,1),
(111,123,1),
(112,124,1),
(113,125,1),
(114,126,1),
(115,127,1),
(116,128,1),
(117,129,1),
(118,130,1),
(119,131,1),
(120,132,1),
(121,133,1),
(122,134,1),
(123,135,1),
(124,136,1),
(125,137,1),
(126,138,1),
(127,139,1),
(128,140,1),
(129,141,0),
(130,142,141),
(131,143,141),
(132,144,141),
(133,145,141),
(134,146,141),
(135,147,141),
(136,149,141),
(137,150,141),
(138,151,141),
(139,152,141);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
