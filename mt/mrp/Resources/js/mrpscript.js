jQuery(document).ready(function()
{
    jQuery(".selectHasil").change(function(event)
    {
        var data_id = jQuery(this).data('id');
        if(jQuery(this).val() == "1")
        {
            jQuery('.selectTindakan[data-id="'+data_id+'"]').attr('disabled', false);
        }else{
            jQuery('.selectTindakan[data-id="'+data_id+'"]').attr('disabled', true);
        }
        checkGroupExists(me);
    });

    jQuery("#selectArea").change(function(event)
    {
        event.preventDefault();
        updateSub();
        var me = jQuery(this);
        if(me.val() != 'All'){
            jQuery(".table-area:not([data-area='"+me.val()+"'])").addClass('d-none');
            jQuery(".table-area[data-area='"+me.val()+"']").removeClass('d-none');;
        }else{
            jQuery('.table-area').removeClass('d-none');
            /*jQuery('.table-area').empty('d-none');*/        
        }
        checkGroupExists(me);
    });
    
    jQuery("#selectOutlet").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        checkGroupExists(me);
    });
    
    jQuery("#selectMonth").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        checkGroupExists(me);
    });

    jQuery("#selectWeek").change(function(event){
        event.preventDefault();
        var me = jQuery(this);
        checkGroupExists(me);
        updateSub();
    });

     jQuery("#submitMRP").submit(function(event){
        if(submitted == 1){
            event.preventDefault();
        }else{
            jQuery('input[type="file"]', jQuery(this)).each(function(){
                if(jQuery(this).val() == ""){
                    jQuery(this).prop("disabled", true);
                }
                return true;
            });
        }
    });

    jQuery('#submitMRP input[type="file"]').change(function(event){
        if(jQuery(this).val() != ""){
            readURL(this, jQuery(this));
        }else{
            jQuery(this).parent().find('label').html('Select File');
            jQuery(this).parent().find('img').remove();
        }
    });

    jQuery("#selectCategory").change(function(event){
        event.preventDefault();
        var me = jQuery(this);
            if(me.val() != 'All'){
                jQuery(".table-area:not([data-category='"+me.val()+"'])").addClass('d-none');
                jQuery(".table-area[data-category='"+me.val()+"']").removeClass('d-none');
            }else{
                jQuery('.table-area').removeClass('d-none');
            }
    });

    jQuery(".file-trigger").click(function(event){
            var me = jQuery(this);
            var parent_parent = me.parent().parent();
            console.log(me.val()+ 'value');
            if(me.val()){
                parent_parent.removeClass('bg-success');
                parent_parent.addClass('bg-warning');
                parent_parent.addClass('text-white');
            }else{
                parent_parent.removeClass('bg-warning');
                parent_parent.removeClass('text-white');
            }
        });
        
    jQuery("#addFile").click(function(event){
            event.preventDefault();
            var me = jQuery(this);
            var html = '<div class="input-group">'+
            '               <input name="image_upload[]" type="file" class="form-control form-control-file" accept="image/*">'+
            '           </div><br/>';
            jQuery("#fileForm").append(html);
    });
});

function checkGroupExists(me){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
    var dataid = me.data("id");
    var outlet = jQuery("#selectOutlet").val();
    var month = jQuery("#selectMonth").val();
    var week = jQuery("#selectWeek").val();
    jQuery.ajax({
        type: "POST",
        url: "group_data.html",
        data: 'outlet='+outlet+'&month='+month+'&week='+week,
		dataType: "json",
        success: function(json){
            if(json.exists==1){
                // window.location.replace(json.url);
                submitted = 1;
                jQuery("#submitButton").attr('disabled','disabled');
                jQuery("#statusModal .modal-body").html('This outlet MRP Week is already submitted, click <a href="'+json.url+'">Here</a> to edit.');
                jQuery("#statusModal").modal('show');
            }else{
                submitted = 0;
                jQuery("#submitButton").removeAttr('disabled');
            }
        },
        complete: function() {
			me.data('requestRunning', false);
		},
    });
    return false;
}

function updateSub()
{
    if($("#selectWeek").val() == "bulanan"){
        var target = 'area_data_monthly.html';
    }else{
        var target = 'area_data.html';
    }
    $area_id = $("#selectArea").val();
    $.ajax({
      type: "POST",
      url: target,
      data: "area_id="+$area_id,
      dataType: 'json',
      success: function(json) {
        $('#selectCategory').html(json.category);
        $('#selectCategory').removeAttr('disabled');
        $('#tableForm').html(json.content);
      }
    });
}

function readURL(input, input2) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        input2.parent().find('label').html('Change');
        input2.parent().find('img').remove();
        input2.parent().append("<img src='"+e.target.result+"' style='width:50px; height:50px;'>");
    }
    reader.readAsDataURL(input.files[0]);
  }
}

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

var refresh_page = function() {
  
    jQuery.ajax({
        type: "POST",
        url: "refresh_user.html",
        data: 'group=1',
		dataType: "json",
        
        complete: function() {
			console.log('page refreshed');
		}
    });
  
};

var interval = 1000 * 60 * 7; // where X is your every X minutes

setInterval(refresh_page, interval);
