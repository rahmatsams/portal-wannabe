/*DELETE CATEGORY*/
jQuery(document).ready(function(){
    jQuery(".delete_cat").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#admin_category_delete").attr("href", "admin_category_delete_"+id+".html"); 
    });
});

/*DELETE RECOMMENDATION*/
jQuery(document).ready(function(){
    jQuery(".delete_recom").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        var cat_id = $("#catid").val();
        jQuery("#admin_recom_delete").attr("href", "admin_recom_delete_"+id+"_"+cat_id+".html"); 
    });
});

/*DELETE EMAIL*/
jQuery(document).ready(function(){
    jQuery(".delete_email").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#admin_delete_email").attr("href", "admin_delete_email_"+id+".html"); 
    });
});
