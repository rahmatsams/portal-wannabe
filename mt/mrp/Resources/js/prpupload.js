$(document).ready(function(){
    $("#selectCategory").change(function(event){
        event.preventDefault();
		var me = $(this);
        loadSub(me);
    });

});


function loadSub(me){
    $.ajax({
        type: "POST",
        url: "sub_data.html",
        data: me.serialize(),
		dataType: "json",
        success: function(json){
            if(json.success==0){
                $("#selectSub").addClass("sr-only");
				$("#selectSub").addAttr('disabled');
            } else {
				
                $("#selectSub").removeClass("sr-only");
                $("#selectSub").html("");
				$("#selectSub").append('<option value="">--Select Point--</option>');
                $(json.data).each(function() {
                    var row_data = '<option value="'+this.category_id+'">'+this.category_name+'</option>';
                    $("#selectSub").append(row_data);
                });
				$("#selectSub").removeAttr('disabled');
            }
        },
        complete: function() {
			me.data('requestRunning', false);
		},
    });
    return false;
}