﻿$(document).ready(function(){
    $(".manage-button").click(function(event){
        event.preventDefault();
		var bid = "#formEdit"+$(this).data('id');
		if($(bid).length > 0){
			$(bid+" td > input[type!='hidden']" ).removeAttr('readonly');
			$(bid+" td > input[type!='hidden']" ).removeClass('form-control-plaintext');
			$(bid+" td > input[type!='hidden']" ).addClass('form-control');

			$(bid+" td > textarea" ).removeAttr('readonly');
			$(bid+" td > textarea" ).removeClass('form-control-plaintext');
			$(bid+" td > textarea" ).addClass('form-control');
			$(bid+" td a.manage-button").addClass('d-none');
			$(bid+" td input[type='submit']").removeClass('d-none');
            $(bid+" td input[type='submit']").removeClass('form-control');
		}else{
			alert('no '+bid);
		}
    });
    $(".delete-button").click(function(event){
        event.preventDefault();
		var bid = "#formEdit"+$(this).data('id');
		if($(bid).length > 0){
			$(bid+" td > input[type!='hidden']" ).removeAttr('readonly');
			$(bid+" td > input[type!='hidden']" ).removeClass('form-control-plaintext');
			$(bid+" td > input[type!='hidden']" ).addClass('form-control');

			$(bid+" td > textarea" ).removeAttr('readonly');
			$(bid+" td > textarea" ).removeClass('form-control-plaintext');
			$(bid+" td > textarea" ).addClass('form-control');
			$(bid+" td a.manage-button").addClass('d-none');
			$(bid+" td input[type='submit']").removeClass('d-none');
            $(bid+" td input[type='submit']").removeClass('form-control');
		}else{
			alert('no '+bid);
		}
    });
});