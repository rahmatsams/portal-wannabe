function doSubmitSearch(){
    jQuery("#searchMRP").submit();
}


function loadData(me){
    if ( me.data('requestRunning') ) {
        return;
    }
    
    me.data('requestRunning', true);
    
    jQuery.ajax({
        type: "POST",
        url: "index_data.html",
        data: me.serialize(),
		dataType: "json",
        success: function(json){
            if(json.success==0){
                jQuery("#selectSub").addClass("sr-only");
				jQuery("#selectSub").addAttr('disabled');
            } else {
                var item = '';
                var l = 1;
                jQuery(json.data).each(function() {
                    var f = 0;
                    /*if(this.total_deduction != null){
                        this.default_point = this.default_point - this.group_point;
                    }*/
                    if(this.group_finding != null && this.group_id != null){
                        this.store_name = '<a href="view_mrp_'+this.store_id+'_'+this.group_year+'_'+this.group_month+'.html" class="btn btn-primary btn-block">'+this.store_name+'</a>';
                    }
                    
                    item += '<tr>'+
                    '   <td>'+l+'</td>'+
                    '   <td>'+this.store_name+'</td>'+
                    '   <td><input type="hidden" value="'+this.group_status+'"/></td>'+
                    '<tr>';
                    l++;
                });
                jQuery("table#indexContent tbody").html(item);
            }
        },
        complete: function() {
			me.data('requestRunning', false);
		},
    });
    return false;
}

jQuery(document).ready(function(){
    jQuery("#selectMonth").change(function(event){
        doSubmitSearch();
    });
    jQuery("#searchMRP").submit(function(event){
        event.preventDefault();
        loadData(jQuery(this));
    });
    
});