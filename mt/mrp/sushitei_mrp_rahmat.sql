/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.21-MariaDB : Database - sushitei_mrp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sushitei_mrp` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sushitei_mrp`;

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `location_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `location_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `location_status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Data for the table `location` */

insert  into `location`(`location_id`,`location_name`,`location_status`) values 
(1,'Jakarta',1),
(2,'Surabaya',1),
(3,'Makasar',1),
(4,'Palembang',1),
(5,'Bali',1),
(6,'Bandung',1),
(7,'Jogja',1),
(8,'Medan',1),
(9,'Pekan Baru',1),
(10,'Batam',1),
(11,'Bengkulu',1);

/*Table structure for table `store` */

DROP TABLE IF EXISTS `store`;

CREATE TABLE `store` (
  `store_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `store_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `store_email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `store_status` tinyint(1) unsigned NOT NULL,
  `location_id` smallint(3) unsigned NOT NULL,
  `default_point` smallint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`store_id`),
  KEY `store_id` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4;

/*Data for the table `store` */

insert  into `store`(`store_id`,`store_name`,`store_email`,`store_status`,`location_id`,`default_point`) values 
(1,'Plaza Indonesia','',1,1,100),
(2,'Plaza Senayan','',1,1,100),
(3,'Pondok Indah Mall 2','',1,1,100),
(4,'Senayan City','',1,1,100),
(5,'Mall Kelapa Gading','',1,1,100),
(6,'Emporium Pluit','',1,1,100),
(7,'Central Park','test.jkt@sushitei.co.id',1,1,100),
(8,'Gandaria City','',1,1,100),
(9,'Flavour Bliss','',1,1,100),
(10,'Supermall Karawaci','',1,1,100),
(11,'Grand Indonesia','',1,1,100),
(12,'Lotte Avenue','manager.pim@sushitei.co.id',1,1,100),
(13,'Kota Kasablanka','',1,1,100),
(14,'Summarecon Bekasi','',1,1,100),
(15,'Bintaro Xchange','it6.jkt@sushitei.co.id',1,1,100),
(16,'The Breeze','',1,1,100),
(17,'Puri Mall','',1,1,100),
(18,'Summarecon Serpong','',1,1,100),
(19,'Margo City','',1,1,100),
(20,'Kemang Village','',1,1,100),
(100,'Sushi Kiosk Puri','',1,1,100),
(103,'aa test test store','it6.jkt@sushitei.co.id',1,1,NULL);

/*Table structure for table `store_area` */

DROP TABLE IF EXISTS `store_area`;

CREATE TABLE `store_area` (
  `area_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `area_desc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `area_status` tinyint(3) unsigned NOT NULL,
  `area_multiple` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`area_id`),
  KEY `area_id` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

/*Data for the table `store_area` */

insert  into `store_area`(`area_id`,`area_name`,`area_desc`,`area_status`,`area_multiple`) values 
(16,'Walk-In Freezer','',1,NULL);

/*Table structure for table `ticket_category` */

DROP TABLE IF EXISTS `ticket_category`;

CREATE TABLE `ticket_category` (
  `category_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `area_id` smallint(5) unsigned NOT NULL,
  `category_monthly` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_category` */

insert  into `ticket_category`(`category_id`,`category_name`,`area_id`,`category_monthly`,`deleted`) values 
(221,'Sistem Panel',16,0,0),
(222,'Storage',16,0,0),
(223,'Condensing Unit',16,0,0),
(224,'Lain-lain',16,0,0),
(228,'Bulanan',16,1,0);

/*Table structure for table `ticket_condition` */

DROP TABLE IF EXISTS `ticket_condition`;

CREATE TABLE `ticket_condition` (
  `condition_id` int(11) NOT NULL AUTO_INCREMENT,
  `condition_name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `category_id` smallint(5) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_condition` */

insert  into `ticket_condition`(`condition_id`,`condition_name`,`category_id`) values 
(24,'Baut terminal listrik kencang',221),
(25,'Indikator Digital berfungsi dengan baik',221),
(26,'Indikator Lampu berfungsi dengan baik',221),
(27,'Saklar berfungsi dengan baik',221),
(28,'Kondisi Panel bersih tidak berdebu',221),
(29,'Cabin Unit dalam keadaan bersih',222),
(30,'Handle pintu berfungsi dengan baik',222),
(31,'Gasket Pintu berfungsi dengan baik',222),
(32,'Engsel Pintu berfungsi dengan baik',222),
(33,'Tidak ada Ice Blocking pada Evaporator',222),
(34,'Putaran Fan Evaporator normal',222),
(35,'Heater Defrost menghasilkan panas yang cukup',222),
(36,'Pemeriksaan Instalasi Refrigerant',223),
(37,'Pemeriksaan pada Fan Condensor',223),
(38,'Pengukuran arus listrik pada compressor',223),
(39,'Kapasitas produk tertata rapi',224),
(40,'Sink untuk cuci tangan sudah lengkap dan sesuai, bersih dan rapi',225),
(41,'Penampilan staff sesuai, baik, dan bersih',226),
(42,'test 123',227),
(43,'Sirkulasi udara tidak bermasalah',224),
(44,'Tes bulanan',228);

/*Table structure for table `ticket_email` */

DROP TABLE IF EXISTS `ticket_email`;

CREATE TABLE `ticket_email` (
  `email_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pic_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_email` */

/*Table structure for table `ticket_group` */

DROP TABLE IF EXISTS `ticket_group`;

CREATE TABLE `ticket_group` (
  `group_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_store` smallint(3) unsigned DEFAULT NULL,
  `group_is_month` tinyint(1) unsigned DEFAULT NULL,
  `group_year` smallint(2) unsigned DEFAULT NULL,
  `group_month` smallint(2) unsigned DEFAULT NULL,
  `group_week` tinyint(1) NOT NULL,
  `group_submit_date` datetime NOT NULL,
  `group_finding` smallint(2) unsigned DEFAULT NULL,
  `group_user` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `group_auditee` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `group_point` smallint(3) unsigned DEFAULT NULL,
  `group_status` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `group_equipment` smallint(5) unsigned DEFAULT NULL,
  `group_notes` varchar(255) NOT NULL,
  `equipment_code` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_group` */

insert  into `ticket_group`(`group_id`,`group_store`,`group_is_month`,`group_year`,`group_month`,`group_week`,`group_submit_date`,`group_finding`,`group_user`,`group_auditee`,`group_point`,`group_status`,`group_equipment`,`group_notes`,`equipment_code`) values 
(1,1,0,2018,12,2,'2018-12-06 13:26:13',17,'Rahmat Samsudin','Fina',0,'Normal',16,'tes aja','AC3');

/*Table structure for table `ticket_log` */

DROP TABLE IF EXISTS `ticket_log`;

CREATE TABLE `ticket_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_title` text CHARACTER SET utf8 NOT NULL,
  `ticket` mediumint(11) unsigned DEFAULT NULL,
  `user` smallint(3) unsigned NOT NULL,
  `log_type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `log_content` text CHARACTER SET utf8 NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `visible` tinyint(1) NOT NULL,
  KEY `log_id` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_log` */

/*Table structure for table `ticket_main` */

DROP TABLE IF EXISTS `ticket_main`;

CREATE TABLE `ticket_main` (
  `ticket_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_group` mediumint(11) unsigned DEFAULT NULL,
  `category` smallint(5) unsigned NOT NULL,
  `ticket_condition` int(11) NOT NULL,
  `actions` int(11) NOT NULL,
  `ticket_creator` smallint(4) unsigned NOT NULL,
  `submit_date` date DEFAULT NULL,
  `finding_images` text CHARACTER SET utf8,
  `resolved_date` date DEFAULT NULL,
  `resolved_images` text CHARACTER SET utf8,
  `resolved_solution` text CHARACTER SET utf8,
  `status` tinyint(1) NOT NULL,
  `last_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `store` smallint(3) DEFAULT NULL,
  `notes` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`ticket_id`),
  KEY `ticket_id` (`ticket_id`),
  KEY `ticket_group` (`ticket_group`),
  CONSTRAINT `ticket_main_ibfk_1` FOREIGN KEY (`ticket_group`) REFERENCES `ticket_group` (`group_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ticket_main` */

insert  into `ticket_main`(`ticket_id`,`ticket_group`,`category`,`ticket_condition`,`actions`,`ticket_creator`,`submit_date`,`finding_images`,`resolved_date`,`resolved_images`,`resolved_solution`,`status`,`last_update`,`store`,`notes`) values 
(1,1,221,24,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(2,1,221,25,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(3,1,221,26,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(4,1,221,27,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(5,1,221,28,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(6,1,222,35,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(7,1,222,29,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(8,1,222,30,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(9,1,222,31,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(10,1,222,32,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(11,1,222,33,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(12,1,222,34,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(13,1,223,36,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(14,1,223,37,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(15,1,223,38,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(16,1,224,39,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes'),
(17,1,224,43,0,1001,'2018-12-06',NULL,NULL,NULL,NULL,1,NULL,1,'tes');

/*Table structure for table `ticket_status` */

DROP TABLE IF EXISTS `ticket_status`;

CREATE TABLE `ticket_status` (
  `status_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(25) NOT NULL,
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_status` */

insert  into `ticket_status`(`status_id`,`status_name`) values 
(1,'Normal'),
(1,'Finding'),
(2,'Resolved');

/*Table structure for table `ticket_type` */

DROP TABLE IF EXISTS `ticket_type`;

CREATE TABLE `ticket_type` (
  `type_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(25) NOT NULL,
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ticket_type` */

insert  into `ticket_type`(`type_id`,`type_name`) values 
(1,'Finding'),
(2,'Resolved');

/*Table structure for table `ticket_upload` */

DROP TABLE IF EXISTS `ticket_upload`;

CREATE TABLE `ticket_upload` (
  `upload_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `upload_name` varchar(50) NOT NULL,
  `upload_location` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` mediumint(11) unsigned NOT NULL,
  `upload_temp` tinyint(1) DEFAULT '0',
  `upload_user` smallint(4) unsigned DEFAULT NULL,
  KEY `upload_id` (`upload_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ticket_upload` */

/*Table structure for table `view_ticket` */

DROP TABLE IF EXISTS `view_ticket`;

/*!50001 DROP VIEW IF EXISTS `view_ticket` */;
/*!50001 DROP TABLE IF EXISTS `view_ticket` */;

/*!50001 CREATE TABLE  `view_ticket`(
 `ticket_id` mediumint(11) unsigned ,
 `ticket_group` mediumint(11) unsigned ,
 `condition_id` int(11) ,
 `condition_name` varchar(100) ,
 `group_week` tinyint(1) ,
 `status` tinyint(1) ,
 `category_id` smallint(5) unsigned ,
 `category_name` varchar(255) ,
 `store` smallint(3) ,
 `store_name` varchar(30) ,
 `default_point` smallint(3) unsigned ,
 `creator` smallint(5) unsigned ,
 `creator_name` varchar(50) ,
 `submit_date` date ,
 `last_update` datetime ,
 `finding_images` text ,
 `resolved_images` text ,
 `resolved_date` date ,
 `resolved_solution` text ,
 `area_id` smallint(5) ,
 `area` varchar(50) ,
 `notes` text ,
 `submit_image` varchar(50) 
)*/;

/*View structure for view view_ticket */

/*!50001 DROP TABLE IF EXISTS `view_ticket` */;
/*!50001 DROP VIEW IF EXISTS `view_ticket` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ticket` AS select `sushitei_mrp`.`ticket_main`.`ticket_id` AS `ticket_id`,`sushitei_mrp`.`ticket_main`.`ticket_group` AS `ticket_group`,`sushitei_mrp`.`ticket_main`.`ticket_condition` AS `condition_id`,`sushitei_mrp`.`ticket_condition`.`condition_name` AS `condition_name`,`sushitei_mrp`.`ticket_group`.`group_week` AS `group_week`,`sushitei_mrp`.`ticket_main`.`status` AS `status`,`sushitei_mrp`.`ticket_main`.`category` AS `category_id`,`sushitei_mrp`.`ticket_category`.`category_name` AS `category_name`,`sushitei_mrp`.`ticket_main`.`store` AS `store`,`sushitei_mrp`.`store`.`store_name` AS `store_name`,`sushitei_mrp`.`store`.`default_point` AS `default_point`,`creator`.`user_id` AS `creator`,`creator`.`display_name` AS `creator_name`,`sushitei_mrp`.`ticket_main`.`submit_date` AS `submit_date`,`sushitei_mrp`.`ticket_main`.`last_update` AS `last_update`,`sushitei_mrp`.`ticket_main`.`finding_images` AS `finding_images`,`sushitei_mrp`.`ticket_main`.`resolved_images` AS `resolved_images`,`sushitei_mrp`.`ticket_main`.`resolved_date` AS `resolved_date`,`sushitei_mrp`.`ticket_main`.`resolved_solution` AS `resolved_solution`,`sushitei_mrp`.`store_area`.`area_id` AS `area_id`,`sushitei_mrp`.`store_area`.`area_name` AS `area`,`sushitei_mrp`.`ticket_main`.`notes` AS `notes`,`si`.`upload_name` AS `submit_image` from (((((((`sushitei_mrp`.`ticket_main` left join `sushitei_mrp`.`ticket_category` on((`sushitei_mrp`.`ticket_main`.`category` = `sushitei_mrp`.`ticket_category`.`category_id`))) left join `sushitei_mrp`.`store_area` on((`sushitei_mrp`.`store_area`.`area_id` = `sushitei_mrp`.`ticket_category`.`area_id`))) left join `sushitei_ticketdesk`.`users` `creator` on((`sushitei_mrp`.`ticket_main`.`ticket_creator` = `creator`.`user_id`))) left join `sushitei_mrp`.`store` on((`sushitei_mrp`.`ticket_main`.`store` = `sushitei_mrp`.`store`.`store_id`))) left join `sushitei_mrp`.`ticket_upload` `si` on((`si`.`ticket_id` = `sushitei_mrp`.`ticket_main`.`ticket_id`))) left join `sushitei_mrp`.`ticket_group` on((`sushitei_mrp`.`ticket_group`.`group_id` = `sushitei_mrp`.`ticket_main`.`ticket_group`))) left join `sushitei_mrp`.`ticket_condition` on((`sushitei_mrp`.`ticket_main`.`ticket_condition` = `sushitei_mrp`.`ticket_condition`.`condition_id`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
