-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2018 at 09:20 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sushitei_prp`
--
CREATE DATABASE IF NOT EXISTS `sushitei_prp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sushitei_prp`;

-- --------------------------------------------------------

--
-- Table structure for table `area_category_link`
--

CREATE TABLE `area_category_link` (
  `link_id` mediumint(6) UNSIGNED NOT NULL,
  `category` smallint(5) UNSIGNED DEFAULT NULL,
  `area` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `area_category_link`
--

INSERT INTO `area_category_link` (`link_id`, `category`, `area`) VALUES
(1, 11, 1),
(2, 12, 1),
(3, 14, 1),
(4, 15, 1),
(5, 16, 2),
(6, 17, 2),
(7, 18, 2),
(8, 20, 3),
(9, 21, 3),
(10, 22, 3),
(11, 23, 3),
(12, 24, 3),
(13, 25, 3),
(14, 26, 3),
(15, 27, 3),
(16, 28, 3),
(17, 29, 3),
(18, 30, 3),
(19, 31, 3),
(20, 32, 3),
(21, 33, 3),
(22, 34, 3),
(23, 35, 3),
(24, 36, 3),
(25, 37, 3),
(26, 38, 3),
(27, 39, 3),
(28, 40, 3),
(29, 41, 3),
(30, 42, 3),
(31, 43, 3),
(32, 44, 4),
(33, 46, 4),
(34, 47, 4),
(35, 48, 4),
(36, 49, 4),
(37, 50, 4),
(38, 51, 4),
(39, 52, 4),
(40, 53, 4),
(41, 54, 5),
(42, 55, 5),
(43, 56, 5),
(44, 57, 5),
(45, 58, 5),
(46, 59, 6),
(47, 60, 6),
(48, 61, 6),
(49, 62, 6),
(50, 63, 6),
(51, 64, 6),
(52, 65, 6),
(53, 66, 6),
(54, 67, 6),
(55, 68, 6),
(56, 69, 6),
(57, 70, 6),
(58, 71, 6),
(59, 72, 6),
(60, 73, 7),
(61, 74, 7),
(62, 75, 7),
(63, 76, 7),
(64, 77, 7),
(65, 78, 7),
(66, 79, 7),
(67, 80, 7),
(68, 81, 7),
(69, 82, 8),
(70, 83, 8),
(71, 84, 8),
(72, 85, 8),
(73, 86, 8),
(74, 87, 8),
(75, 88, 8),
(76, 89, 8),
(77, 90, 8),
(78, 91, 8),
(79, 92, 8),
(80, 93, 8),
(81, 94, 9),
(82, 95, 9),
(83, 96, 9),
(84, 97, 9),
(85, 98, 9),
(86, 99, 9),
(87, 100, 10),
(88, 101, 10),
(89, 102, 10),
(90, 103, 10),
(91, 104, 10),
(92, 105, 10),
(93, 106, 10),
(94, 107, 10),
(95, 108, 10),
(96, 109, 10),
(97, 110, 10),
(98, 111, 1),
(99, 112, 1),
(100, 113, 1),
(101, 114, 1),
(102, 115, 1),
(103, 116, 1),
(104, 117, 1),
(105, 118, 1),
(106, 119, 1),
(107, 120, 1),
(108, 121, 1),
(109, 122, 1),
(110, 123, 1),
(111, 124, 1),
(112, 125, 1),
(113, 126, 1),
(114, 127, 1),
(115, 128, 1),
(116, 129, 1),
(117, 130, 1),
(118, 131, 1),
(119, 132, 1),
(120, 133, 1),
(121, 134, 1),
(122, 135, 1),
(123, 136, 1),
(124, 137, 1),
(125, 138, 1),
(126, 139, 1),
(127, 140, 1),
(128, 142, 12),
(129, 143, 12),
(130, 144, 12),
(131, 145, 12),
(132, 146, 12),
(133, 147, 12),
(134, 148, 12),
(135, 149, 12),
(136, 150, 12),
(137, 151, 12),
(138, 152, 12);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` smallint(3) UNSIGNED NOT NULL,
  `location_name` varchar(30) NOT NULL,
  `location_status` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `location_name`, `location_status`) VALUES
(1, 'Jakarta', 1),
(2, 'Surabaya', 1),
(3, 'Makasar', 1),
(4, 'Palembang', 1),
(5, 'Bali', 1),
(6, 'Bandung', 1),
(7, 'Jogja', 1),
(8, 'Medan', 1),
(9, 'Pekan Baru', 1),
(10, 'Batam', 1),
(11, 'Bengkulu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `store_id` smallint(3) UNSIGNED NOT NULL,
  `store_name` varchar(30) NOT NULL,
  `store_status` tinyint(1) UNSIGNED NOT NULL,
  `location_id` smallint(3) UNSIGNED NOT NULL,
  `default_point` smallint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`store_id`, `store_name`, `store_status`, `location_id`, `default_point`) VALUES
(1, 'Plaza Indonesia', 1, 1, 100),
(2, 'Plaza Senayan', 1, 1, 100),
(3, 'Pondok Indah Mall 2', 1, 1, 100),
(4, 'Senayan City', 1, 1, 100),
(5, 'Mall Kelapa Gading', 1, 1, 100),
(6, 'Emporium Pluit', 1, 1, 100),
(7, 'Central Park', 1, 1, 100),
(8, 'Gandaria City', 1, 1, 100),
(9, 'Flavour Bliss', 1, 1, 100),
(10, 'Supermall Karawaci', 1, 1, 100),
(11, 'Grand Indonesia', 1, 1, 100),
(12, 'Lotte Avenue', 1, 1, 100),
(13, 'Kota Kasablanka', 1, 1, 100),
(14, 'Summarecon Bekasi', 1, 1, 100),
(15, 'Bintaro Xchange', 1, 1, 100),
(16, 'The Breeze', 1, 1, 100),
(17, 'Puri Mall', 1, 1, 100),
(18, 'Summarecon Serpong', 1, 1, 100),
(19, 'Margo City', 1, 1, 100),
(20, 'Kemang Village', 1, 1, 100),
(100, 'Sushi Kiosk Puri', 1, 1, 100),
(101, 'a Test Outlet', 1, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_area`
--

CREATE TABLE `store_area` (
  `area_id` smallint(5) NOT NULL,
  `area_name` varchar(50) NOT NULL,
  `area_desc` text NOT NULL,
  `area_status` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store_area`
--

INSERT INTO `store_area` (`area_id`, `area_name`, `area_desc`, `area_status`) VALUES
(1, 'Generals', 'Area selain yang disebutkan', 1),
(2, 'Eksterior', 'Area service', 1),
(3, 'Dine-In', 'Area Open Kitchen', 1),
(4, 'Janitor dan Loker', 'Area Back Kitchen', 1),
(5, 'Toilet', 'Area Gudang', 1),
(6, 'Hot & Cold Kitchen', '', 1),
(7, 'Back Kitchen', '', 1),
(8, 'Penerimaan & Penyimpanan Barang', '', 1),
(9, 'Personal Hygiene & Sanitasi', '', 1),
(10, 'Infrastruktur dan Utiliti', '', 1),
(12, 'Bar', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_category`
--

CREATE TABLE `ticket_category` (
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `category_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `area_id` smallint(5) UNSIGNED DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `point_deduction` decimal(3,0) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_category`
--

INSERT INTO `ticket_category` (`category_id`, `category_name`, `area_id`, `description`, `point_deduction`, `deleted`) VALUES
(11, 'Sink untuk cuci tangan dilengkapi dengan sabun, hand sanitizer, dan tissue dalam dispenser', 1, 'Sink untuk cuci tangan dilengkapi dengan sabun, hand sanitizer, dan tissue dalam dispenser', '2', 0),
(12, 'prosedur pencucian tangan dilakukan secara benar. Pencucian tangan dilakukan sebelum operasional, setelah briefing, masuk area medium sanitasi, memegang tempat sampah, dan kondisi lainnya yang dapat mengkontaminasi)', 1, 'prosedur pencucian tangan dilakukan secara benar. Pencucian tangan dilakukan sebelum operasional, setelah briefing, masuk area medium sanitasi, memegang tempat sampah, dan kondisi lainnya yang dapat mengkontaminasi)', '2', 0),
(14, 'Sink cuci tangan karyawan tidak digunakan untuk aktivitas lain selain mencuci tangan', 1, 'Sink cuci tangan karyawan tidak digunakan untuk aktivitas lain selain mencuci tangan', '1', 0),
(15, 'Penampilan staff sesuai dengan standard seragam dan personal hygiene yang ditetapkan', 1, 'Penampilan staff sesuai dengan standard seragam dan personal hygiene yang ditetapkan', '2', 0),
(16, 'Jalan di depan outlet (public area) dan entrance masuk ke outlet bersih dan terawat', 2, 'Jalan di depan outlet (public area) dan entrance masuk ke outlet bersih dan terawat', '1', 0),
(17, 'Panel, kaca, hiasan dan kursi waiting list eksterior bersih dan terawat', 2, 'Panel, kaca, hiasan dan kursi waiting list eksterior bersih dan terawat', '1', 0),
(18, 'Meja greeter dalam kondisi bersih, rapi dan terawat', 2, 'Meja greeter dalam kondisi bersih, rapi dan terawat', '1', 0),
(20, 'Meja, sofa dan kursi dalam kondisi bersih (termasuk bagian bawah meja, sela-sela syscall, sela-sela sofa dan kursi, penyimpanan bawah sofa )', 3, 'Meja, sofa dan kursi dalam kondisi bersih (termasuk bagian bawah meja, sela-sela syscall, sela-sela sofa dan kursi, penyimpanan bawah sofa )', '1', 0),
(21, 'Lampu menyala dalam kondisi baik, speaker berfungsi dengan baik', 3, 'Lampu menyala dalam kondisi baik, speaker berfungsi dengan baik', '1', 0),
(22, 'Meja, sofa dan kursi dalam kondisi baik dan terawat (tidak gompal, goyang, retak, terkelupas)', 3, 'Meja, sofa dan kursi dalam kondisi baik dan terawat (tidak gompal, goyang, retak, terkelupas)', '1', 0),
(23, 'Lantai, dinding, langit-langit, difuser ac bersih dan terawat (tidak retak, pecah dan gompal)', 3, 'Lantai, dinding, langit-langit, difuser ac bersih dan terawat (tidak retak, pecah dan gompal)', '1', 0),
(24, 'Kaca, partisi, desain interior dalam keadaan bersih dan terawat', 3, 'Kaca, partisi, desain interior dalam keadaan bersih dan terawat', '1', 0),
(25, 'Buku menu dalam keadaan bersih(tidak sobek,tidak kotor, berbau, dan lengket)', 3, 'Buku menu dalam keadaan bersih(tidak sobek,tidak kotor, berbau, dan lengket)', '1', 0),
(26, 'Setting table dalam keadaan bersih dan lengkap (side plate, saucer, chopstick, tissue, toothpick)', 3, 'Setting table dalam keadaan bersih dan lengkap (side plate, saucer, chopstick, tissue, toothpick)', '1', 0),
(27, 'Tent card marketing dalam kondisi bersih dan terawat (tidak pecah)', 3, 'Tent card marketing dalam kondisi bersih dan terawat (tidak pecah)', '1', 0),
(28, 'Warmer chawan mushi, Teko, selang teko, gagang teko  dan boiler dalam keadaan bersih dan terawat', 3, 'Warmer chawan mushi, Teko, selang teko, gagang teko  dan boiler dalam keadaan bersih dan terawat', '1', 0),
(29, 'Peralatan makan yang akan digunakan kondisi bersih dan layak pakai (sendok, garpu, duckspoon, chopstick, laddle, sendok nasi, mangkok gohan, mangkok soup, gelas ocha)', 3, 'Peralatan makan yang akan digunakan kondisi bersih dan layak pakai (sendok, garpu, duckspoon, chopstick, laddle, sendok nasi, mangkok gohan, mangkok soup, gelas ocha)', '1', 0),
(30, 'Peralatan kerja (capitan take away, bill cover, tray, trolley bush boy, tempat piring kotor,dll) dalam kondisi bersih dan layak pakai', 3, 'Peralatan kerja (capitan take away, bill cover, tray, trolley bush boy, tempat piring kotor,dll) dalam kondisi bersih dan layak pakai', '1', 0),
(31, 'Station, drawer, dan lemari penyimpanan dalam kondisi bersih dan terawat (termasuk area sekitar station)', 3, 'Station, drawer, dan lemari penyimpanan dalam kondisi bersih dan terawat (termasuk area sekitar station)', '1', 0),
(32, 'Conveyor belt (keadaan mati) dalam keadaan bersih (tidak ada ceceran makanan dan debu di atas dan bagian bawah, termasuk stainless sekitar conveyor)', 3, 'Conveyor belt (keadaan mati) dalam keadaan bersih (tidak ada ceceran makanan dan debu di atas dan bagian bawah, termasuk stainless sekitar conveyor)', '1', 0),
(33, 'Lampu Flies Catcher, mosquito trap dalam kondisi menyala. ', 3, 'Lampu Flies Catcher, mosquito trap dalam kondisi menyala. ', '1', 0),
(34, 'Tempat penyimpanan sauce, mayonaise, shoyu, sweet sauce terdapat label tanggal pindah botol', 3, 'Tempat penyimpanan sauce, mayonaise, shoyu, sweet sauce terdapat label tanggal pindah botol', '2', 0),
(35, 'Form pencatatan suhu ruang terisi lengkap dan alat berfungsi baik', 3, 'Form pencatatan suhu ruang terisi lengkap dan alat berfungsi baik', '1', 0),
(36, 'Produk khusus take away diberi label best consumed 2 hours. Sampling implementasi (catat nama karyawan dan jam periksa)', 3, 'Produk khusus take away diberi label best consumed 2 hours. Sampling implementasi (catat nama karyawan dan jam periksa)', '2', 0),
(37, 'Lemari penyimpanan perlengkapan teknisi dalam kondisi rapi dan bersih', 3, 'Lemari penyimpanan perlengkapan teknisi dalam kondisi rapi dan bersih', '1', 0),
(38, 'Tempat sampah dalam kondisi tertutup, pijakan berfungsi dengan baik, bagian dalam terdapat plastik sampah', 3, 'Tempat sampah dalam kondisi tertutup, pijakan berfungsi dengan baik, bagian dalam terdapat plastik sampah', '1', 0),
(39, 'Dust mats yang menuju ke area dinning, open kitchen atau back kitchen dalam kondisi bersih, dan kering  ', 3, 'Dust mats yang menuju ke area dinning, open kitchen atau back kitchen dalam kondisi bersih, dan kering  ', '1', 0),
(40, 'Penyimpanan  bahan makanan di station / dilemari area service hanya untuk kebutuhan operasional 1 hari kecuali dilakukan monitoring dan pengendalian suhu dan kelembaban', 3, 'Penyimpanan  bahan makanan di station / dilemari area service hanya untuk kebutuhan operasional 1 hari kecuali dilakukan monitoring dan pengendalian suhu dan kelembaban', '1', 0),
(41, 'penyimpanan bahan baku service disimpan dalam tempat tertutup(contoh: sauce tomat, gari dan wasabi dalam kemasan tertutup di chiller,tea pack disimpan dalam tempat tertutup)', 3, 'penyimpanan bahan baku service disimpan dalam tempat tertutup(contoh: sauce tomat, gari dan wasabi dalam kemasan tertutup di chiller,tea pack disimpan dalam tempat tertutup)', '3', 0),
(42, 'pelabelan kondimen sweet dan shoyu dilaksanakan dengan tepat(hijau=1 hari, kuning=2 hari, merah=3 hari)', 3, 'pelabelan kondimen sweet dan shoyu dilaksanakan dengan tepat(hijau=1 hari, kuning=2 hari, merah=3 hari)', '2', 0),
(43, 'clear up bushboy dilkaukan dengan tepat(meja dalam keadaan bersih tidak berminyak, lantai bersih, sampah tidak dimasukkan ke dalam alat saji)', 3, 'clear up bushboy dilkaukan dengan tepat(meja dalam keadaan bersih tidak berminyak, lantai bersih, sampah tidak dimasukkan ke dalam alat saji)', '1', 0),
(44, 'Lemari janitor dalam keadaan rapi dan bersih', 4, 'Lemari janitor dalam keadaan rapi dan bersih', '1', 0),
(46, 'Bahan kimia yang disimpan dalam wadah tertutup dan diberi label yang jelas', 4, 'Bahan kimia yang disimpan dalam wadah tertutup dan diberi label yang jelas', '2', 0),
(47, 'Peralatan pembuatan bahan kimia tersedia lengkap seperti kaca mata, sarung tangan karet, cangkir takar dan sendok takar.', 4, 'Peralatan pembuatan bahan kimia tersedia lengkap seperti kaca mata, sarung tangan karet, cangkir takar dan sendok takar.', '1', 0),
(48, 'Bahan kimia yang disimpan di dalam janitor adalah bahan kimia yang memiliki MSDS lengkap dan tertera di List Bahan Kimia yang digunakan di outlet (tertempel di lemari/dinding janitor) [NOTE: kecuali bebek biru dan pengharum toilet di FS]', 4, 'Bahan kimia yang disimpan di dalam janitor adalah bahan kimia yang memiliki MSDS lengkap dan tertera di List Bahan Kimia yang digunakan di outlet (tertempel di lemari/dinding janitor) [NOTE: kecuali bebek biru dan pengharum toilet di FS]', '1', 0),
(49, 'Bush boy  dan steward mengetahui takaran pengenceran bahan kimia untuk yang dicampur secara manual (suma titan, suma calc, oven grill, demonpine) (catat nama karyawan dan lama kerja)', 4, 'Bush boy  dan steward mengetahui takaran pengenceran bahan kimia untuk yang dicampur secara manual (suma titan, suma calc, oven grill, demonpine) (catat nama karyawan dan lama kerja)', '1', 0),
(50, 'Peralatan kebersihan dalam kondisi layak pakai', 4, 'Peralatan kebersihan dalam kondisi layak pakai', '1', 0),
(51, 'Memiliki jadwal pembersihan locker setiap harinya', 4, 'Memiliki jadwal pembersihan locker setiap harinya', '1', 0),
(52, 'Ruangan locker kondisi bersih dan terawat, sampah dibuang secara periodik', 4, 'Ruangan locker kondisi bersih dan terawat, sampah dibuang secara periodik', '1', 0),
(53, 'Tas tersimpan didalam locker/megabox dan dapat ditutup dengan rapi/terkunci', 4, 'RTas tersimpan didalam locker/megabox dan dapat ditutup dengan rapi/terkunci', '1', 0),
(54, 'Toilet dalam keadaan bersih, kering dan tidak berbau', 5, 'Toilet dalam keadaan bersih, kering dan tidak berbau', '1', 0),
(55, 'Tissue dan handsoap tersedia, tempat sampah dalam kondisi layak dan terawat', 5, 'Tissue dan handsoap tersedia, tempat sampah dalam kondisi layak dan terawat', '1', 0),
(56, 'Hand dryer berfungsi normal', 5, 'Hand dryer berfungsi normal', '1', 0),
(57, 'Bagian dalam closet/urinair tidak terdapat kotoran ataupun kerak air', 5, 'Bagian dalam closet/urinair tidak terdapat kotoran ataupun kerak air', '1', 0),
(58, 'Checklist kebersihan tersedia dan terisi lengkap', 5, 'Checklist kebersihan tersedia dan terisi lengkap', '1', 0),
(59, 'Semua produk dalam kondisi baik dan layak pakai', 6, 'Semua produk dalam kondisi baik dan layak pakai', '1', 0),
(60, 'Semua kemungkinan adanya kontaminasi silang harus dihindari', 6, 'Semua kemungkinan adanya kontaminasi silang harus dihindari', '1', 0),
(61, 'Aquarium bersih dan tidak berlumut, kadar garam sesuai dengan standard', 6, 'Aquarium bersih dan tidak berlumut, kadar garam sesuai dengan standard', '1', 0),
(62, 'Timbangan/peralatan ukur yang digunakan dalam kondisi bersih, terawat baik dan tidak rusak. Terdapat label QA dan label kalibrasi/verifikasi', 6, 'Timbangan/peralatan ukur yang digunakan dalam kondisi bersih, terawat baik dan tidak rusak. Terdapat label QA dan label kalibrasi/verifikasi', '1', 0),
(63, 'Cover plate bersih, tidak rusak/retak dan dilengkapi dengan stiker warna', 6, 'Cover plate bersih, tidak rusak/retak dan dilengkapi dengan stiker warna', '1', 0),
(64, 'Color plate bersih, bagian bawah tidak berkerak atau bercak hitam, tidak gompal atau retak', 6, 'Color plate bersih, bagian bawah tidak berkerak atau bercak hitam, tidak gompal atau retak', '1', 0),
(65, 'Foil dynamite roll dalam kondisi bersih, bentuk masih utuh dan layak pakai', 6, 'Foil dynamite roll dalam kondisi bersih, bentuk masih utuh dan layak pakai', '1', 0),
(66, 'Nabe, donburi , yanagawa plate dan kompor sukiyaki tidak berkarat, kondisi kering dan bersih', 6, 'Nabe, donburi , yanagawa plate dan kompor sukiyaki tidak berkarat, kondisi kering dan bersih', '1', 0),
(67, 'Tidak menyimpan bahan baku atau bahan makanan di atas rak interior. NOTE: maksimal jam 10.30', 6, 'Tidak menyimpan bahan baku atau bahan makanan di atas rak interior. NOTE: maksimal jam 10.30', '3', 0),
(68, 'Pencatatan penaikan dan penurunan color plate terdokumentasi dengan lengkap dan benar. Sampling penurunan color plate dilakukan tepat waktu. Tidak ada produk yang berada di conveyor belt lebih dari 2 jam (termasuk edamame dan chinmi) (catat jam periksa)', 6, 'Pencatatan penaikan dan penurunan color plate terdokumentasi dengan lengkap dan benar. Sampling penurunan color plate dilakukan tepat waktu. Tidak ada produk yang berada di conveyor belt lebih dari 2 jam (termasuk edamame dan chinmi) (catat jam periksa)', '3', 0),
(69, 'Sampling cara pemeriksaan suhu internal produk dilakukan dengan benar (catat jam periksa). Melakukan sanitasi sebelum mengukur suhu', 6, 'Sampling cara pemeriksaan suhu internal produk dilakukan dengan benar (catat jam periksa). Melakukan sanitasi sebelum mengukur suhu', '1', 0),
(70, 'Form pencabutan tulang, suhu internal produk, dan checklist kebersihan tersedia dan terisi lengkap ', 6, 'Form pencabutan tulang, suhu internal produk, dan checklist kebersihan tersedia dan terisi lengkap ', '1', 0),
(71, 'Staff melakukan sanitasi testo sebelum digunakan untuk pengecekan suhu internal produk ', 6, 'Staff melakukan sanitasi testo sebelum digunakan untuk pengecekan suhu internal produk ', '1', 0),
(72, 'Rotasi pergantian manaita dan pergantian makisu sesuai waktu yang telah ditentukan. Pengunaan manaita hijau untuk pemotonga produk Ready to Eat di Hot kitchen', 6, 'Rotasi pergantian manaita dan pergantian makisu sesuai waktu yang telah ditentukan. Pengunaan manaita hijau untuk pemotonga produk Ready to Eat di Hot kitchen', '2', 0),
(73, 'Semua peralatan yang digunakan dalam kondisi bersih, terawat dan berfungsi baik ', 7, 'Semua peralatan yang digunakan dalam kondisi bersih, terawat dan berfungsi baik ', '1', 0),
(74, 'Pencucian peralatan dengan diswaher dilakukan dengan tepat(Bak sink terakhir sebelum ke dishwasher terisi dengan multiporpose soap, penyusunan piring pada rak tidak menumpuk, penggunaan rak cutleries untu peralatan service seperti chopstick, sendok, garpu, dan peralatn kecil lainnya)', 7, 'Pencucian peralatan dengan diswaher dilakukan dengan tepat(Bak sink terakhir sebelum ke dishwasher terisi dengan multiporpose soap, penyusunan piring pada rak tidak menumpuk, penggunaan rak cutleries untu peralatan service seperti chopstick, sendok, garpu, dan peralatn kecil lainnya)', '1', 0),
(75, 'Air dishwasher diganti sesuai dengan jadwal yang ditetapkan dan di catat di checklist pergantian dishwasher', 7, 'Air dishwasher diganti sesuai dengan jadwal yang ditetapkan dan di catat di checklist pergantian dishwasher', '1', 0),
(76, 'Bak/ember piring  kondisi bersih dari kotoran yang mengerak/lumut', 7, 'Bak/ember piring  kondisi bersih dari kotoran yang mengerak/lumut', '1', 0),
(77, 'Peralatan atau kitchen ware yang diletakkan pada rak bersih kondisi bersih, layak pakai, dan penyimpanannya sesuai( wajan tamago, plate gyu misoyaki dan cap tamago dilapisi minyak untuk penyimpanannya)', 7, 'Peralatan atau kitchen ware yang diletakkan pada rak bersih kondisi bersih, layak pakai, dan penyimpanannya sesuai( wajan tamago, plate gyu misoyaki dan cap tamago dilapisi minyak untuk penyimpanannya)', '1', 0),
(78, 'Suhu gun sprayer steward mencapai 43oC', 7, 'Suhu gun sprayer steward mencapai 43oC', '1', 0),
(79, 'Melakukan sanitasi dengan larutan sanitasi pada pencucian panci atau peralatan yang tidak masuk ke dishwasher ', 7, 'Melakukan sanitasi dengan larutan sanitasi pada pencucian panci atau peralatan yang tidak masuk ke dishwasher ', '2', 0),
(80, 'Sampling cara pencabutan tulang dilakukan di back kitchen dan verifikasi pencabutan tulang dilakukan oleh CoD. (catat jam periksa)', 7, 'Sampling cara pencabutan tulang dilakukan di back kitchen dan verifikasi pencabutan tulang dilakukan oleh CoD. (catat jam periksa)', '1', 0),
(81, 'proses pencucian prepare sayuran dilakuakan di back kitchen(termasuk prepare avocado) dan diproses sesuai ketentuan FnB', 7, 'proses pencucian prepare sayuran dilakuakan di back kitchen(termasuk prepare avocado) dan diproses sesuai ketentuan FnB', '2', 0),
(82, 'Barang yang datang sesuai dengan standard', 8, 'Barang yang datang sesuai dengan standard', '1', 0),
(83, 'Kemasan produk diberi label tanggal kedatangan dan label exp date (label Exp date dikirimkan dari ICW. Jika tidak ada label atau jumlah kurang maka dimunculkan PQNC)', 8, 'Kemasan produk diberi label tanggal kedatangan dan label exp date (label Exp date dikirimkan dari ICW. Jika tidak ada label atau jumlah kurang maka dimunculkan PQNC)', '1', 0),
(84, 'Temperatur produk dan tanggal kadaluarsa/produksi tertulis jelas di copy faktur/PO/TO dan pada form penerimaan barang', 8, 'Temperatur produk dan tanggal kadaluarsa/produksi tertulis jelas di copy faktur/PO/TO dan pada form penerimaan barang', '3', 0),
(85, 'Form suhu dan RH terisi lengkap dan benar', 8, 'Form suhu dan RH terisi lengkap dan benar', '1', 0),
(86, 'Penyimpanan barang tidak menempel dinding dan tidak di atas lantai', 8, 'Penyimpanan barang tidak menempel dinding dan tidak di atas lantai', '3', 0),
(87, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan layak pakai', 8, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan layak pakai', '1', 0),
(88, 'Penyimpanan bahan kimia terpisah dari bahan makanan', 8, 'Penyimpanan bahan kimia terpisah dari bahan makanan', '3', 0),
(89, 'Suhu gudang penyimpanan bahan makanan sesuai standart (maksimal 28 deg C=ruangan AC dan 33 deg C=non AC ', 8, 'Suhu gudang penyimpanan bahan makanan sesuai standart (maksimal 28 deg C=ruangan AC dan 33 deg C=non AC ', '1', 0),
(90, 'Tidak meletakkan produk langsung di atas lantai pada saat proses penerimaan', 8, 'Tidak meletakkan produk langsung di atas lantai pada saat proses penerimaan', '1', 0),
(91, 'Suplier ataupun logistik Sushi Tei  yang membawakan barang menuju gudang dengan terlebih dahulu melintasi area produksi/kitchen menggunakan hair net/penutup kepala', 8, 'Suplier ataupun logistik Sushi Tei  yang membawakan barang menuju gudang dengan terlebih dahulu melintasi area produksi/kitchen menggunakan hair net/penutup kepala', '2', 0),
(92, 'Sampling cara pemeriksaan suhu internal salmon atau produk beku lainnya (catat nama karyawan)', 8, 'Sampling cara pemeriksaan suhu internal salmon atau produk beku lainnya (catat nama karyawan)', '3', 0),
(93, 'Penyimpanan bahan makanan di gudang dry, dalam walk in chiller dan walk in freezer tinggi minimal 15 cm ', 8, 'Penyimpanan bahan makanan di gudang dry, dalam walk in chiller dan walk in freezer tinggi minimal 15 cm ', '1', 0),
(94, 'Staff yang memiliki luka harus menutup luka dengan perban dan menggunakan hand gloves', 9, 'Staff yang memiliki luka harus menutup luka dengan perban dan menggunakan hand gloves', '2', 0),
(95, 'Telepon seluler tidak dibawa selama jam tugas berlangsung (tidak di dalam saku) kecuali untuk karyawan (MOD, COD, Stock Keeper, Ass. Stock Keeper) yang memang pekerjaannya membutuhkan HP untuk berkoordinasi dengan pihak lain atau terkait dengan pekerjaannya. NOTE: untuk Bar simpan di kasir, untuk kitchen di rak admin open kitchen/back kitchen dalam kondisi tertutup', 9, 'Telepon seluler tidak dibawa selama jam tugas berlangsung (tidak di dalam saku) kecuali untuk karyawan (MOD, COD, Stock Keeper, Ass. Stock Keeper) yang memang pekerjaannya membutuhkan HP untuk berkoordinasi dengan pihak lain atau terkait dengan pekerjaannya. NOTE: untuk Bar simpan di kasir, untuk kitchen di rak admin open kitchen/back kitchen dalam kondisi tertutup', '2', 0),
(96, 'Fasilitas sanitasi karyawan dilengkapi dengan visual cara mencuci tangan', 9, 'Fasilitas sanitasi karyawan dilengkapi dengan visual cara mencuci tangan', '1', 0),
(97, 'Tidak ada tanda-tanda adanya tikus dan  kecoa yang hidup', 9, 'Tidak ada tanda-tanda adanya tikus dan  kecoa yang hidup', '1', 0),
(98, 'Pest control selalu dilakukan dan jadwal serta laporan kerja harus didokumentasikan', 9, 'Pest control selalu dilakukan dan jadwal serta laporan kerja harus didokumentasikan', '1', 0),
(99, 'Peralatan pest control (Misal AMS, Lampu Flycatcher, dan Mosquito trap) harus selalu bersih, terawat (tidak basah, tidak pecah), berfungsi', 9, 'Peralatan pest control (Misal AMS, Lampu Flycatcher, dan Mosquito trap) harus selalu bersih, terawat (tidak basah, tidak pecah), berfungsi', '1', 0),
(100, 'Semua pintu berfungsi dengan baik, dapat menutup sempurna ', 10, 'Semua pintu berfungsi dengan baik, dapat menutup sempurna ', '1', 0),
(101, 'Electrical panels, saklar, kabel listrik, stop kontak dalam keadaan bersih, terawat, berfungsi baik dan aman', 10, 'Electrical panels, saklar, kabel listrik, stop kontak dalam keadaan bersih, terawat, berfungsi baik dan aman', '1', 0),
(102, 'Stop kontak yang sudah tidak digunakan atau berfungsi ditutup untuk mencegah sarang hama', 10, 'Stop kontak yang sudah tidak digunakan atau berfungsi ditutup untuk mencegah sarang hama', '1', 0),
(103, 'Tidak dijumpai peralatan atau perbaikan sementara yang menggunakan bahan isolasi/plester atau bahan yang bersifat absorbent atau yang mudah menimbulkan infestasi pest', 10, 'Tidak dijumpai peralatan atau perbaikan sementara yang menggunakan bahan isolasi/plester atau bahan yang bersifat absorbent atau yang mudah menimbulkan infestasi pest', '1', 0),
(104, 'Kontainer plastik yang digunakan untuk penyimpanan makanan dan minuman terbuat dari bahan food grade dan tidak beracun ', 10, 'Kontainer plastik yang digunakan untuk penyimpanan makanan dan minuman terbuat dari bahan food grade dan tidak beracun ', '1', 0),
(105, 'Temperatur suhu ruang di sekitar conveyor belt maksimal 28 deg C', 10, 'Temperatur suhu ruang di sekitar conveyor belt maksimal 28 deg C', '1', 0),
(106, 'Penggantian filter air dilakukan berkala dan dapat dibuktikan dengan label di alat dan juga kelengkapan pencatatan di form terkait(Filter air besar dan Filter air kecil)', 10, 'Penggantian filter air dilakukan berkala dan dapat dibuktikan dengan label di alat dan juga kelengkapan pencatatan di form terkait(Filter air besar dan Filter air kecil)', '1', 0),
(107, 'Aktivitas pencucian filter atau backwash dilakukan secara berkala dan dapat dibuktikan dengan kelengkapan pencatatan di form terkait', 10, 'Aktivitas pencucian filter atau backwash dilakukan secara berkala dan dapat dibuktikan dengan kelengkapan pencatatan di form terkait', '1', 0),
(108, 'Lampu UV filter dalam kondisi menyala (di area bar dan di area  back/open kitchen) ', 10, 'Lampu UV filter dalam kondisi menyala (di area bar dan di area  back/open kitchen) ', '3', 0),
(109, 'APAR tidak expired dan tidak di batas merah bawah (lengkap sesuai lay out)', 10, 'APAR tidak expired dan tidak di batas merah bawah (lengkap sesuai lay out)', '1', 0),
(110, 'Air yang digunakan melewati sistem water treatment (kran / valve dalam posisi yang benar, tidak di-bypass)', 10, 'Air yang digunakan melewati sistem water treatment (kran / valve dalam posisi yang benar, tidak di-bypass)', '1', 0),
(111, 'Checklist personel hygiene terisi dengan lengkap dan mencakup untuk karyawan (admin ISA, captain, MoD, Teknisi, Bartender,steward cook,  stockeeper, COD)', 1, 'Checklist personel hygiene terisi dengan lengkap dan mencakup untuk karyawan (admin ISA, captain, MoD, Teknisi, Bartender,steward cook,  stockeeper, COD)', '1', 0),
(112, 'Tidak ada barang pribadi di area proses kerja (termasuk box katering, botol dan gelas minuman). Apabila diperlukan penyimpanan box catring, botol dan gelas minum hanya dilokasi yang ditentukan dan tidak di lokasi kerja/working table.', 1, 'Tidak ada barang pribadi di area proses kerja (termasuk box katering, botol dan gelas minuman). Apabila diperlukan penyimpanan box catring, botol dan gelas minum hanya dilokasi yang ditentukan dan tidak di lokasi kerja/working table.', '2', 0),
(113, 'Produk yang disimpan memiliki label identitas (nama produk, tanggal pembuatan,tanggal pertama kali dibuka kemasan, dll) untuk produk yang tidak habis pada hari dia dibuka/dipindahkan', 1, 'Produk yang disimpan memiliki label identitas (nama produk, tanggal pembuatan,tanggal pertama kali dibuka kemasan, dll) untuk produk yang tidak habis pada hari dia dibuka/dipindahkan', '2', 0),
(114, 'Produk harus disimpan dalam tempat tertutup, terjaga dari kontaminasi. NOTE: kecuali garnish diatas meja ', 1, 'Produk harus disimpan dalam tempat tertutup, terjaga dari kontaminasi. NOTE: kecuali garnish diatas meja ', '3', 0),
(115, 'Produk rusak(termasuk expired, buah dan sayur yang sudah tidak layak pakai, berjamur atau busuk) dipisahkan dan diberi label yang jelas, serta sudah dilaporkan dalam PQNC. NOTE: label tidak wajib warna', 1, 'Produk rusak(termasuk expired, buah dan sayur yang sudah tidak layak pakai, berjamur atau busuk) dipisahkan dan diberi label yang jelas, serta sudah dilaporkan dalam PQNC. NOTE: label tidak wajib warna', '3', 0),
(116, 'Penyimpanan WIP atau ready to eat food terpisah dari bahan makanan mentah/raw food  Pemisahan dapat berupa disimpan dalam kontainer tertutup atau pada rak yang terpisah. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', 1, 'Penyimpanan WIP atau ready to eat food terpisah dari bahan makanan mentah/raw food  Pemisahan dapat berupa disimpan dalam kontainer tertutup atau pada rak yang terpisah. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', '3', 0),
(117, 'Bahan baku yang bersifat alergen ditempatkan terpisah dan/atau tertutup dari produk lainnya. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', 1, 'Bahan baku yang bersifat alergen ditempatkan terpisah dan/atau tertutup dari produk lainnya. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', '3', 0),
(118, 'Tidak melakukan thawing di suhu ruang dan membiarkan produk beku dalam jangka waktu >30 menit di suhu ruang', 1, 'Tidak melakukan thawing di suhu ruang dan membiarkan produk beku dalam jangka waktu >30 menit di suhu ruang', '3', 0),
(119, 'Sistem penyusunan di rak atau dalam chiller FREEZER memperhatikan FIFO atau FEFO', 1, 'Sistem penyusunan di rak atau dalam chiller FREEZER memperhatikan FIFO atau FEFO', '3', 0),
(120, 'idak meletakkan bahan makanan atau container penyimpanan bahan makanan langsung di atas lantai', 1, 'idak meletakkan bahan makanan atau container penyimpanan bahan makanan langsung di atas lantai', '3', 0),
(121, 'Karyawan menggunakan handgloves pada saat memegang bahan makanan yang akan disiapkan atau disajikan', 1, 'Karyawan menggunakan handgloves pada saat memegang bahan makanan yang akan disiapkan atau disajikan', '2', 0),
(122, 'Penggunaan lap sesuai dengan standard managemen lap, dalam keadaan bersih, dan layak pakai (tidak mengeringkan di atas boiler air )', 1, 'Penggunaan lap sesuai dengan standard managemen lap, dalam keadaan bersih, dan layak pakai (tidak mengeringkan di atas boiler air )', '2', 0),
(123, 'Setiap bahan kimia yang tersimpan di luar janitor tidak dalam kondisi murni(kecuali pada dispenser steward, bar,open kitchen, hand soap dan sanitizer) dan diberi label yang sesuai.', 1, 'Setiap bahan kimia yang tersimpan di luar janitor tidak dalam kondisi murni(kecuali pada dispenser steward, bar,open kitchen, hand soap dan sanitizer) dan diberi label yang sesuai.', '2', 0),
(124, 'Dispenser chemical berfungsi dengan baik dan semua terisi dengan chemical(dispenser yang rusak harus dibuktikan dengan pengajuan perbaikan)', 1, 'Dispenser chemical berfungsi dengan baik dan semua terisi dengan chemical(dispenser yang rusak harus dibuktikan dengan pengajuan perbaikan)', '1', 0),
(125, 'Soaking dilaksanakan setiap hari sesuai jadwal dan cara soaking sesuai dengan standard (catat jam dilakukan soaking)', 1, 'Soaking dilaksanakan setiap hari sesuai jadwal dan cara soaking sesuai dengan standard (catat jam dilakukan soaking)', '1', 0),
(126, 'Pada kitchen (open dan back kitchen) tersedia ember yang berisi larutan nobla liquid(untuk mencuci) dan  J-512(untuk mensanitasi)', 1, 'Pada kitchen (open dan back kitchen) tersedia ember yang berisi larutan nobla liquid(untuk mencuci) dan  J-512(untuk mensanitasi)', '2', 0),
(127, 'Tidak ada carton (kemasan primer) di chest freezer(kecuali chest frezer yang digunakan oleh stockeeper karena tidak ada Walking Freezer)', 1, 'Tidak ada carton (kemasan primer) di chest freezer(kecuali chest frezer yang digunakan oleh stockeeper karena tidak ada Walking Freezer)', '3', 0),
(128, 'Sampling cara pengisian form suhu chiller/freezer dilakukan dengan benar (catat jam periksa)', 1, 'Sampling cara pengisian form suhu chiller/freezer dilakukan dengan benar (catat jam periksa)', '2', 0),
(129, 'Kondisi chiller yang digunakan suhu 1-4 deg C. NOTE: kecuali yang sudah diberi label QA', 1, 'Kondisi chiller yang digunakan suhu 1-4 deg C. NOTE: kecuali yang sudah diberi label QA', '3', 0),
(130, 'Kondisi Freezer yang digunakan suhu maksimal -15 deg C (kecuali yang sudah dilabel QA)', 1, 'Kondisi Freezer yang digunakan suhu maksimal -15 deg C (kecuali yang sudah dilabel QA)', '3', 0),
(131, 'Form pencatatan suhu chiller dan freezer tersedia dan terisi lengkap', 1, 'Form pencatatan suhu chiller dan freezer tersedia dan terisi lengkap', '1', 0),
(132, 'Chiller dan freezer dalam kondisi bersih(termasuk bagian gasket, handle, bagian dalam, dan luar) dan tertatapa rapi', 1, 'Chiller dan freezer dalam kondisi bersih(termasuk bagian gasket, handle, bagian dalam, dan luar) dan tertatapa rapi', '1', 0),
(133, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan teridentifikasi dengan jelas', 1, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan teridentifikasi dengan jelas', '1', 0),
(134, 'Lantai, dinding, langit-langit, difuser ac, exhaust bersih, terawat (tidak retak, pecah dan gompal) dan tidak dalam kondisi terbuka', 1, 'Lantai, dinding, langit-langit, difuser ac, exhaust bersih, terawat (tidak retak, pecah dan gompal) dan tidak dalam kondisi terbuka', '1', 0),
(135, 'Bagian kolong chiller, freezer, meja, rak dan peralatan lainnya dalam kondisi bersih', 1, 'Bagian kolong chiller, freezer, meja, rak dan peralatan lainnya dalam kondisi bersih', '1', 0),
(136, 'Sink, gutter dan saluran pembuangan dalam kondisi tidak tersumbat, tidak berbau, bersih dan terawat', 1, 'Sink, gutter dan saluran pembuangan dalam kondisi tidak tersumbat, tidak berbau, bersih dan terawat', '1', 0),
(137, 'Tempat sampah dalam kondisi bersih, layak pakai(terdapat pijakan), terdapat plastik di bagian dalam dan tertutup', 1, 'Tempat sampah dalam kondisi bersih, layak pakai(terdapat pijakan), terdapat plastik di bagian dalam dan tertutup', '1', 0),
(138, 'Semua peralatan dalam kondisi bersih dan layak pakai', 1, 'Semua peralatan dalam kondisi bersih dan layak pakai', '1', 0),
(139, 'Meja kerja dan rak penyimpanan dalam kondisi bersih, terawat dan bebas dari lemak/minyak', 1, 'Meja kerja dan rak penyimpanan dalam kondisi bersih, terawat dan bebas dari lemak/minyak', '1', 0),
(140, 'Tidak menyimpan scoop/sendok didalam wadah penyimpanan (tepung, produk )', 1, 'Tidak menyimpan scoop/sendok didalam wadah penyimpanan (tepung, produk )', '1', 0),
(142, 'Ice maker dan ice bin dalam kondisi bersih dan terawat (tidak ada bahan lain selain ice cube di dalam ice bin) dan dalam kondisi tertutup.', 12, 'Ice maker dan ice bin dalam kondisi bersih dan terawat (tidak ada bahan lain selain ice cube di dalam ice bin) dan dalam kondisi tertutup.', '1', 0),
(143, 'Pengambilan ice menggunakan ice scoop yang disediakan dan ice cube tidak disentuh dengan tangan tanpa hand gloves', 12, 'Pengambilan ice menggunakan ice scoop yang disediakan dan ice cube tidak disentuh dengan tangan tanpa hand gloves', '2', 0),
(144, 'Peralatan kerja dalam kondisi bersih, terawat,  berfungsi dengan baik dan layak pakai (ice scoop, shaker, muddle, pisau, maneta,dll). Peralatan kerja segera dibersihkan setelah digunakan dan disimpan di dalam larutan sanitizer saat tdk digunakan dengan bagian yang terkena produk tercelup seluruhnya ke dalam larutan(scoop ice cube dan ice cream, muddle, pisau, spoon, dan lainnya)', 12, 'Peralatan kerja dalam kondisi bersih, terawat,  berfungsi dengan baik dan layak pakai (ice scoop, shaker, muddle, pisau, maneta,dll). Peralatan kerja segera dibersihkan setelah digunakan dan disimpan di dalam larutan sanitizer saat tdk digunakan dengan bagian yang terkena produk tercelup seluruhnya ke dalam larutan(scoop ice cube dan ice cream, muddle, pisau, spoon, dan lainnya)', '1', 0),
(145, 'Gelas, cangkir, color plate dan peralatan makan bersih dan terawat, tidak gompal atau berkerak', 12, 'Gelas, cangkir, color plate dan peralatan makan bersih dan terawat, tidak gompal atau berkerak', '1', 0),
(146, 'Microwave, chiller dan freezer dalam kondisi bersih dan berfungsi dengan baik', 12, 'Microwave, chiller dan freezer dalam kondisi bersih dan berfungsi dengan baik', '1', 0),
(147, 'Tidak  menyimpan ice scoop di dalam ice maker ', 12, 'Tidak  menyimpan ice scoop di dalam ice maker ', '1', 0),
(148, 'Tidak  menyimpan ice scoop di dalam ice maker ', 12, 'Tidak  menyimpan ice scoop di dalam ice maker ', '1', 0),
(149, 'Semua produk ice cream yang disimpan di dalam freezer memiliki label expired date (atau memiliki kemampuan telusur untuk penentuan exp date)', 12, 'Semua produk ice cream yang disimpan di dalam freezer memiliki label expired date (atau memiliki kemampuan telusur untuk penentuan exp date)', '1', 0),
(150, 'pencucian gelas hiball dilakukan di lakukan di bar sesuai standart yang berlaku', 12, 'pencucian gelas hiball dilakukan di lakukan di bar sesuai standart yang berlaku', '1', 0),
(151, 'tidak menyimpan bahan makanan di samping grease trap', 12, 'tidak menyimpan bahan makanan di samping grease trap', '1', 0),
(152, 'Wine chiller (red wine) yang digunakan suhu 15-20 deg C', 12, 'Wine chiller (red wine) yang digunakan suhu 15-20 deg C', '1', 0),
(191, 'halu', 16, 'halusinasi adalah', '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_group`
--

CREATE TABLE `ticket_group` (
  `group_id` mediumint(11) UNSIGNED NOT NULL,
  `group_store` smallint(3) UNSIGNED DEFAULT NULL,
  `group_date` date DEFAULT NULL,
  `group_submit_date` date DEFAULT NULL,
  `group_finding` smallint(2) UNSIGNED DEFAULT NULL,
  `group_user` varchar(50) DEFAULT NULL,
  `group_auditee` varchar(50) DEFAULT NULL,
  `group_point` smallint(3) UNSIGNED DEFAULT NULL,
  `group_status` tinyint(1) DEFAULT NULL,
  `group_recommendation` mediumint(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_log`
--

CREATE TABLE `ticket_log` (
  `log_id` int(11) UNSIGNED NOT NULL,
  `log_title` text NOT NULL,
  `ticket` mediumint(11) UNSIGNED DEFAULT NULL,
  `user` smallint(3) UNSIGNED NOT NULL,
  `log_type` varchar(20) NOT NULL,
  `log_content` text NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_main`
--

CREATE TABLE `ticket_main` (
  `ticket_id` mediumint(11) UNSIGNED NOT NULL,
  `ticket_group` mediumint(11) DEFAULT NULL,
  `category` smallint(5) UNSIGNED NOT NULL,
  `prp_date` date DEFAULT NULL,
  `ticket_creator` smallint(4) UNSIGNED NOT NULL,
  `submit_date` date DEFAULT NULL,
  `finding_images` text,
  `resolved_date` date DEFAULT NULL,
  `resolved_images` text,
  `resolved_solution` text,
  `status` tinyint(1) NOT NULL,
  `last_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `store` smallint(3) DEFAULT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_priority`
--

CREATE TABLE `ticket_priority` (
  `priority_id` smallint(13) UNSIGNED NOT NULL,
  `priority_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_priority`
--

INSERT INTO `ticket_priority` (`priority_id`, `priority_name`) VALUES
(1, 'Rendah'),
(2, 'Sedang'),
(3, 'Tinggi'),
(4, 'ASAP');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_recommendation`
--

CREATE TABLE `ticket_recommendation` (
  `recommendation_id` mediumint(11) UNSIGNED NOT NULL,
  `recommendation_content` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_feedback` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_image` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_status` tinyint(1) DEFAULT NULL,
  `ticket_group` mediumint(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_status`
--

CREATE TABLE `ticket_status` (
  `status_id` smallint(3) UNSIGNED NOT NULL,
  `status_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_status`
--

INSERT INTO `ticket_status` (`status_id`, `status_name`) VALUES
(1, 'Normal'),
(1, 'Finding'),
(2, 'Resolved');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_type`
--

CREATE TABLE `ticket_type` (
  `type_id` tinyint(2) UNSIGNED NOT NULL,
  `type_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_type`
--

INSERT INTO `ticket_type` (`type_id`, `type_name`) VALUES
(1, 'Finding'),
(2, 'Resolved');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_upload`
--

CREATE TABLE `ticket_upload` (
  `upload_id` smallint(3) UNSIGNED NOT NULL,
  `upload_name` varchar(50) NOT NULL,
  `upload_location` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` mediumint(11) UNSIGNED NOT NULL,
  `upload_temp` tinyint(1) DEFAULT '0',
  `upload_user` smallint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_upload`
--

INSERT INTO `ticket_upload` (`upload_id`, `upload_name`, `upload_location`, `upload_time`, `ticket_id`, `upload_temp`, `upload_user`) VALUES
(1, '15308173990.jpg', 'Resources/images/prp\\', '2018-07-05 19:03:21', 3, 0, 1001),
(2, '15308174010.jpg', 'Resources/images/prp\\', '2018-07-05 19:03:21', 4, 0, 1001),
(3, '15308444270.png', 'Resources/images/prp\\', '2018-07-06 02:33:48', 7, 0, 1001),
(4, '15318277830.png', 'Resources/images/prp\\', '2018-07-17 11:43:03', 144, 0, 1001);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_prp`
-- (See below for the actual view)
--
CREATE TABLE `view_prp` (
`ticket_id` mediumint(11) unsigned
,`ticket_group` mediumint(11)
,`status` tinyint(1)
,`category_id` smallint(5) unsigned
,`category_name` text
,`point` decimal(3,0) unsigned
,`store` smallint(3)
,`store_name` varchar(30)
,`default_point` smallint(3) unsigned
,`creator` smallint(5) unsigned
,`creator_name` varchar(50)
,`prp_date` date
,`submit_date` date
,`last_update` datetime
,`finding_images` text
,`resolved_images` text
,`resolved_date` date
,`resolved_solution` text
,`area_id` smallint(5)
,`area` varchar(50)
,`notes` text
,`submit_image` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `view_prp`
--
DROP TABLE IF EXISTS `view_prp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_prp`  AS  select `ticket_main`.`ticket_id` AS `ticket_id`,`ticket_main`.`ticket_group` AS `ticket_group`,`ticket_main`.`status` AS `status`,`ticket_main`.`category` AS `category_id`,`ticket_category`.`category_name` AS `category_name`,`ticket_category`.`point_deduction` AS `point`,`ticket_main`.`store` AS `store`,`store`.`store_name` AS `store_name`,`store`.`default_point` AS `default_point`,`creator`.`user_id` AS `creator`,`creator`.`display_name` AS `creator_name`,`ticket_main`.`prp_date` AS `prp_date`,`ticket_main`.`submit_date` AS `submit_date`,`ticket_main`.`last_update` AS `last_update`,`ticket_main`.`finding_images` AS `finding_images`,`ticket_main`.`resolved_images` AS `resolved_images`,`ticket_main`.`resolved_date` AS `resolved_date`,`ticket_main`.`resolved_solution` AS `resolved_solution`,`store_area`.`area_id` AS `area_id`,`store_area`.`area_name` AS `area`,`ticket_main`.`notes` AS `notes`,`si`.`upload_name` AS `submit_image` from (((((`ticket_main` left join `ticket_category` on((`ticket_main`.`category` = `ticket_category`.`category_id`))) left join `store_area` on((`store_area`.`area_id` = `ticket_category`.`area_id`))) left join `sushitei_ticketdesk`.`users` `creator` on((`ticket_main`.`ticket_creator` = `creator`.`user_id`))) left join `store` on((`ticket_main`.`store` = `store`.`store_id`))) left join `ticket_upload` `si` on((`si`.`ticket_id` = `ticket_main`.`ticket_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area_category_link`
--
ALTER TABLE `area_category_link`
  ADD KEY `link_id` (`link_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD KEY `store_id` (`store_id`);

--
-- Indexes for table `store_area`
--
ALTER TABLE `store_area`
  ADD KEY `area_id` (`area_id`);

--
-- Indexes for table `ticket_category`
--
ALTER TABLE `ticket_category`
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `ticket_group`
--
ALTER TABLE `ticket_group`
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `ticket_log`
--
ALTER TABLE `ticket_log`
  ADD KEY `log_id` (`log_id`);

--
-- Indexes for table `ticket_main`
--
ALTER TABLE `ticket_main`
  ADD KEY `ticket_id` (`ticket_id`);

--
-- Indexes for table `ticket_priority`
--
ALTER TABLE `ticket_priority`
  ADD KEY `priority_id` (`priority_id`);

--
-- Indexes for table `ticket_recommendation`
--
ALTER TABLE `ticket_recommendation`
  ADD PRIMARY KEY (`recommendation_id`);

--
-- Indexes for table `ticket_status`
--
ALTER TABLE `ticket_status`
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `ticket_type`
--
ALTER TABLE `ticket_type`
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `ticket_upload`
--
ALTER TABLE `ticket_upload`
  ADD KEY `upload_id` (`upload_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area_category_link`
--
ALTER TABLE `area_category_link`
  MODIFY `link_id` mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `store_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `store_area`
--
ALTER TABLE `store_area`
  MODIFY `area_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ticket_category`
--
ALTER TABLE `ticket_category`
  MODIFY `category_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `ticket_group`
--
ALTER TABLE `ticket_group`
  MODIFY `group_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_log`
--
ALTER TABLE `ticket_log`
  MODIFY `log_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_main`
--
ALTER TABLE `ticket_main`
  MODIFY `ticket_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_priority`
--
ALTER TABLE `ticket_priority`
  MODIFY `priority_id` smallint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticket_recommendation`
--
ALTER TABLE `ticket_recommendation`
  MODIFY `recommendation_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_status`
--
ALTER TABLE `ticket_status`
  MODIFY `status_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ticket_type`
--
ALTER TABLE `ticket_type`
  MODIFY `type_id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ticket_upload`
--
ALTER TABLE `ticket_upload`
  MODIFY `upload_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
