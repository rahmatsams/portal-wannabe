<?php
    class Controller extends SISTER{
        protected $_controller;
        protected $_action;
        protected $load;
        protected $_session;
        protected $_accessRules;
        
        // Controller Class Constructor
        public function __construct($cons_con, $cons_act)
        {
            $this->_controller = $cons_con;
            $this->_action = $cons_act;
            // Load - Load Class as child Function of this Controller
            $this->load = new Load($this->_controller);
            session_start(); 
            define("__SESS_ID", session_id());
            
            if(!isset($_SESSION[__SESS_ID]) || count($_SESSION[__SESS_ID]['login_info']) == 0) {
                $s = array(
                    'username'  => 'Guest',
                    'group'     => 'Guest',
                    'attempt'   => 0,
                    'role'      => 1
                );
                $this->setSession('login_info', $s);
            }
            $this->_session = $this->getSession();
            if(method_exists($this,'accessRules'))  $this->_accessRules = $this->accessRules();
            
            if(method_exists($this, $cons_act)) {
            
            #if(in_array($this->_action, get_class_methods($this))){
                $allowAccess = 1;
                if($this->checkAccess() > 0) {
                    
                    $this->{$cons_act}();
                    
                } else {
                    if($this->_session['login_info']['username'] == 'Guest'){
                        $this->showError(1);
                    }else{
                        $this->showError(3);
                    }
                    
                }
              
            } else {
                $this->showError(2);
            }
            
            $this->setSession('last_url', "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
            if(count($this->getFlashData()) >= 1) {
                $flashdata = $this->getFlashData();
                ($flashdata['last_uri'] != $_SERVER['REQUEST_URI'] ? $this->unsetFlashData() : '');
            }
        }
        
        // Check Access
        protected function checkAccess(){
            $allowed = 0;
            if(is_array($this->_accessRules)) {
                foreach($this->_accessRules as $rules){
                    if($rules[0] == 'Allow'){
                        foreach($rules['actions'] as $rule){
                            if($rule == $this->_action && isset($rules['users']) && (in_array($this->_session['login_info']['username'], $rules['users'], true) || in_array('*', $rules['users'], true))){
                                $allowed = 1;
                            } elseif($rule == $this->_action && isset($rules['role']) && $rules['role'] > $this->_session['login_info']['role']){
                                $allowed = 1;
                            } elseif($rule == $this->_action && isset($rules['groups']) && (in_array($this->_session['login_info']['group'], $rules['groups'], true) || in_array('*', $rules['groups'], true))){
                                $allowed = 1;
                            }
                        }
                    } elseif($rules[0] == 'Deny') {
                        foreach($rules['actions'] as $rule){
                            if($rule == $this->_action && isset($rules['users']) && (in_array($this->_session['login_info']['username'], $rules['users'], true) || in_array('*', $rules['users'], true))){
                                $allowed = 0;
                            } elseif($rule == $this->_action && isset($rules['role']) && $rules['role'] > $this->_session['login_info']['role']){
                                $allowed = 0;
                            } elseif($rule == $this->_action && isset($rules['groups']) && (in_array($this->_session['login_info']['group'], $rules['groups'], true) || in_array('*', $rules['groups'], true))){
                                $allowed = 0;
                            }
                        }
                    }
                }
            } else{
                $allowed = 1;
            }
            return $allowed;
        }
        
        
        // Show Controller Error
        protected function showError($code)
        {
            switch($code) {
                case 1:
                    header("Location: login.html");
                break;
                case 2:
                    switch(__DEV_MODE){
                        case 0:
                            $this->load->view('error/notfound');
                            break;
                        default:
                            echo "Controller -> {$this->_controller} <br> Action -> {$this->_action} not Found<br>";
                    }
                break;
                case 3:
                    $this->load->view('error/access_denied');
                break;
                default:
                    header("HTTP/1.0 404 Not Found");
            }
        }
        protected function isGuest()
        {
            if($this->_session['login_info']['username'] == 'Guest') {
                return 1;
            } else {
                return 0;
            }
        }
        
        
        protected function setFlashData($input)
        {
            foreach($input as $data => $value) {
                $_SESSION[__SESS_ID]['flash_data'][$data] = $value; 
            }
            $_SESSION[__SESS_ID]['flash_data']['last_uri'] = $_SERVER["REQUEST_URI"];
        }

        protected function getFlashData($opt=0)
        {
            if(isset($_SESSION[__SESS_ID]['flash_data'])) {
                if($opt != 0){
                    return (isset($_SESSION[__SESS_ID]['flash_data'][$opt])) ? $_SESSION[__SESS_ID]['flash_data'][$opt] : '';
                }else{
                    return $_SESSION[__SESS_ID]['flash_data'];
                }
            }else{
                return array();
            }
        }
        
        protected function unsetFlashData()
        {
            if(isset($_SESSION[__SESS_ID]['flash_data'])) {
                unset($_SESSION[__SESS_ID]['flash_data']);
            }
        }
        protected function setSession($name,$value)
        {
            $_SESSION[__SESS_ID][$name] = $value;
        }
        protected function unsetSession($name)
        {
            if(isset($_SESSION[__SESS_ID][$name])) {
                unset($_SESSION[__SESS_ID][$name]);
            }
        }
        protected function getSession() {
            if(!empty($_SESSION[__SESS_ID])) {
                return $_SESSION[__SESS_ID];
            }else{
                return 0;
            }
        }
    }
// End Controller Class