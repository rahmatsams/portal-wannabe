<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Condition extends Controller {

	public function accessRules()
	{
		return array( 
			array('Deny',
				'actions' =>array(''),
				'groups' =>array('Guest')

		),
			array('Allow',
		 		'actions' =>array(''),
		 		'groups'=>array(''); 
		)
	
	);
		
	}

	public function viewConditionIndex()
	{
		
	}

	public function viewManageConditions() 
    {
        $input = $this->load->lib('Input');
        $m_storea = $this->load->model('StoreArea');
        $m_category = $this->load->model('TicketCategory');
        $m_con = $this->load->model('Condition');
        $data = array(
            'session' => $this->_session['login_info'],
            'fdata' => $this->getFlashData(),
        );
        $opt = array();
        if(isset($_POST['action'])){
            $data = array(
                'success' => 0
            );
            /* Fail over if parent is not found */
            $parent = 1;
            if($_POST['action'] == 'Add Condition'){ #value button add new point
                $input->addValidation('parent_max', $_POST['category_id'], 'max=6', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['category_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['condition_name'], 'min=1', 'Cek kembali input anda');
                $input->addValidation('category_max', $_POST['condition_point'], 'min=0', 'Cek kembali input anda');
                if($input->validate()){
                    unset($_POST['action']);
                    if($m_con->createCondition($_POST)){
                        $data['success'] = 1;
                        $parent = $_POST['category_id'];
                    } else {
                        $data['error'] = 'Unknown Error';
                    }
                }else{
                    $data['error'] = $input->_error;
                }
            }elseif($_POST['action'] == 'Edit'){
                $input->addValidation('category_max', $_POST['condition_name'], 'min=1', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['category_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['description'], 'min=0', 'Cek kembali input anda');
                $input->addValidation('point_deduction', $_POST['point_deduction'], 'min=0', 'Cek kembali input anda');
                if($input->validate()){
                    $m_con = $this->load->model('Condition');
                    $parent = $_POST['category_id'];
                    unset($_POST['action']);
                    unset($_POST['category_id']);
                    if($m_con->editCondition($_POST, array('condition_id' => $_POST['condition_id']))){
                        $data['success'] = 1;
                    } else {
                        $data['error'] = 'Unknown Error';
                    }
                }else{
                    $data['error'] = $input->_error;
                }
            }
            $this->setFlashData($data);
            header("Location: ".controllerUrl('Condition','ManageCondition', array('id' => $parent)));
        }else{
            
            if(isset($_GET['id'])){
                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                if($input->validate()){
                    $data['option'] = array(
                        'exjs' => array('./Resources/js/adminprp.js')
                    );
                    $data['result'] = $m_category->getCategoryId(array('cid'=>$_GET['id']));
                    $data['list'] = $m_con->getConditionSub($_GET['id'], $opt);
                    #$data['storearea'] = $m_storea->getStoreAreaId(array('sid'=>$_GET['id']));
                    $this->load->template('admin/category/manage_sub', $data);
                } else {
                    $this->showError(2);
                }
            }else{
                $this->showError(2);
            }
        } 
    }

}

/* End of file Condition.php */
/* Location: .//C/Users/User/Dropbox/htdocs/mrp/Systems/Applications/Controllers/Condition.php */

?>