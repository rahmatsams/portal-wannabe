<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ticket extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewEmail', 'viewEdit', 'viewDeleteMRP', 'viewTicketDetail', 'viewDetail'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin MRP'),
            ),
            array('Allow', 
                'actions'=>array('viewSubmit', 'viewTicket', 'viewGroup', 'viewDeleteRecommend', 'viewAreaData', 'viewTicketDetail'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin MRP', 'Teknisi MRP'),
            ),
            array('Deny', 
                'actions'=>array('viewEdit', 'viewSubmit', 'viewSubmitUpload', 'viewTicket', 'viewDeletePending', 'viewProccessSubmit','viewGroup','viewExportReport', 'viewAreaData'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
	public function site()
    {
        $site = array(
            'root' => 'prp'
        );
        return $site;
    }
	
    public function viewSubmit() 
    {
        $ticket = $this->load->model('Ticket');
        $category = $this->load->model('TicketCategory');
        $area = $this->load->model('StoreArea');
        $m_con = $this->load->model('Condition');
        $sess = $this->getSession();
        $data = array(
			'site' 			 => $this->Site(),
            'page' 			 => 'Submit MRP',
            'session' 		 => $sess['login_info'],
            'condition_list' => $m_con->getAllCondition(),
			'area'			 => $area->getAllArea(),
            'category'       => $category->getAllCategory(),
            'cat'       => $category->getAllCategory(),
			'store_list'	 => $ticket->getAllActiveStore(),
            'option' 		 => array(
                'injs' => array(),
                'exjs' => array('Resources/js/mrpscript.js')
            )
        );
        #upload gambar
        if(isset($_POST['mrp_submit']) && $_POST['mrp_submit'] == "Submit"){
            $form = array();
            foreach($_POST['link'] as $k => $v){
                $form[$k]['category'] = $v;
                $form[$k]['ticket_condition'] = $_POST['conditions'][$k];
                $form[$k]['store'] = $_POST['store'];
                $form[$k]['status'] = $_POST['status'][$k]; #offset 1
                $form[$k]['ticket_creator'] = $sess['login_info']['userid'];
                $form[$k]['submit_date'] = date("Y-m-d");
                $form[$k]['notes'] = $_POST['notes'][$k];
                if(isset($_FILES['image_upload']['error'][$k]) && $_FILES['image_upload']['error'][$k] == 0){
                    $form[$k]['file'][0] = array(
                        'name' => $_FILES['image_upload']['name'][$k],
                        'type' => $_FILES['image_upload']['type'][$k],
                        'tmp_name' => $_FILES['image_upload']['tmp_name'][$k],
                        'error' => $_FILES['image_upload']['error'][$k],
                        'size' => $_FILES['image_upload']['size'][$k],
                    );
                }
            }
            #exit(print_r($_POST));
            if(count($form) > 0){
                $group = array(
                    'store' => $_POST['store'],
                    'group_submit_date' => date("Y-m-d H:i:s"),
                    'uid' => $sess['login_info']['username'],
                    'auditee' => $_POST['auditee'],
                    'group_equipment' => $_POST['area'],
                    'equipment_code' => $_POST['equipment'],
                    'group_is_month' => ($_POST['group_week'] == 'bulanan' ? 1 : 0),
                    'group_month' => date("m", strtotime($_POST['date']."-01")),
                    'group_week' => ($_POST['group_week'] != 'bulanan' ? $_POST['group_week'] : 0),
                    'group_status' => $_POST['group_status'],
                    'group_notes' => $_POST['group_notes'],
                    'group_year' => date("Y", strtotime($_POST['date']."-01"))
                );
                $gid = $ticket->checkTicketGroup($group);
                foreach($form as $post){
                    $data['submit'][$post['category']] = $this->proccessSubmit($post, $gid);
                }
                if(count($form) > 0){
                    $tg = $this->load->model('TicketGroup');
                    $tg->updateGroupByTicket($gid['group_id']);
                    echo "<script>alert('Submit Success'); window.location.replace('index.html');</script>";
                }
            }
        }
        
        $this->load->template('ticket/submit_new', $data);
    }

    protected function proccessSubmit($POST, $gid)
    {
        $data = array();
        $sess = $this->getSession();
        if(1 == 1) {
            $input = $this->load->lib('Input');
            $input->addValidation('store', $POST['store'] ,'min=1','Outlet is must be filled.');
            $input->addValidation('store', $POST['store'] ,'numeric','Invalid Outlet');
            $input->addValidation('category', $POST['category'],'numeric','Invalid Category');
            if(isset($_POST['sub']) && !empty($POST['sub'])){
                $input->addValidation('sub', $POST['sub'],'numeric','Invalid Point');
            }
            
            if ($input->validate()){

                $ticket = $this->load->model('Ticket');

                $mc = $this->load->model('TicketCategory');
                
                $ci = $mc->getCategoryFromLink(array('cid' => $POST['category']));
                
                $POST['ticket_group'] = $gid['group_id'];
              
                if(isset($POST['file'])){
                    $file = $POST['file'];
                    unset($POST['file']);
                    
                    $upload = $this->doUpload($file);
                    if(!isset($upload[0]['error'])){
                        
                        $POST['finding_images'] = $upload[0]['upload_name'];
                        
                    }   
                }

                if($ticket->createTicket($POST)){
                    
                    $data['status'] = $POST['status'];
                    
                } else {
                    $data['error']['unknown'] = 'Unknown Error';
                }                    
                #print_r($POST);
                #exit;
            } else {
                $data['error'] = $input->_error;
            }
        } else {
            $data['error']['recaptcha'] = 1;
        }
        
        return $data;
    }

    
    public function viewAreaData($monthly=0)
    {
        $json = array();
        if(!empty($_GET['monthly']) && $_GET['monthly'] == '1'){
            $monthly = 1;
        }
        $idArea = $_POST['area_id'];
        if ($idArea != "All") {
            $m_con = $this->load->model('Condition');
            $category = $this->load->model('TicketCategory');
            if($monthly == 1){
                $con = $m_con->getAllConditionByAreaMonthly($idArea);
                $data = $category->getCategoryByAreaMonthly($idArea);
            }else{
                $con = $m_con->getAllConditionByArea($idArea);
                $data = $category->getCategoryByArea($idArea);
            }
            $json['category'] = "<option value=\"All\" selected>- All -</option>";
            foreach ($data as $w) {
                $json['category'] .= "
                <option value=\"{$w['category_id']}\">{$w['category_name']}</option>";
            }
            $json['content'] = $this->load->view('ticket/view_area', array('condition_list' => $con));
        }else{
            $json = array(
                'category' => "<option selected disabled>- Item -</option>",
                'content' => ''
            );
        }
        
        echo json_encode($json);
    }

    #group_data.php
    public function viewGroup()
    {
        if(isset($_POST['month']) && isset($_POST['outlet']) && isset($_POST['week'])){
            $input = $this->load->lib('Input');
            $submitted = array(
                'group_store'  => $_POST['outlet'],
                'group_submit_date' => $_POST['month'].'-01',
                'group_week' => $_POST['week']
            );
            $input->addValidation('month_format', $submitted['group_submit_date'], 'date', 'Unknown Format');
            $input->addValidation('store_format', $submitted['group_store'], 'numeric', 'Unknown Format');
            $input->addValidation('store_req', $submitted['group_store'], 'min=1', 'Should be filled');
            $input->addValidation('store_max', $submitted['group_store'], 'max=6', 'Should be filled');
            if($input->validate()){
                $ticket = $this->load->model('Ticket');
                $group = $ticket->verifyTicketGroup($submitted);
                $display = array(
                    'exists' => 0,
                );
                if(is_array($group)){
                    $display = array(
                        'exists' => 1,
                        'url' => getConfig('base_url')."edit_prp_{$group['group_id']}.html",
                        'data' => $group
                    );
                }
                echo json_encode($display);
            }else{
                $display = array(
                    'error' => 1,
                    'message' => $input->_error
                );
                echo json_encode($display);
            }
        }else{
            #print_r($_POST);
            exit('Unknown Error');
        }
        
    }
    
    public function viewDeletePending()
    {
        $sess = $this->getSession();
        if(isset($_POST['submit']) && $_POST['submit'] == 'Delete'){
            $input = $this->load->lib('Input');
            $input->addValidation('upload_id', $_POST['upload_id'] ,'min=1','Upload info not found.');
            $input->addValidation('upload_id', $_POST['upload_id'] ,'numeric','Upload info not found.');
            $input->addValidation('upload_id', $_POST['upload_id'] ,'max=6','Upload info not found.');
            if($input->validate()){
                $m = $this->load->model('Upload');
                $mi = $m->getUploadInfo(array('upload_id' => $_POST['upload_id']));
                $dd = array(
                    'upload_id' => $_POST['upload_id'],
                    'upload_temp' => 1
                );
                if(is_array($mi)){
                    if($m->deletePending($dd)){
                        unlink("{$mi['upload_location']}/{$mi['upload_name']}");
                        $pending = $m->getPendingUpload(array('user_id' => $sess['login_info']['userid']));
                        $this->setSession('pending', $pending);
                        if(count($pending) > 0){
                            header("Location: submit_upload.html");
                        }else{
                            header("Location: submit_mrp.html");
                        }
                    }else{
                        exit('Unexpected Error');
                    }
                }
            }
        }
    }

    #Delete MRP (delete by group)
    public function viewDeleteMRP()
    {
        $id = $_GET['idTicketGroup'];
        $tg = $this->load->model('TicketGroup');
        $gg = $this->load->model('Ticket');
        if(is_array($tg->getTicketGroupBy(array('ticket_group' => $_GET['idTicketGroup'], 'group_store' => $_GET['groupStore'], 'group_year' => $_GET['groupYear'], 'group_month' => $_GET['groupMonth']))))
        {
            $ug = $gg->checkFindingImage(array('ticket_group'=> $_GET['idTicketGroup']));
            foreach($ug AS $img){
                $file = "Resources/images/prp/".$img['finding_images'];
                if (file_exists($file) && is_file($file))
                {
                    unlink($file);               
                }
            }
            $tg->deleteMRP($id);
            echo "<script>window.location.replace('view_mrp_".$_GET['groupStore']."_".$_GET['groupYear']."_".$_GET['groupMonth'].".html');</script>";
        }else{
            exit('No data');
        }
    }    

    #ViewTicket
    public function viewTicket()
    {
        if(isset($_GET['n']) && isset($_GET['groupYear']) && isset($_GET['groupMonth'])){
            $ticket = $this->load->model('Ticket');
            $tg = $this->load->model('TicketGroup');
            $session = $this->_session['login_info'];
            $data = array(
                'site'          => $this->Site(),
                'page'          => 'View MRP',
                'session'       => $session,
                'option'        => array(
                    'injs' => array(),
                    'exjs' => array('Resources/js/mrpscript.js')
                )
            );
            $datapersentase = array();
            $input = $this->load->lib('Input');
            $input->addValidation('idformat', $_GET['n'], 'numeric', 'Unknown Error');
            $input->addValidation('idlen', $_GET['n'], 'min=1', 'Unknown Error');
            $input->addValidation('idmax', $_GET['n'], 'max=6', 'Unknown Error');
            $input->addValidation('yearformat', $_GET['groupYear'], 'numeric', 'Unknown Error');
            $input->addValidation('yearlen', $_GET['groupYear'], 'min=1', 'Unknown Error');
            $input->addValidation('yearmax', $_GET['groupYear'], 'max=4', 'Unknown Error');
            $input->addValidation('monthformat', $_GET['groupMonth'], 'numeric', 'Unknown Error');
            $input->addValidation('monthlen', $_GET['groupMonth'], 'min=1', 'Unknown Error');
            $input->addValidation('monthmax', $_GET['groupMonth'], 'max=2', 'Unknown Error');
            if($input->validate()){
                $persen = $tg->getTicketPercent(array('store_id' => $_GET['n'], 'group_year' => $_GET['groupYear'],'group_month' => $_GET['groupMonth']));
                $new_data = array();
                foreach ($persen as $key) {
                    $hitung = 100-(($key['rusak']/$key['total'])*100);
                    $hasil = number_format($hitung, 0);
                    array_push($key, $hasil);
                    array_push($new_data, $key);
                }
                $data['datapercent'] = $new_data;
                //var_dump($data['datapercent']);exit();
                $data['prp'] = $tg->getTicketGroup(array('store_id' => $_GET['n'], 'group_year' => $_GET['groupYear'],'group_month' => $_GET['groupMonth']));
                if(is_array($data['prp'])){
                    if($session['group'] == 'Super Admin' || $session['group'] == 'Administator' || $session['group'] == 'Admin MRP'){
                        $this->load->template('ticket/view', $data);
                    }elseif($session['group'] == 'Teknisi MRP'){
                        $this->load->template('ticket/view_user', $data);
                    }
                }
            }else{
                $this->showError(2);
            }
            
        }else{
            $this->showError(2);
        }   
    }

    #ViewTicketDetail
    public function viewTicketDetail()
    {
        if(isset($_GET['n'])){
            $ticket = $this->load->model('Ticket');
            $tg = $this->load->model('TicketGroup');
            $session = $this->_session['login_info'];
            $data = array(
                'site'          => $this->Site(),
                'page'          => 'View PRP',
                'session'       => $session,
                'option'        => array(
                    'injs' => array(),
                    'exjs' => array('Resources/js/mrpscript.js')
                )
            );
            $input = $this->load->lib('Input');
            $input->addValidation('idformat', $_GET['n'], 'numeric', 'Unknown Error');
            $input->addValidation('idlen', $_GET['n'], 'min=1', 'Unknown Error');
            $input->addValidation('idmax', $_GET['n'], 'max=6', 'Unknown Error');
            if($input->validate()){
                $data['prp'] = $tg->getTicketGroupDetail(array('group' => $_GET['n']));
                $data['sub'] = $ticket->getTicketByGroup(array('group'=>$_GET['n']));
                if(is_array($data['prp'])){
                    if($session['group'] == 'Super Admin' || $session['group'] == 'Administator' || $session['group'] == 'Admin MRP'){
                        $this->load->template('ticket/view_detail', $data);
                    }else{
                        $this->load->template('ticket/view_detail_user', $data);
                    }
            }
            }else{
                $this->showError(2);
            }
            
        }else{
            $this->showError(2);
        }   
    }

    ##View Group view_group.php
    public function viewDetail()
    {
        
        if(isset($_GET['n']) && isset($_GET['groupYear']) && isset($_GET['groupMonth']) && isset($_GET['groupWeek'])){
            $ticket = $this->load->model('Ticket');
            $tg = $this->load->model('TicketGroup');
            $session = $this->_session['login_info'];
            $data = array(
                'site'          => $this->Site(),
                'page'          => 'View MRP',
                'session'       => $session,
                'option'        => array(
                    'injs' => array(),
                    'exjs' => array('Resources/js/mrpscript.js')
                )
            );
            $datapersentase = array();
            $input = $this->load->lib('Input');
            $input->addValidation('idformat', $_GET['n'], 'numeric', 'Unknown Error');
            $input->addValidation('idlen', $_GET['n'], 'min=1', 'Unknown Error');
            $input->addValidation('idmax', $_GET['n'], 'max=6', 'Unknown Error');
            $input->addValidation('yearformat', $_GET['groupYear'], 'numeric', 'Unknown Error');
            $input->addValidation('yearlen', $_GET['groupYear'], 'min=1', 'Unknown Error');
            $input->addValidation('yearmax', $_GET['groupYear'], 'max=4', 'Unknown Error');
            $input->addValidation('monthformat', $_GET['groupMonth'], 'numeric', 'Unknown Error');
            $input->addValidation('monthlen', $_GET['groupMonth'], 'min=1', 'Unknown Error');
            $input->addValidation('monthmax', $_GET['groupMonth'], 'max=2', 'Unknown Error');
            if($input->validate()){
                $persen = $tg->getTicketPercent(array('store_id' => $_GET['n'], 'group_year' => $_GET['groupYear'],'group_month' => $_GET['groupMonth']));
                $new_data = array();
                foreach ($persen as $key) {
                    $hitung = 100-(($key['rusak']/$key['total'])*100);
                    $hasil = number_format($hitung, 0);
                    array_push($key, $hasil);
                    array_push($new_data, $key);
                }
                $data['datapercent'] = $new_data;
                $data['prp'] = $tg->getGroupDetailPeriode(array('store_id' => $_GET['n'], 'group_year' => $_GET['groupYear'],'group_month' => $_GET['groupMonth'],'group_week' => $_GET['groupWeek']));
                
                if(is_array($data['prp'])){
                    if($session['group'] == 'Super Admin' || $session['group'] == 'Administator' || $session['group'] == 'Admin MRP'){
                        $this->load->template('ticket/view_group', $data);
                    }else{
                        $this->load->template('ticket/view_user', $data);
                    }
                }
            }else{
                $this->showError(2);
            }
            
        }else{
            $this->showError(2);
        }   
    }
    
    public function viewSubmitUpload()
    {
        $ticket = $this->load->model('Ticket');
        $sess = $this->getSession();
        $area = $this->load->model('StoreArea');
        $data = array(
			'site' 			=> $this->Site(),
            'page' 			=> 'Upload',
            'session' 		=> $sess['login_info'],
            'category_list' => $ticket->getMainCategory(),
            'type' 			=> $ticket->getTicketType(),
			'area'			=> $area->getAllArea(),
			'store_list'	=> $ticket->getAllActiveStore(),
            'pending'       => $sess['pending'],
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/mrpscript.js')
            )
        );
        $uploaded = '';
      
        $this->load->template('ticket/pending', $data);
    }
	
    
    /* Edit MRP */
    public function viewEdit()
    {
        $ticket = $this->load->model('Ticket');
        $category = $this->load->model('TicketCategory');
        $area = $this->load->model('StoreArea');
        $tg = $this->load->model('TicketGroup');
        $m_con = $this->load->model('Condition');
        $sess = $this->getSession();
        $data = array(
			'site' 			=> $this->Site(),
            'page' 			=> 'Edit MRP',
            'session' 		=> $sess['login_info'],
            'condition_list' => $m_con->getAllCondition(),
			'area'			=> $area->getAllArea(),
            'category'       => $category->getAllCategory(),
			'store_list'	=> $ticket->getAllActiveStore(),
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/mrpscript.js'),
            ),
            'checklist' => $ticket->getTicketFromGroup(array(
                'ticket_group' => $_GET['id'])
            ),
            'group' => $tg->getTicketGroupDetail(array('group'=>$_GET['id']))
        );
        if(isset($_POST['mrp_edit']) && $_POST['mrp_edit'] == "Edit MRP"){
            
            $form = array();
            
            foreach($_POST['ticket'] as $k => $v){
                $form[$k]['value']['last_update'] = date("Y-m-d H:i:s");
                $form[$k]['value']['status'] = $_POST['status'][$k];
                $form[$k]['value']['last_status'] = $_POST['last_status'][$k];
                $form[$k]['value']['notes'] = (isset($_POST['notes'][$k]) ? $_POST['notes'][$k] : '');
                $form[$k]['where']['ticket_id'] = $_POST['ticket'][$k];
                if(isset($_FILES['image_upload']['error'][$k]) && $_FILES['image_upload']['error'][$k] == 0){
                    $form[$k]['file'][0] = array(
                        'name' => $_FILES['image_upload']['name'][$k],
                        'type' => $_FILES['image_upload']['type'][$k],
                        'tmp_name' => $_FILES['image_upload']['tmp_name'][$k],
                        'error' => $_FILES['image_upload']['error'][$k],
                        'size' => $_FILES['image_upload']['size'][$k],
                    );
                }
            }
            #exit(print_r($form));
            if(count($form) > 0){
                $num = 0;
                foreach($form as $post){
                    $t = $ticket->getTicketDetailForUpdate(array('ticket_id' => $post['where']['ticket_id']));
                    if(($post['value']['status'] != $post['value']['last_status']) || (empty($t['finding_images']) && isset($post['file'])) || ($post['value']['status'] == $post['value']['status'] && isset($post['file']))  || ($post['value']['status'] == $post['value']['status'] && isset($post['value']['notes']) )){
                        $data['submit'][$post['where']['ticket_id']] = $this->proccessEdit($post, $t);
                        $num++;
                    }
                }
                $tg = $this->load->model('TicketGroup');
                $gr_data = array(
                    'value' => array (
                        'group_status' => $_POST['group_status'],
                        'group_notes' => $_POST['group_notes']),
                    'where' => array(
                        'group_id' => $_POST['group']
                    )
                );
                if( $tg->editGroup($gr_data)){
                    #exit(print_r($data));
                    echo "<script>alert('Update Success'); window.location.replace('edit_mrp_{$_GET['id']}.html');</script>";
                }
                else
                {
                    echo "Gagal";
                }
            }
            exit;
        }
        
        $this->load->template('ticket/edit', $data);
        
    }
    
    
    protected function proccessEdit($POST, $ci)
    {
        $data = array();
        $sess = $this->getSession();
        
        $ticket = $this->load->model('Ticket');

        $mc = $this->load->model('TicketCategory');
        $location = "Resources/images/prp/{$ci['finding_images']}";
        
        if($POST['value']['status'] == $POST['value']['status'] && isset($POST['file'])){
            $file = $POST['file'];
            unset($POST['file']);
            if(!empty($ci['finding_images']) && file_exists($location)){
                unlink($location);
            }
            
            $upload = $this->doUpload($file);
            
            if(count($upload) > 0 && !isset($upload[0]['error'])){
                $POST['value']['finding_images'] = $upload[0]['upload_name'];
            }
            
        }
        /*if($POST['value']['status'] == 0 && file_exists($location)){
            $POST['value']['finding_images'] = '';
            unlink($location);
        }*/
        #echo "file: {$ci['finding_images']}";
        #exit(var_dump(file_exists($location)));
        
        unset($POST['value']['last_status']);
        if($ticket->editTicket($POST)){
        #exit(print_r($POST));    
            $data['status'] = $POST['value']['status'];
            
        } else {
            $data['error']['unknown'] = 'Unknown Error';
        }
            
        return $data;
    }
    
    
    public function viewEmail()
    {
        $data = array();
        if(isset($_POST['group'])){
            $input = $this->load->lib('Input');
            $input->addValidation('group_id', $_POST['group'], 'min=1', 'Must be Filled');
            $input->addValidation('group_id', $_POST['group'], 'max=11', 'Unknown Error');
            $input->addValidation('group_id', $_POST['group'], 'numeric', 'Unknown Format');
            if($input->validate()){
                $mtg = $this->load->model('TicketGroup');
                $group = $mtg->getTicketGroup(array('group' => $_POST['group']));
                if(count($group) > 0){
                    $option = array(
                        'title' => "PRP Result #{$group['store_name']}",
                        'view' => 'email/send',
                        'email' => $group['store_email'],
                        'store' => $group['store_name'],
                        'data' => array(
                            'detail' => $group,
                            'content' => 'Test Email PRP',
                            'header' => 'Email tentang PRP.'
                        ),
                    );
                    $email = $this->_sendEmail($option);
                    if($email['email_status'] == 1){
                        $mtg->editGroup(array(
                            'value' => array(
                                'group_status' => 2
                            ),
                            'where' => array(
                                'group_id' => $_POST['group']
                            )
                        ));
                        
                        $data['success'] = 1;
                        $data['email'] = 1;
                    }else{
                        $data['success'] = 0;
                        $data['email'] = $email;
                    }
                    
                }else{
                    $data['success'] = 0;
                    $data['error'] = 'No Data';
                }
            }else{
                $data['success'] = 0;
                $data['error'] = $input->_error;
            }
        }else{
            $data['success'] = 0;
            $data['error'] = 'No request submitted';
             
        }
        echo json_encode($data);
    }
    # Protected function
    
    protected function doUpload($file, $location='Resources/images/prp')
    {
        
        if(count($file) > 0){
            $uploaded = array();
            $num=0;
            $upload = $this->load->lib('Upload', $file);
            for($uid=0;$uid < count($file); $uid++){
                $file[$uid] = new Upload($file[$uid]);
                if($file[$uid]->uploaded) {
                    $file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num;
                    $file[$uid]->image_resize = true;
                    $file[$uid]->image_x = 640;
                    $file[$uid]->image_y = 480;
                    $file[$uid]->allowed = array('image/jpeg', 'image/png');
                    $file[$uid]->image_convert = 'jpg';
                    $file[$uid]->process($location);
                    if ($file[$uid]->processed) {
                        $file[$uid]->clean();
                        $uploaded[$num] = array(
                            'upload_name' => $file[$uid]->file_dst_name,
                            'upload_location' => $file[$uid]->file_dst_path,
                            'upload_time' => date("Y-m-d H:i:s"),
                            'upload_temp' => 0,
                            'upload_user' => $this->_session['login_info']['userid'],
                        );
                        $num++;
                    }else{
                        $uploaded[$num]['error'] = $file[$uid]->error;
                    }
                }
            }
            return $uploaded;
        }
    }
    
    protected function reArrayFiles(&$file_post)
    {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
    
    protected function _sendEmail($option){
        $html = $this->load->view($option['view'], $option['data']);
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail   = new PHPMailer(true);
        #$stmail = $this->load->lib('STMail');
        try
        {
            $stmail->IsSMTP(); // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "it1.jkt@sushitei.co.id"; // SMTP account username
            $stmail->Password   = "Fangblade_99";        // SMTP account password

            $stmail->SetFrom('it1.jkt@sushitei.co.id', 'PRP');

            $stmail->AddReplyTo('it1.jkt@sushitei.co.id', 'PRP');
            
            $url = siteUrl()."report_prp_{$option['data']['detail']['group_id']}.html";
            
            $stmail->addStringAttachment(file_get_contents($url), 'Hasil PRP.xlsx');
            
            $stmail->isHTML('true');

            $stmail->Subject    = $option['title'];
            
            $stmail->MsgHTML($html);
            
            $mr = $this->load->model('Email');
            
            $stmail->AddAddress($option['email'], $option['store']);
            
            foreach($mr->getAllActiveEmail() as $edata){
                $stmail->AddBCC($edata['email'], $edata['pic_name']);
            }
            
            #$m = $this->load->model('Event');
            #foreach($m->getAdminAddress() as $recipient){
            #    $stmail->AddBCC($recipient['email'], $recipient['display_name']);
            #}
            
            $sd = array(
                'user' => 'Rahmat Samsudin',
                'email_status'  => $stmail->Send()
            );
            return $sd;
            
        } catch (phpmailerException $e) {
            $sd = array(
                'user' => 'Rahmat Samsudin',
                'email_status'  => 0,
                'message' => $e->errorMessage()
            );
            
            return $sd;
            
        } catch (Exception $e) {
                $sd = array(
                    'user' => $option['recipient']['address'],
                    'email_status'  => 0,
                    'message' => $e->getMessage(),
                    'data' => $returned
                );
                return $sd;
        }
    }
}
/*
* End Home Class
*/