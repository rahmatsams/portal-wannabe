<?php 
/**
 * 
 */
class Email extends Controller
{
	
	public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('index', 'edit_user'),
                'groups'=>array('Guest'),
                
            ),
            array('Allow', 
                'actions'=>array('viewEmailIndex', 'viewNewEmail', 'viewEditEmail', 'viewDeleteEmail'),
                'groups'=>array('Super Admin', 'Administrator' , 'Admin QA'),
            )
        );
    }
    
    protected function site()
    {
        $site = array(
            'root' => 'admin'
        );
        return $site;
    }

    #Email Index
    function viewEmailIndex()
    {
    	$m_email = $this->load->model('Email');
    	$query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'pic_name',
            'order' => 'ASC'
        );
    	$data = array(
			'session' => $this->_session['login_info'],
			'email' => $m_email->getAllEmail($query_option),
			'total' => $m_email->getCountResult(),
			'page' => $query_option['page'],
            'max_result' => $query_option['result'],
            'option' => array(
                'exjs' => array(
                    './Resources/js/admin_category.js'
                )
            ),  	
		);
        #echo 'console.log('. json_encode( $data ) .')';
		$this->load->template('admin/email/index', $data);
    }

    #Add New Email
    function viewNewEmail()
    {
    	$m_email = $this->load->model('Email');
    	$data = array(
    		'session' => $this->_session['login_info'],
            'page' => 'Manage Email',
    	);
    	if (isset($_POST['submit']) && $_POST['submit'] == 'create_email') {
    		$input = $this->load->lib('Input');
            $input->addValidation('pic_name', $_POST['pic_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('pic_name', $_POST['pic_name'], 'min=1', 'Must be filled');
            $input->addValidation('pic_name', $_POST['pic_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('email', $_POST['email'], 'min=1', 'Must be filled');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('email_status', $_POST['email_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('email_status', $_POST['email_status'], 'min=1', 'Check your input');
            $input->addValidation('email_status', $_POST['email_status'], 'max=1', 'Check your input');
            if ($input->validate()) {
            		$email = array(
            			'pic_name' => $_POST['pic_name'],
            			'email' => $_POST['email'],
            			'email_status' => $_POST['email_status'] 
            		);
            		if ($m_email->newEmail($email)) {
            			header("Location: admin_email.html");
            		}
            	}else{
            		$data['error'] = $input->_error;
            		$this->load->template('admin/email/new', $data);
            	}	
    	}else{
    		$this->load->template('admin/email/new', $data);

    	}
    }

    #Edit Email
    function viewEditEmail()
    {
    	$m_email = $this->load->model('Email');
    	$input = $this->load->lib('Input');
    	$data = array(
    		'session' => $this->_session['login_info'],
            'page' => 'Manage Email',
    	);
    	if (isset($_POST['submit']) && $_POST['submit'] == 'edit_email') {
    		$input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            $input->addValidation('pic_name', $_POST['pic_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('pic_name', $_POST['pic_name'], 'min=1', 'Must be filled');
            $input->addValidation('pic_name', $_POST['pic_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('email', $_POST['email'], 'min=1', 'Must be filled');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('email_status', $_POST['email_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('email_status', $_POST['email_status'], 'min=1', 'Check your input');
            $input->addValidation('email_status', $_POST['email_status'], 'max=1', 'Check your input');
            if ($input->validate()) {
            	$email = array(
            		'pic_name' => $_POST['pic_name'],
            		'email' => $_POST['email'],
            		'email_status' => $_POST['email_status'] 
            	);
            	if ($m_email->editEmail($email, array('email_id' => $_GET['id']))) {
            		header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
            	}
            }else{
            	$data['error'] = $input->_error;
            	$this->load->template('admin/email/edit', $data);
            }
    	}else{
    		if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            	$input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                if($input->validate()){
                    $data['option'] = array(
                        'exjs' => array('./Resources/js/adminprp.js')
                    );
                    $data['email'] = $m_email->getEmailById(array('email_id'=>$_GET['id']));
                    $this->load->template('admin/email/edit', $data);
                } else {
                    $this->showError(2);
                }
            }else{
                $this->showError(2);
            }
    	}	
    }

    #Delete Email
    public function viewDeleteEmail()
    {
        $id = $_GET['idEmail'];
        $m_email = $this->load->model('Email');
        $m_email->deleteEmail($id);
        header("Location: admin_email.html");
    }

}

 ?>