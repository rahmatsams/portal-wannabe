<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Home extends Controller
{
    
    public function site()
    {
        $site = array(
            'template' => getConfig('default_template'),
            'root' => 'home'
        );
        return $site;
    }
    
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewIndex', 'viewAjaxIndex'),
                'role'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Guest'),
            ),
            
        );
    }
    
    public function viewIndex()
    {
        $t = $this->load->model('Ticket');
        $tg = $this->load->model('TicketGroup');
        $df = array(
            'year' => date("Y"),
            'month' => date("m")
        );
        
        $session = $this->getSession();
        $data = array(
            'site'          => $this->Site(),
            'page'          => 'Dashboard',
            'session'       => $session['login_info'],
            'option'        => array(
                'injs' => array(),
                'exjs' => array('Resources/js/indexmrp.js')
            )
        );
        if($session['login_info']['group'] == 'Administrator' || $session['login_info']['group'] == 'Super Admin' || $session['login_info']['group'] == 'Admin MRP' || $session['login_info']['group'] == 'Admin View' || $session['login_info']['group'] == 'Teknisi MRP'){
            $data['result'] = $this->prpListAdmin($df);
            $flag = 0;
            foreach ($data['result'] as $get) {
                $persen = $tg->getTicketAverage($get['store_id']);
                $jumlah_data = 0;
                $new_data = array();
                $hasil2 = 0;
                foreach ($persen as $key) {
                    if ($df['year'] == $key['group_year'] && $df['month'] == $key['group_month']) {
                        
                        $hitung = 100-(($key['rusak']/$key['total'])*100); //previous formula
                        $hasil = number_format($hitung, 0);
                        // penjumlahan equipment 
                        $hasil2 += $hasil;
                    }                    
                    $jumlah_data++;
                }
                
                if ($hasil2 != 0) {
                    
                    $hasil2 = number_format($hasil2/$jumlah_data, 0);
                    
                    array_push($new_data, $hasil2);
                } else {
                    array_push($new_data, $hasil2);
                }
                
                $data['result'][$flag]['persen_equipment'] = $new_data[0];
                $flag++;
            }
            $this->load->template('home/index', $data);
        }else{
            $df['group'] = $session['login_info']['outlet'];
            $data['result'] = $this->prpListUser($df);
            $this->load->template('home/index_user', $data);
        }
            
    }
    
    public function viewIndexDetail()
    {
        $t = $this->load->model('Ticket');
        $df = array(
            'submit' => date("Y-m-1"),
            'end' => date("Y-m-t")
        );
        $session = $this->getSession();
        $data = array(
            'site'          => $this->Site(),
            'page'          => 'Dashboard',
            'session'       => $session,
            'option'        => array(
                'injs' => array(),
                'exjs' => array('Resources/js/indexprp.js')
            )
        );

        if($session['login_info']['group'] == 'Administrator' || $session['login_info']['group'] == 'Super Admin' || $session['login_info']['group'] == 'Admin MRP'){
            $data['result'] = $this->prpListAdmin($df);
            $this->load->template('home/index', $data);
        }else{
            $data['result'] = $this->prpListUser($df);
            $this->load->template('home/index_user', $data);
        }
            
    }
    
    public function viewIndexSub()
    {
        $t = $this->load->model('Ticket');
        $df = array(
            'submit' => date("Y-m-1"),
            'end' => date("Y-m-t")
        );
        $session = $this->getSession();
        $data = array(
            'site'          => $this->Site(),
            'page'          => 'Dashboard',
            'session'       => $session,
            'option'        => array(
                'injs' => array(),
                'exjs' => array('Resources/js/indexprp.js')
            )
        );

        if($session['login_info']['group'] == 'Administrator' || $session['login_info']['group'] == 'Super Admin' || $session['login_info']['group'] == 'Admin MRP'){
            $data['result'] = $this->prpListAdmin($df);
            $this->load->template('home/index', $data);
        }else{
            $data['result'] = $this->prpListUser($df);
            $this->load->template('home/index_user', $data);
        }
    }
    
    public function viewAjaxIndex()
    {
        $session = $this->getSession();
        $df = array(
            'year' => date("Y"),
            'month' => date("m")
        );
        $data = array();
        if(isset($_POST['date'])){
            $input = $this->load->lib('Input');
            $_POST['date'] .= '-01';
            $input->addValidation('date_req', $_POST['date'], 'min=1', 'Invalid Month Format');
            $input->addValidation('date_format', $_POST['date'], 'date', 'Invalid Month Format');        
            if($input->validate()){
                $df['year'] = date("Y", strtotime($_POST['date']));
                $df['month'] = date("m", strtotime($_POST['date']));
                if($session['login_info']['group'] == 'Administrator' || $session['login_info']['group'] == 'Super Admin' || $session['login_info']['group'] == 'Admin MRP' || $session['login_info']['group'] == 'Admin View'){
                    $data['success'] = 1;
                    $data['data'] = $this->prpListAdmin($df);
                    
                }else{
                    $df['group'] = $session['login_info']['outlet'];
                    $data['success'] = 1;
                    $data['data'] = $this->prpListUser($df);
                }
            }else{
                $data['success'] = 0;
                $data['error'] = $input->_error;
            }
        }
        echo json_encode($data);
    }
    
    protected function prpListAdmin($df)
    {
        $t = $this->load->model('Ticket');

        return $t->getTicketAdmin($df);
    
    }
    
    protected function prpListUser($df)
    {
        $t = $this->load->model('TicketGroup');
        
        return $t->getTicketGroupByStore($df);
    }
}
/*
* End Home Class
*/