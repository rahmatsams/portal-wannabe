<?php
/*
* Model for Ticketdesk
*/
class ModelTicketCategory extends Model
{
	public $lastInsertID;
    function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, point_deduction FROM ticket_category WHERE category_parent_id=0 AND deleted=0';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
    
    function getAllCategory()
    {
        $query_string = 'SELECT 
            tc.category_id, 
            tc.category_name, 
            tp.area_id, 
            tp.area_name AS area_name FROM ticket_category tc 
                LEFT JOIN store_area tp ON tc.area_id=tp.area_id 
                WHERE tc.deleted=0 AND tp.area_status=1 
                    ORDER BY tc.area_id ASC';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }

    function getCategoryByMonthly()
    {
        $query_string = "SELECT 
                            category_name 
                            FROM ticket_category 
                            WHERE category_monthly=1";
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
	
    function getCategoryId($input){ #dtambah i area_id
        $query_string = 'SELECT 
            s.category_id, 
            s.category_name, 
            s.area_id, 
            r.area_name
            FROM ticket_category s
            LEFT JOIN store_area r
            ON s.area_id=r.area_id 
            WHERE category_id=:cid AND deleted=0';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }
    
	function getSubCategory($id, $opt)
	{
		$query = 'SELECT * FROM ticket_category WHERE area_id=:cat_id AND deleted=0';
		$value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query, $value, $opt);
		return $result;
	}
    
    function getSubCategoryId($input)
    {
        $query = "SELECT * FROM ticket_category WHERE category_id=:cid";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function getParentCategory($id)
	{
		$query_string = 'SELECT * FROM ticket_category WHERE category_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function createCategory($form){
		try{
			$result = $this->insertQuery('ticket_category', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
		} catch(Exception $e){
			if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
		}
	}
    function editCategory($form, $where){
			$result = $this->editQuery('ticket_category', $form, $where);
            return $result;
    }
        
    function getCategoryFromLink($input){
        $query_string = 'SELECT ticket_category.category_id, category_name, condition_id, condition_name FROM ticket_condition LEFT JOIN ticket_category ON (ticket_condition.category_id=ticket_category.category_id AND ticket_category.deleted=0) WHERE condition_id=:cid';
        $result = $this->fetchSingleQuery($query_string, $input);
		return $result;
    }

    #delete sub category
    function deleteSubCategory($id)
    {
        $query = $this->doQuery("DELETE FROM ticket_category WHERE category_id=$id");
    }

    public function getCategoryByArea($idArea)
    {
        #$query = "SELECT * FROM ticket_category WHERE area_id={$idArea}"; 
        #return $this->fetchAllQuery($query);
        $where_clause = '';
        if ($idArea != 'All'){
            $where_clause = " area_id={$idArea} AND category_monthly=0";
        }
        $where_clause = "SELECT * FROM ticket_category WHERE ".$where_clause;

        return $this->fetchAllQuery($where_clause);
    }
    public function getCategoryByAreaMonthly($idArea)
    {
        #$query = "SELECT * FROM ticket_category WHERE area_id={$idArea}"; 
        #return $this->fetchAllQuery($query);
        $where_clause = '';
        if ($idArea != 'All'){
            $where_clause = " area_id={$idArea} ";
        }
        $where_clause = "SELECT * FROM ticket_category WHERE".$where_clause;

        return $this->fetchAllQuery($where_clause);
    }

    public function getDataByArea($idArea)
    {
        $query = "SELECT * FROM ticket_category WHERE area_id={$idArea}"; 

        return $this->fetchAllQuery($query);
    }
}