<?php
/*
* Model for TicketCondition
*/
class ModelCondition extends Model {

	function getAllCondition()
	{
		$query_string = "SELECT 
						con.condition_id, 
						con.condition_name, 
						tc.category_id, 
						tc.category_name AS category_name ,
                        tc.area_id AS area,
                        k.area_name AS area_name
						FROM  ticket_category tc
						LEFT JOIN ticket_condition con ON con.category_id=tc.category_id
						LEFT JOIN store_area k ON tc.area_id=k.area_id 
						ORDER BY tc.category_id ASC, con.condition_id ASC ";

		$result = $this->fetchAllQuery($query_string);
        return $result;
	}

	function getAllConditionByArea($idArea)
	{
		
		$where_clause = '';
		if ($idArea != 'All'){
            $where_clause = " tc.area_id={$idArea} AND tc.category_monthly=0";
		} 
		$where_clause = "SELECT 
						con.condition_id, 
						con.condition_name, 
						tc.category_id, 
						tc.category_name AS category_name ,
                        tc.area_id AS area,
                        k.area_name AS area_name
						FROM  ticket_category tc
						LEFT JOIN ticket_condition con ON con.category_id=tc.category_id
						LEFT JOIN store_area k ON tc.area_id=k.area_id
						WHERE". $where_clause ." 
						ORDER BY tc.category_id ASC, con.condition_id ASC ";
		$result = $this->fetchAllQuery($where_clause);
		#$result = $this->fetchAllQuery(" ");
        return $result;
	}
    
    function getAllConditionByAreaMonthly($idArea)
	{
		
		$where_clause = '';
		if ($idArea != 'All'){
		$where_clause = " AND tc.area_id={$idArea} ";
		} 
		$where_clause = "SELECT 
						con.condition_id, 
						con.condition_name, 
						tc.category_id, 
						tc.category_name AS category_name ,
                        tc.area_id AS area,
                        k.area_name AS area_name
						FROM  ticket_category tc
						LEFT JOIN ticket_condition con ON con.category_id=tc.category_id
						LEFT JOIN store_area k ON tc.area_id=k.area_id
						WHERE 1=1". $where_clause ." 
						ORDER BY tc.category_id ASC, con.condition_id ASC ";
		$result = $this->fetchAllQuery($where_clause);
		#$result = $this->fetchAllQuery(" ");
        return $result;
	}

	function getConditionById($input)
	{
		#$query = "SELECT * FROM ticket_condition WHERE condition_id=:nid";
		$query = "SELECT 
					a.condition_id,
					a.condition_name,
					b.category_id,
					b.category_name,
					c.area_id,
					c.area_name
				FROM ticket_category b
				LEFT JOIN ticket_condition a ON a.category_id=b.category_id
				LEFT JOIN store_area c ON c.area_id=b.area_id WHERE a.condition_id=:nid";
		$result = $this->fetchSingleQuery($query, $input);
		return $result;
	}

	function getConditionSub($id, $opt)
	{
		$query = 'SELECT * FROM ticket_condition WHERE category_id=:cat_id';
		$value = array(
			'cat_id' => $id 
		);
		$result = $this->fetchAllQuery($query, $value, $opt);
		return $result;
	}

	function getSubConditionId($input)
	{
		$query = "SELECT * FROM ticket_condition WHERE condition_id=:tid";
		$result = $this->fetchSingleQuery($query, $input);
		return $result;
	}

	#create
	function createCondition($form)
	{
		try {
			$result = $this->insertQuery('ticket_condition', $form);
			$this->lastInsertID = $this->lastInsertID();
			return 1;
		} catch (Exception $e) {
			if (getConfig('development') == 1) {
				echo $e;
				exit;
			}
			return 0;
		}
	}

	#edit
	function editCondition($form, $where)
	{
		$result = $this->editQuery('ticket_condition', $form, $where);
		return $result;
	}

	#delete
	function deleteCondition($id)
	{
		$query = $this->doQuery("DELETE FROM  ticket_condition WHERE condition_id=$id");
	}
	

}
