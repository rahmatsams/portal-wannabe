<?php 
/**
 * Model for Email

 */
class ModelEmail extends Model
{
	
	function getAllEmail($query_option)
	{
		$query_string = "SELECT * FROM ticket_email WHERE email_status=1 ";
		$count_query = "SELECT COUNT(*) AS row_total FROM ticket_email ";

		$result = $this->pagingQuery($query_string, $count_query, $query_option);
		return $result;
	}

	function getAllActiveEmail()
	{
		$query_string = "SELECT * FROM ticket_email WHERE email_status=1 ";

		$result = $this->fetchAllQuery($query_string);
        return $result;
	}

	function getEmailById($input)
	{
		$query_string = "SELECT * FROM ticket_email ";
		$query_string .= "WHERE email_id=:email_id ";
		$result = $this->fetchSingleQuery($query_string, $input);
		return $result;
	}

	function newEmail($input)
	{
		$result = $this->insertQuery('ticket_email', $input);
		return $result;
	}

	function editEmail($form  = array(), $where = array())
	{
		$table = 'ticket_email';
		$result = $this->editQuery($table, $form, $where);
		return $result;
	}

	function deleteEmail($id)
	{
		$query_string = $this->doQuery("DELETE FROM ticket_email WHERE email_id=$id ");
	}
	
}
 ?>