
			<div class="maincontent">
				<div class="content">
					<?php 
						$mainquotation = array('quotation_id'=>$quotation[0]['tender_no'],
											'company'=>$quotation[0]['company_name'],
											'address'=>$quotation[0]['company_address'],
											'contact'=>$quotation[0]['contact_person'],
											'fax'=>$quotation[0]['company_fax'],
											'e_mail'=>$quotation[0]['email'],
											'deadline'=>$quotation[0]['quotation_deadline'],
											'requirement'=>$quotation[0]['date_requirement']
											);
						
					?>
					<h3>Quotation Detail</h3>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Quotation Number</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['quotation_id']; ?></div>
							</div>
					</div>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Company Name</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['company']; ?></div>
							</div>
					</div>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Company Address</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['address']; ?></div>
							</div>
					</div>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Contact Person</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['contact']; ?></div>
							</div>
					</div>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Company Fax Number</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['fax']; ?></div>
							</div>
					</div>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Contact E-mail</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['e_mail']; ?></div>
							</div>
					</div>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Deadline Date</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['deadline']; ?></div>
							</div>
					</div>
					<div id="quotation-form">
							<div class="formline">
								<div class="formcolumn">Date Requirement</div>
								<div class="formcolumn">:</div>
								<div class="formcolumn"><?php echo $mainquotation['requirement']; ?></div>
							</div>
					</div>
					<?php echo '<div class="formline formlinehead">
						<div class="detailcolumn">No.</div>
						<div class="detailcolumn">Item</div>
						<div class="detailcolumn">Quantity</div>
						<div class="detailcolumn">Requirement date</div>
						<div class="detailcolumn">Description (Quotation Detail)</div>
					</div>';
					$i = 0;
					foreach($quotation as $data){
						if(!empty($data) && count($data) != 0){
							echo '
					<div class="formline">
						<div class="detailcolumn">'.($i+1).'</div>
						<div class="detailcolumn">'.$data['item_id'].'</div>
						<div class="detailcolumn">'.$data['quantity'].'</div>
						<div class="detailcolumn">'.$data['requirement_date'].'</div>
						<div class="detailcolumn">'.$data['description'].'</div>
					</div>';
						$i++;
						}
					}
					?>
				</div>
			</div>

		