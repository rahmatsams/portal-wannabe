<?php 
    $cat = $result['category_name'];
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="admin_category.html">Manage Equipment</a></li>
        <li class="breadcrumb-item"><a href="admin_edit_storearea_<?=$result['area_id']?>.html">Manage <?=$result['area_name']?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><b>Item <?= $cat ?></b></li>
<!--         <li class="breadcrumb-item active" aria-current="page">Item <?= substr($cat, 0, 10), ".."?> </li>
         -->        <!-- <li class="breadcrumb-item active" aria-current="page">Item <?= substr($cat, 0, 9), ".."?> </li> -->
        
    </ol>
</nav>

<?php
    if(is_array($fdata) && count($fdata) > 0){
        if($fdata['success'] == 1){
        echo '<div class="p-3 mb-2 bg-success text-white">Updated Successfully !!</div>';
    }else{
        echo '<div class="p-3 mb-2 bg-danger text-white">Please check your input !!</div>';
        }
    }
?>

<div class="card-deck">
    <div class="card col-lg-12 px-0 mb-4"> 
        <!-- <div class="card-header">
            <strong class="card-title">Manage Item Pemeriksaan / Perawatan</strong>
        </div> -->
<div class="card-body">
    <div class="card-deck">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Edit Item Pemeriksaan / Perawatan</strong>
            </div>
        <div class="card-body p-0 pt-3 pb-3">
            <form id="editcategory" method="POST" action="admin_edit_subcategory_<?=$result['category_id']?>.html">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Item Name</label>
                    <input id="subid" name="category_id" type="hidden" value="<?=$result['category_id']?>" class="form-control" maxlength="6" required>
                    <input id="catid" name="area_id" type="hidden" value="<?=$result['area_id']?>" class="form-control" maxlength="6" required>
                    <textarea id="catname" name="category_name" cols="50" rows="2" class="form-control" required><?=$result['category_name']?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" name="action" value="Edit Item" class="btn btn-md btn-success waves-effect">
                </div>
            </div>
            </form>
        </div>
        </div>
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Add Point</strong>
            </div>
        <div class="card-body p-0 pt-3 pb-3">
            <form id="addsubcategory" method="POST" action="admin_edit_condition.html">
                <div class="col-md-12 form-group">
                    <label for="insertCatName">Point Name</label>
                    <input id="subid" name="category_id" type="hidden" value="<?=$result['category_id']?>" class="form-control" maxlength="6" required>
                    <textarea name="condition_name" id="insertCatName" class="form-control" cols="70" rows="2" required></textarea>
                </div>
                <div class="col-md-12 form-group">
                    <input type='submit' class='btn btn-md btn-primary' name="action" value="Tambah">
                </div>
            </form>
        </div>
        </div>
        </div>
        </div>
<div class="card-header">
    <strong class="card-title">Manage Point</strong>
</div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Point Name</th> 
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
            if(count($list) > 0){
                foreach($list as $result){
                echo "
                <tr class='form-group' id='formEdit{$result['condition_id']}'>
                <form method='POST' action='admin_edit_condition_{$result['category_id']}.html'>
                    <td>
                        <input type='hidden' name='category_id' value='{$result['category_id']}' readonly>
                        <input type='hidden' name='condition_id' value='{$result['condition_id']}' readonly>{$result['condition_id']}
                    </td>
                    <td>
                        <textarea name='condition_name' cols='70' rows='3' readonly class='form-control-plaintext' required>{$result['condition_name']}</textarea>
                    </td>                
                    <td>
                        <a href=\"admin_edit_action_{$result['condition_id']}.html\" class='manage-button' data-id='{$result['condition_id']}'><button type=\"button\" class=\"btn btn-success btn-md btn-block\">Edit</button></a><input type='submit' name='action' class='btn btn-warning btn-md btn-block d-none' value='Edit'>
                    </td>
                    <td>
                        <a href=\"#\" class=\"delete_sub deleteconfirmationmodal\" data-id=\"{$result['condition_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"btn btn-danger btn-md btn-block\">Hapus</button></a>
                    </td>
                </form>
                </tr>
                ";
                    }
                        }else{
                echo '
                    <tr>
                        <td colspan="6" align="center">No Record</td>
                    </tr>';
                        }
        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            <!-- DELETE CONDITION -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">Hapus Sub Condition</h5>
                                    </div>
                                    <div class="modal-body">
                                        Apa kamu yakin ingin menghapus Sub Category ini?
                                    </div>
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-secondary btn-md" data-dismiss="modal">Batal</button> 
                                        <a id="admin_condition_delete" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger btn-md">Ya</button></a>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
            </div>
        </div>
        <!-- JQUERY DELETE SUB CATEGORY -->
                <script type="text/javascript">
                    $(document).ready(function(){
                        $(".delete_sub").click(function() {
                            var id = $(this).attr('data-id'); /*data-id ada di button Hapus*/
                            var cat_id = $("#subid").val();
                            $("#admin_condition_delete").attr("href", "admin_condition_delete_"+id+"_"+cat_id+".html"); 
                        });
                    });
                </script>