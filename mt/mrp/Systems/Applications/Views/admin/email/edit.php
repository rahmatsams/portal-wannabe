<?php 
    $em = $email['pic_name'];
?>
<div class="content-right">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="administrator.html">Administrator Page</a></li>
        <li class="breadcrumb-item active"><a href="admin_email.html">Manage Email</a></li>
        <li class="breadcrumb-item active"><?= substr($em, 0, 4), ".." ?></li>
    </ol>

    <div class="card-body col-lg-6">
        <div class="card-deck">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Edit Detail Email</strong>
                </div>  
<div class="card-body p-0 pt-3 pb-3">
    <form id="editStore" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
        <div class="col-md-12 form-group">
            <?=(isset($error) && isset($error['pic_name']) ? "<div class=\"alert alert-danger\"><strong>PIC Name </strong>{$error['pic_name']}</div>" : '<label>PIC Name</label>')?>
            <input name="pic_name" type="text" placeholder="Name" value="<?=$email['pic_name']?>" class="form-control" maxlength="25" required>
        </div>
<!-- Email -->
        <div class="col-md-12 form-group">
            <?=(isset($error) && isset($error['email']) ? "<div class=\"alert alert-danger\"><strong>Email </strong>{$error['email']}</div>" : '<label>Email</label>')?>
            <input name="email" type="email" placeholder="Email" value="<?=$email['email']?>" class="form-control" maxlength="50" required>
        </div>
        <div class="col-md-12 form-group">
            <?=(isset($error) && isset($error['email_status']) ? "<div class=\"alert alert-danger\"><strong>Status </strong>{$error['email_status']}</div>" : '<label>Status</label>')?>
            <select name="email_status" class="form-control">
            <option value="0" <?=($email['email_status'] == 0 ? 'selected' : '')?>>Disabled</option>
            <option value="1" <?=($email['email_status'] == 1 ? 'selected' : '')?>>Enabled</option>
            </select>
        </div>
        <div class="col-md-12 form-group">
            <button type="submit" name="submit" value="edit_email" class="btn btn-primary">Edit</button>
        </div>
        </div>
    </form>
</div>
</div>
					