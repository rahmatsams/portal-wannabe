            <div class="card-deck">
            <div class="card">
              <div class="card-header">
                <strong class="card-title">Dashboard</strong>
              </div>
              <div class="card-body">
                <form id="searchPRP" class="forms-sample">
                    <div class="form-group">
                      <label for="selectMonth">Tahun</label>
                      <select name="date" class="form-control p-input" id="selectYear">
                        <?php
                            $year = 13;
                            for($i=0; $i < $year; $i++){
                                $y = date('Y') - $i;
                                echo "<option value='{$y}'>{$y}</option>
                        ";
                            }
                        ?>
                      </select>
                    </div>
                </form>
                <div class="table-responsive">
                  <table id="indexContent" class="table center-aligned-table table-striped">
                    <thead>
                      <tr class="text-primary">
                        <th>Bulan</th>
                        <th>Total Finding</th>
                        <th>Pengurangan</th>
                        <th>Nilai Saat ini</th>
                        <th>Email</th>
                      </tr>
                    </thead>
                    <tbody>
						<?php
							if(is_array($result) && count($result) > 0){
								foreach($result as $d){
									$c = $d['default_point'] - $d['group_point'];
                                    $s = ($d['group_status'] == 0) ? '<button class="btn btn-sm btn-danger">Not Sent</button>' : '<button class="btn btn-small btn-success">Sent</button>';
									echo "<tr>
							<td>".(isset($d['group_id']) ? "<a href=\"view_prp_{$d['group_id']}.html\" class='btn btn-primary btn-block'>".strftime("%B", strtotime($d['group_date']))."</a>" : $d['store_name'])."</td>
							<td>{$d['group_finding']}</td>
							<td>{$d['group_point']}</td>
							<td>{$c}</td>
                            <td>{$s}</td>
						</tr>";
								$i++;
								}
							}else{
                                echo '<tr>
                            <td colspan="5">Tidak ada data PRP</td>
                        </tr>';
                            }
						?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>