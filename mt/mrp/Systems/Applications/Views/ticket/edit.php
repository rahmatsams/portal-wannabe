                <?php
                    $ad = '';
                    $sl = '';
                    $cls = '';
                    foreach($area as $data){
                        $selected = (isset($input) && $input['area'] == $data['area_id']) ? 'selected' : '';
                        $ad .= "
                                                <option value=\"{$data['area_id']}\" {$selected}>{$data['area_name']}</option>";
                    }
                    foreach($category as $ctg){
                        $cls .= "
                                <option value=\"{$ctg['category_id']}\">{$ctg['category_name']}</option>";
                    }
                    foreach($store_list as $store){
                        $selected = (isset($input) && $input['store'] == $store['store_id']) ? 'selected' : '';
                        $sl .= "
                                                <option value=\"{$store['store_id']}\" {$selected}>{$store['store_name']}</option>";
                    }
                ?>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#"><b><?=$group['store_name']?> - <?=strftime("%B %Y", strtotime($group['group_submit_date']))?></b></a></li>
                </ol>
                <?php 
                $monthly = $group['group_week'];
                $gstatus = $group['group_status'];
                ?>
                <div class="card-deck">
                    <div class="card col-lg-12 px-0 mb-4">  
                        <div class="card-header">
                            <strong class="card-title">Edit Equipment <?=$group['equipment']?> - <?= $group['group_week'] == 0 ? 'Bulanan' : 'Minggu '.$monthly ?></strong>
                        </div>
                        <?=(isset($result) && isset($result['error']['exists'])) ? '<div class="p-3 mb-2 bg-warning text-dark">This MRP Already Submitted by '.$result['submitted']['creator_name'].'</div>' : '';?>
                    
                        <div class="card-body">
                            <form id="editMRP" class="forms-sample" method="POST" enctype="multipart/form-data" action="edit_mrp_<?=$_GET['id']?>.html">
                                <input type='hidden' name='group' value='<?=$group['group_id']?>'>
                                <div class="table-responsive" id="tableForm">
                                    <table class="table center-aligned-table table-striped">
                                        <thead>
                                            <tr class="text-primary text-center">
                                                <th style="min-width: 275px;">Point MRP</th>
                                                <th>Status</th>
                                                <th>Keterangan</th>
                                                <th>Upload</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                            <?php
                                #print_r($checklist);
                                $last = '';
                                $num = 0;
                                if(is_array($checklist) && count($checklist) > 0){
                                    foreach($checklist as $cl){
                                        $enable = ($cl['status'] == 1 ? '' : '');
                                        echo "
                                        <input type='hidden' name='group' value='{$cl['ticket_group']}'>";
                                        if($last != $cl['category_id']){
                                            echo '
                                                <tr class=\"bg-primary text-white table-area\" data-area=\"'.$cl['area'].'\" data-category=\"'.$cl['category_id'].'\">
                                                <td colspan=6><strong>'.$cl['category_name'].'</strong>
                                                </td>
                                                </tr>
                                                ';
                                            $last = $cl['category_id'];
                                        }
                                        echo "
                                        <tr class='table-area control-row ".($cl['status'] == 1 ? 'bg-success text-white' : 'bg-danger')."' data-id='{$num}' data-area='{$cl['area_id']}' data-category='{$cl['category_id']}'>
                                            <td>{$cl['condition_name']}</td>
                                            <td class=''>
                                                <div class='form-group col-lg-12 mx-auto'>
                                                <select id='selectHasil' name='status[{$num}]' class='form-control selectHasil' data-id='{$num}'>
                                                    <option value='0' ".($cl['status'] == 0 ? 'selected' : '').">Tidak</option>
                                                    <option value='1' ".($cl['status'] == 1 ? 'selected' : '').">Ya</option>
                                                </select> 
                                                </div>
                                                <input type='hidden' name='ticket[{$num}]' value='{$cl['ticket_id']}'>
                                                <input type='hidden' name='last_status[{$num}]' value='{$cl['status']}'>
                                            </td>
                                            <td><textarea class='form-control note-trigger mx-auto' name='notes[{$num}]' style='padding:5px; margin:5px;'>{$cl['notes']}</textarea>
                                            </td>
                                            <td>".(!empty($cl['finding_images']) ? "<a href=\"./Resources/images/prp/{$cl['finding_images']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$cl['finding_images']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/{$cl['finding_images']}\"></a><input type='file' accept='image/*' id='img_{$num}' class='d-none' name='image_upload[{$num}]'><label class='btn btn-sm btn-secondary' for='img_{$num}'>Select File</label>" : "<input type='file' accept='image/*' id='img_{$num}' class='d-none' name='image_upload[{$num}]'><label class='btn btn-sm btn-secondary' for='img_{$num}'>Select File</label>")."</td>
                                            
                                        </tr>";
                                        $num++;
                                        
                                    }
                                }
                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan=2>Kondisi</td>
                                                <td colspan=4>
                                                    <select name='group_status' class='form-control' required>
                                                        <option value='Normal' <?= ($group['group_status'] == 'Normal' ? 'selected' : '') ?> >Normal</option>
                                                        <option value='Kurang' <?= ($group['group_status'] == 'Kurang' ? 'selected' : '') ?> >Kurang Baik</option>
                                                        <option value='Rusak' <?= ($group['group_status'] == 'Rusak' ? 'selected' : '') ?> >Rusak</option>
                                                    </select>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan=2>Catatan</td>
                                                <td colspan=4>
                                                    <textarea class='form-control note-trigger mx-auto' name='group_notes' style='padding:5px; margin:5px;'><?= $group['group_notes'] ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><input type="submit" id="submitButton" class="btn btn-lg btn-primary" name="mrp_edit" value="Edit MRP"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-warning text-white" style="position: fixed; bottom: 40px; right: 30px; z-index: 99; border-radius: 4px;">Top</button>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#editMRP input[type="file"]').change(function(event){
                            if($(this).val() != ''){
                                readURL(this, $(this));
                            }else{
                                $(this).parent().find('label').html('Select File');
                                $(this).parent().find('img').remove();
                            }
                        });
                    });

                    function readURL(input, input2) {

                      if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function(e) {
                            input2.parent().find('label').html('Ganti');
                            input2.parent().find('img').remove();
                            input2.parent().append("<img src='"+e.target.result+"' style='width:50px; height:50px;'>");
                        }
                        reader.readAsDataURL(input.files[0]);
                      }
                    }
                </script>