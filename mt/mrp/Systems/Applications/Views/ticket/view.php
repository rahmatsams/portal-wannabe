<style>
    .modal-mrp 
    {
    width: 850px;
    position: relative;
    }
    
    .modal-admin 
    {
    width: 900px;
    position: relative;
    }

    #rcorners1 {
      border-radius: 15px;
      background: #ABB2B9;
      padding-top: 5px;
      padding-bottom: 30px; 
      width: 200px;
      height: 15px;  
    }

    #rcorners2 {
      border-radius: 15px;
      border: 2px solid #ABB2B9;
      padding-top: 5px;
      padding-bottom: 15px; 
      width: 200px;
      height: 15px;  
    }

</style>

<div class="card">
  <div class="card-header">
    Summary Maintenance MRP
  </div>
  <div class="card-body">
    <h5 class="card-title"><?=$prp[0]['store']?> - <?=date('F Y',strtotime($prp[0]['group_submit_date'])) ?> <?="<a href='report_outlet_{$prp[0]['store_id']}_{$prp[0]['group_year']}_{$prp[0]['group_month']}.html'>(<i class='menu-icon fa fa-download'></i>)</a>"?></h5>
  </div>
</div>
<div class="card-deck">
    <div class="card">
        <div class="card-body"> 
            <div class="container">
            <?php
                    $current_week = '';
                    if(is_array($prp) && count($prp) > 0){                    
                        $flag=1; //periode
                        $first = 1;
                        foreach($prp as $d){
                            if($current_week != $d['group_week']){
                                
                                foreach ($datapercent as $value) {
                                    if ($value['group_week'] == $d['group_week']) {
                                        $retVal = $value[0];
                                    }                                     
                                }
                                $monthly = $d['group_week'];
                                if($first == 0){
                                    echo '</tbody></table></div>';
                                    $first = 1;
                                }
                                echo "
                                        <div class='bg-info text-white col-lg-12 mb-2 p-2'>
                                            <div class='col-lg-9'>".($d['group_week'] == 0 ? 'Bulanan' : 'Minggu '.$monthly)." &nbsp;({$retVal}%)</div>
                                            <div class='col-lg-3'><button class='btn btn-small btn-secondary text-white show' value='category{$flag}'>View</button></div>
                                        </div>
                                    
                                        <div class='table-responsive category{$flag}' style='display:none;'>
                                            <table id='indexContent' class='table center-aligned-table table-striped'>
                                            <thead>
                                                <tr class='text-primary'>
                                                    <th>Nama Alat</th>
                                                    <th>Code</th>
                                                    <th>Kondisi</th>
                                                    <th>Catatan</th>
                                                    <th>Perintah</th>
                                                </tr>
                                            </thead>
                                            <tbody>";
                                            
                                
                                
                                $first = 0;
                                $flag++;
                                $current_week = $d['group_week'];
                            }
                            echo "
                                <tr>
                                    <td>{$d['area']}</td>
                                    <td>{$d['equipment_code']}</td>
                                    <td>
                                    ".($d['group_status'] == 'Normal' ? 'Normal' : '' )." 
                                    ".($d['group_status'] == 'Kurang' ? 'Kurang Baik' : '' )."
                                    ".($d['group_status'] == 'Rusak' ? 'Rusak' : '' )."

                                    </td>
                                    <td>{$d['group_notes']}</td>
                                    <td>
                                        <a href=\"view_mrp_detail_{$d['group_id']}.html\" class='btn btn-info'>Detail</a>
                                        <a href=\"report_mrp_{$d['group_id']}.html\"><button class=\"btn btn-small btn-secondary\">Unduh</button></a>
                                        <a href=\"edit_mrp_{$d['group_id']}.html\" class='btn btn-success'>Edit</a>
                                        <input id=\"group_store\" value=\"{$d['store_id']}\" type=\"hidden\" class=\"form-control\" maxlength=\"6\" required>
                                        <input id=\"group_year\" value=\"{$d['group_year']}\" type=\"hidden\" class=\"form-control\" maxlength=\"6\" required>
                                        <input id=\"group_month\" value=\"{$d['group_month']}\" type=\"hidden\" class=\"form-control\" maxlength=\"6\" required>
                                        <a class=\"delete_group deleteconfirmationmodal btn btn-danger\" data-id=\"{$d['group_id']}\" data-toggle=\"modal\" data-target=\"#confirmDeleteMRP\" href=\"#\">Hapus</a>
                                    </td>
                                </tr>";
                                
                            
                        }                        

                    }else{
                        echo '<div class="p-3 mb-2 bg-success text-white">Tidak ditemukan data bulan ini</div>';
                    }
                ?>
            </div>
        </div>                      
    </div>
</div>          
        <!-- MODAL EMAIL-->
        
        <div id="successModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title">Success</h4></div>
                        <div class="modal-body">Email has been sent to Outlet.</div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="failedModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title">Failed</h4></div>
                        <div class="modal-body">Send Email Failed.</div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- DELETE MRP -->
        <div class="modal fade" id="confirmDeleteMRP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title" id="myModalLabel">Hapus MRP</h4></div>
                        <div class="modal-body">Apa kamu yakin ingin menghapus Ticket di periode ini?</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <a id="admin_delete_group" href="<?= "delete_mrp_".$d['group_id']."_".$d['group_store']."_".$d['group_year']."_".$d['group_month'].".html"; ?>"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                              
                        </div>
                      </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="viewMRP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="vertical-alignment-helper">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title" id="myModalLabel">Detail MRP</h4></div>
                        <div class="modal-body"><?= $d['area']; ?></div>
                        
                      </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $(".delete_group").click(function() {
                    var id = $(this).attr('data-id');
                    var idstore = $("#group_store").val();
                    var idyear = $("#group_year").val();
                    var idmonth = $("#group_month").val();
                    $("#admin_delete_group").attr("href", "delete_mrp_"+id+"_"+idstore+"_"+idyear+"_"+idmonth+".html"); 
                });

                //js view hide show
                $(".show").click(function(){
                    var flag = $(this).val();
                    $('.'+flag).toggle(500);
                });
            });
        </script> 