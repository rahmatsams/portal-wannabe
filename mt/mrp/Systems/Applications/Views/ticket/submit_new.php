                <?php
                    $ad = '';
                    $sl = '';
                    $cls = '';
                    $cus = '';
                    foreach($area as $data){ /*data store area*/
                        $selected = (isset($input) && $input['area'] == $data['area_id']) ? 'selected' : '';
                        $ad .= "
                                            <option value=\"{$data['area_id']}\" {$selected}>{$data['area_name']}</option>";
                    }

                    /*data category*/
                    foreach($category as $ctg){
                        $cls .= "
                                            <option value=\"{$ctg['category_id']}\">{$ctg['category_name']}</option>";
                    }

                    foreach($store_list as $store){
                        $selected = (isset($input) && $input['store'] == $store['store_id']) ? 'selected' : '';
                        $sl .= "
                                            <option value=\"{$store['store_id']}\" {$selected}>{$store['store_name']}</option>";
                    }
                ?>
                <div class="card-deck">
                    <div class="card col-lg-12 px-0 mb-4">  
                        <div class="card-header">
                            <strong class="card-title">Submit Maintenance</strong>
                        </div>
                        <!-- Warning Submit -->
                        <?=(isset($result) && isset($result['error']['exists'])) ? '<div class="p-3 mb-2 bg-warning text-dark">This MRP Already Submitted by '.$result['submitted']['creator_name'].'</div>' : '';?>
                    
                        <div class="card-body">
                            <form id="submitMRP" class="forms-sample" method="POST" enctype="multipart/form-data" action="submit_mrp.html">
                                <div class="form-row">
                                    <div class="form-group col-lg-4">                           
                                        <label for="selectOutlet" class="font-weight-bold">Outlet*</label>
                                        <select id="selectOutlet" name="store" class="form-control" required>
                                            <option value="" selected> - Outlet -</option><?=$sl?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="selectWeek" class="font-weight-bold">Periode*</label>
                                        <select id='selectWeek' name='group_week' class='form-control' required>
                                            <option value='' selected>- Periode -</option>
                                            <option value='1'>Minggu 1</option>
                                            <option value='2'>Minggu 2</option>
                                            <option value='3'>Minggu 3</option>
                                            <option value='4'>Minggu 4</option>
                                            <option value='5'>Minggu 5</option>
                                            <option value='bulanan'>Bulanan</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="selectMonth" class="font-weight-bold">Bulan</label>
                                        <input type="month" name="date" class="form-control" id="selectMonth" aria-describedby="emailHelp" placeholder="Select Month" <?=(isset($input)) ? "value=\"{$input['date']}\"" : 'value="'.date("Y-m").'"'?> required>
                                    </div>
                                    <div class="form-group col-lg-1">
                                        <label for="inputCode" class="font-weight-bold">Kode Alat</label>
                                        <input type="text" name="equipment" class="form-control" id="inputCode" aria-describedby="inputAudite" placeholder="AB00 .." required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-lg-4">
                                        <label for="selectArea" class="font-weight-bold">Peralatan*</label>
                                        <select id="selectArea" name="area" class="form-control" onChange="" style="padding-right: 10px" required>
                                            <option value="All" selected>- Nama Alat -</option><?=$ad?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="selectCategory" class="font-weight-bold">Item*</label>
                                        <select id="selectCategory" name="" class="form-control selectCategory" required disabled>
                                            <option value="All" selected>- Item -</option><?=$cls?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="inputAudite" class="font-weight-bold">Tim Outlet*</label>
                                        <input type="text" name="auditee" class="form-control" id="inputAudite" aria-describedby="inputAudite" placeholder="Nama Tim Outlet.." required>
                                    </div>  
                                </div>
                                <div id="tableForm">
                                </div> 
                                <!-- TABLEFORM -->
                            </form>
                        </div>
                    </div>
                </div>
                <div id="statusModal" class="modal fade" tabindex="-1" role="loading" data-backdrop="static" data-keyboard="false">
                    <div class="vertical-alignment-helper">
                        <div class="modal-dialog vertical-align-center">
                            <div class="modal-content">
                                <div class="modal-header bg-success text-white">
                                    <h4 class="modal-title" id="myModalLabel">Sudah pernah disubmit</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>