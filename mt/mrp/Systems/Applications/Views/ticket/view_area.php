<div class="table-responsive" id="tableForm">
    <table class="table center-aligned-table table-striped" id="tableForm">
         <thead>
            <tr class='text-primary text-center'>
                <th style="min-width: 225px;">Point MRP</th>
                <th style="max-width: 50px;"></th>
                <th>Status</th>
                <th>Keterangan</th>
                <th>Upload</th>
            </tr>
        </thead>
    <tbody>
        <?php 
         $last = '';
            $num = 0;
                if(is_array($condition_list) && count($condition_list) > 0){
                    
                    foreach($condition_list as $cl){
                        if($last != $cl['category_id'] && !empty($cl['condition_id'])){ 
                            echo '
                                <tr class=\"bg-secondary text-white table-area\" data-area=\"'.$cl['area'].'\" data-category=\"'.$cl['category_id'].'\">
                                <td colspan=6><strong>'.$cl['category_name'].'</strong>

                                </td>
                                </tr>';
                                $last = $cl['category_id'];
                        }
                        if(!empty($cl['condition_id'])){
                            echo " 
                                <tr class='table-area control-row' data-id='{$num}' data-category='{$cl['category_id']}' data-area='{$cl['area']}'>
                                <td colspan='2' >{$cl['condition_name']}
                                <input type='hidden' name='conditions[{$num}]' value='{$cl['condition_id']}'>
                                
                                </td>
                                <td>
                                    <div class='form-group col-lg-12 mx-auto'>
                                    <select id='selectHasil' name='status[{$num}]' class='form-control selectHasil' data-id='{$num}'>
                                        <option value='1'>Ya</option>
                                        <option value='0'>Tidak</option>
                                    </select> 
                                    </div>
                                </td>
                                <td>
                                    <div class='form-group col-lg-12 mx-auto'>
                                    <input type='hidden' name='link[{$num}]' value='{$cl['category_id']}'>
                                    <textarea class='form-control note-trigger mx-auto' name='notes[{$num}]' style='padding:5px; margin:5px;' required></textarea>
                                    </div>
                                </td>
                                <td>
                                    <div class='form-group col-lg-12 mx-auto'>
                                    <input type='file' accept='image/*' id='img_{$num}' class='d-none file-trigger' name='image_upload[{$num}]'>
                                    <label class='btn btn-sm btn-secondary' for='img_{$num}'>Select File</label>
                                    </div>
                                </td>
                            </tr>";
                            $num++;
                        }
                    }
                }

         ?>
         </tbody>
                    <tfoot class="bg-success text-white">
                        <tr>
                            <td colspan=3>Kondisi</td>
                            <td colspan=3>
                                <select name='group_status' class='form-control' required>
                                    <option value='Normal'>Normal</option>
                                    <option value='Kurang'>Kurang Baik</option>
                                    <option value='Rusak'>Rusak</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=3>Catatan</td>
                            <td colspan=3>
                                <textarea class='form-control note-trigger mx-auto' name='group_notes' style='padding:5px; margin:5px;' ></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=6><input type="submit" id="submitButton" class="btn btn-lg btn-primary" name="mrp_submit" value="Submit"></td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
                <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-warning text-white" style="position: fixed; bottom: 40px; right: 30px; z-index: 99; border-radius: 4px;">Top</button>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#submitMRP input[type="file"]').change(function(event){
                            if($(this).val() != ''){
                                readURL(this, $(this));
                            }else{
                                $(this).parent().find('label').html('Select File');
                                $(this).parent().find('img').remove();
                            }
                        });
                    });

                    function readURL(input, input2) {

                      if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function(e) {
                            input2.parent().find('label').html('Change');
                            input2.parent().find('img').remove();
                            input2.parent().append("<img src='"+e.target.result+"' style='width:50px; height:50px;'>");
                        }
                        reader.readAsDataURL(input.files[0]);
                      }
                    }
                </script>