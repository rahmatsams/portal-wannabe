
            <h3 class="page-heading mb-4">Pending Files</h3>
            

            <div class="card-deck">
                <div class="form-row col-lg-12">
                    <div class="form-group col-lg-6">
                        <label for="selectOutlet">Outlet</label>
                        <select id="selectOutlet" name="store" class="form-control" required>
                            <option value="">--Select Outlet--</option>
                            <?php
                                
                                foreach($store_list as $store){
                                    $selected = (isset($input) && $input['store'] == $store['store_id']) ? 'selected' : '';
                                    echo "
                                    <option value=\"{$store['store_id']}\" {$selected}>{$store['store_name']}</option>";
                                }
                            ?>
                        </select>
                    
                    </div>
                    <div class="form-group col-lg-6">
                          <label for="exampleInputEmail1">Month</label>
                          <input type="month" name="date" class="form-control p-input" id="selectMonth" aria-describedby="emailHelp" placeholder="Select Month"<?=(isset($input)) ? "value=\"{$input['date']}\"" : ''?>>
                    </div>
                </div>
                <div class="card col-lg-12 px-0 mb-12 scrollspy-example"  data-spy="scroll" data-offset="0">
                    <div class="card-body">
                        <?php
                            
                            if(isset($pending) && count($pending) > 0){
                                $cat = '';
                                foreach($category_list as $category){
                                    $cat .= "
                                                        <option value=\"{$category['category_id']}\">{$category['category_name']}</option>";
                                }
                                
                                foreach($pending as $row){
                                    echo "<div class='p-3 mb-2 bg-light list-pending'>
                                <div class='row'>
                                    <div class='col-lg-6'>
                                        <img src=\"{$row['upload_location']}/{$row['upload_name']}\" class='img-fluid'>
                                    </div>
                                    <div class='col-lg-6'>
                                        <form id='form{$row['upload_id']}' action='admin_prp_submit.html' method='POST'>
                                            <input type='hidden' name='upload_id' value='{$row['upload_id']}'>
                                            <input type='hidden' name='upload_temp' value='{$row['upload_temp']}'>
                                            <input type='hidden' class='store-trigger' name='store' value='0'>
                                            <input type='hidden' class='month-trigger' name='month' value='".date("Y-m")."'>
                                            <div class='row'>
                                                <div class='col-lg-6'>
                                                    <div class='form-group'>
                                                        <label for='selectCategory'>Category</label>
                                                        <select name='category' class='form-control category-trigger' data-id='{$row['upload_id']}' required>
                                                            <option value='0'>--Select Category--</option>{$cat}
                                                        </select>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label for='selectSub'>Point</label>
                                                        <select name='sub' class='form-control sub-trigger' disabled required>
                                                            <option value='0'>--Select Point--</option>
                                                        </select>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label for='selectCategory'>Area</label>
                                                        <select name='area' class='form-control area-tigger' disabled required>
                                                            <option value='0'>--Select Area--</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class='form-group col-lg-6'>
                                                    <label for='inputNote'>Notes</label>
                                                    <textarea id='inputNote' name='notes' class='form-control' rows='11' required></textarea>
                                                </div>
                                            </div>
                                        <div class='row'>
                                            <div class='form-group col-lg-6'>
                                                <input type='submit' value='Save' class='btn btn-md btn-info btn-block save-pending'>
                                            </div>
                                        </form> 
                                            <div class='form-group col-lg-6'>
                                                <form method='POST' action='admin_delete_pending.html'>
                                                    <input type='hidden' name='upload_id' value='{$row['upload_id']}'>
                                                    <input type='submit' name='submit' value='Delete' class='btn btn-md btn-danger btn-block delete-pending'>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>";
                        ?>
                        <?php
                                }
                            }else{
                        ?>
                        
                        No Data
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>