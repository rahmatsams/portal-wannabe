-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2018 at 04:30 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sushitei_mrp`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_category_link`
--

CREATE TABLE `area_category_link` (
  `link_id` mediumint(6) UNSIGNED NOT NULL,
  `category` smallint(5) UNSIGNED DEFAULT NULL,
  `area` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` smallint(3) UNSIGNED NOT NULL,
  `location_name` varchar(30) NOT NULL,
  `location_status` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `location_name`, `location_status`) VALUES
(1, 'Jakarta', 1),
(2, 'Surabaya', 1),
(3, 'Makasar', 1),
(4, 'Palembang', 1),
(5, 'Bali', 1),
(6, 'Bandung', 1),
(7, 'Jogja', 1),
(8, 'Medan', 1),
(9, 'Pekan Baru', 1),
(10, 'Batam', 1),
(11, 'Bengkulu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `store_id` smallint(3) UNSIGNED NOT NULL,
  `store_name` varchar(30) NOT NULL,
  `store_email` varchar(50) NOT NULL,
  `store_status` tinyint(1) UNSIGNED NOT NULL,
  `location_id` smallint(3) UNSIGNED NOT NULL,
  `default_point` smallint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`store_id`, `store_name`, `store_email`, `store_status`, `location_id`, `default_point`) VALUES
(1, 'Plaza Indonesia', '', 1, 1, 100),
(2, 'Plaza Senayan', '', 1, 1, 100),
(3, 'Pondok Indah Mall 2', '', 1, 1, 100),
(4, 'Senayan City', '', 1, 1, 100),
(5, 'Mall Kelapa Gading', '', 1, 1, 100),
(6, 'Emporium Pluit', '', 1, 1, 100),
(7, 'Central Park', 'test.jkt@sushitei.co.id', 1, 1, 100),
(8, 'Gandaria City', '', 1, 1, 100),
(9, 'Flavour Bliss', '', 1, 1, 100),
(10, 'Supermall Karawaci', '', 1, 1, 100),
(11, 'Grand Indonesia', '', 1, 1, 100),
(12, 'Lotte Avenue', 'manager.pim@sushitei.co.id', 1, 1, 100),
(13, 'Kota Kasablanka', '', 1, 1, 100),
(14, 'Summarecon Bekasi', '', 1, 1, 100),
(15, 'Bintaro Xchange', 'it6.jkt@sushitei.co.id', 1, 1, 100),
(16, 'The Breeze', '', 1, 1, 100),
(17, 'Puri Mall', '', 1, 1, 100),
(18, 'Summarecon Serpong', '', 1, 1, 100),
(19, 'Margo City', '', 1, 1, 100),
(20, 'Kemang Village', '', 1, 1, 100),
(100, 'Sushi Kiosk Puri', '', 1, 1, 100),
(103, 'aa test test store', 'it6.jkt@sushitei.co.id', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_area`
--

CREATE TABLE `store_area` (
  `area_id` smallint(5) NOT NULL,
  `area_name` varchar(50) NOT NULL,
  `area_desc` text NOT NULL,
  `area_status` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store_area`
--

INSERT INTO `store_area` (`area_id`, `area_name`, `area_desc`, `area_status`) VALUES
(16, 'Walk-In Freezer', '', 1),
(17, 'General', '', 1),
(18, 'Back Kitchen', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_actions`
--

CREATE TABLE `ticket_actions` (
  `action_id` int(11) NOT NULL,
  `action_name` varchar(50) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `action_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_actions`
--

INSERT INTO `ticket_actions` (`action_id`, `action_name`, `condition_id`, `action_desc`) VALUES
(4, 'Dilakukan pengencangan ', 24, ''),
(5, 'Kondisi Indikator', 25, ''),
(6, 'Dilakukan Pengukuran ', 26, ''),
(7, 'Diusulkan utk Diperbaiki', 27, ''),
(8, 'Dilakukan Pembersihan', 28, ''),
(9, 'Dilakukan pembersihan', 36, ''),
(10, 'Dilakukan pembersihan', 37, ''),
(11, 'Dilakukan pembersihan', 38, ''),
(12, 'Dilakukan penataan', 39, ''),
(13, 'Dilakukan pembersihan', 29, ''),
(14, 'Diperiksa', 30, ''),
(15, 'Diperiksa', 31, ''),
(16, 'Dilakukan pelumasan', 32, ''),
(17, 'Saran utk diperbaiki', 33, ''),
(18, 'Saran utk diperbaiki', 34, ''),
(19, 'Saran utk diperbaiki', 35, ''),
(20, 'Sudah baik dan bersih', 40, ''),
(21, 'Sudah sesuai standard, baik, dan bersih', 41, ''),
(22, 'tindakan back kitchen', 42, '');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_category`
--

CREATE TABLE `ticket_category` (
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `category_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `area_id` smallint(5) UNSIGNED DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `point_deduction` decimal(3,0) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_category`
--

INSERT INTO `ticket_category` (`category_id`, `category_name`, `area_id`, `description`, `point_deduction`, `deleted`) VALUES
(221, 'Sistem Panel', 16, '', '0', 0),
(222, 'Storage', 16, '', '0', 0),
(223, 'Condensing Unit', 16, '', '0', 0),
(224, 'Lain-lain', 16, '', '0', 0),
(225, 'Sink untuk Cuci Tangan', 17, '', '0', 0),
(226, 'Penampilan Staff ', 17, '', '0', 0),
(227, 'test back kitchen', 18, '', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_condition`
--

CREATE TABLE `ticket_condition` (
  `condition_id` int(11) NOT NULL,
  `condition_name` varchar(100) NOT NULL,
  `category_id` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_condition`
--

INSERT INTO `ticket_condition` (`condition_id`, `condition_name`, `category_id`) VALUES
(24, 'Terminasi Instalasi Listrik (Baik/baut terminal listrik Kencang)', 221),
(25, 'Indikator Digital (Baik/berfungsi)', 221),
(26, 'Indikator Lampu (Baik/berfungsi)', 221),
(27, 'Saklar (Baik/berfungsi)', 221),
(28, 'Kondisi Panel (Bersih tidak berdebu)', 221),
(29, 'Pemeriksaan Cabin dan Unit', 222),
(30, 'Pemeriksaan Handle Pintu', 222),
(31, 'Pemeriksaan Gasket Pintu', 222),
(32, 'Pemeriksaan Engsel Pintu', 222),
(33, 'Pemeriksaan pada Evaporator', 222),
(34, 'Pemeriksaan pada Fan Evaporator', 222),
(35, 'Pemeriksaan Heater defrost', 222),
(36, 'Pemeriksaan Instalasi Refrigerant', 223),
(37, 'Pemeriksaan pada Fan Condensor', 223),
(38, 'Pengukuran arus listrik pada compressor', 223),
(39, 'Kapasitas produk didalam cold storage', 224),
(40, 'Sink untuk cuci tangan sudah lengkap dan sesuai, bersih dan rapi', 225),
(41, 'Penampilan staff sesuai, baik, dan bersih', 226),
(42, 'test 123', 227);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_email`
--

CREATE TABLE `ticket_email` (
  `email_id` smallint(5) UNSIGNED NOT NULL,
  `pic_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_email`
--

INSERT INTO `ticket_email` (`email_id`, `pic_name`, `email`, `email_status`) VALUES
(19, 'fina aj', 'it6.jkt@sushitei.co.id', 1),
(20, 'Fina 1', 'fnaljh89@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_group`
--

CREATE TABLE `ticket_group` (
  `group_id` mediumint(11) UNSIGNED NOT NULL,
  `group_store` smallint(3) UNSIGNED DEFAULT NULL,
  `group_date` date DEFAULT NULL,
  `group_week` tinyint(1) NOT NULL,
  `group_submit_date` date DEFAULT NULL,
  `group_finding` smallint(2) UNSIGNED DEFAULT NULL,
  `group_user` varchar(50) DEFAULT NULL,
  `group_auditee` varchar(50) DEFAULT NULL,
  `group_point` smallint(3) UNSIGNED DEFAULT NULL,
  `group_status` char(10) DEFAULT NULL,
  `group_equipment` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_group`
--

INSERT INTO `ticket_group` (`group_id`, `group_store`, `group_date`, `group_week`, `group_submit_date`, `group_finding`, `group_user`, `group_auditee`, `group_point`, `group_status`, `group_equipment`) VALUES
(37, 1, '2018-11-01', 1, '2018-11-14', 1, 'Fina Alfiatul J', 'fina', 0, '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_log`
--

CREATE TABLE `ticket_log` (
  `log_id` int(11) UNSIGNED NOT NULL,
  `log_title` text NOT NULL,
  `ticket` mediumint(11) UNSIGNED DEFAULT NULL,
  `user` smallint(3) UNSIGNED NOT NULL,
  `log_type` varchar(20) NOT NULL,
  `log_content` text NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_main`
--

CREATE TABLE `ticket_main` (
  `ticket_id` mediumint(11) UNSIGNED NOT NULL,
  `ticket_group` mediumint(11) UNSIGNED DEFAULT NULL,
  `category` smallint(5) UNSIGNED NOT NULL,
  `condition` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `actions` int(11) NOT NULL,
  `hasil_pemeriksaan` tinyint(1) NOT NULL,
  `prp_date` date DEFAULT NULL,
  `ticket_creator` smallint(4) UNSIGNED NOT NULL,
  `submit_date` date DEFAULT NULL,
  `finding_images` text,
  `resolved_date` date DEFAULT NULL,
  `resolved_images` text,
  `resolved_solution` text,
  `status` tinyint(1) NOT NULL,
  `last_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `store` smallint(3) DEFAULT NULL,
  `notes` text NOT NULL,
  `ticket_hasil` smallint(5) UNSIGNED NOT NULL,
  `ticket_tindakan` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_main`
--

INSERT INTO `ticket_main` (`ticket_id`, `ticket_group`, `category`, `condition`, `condition_id`, `actions`, `hasil_pemeriksaan`, `prp_date`, `ticket_creator`, `submit_date`, `finding_images`, `resolved_date`, `resolved_images`, `resolved_solution`, `status`, `last_update`, `store`, `notes`, `ticket_hasil`, `ticket_tindakan`) VALUES
(55, 37, 227, 0, 42, 0, 0, '2018-11-01', 6497, '2018-11-14', NULL, NULL, NULL, NULL, 1, NULL, 1, 'ini sudah ok', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_pemeriksaan`
--

CREATE TABLE `ticket_pemeriksaan` (
  `id_pemeriksaan` int(11) NOT NULL,
  `hasil_pemeriksaan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_pemeriksaan`
--

INSERT INTO `ticket_pemeriksaan` (`id_pemeriksaan`, `hasil_pemeriksaan`) VALUES
(1, 'OK'),
(2, 'NOT OK');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_priority`
--

CREATE TABLE `ticket_priority` (
  `priority_id` smallint(13) UNSIGNED NOT NULL,
  `priority_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_priority`
--

INSERT INTO `ticket_priority` (`priority_id`, `priority_name`) VALUES
(1, 'Rendah'),
(2, 'Sedang'),
(3, 'Tinggi'),
(4, 'ASAP');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_recommendation`
--

CREATE TABLE `ticket_recommendation` (
  `recommendation_id` mediumint(11) UNSIGNED NOT NULL,
  `recommendation_content` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_feedback` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_image` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `recommendation_status` tinyint(1) DEFAULT NULL,
  `group_id` mediumint(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_status`
--

CREATE TABLE `ticket_status` (
  `status_id` smallint(3) UNSIGNED NOT NULL,
  `status_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_status`
--

INSERT INTO `ticket_status` (`status_id`, `status_name`) VALUES
(1, 'Normal'),
(1, 'Finding'),
(2, 'Resolved');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_type`
--

CREATE TABLE `ticket_type` (
  `type_id` tinyint(2) UNSIGNED NOT NULL,
  `type_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_type`
--

INSERT INTO `ticket_type` (`type_id`, `type_name`) VALUES
(1, 'Finding'),
(2, 'Resolved');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_upload`
--

CREATE TABLE `ticket_upload` (
  `upload_id` smallint(3) UNSIGNED NOT NULL,
  `upload_name` varchar(50) NOT NULL,
  `upload_location` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` mediumint(11) UNSIGNED NOT NULL,
  `upload_temp` tinyint(1) DEFAULT '0',
  `upload_user` smallint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_upload`
--

INSERT INTO `ticket_upload` (`upload_id`, `upload_name`, `upload_location`, `upload_time`, `ticket_id`, `upload_temp`, `upload_user`) VALUES
(1, '15308173990.jpg', 'Resources/images/prp\\', '2018-07-05 19:03:21', 3, 0, 1001),
(2, '15308174010.jpg', 'Resources/images/prp\\', '2018-07-05 19:03:21', 4, 0, 1001),
(3, '15308444270.png', 'Resources/images/prp\\', '2018-07-06 02:33:48', 7, 0, 1001),
(4, '15318277830.png', 'Resources/images/prp\\', '2018-07-17 11:43:03', 144, 0, 1001);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ticket`
-- (See below for the actual view)
--
CREATE TABLE `view_ticket` (
`ticket_id` mediumint(11) unsigned
,`ticket_group` mediumint(11) unsigned
,`group_week` tinyint(1)
,`status` tinyint(1)
,`category_id` smallint(5) unsigned
,`category_name` text
,`point` decimal(3,0) unsigned
,`store` smallint(3)
,`store_name` varchar(30)
,`default_point` smallint(3) unsigned
,`creator` smallint(5) unsigned
,`creator_name` varchar(50)
,`prp_date` date
,`actions` int(11)
,`action_name` varchar(50)
,`submit_date` date
,`last_update` datetime
,`finding_images` text
,`resolved_images` text
,`resolved_date` date
,`resolved_solution` text
,`area_id` smallint(5)
,`area` varchar(50)
,`notes` text
,`hasil_pemeriksaan` tinyint(1)
,`condition_id` int(11)
,`condition_name` varchar(100)
,`submit_image` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `view_ticket`
--
DROP TABLE IF EXISTS `view_ticket`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ticket`  AS  select `ticket_main`.`ticket_id` AS `ticket_id`,`ticket_main`.`ticket_group` AS `ticket_group`,`ticket_group`.`group_week` AS `group_week`,`ticket_main`.`status` AS `status`,`ticket_main`.`category` AS `category_id`,`ticket_category`.`category_name` AS `category_name`,`ticket_category`.`point_deduction` AS `point`,`ticket_main`.`store` AS `store`,`store`.`store_name` AS `store_name`,`store`.`default_point` AS `default_point`,`creator`.`user_id` AS `creator`,`creator`.`display_name` AS `creator_name`,`ticket_main`.`prp_date` AS `prp_date`,`ticket_main`.`actions` AS `actions`,`ticket_actions`.`action_name` AS `action_name`,`ticket_main`.`submit_date` AS `submit_date`,`ticket_main`.`last_update` AS `last_update`,`ticket_main`.`finding_images` AS `finding_images`,`ticket_main`.`resolved_images` AS `resolved_images`,`ticket_main`.`resolved_date` AS `resolved_date`,`ticket_main`.`resolved_solution` AS `resolved_solution`,`store_area`.`area_id` AS `area_id`,`store_area`.`area_name` AS `area`,`ticket_main`.`notes` AS `notes`,`ticket_main`.`hasil_pemeriksaan` AS `hasil_pemeriksaan`,`ticket_main`.`condition_id` AS `condition_id`,`ticket_condition`.`condition_name` AS `condition_name`,`si`.`upload_name` AS `submit_image` from ((((((((`ticket_main` left join `ticket_category` on((`ticket_main`.`category` = `ticket_category`.`category_id`))) left join `store_area` on((`store_area`.`area_id` = `ticket_category`.`area_id`))) left join `sushitei_ticketdesk`.`users` `creator` on((`ticket_main`.`ticket_creator` = `creator`.`user_id`))) left join `store` on((`ticket_main`.`store` = `store`.`store_id`))) left join `ticket_upload` `si` on((`si`.`ticket_id` = `ticket_main`.`ticket_id`))) left join `ticket_group` on((`ticket_group`.`group_id` = `ticket_main`.`ticket_id`))) left join `ticket_actions` on((`ticket_main`.`actions` = `ticket_actions`.`action_id`))) left join `ticket_condition` on((`ticket_main`.`condition_id` = `ticket_condition`.`condition_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area_category_link`
--
ALTER TABLE `area_category_link`
  ADD KEY `link_id` (`link_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD KEY `store_id` (`store_id`);

--
-- Indexes for table `store_area`
--
ALTER TABLE `store_area`
  ADD KEY `area_id` (`area_id`);

--
-- Indexes for table `ticket_actions`
--
ALTER TABLE `ticket_actions`
  ADD PRIMARY KEY (`action_id`);

--
-- Indexes for table `ticket_category`
--
ALTER TABLE `ticket_category`
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `ticket_condition`
--
ALTER TABLE `ticket_condition`
  ADD PRIMARY KEY (`condition_id`);

--
-- Indexes for table `ticket_email`
--
ALTER TABLE `ticket_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `ticket_group`
--
ALTER TABLE `ticket_group`
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `ticket_log`
--
ALTER TABLE `ticket_log`
  ADD KEY `log_id` (`log_id`);

--
-- Indexes for table `ticket_main`
--
ALTER TABLE `ticket_main`
  ADD KEY `ticket_id` (`ticket_id`),
  ADD KEY `ticket_group` (`ticket_group`);

--
-- Indexes for table `ticket_pemeriksaan`
--
ALTER TABLE `ticket_pemeriksaan`
  ADD PRIMARY KEY (`id_pemeriksaan`);

--
-- Indexes for table `ticket_priority`
--
ALTER TABLE `ticket_priority`
  ADD KEY `priority_id` (`priority_id`);

--
-- Indexes for table `ticket_recommendation`
--
ALTER TABLE `ticket_recommendation`
  ADD PRIMARY KEY (`recommendation_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `ticket_status`
--
ALTER TABLE `ticket_status`
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `ticket_type`
--
ALTER TABLE `ticket_type`
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `ticket_upload`
--
ALTER TABLE `ticket_upload`
  ADD KEY `upload_id` (`upload_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area_category_link`
--
ALTER TABLE `area_category_link`
  MODIFY `link_id` mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `store_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `store_area`
--
ALTER TABLE `store_area`
  MODIFY `area_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `ticket_actions`
--
ALTER TABLE `ticket_actions`
  MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ticket_category`
--
ALTER TABLE `ticket_category`
  MODIFY `category_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=228;

--
-- AUTO_INCREMENT for table `ticket_condition`
--
ALTER TABLE `ticket_condition`
  MODIFY `condition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `ticket_email`
--
ALTER TABLE `ticket_email`
  MODIFY `email_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ticket_group`
--
ALTER TABLE `ticket_group`
  MODIFY `group_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `ticket_log`
--
ALTER TABLE `ticket_log`
  MODIFY `log_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_main`
--
ALTER TABLE `ticket_main`
  MODIFY `ticket_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `ticket_pemeriksaan`
--
ALTER TABLE `ticket_pemeriksaan`
  MODIFY `id_pemeriksaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ticket_priority`
--
ALTER TABLE `ticket_priority`
  MODIFY `priority_id` smallint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticket_recommendation`
--
ALTER TABLE `ticket_recommendation`
  MODIFY `recommendation_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket_status`
--
ALTER TABLE `ticket_status`
  MODIFY `status_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ticket_type`
--
ALTER TABLE `ticket_type`
  MODIFY `type_id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ticket_upload`
--
ALTER TABLE `ticket_upload`
  MODIFY `upload_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ticket_main`
--
ALTER TABLE `ticket_main`
  ADD CONSTRAINT `ticket_main_ibfk_1` FOREIGN KEY (`ticket_group`) REFERENCES `ticket_group` (`group_id`) ON DELETE CASCADE;

--
-- Constraints for table `ticket_recommendation`
--
ALTER TABLE `ticket_recommendation`
  ADD CONSTRAINT `ticket_recommendation_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `ticket_group` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
