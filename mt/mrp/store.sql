-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Sep 2018 pada 09.59
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sushitei_prp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `store`
--

CREATE TABLE `store` (
  `store_id` smallint(3) UNSIGNED NOT NULL,
  `store_name` varchar(30) NOT NULL,
  `store_email` varchar(50) NOT NULL,
  `store_status` tinyint(1) UNSIGNED NOT NULL,
  `location_id` smallint(3) UNSIGNED NOT NULL,
  `default_point` smallint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `store`
--

INSERT INTO `store` (`store_id`, `store_name`, `store_email`, `store_status`, `location_id`, `default_point`) VALUES
(1, 'Plaza Indonesia', '', 1, 1, 100),
(2, 'Plaza Senayan', '', 1, 1, 100),
(3, 'Pondok Indah Mall 2', '', 1, 1, 100),
(4, 'Senayan City', '', 1, 1, 100),
(5, 'Mall Kelapa Gading', '', 1, 1, 100),
(6, 'Emporium Pluit', '', 1, 1, 100),
(7, 'Central Park', 'test.jkt@sushitei.co.id', 1, 1, 100),
(8, 'Gandaria City', '', 1, 1, 100),
(9, 'Flavour Bliss', '', 1, 1, 100),
(10, 'Supermall Karawaci', '', 1, 1, 100),
(11, 'Grand Indonesia', '', 1, 1, 100),
(12, 'Lotte Avenue', '', 1, 1, 100),
(13, 'Kota Kasablanka', '', 1, 1, 100),
(14, 'Summarecon Bekasi', '', 1, 1, 100),
(15, 'Bintaro Xchange', 'it6.jkt@sushitei.co.id', 1, 1, 100),
(16, 'The Breeze', '', 1, 1, 100),
(17, 'Puri Mall', '', 1, 1, 100),
(18, 'Summarecon Serpong', '', 1, 1, 100),
(19, 'Margo City', '', 1, 1, 100),
(20, 'Kemang Village', '', 1, 1, 100),
(100, 'Sushi Kiosk Puri', '', 1, 1, 100),
(103, 'aa test test store', 'it6.jkt@sushitei.co.id', 1, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `store`
--
ALTER TABLE `store`
  ADD KEY `store_id` (`store_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `store`
--
ALTER TABLE `store`
  MODIFY `store_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
