-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Agu 2018 pada 08.46
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `area_category_link`
--

CREATE TABLE `area_category_link` (
  `link_id` mediumint(6) UNSIGNED NOT NULL,
  `category` smallint(5) UNSIGNED DEFAULT NULL,
  `area` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `area_category_link`
--

INSERT INTO `area_category_link` (`link_id`, `category`, `area`) VALUES
(1, 11, 1),
(2, 12, 1),
(3, 14, 1),
(4, 15, 1),
(5, 16, 2),
(6, 17, 2),
(7, 18, 2),
(8, 20, 3),
(9, 21, 3),
(10, 22, 3),
(11, 23, 3),
(12, 24, 3),
(13, 25, 3),
(14, 26, 3),
(15, 27, 3),
(16, 28, 3),
(17, 29, 3),
(18, 30, 3),
(19, 31, 3),
(20, 32, 3),
(21, 33, 3),
(22, 34, 3),
(23, 35, 3),
(24, 36, 3),
(25, 37, 3),
(26, 38, 3),
(27, 39, 3),
(28, 40, 3),
(29, 41, 3),
(30, 42, 3),
(31, 43, 3),
(32, 44, 4),
(33, 46, 4),
(34, 47, 4),
(35, 48, 4),
(36, 49, 4),
(37, 50, 4),
(38, 51, 4),
(39, 52, 4),
(40, 53, 4),
(41, 54, 5),
(42, 55, 5),
(43, 56, 5),
(44, 57, 5),
(45, 58, 5),
(46, 59, 6),
(47, 60, 6),
(48, 61, 6),
(49, 62, 6),
(50, 63, 6),
(51, 64, 6),
(52, 65, 6),
(53, 66, 6),
(54, 67, 6),
(55, 68, 6),
(56, 69, 6),
(57, 70, 6),
(58, 71, 6),
(59, 72, 6),
(60, 73, 7),
(61, 74, 7),
(62, 75, 7),
(63, 76, 7),
(64, 77, 7),
(65, 78, 7),
(66, 79, 7),
(67, 80, 7),
(68, 81, 7),
(69, 82, 8),
(70, 83, 8),
(71, 84, 8),
(72, 85, 8),
(73, 86, 8),
(74, 87, 8),
(75, 88, 8),
(76, 89, 8),
(77, 90, 8),
(78, 91, 8),
(79, 92, 8),
(80, 93, 8),
(81, 94, 9),
(82, 95, 9),
(83, 96, 9),
(84, 97, 9),
(85, 98, 9),
(86, 99, 9),
(87, 100, 10),
(88, 101, 10),
(89, 102, 10),
(90, 103, 10),
(91, 104, 10),
(92, 105, 10),
(93, 106, 10),
(94, 107, 10),
(95, 108, 10),
(96, 109, 10),
(97, 110, 10),
(98, 111, 1),
(99, 112, 1),
(100, 113, 1),
(101, 114, 1),
(102, 115, 1),
(103, 116, 1),
(104, 117, 1),
(105, 118, 1),
(106, 119, 1),
(107, 120, 1),
(108, 121, 1),
(109, 122, 1),
(110, 123, 1),
(111, 124, 1),
(112, 125, 1),
(113, 126, 1),
(114, 127, 1),
(115, 128, 1),
(116, 129, 1),
(117, 130, 1),
(118, 131, 1),
(119, 132, 1),
(120, 133, 1),
(121, 134, 1),
(122, 135, 1),
(123, 136, 1),
(124, 137, 1),
(125, 138, 1),
(126, 139, 1),
(127, 140, 1),
(128, 142, 12),
(129, 143, 12),
(130, 144, 12),
(131, 145, 12),
(132, 146, 12),
(133, 147, 12),
(134, 148, 12),
(135, 149, 12),
(136, 150, 12),
(137, 151, 12),
(138, 152, 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `location`
--

CREATE TABLE `location` (
  `location_id` smallint(3) UNSIGNED NOT NULL,
  `location_name` varchar(30) NOT NULL,
  `location_status` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `location`
--

INSERT INTO `location` (`location_id`, `location_name`, `location_status`) VALUES
(1, 'Jakarta', 1),
(2, 'Surabaya', 1),
(3, 'Makasar', 1),
(4, 'Palembang', 1),
(5, 'Bali', 1),
(6, 'Bandung', 1),
(7, 'Jogja', 1),
(8, 'Medan', 1),
(9, 'Pekan Baru', 1),
(10, 'Batam', 1),
(11, 'Bengkulu', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `store`
--

CREATE TABLE `store` (
  `store_id` smallint(3) UNSIGNED NOT NULL,
  `store_name` varchar(30) NOT NULL,
  `store_status` tinyint(1) UNSIGNED NOT NULL,
  `location_id` smallint(3) UNSIGNED NOT NULL,
  `default_point` smallint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `store`
--

INSERT INTO `store` (`store_id`, `store_name`, `store_status`, `location_id`, `default_point`) VALUES
(1, 'Plaza Indonesia', 1, 1, 100),
(2, 'Plaza Senayan', 1, 1, 100),
(3, 'Pondok Indah Mall 2', 1, 1, 100),
(4, 'Senayan City', 1, 1, 100),
(5, 'Mall Kelapa Gading', 1, 1, 100),
(6, 'Emporium Pluit', 1, 1, 100),
(7, 'Central Park', 1, 1, 100),
(8, 'Gandaria City', 1, 1, 100),
(9, 'Flavour Bliss', 1, 1, 100),
(10, 'Supermall Karawaci', 1, 1, 100),
(11, 'Grand Indonesia', 1, 1, 100),
(12, 'Lotte Avenue', 1, 1, 100),
(13, 'Kota Kasablanka', 1, 1, 100),
(14, 'Summarecon Bekasi', 1, 1, 100),
(15, 'Bintaro Xchange', 1, 1, 100),
(16, 'The Breeze', 1, 1, 100),
(17, 'Puri Mall', 1, 1, 100),
(18, 'Summarecon Serpong', 1, 1, 100),
(19, 'Margo City', 1, 1, 100),
(20, 'Kemang Village', 1, 1, 100),
(100, 'Sushi Kiosk Puri', 1, 1, 100),
(101, 'a Test Outlet', 1, 2, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `store_area`
--

CREATE TABLE `store_area` (
  `area_id` smallint(5) NOT NULL,
  `area_name` varchar(50) NOT NULL,
  `area_desc` text NOT NULL,
  `area_status` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `store_area`
--

INSERT INTO `store_area` (`area_id`, `area_name`, `area_desc`, `area_status`) VALUES
(1, 'General', 'Area selain yang disebutkan', 1),
(2, 'Eksterior', 'Area service', 1),
(3, 'Dine-In', 'Area Open Kitchen', 1),
(4, 'Janitor dan Loker', 'Area Back Kitchen', 1),
(5, 'Toilet', 'Area Gudang', 1),
(6, 'Hot & Cold Kitchen', '', 1),
(7, 'Back Kitchen', '', 1),
(8, 'Penerimaan & Penyimpanan Barang', '', 1),
(9, 'Personal Hygiene & Sanitasi', '', 1),
(10, 'Infrastruktur dan Utiliti', '', 1),
(12, 'Bar', '', 1),
(16, 'test', 'test update', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_category`
--

CREATE TABLE `ticket_category` (
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `category_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `area_id` smallint(5) UNSIGNED DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `point_deduction` decimal(3,0) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ticket_category`
--

INSERT INTO `ticket_category` (`category_id`, `category_name`, `area_id`, `description`, `point_deduction`, `deleted`) VALUES
(11, 'Sink untuk cuci tangan dilengkapi dengan sabun, hand sanitizer, dan tissue dalam dispenser', 1, 'Sink untuk cuci tangan dilengkapi dengan sabun, hand sanitizer, dan tissue dalam dispenser', '2', 0),
(12, 'prosedur pencucian tangan dilakukan secara benar. Pencucian tangan dilakukan sebelum operasional, setelah briefing, masuk area medium sanitasi, memegang tempat sampah, dan kondisi lainnya yang dapat mengkontaminasi)', 1, 'prosedur pencucian tangan dilakukan secara benar. Pencucian tangan dilakukan sebelum operasional, setelah briefing, masuk area medium sanitasi, memegang tempat sampah, dan kondisi lainnya yang dapat mengkontaminasi)', '2', 0),
(14, 'Sink cuci tangan karyawan tidak digunakan untuk aktivitas lain selain mencuci tangan', 1, 'Sink cuci tangan karyawan tidak digunakan untuk aktivitas lain selain mencuci tangan', '1', 0),
(15, 'Penampilan staff sesuai dengan standard seragam dan personal hygiene yang ditetapkan', 1, 'Penampilan staff sesuai dengan standard seragam dan personal hygiene yang ditetapkan', '2', 0),
(16, 'Jalan di depan outlet (public area) dan entrance masuk ke outlet bersih dan terawat', 2, 'Jalan di depan outlet (public area) dan entrance masuk ke outlet bersih dan terawat', '1', 0),
(17, 'Panel, kaca, hiasan dan kursi waiting list eksterior bersih dan terawat', 2, 'Panel, kaca, hiasan dan kursi waiting list eksterior bersih dan terawat', '1', 0),
(18, 'Meja greeter dalam kondisi bersih, rapi dan terawat', 2, 'Meja greeter dalam kondisi bersih, rapi dan terawat', '1', 0),
(20, 'Meja, sofa dan kursi dalam kondisi bersih (termasuk bagian bawah meja, sela-sela syscall, sela-sela sofa dan kursi, penyimpanan bawah sofa )', 3, 'Meja, sofa dan kursi dalam kondisi bersih (termasuk bagian bawah meja, sela-sela syscall, sela-sela sofa dan kursi, penyimpanan bawah sofa )', '1', 0),
(21, 'Lampu menyala dalam kondisi baik, speaker berfungsi dengan baik', 3, 'Lampu menyala dalam kondisi baik, speaker berfungsi dengan baik', '1', 0),
(22, 'Meja, sofa dan kursi dalam kondisi baik dan terawat (tidak gompal, goyang, retak, terkelupas)', 3, 'Meja, sofa dan kursi dalam kondisi baik dan terawat (tidak gompal, goyang, retak, terkelupas)', '1', 0),
(23, 'Lantai, dinding, langit-langit, difuser ac bersih dan terawat (tidak retak, pecah dan gompal)', 3, 'Lantai, dinding, langit-langit, difuser ac bersih dan terawat (tidak retak, pecah dan gompal)', '1', 0),
(24, 'Kaca, partisi, desain interior dalam keadaan bersih dan terawat', 3, 'Kaca, partisi, desain interior dalam keadaan bersih dan terawat', '1', 0),
(25, 'Buku menu dalam keadaan bersih(tidak sobek,tidak kotor, berbau, dan lengket)', 3, 'Buku menu dalam keadaan bersih(tidak sobek,tidak kotor, berbau, dan lengket)', '1', 0),
(26, 'Setting table dalam keadaan bersih dan lengkap (side plate, saucer, chopstick, tissue, toothpick)', 3, 'Setting table dalam keadaan bersih dan lengkap (side plate, saucer, chopstick, tissue, toothpick)', '1', 0),
(27, 'Tent card marketing dalam kondisi bersih dan terawat (tidak pecah)', 3, 'Tent card marketing dalam kondisi bersih dan terawat (tidak pecah)', '1', 0),
(28, 'Warmer chawan mushi, Teko, selang teko, gagang teko  dan boiler dalam keadaan bersih dan terawat', 3, 'Warmer chawan mushi, Teko, selang teko, gagang teko  dan boiler dalam keadaan bersih dan terawat', '1', 0),
(29, 'Peralatan makan yang akan digunakan kondisi bersih dan layak pakai (sendok, garpu, duckspoon, chopstick, laddle, sendok nasi, mangkok gohan, mangkok soup, gelas ocha)', 3, 'Peralatan makan yang akan digunakan kondisi bersih dan layak pakai (sendok, garpu, duckspoon, chopstick, laddle, sendok nasi, mangkok gohan, mangkok soup, gelas ocha)', '1', 0),
(30, 'Peralatan kerja (capitan take away, bill cover, tray, trolley bush boy, tempat piring kotor,dll) dalam kondisi bersih dan layak pakai', 3, 'Peralatan kerja (capitan take away, bill cover, tray, trolley bush boy, tempat piring kotor,dll) dalam kondisi bersih dan layak pakai', '1', 0),
(31, 'Station, drawer, dan lemari penyimpanan dalam kondisi bersih dan terawat (termasuk area sekitar station)', 3, 'Station, drawer, dan lemari penyimpanan dalam kondisi bersih dan terawat (termasuk area sekitar station)', '1', 0),
(32, 'Conveyor belt (keadaan mati) dalam keadaan bersih (tidak ada ceceran makanan dan debu di atas dan bagian bawah, termasuk stainless sekitar conveyor)', 3, 'Conveyor belt (keadaan mati) dalam keadaan bersih (tidak ada ceceran makanan dan debu di atas dan bagian bawah, termasuk stainless sekitar conveyor)', '1', 0),
(33, 'Lampu Flies Catcher, mosquito trap dalam kondisi menyala. ', 3, 'Lampu Flies Catcher, mosquito trap dalam kondisi menyala. ', '1', 0),
(34, 'Tempat penyimpanan sauce, mayonaise, shoyu, sweet sauce terdapat label tanggal pindah botol', 3, 'Tempat penyimpanan sauce, mayonaise, shoyu, sweet sauce terdapat label tanggal pindah botol', '2', 0),
(35, 'Form pencatatan suhu ruang terisi lengkap dan alat berfungsi baik', 3, 'Form pencatatan suhu ruang terisi lengkap dan alat berfungsi baik', '1', 0),
(36, 'Produk khusus take away diberi label best consumed 2 hours. Sampling implementasi (catat nama karyawan dan jam periksa)', 3, 'Produk khusus take away diberi label best consumed 2 hours. Sampling implementasi (catat nama karyawan dan jam periksa)', '2', 0),
(37, 'Lemari penyimpanan perlengkapan teknisi dalam kondisi rapi dan bersih', 3, 'Lemari penyimpanan perlengkapan teknisi dalam kondisi rapi dan bersih', '1', 0),
(38, 'Tempat sampah dalam kondisi tertutup, pijakan berfungsi dengan baik, bagian dalam terdapat plastik sampah', 3, 'Tempat sampah dalam kondisi tertutup, pijakan berfungsi dengan baik, bagian dalam terdapat plastik sampah', '1', 0),
(39, 'Dust mats yang menuju ke area dinning, open kitchen atau back kitchen dalam kondisi bersih, dan kering  ', 3, 'Dust mats yang menuju ke area dinning, open kitchen atau back kitchen dalam kondisi bersih, dan kering  ', '1', 0),
(40, 'Penyimpanan  bahan makanan di station / dilemari area service hanya untuk kebutuhan operasional 1 hari kecuali dilakukan monitoring dan pengendalian suhu dan kelembaban', 3, 'Penyimpanan  bahan makanan di station / dilemari area service hanya untuk kebutuhan operasional 1 hari kecuali dilakukan monitoring dan pengendalian suhu dan kelembaban', '1', 0),
(41, 'penyimpanan bahan baku service disimpan dalam tempat tertutup(contoh: sauce tomat, gari dan wasabi dalam kemasan tertutup di chiller,tea pack disimpan dalam tempat tertutup)', 3, 'penyimpanan bahan baku service disimpan dalam tempat tertutup(contoh: sauce tomat, gari dan wasabi dalam kemasan tertutup di chiller,tea pack disimpan dalam tempat tertutup)', '3', 0),
(42, 'pelabelan kondimen sweet dan shoyu dilaksanakan dengan tepat(hijau=1 hari, kuning=2 hari, merah=3 hari)', 3, 'pelabelan kondimen sweet dan shoyu dilaksanakan dengan tepat(hijau=1 hari, kuning=2 hari, merah=3 hari)', '2', 0),
(43, 'clear up bushboy dilkaukan dengan tepat(meja dalam keadaan bersih tidak berminyak, lantai bersih, sampah tidak dimasukkan ke dalam alat saji)', 3, 'clear up bushboy dilkaukan dengan tepat(meja dalam keadaan bersih tidak berminyak, lantai bersih, sampah tidak dimasukkan ke dalam alat saji)', '1', 0),
(44, 'Lemari janitor dalam keadaan rapi dan bersih', 4, 'Lemari janitor dalam keadaan rapi dan bersih', '1', 0),
(46, 'Bahan kimia yang disimpan dalam wadah tertutup dan diberi label yang jelas', 4, 'Bahan kimia yang disimpan dalam wadah tertutup dan diberi label yang jelas', '2', 0),
(47, 'Peralatan pembuatan bahan kimia tersedia lengkap seperti kaca mata, sarung tangan karet, cangkir takar dan sendok takar.', 4, 'Peralatan pembuatan bahan kimia tersedia lengkap seperti kaca mata, sarung tangan karet, cangkir takar dan sendok takar.', '1', 0),
(48, 'Bahan kimia yang disimpan di dalam janitor adalah bahan kimia yang memiliki MSDS lengkap dan tertera di List Bahan Kimia yang digunakan di outlet (tertempel di lemari/dinding janitor) [NOTE: kecuali bebek biru dan pengharum toilet di FS]', 4, 'Bahan kimia yang disimpan di dalam janitor adalah bahan kimia yang memiliki MSDS lengkap dan tertera di List Bahan Kimia yang digunakan di outlet (tertempel di lemari/dinding janitor) [NOTE: kecuali bebek biru dan pengharum toilet di FS]', '1', 0),
(49, 'Bush boy  dan steward mengetahui takaran pengenceran bahan kimia untuk yang dicampur secara manual (suma titan, suma calc, oven grill, demonpine) (catat nama karyawan dan lama kerja)', 4, 'Bush boy  dan steward mengetahui takaran pengenceran bahan kimia untuk yang dicampur secara manual (suma titan, suma calc, oven grill, demonpine) (catat nama karyawan dan lama kerja)', '1', 0),
(50, 'Peralatan kebersihan dalam kondisi layak pakai', 4, 'Peralatan kebersihan dalam kondisi layak pakai', '1', 0),
(51, 'Memiliki jadwal pembersihan locker setiap harinya', 4, 'Memiliki jadwal pembersihan locker setiap harinya', '1', 0),
(52, 'Ruangan locker kondisi bersih dan terawat, sampah dibuang secara periodik', 4, 'Ruangan locker kondisi bersih dan terawat, sampah dibuang secara periodik', '1', 0),
(53, 'Tas tersimpan didalam locker/megabox dan dapat ditutup dengan rapi/terkunci', 4, 'RTas tersimpan didalam locker/megabox dan dapat ditutup dengan rapi/terkunci', '1', 0),
(54, 'Toilet dalam keadaan bersih, kering dan tidak berbau', 5, 'Toilet dalam keadaan bersih, kering dan tidak berbau', '1', 0),
(55, 'Tissue dan handsoap tersedia, tempat sampah dalam kondisi layak dan terawat', 5, 'Tissue dan handsoap tersedia, tempat sampah dalam kondisi layak dan terawat', '1', 0),
(56, 'Hand dryer berfungsi normal', 5, 'Hand dryer berfungsi normal', '1', 0),
(57, 'Bagian dalam closet/urinair tidak terdapat kotoran ataupun kerak air', 5, 'Bagian dalam closet/urinair tidak terdapat kotoran ataupun kerak air', '1', 0),
(58, 'Checklist kebersihan tersedia dan terisi lengkap', 5, 'Checklist kebersihan tersedia dan terisi lengkap', '1', 0),
(59, 'Semua produk dalam kondisi baik dan layak pakai', 6, 'Semua produk dalam kondisi baik dan layak pakai', '1', 0),
(60, 'Semua kemungkinan adanya kontaminasi silang harus dihindari', 6, 'Semua kemungkinan adanya kontaminasi silang harus dihindari', '1', 0),
(61, 'Aquarium bersih dan tidak berlumut, kadar garam sesuai dengan standard', 6, 'Aquarium bersih dan tidak berlumut, kadar garam sesuai dengan standard', '1', 0),
(62, 'Timbangan/peralatan ukur yang digunakan dalam kondisi bersih, terawat baik dan tidak rusak. Terdapat label QA dan label kalibrasi/verifikasi', 6, 'Timbangan/peralatan ukur yang digunakan dalam kondisi bersih, terawat baik dan tidak rusak. Terdapat label QA dan label kalibrasi/verifikasi', '1', 0),
(63, 'Cover plate bersih, tidak rusak/retak dan dilengkapi dengan stiker warna', 6, 'Cover plate bersih, tidak rusak/retak dan dilengkapi dengan stiker warna', '1', 0),
(64, 'Color plate bersih, bagian bawah tidak berkerak atau bercak hitam, tidak gompal atau retak', 6, 'Color plate bersih, bagian bawah tidak berkerak atau bercak hitam, tidak gompal atau retak', '1', 0),
(65, 'Foil dynamite roll dalam kondisi bersih, bentuk masih utuh dan layak pakai', 6, 'Foil dynamite roll dalam kondisi bersih, bentuk masih utuh dan layak pakai', '1', 0),
(66, 'Nabe, donburi , yanagawa plate dan kompor sukiyaki tidak berkarat, kondisi kering dan bersih', 6, 'Nabe, donburi , yanagawa plate dan kompor sukiyaki tidak berkarat, kondisi kering dan bersih', '1', 0),
(67, 'Tidak menyimpan bahan baku atau bahan makanan di atas rak interior. NOTE: maksimal jam 10.30', 6, 'Tidak menyimpan bahan baku atau bahan makanan di atas rak interior. NOTE: maksimal jam 10.30', '3', 0),
(68, 'Pencatatan penaikan dan penurunan color plate terdokumentasi dengan lengkap dan benar. Sampling penurunan color plate dilakukan tepat waktu. Tidak ada produk yang berada di conveyor belt lebih dari 2 jam (termasuk edamame dan chinmi) (catat jam periksa)', 6, 'Pencatatan penaikan dan penurunan color plate terdokumentasi dengan lengkap dan benar. Sampling penurunan color plate dilakukan tepat waktu. Tidak ada produk yang berada di conveyor belt lebih dari 2 jam (termasuk edamame dan chinmi) (catat jam periksa)', '3', 0),
(69, 'Sampling cara pemeriksaan suhu internal produk dilakukan dengan benar (catat jam periksa). Melakukan sanitasi sebelum mengukur suhu', 6, 'Sampling cara pemeriksaan suhu internal produk dilakukan dengan benar (catat jam periksa). Melakukan sanitasi sebelum mengukur suhu', '1', 0),
(70, 'Form pencabutan tulang, suhu internal produk, dan checklist kebersihan tersedia dan terisi lengkap ', 6, 'Form pencabutan tulang, suhu internal produk, dan checklist kebersihan tersedia dan terisi lengkap ', '1', 0),
(71, 'Staff melakukan sanitasi testo sebelum digunakan untuk pengecekan suhu internal produk ', 6, 'Staff melakukan sanitasi testo sebelum digunakan untuk pengecekan suhu internal produk ', '1', 0),
(72, 'Rotasi pergantian manaita dan pergantian makisu sesuai waktu yang telah ditentukan. Pengunaan manaita hijau untuk pemotonga produk Ready to Eat di Hot kitchen', 6, 'Rotasi pergantian manaita dan pergantian makisu sesuai waktu yang telah ditentukan. Pengunaan manaita hijau untuk pemotonga produk Ready to Eat di Hot kitchen', '2', 0),
(73, 'Semua peralatan yang digunakan dalam kondisi bersih, terawat dan berfungsi baik ', 7, 'Semua peralatan yang digunakan dalam kondisi bersih, terawat dan berfungsi baik ', '1', 0),
(74, 'Pencucian peralatan dengan diswaher dilakukan dengan tepat(Bak sink terakhir sebelum ke dishwasher terisi dengan multiporpose soap, penyusunan piring pada rak tidak menumpuk, penggunaan rak cutleries untu peralatan service seperti chopstick, sendok, garpu, dan peralatn kecil lainnya)', 7, 'Pencucian peralatan dengan diswaher dilakukan dengan tepat(Bak sink terakhir sebelum ke dishwasher terisi dengan multiporpose soap, penyusunan piring pada rak tidak menumpuk, penggunaan rak cutleries untu peralatan service seperti chopstick, sendok, garpu, dan peralatn kecil lainnya)', '1', 0),
(75, 'Air dishwasher diganti sesuai dengan jadwal yang ditetapkan dan di catat di checklist pergantian dishwasher', 7, 'Air dishwasher diganti sesuai dengan jadwal yang ditetapkan dan di catat di checklist pergantian dishwasher', '1', 0),
(76, 'Bak/ember piring  kondisi bersih dari kotoran yang mengerak/lumut', 7, 'Bak/ember piring  kondisi bersih dari kotoran yang mengerak/lumut', '1', 0),
(77, 'Peralatan atau kitchen ware yang diletakkan pada rak bersih kondisi bersih, layak pakai, dan penyimpanannya sesuai( wajan tamago, plate gyu misoyaki dan cap tamago dilapisi minyak untuk penyimpanannya)', 7, 'Peralatan atau kitchen ware yang diletakkan pada rak bersih kondisi bersih, layak pakai, dan penyimpanannya sesuai( wajan tamago, plate gyu misoyaki dan cap tamago dilapisi minyak untuk penyimpanannya)', '1', 0),
(78, 'Suhu gun sprayer steward mencapai 43oC', 7, 'Suhu gun sprayer steward mencapai 43oC', '1', 0),
(79, 'Melakukan sanitasi dengan larutan sanitasi pada pencucian panci atau peralatan yang tidak masuk ke dishwasher ', 7, 'Melakukan sanitasi dengan larutan sanitasi pada pencucian panci atau peralatan yang tidak masuk ke dishwasher ', '2', 0),
(80, 'Sampling cara pencabutan tulang dilakukan di back kitchen dan verifikasi pencabutan tulang dilakukan oleh CoD. (catat jam periksa)', 7, 'Sampling cara pencabutan tulang dilakukan di back kitchen dan verifikasi pencabutan tulang dilakukan oleh CoD. (catat jam periksa)', '1', 0),
(81, 'proses pencucian prepare sayuran dilakuakan di back kitchen(termasuk prepare avocado) dan diproses sesuai ketentuan FnB', 7, 'proses pencucian prepare sayuran dilakuakan di back kitchen(termasuk prepare avocado) dan diproses sesuai ketentuan FnB', '2', 0),
(82, 'Barang yang datang sesuai dengan standard', 8, 'Barang yang datang sesuai dengan standard', '1', 0),
(83, 'Kemasan produk diberi label tanggal kedatangan dan label exp date (label Exp date dikirimkan dari ICW. Jika tidak ada label atau jumlah kurang maka dimunculkan PQNC)', 8, 'Kemasan produk diberi label tanggal kedatangan dan label exp date (label Exp date dikirimkan dari ICW. Jika tidak ada label atau jumlah kurang maka dimunculkan PQNC)', '1', 0),
(84, 'Temperatur produk dan tanggal kadaluarsa/produksi tertulis jelas di copy faktur/PO/TO dan pada form penerimaan barang', 8, 'Temperatur produk dan tanggal kadaluarsa/produksi tertulis jelas di copy faktur/PO/TO dan pada form penerimaan barang', '3', 0),
(85, 'Form suhu dan RH terisi lengkap dan benar', 8, 'Form suhu dan RH terisi lengkap dan benar', '1', 0),
(86, 'Penyimpanan barang tidak menempel dinding dan tidak di atas lantai', 8, 'Penyimpanan barang tidak menempel dinding dan tidak di atas lantai', '3', 0),
(87, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan layak pakai', 8, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan layak pakai', '1', 0),
(88, 'Penyimpanan bahan kimia terpisah dari bahan makanan', 8, 'Penyimpanan bahan kimia terpisah dari bahan makanan', '3', 0),
(89, 'Suhu gudang penyimpanan bahan makanan sesuai standart (maksimal 28 deg C=ruangan AC dan 33 deg C=non AC ', 8, 'Suhu gudang penyimpanan bahan makanan sesuai standart (maksimal 28 deg C=ruangan AC dan 33 deg C=non AC ', '1', 0),
(90, 'Tidak meletakkan produk langsung di atas lantai pada saat proses penerimaan', 8, 'Tidak meletakkan produk langsung di atas lantai pada saat proses penerimaan', '1', 0),
(91, 'Suplier ataupun logistik Sushi Tei  yang membawakan barang menuju gudang dengan terlebih dahulu melintasi area produksi/kitchen menggunakan hair net/penutup kepala', 8, 'Suplier ataupun logistik Sushi Tei  yang membawakan barang menuju gudang dengan terlebih dahulu melintasi area produksi/kitchen menggunakan hair net/penutup kepala', '2', 0),
(92, 'Sampling cara pemeriksaan suhu internal salmon atau produk beku lainnya (catat nama karyawan)', 8, 'Sampling cara pemeriksaan suhu internal salmon atau produk beku lainnya (catat nama karyawan)', '3', 0),
(93, 'Penyimpanan bahan makanan di gudang dry, dalam walk in chiller dan walk in freezer tinggi minimal 15 cm ', 8, 'Penyimpanan bahan makanan di gudang dry, dalam walk in chiller dan walk in freezer tinggi minimal 15 cm ', '1', 0),
(94, 'Staff yang memiliki luka harus menutup luka dengan perban dan menggunakan hand gloves', 9, 'Staff yang memiliki luka harus menutup luka dengan perban dan menggunakan hand gloves', '2', 0),
(95, 'Telepon seluler tidak dibawa selama jam tugas berlangsung (tidak di dalam saku) kecuali untuk karyawan (MOD, COD, Stock Keeper, Ass. Stock Keeper) yang memang pekerjaannya membutuhkan HP untuk berkoordinasi dengan pihak lain atau terkait dengan pekerjaannya. NOTE: untuk Bar simpan di kasir, untuk kitchen di rak admin open kitchen/back kitchen dalam kondisi tertutup', 9, 'Telepon seluler tidak dibawa selama jam tugas berlangsung (tidak di dalam saku) kecuali untuk karyawan (MOD, COD, Stock Keeper, Ass. Stock Keeper) yang memang pekerjaannya membutuhkan HP untuk berkoordinasi dengan pihak lain atau terkait dengan pekerjaannya. NOTE: untuk Bar simpan di kasir, untuk kitchen di rak admin open kitchen/back kitchen dalam kondisi tertutup', '2', 0),
(96, 'Fasilitas sanitasi karyawan dilengkapi dengan visual cara mencuci tangan', 9, 'Fasilitas sanitasi karyawan dilengkapi dengan visual cara mencuci tangan', '1', 0),
(97, 'Tidak ada tanda-tanda adanya tikus dan  kecoa yang hidup', 9, 'Tidak ada tanda-tanda adanya tikus dan  kecoa yang hidup', '1', 0),
(98, 'Pest control selalu dilakukan dan jadwal serta laporan kerja harus didokumentasikan', 9, 'Pest control selalu dilakukan dan jadwal serta laporan kerja harus didokumentasikan', '1', 0),
(99, 'Peralatan pest control (Misal AMS, Lampu Flycatcher, dan Mosquito trap) harus selalu bersih, terawat (tidak basah, tidak pecah), berfungsi', 9, 'Peralatan pest control (Misal AMS, Lampu Flycatcher, dan Mosquito trap) harus selalu bersih, terawat (tidak basah, tidak pecah), berfungsi', '1', 0),
(100, 'Semua pintu berfungsi dengan baik, dapat menutup sempurna ', 10, 'Semua pintu berfungsi dengan baik, dapat menutup sempurna ', '1', 0),
(101, 'Electrical panels, saklar, kabel listrik, stop kontak dalam keadaan bersih, terawat, berfungsi baik dan aman', 10, 'Electrical panels, saklar, kabel listrik, stop kontak dalam keadaan bersih, terawat, berfungsi baik dan aman', '1', 0),
(102, 'Stop kontak yang sudah tidak digunakan atau berfungsi ditutup untuk mencegah sarang hama', 10, 'Stop kontak yang sudah tidak digunakan atau berfungsi ditutup untuk mencegah sarang hama', '1', 0),
(103, 'Tidak dijumpai peralatan atau perbaikan sementara yang menggunakan bahan isolasi/plester atau bahan yang bersifat absorbent atau yang mudah menimbulkan infestasi pest', 10, 'Tidak dijumpai peralatan atau perbaikan sementara yang menggunakan bahan isolasi/plester atau bahan yang bersifat absorbent atau yang mudah menimbulkan infestasi pest', '1', 0),
(104, 'Kontainer plastik yang digunakan untuk penyimpanan makanan dan minuman terbuat dari bahan food grade dan tidak beracun ', 10, 'Kontainer plastik yang digunakan untuk penyimpanan makanan dan minuman terbuat dari bahan food grade dan tidak beracun ', '1', 0),
(105, 'Temperatur suhu ruang di sekitar conveyor belt maksimal 28 deg C', 10, 'Temperatur suhu ruang di sekitar conveyor belt maksimal 28 deg C', '1', 0),
(106, 'Penggantian filter air dilakukan berkala dan dapat dibuktikan dengan label di alat dan juga kelengkapan pencatatan di form terkait(Filter air besar dan Filter air kecil)', 10, 'Penggantian filter air dilakukan berkala dan dapat dibuktikan dengan label di alat dan juga kelengkapan pencatatan di form terkait(Filter air besar dan Filter air kecil)', '1', 0),
(107, 'Aktivitas pencucian filter atau backwash dilakukan secara berkala dan dapat dibuktikan dengan kelengkapan pencatatan di form terkait', 10, 'Aktivitas pencucian filter atau backwash dilakukan secara berkala dan dapat dibuktikan dengan kelengkapan pencatatan di form terkait', '1', 0),
(108, 'Lampu UV filter dalam kondisi menyala (di area bar dan di area  back/open kitchen) ', 10, 'Lampu UV filter dalam kondisi menyala (di area bar dan di area  back/open kitchen) ', '3', 0),
(109, 'APAR tidak expired dan tidak di batas merah bawah (lengkap sesuai lay out)', 10, 'APAR tidak expired dan tidak di batas merah bawah (lengkap sesuai lay out)', '1', 0),
(110, 'Air yang digunakan melewati sistem water treatment (kran / valve dalam posisi yang benar, tidak di-bypass)', 10, 'Air yang digunakan melewati sistem water treatment (kran / valve dalam posisi yang benar, tidak di-bypass)', '1', 0),
(111, 'Checklist personel hygiene terisi dengan lengkap dan mencakup untuk karyawan (admin ISA, captain, MoD, Teknisi, Bartender,steward cook,  stockeeper, COD)', 1, 'Checklist personel hygiene terisi dengan lengkap dan mencakup untuk karyawan (admin ISA, captain, MoD, Teknisi, Bartender,steward cook,  stockeeper, COD)', '1', 0),
(112, 'Tidak ada barang pribadi di area proses kerja (termasuk box katering, botol dan gelas minuman). Apabila diperlukan penyimpanan box catring, botol dan gelas minum hanya dilokasi yang ditentukan dan tidak di lokasi kerja/working table.', 1, 'Tidak ada barang pribadi di area proses kerja (termasuk box katering, botol dan gelas minuman). Apabila diperlukan penyimpanan box catring, botol dan gelas minum hanya dilokasi yang ditentukan dan tidak di lokasi kerja/working table.', '2', 0),
(113, 'Produk yang disimpan memiliki label identitas (nama produk, tanggal pembuatan,tanggal pertama kali dibuka kemasan, dll) untuk produk yang tidak habis pada hari dia dibuka/dipindahkan', 1, 'Produk yang disimpan memiliki label identitas (nama produk, tanggal pembuatan,tanggal pertama kali dibuka kemasan, dll) untuk produk yang tidak habis pada hari dia dibuka/dipindahkan', '2', 0),
(114, 'Produk harus disimpan dalam tempat tertutup, terjaga dari kontaminasi. NOTE: kecuali garnish diatas meja ', 1, 'Produk harus disimpan dalam tempat tertutup, terjaga dari kontaminasi. NOTE: kecuali garnish diatas meja ', '3', 0),
(115, 'Produk rusak(termasuk expired, buah dan sayur yang sudah tidak layak pakai, berjamur atau busuk) dipisahkan dan diberi label yang jelas, serta sudah dilaporkan dalam PQNC. NOTE: label tidak wajib warna', 1, 'Produk rusak(termasuk expired, buah dan sayur yang sudah tidak layak pakai, berjamur atau busuk) dipisahkan dan diberi label yang jelas, serta sudah dilaporkan dalam PQNC. NOTE: label tidak wajib warna', '3', 0),
(116, 'Penyimpanan WIP atau ready to eat food terpisah dari bahan makanan mentah/raw food  Pemisahan dapat berupa disimpan dalam kontainer tertutup atau pada rak yang terpisah. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', 1, 'Penyimpanan WIP atau ready to eat food terpisah dari bahan makanan mentah/raw food  Pemisahan dapat berupa disimpan dalam kontainer tertutup atau pada rak yang terpisah. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', '3', 0),
(117, 'Bahan baku yang bersifat alergen ditempatkan terpisah dan/atau tertutup dari produk lainnya. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', 1, 'Bahan baku yang bersifat alergen ditempatkan terpisah dan/atau tertutup dari produk lainnya. Khusus untuk produk yang akan/sudah thawing, wajib dilakukan pemisahan.', '3', 0),
(118, 'Tidak melakukan thawing di suhu ruang dan membiarkan produk beku dalam jangka waktu >30 menit di suhu ruang', 1, 'Tidak melakukan thawing di suhu ruang dan membiarkan produk beku dalam jangka waktu >30 menit di suhu ruang', '3', 0),
(119, 'Sistem penyusunan di rak atau dalam chiller FREEZER memperhatikan FIFO atau FEFO', 1, 'Sistem penyusunan di rak atau dalam chiller FREEZER memperhatikan FIFO atau FEFO', '3', 0),
(120, 'idak meletakkan bahan makanan atau container penyimpanan bahan makanan langsung di atas lantai', 1, 'idak meletakkan bahan makanan atau container penyimpanan bahan makanan langsung di atas lantai', '3', 0),
(121, 'Karyawan menggunakan handgloves pada saat memegang bahan makanan yang akan disiapkan atau disajikan', 1, 'Karyawan menggunakan handgloves pada saat memegang bahan makanan yang akan disiapkan atau disajikan', '2', 0),
(122, 'Penggunaan lap sesuai dengan standard managemen lap, dalam keadaan bersih, dan layak pakai (tidak mengeringkan di atas boiler air )', 1, 'Penggunaan lap sesuai dengan standard managemen lap, dalam keadaan bersih, dan layak pakai (tidak mengeringkan di atas boiler air )', '2', 0),
(123, 'Setiap bahan kimia yang tersimpan di luar janitor tidak dalam kondisi murni(kecuali pada dispenser steward, bar,open kitchen, hand soap dan sanitizer) dan diberi label yang sesuai.', 1, 'Setiap bahan kimia yang tersimpan di luar janitor tidak dalam kondisi murni(kecuali pada dispenser steward, bar,open kitchen, hand soap dan sanitizer) dan diberi label yang sesuai.', '2', 0),
(124, 'Dispenser chemical berfungsi dengan baik dan semua terisi dengan chemical(dispenser yang rusak harus dibuktikan dengan pengajuan perbaikan)', 1, 'Dispenser chemical berfungsi dengan baik dan semua terisi dengan chemical(dispenser yang rusak harus dibuktikan dengan pengajuan perbaikan)', '1', 0),
(125, 'Soaking dilaksanakan setiap hari sesuai jadwal dan cara soaking sesuai dengan standard (catat jam dilakukan soaking)', 1, 'Soaking dilaksanakan setiap hari sesuai jadwal dan cara soaking sesuai dengan standard (catat jam dilakukan soaking)', '1', 0),
(126, 'Pada kitchen (open dan back kitchen) tersedia ember yang berisi larutan nobla liquid(untuk mencuci) dan  J-512(untuk mensanitasi)', 1, 'Pada kitchen (open dan back kitchen) tersedia ember yang berisi larutan nobla liquid(untuk mencuci) dan  J-512(untuk mensanitasi)', '2', 0),
(127, 'Tidak ada carton (kemasan primer) di chest freezer(kecuali chest frezer yang digunakan oleh stockeeper karena tidak ada Walking Freezer)', 1, 'Tidak ada carton (kemasan primer) di chest freezer(kecuali chest frezer yang digunakan oleh stockeeper karena tidak ada Walking Freezer)', '3', 0),
(128, 'Sampling cara pengisian form suhu chiller/freezer dilakukan dengan benar (catat jam periksa)', 1, 'Sampling cara pengisian form suhu chiller/freezer dilakukan dengan benar (catat jam periksa)', '2', 0),
(129, 'Kondisi chiller yang digunakan suhu 1-4 deg C. NOTE: kecuali yang sudah diberi label QA', 1, 'Kondisi chiller yang digunakan suhu 1-4 deg C. NOTE: kecuali yang sudah diberi label QA', '3', 0),
(130, 'Kondisi Freezer yang digunakan suhu maksimal -15 deg C (kecuali yang sudah dilabel QA)', 1, 'Kondisi Freezer yang digunakan suhu maksimal -15 deg C (kecuali yang sudah dilabel QA)', '3', 0),
(131, 'Form pencatatan suhu chiller dan freezer tersedia dan terisi lengkap', 1, 'Form pencatatan suhu chiller dan freezer tersedia dan terisi lengkap', '1', 0),
(132, 'Chiller dan freezer dalam kondisi bersih(termasuk bagian gasket, handle, bagian dalam, dan luar) dan tertatapa rapi', 1, 'Chiller dan freezer dalam kondisi bersih(termasuk bagian gasket, handle, bagian dalam, dan luar) dan tertatapa rapi', '1', 0),
(133, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan teridentifikasi dengan jelas', 1, 'Semua tempat penyimpanan dalam kondisi bersih, rapi, terawat dan teridentifikasi dengan jelas', '1', 0),
(134, 'Lantai, dinding, langit-langit, difuser ac, exhaust bersih, terawat (tidak retak, pecah dan gompal) dan tidak dalam kondisi terbuka', 1, 'Lantai, dinding, langit-langit, difuser ac, exhaust bersih, terawat (tidak retak, pecah dan gompal) dan tidak dalam kondisi terbuka', '1', 0),
(135, 'Bagian kolong chiller, freezer, meja, rak dan peralatan lainnya dalam kondisi bersih', 1, 'Bagian kolong chiller, freezer, meja, rak dan peralatan lainnya dalam kondisi bersih', '1', 0),
(136, 'Sink, gutter dan saluran pembuangan dalam kondisi tidak tersumbat, tidak berbau, bersih dan terawat', 1, 'Sink, gutter dan saluran pembuangan dalam kondisi tidak tersumbat, tidak berbau, bersih dan terawat', '1', 0),
(137, 'Tempat sampah dalam kondisi bersih, layak pakai(terdapat pijakan), terdapat plastik di bagian dalam dan tertutup', 1, 'Tempat sampah dalam kondisi bersih, layak pakai(terdapat pijakan), terdapat plastik di bagian dalam dan tertutup', '1', 0),
(138, 'Semua peralatan dalam kondisi bersih dan layak pakai', 1, 'Semua peralatan dalam kondisi bersih dan layak pakai', '1', 0),
(139, 'Meja kerja dan rak penyimpanan dalam kondisi bersih, terawat dan bebas dari lemak/minyak', 1, 'Meja kerja dan rak penyimpanan dalam kondisi bersih, terawat dan bebas dari lemak/minyak', '1', 0),
(140, 'Tidak menyimpan scoop/sendok didalam wadah penyimpanan (tepung, produk )', 1, 'Tidak menyimpan scoop/sendok didalam wadah penyimpanan (tepung, produk )', '1', 0),
(142, 'Ice maker dan ice bin dalam kondisi bersih dan terawat (tidak ada bahan lain selain ice cube di dalam ice bin) dan dalam kondisi tertutup.', 12, 'Ice maker dan ice bin dalam kondisi bersih dan terawat (tidak ada bahan lain selain ice cube di dalam ice bin) dan dalam kondisi tertutup.', '1', 0),
(143, 'Pengambilan ice menggunakan ice scoop yang disediakan dan ice cube tidak disentuh dengan tangan tanpa hand gloves', 12, 'Pengambilan ice menggunakan ice scoop yang disediakan dan ice cube tidak disentuh dengan tangan tanpa hand gloves', '2', 0),
(144, 'Peralatan kerja dalam kondisi bersih, terawat,  berfungsi dengan baik dan layak pakai (ice scoop, shaker, muddle, pisau, maneta,dll). Peralatan kerja segera dibersihkan setelah digunakan dan disimpan di dalam larutan sanitizer saat tdk digunakan dengan bagian yang terkena produk tercelup seluruhnya ke dalam larutan(scoop ice cube dan ice cream, muddle, pisau, spoon, dan lainnya)', 12, 'Peralatan kerja dalam kondisi bersih, terawat,  berfungsi dengan baik dan layak pakai (ice scoop, shaker, muddle, pisau, maneta,dll). Peralatan kerja segera dibersihkan setelah digunakan dan disimpan di dalam larutan sanitizer saat tdk digunakan dengan bagian yang terkena produk tercelup seluruhnya ke dalam larutan(scoop ice cube dan ice cream, muddle, pisau, spoon, dan lainnya)', '1', 0),
(145, 'Gelas, cangkir, color plate dan peralatan makan bersih dan terawat, tidak gompal atau berkerak', 12, 'Gelas, cangkir, color plate dan peralatan makan bersih dan terawat, tidak gompal atau berkerak', '1', 0),
(146, 'Microwave, chiller dan freezer dalam kondisi bersih dan berfungsi dengan baik', 12, 'Microwave, chiller dan freezer dalam kondisi bersih dan berfungsi dengan baik', '1', 0),
(147, 'Tidak  menyimpan ice scoop di dalam ice maker ', 12, 'Tidak  menyimpan ice scoop di dalam ice maker ', '1', 0),
(148, 'Tidak  menyimpan ice scoop di dalam ice maker ', 12, 'Tidak  menyimpan ice scoop di dalam ice maker ', '1', 0),
(149, 'Semua produk ice cream yang disimpan di dalam freezer memiliki label expired date (atau memiliki kemampuan telusur untuk penentuan exp date)', 12, 'Semua produk ice cream yang disimpan di dalam freezer memiliki label expired date (atau memiliki kemampuan telusur untuk penentuan exp date)', '1', 0),
(150, 'pencucian gelas hiball dilakukan di lakukan di bar sesuai standart yang berlaku', 12, 'pencucian gelas hiball dilakukan di lakukan di bar sesuai standart yang berlaku', '1', 0),
(151, 'tidak menyimpan bahan makanan di samping grease trap', 12, 'tidak menyimpan bahan makanan di samping grease trap', '1', 0),
(152, 'Wine chiller (red wine) yang digunakan suhu 15-20 deg C', 12, 'Wine chiller (red wine) yang digunakan suhu 15-20 deg C', '1', 0),
(186, 'kamu', NULL, 'aku', '1', 0),
(187, 'test1', 1, 'test2', '1', 0),
(188, 'test', 13, 'tes1212', '1', 0),
(189, 'test 2', 14, 'tesssss 2', '2', 0),
(190, 'test', 15, 'test', '1', 0),
(191, 'halu', 16, 'halusinasi adalah', '2', 1),
(194, 'testt', 16, 'testt', '1', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_group`
--

CREATE TABLE `ticket_group` (
  `group_id` mediumint(11) UNSIGNED NOT NULL,
  `group_store` smallint(3) UNSIGNED DEFAULT NULL,
  `group_date` date DEFAULT NULL,
  `group_finding` smallint(2) UNSIGNED DEFAULT NULL,
  `group_point` smallint(3) UNSIGNED DEFAULT NULL,
  `group_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ticket_group`
--

INSERT INTO `ticket_group` (`group_id`, `group_store`, `group_date`, `group_finding`, `group_point`, `group_status`) VALUES
(1, 1, '2018-07-01', 0, 0, 0),
(2, 2, '2018-07-01', 1, 1, 0),
(3, 3, '2018-07-01', 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_log`
--

CREATE TABLE `ticket_log` (
  `log_id` int(11) UNSIGNED NOT NULL,
  `log_title` text NOT NULL,
  `ticket` mediumint(11) UNSIGNED DEFAULT NULL,
  `user` smallint(3) UNSIGNED NOT NULL,
  `log_type` varchar(20) NOT NULL,
  `log_content` text NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_main`
--

CREATE TABLE `ticket_main` (
  `ticket_id` mediumint(11) UNSIGNED NOT NULL,
  `ticket_group` mediumint(11) DEFAULT NULL,
  `category` smallint(5) UNSIGNED NOT NULL,
  `prp_date` date DEFAULT NULL,
  `ticket_creator` smallint(4) UNSIGNED NOT NULL,
  `submit_date` date DEFAULT NULL,
  `finding_images` text,
  `resolved_date` date DEFAULT NULL,
  `resolved_images` text,
  `resolved_solution` text,
  `status` tinyint(1) NOT NULL,
  `last_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `store` smallint(3) DEFAULT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ticket_main`
--

INSERT INTO `ticket_main` (`ticket_id`, `ticket_group`, `category`, `prp_date`, `ticket_creator`, `submit_date`, `finding_images`, `resolved_date`, `resolved_images`, `resolved_solution`, `status`, `last_update`, `store`, `notes`) VALUES
(1, 1, 60, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(2, 1, 61, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(3, 1, 62, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(4, 1, 63, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(5, 1, 64, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(6, 1, 65, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(7, 1, 66, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(8, 1, 67, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(9, 1, 68, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(10, 1, 128, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(11, 1, 129, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(12, 1, 130, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(13, 1, 131, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(14, 1, 132, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(15, 1, 133, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(16, 1, 134, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(17, 1, 135, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(18, 1, 136, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(19, 1, 137, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(20, 1, 138, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(21, 1, 8, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(22, 1, 9, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(23, 1, 10, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(24, 1, 11, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(25, 1, 12, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(26, 1, 13, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(27, 1, 14, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(28, 1, 15, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(29, 1, 16, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(30, 1, 17, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(31, 1, 18, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(32, 1, 19, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(33, 1, 20, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(34, 1, 21, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(35, 1, 22, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(36, 1, 23, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(37, 1, 24, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(38, 1, 25, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(39, 1, 26, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(40, 1, 27, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(41, 1, 28, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(42, 1, 29, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(43, 1, 30, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(44, 1, 31, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(45, 1, 5, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(46, 1, 6, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(47, 1, 7, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(48, 1, 1, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(49, 1, 2, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(50, 1, 3, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(51, 1, 4, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(52, 1, 98, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(53, 1, 99, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(54, 1, 100, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(55, 1, 101, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(56, 1, 102, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(57, 1, 103, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(58, 1, 104, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(59, 1, 105, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(60, 1, 106, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(61, 1, 107, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(62, 1, 108, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(63, 1, 109, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(64, 1, 110, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(65, 1, 111, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(66, 1, 112, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(67, 1, 113, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(68, 1, 114, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(69, 1, 115, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(70, 1, 116, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(71, 1, 117, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(72, 1, 118, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(73, 1, 119, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(74, 1, 120, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(75, 1, 121, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(76, 1, 122, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(77, 1, 123, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(78, 1, 124, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(79, 1, 125, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(80, 1, 126, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(81, 1, 127, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(82, 1, 46, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(83, 1, 47, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(84, 1, 48, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(85, 1, 49, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(86, 1, 50, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(87, 1, 51, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(88, 1, 52, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(89, 1, 53, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(90, 1, 54, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(91, 1, 55, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(92, 1, 56, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(93, 1, 57, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(94, 1, 58, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(95, 1, 59, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(96, 1, 87, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(97, 1, 88, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(98, 1, 89, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(99, 1, 90, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(100, 1, 91, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(101, 1, 92, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(102, 1, 93, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(103, 1, 94, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(104, 1, 95, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(105, 1, 96, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(106, 1, 97, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(107, 1, 32, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(108, 1, 33, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(109, 1, 34, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(110, 1, 35, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(111, 1, 36, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(112, 1, 37, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(113, 1, 38, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(114, 1, 39, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(115, 1, 40, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(116, 1, 69, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(117, 1, 70, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(118, 1, 71, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(119, 1, 72, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(120, 1, 73, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(121, 1, 74, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(122, 1, 75, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(123, 1, 76, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(124, 1, 77, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(125, 1, 78, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(126, 1, 79, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(127, 1, 80, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(128, 1, 81, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(129, 1, 82, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(130, 1, 83, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(131, 1, 84, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(132, 1, 85, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(133, 1, 86, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(134, 1, 41, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(135, 1, 42, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(136, 1, 43, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(137, 1, 44, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(138, 1, 45, '2018-07-01', 1001, '2018-07-20', NULL, NULL, NULL, NULL, 0, NULL, 1, ''),
(139, 2, 60, '2018-07-01', 1001, '2018-07-24', '15324052980.jpg', NULL, NULL, NULL, 1, NULL, 2, ''),
(140, 2, 61, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(141, 2, 62, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(142, 2, 63, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(143, 2, 64, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(144, 2, 65, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(145, 2, 66, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(146, 2, 67, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(147, 2, 68, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(148, 2, 128, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(149, 2, 129, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(150, 2, 130, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(151, 2, 131, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(152, 2, 132, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(153, 2, 133, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(154, 2, 134, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(155, 2, 135, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(156, 2, 136, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(157, 2, 137, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(158, 2, 138, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(159, 2, 8, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(160, 2, 9, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(161, 2, 10, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(162, 2, 11, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(163, 2, 12, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(164, 2, 13, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(165, 2, 14, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(166, 2, 15, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(167, 2, 16, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(168, 2, 17, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(169, 2, 18, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(170, 2, 19, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(171, 2, 20, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(172, 2, 21, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(173, 2, 22, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(174, 2, 23, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(175, 2, 24, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(176, 2, 25, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(177, 2, 26, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(178, 2, 27, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(179, 2, 28, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(180, 2, 29, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(181, 2, 30, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(182, 2, 31, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(183, 2, 5, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(184, 2, 6, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(185, 2, 7, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(186, 2, 1, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(187, 2, 2, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(188, 2, 3, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(189, 2, 4, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(190, 2, 98, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(191, 2, 99, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(192, 2, 100, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(193, 2, 101, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(194, 2, 102, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(195, 2, 103, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(196, 2, 104, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(197, 2, 105, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(198, 2, 106, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(199, 2, 107, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(200, 2, 108, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(201, 2, 109, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(202, 2, 110, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(203, 2, 111, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(204, 2, 112, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(205, 2, 113, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(206, 2, 114, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(207, 2, 115, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(208, 2, 116, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(209, 2, 117, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(210, 2, 118, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(211, 2, 119, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(212, 2, 120, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(213, 2, 121, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(214, 2, 122, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(215, 2, 123, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(216, 2, 124, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(217, 2, 125, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(218, 2, 126, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(219, 2, 127, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(220, 2, 46, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(221, 2, 47, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(222, 2, 48, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(223, 2, 49, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(224, 2, 50, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(225, 2, 51, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(226, 2, 52, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(227, 2, 53, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(228, 2, 54, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(229, 2, 55, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(230, 2, 56, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(231, 2, 57, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(232, 2, 58, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(233, 2, 59, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(234, 2, 87, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(235, 2, 88, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(236, 2, 89, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(237, 2, 90, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(238, 2, 91, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(239, 2, 92, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(240, 2, 93, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(241, 2, 94, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(242, 2, 95, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(243, 2, 96, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(244, 2, 97, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(245, 2, 32, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(246, 2, 33, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(247, 2, 34, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(248, 2, 35, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(249, 2, 36, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(250, 2, 37, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(251, 2, 38, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(252, 2, 39, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(253, 2, 40, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(254, 2, 69, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(255, 2, 70, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(256, 2, 71, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(257, 2, 72, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(258, 2, 73, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(259, 2, 74, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(260, 2, 75, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(261, 2, 76, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(262, 2, 77, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(263, 2, 78, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(264, 2, 79, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(265, 2, 80, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(266, 2, 81, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(267, 2, 82, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(268, 2, 83, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(269, 2, 84, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(270, 2, 85, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(271, 2, 86, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(272, 2, 41, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(273, 2, 42, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(274, 2, 43, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(275, 2, 44, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(276, 2, 45, '2018-07-01', 1001, '2018-07-24', NULL, NULL, NULL, NULL, 0, NULL, 2, ''),
(277, 3, 60, '2018-07-01', 1066, '2018-07-27', '15326660010.jpg', NULL, NULL, NULL, 1, NULL, 3, ''),
(278, 3, 61, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(279, 3, 62, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(280, 3, 63, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(281, 3, 64, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(282, 3, 65, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(283, 3, 66, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(284, 3, 67, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(285, 3, 68, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(286, 3, 128, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(287, 3, 129, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(288, 3, 130, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(289, 3, 131, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(290, 3, 132, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(291, 3, 133, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(292, 3, 134, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(293, 3, 135, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(294, 3, 136, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(295, 3, 137, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(296, 3, 138, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(297, 3, 8, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(298, 3, 9, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(299, 3, 10, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(300, 3, 11, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(301, 3, 12, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(302, 3, 13, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(303, 3, 14, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(304, 3, 15, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(305, 3, 16, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(306, 3, 17, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(307, 3, 18, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(308, 3, 19, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(309, 3, 20, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(310, 3, 21, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(311, 3, 22, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(312, 3, 23, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(313, 3, 24, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(314, 3, 25, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(315, 3, 26, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(316, 3, 27, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(317, 3, 28, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(318, 3, 29, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(319, 3, 30, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(320, 3, 31, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(321, 3, 5, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(322, 3, 6, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(323, 3, 7, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(324, 3, 1, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(325, 3, 2, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(326, 3, 3, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(327, 3, 4, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(328, 3, 98, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(329, 3, 99, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(330, 3, 100, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(331, 3, 101, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(332, 3, 102, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(333, 3, 103, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(334, 3, 104, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(335, 3, 105, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(336, 3, 106, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(337, 3, 107, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(338, 3, 108, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(339, 3, 109, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(340, 3, 110, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(341, 3, 111, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(342, 3, 112, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(343, 3, 113, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(344, 3, 114, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(345, 3, 115, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(346, 3, 116, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(347, 3, 117, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(348, 3, 118, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(349, 3, 119, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(350, 3, 120, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(351, 3, 121, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(352, 3, 122, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(353, 3, 123, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(354, 3, 124, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(355, 3, 125, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(356, 3, 126, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(357, 3, 127, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(358, 3, 46, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(359, 3, 47, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(360, 3, 48, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(361, 3, 49, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(362, 3, 50, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(363, 3, 51, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(364, 3, 52, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(365, 3, 53, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(366, 3, 54, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(367, 3, 55, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(368, 3, 56, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(369, 3, 57, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(370, 3, 58, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(371, 3, 59, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(372, 3, 87, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(373, 3, 88, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(374, 3, 89, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(375, 3, 90, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(376, 3, 91, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(377, 3, 92, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(378, 3, 93, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(379, 3, 94, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(380, 3, 95, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(381, 3, 96, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(382, 3, 97, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(383, 3, 32, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(384, 3, 33, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(385, 3, 34, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(386, 3, 35, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(387, 3, 36, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(388, 3, 37, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(389, 3, 38, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(390, 3, 39, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(391, 3, 40, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(392, 3, 69, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(393, 3, 70, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(394, 3, 71, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(395, 3, 72, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(396, 3, 73, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(397, 3, 74, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(398, 3, 75, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(399, 3, 76, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(400, 3, 77, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(401, 3, 78, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(402, 3, 79, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(403, 3, 80, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(404, 3, 81, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(405, 3, 82, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(406, 3, 83, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(407, 3, 84, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(408, 3, 85, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(409, 3, 86, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(410, 3, 41, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(411, 3, 42, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(412, 3, 43, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(413, 3, 44, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, ''),
(414, 3, 45, '2018-07-01', 1066, '2018-07-27', NULL, NULL, NULL, NULL, 0, NULL, 3, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_priority`
--

CREATE TABLE `ticket_priority` (
  `priority_id` smallint(13) UNSIGNED NOT NULL,
  `priority_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ticket_priority`
--

INSERT INTO `ticket_priority` (`priority_id`, `priority_name`) VALUES
(1, 'Rendah'),
(2, 'Sedang'),
(3, 'Tinggi'),
(4, 'ASAP');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_status`
--

CREATE TABLE `ticket_status` (
  `status_id` smallint(3) UNSIGNED NOT NULL,
  `status_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ticket_status`
--

INSERT INTO `ticket_status` (`status_id`, `status_name`) VALUES
(0, 'Normal'),
(1, 'Finding'),
(2, 'Resolved');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_type`
--

CREATE TABLE `ticket_type` (
  `type_id` tinyint(2) UNSIGNED NOT NULL,
  `type_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ticket_type`
--

INSERT INTO `ticket_type` (`type_id`, `type_name`) VALUES
(1, 'Finding'),
(2, 'Resolved');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_upload`
--

CREATE TABLE `ticket_upload` (
  `upload_id` smallint(3) UNSIGNED NOT NULL,
  `upload_name` varchar(50) NOT NULL,
  `upload_location` varchar(50) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ticket_id` mediumint(11) UNSIGNED NOT NULL,
  `upload_temp` tinyint(1) DEFAULT '0',
  `upload_user` smallint(4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ticket_upload`
--

INSERT INTO `ticket_upload` (`upload_id`, `upload_name`, `upload_location`, `upload_time`, `ticket_id`, `upload_temp`, `upload_user`) VALUES
(1, '15308173990.jpg', 'Resources/images/prp\\', '2018-07-05 19:03:21', 3, 0, 1001),
(2, '15308174010.jpg', 'Resources/images/prp\\', '2018-07-05 19:03:21', 4, 0, 1001),
(3, '15308444270.png', 'Resources/images/prp\\', '2018-07-06 02:33:48', 7, 0, 1001),
(4, '15318277830.png', 'Resources/images/prp\\', '2018-07-17 11:43:03', 144, 0, 1001);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` smallint(4) UNSIGNED NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `store_id` smallint(3) UNSIGNED NOT NULL,
  `department_id` smallint(3) UNSIGNED NOT NULL,
  `group_id` smallint(2) UNSIGNED NOT NULL,
  `employee_id` mediumint(8) UNSIGNED ZEROFILL NOT NULL,
  `security_question` tinyint(1) UNSIGNED NOT NULL,
  `security_answer` varchar(40) NOT NULL,
  `user_status` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_password`, `display_name`, `email`, `store_id`, `department_id`, `group_id`, `employee_id`, `security_question`, `security_answer`, `user_status`) VALUES
(1001, 'rahmats', 'd6d0f7a13d087f5c11f501fd264d6019', 'Rahmat Samsudin', 'it1.jkt@sushitei.co.id', 99, 0, 1, 00009311, 2, 'rocky', 1),
(1023, 'hadi.chandra', '613e8301788b5a4d73f3da22ead8ac27', 'Hadi Chandra', 'hadi.chandra@sushitei.co.id', 99, 0, 5, 00000000, 4, 'ayam', 1),
(1027, 'manager.pim', '2c20f79b5ac0b29140ba36346b0a70d7', 'Sushitei PIM', 'manager.pim@sushitei.co.id', 3, 0, 4, 00000000, 0, '', 1),
(1028, 'vendorst', 'e10adc3949ba59abbe56e057f20f883e', 'Vendor', 'matsu.yoshioka@gmail.com', 99, 0, 3, 00000000, 0, '', 1),
(1029, 'teknisist', 'e10adc3949ba59abbe56e057f20f883e', 'Teknisi', 'matsu.yoshioka@gmail.com', 99, 0, 3, 00000000, 0, '', 1),
(1030, 'manager.ps', 'b2c3369082cfbdb5e5442f2f63f3ad4b', 'Sushitei Plasa Senayan', 'manager.ps@sushitei.co.id', 2, 0, 4, 00000000, 0, '', 1),
(1031, 'manager.pi', '4888ffaac1603f8d4674b287eaa7677b', 'Sushitei Plaza Indonesia', 'manager.pi@sushitei.co.id', 1, 0, 4, 00000000, 0, '', 1),
(1032, 'manager.sency', '8c130661ebe27564fd1411ba879503ac', 'Sushitei Senayan City', 'manager.senci@sushitei.co.id', 4, 0, 4, 00000000, 0, '', 1),
(1033, 'manager.mkg', 'e10adc3949ba59abbe56e057f20f883e', 'Sushitei Kelapa Gading', 'manager.mkg@sushitei.co.id', 5, 0, 4, 00000000, 0, '', 1),
(1034, 'manager.ep', '77b6ea832fbe5649467823c3eb0d65ef', 'Sushitei Emporium Pluit', 'manager.ep@sushitei.co.id', 6, 0, 4, 00000000, 0, '', 1),
(1035, 'manager.cp', '94a9a24642521953fef39522471605da', 'Sushitei Central Park', 'manager.cp@sushitei.co.id', 7, 0, 4, 00000000, 0, '', 1),
(1036, 'manager.gc', '8cd2058b051beecc76efa5b322fbf90e', 'Sushitei Gandaria City', 'manager.gc@sushitei.co.id', 8, 0, 4, 00000000, 0, '', 1),
(1037, 'manager.fs', '20b4ce998d952765019f21d2f516f711', 'Sushitei Flavour Bliss', 'manager.fs@sushitei.co.id', 9, 0, 4, 00000000, 0, '', 1),
(1038, 'manager.kw', 'fddb8f887522a2b9eab2752788f8886b', 'Sushitei Karawaci', 'manager.karawaci@sushitei.co.id', 10, 0, 4, 00000000, 0, '', 1),
(1039, 'manager.gi', 'd6d0f7a13d087f5c11f501fd264d6019', 'Sushitei Grand Indonesia', 'manager.gi@sushitei.co.id', 11, 0, 4, 00000000, 0, '', 1),
(1040, 'manager.la', '188d99e16fda475b34ecb4fd5add6a89', 'Sushitei Lotte Avenue', 'manager.la@sushitei.co.id', 12, 0, 4, 00000000, 0, '', 1),
(1041, 'manager.kk', 'd1fb9aebb070e2f1e2473ece7fdcbbf2', 'Sushitei Kota Kasablanka', 'manager.kk@sushitei.co.id', 13, 0, 4, 00000000, 0, '', 1),
(1042, 'manager.smb', '57e19e7c607c0b6c39053f15c04c86d5', 'Sushitei Summarecon Bekasi', 'manager.sb@sushitei.co.id', 14, 0, 4, 00000000, 0, '', 1),
(1043, 'manager.bx', '5f4b5cda1bc940f6722ad866d2c0cf55', 'Sushitei Bintaro Xchange', 'manager.bx@sushitei.co.id', 15, 0, 4, 00000000, 0, '', 1),
(1044, 'manager.tb', 'ea4af47f30aceda460d727a28ae3c57d', 'Sushitei The Breeze', 'manager.tb@sushitei.co.id', 16, 0, 4, 00000000, 0, '', 1),
(1045, 'manager.pm', 'cf25c9a7182456585c8dcdc4782e97bf', 'Sushitei Puri Mall', 'manager.pm@sushitei.co.id', 17, 0, 4, 00000000, 0, '', 1),
(1046, 'manager.sms', 'e10adc3949ba59abbe56e057f20f883e', 'Sushitei Summarecon Serpong', 'manager.sms@sushitei.co.id', 18, 0, 4, 00000000, 0, '', 1),
(1047, 'admin.mrp1', 'e10adc3949ba59abbe56e057f20f883e', 'Lia', 'mrp1.jkt@sushitei.co.id', 99, 0, 5, 00000000, 0, '', 1),
(1050, 'sk.puri', 'e10adc3949ba59abbe56e057f20f883e', 'Sushi-Kiosk Puri', 'puriindahmall@sushi-kiosk.com', 100, 0, 4, 00000000, 0, '', 1),
(1051, 'sk.wtc', 'e10adc3949ba59abbe56e057f20f883e', 'Sushi-Kiosk WTC 2', 'wtc_2@sushi-kiosk.com', 104, 0, 4, 00000000, 0, '', 1),
(1052, 'sk.sentul', 'e10adc3949ba59abbe56e057f20f883e', 'Sushi-Kiosk Sentul', 'giantsentul@sushi-kiosk.com', 101, 0, 4, 00000000, 0, '', 1),
(1053, 'sk.botani', 'e10adc3949ba59abbe56e057f20f883e', 'Sushi-Kiosk Botani', 'giantbotani@sushi-kiosk.com', 102, 0, 4, 00000000, 0, '', 1),
(1054, 'sk.gi', 'e10adc3949ba59abbe56e057f20f883e', 'Sushi-Kiosk Grand Indonesia', 'grandindonesia@sushi-kiosk.com', 103, 0, 4, 00000000, 0, '', 1),
(1055, 'admin.mrp2', '25d55ad283aa400af464c76d713c07ad', 'Rudi (MRP-2)', 'mrp2.jkt@sushitei.co.id', 99, 0, 5, 00000000, 0, '', 1),
(1056, 'kris.h', '4f9b7c5a93ce2dc8281a9820a12866e0', 'Krishandaya', 'krishandaya@sushitei.co.id', 99, 0, 1, 00000000, 0, '', 1),
(1057, 'admin.hck', '482c811da5d5b4bc6d497ffa98491e38', 'Admin HCK', 'cdp.ck@sushitei.co.id', 105, 0, 4, 00000000, 0, '', 1),
(1058, 'hadi.chandra2', '613e8301788b5a4d73f3da22ead8ac27', 'Hadi Chandra', 'hadi.chandra@sushitei.co.id', 99, 0, 2, 00000000, 4, 'ayam', 1),
(1060, 'admin.mrp3', 'e10adc3949ba59abbe56e057f20f883e', 'Admin MRP 1', 'mrp1.jkt@sushitei.co.id', 99, 0, 5, 00000000, 0, '', 1),
(1061, 'alfin', '20b4ce998d952765019f21d2f516f711', 'Alfin', 'alfin@sushitei.co.id', 99, 0, 7, 00000000, 1, 'Plaza Indonesia', 1),
(1062, 'purchasing.pqnc', 'e10adc3949ba59abbe56e057f20f883e', 'Purchasing', 'purchteam.jkt@sushitei.co.id', 99, 0, 9, 00000000, 0, '', 1),
(1063, 'fbv.pqnc', 'e10adc3949ba59abbe56e057f20f883e', 'FBV', 'ronald@sushitei.co.id', 99, 0, 9, 00000000, 0, '', 1),
(1064, 'hck.pqnc', 'e10adc3949ba59abbe56e057f20f883e', 'HCK', 'central.kitchen@sushitei.co.id', 105, 0, 9, 00000000, 0, '', 1),
(1065, 'other.pqnc', '25d55ad283aa400af464c76d713c07ad', 'Other', 'other@pqnc.co.id', 99, 0, 9, 00000000, 0, '', 1),
(1066, 'qa.spv3', 'e10adc3949ba59abbe56e057f20f883e', 'Levi', 'levi@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1067, 'peter', 'e10adc3949ba59abbe56e057f20f883e', 'Peter', 'peter@sushitei.co.id', 99, 0, 7, 00000000, 3, 'agatha', 1),
(1068, 'dede', 'e10adc3949ba59abbe56e057f20f883e', 'Dede Mardinah', 'adminfbspv@sushitei.co.id', 99, 0, 7, 00000000, 1, 'PI', 1),
(1070, 'dcr1.jkt', 'e10adc3949ba59abbe56e057f20f883e', 'Fika', 'dcr1.jkt@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1071, 'deasy', 'e10adc3949ba59abbe56e057f20f883e', 'Deasy', 'deasy@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1072, 'fanny', 'e10adc3949ba59abbe56e057f20f883e', 'Fanny', 'fanny@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 0),
(1073, 'qa.spv', 'e10adc3949ba59abbe56e057f20f883e', 'Angkit', 'qa.spv@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1074, 'qa3.jkt', 'e10adc3949ba59abbe56e057f20f883e', 'Rianty', 'qa3.jkt@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1075, 'qa.spv2', 'e10adc3949ba59abbe56e057f20f883e', 'Rizki', 'qa.spv2@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1076, 'qa1.jkt', 'e10adc3949ba59abbe56e057f20f883e', 'Cholil', 'qa1.jkt@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1077, 'qa4.jkt', 'e10adc3949ba59abbe56e057f20f883e', 'Yonas', 'qa4.jkt@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1078, 'qa.spv4', 'e10adc3949ba59abbe56e057f20f883e', 'Dhini', 'qa.spv4@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1079, 'admin.GA', 'e10adc3949ba59abbe56e057f20f883e', 'GA Dept', 'ga1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1080, 'warehouse.pqnc', 'e10adc3949ba59abbe56e057f20f883e', 'Warehouse', 'warehouse.jkt@sushitei.co.id', 107, 0, 4, 00000000, 0, '', 1),
(1082, 'admin.fb', 'e1e3f3b0023a13d7fde2a2f855fe674e', 'Anna', 'adminfb@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1083, 'headsk1', '7622577d8ba88def79fdb4a5d7287c62', 'Widodo', 'headsk1@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1084, 'headsk2', 'bb21f57669bd67510c8a1f5b11c107d6', 'Dwi Kristanto', 'headsk2@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1085, 'headsk3', 'e4f7a903ea1a5b02731bed33a7c8ae9c', 'Fadilah Hakim', 'headsk3@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1086, 'headsk4', 'cbae336ca858efebc022a2cfd15966fd', 'Ridwan Golprit', 'headsk4@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1087, 'rizal', 'f038b99ff44cc49470c6625468df8945', 'Rizal', 'rizal@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1088, 'suandi', '1cf2a6f80ac6617d92f779117c16040a', 'Suandi', 'suandi@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1089, 'diky', '885fb122bf4060b884f90e4723ddecd5', 'Diky', 'diky@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1090, 'wahyuni', '8be51db6edbf812541b84432784e41be', 'Wahyuni', 'wahyuni@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1091, 'oktavianus', '5022b977c0256b7f1ee347f562100359', 'Oktavianus', 'oktavianus@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1092, 'm.chandra', 'e10adc3949ba59abbe56e057f20f883e', 'Michael', 'm.chandra@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1093, 'thubagus', 'a1ff0d8eb36cc3a08a9a87e0a77b7ab9', 'Thubagus', 'thubagus@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1094, 'ade.gendawi', 'bc7ea0787ab33f7be740dba9170de02b', 'Ade Gendawi', 'ade.gendawi@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1095, 'alam', '6228ee0112cacd194bb8621df5162dca', 'Alam', 'alam@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1096, 'ronald', 'e10adc3949ba59abbe56e057f20f883e', 'Ronald', 'ronald@sushitei.co.id', 99, 0, 5, 00000000, 0, '', 1),
(1097, 'fbv.sk', 'e10adc3949ba59abbe56e057f20f883e', 'FBV Sushi Kiosk', 'arief@sushi-kiosk.com', 99, 0, 7, 00000000, 0, '', 1),
(1098, 'purchasing', 'e10adc3949ba59abbe56e057f20f883e', 'Purchasing Team', 'purchteam.jkt@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1099, 'st.pim', 'e10adc3949ba59abbe56e057f20f883e', 'ST PIM', 'st.pim@sushitei.co.id', 3, 0, 4, 00000000, 0, '', 1),
(1100, 'st.mkg', 'e10adc3949ba59abbe56e057f20f883e', 'ST MKG', 'st.mkg@sushitei.co.id', 5, 0, 4, 00000000, 0, '', 1),
(1101, 'st.gi', 'e10adc3949ba59abbe56e057f20f883e', 'ST GI', 'st.gi@sushitei.co.id', 11, 0, 4, 00000000, 0, '', 1),
(1102, 'st.karawaci', 'e10adc3949ba59abbe56e057f20f883e', 'ST Karawaci', 'st.karawaci@sushitei.co.id', 10, 0, 4, 00000000, 0, '', 1),
(1103, 'st.fs', 'e10adc3949ba59abbe56e057f20f883e', 'ST FS', 'st.fs@sushitei.co.id', 9, 0, 4, 00000000, 0, '', 1),
(1104, 'st.gc', 'e10adc3949ba59abbe56e057f20f883e', 'ST GC', 'st.gc@sushitei.co.id', 8, 0, 4, 00000000, 0, '', 1),
(1105, 'opr.pqnc', 'e10adc3949ba59abbe56e057f20f883e', 'Dept OPR', 'rizal@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1108, 'st.sms', 'e10adc3949ba59abbe56e057f20f883e', 'ST SMS', 'st.sms@sushitei.co.id', 18, 0, 4, 00000000, 0, '', 1),
(1109, 'st.bx', 'e10adc3949ba59abbe56e057f20f883e', 'ST BX', 'st.bx@sushitei.co.id', 15, 0, 4, 00000000, 0, '', 1),
(1110, 'st.tb', 'e10adc3949ba59abbe56e057f20f883e', 'ST TB', 'st.tb@sushitei.co.id', 16, 0, 4, 00000000, 0, '', 1),
(1111, 'st.ps', 'e10adc3949ba59abbe56e057f20f883e', 'ST PS', 'st.ps@sushitei.co.id', 2, 0, 4, 00000000, 0, '', 1),
(1112, 'st.senci', 'e10adc3949ba59abbe56e057f20f883e', 'ST SC', 'st.senci@sushitei.co.id', 4, 0, 4, 00000000, 0, '', 1),
(1113, 'st.kk', 'e10adc3949ba59abbe56e057f20f883e', 'ST KK', 'st.kk@sushitei.co.id', 13, 0, 4, 00000000, 0, '', 1),
(1114, 'st.la', 'e10adc3949ba59abbe56e057f20f883e', 'ST LA', 'st.la@sushitei.co.id', 12, 0, 4, 00000000, 0, '', 1),
(1115, 'st.pi', 'e10adc3949ba59abbe56e057f20f883e', 'ST PI ', 'st.pi@sushitei.co.id', 1, 0, 4, 00000000, 0, '', 1),
(1116, 'st.pm', 'e10adc3949ba59abbe56e057f20f883e', 'ST PM', 'st.pm@sushitei.co.id', 17, 0, 4, 00000000, 0, '', 1),
(1117, 'st.cp', 'e10adc3949ba59abbe56e057f20f883e', 'ST CP', 'st.cp@sushitei.co.id', 7, 0, 4, 00000000, 0, '', 1),
(1118, 'st.ep', 'e10adc3949ba59abbe56e057f20f883e', 'ST EP ', 'st.ep@sushitei.co.id', 6, 0, 4, 00000000, 0, '', 1),
(1119, 'st.sb', 'e10adc3949ba59abbe56e057f20f883e', 'ST SMB', 'st.sb@sushitei.co.id', 14, 0, 4, 00000000, 0, '', 1),
(1120, 'manager.kv', '20b4ce998d952765019f21d2f516f711', 'Sushitei Kemang Village', 'manager.kv@sushitei.co.id', 108, 0, 4, 00000000, 0, '', 1),
(1121, 'senior.bartender', 'e10adc3949ba59abbe56e057f20f883e', 'Sayuti', 'senior.bartender@sushitei.co.id', 99, 0, 7, 00000000, 0, '', 1),
(1122, 'qa2.jkt', 'e10adc3949ba59abbe56e057f20f883e', 'Dian', 'qa2.jkt@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 0),
(1123, 'deasy2', 'e10adc3949ba59abbe56e057f20f883e', 'deasy2', 'feritadeasypanjaitan@gmail.com', 99, 0, 4, 00000000, 0, '', 1),
(1124, 'mchandra', 'd4c3740ca5424d82139a42f76775aad4', 'michael chandra ', 'm.chandra@sushitei.co.id', 99, 0, 8, 00000000, 1, 'plaza indonesia ', 1),
(1126, 'yanuar', '5f4dcc3b5aa765d61d8327deb882cf99', 'Yanuar Triansyah', 'yanuar@sushitei.co.id', 99, 0, 2, 00000000, 0, '', 1),
(1127, 'test.it', 'e10adc3949ba59abbe56e057f20f883e', 'Test IT', 'it1.jkt@sushitei.co.id', 18, 0, 4, 00000000, 0, '', 0),
(1128, 'dessy', 'e10adc3949ba59abbe56e057f20f883e', 'Dessy Anggoro Putri', 'it2.jkt@sushitei.co.id', 99, 0, 10, 00000000, 0, '', 1),
(1129, 'oddy', 'e10adc3949ba59abbe56e057f20f883e', 'Akhmad Rosaddy', 'it5.jkt@sushitei.co.id', 99, 0, 1, 00000000, 0, '', 1),
(1130, 'test.it2', 'e10adc3949ba59abbe56e057f20f883e', 'Test IT2', 'matsu.yoshioka@gmail.com', 17, 0, 4, 00000000, 0, '', 0),
(1131, 'Maya', 'f4a637ba636f4a3b258273c5605d4266', 'maya', 'purchasing2.jkt@sushitei.co.id', 99, 0, 4, 00000000, 4, 'mie ayam', 1),
(1132, 'herusartono', '13b182adad8c5eaa36fd1a56b193c43e', 'Heru Sartono', 'it7.jkt@sushitei.co.id', 99, 0, 10, 00000000, 2, 'heru', 1),
(1133, 'murni', '0a37c8bbca94a02da2e92277aba4db87', 'Murni', 'murniati@sushitei.co.id', 99, 0, 4, 00000000, 3, 'suwarti', 1),
(1134, 'eka.nuari', 'd4e90818aebefa9409d254b3bb9a6154', 'Eka Nuari', 'it4.jkt@sushitei.co.id', 99, 0, 10, 00000000, 0, '', 1),
(1135, 'wisnu', 'fc4d477402d75c8cf7c37e07d68ada9c', 'Wisnu Trijaya', 'it8.jkt@sushitei.co.id', 99, 0, 10, 00000000, 2, 'wisnu ganteng', 1),
(1136, 'dodi', 'd8a97849a8575cf7d95e84ed1b42c4fd', 'dodi', 'dodi@sushitei.co.id', 99, 0, 4, 00000000, 3, 'patmawati', 1),
(1137, 'poedi', 'e10adc3949ba59abbe56e057f20f883e', 'Poedi Udi', 'it3.jkt@sushitei.co.id', 99, 0, 10, 00000000, 0, '', 1),
(1138, 'itsby', 'e10adc3949ba59abbe56e057f20f883e', 'Norman', 'it.sby@sushitei.co.id', 99, 0, 4, 00000000, 1, 'Galaxy', 1),
(1139, 'yennita', 'cc531d104dbb91ac691e09e1f63d24d2', 'Yennita', 'yennita@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1140, 'riris', 'e10adc3949ba59abbe56e057f20f883e', 'Riris', 'purchasing3.jkt@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1141, 'audit', 'e10adc3949ba59abbe56e057f20f883e', 'IAD Department', 'kris@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1142, 'Suyatno', 'e10adc3949ba59abbe56e057f20f883e', 'Suyatno', 'suyatno@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1143, 'indri', '20b4ce998d952765019f21d2f516f711', 'Indri Fitriasari', 'indri@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1144, 'finance', '20b4ce998d952765019f21d2f516f711', 'FAT', 'aora@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1145, 'secretary.dir', '20b4ce998d952765019f21d2f516f711', 'Secretary Director', 'secretary.dir@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1146, 'yuliana', 'e10adc3949ba59abbe56e057f20f883e', 'Yuliana', 'yuliana@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1147, 'trainingdept', 'e10adc3949ba59abbe56e057f20f883e', 'Training Dept', 'training1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'ho', 1),
(1148, 'Nur.Hasanah', 'e10adc3949ba59abbe56e057f20f883e', 'Nur Hasanah', 'adminmarketing@sushitei.co.id', 99, 0, 4, 00000000, 2, 'nuy', 1),
(1149, 'isadept', '20b4ce998d952765019f21d2f516f711', 'ISA Dept', 'hanisah@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1150, 'qaspv3', '20b4ce998d952765019f21d2f516f711', 'Levi Saesari', 'qa.spv3@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 0),
(1151, 'witia', '20b4ce998d952765019f21d2f516f711', 'Witia ', 'witia@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1152, 'eva.kopber', '20b4ce998d952765019f21d2f516f711', 'Eva Koperasi Berkat', 'ketua.kopber@gmail.com', 99, 0, 4, 00000000, 1, 'HO', 1),
(1153, 'PayrolL', '994147d5e957c088b973969245f59e76', 'Payroll', 'Ismail@sushitei.co.id', 99, 0, 4, 00000000, 4, 'Kambing', 1),
(1154, 'st.medan', 'a5b5575afa655facd448eac9219ca8f8', 'Sushi Tei Medan', 'henrywijaya@bisagroup.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1155, 'mega', 'cd34f589fb25dd5d09de72df0dd83949', 'mega', 'mega@sushitei.co.id', 99, 0, 4, 00000000, 1, 'PI', 1),
(1156, 'Rifky.MKT', 'e10adc3949ba59abbe56e057f20f883e', 'Rifky', 'mkt_sk@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'HO', 1),
(1157, 'yudi', 'e10adc3949ba59abbe56e057f20f883e', 'Yudi Andryatno', 'yudi@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1158, 'ari', 'e10adc3949ba59abbe56e057f20f883e', 'Masri Maulia', 'purchasing4.jkt@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(1159, 'awaluddin', '3ef48b2eba47dcd26780419fe5e91fbe', 'M Awal Zamzami', 'awaluddin@sushitei.co.id', 105, 0, 4, 00000000, 1, 'QA', 1),
(1160, 'michael', '20b4ce998d952765019f21d2f516f711', 'michael', 'm.chandra@sushitei.co.id', 99, 0, 8, 00000000, 1, 'PI', 1),
(1161, 'recruitment', '20b4ce998d952765019f21d2f516f711', 'Recruitment Dept', 'recruitment1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1162, 'aora', '198084dd007ca74964c6de2dac09ea54', 'aora nathalia', 'aora@sushitei.co.id', 99, 0, 4, 00000000, 3, 'lily herlina', 1),
(1163, 'st.mc', 'e10adc3949ba59abbe56e057f20f883e', 'ST MC', 'st.mc@sushitei.co.id', 109, 0, 4, 00000000, 0, '', 1),
(1164, 'manager.mc', 'a68d7b83eebeb8d61b5a478409eed8a0', 'Sushi Tei Margo City', 'manager.mc@sushitei.co.id', 109, 0, 4, 00000000, 0, '', 1),
(1165, 'stmc', 'e10adc3949ba59abbe56e057f20f883e', 'Sushitei Margo City', 'st.mc@sushitei.co.id', 109, 0, 4, 00000000, 4, 'baso', 1),
(1166, 'Neta', 'e10adc3949ba59abbe56e057f20f883e', 'Neta', 'mkt1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 4, 'nasi goreng', 1),
(1167, 'venda', 'bf3bd8f8f2751d9341927829abc03286', 'Nurvenda Lafian', 'ppfbspv2@sushitei.co.id', 99, 0, 7, 00000000, 3, 'nunung', 1),
(1168, 'it.st.plg', '5bd180e9fda519ead64dc32a6971192f', 'Sushi Tei Palembang', 'it.st.plg@sushitei.co.id', 110, 0, 4, 00000000, 0, '', 1),
(1169, 'st.kv', 'e10adc3949ba59abbe56e057f20f883e', 'ST KV', 'st.kv@sushitei.co.id', 108, 0, 4, 00000000, 0, '', 1),
(1170, 'stbali', '6aac492f4df5e1592ffc3be37c01a884', 'Sushi Tei Bali', 'spv.it.bali@sushitei.co.id', 111, 0, 4, 00000000, 1, 'bali', 1),
(1171, 'qa.spv5', 'e10adc3949ba59abbe56e057f20f883e', 'Wenty', 'qa.spv5@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(1172, 'vera', 'e10adc3949ba59abbe56e057f20f883e', 'Vera Kusliawan', 'vera.kusliawan@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(1173, 'MRP.SPV', 'e10adc3949ba59abbe56e057f20f883e', 'Cece Rosadi', 'mrp.spv@sushitei.co.id', 99, 0, 5, 00000000, 0, '', 1),
(1174, 'adventputra', 'e10adc3949ba59abbe56e057f20f883e', 'Advent Putra', 'creative.spv@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(6001, 'accounting1.jkt', '20b4ce998d952765019f21d2f516f711', 'Adel Accounting', 'accounting1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6002, 'manda', '20b4ce998d952765019f21d2f516f711', 'Amanda Shajida', 'accounting2.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6003, 'accounting3.jkt', '20b4ce998d952765019f21d2f516f711', 'Vera Prastiana', 'accounting3.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6004, 'ade gendawi', 'e10adc3949ba59abbe56e057f20f883e', 'ade', 'ade.gendawi@sushitei.co.id', 99, 0, 4, 00000000, 1, 'amanda', 1),
(6005, 'Joelizar CK', 'c69f2ab2fbf52a6b28257f5659333790', 'Joelizar CK', 'adm.ck2@sushitei.co.id', 99, 0, 4, 00000000, 1, 'kitchen', 1),
(6008, 'cholis', '827ccb0eea8a706c4c34a16891f84e7b', 'Achmad Nurcholis', 'adminhrd2.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'hrd', 1),
(6009, 'Adminhrd', '96d0205a001056dea02f06b11533f4aa', 'Adminhrd', 'adminhrd3@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6011, 'skmas', '4a4c4d9bd8645ee7eb96def18647bae6', 'skmas', 'alamsutera@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'skmas', 1),
(6012, 'ga', '211c42bb02f7a9e63d30e9a12222577c', 'GA', 'alfin@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6013, 'Anastasia Eka', 'dbadeb374711be696aa01c9c51b33c80', 'Anastasia Eka', 'anastasia@sushitei.co.id', 99, 0, 4, 00000000, 1, 'eka', 1),
(6014, 'any puspa', '20b4ce998d952765019f21d2f516f711', 'any puspa', 'any@sushitei.co.id', 99, 0, 4, 00000000, 1, 'felicia', 1),
(6016, 'berryn', '629ab14fab772d78a58eea752bdfc0dc', 'berryn', 'berryn@boga.co.id', 99, 0, 4, 00000000, 1, 'saya berryn', 1),
(6017, 'Bona Tampubolon', '4d39661a6f14a86294dcb3de363fd611', 'Bona', 'bona@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'nurmala siregar', 1),
(6018, 'ck', 'e10adc3949ba59abbe56e057f20f883e', 'Central Kitchen', 'cdp.ck@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6020, 'cod.btm', '20b4ce998d952765019f21d2f516f711', 'Batam', 'cod.btm@sushitei.co.id', 99, 0, 4, 00000000, 1, 'batam', 1),
(6021, 'creative.mkt', '20b4ce998d952765019f21d2f516f711', 'Advent', 'creative.mkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'jakarta', 1),
(6022, 'wira', '20b4ce998d952765019f21d2f516f711', 'Creative Design SPV', 'creative.spv@sushitei.co.id', 99, 0, 4, 00000000, 1, 'si lucu', 1),
(6023, 'fbvsk', '211c42bb02f7a9e63d30e9a12222577c', 'Food & Beverage Sushi Kiosk', 'dadang@sushi-kiosk.com', 99, 0, 4, 00000000, 1, '123', 1),
(6024, 'fikaandita', '0f4f98975c3b7b2df8f90ade9930ad0b', 'fikaandita', 'dcr1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'sushi tei', 1),
(6025, 'qa', '211c42bb02f7a9e63d30e9a12222577c', 'QA', 'deasy@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6026, 'rahman', '20b4ce998d952765019f21d2f516f711', 'rahman', 'demi-cdp@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'auliarahman', 1),
(6027, 'dwi', 'e10adc3949ba59abbe56e057f20f883e', 'dwi setyawan', 'demi-cdp2@sushi-kiosk.com', 99, 0, 4, 00000000, 1, '123456', 1),
(6028, 'DianAccounting', '750f48161355ac52ad11c48ef5be70b6', 'DianAccounting', 'dian@sushitei.co.id', 99, 0, 4, 00000000, 1, 'accounting', 1),
(6029, 'South Area', 'e10adc3949ba59abbe56e057f20f883e', 'South Area', 'diky@sushitei.co.id', 99, 0, 4, 00000000, 1, 'diky', 1),
(6031, 'training', '211c42bb02f7a9e63d30e9a12222577c', 'Training', 'dodi@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6032, 'Dwisuryanti', '6116829f7b4b521adc60043e97240958', 'Dwi suryanti', 'dwi.ana.lope@gmail.com', 99, 0, 4, 00000000, 1, '31209', 1),
(6033, 'advent', '5f5057fd952c1256fd3070ff1807f05a', 'Advent Putra', 'creative.spv@sushitei.co.id', 99, 0, 4, 00000000, 1, 'hartini', 1),
(6034, 'admin', '12dbb1cea0b064a80830cdfbf9fab87f', 'admin', 'edy.tantra@sushitei.co.id', 18, 0, 4, 00000000, 1, '123', 1),
(6035, 'icw', '211c42bb02f7a9e63d30e9a12222577c', 'Inventory Control & Warehouse', 'edy.tantra@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6036, 'eko', 'c178ad9760f4ff695cd571313343061a', 'eko boga', 'eko@boga.co.id', 99, 0, 4, 00000000, 1, 'apul', 1),
(6037, 'hrd', '393c7e33e91519e87e1894cbc9dcd8eb', 'HRD', 'ernica@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6038, 'rianti', '4d8da22b4aa44aa696909c6937ed7def', 'anti', 'fabiandaffadaffa@yahoo.com', 99, 0, 4, 00000000, 1, '250183', 1),
(6040, 'finance4.jkt', '20b4ce998d952765019f21d2f516f711', 'Finance4.jkt', 'finance4.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6041, 'andrio', 'e10adc3949ba59abbe56e057f20f883e', 'andrio', 'ga1.jkt@sushitei.co.id', 99, 0, 11, 00000000, 1, 'rio123', 1),
(6042, 'ga2.jkt', '20b4ce998d952765019f21d2f516f711', 'Nina Rica', 'ga2.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'ga', 1),
(6043, 'garcia', '211c42bb02f7a9e63d30e9a12222577c', 'Garcia', 'garcia@sushitei.co.id', 99, 0, 4, 00000000, 1, 'garcia', 1),
(6044, 'skbintaro', '4a4c4d9bd8645ee7eb96def18647bae6', 'skbintaro', 'giantbintaro@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'skbintaro', 1),
(6045, 'skbotani', '4a4c4d9bd8645ee7eb96def18647bae6', 'SK Botani', 'giantbotani@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'botani', 1),
(6046, 'skbsd', '4a4c4d9bd8645ee7eb96def18647bae6', 'skbsd', 'giantbsd@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'skbsd', 1),
(6047, 'sksentul', '4a4c4d9bd8645ee7eb96def18647bae6', 'sksentul', 'giantsentul@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'sksentul', 1),
(6048, 'skgi', 'b94a30e7f6c59728c83021bd23b847b7', 'sushi kiosk gi', 'grandindonesia@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'win', 1),
(6049, 'gi', '211c42bb02f7a9e63d30e9a12222577c', 'sushi kiosk', 'grandindonesia@sushi-kiosk.com1', 99, 0, 4, 00000000, 1, '123', 1),
(6051, 'chacha', '20b4ce998d952765019f21d2f516f711', 'siti hanisah ', 'hanisah@sushitei.co.id', 99, 0, 4, 00000000, 1, 'allesya', 1),
(6052, 'happy.nurlia', '20b4ce998d952765019f21d2f516f711', 'Happy Nurlia', 'happy.nurlia@sushitei.co.id', 99, 0, 4, 00000000, 1, 'marketing', 1),
(6053, 'benny', 'e10adc3949ba59abbe56e057f20f883e', 'Head Security', 'head.security@sushitei.co.id', 99, 0, 4, 00000000, 1, 'benny', 1),
(6054, 'dwi kristanto', '827ccb0eea8a706c4c34a16891f84e7b', 'dwi krist', 'headsk2@sushitei.co.id', 99, 0, 4, 00000000, 1, 'mieayam', 1),
(6056, 'henky', '263a0c6924eb8274b8622813f6e35892', 'Henky Pho', 'henky@sushitei.co.id', 99, 0, 4, 00000000, 1, '456', 1),
(6057, 'Novie Hernawati', '6232b8da297873d8bf2c42820f34a377', 'Novie Hernawati', 'hernawati@sushitei.co.id', 99, 0, 4, 00000000, 1, '23111990', 1),
(6059, 'irma', '7c9eb82d818251456962e698fcb338ba', 'irma', 'irma@sushitei.co.id', 99, 0, 4, 00000000, 1, 'irma', 1),
(6060, 'sushiteiEP', '726d76a0ea68e4861ed512448b1eadaa', 'EMPORIUMPLUIT', 'ISA ST-EP', 99, 0, 4, 00000000, 1, 'sushi tei ep', 1),
(6061, 'ISA MKG', '726d76a0ea68e4861ed512448b1eadaa', 'ISA MKG', 'isa.mkg@sushitei.co.id', 99, 0, 4, 00000000, 1, 'selow', 1),
(6062, 'ISABATAM', 'e07370c580022bcf077a8763357d1da3', 'ISABATAM', 'isa1.batam@sushitei.co.id', 99, 0, 4, 00000000, 1, 'salmon', 1),
(6063, 'nida', '17cc53731572f4cbf6eaaa130e9ca10d', 'nida', 'isa1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'ada dech', 1),
(6065, 'ithelpdesk', 'cf203533c13244ec098d7779660f1538', 'IT Help Desk', 'it.helpdesk@sushitei.co.id', 99, 0, 4, 00000000, 1, 'selalu terdepan', 1),
(6071, 'AkhmadRosaddy', 'e10adc3949ba59abbe56e057f20f883e', 'AkhmadRosaddy', 'it5.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'jakarta', 1),
(6072, 'fathur', 'e10adc3949ba59abbe56e057f20f883e', 'Fathur IT', 'it6.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fathur', 1),
(6075, 'julianti', '32309148f9eec5dd2e609f2a827af759', 'Julie', 'julianti@sushitei.co.id', 99, 0, 4, 00000000, 1, 'julianti@sushitei.com', 1),
(6076, 'junaedi', '8da263ed4307545bb90c5b2baf1acf73', 'junaedi', 'junaedi@sushitei.co.id', 99, 0, 4, 00000000, 1, 'warehouse', 1),
(6078, 'kris2', '4f9b7c5a93ce2dc8281a9820a12866e0', 'kris2', 'kris.handaya@sushitei.co.id', 99, 0, 4, 00000000, 1, 'asdf', 1),
(6081, 'sklbs', '4a4c4d9bd8645ee7eb96def18647bae6', 'sklbs', 'lebakbulus@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'sklbs', 1),
(6082, 'bisma', '372a2193710ef88cfe1f28d6cc1a294b', 'Bisma Jaul', 'legal.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'jaul', 1),
(6083, 'liana', '91b9e243dc67e244123617bb49766ad1', 'Liana', 'liana@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6084, 'herwin', '20b4ce998d952765019f21d2f516f711', 'Herwin', 'logistic@sushitei.co.id', 99, 0, 4, 00000000, 1, 'warehouse', 1),
(6085, 'luciana', '20b4ce998d952765019f21d2f516f711', 'Luciana', 'luciana@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6086, 'luciana2', '450336441380591df61c97aa6791b692', 'Finance', 'luciana@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6088, 'skcl', '4a4c4d9bd8645ee7eb96def18647bae6', 'skcl', 'malciputra@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'skcl', 1),
(6089, 'stbx', '20b4ce998d952765019f21d2f516f711', 'stbx', 'manager.bx@sushitei.co.id', 15, 0, 4, 00000000, 1, 'bx', 1),
(6090, 'stcp', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei CP', 'manager.cp@sushitei.co.id', 7, 0, 4, 00000000, 1, '123', 1),
(6091, 'step', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei EP', 'manager.ep@sushitei.co.id', 6, 0, 4, 00000000, 1, '123', 1),
(6092, 'stfs', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei FS', 'manager.fs@sushitei.co.id', 9, 0, 4, 00000000, 1, '123', 1),
(6093, 'stgc', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei GC', 'manager.gc@sushitei.co.id', 8, 0, 4, 00000000, 1, '123', 1),
(6094, 'STGI', '20b4ce998d952765019f21d2f516f711', 'STGI', 'manager.gi@sushitei.co.id', 11, 0, 4, 00000000, 1, '23581808', 1),
(6095, 'stkw', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei KW', 'manager.karawaci@sushitei.co.id', 10, 0, 4, 00000000, 1, '123', 1),
(6096, 'STKG', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei Mall Kelapa Gading', 'manager.kg@sushitei.co.id', 5, 0, 4, 00000000, 1, 'stkg', 1),
(6097, 'st@kk', '20b4ce998d952765019f21d2f516f711', 'st@kk', 'manager.kk@sushitei.co.id', 13, 0, 4, 00000000, 1, 'kota kasablanka', 1),
(6098, 'stkv', '20b4ce998d952765019f21d2f516f711', 'Manager Kemang Village', 'manager.kv@sushitei.co.id', 108, 0, 4, 00000000, 1, 'kemang village', 1),
(6099, 'ST LA', '20b4ce998d952765019f21d2f516f711', 'ST Lotte Shop Avenue', 'manager.la@sushitei.co.id', 12, 0, 4, 00000000, 1, 'terdepan', 1),
(6100, 'stmkg', '20b4ce998d952765019f21d2f516f711', 'stmkg', 'manager.mkg@sushitei.co.id', 5, 0, 4, 00000000, 1, 'yanti', 1),
(6101, 'stpi', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei PI', 'manager.pi@sushitei.co.id', 1, 0, 4, 00000000, 1, 'pi', 1),
(6102, 'stpim', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei PIM', 'manager.pim@sushitei.co.id', 3, 0, 4, 00000000, 1, '123', 1),
(6103, 'Manager PM', '211c42bb02f7a9e63d30e9a12222577c', 'Manager PM', 'Manager.pm@sushitei.co.id', 17, 0, 4, 00000000, 1, '17', 1),
(6104, 'st-ps', '20b4ce998d952765019f21d2f516f711', 'st-ps', 'manager.ps@sushitei.co.id', 2, 0, 4, 00000000, 1, '18000', 1),
(6105, 'stps', '20b4ce998d952765019f21d2f516f711', 'Sushi tei PS', 'manager.ps@sushitei.co.id', 2, 0, 4, 00000000, 1, '123', 1),
(6106, 'stsmb', '20b4ce998d952765019f21d2f516f711', 'stsmb', 'manager.sb@sushitei.co.id', 14, 0, 4, 00000000, 1, 'smb_bisa', 1),
(6107, 'stsc', '20b4ce998d952765019f21d2f516f711', 'Sushi Tei Senci', 'manager.senci@sushitei.co.id', 4, 0, 4, 00000000, 1, '123', 1),
(6108, 'manager.sms@sushitei.co.id', 'e10adc3949ba59abbe56e057f20f883e', 'manager.sms@sushitei.co.id', 'manager.sms@sushitei.co.id', 18, 0, 4, 00000000, 1, 'manager', 1),
(6111, 'michiko', '827ccb0eea8a706c4c34a16891f84e7b', 'Michiko', 'michiko@sushitei.co.id', 99, 0, 4, 00000000, 1, 'marketing', 1),
(6113, 'eva', '20b4ce998d952765019f21d2f516f711', 'eva', 'mkt2.jtk@sushitei.co.id', 99, 0, 4, 00000000, 1, 'eva', 1),
(6114, 'oprst', '211c42bb02f7a9e63d30e9a12222577c', 'Operation Sushi Tei', 'monica@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6115, 'Lia', '827ccb0eea8a706c4c34a16891f84e7b', 'lia', 'mrp1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, '1', 1),
(6116, 'Rudi', '827ccb0eea8a706c4c34a16891f84e7b', 'Rudi', 'mrp2.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'ini saya', 1),
(6117, 'rudi mrp', '12c33b1c30e356053378a5d0328f75a6', 'Rudi MRP', 'mrp2@sushitei.co.id', 99, 0, 4, 00000000, 1, 'ini saya', 1),
(6118, 'nur.erni', '20b4ce998d952765019f21d2f516f711', 'Nur Erni', 'nur.erni@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6119, 'nurul', '20b4ce998d952765019f21d2f516f711', 'Nurul', 'nurul@sushitei.co.id', 99, 0, 4, 00000000, 1, 'hck', 1),
(6120, 'oki', 'e10adc3949ba59abbe56e057f20f883e', 'oki', 'okichan79@yahoo.com', 99, 0, 4, 00000000, 1, 'honda', 1),
(6121, 'fbvst', '211c42bb02f7a9e63d30e9a12222577c', 'Food & Beverage Sushi Tei', 'oktavianus@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6122, 'zukhruf dhuhariah', 'ceada91833bc111992ee798129408343', 'zukhruf', 'operationst.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'dhuhariah', 1),
(6123, 'ekky', '20b4ce998d952765019f21d2f516f711', 'Ekky Hartanti', 'org.dev@sushitei.co.id', 99, 0, 4, 00000000, 1, 'hartanti', 1),
(6124, 'wenty', '7c6a180b36896a0a8c02787eeafb0e4c', 'wenty febrianti', 'payroll.spv@sushitei.co.id', 99, 0, 4, 00000000, 1, 'ho', 1),
(6127, 'ppfbspv2', 'e10adc3949ba59abbe56e057f20f883e', 'ppfbspv2', 'ppfbspv2@sushitei.co.id', 99, 0, 4, 00000000, 1, 'michael chandra', 1),
(6128, 'ari purchasing', '101345ba77f8222dfe153e06123def94', 'ari purchasing', 'purchasing4.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'ari', 1),
(6129, 'skpuri', 'e10adc3949ba59abbe56e057f20f883e', 'Sushi Kiosk PI', 'puriindahmall@sushi-kiosk.com', 100, 0, 4, 00000000, 1, '123', 1),
(6130, 'putra', 'e00b29d5b34c3f78df09d45921c9ec47', 'Putra', 'putra@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'black', 1),
(6131, 'eka', 'ff1d176e38776cbecc4666d95a820d38', 'ecka', 'putraecka20@gmail.com', 99, 0, 4, 00000000, 1, 'my age is 29', 1),
(6132, 'qa.spv@sushitei.co.id', 'e10adc3949ba59abbe56e057f20f883e', 'Anita Gozali', 'qa.spv@sushitei.co.id', 99, 0, 4, 00000000, 1, 'qa', 1),
(6134, 'dhini', '20b4ce998d952765019f21d2f516f711', 'Handayani Dhiniyanti', 'qa.spv4@sushitei.co.id', 99, 0, 4, 00000000, 1, 'qa', 1),
(6135, 'Quality System', '9caffbd9376ee4fbfcbfe8206524a3a9', 'Cholil Anwar', 'qa1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'taman tekno bsd', 1),
(6137, 'QAS', '20b4ce998d952765019f21d2f516f711', 'QAS', 'qa3.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'deasy', 1),
(6139, 'recruitment2.jkt', '20b4ce998d952765019f21d2f516f711', 'Recruitment Team', 'recruitment2.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'hrd', 1),
(6140, 'rika', '32309148f9eec5dd2e609f2a827af759', 'Rika Hertya', 'rika@sushitei.co.id', 99, 0, 4, 00000000, 1, 'rika', 1),
(6141, 'rina', '20b4ce998d952765019f21d2f516f711', 'Rina Maryana', 'rina@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6142, 'amutara', '211c42bb02f7a9e63d30e9a12222577c', 'OM UTARA', 'rizal@sushitei.co.id', 99, 0, 4, 00000000, 1, 'dragon roll', 1),
(6145, 'Sayuti.seniorbar', '239adcca45a13140bf1ab2bd00226747', 'sayuti.seniorbar', 'sayuti.seniorbar@sushitei.co.id', 99, 0, 4, 00000000, 1, '100787', 1),
(6147, 'secretaryGM', 'e10adc3949ba59abbe56e057f20f883e', 'secretaryGM', 'secretary.gm@sushitei.co.id', 99, 0, 4, 00000000, 1, 'marina', 1),
(6148, 'sigit', '20b4ce998d952765019f21d2f516f711', 'sigit', 'sigit@sushitei.co.id', 99, 0, 4, 00000000, 1, 'hrd', 1),
(6149, 'sushi kiosk', '4a4c4d9bd8645ee7eb96def18647bae6', 'sk wtc', 'SK WTC [wtc_2@sushi-kiosk.com]', 99, 0, 4, 00000000, 1, 'ya', 1),
(6151, 'sk.ck2', '20b4ce998d952765019f21d2f516f711', 'Yudi HCK', 'sk.ck2@sushitei.co.id', 99, 0, 4, 00000000, 1, 'hck', 1),
(6152, 'sonya', '32309148f9eec5dd2e609f2a827af759', 'Sonya', 'sonya@sushite.com', 99, 0, 4, 00000000, 1, 'sonya@sushitei.com', 1),
(6153, 'st bx', '726d76a0ea68e4861ed512448b1eadaa', 'adminisa bx', 'st.bx@sushitei.co.id', 99, 0, 4, 00000000, 1, 'sushitei bintaro xchange', 1),
(6156, 'adminfs', '726d76a0ea68e4861ed512448b1eadaa', 'ST Flavour Bliss', 'st.fs@sushitei.co.id', 9, 0, 4, 00000000, 1, 'st flavour bliss', 1),
(6157, 'ppic gc', '827ccb0eea8a706c4c34a16891f84e7b', 'admingc', 'st.gc@sushitei.co.id', 8, 0, 4, 00000000, 1, 'admingc', 1),
(6159, 'ST KW', '726d76a0ea68e4861ed512448b1eadaa', 'ISA ST KW', 'st.karawaci@sushitei.co.id', 10, 0, 4, 00000000, 1, 'sushi tei karawaci', 1),
(6160, 'stkk', '726d76a0ea68e4861ed512448b1eadaa', 'Sushi Tei Kota Kasablanka', 'st.kk@sushitei.co.id', 13, 0, 4, 00000000, 1, 'stkk', 1),
(6161, 'adminla', '726d76a0ea68e4861ed512448b1eadaa', 'AdminLA', 'st.la@sushitei.co.id', 12, 0, 4, 00000000, 1, 'north', 1),
(6162, 'isa', 'fe4d5c2b7df0cc0f6b532df061960a68', 'Admin KG', 'st.mkg@sushitei.co.id', 5, 0, 4, 00000000, 1, '123', 1),
(6163, 'st.kg', '726d76a0ea68e4861ed512448b1eadaa', 'Sushitei Mall Kelapa gading', 'st.mkg1@sushitei.co.id', 5, 0, 4, 00000000, 1, 'dwi', 1),
(6164, 'admin PI', '726d76a0ea68e4861ed512448b1eadaa', 'admin PI', 'st.pi@sushitei.co.id', 99, 0, 4, 00000000, 1, 'sushi tei plaza indonesia', 1),
(6165, 'adminpim', '726d76a0ea68e4861ed512448b1eadaa', 'isapim', 'st.pim@sushitei.co.id', 3, 0, 4, 00000000, 1, 'tejo', 1),
(6166, 'st.puri', '726d76a0ea68e4861ed512448b1eadaa', 'pujiastuti', 'st.pm@sushitei.co.id', 3, 0, 4, 00000000, 1, 'sushitei puri', 1),
(6167, 'adminps', '726d76a0ea68e4861ed512448b1eadaa', 'plasa senayan', 'st.ps@sushitei.co.id', 2, 0, 4, 00000000, 1, 'plasa senayan', 1),
(6168, 'st smb', '726d76a0ea68e4861ed512448b1eadaa', 'Sushi Tei Summarecon Mall Bekasi', 'st.sb@sushitei.co.id', 14, 0, 4, 00000000, 1, 'sushi tei summarecon mall bekasi', 1),
(6170, 'adminsms', '726d76a0ea68e4861ed512448b1eadaa', 'Sushi Tei Summarecon Mall Serpong', 'st.sms@sushitei.co.id', 18, 0, 4, 00000000, 1, 'st summarecon mall serpong', 1),
(6171, 'sttb', '54dd7dce8268b1cc0fc2821bbff85bd0', 'Admin ISA ST TB', 'st.tb@sushitei.co.id', 16, 0, 4, 00000000, 1, '25', 1),
(6172, 'st gc', '726d76a0ea68e4861ed512448b1eadaa', 'stgandariacity', 'st_gc@sushitei.co.id', 8, 0, 4, 00000000, 1, 'gandaria city', 1),
(6175, 'tax2.jkt', '20b4ce998d952765019f21d2f516f711', 'Devi Tax', 'tax2.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6176, 'tax.3', '20b4ce998d952765019f21d2f516f711', 'Tiya Tax', 'tax3.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'fat', 1),
(6177, 'theresia', '47a7e8e62cd022f99248bc159ea208cf', 'theresia', 'theresia@sushitei.co.id', 99, 0, 4, 00000000, 1, 'tereaja', 1),
(6178, 'Toya', 'de92bc7ed4e6af9c47401fc87a9e2a96', 'Toya San', 'toya@sushitei.co.id', 99, 0, 4, 00000000, 1, 'teh tarik', 1),
(6179, 'sugeng widodo', '20b4ce998d952765019f21d2f516f711', 'Sugeng Widodo', 'training.spv2@sushitei.co.id', 99, 0, 4, 00000000, 1, 'training', 1),
(6181, 'marst', '211c42bb02f7a9e63d30e9a12222577c', 'Marketing Sushi Tei', 'vera.kusliawan@sushitei.co.id', 99, 0, 4, 00000000, 1, '123', 1),
(6183, 'warehouse.admin1', '20b4ce998d952765019f21d2f516f711', 'Rianti', 'warehouse.admin1@sushitei.co.id', 99, 0, 4, 00000000, 1, 'icw', 1),
(6184, 'warehouse.admin2', '20b4ce998d952765019f21d2f516f711', 'Wahyu BSD', 'warehouse.admin2@sushitei.co.id', 99, 0, 4, 00000000, 1, 'warehouse', 1),
(6186, 'warehouse BSD', 'f0d2b404929238d60918a99d1ac2f46f', 'Fazriah Febriani', 'warehouse.admin4@sushitei.co.id', 99, 0, 4, 00000000, 1, 'febi', 1),
(6187, 'warehouse', 'ca2c0877ef674bd5f36d2f149d5070ae', 'Nuraini Oktavia', 'warehouse.admin5@sushitei.co.id', 99, 0, 4, 00000000, 1, 'warehousebsd', 1),
(6188, 'warehousebsd', 'e2f75d06eab6765990ef8cc7c7911385', 'nazila', 'warehouse.admin6@sushitei.co.id', 99, 0, 4, 00000000, 1, 'nayla athalla dennyza', 1),
(6189, 'hermansyah', '7341ccc47f5cda607c5e477e5eda9b23', 'hermansyah', 'warehouse@sushitei.co.id', 99, 0, 4, 00000000, 1, '1', 1),
(6191, 'SK_WTC', 'b94a30e7f6c59728c83021bd23b847b7', 'SKWTC', 'wtc_2@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'wtc', 1),
(6197, 'yonasoctora', '3f84287d3541c464952805a0ce31c9d8', 'Yonas Octora', 'yonasoctora@gmail.com', 99, 0, 4, 00000000, 1, 'hck', 1),
(6198, 'Dora', '20b4ce998d952765019f21d2f516f711', 'EEC & Talent Management', 'yuli.ratna@sushitei.co.id', 99, 0, 4, 00000000, 1, 'selalu terdepan', 1),
(6199, 'yunda', '20b4ce998d952765019f21d2f516f711', 'Yunda', 'yunda@sushitei.co.id', 99, 0, 4, 00000000, 1, 'warehouse', 1),
(6366, 'rahmat.s', 'd43548c6fa6296bed711a436e391eb5e', 'Rahmat Tes', 'it1.jkt@sushitei.co.id', 99, 0, 7, 00009311, 1, 'inusan', 1),
(6425, 'michael.test', 'e10adc3949ba59abbe56e057f20f883e', 'Michael Chandra', 'm.chandra@sushitei.co.id', 99, 0, 4, 00000000, 2, 'doraemon', 1),
(6432, 'headsk', 'e10adc3949ba59abbe56e057f20f883e', 'Head Stock', 'headsk@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 0),
(6433, 'head.sk2', 'e10adc3949ba59abbe56e057f20f883e', 'Head Stock 2', 'headsk2@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(6434, 'head.sk3', 'e10adc3949ba59abbe56e057f20f883e', 'Head Stock 3', 'headsk3@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(6435, 'ppfb.spv2', 'e10adc3949ba59abbe56e057f20f883e', 'Nurvenda Lafian', 'ppfbspv2@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(6436, 'head.sk1', 'e10adc3949ba59abbe56e057f20f883e', 'Head Stock 1', 'headsk@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(6437, 'head.sk4', 'e10adc3949ba59abbe56e057f20f883e', 'Head Stock 4', 'headsk4@sushitei.co.id', 99, 0, 8, 00000000, 0, '', 1),
(6438, 'levi', 'e10adc3949ba59abbe56e057f20f883e', 'Levi', 'levi@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 0),
(6439, 'maria', 'e10adc3949ba59abbe56e057f20f883e', 'Maria', 'maria@sushitei.co.id', 99, 0, 11, 00000000, 0, '', 1),
(6440, 'ninarica', 'cfc49924874504a8cb259a77ed9181fd', 'Nina', 'ga2.jkt@sushitei.co.id', 99, 0, 11, 00000000, 0, '', 1),
(6441, 'marketing', '8c908f85eaafc5d53e98d5a9b1a5dcbd', 'Marketing Kece', 'happy.nurlia@sushitei.co.id', 99, 0, 4, 00000000, 3, 'happy', 1),
(6442, 'mery', 'e10adc3949ba59abbe56e057f20f883e', 'Mery', 'fbspv@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(6443, 'any.puspa', '6fab034efe82b1aa05a07d3cb6bbd6cc', 'Any Puspa', 'any@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(6444, 'BennyHariyanto', 'aa9f5b706fe6f2efe6a84cf9bb6efb34', 'Benny Hariyanto', 'intauditspv1.jkt@sushitei.co.id', 99, 0, 4, 00000000, 4, 'mie ayam', 1),
(6445, 'indra', 'e10adc3949ba59abbe56e057f20f883e', 'Indra', 'indra@sushitei.co.id', 99, 0, 4, 00000000, 0, '', 1),
(6446, 'kenesintan', '0e646ac433eee091db9e4b434a04ab11', 'Kenes Intan', 'intaudit3.jkt@sushitei.co.id', 99, 0, 4, 00000000, 4, 'es krim', 1),
(6447, 'IMAM', 'e3aac40a8e1d65900556839c5121e83a', 'IMAM', 'spv.kopber2@gmail.com', 99, 0, 4, 00000000, 4, 'sushie', 1),
(6448, 'febriarbi', '24d432db8fa99754d3f134398f00e6ef', 'Febriansyah Arbi', 'costcontrol3.jkt@sushitei.co.id', 99, 0, 4, 00000000, 1, 'HO', 1),
(6449, 'wahyuiad', 'f6727d02a891fb5c88de08713a485e9e', 'wahyu', 'costcontrol4.jkt@sushitei.co.id', 99, 0, 4, 00000000, 2, 'adit', 1),
(6450, 'abrar1', '177849a379bc4d3296e617dd5e975ac8', 'abrar', 'intaudit2.jkt@sushitei.co.id', 1, 0, 4, 00000000, 2, 'abrartampan', 1),
(6451, 'MKTputra', 'b3d7f164ab53cf83d0bca0011a146382', 'putra', 'putra@sushi-kiosk.com', 99, 0, 4, 00000000, 1, 'PIM ST', 1),
(6452, 'kokoko', 'e10adc3949ba59abbe56e057f20f883e', 'kokokokok', 'kokok@gmail.com', 99, 0, 4, 00000000, 0, '', 1),
(6453, 'finaaj', 'e10adc3949ba59abbe56e057f20f883e', 'Fina Alfiatul J', 'it6.jkt@sushitei.co.id', 101, 0, 8, 00000000, 0, '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_groups`
--

CREATE TABLE `user_groups` (
  `group_id` smallint(2) UNSIGNED NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `level` smallint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_groups`
--

INSERT INTO `user_groups` (`group_id`, `group_name`, `level`) VALUES
(1, 'Super Admin', 99),
(2, 'Administrator', 98),
(3, 'MRP Engineer', 5),
(4, 'Ticket Users', 1),
(5, 'Admin MRP', 10),
(6, 'Area Manager', 90),
(7, 'Admin View', 91),
(8, 'Admin QA', 10),
(9, 'PQNC Engineer', 5),
(10, 'Admin IT', 10),
(11, 'Admin GA', 10);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_admin`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `view_admin` (
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `view_ticket`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `view_ticket` (
`ticket_id` mediumint(11) unsigned
,`ticket_group` mediumint(11)
,`status` tinyint(1)
,`link_id` mediumint(6) unsigned
,`category_id` smallint(5) unsigned
,`category_name` text
,`point` decimal(3,0) unsigned
,`store` smallint(3)
,`store_name` varchar(30)
,`default_point` smallint(3) unsigned
,`creator` smallint(4) unsigned
,`creator_name` varchar(50)
,`prp_date` date
,`submit_date` date
,`last_update` datetime
,`finding_images` text
,`resolved_images` text
,`resolved_date` date
,`resolved_solution` text
,`area_id` smallint(5)
,`area` varchar(50)
,`notes` text
,`submit_image` varchar(50)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `view_admin`
--
DROP TABLE IF EXISTS `view_admin`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_admin`  AS  select `store`.`store_id` AS `store_id`,`store`.`store_name` AS `store_name`,`store`.`default_point` AS `default_point`,`ticket_main`.`ticket_id` AS `ticket_id`,`ticket_main`.`category` AS `category_id`,`ticket_category`.`category_name` AS `category_name`,`ticket_category`.`point_deduction` AS `point`,`user`.`user_id` AS `user`,`user`.`display_name` AS `user_name`,`assigned`.`user_id` AS `staff`,`assigned`.`display_name` AS `staff_name`,`creator`.`user_id` AS `creator`,`creator`.`display_name` AS `creator_name`,`ticket_main`.`submit_date` AS `submit_date`,`ticket_main`.`last_update` AS `last_update`,`ticket_main`.`resolved_date` AS `resolved_date`,`ticket_main`.`resolved_solution` AS `resolved_solution`,`ticket_main`.`area` AS `area`,`ticket_main`.`notes` AS `notes` from (((((`store` left join `ticket_main` on((`store`.`store_id` = `ticket_main`.`store`))) left join `ticket_category` on((`ticket_main`.`category` = `ticket_category`.`category_id`))) left join `users` `assigned` on((`ticket_main`.`assigned_staff` = `assigned`.`user_id`))) left join `users` `user` on((`ticket_main`.`ticket_user` = `user`.`user_id`))) left join `users` `creator` on((`ticket_main`.`ticket_creator` = `creator`.`user_id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `view_ticket`
--
DROP TABLE IF EXISTS `view_ticket`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ticket`  AS  select `ticket_main`.`ticket_id` AS `ticket_id`,`ticket_main`.`ticket_group` AS `ticket_group`,`ticket_main`.`status` AS `status`,`area_category_link`.`link_id` AS `link_id`,`ticket_main`.`category` AS `category_id`,`ticket_category`.`category_name` AS `category_name`,`ticket_category`.`point_deduction` AS `point`,`ticket_main`.`store` AS `store`,`store`.`store_name` AS `store_name`,`store`.`default_point` AS `default_point`,`creator`.`user_id` AS `creator`,`creator`.`display_name` AS `creator_name`,`ticket_main`.`prp_date` AS `prp_date`,`ticket_main`.`submit_date` AS `submit_date`,`ticket_main`.`last_update` AS `last_update`,`ticket_main`.`finding_images` AS `finding_images`,`ticket_main`.`resolved_images` AS `resolved_images`,`ticket_main`.`resolved_date` AS `resolved_date`,`ticket_main`.`resolved_solution` AS `resolved_solution`,`area`.`area_id` AS `area_id`,`area`.`area_name` AS `area`,`ticket_main`.`notes` AS `notes`,`si`.`upload_name` AS `submit_image` from ((((((`ticket_main` left join `area_category_link` on((`area_category_link`.`link_id` = `ticket_main`.`category`))) left join `ticket_category` on((`area_category_link`.`category` = `ticket_category`.`category_id`))) left join `users` `creator` on((`ticket_main`.`ticket_creator` = `creator`.`user_id`))) left join `store_area` `area` on((`area`.`area_id` = `area_category_link`.`area`))) left join `store` on((`ticket_main`.`store` = `store`.`store_id`))) left join `ticket_upload` `si` on((`si`.`ticket_id` = `ticket_main`.`ticket_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `area_category_link`
--
ALTER TABLE `area_category_link`
  ADD PRIMARY KEY (`link_id`);

--
-- Indeks untuk tabel `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indeks untuk tabel `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`store_id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indeks untuk tabel `store_area`
--
ALTER TABLE `store_area`
  ADD PRIMARY KEY (`area_id`);

--
-- Indeks untuk tabel `ticket_category`
--
ALTER TABLE `ticket_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `category_parent_id` (`area_id`);

--
-- Indeks untuk tabel `ticket_group`
--
ALTER TABLE `ticket_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indeks untuk tabel `ticket_log`
--
ALTER TABLE `ticket_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `ticket_id` (`ticket`);

--
-- Indeks untuk tabel `ticket_main`
--
ALTER TABLE `ticket_main`
  ADD PRIMARY KEY (`ticket_id`,`category`,`ticket_creator`);

--
-- Indeks untuk tabel `ticket_priority`
--
ALTER TABLE `ticket_priority`
  ADD PRIMARY KEY (`priority_id`);

--
-- Indeks untuk tabel `ticket_status`
--
ALTER TABLE `ticket_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indeks untuk tabel `ticket_type`
--
ALTER TABLE `ticket_type`
  ADD PRIMARY KEY (`type_id`),
  ADD UNIQUE KEY `type_id` (`type_id`);

--
-- Indeks untuk tabel `ticket_upload`
--
ALTER TABLE `ticket_upload`
  ADD PRIMARY KEY (`upload_id`),
  ADD KEY `ticket_id` (`ticket_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `outlet` (`store_id`),
  ADD KEY `store_id` (`store_id`);

--
-- Indeks untuk tabel `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `area_category_link`
--
ALTER TABLE `area_category_link`
  MODIFY `link_id` mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT untuk tabel `location`
--
ALTER TABLE `location`
  MODIFY `location_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `store`
--
ALTER TABLE `store`
  MODIFY `store_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT untuk tabel `store_area`
--
ALTER TABLE `store_area`
  MODIFY `area_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `ticket_category`
--
ALTER TABLE `ticket_category`
  MODIFY `category_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT untuk tabel `ticket_group`
--
ALTER TABLE `ticket_group`
  MODIFY `group_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `ticket_log`
--
ALTER TABLE `ticket_log`
  MODIFY `log_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ticket_main`
--
ALTER TABLE `ticket_main`
  MODIFY `ticket_id` mediumint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=415;

--
-- AUTO_INCREMENT untuk tabel `ticket_priority`
--
ALTER TABLE `ticket_priority`
  MODIFY `priority_id` smallint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `ticket_status`
--
ALTER TABLE `ticket_status`
  MODIFY `status_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ticket_type`
--
ALTER TABLE `ticket_type`
  MODIFY `type_id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ticket_upload`
--
ALTER TABLE `ticket_upload`
  MODIFY `upload_id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6454;

--
-- AUTO_INCREMENT untuk tabel `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `group_id` smallint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `storeLink` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
