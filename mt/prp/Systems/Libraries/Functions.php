<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
    if(file_exists(__CONFIGS_DIR .__BASE_CONFIG_FILE)){
        $__config = array();
        require_once(__CONFIGS_DIR .__BASE_CONFIG_FILE);
    }else{
        exit ('Configuration File Cannot Be Found');
    }

    function getConfig($item)
    {
        global $_config;
        $data = $_config[$item];
        return $data;
    }

    function siteUrl()
    {
        return getConfig('base_url');
    }
    
    function controllerUrl($controller,$action=null,$custom=null)
    {
        if (empty($action)) {
            $action = getConfig('default_action');
        }
        #ugly
        $url = siteurl() .getConfig('index_page') .'?'.__CONTROLLER_COMMAND.'='.$controller.'&'.__ACTION_COMMAND.'='.$action;
        #beauty
        #$url = siteurl() .getConfig('index_page') ."?/$controller/$action";
        if(is_array($custom)){
            foreach($custom as $key => $value){
                #ugly
                $url .= "&{$key}={$value}";
                #beauty
                #$url .= "/{$key}/{$value}";
            }
        }
        return $url;
    }
	
    function redirect($controller,$view,$exit=1)
    {
        if ($exit == 1) {
            header('Location: '.controllerUrl($controller,$view));
            exit;
        } else {
            header('Location: '.controllerUrl($controller,$view));
        }
    }
    
?>