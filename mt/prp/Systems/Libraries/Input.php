<?php
/**
* Sister input validation
*/
class Input
{
    protected $_error = array();
    protected $_input = array();
    protected $form = array();
    
    public function validate() /* Validating the input */
    {
        foreach($this->_input as $input){
            /* Split Rules String to Check if there is '=' separator */
            $newrules = explode('=',$input['rules']);

            if (count($newrules) > 1){ 
                if (($newrules[0] == 'min' && strlen($input['value']) < $newrules[1]) || /* if the rules contain 'min' */ 
                    ($newrules[0] == 'max' && strlen($input['value']) > $newrules[1]) || /* if the rules contain 'max' */ 
                    ($newrules[0] == 'like' && $input['value'] != $newrules[1])){  /* if the rules contain 'like' */ 
                    $this->_error[$input['name']] = $input['error'];
                }
            } else {
                $rules = $this->getRules($input['rules']);
                if (!empty($rules)) {
                    if(!preg_match($rules, $input['value'])){
                        $this->_error[$input['name']] = $input['error'];
                    }
                } else {
                    exit("Rule {$input['rules']} not found. exitting.");
                }
            }
        }
        if (count($this->_error) > 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
    protected function getRules($rules)
    {
        switch ($rules) {
            case 'alpha_numeric': /* Alphanumeric format ex: ABC123 */
                $rules = '/^[A-Za-z0-9]+$/';
            break;
            case 'alpha_numeric_sp'; /* Alpha numeric with space ex: ABC 123!! */
                $rules = '/^[A-Za-z0-9 ]+$/';
            break;
            case 'username': /* Alphanumeric format ex: ABC123 */
                $rules = '/^[A-Za-z0-9.\-^&*]+$/';
            break;
            case 'email': /* Email format example@example.com */
                $rules = '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/';
            break; /* Numeric format ex: 12345 */
            case 'numeric':
                $rules = '/^[0-9]*$/';
            break;
            case 'boolean':
                $rules = '/^[0-1]*$/';
            break;
            case 'date': /* Date format YYYY-MM-DD */
                $rules = '/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/';
            break;
            case 'alpha_numeric_sc'; /* Alpha numeric with special character ex: ABC_123!! */
                $rules = '/^[A-Za-z0-9_ ,?~;\/\-!@#\$%\^&*\(\)]+$/';
            break;
            case 'file_name'; /* File name, only accept A-z 0-9 .- */
                $rules = '/^[A-Za-z0-9_ .\-]+$/';
            break;
			case 'textarea_no_html'; /* Text area without HTML Tag!! */
                $rules = '/^[a-zA-Z0-9?$@#()\'!,+\-=_:.&€£*%\s]+$/';
            break;

            case 'phone': /* Phone number format XXX-XXX-XXXX */
                $rules = '/^[0-9]{3}-|\s[0-9]{3}-|\s[0-9]{4}$/';
            break;
            case 'hour': /* Phone number format XXX-XXX-XXXX */
                $rules = '/(2[0-3]|[01][0-9]):([0-5][0-9])/';
            break;
            default:
                $rules = '';
        }
        return $rules;
    }
    
    public function addValidation($name, $value, $rules, $error)
    {
        if(is_array($rules)){
            foreach($rules as $rule){
                array_push($this->_input, array('name' => $name, 'value' => $value, 'rules' => $rule, 'error' => $error));
            }
            
        }else{
            array_push($this->_input, array('name' => $name, 'value' => $value, 'rules' => $rules, 'error' => $error));
        }

    }
    
	protected function cleanInput($str) {
		return filter_var(filter_var(trim(urldecode($str)), FILTER_SANITIZE_STRING), FILTER_SANITIZE_SPECIAL_CHARS);
	}
    
    public function cleanInputArray($arr) {
        $res = array();
        foreach($arr as $key=>$val){
            $res[$key] = $this->cleanInput($val);
        }
		return $res;
	}

    public function showError($name='')
    {
        if(isset($this->_error[$name]) && !empty($this->_error[$name])){
            return $this->_error[$name];
        }else{
            return $this->_error;
        }
    }
    
    public function form($array)
    {
        if(!empty($_POST[$array])){
            return $this->cleanInput($_POST[$array]);
        }else{
            return false;
        }
    }
    
}