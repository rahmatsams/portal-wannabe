            <div class="card-deck">
            <div class="card">
              <div class="card-header">
                  <strong class="card-title">PRP Detail</strong>
              </div>
              <div class="card-body">
                <form id="searchPRP" class="forms-sample">
                    <div class="form-group">
                      <label for="selectMonth">Month</label>
                      <input type="month" name="date" class="form-control p-input" id="selectMonth" aria-describedby="emailHelp" placeholder="Select Month" value="<?=date("Y-m")?>">
                    </div>
                </form>
                <div class="table-responsive">
                  <table id="indexContent" class="table center-aligned-table table-striped">
                    <thead>
                      <tr class="text-primary">
                        <th>No</th>
                        <th style="min-width: 180px;">Outlet</th>
                        <th>Total Finding</th>
                        <th>Rekomendasi</th>
                        <th>Pengurangan</th>
                        <th>Nilai Saat ini</th>
                        <th>Email</th>
                      </tr>
                    </thead>
                    <tbody>
						<?php
							if(is_array($result) && count($result) > 0){
								$i=1;
								foreach($result as $d){
									$c = $d['default_point'] - $d['group_point'];
                  $s = ($d['group_status'] == 0) ? '<button class="btn btn-sm btn-danger">Not Sent</button>' : '<button class="btn btn-small btn-success">Sent</button>';
									echo "
                    <tr class=''>

							      <td>{$i}</td>
							      <td>".(isset($d['group_id']) ? "<a href=\"view_prp_{$d['group_id']}.html\" class='btn btn-primary btn-block'>{$d['store_name']}</a>" : $d['store_name'])."</td>
							      <td>{$d['group_finding']}</td>
                    <td>{$d['group_recommendation']}</td>
							      <td>{$d['group_point']}</td>
							      <td>{$c}</td>
                    <td>{$s}</td>

						        </tr>";
								$i++;
								}
							}
						?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>