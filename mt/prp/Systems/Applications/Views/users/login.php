<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    
    <title>Login - <?=getConfig('site_name')?></title>
    
    
    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="Resources/assets/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="Resources/assets/bootstrap-4/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="Resources/assets/web-fonts-with-css/css/fontawesome-all.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="Resources/images/Ico/favicon.png">
    <style>
    .loader {
         border-top: 16px solid blue;
         border-right: 16px solid green;
         border-bottom: 16px solid red;
         border-left: 16px solid pink;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    </style>
</head>
<!-- [endif]
<body style="background: url('Resources/images/bg.jpg'); background-size: cover;">
-->
<body>
    <div class="container">
		<div class="row">
			<div class="container-fluid col-lg-4">
				<div class="main border rounded border-info" style="margin-top: 20px; padding: 20px;">
					<h1 class="form-signin add-margin-bot well">QA - PRP</h1>
					<form class="form-signin well" id="loginForm">
						
						<div class="form-group" id="userName">
							<label class="control-label sr-only" for="inputName">Please check your input.</label>
							<input name="last_url" type="hidden" id="lastUrl" class="form-control" value="<?=(isset($session['last_url']) ? $session['last_url'] : '')?>">
							<input name="ticket_email" type="text" id="inputName" class="form-control" placeholder="Nama User" maxlength="15" required autofocus>
						</div>
						<div class="form-group">
							<label for="inputPassword" class="sr-only">Password</label>
							<input name="ticket_password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
						</div>
                        <div class="form-group <?=$session['attempt'] > 5 ? '' : 'd-none'?>" id="recaptchaDiv">
						</div>
                        
						<div class="form-group">
							<button class="btn btn-lg btn-primary btn-block" name="login" <?=$session['attempt'] > 5 ? 'disabled' : ''?>>Login <i id="loginSpinner" class="fa fa-spinner fa-spin d-none" style="font-size:24px"></i></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="loader d-none"></div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="Resources/assets/jquery/jquery-1.12.3.min.js"></script>
    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
    <script src="Resources/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script src="Resources/js/login.js"></script>
    <script>
        var onloadCallback = function() {
            grecaptcha.render('recaptchaDiv', {
                'sitekey': '6LemwyETAAAAACZCN9RkUJ1D4j_bxeBvu1lgmAZP',
                'callback' : verifyCallback
            });
        };
        var verifyCallback = function (response) {
            if (response.length == 0) { 
            
            }//reCaptcha not verified
            else {
                $("#loginForm button[name='login']").removeAttr("disabled");
            }//reCaptch verified      
        };
        $(document).ready(function(){
            $("#loginForm").submit(function(event){
                // cancels the form submission
                event.preventDefault();
                var me = $(this);
                var forms = $("#loginForm");
                $("#loginSpinner").toggleClass("d-none");
                loginForm(me, forms);
            });
        });
    </script>
</body>
</html>
