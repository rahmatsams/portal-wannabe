        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="administrator.html">Administrator</a></li>
                <li class="breadcrumb-item"><a href="admin_category.html">Manage PRP</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$result['area_name']?></li>
            </ol>
        </nav>
        <?php
            if(is_array($fdata) && count($fdata) > 0){
                if($fdata['success'] == 1){
                    echo '<div class="p-3 mb-2 bg-success text-white">Category Updated Successfully !!</div>';
                }else{
                    echo '<div class="p-3 mb-2 bg-danger text-white">Please check your input !!</div>';
                }
            }
        ?>
        <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4"> 
                <div class="card-header">
                    <strong class="card-title">Manage PRP</strong>
                </div>
                <div class="card-body">
                    <div class="card-deck">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Edit Detail</strong>
                            </div>
                            <div class="card-body p-0 pt-3 pb-3">
                                <form id="editcategory" method="POST" action="admin_edit_category_<?=$result['area_id']?>.html">
                                    <div class="col-md-12 form-group">
                                        <label>Area</label>
                                        <input id="catid" name="area_id" type="hidden" value="<?=$result['area_id']?>" class="form-control" maxlength="6" required>
                                        <input id="catname" name="area_name" type="text" value="<?=$result['area_name']?>" class="form-control" maxlength="25" required>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Description</label>
                                        <textarea id="catdesc" name="area_desc" cols="20" rows="3" class="form-control"><?=$result['area_desc']?></textarea>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input type="submit" name="action" value="Update" class="btn btn-sm btn-primary">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Add Point</strong>
                            </div>
                            <div class="card-body p-0 pt-3 pb-3">
                                <form id="addsubcategory" method="POST" action="admin_edit_subcategory.html">
                                    <div class="col-md-12 form-group">
                                        <label for="insertCatName">Point</label>
                                        <input id="catid" name="area_id" type="hidden" value="<?=$result['area_id']?>" class="form-control" maxlength="6" required>
                                        <textarea name="category_name" id="insertCatName" class="form-control" cols="70" rows="3" required></textarea>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="insertPoint">Point Deduction</label>
                                        <input type="number" name="point_deduction" id="insertPoint" class="form-control" required>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input type='submit' class='btn btn-sm btn-primary' name="action" value="Add Point">
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="card-header">
                    <strong class="card-title">Manage Point</strong>
                </div>
                <div class="card-body">
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Point</th> 
                                <th>Point Deduction</th>
                                <th colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(count($list) > 0){
                                foreach($list as $result){
                                    echo "
                                        <tr class='form-group' id='formEdit{$result['category_id']}'>
                                            <form method='POST' action='admin_edit_subcategory_{$result['category_id']}.html'>
                                                <td>
                                                    <input type='hidden' name='category_id' value='{$result['category_id']}' required>
                                                    <input type='hidden' name='area_id' value='{$result['area_id']}' readonly>{$result['category_id']}
                                                </td>
                                                <td>
                                                    <textarea name='category_name' cols='70' rows='3' readonly class='form-control-plaintext' required>{$result['category_name']}</textarea>
                                                </td>
                                                <td>
                                                    <input type='number' name='point_deduction' readonly class='form-control-plaintext' value='{$result['point_deduction']}' required>
                                                </td>
                                                <td>
                                                    <a href=\"admin_edit_subcategory_{$result['category_id']}.html\" class='manage-button' data-id='{$result['category_id']}'><button type=\"button\" class=\"btn btn-success btn-sm btn-block\">Edit</button></a><input type='submit' name='action' class='btn btn-sm btn-primary d-none' value='Edit'>
                                                </td>
                                                <td>
                                                    <a href=\"#\" class=\"delete_sub deleteconfirmationmodal\" data-id=\"{$result['category_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"btn btn-danger btn-sm btn-block\">Hapus</button></a>
                                                </td>
                                            </form>
                                        </tr>
                                        ";
                                }
                            }else{
                                echo '
                            <tr>
                                <td colspan="6" align="center">No data</td>
                            </tr>';
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            <!-- DELETE SUB CATEGORY -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">Hapus Sub Category</h5>
                                    </div>
                                    <div class="modal-body">
                                        Apa kamu yakin ingin menghapus Sub Category ini?
                                    </div>
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <!-- id="admin_recom_delete" untuk jquery-->    
                                        <a id="admin_subcategory_delete" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
