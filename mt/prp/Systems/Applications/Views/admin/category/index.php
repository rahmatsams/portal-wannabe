		<h3 class="page-heading mb-4">Manage PRP</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="administrator.html">Administrator Page</a></li>
            <li class="breadcrumb-item active"><a href="admin_category.html">Manage PRP</a></li>
        </ol>
		<div class="table-responsive">
            <h3>Main Category</h3>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Category Name</th>
                        <th>Description</th>
                        <th>Point Deduction</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                
                <tbody id="listTable">
                    <?php
                        if(is_array($store_area) && !empty($store_area)){
                            $total = 0;
                            $num = 1;
                            foreach($store_area as $result){
                                        
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['area_name']}</td>
                        <td>{$result['area_desc']}</td>
                        <td>{$result['points']}</td>
                        <td><a href='admin_edit_storearea_{$result['area_id']}.html'><button type=\"button\" class=\"btn btn-success btn-sm btn-block\">Edit</button></a></td>
                        <td>
                            <a href=\"#\" class=\"delete_cat deleteconfirmationmodal\" data-id=\"{$result['area_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"btn btn-danger btn-sm btn-block\">Hapus</button></a>
                        </td>
                    </tr>";
                                $num++;
                                $total = $total+$result['points'];
                            }
                            echo "
                    <tr>
                        <td colspan='3'><b>Total<b></td>
                        <td colspan='3'>{$total}</td>                        
                    </tr>";
                        } else {
                    ?>
                    <tr>
                        <td colspan="3">No Record</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
               
            </table>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2">Add Main Category</button>
            
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Main Category</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12 well">
                                <div class="row">
                                    <form id="addcategory" method="POST" action="index.html?s=admin&&i=createCategory">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Category Name</label>
                                                <input name="area_name" type="text" value="" class="form-control" maxlength="25" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea name="area_desc" cols="20" rows="4" class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group" hidden="hidden">
                                                <label>Category Status</label>
                                                <input name="area_status" type="text" value="1" class="form-control" maxlength="25" required>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" name="submit" value="kirim" class="btn btn-sm btn-primary">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                 </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- DELETE REKOMENDASI -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">Hapus Category</h5>
                                    </div>
                                    <div class="modal-body">
                                        Apa kamu yakin ingin menghapus Category ini?
                                    </div>
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <!-- id="admin_recom_delete" untuk jquery-->    
                                        <a id="admin_category_delete" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <!-- JQUERY DELETE CATEGORY -->