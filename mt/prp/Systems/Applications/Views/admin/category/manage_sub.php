	<div class="container-fluid">
        <h2 class="sub-header">Edit Sub Kategori</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="administrator.html">Administrator</a></li>
            <li class="breadcrumb-item"><a href="admin_category.html">Manage Category</a></li>
            <li class="breadcrumb-item"><a>Manage Sub Category</a></li>
        </ol>
		<div class="table-responsive">
            <div class="col-md-12 well">
                <div class="row">
                    <form id="editcategory" method="POST" action="admin_edit_subcategory_<?=$result['category_id']?>.html">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Point</label>
                                <input id="catid" name="category_id" type="hidden" value="<?=$result['category_id']?>" class="form-control" maxlength="6" required>
                                <input id="catid" name="area_id" type="hidden" value="<?=$result['area_id']?>" class="form-control" maxlength="6" required>
                                <textarea id="catname" name="category_name" cols="50" rows="7" class="form-control" required><?=$result['category_name']?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Point Deduction</label>
                                <input id="catid" name="point_deduction" type="number" value="<?=$result['point_deduction']?>" class="form-control" maxlength="6" required>
                            </div>

                            <div class="form-group">
                                <input type="submit" name="action" value="Edit" class="btn btn-sm btn-primary">
                            </div>
                        </div>
                    </form>
                 </div> 
            </div>
        </div>
    </div>