                    <?php
                        $img_url = (!empty($last_data['category_images']) && file_exists("Resources/images/Categories/{$last_data['category_images']}")) ? "Resources/images/Categories/{$last_data['category_images']}" : "Resources/images/Categories/not_found.jpg";   
                    ?>
					<h2>Edit Product Categories</h2>
					<form method="POST" action="<?php echo controllerUrl('AdminCategory','edit_category'); ?>" enctype="multipart/form-data">
						<input type="hidden" name="category_number" value="<?php echo $last_data['category_id']; ?>">
						<input type="hidden" name="last_images" value="<?php echo $last_data['category_images']; ?>">
						<table border="1" id="event-form">
								<tr>
									<td>Category Name</td>
									<td>:</td>
									<td><input type="text" name="category_name" value="<?php  echo $last_data['category_name']; ?>" /></td>
								</tr>
								<tr>
									<td>Parent Category</td>
									<td>:</td>
									<td>
										<select name="category_parents">
											<option value="0">None</option>
											<?php
												foreach($category_list as $category_data){
													if(is_array($category_data)){
														echo "\t\t\t\t\t\t\t\t\t\t\t";
														echo '<option value="'.$category_data['category_id'].'" '.($last_data['category_parents'] == $category_data['category_parents'] ? 'selected' : '').'>'.$category_data['category_name'].'</option>';
														echo "\n";
													}
												}
											?>
										</select>
									</td>
								</tr>
								<tr>
									<td class="input-column">Category Image</td>
									<td class="input-column">:</td>
									<td class="input-column"><img src="<?php echo $img_url; ?>" height="100" width="100" alt="Category images"/><input type="file" name="category_images"/></td>
								</tr>
								<tr>
									<td>Category Description</td>
									<td>:</td>
									<td><textarea name="category_description" rows="15" cols="80" style="width: 60%" class="tiny_mce"><?php echo $last_data['category_desc']; ?></textarea></td>
								</tr>
                                <tr>
									<td>Published</td>
									<td>:</td>
									<td><input type="checkbox" name="publish" value="1" <?php echo ($last_data['published'] == 1 ? 'checked="checked"' : '') ?>/></td>
								</tr>
                                <tr>
									<td>Front Page</td>
									<td>:</td>
									<td><input type="checkbox" name="front" value="1" <?php echo ($last_data['front'] == 1 ? 'checked="checked"' : '') ?>/></td>
								</tr>
								<tr>
									<td colspan="3"><input type="submit" name="edit" value="Edit"></td>
								</tr>
							</table>
					</form>
					<p><span style="font-size: 80%; font-style: italic;">*For 'None' Category, Please upload an image file.</span></p>
                    <script type="text/javascript">
                        
                        //<![CDATA[
                            jQuery(document).ready(function(){
                                
                                jQuery('.tiny_mce').each(function(){
                                    $(this).tinymce({
                                        // Location of TinyMCE script
                                        script_url : 'Resources/js/tiny_mce/jquery.tinymce.js',

                                        // General options
                                        theme : "advanced",
                                        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

                                        // Theme options
                                        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                                        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                                        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                                        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                                        theme_advanced_toolbar_location : "top",
                                        theme_advanced_toolbar_align : "left",
                                        theme_advanced_statusbar_location : "bottom",
                                        theme_advanced_resizing : true,

                                    });
                                });
                            });   
                        //]]>
                    </script>  