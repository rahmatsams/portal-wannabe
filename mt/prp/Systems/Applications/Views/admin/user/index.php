    
        <h2 class="page-heading mb-4">Manage User</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="administrator.html">Administrator Page</a></li>
            <li class="breadcrumb-item active"><a href="#">Manage User</a></li>
        </ol>
        <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="float: right; margin-bottom: 10px;">
            Advanced Search
        </a>
        <div class="col-lg-12" id="collapseExample">
            <div class="row">
                <form action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" method="POST" enctype="multipart/form-data">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-3 form-group">
                                <label>User Name</label>
                                <input name="user_name" type="text" class="form-control" maxlength="50" value="<?=(!empty($form['user_name']['value']) ? $form['user_name']['value'] : '')?>">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Display Name</label>
                                <input name="display_name" type="text" class="form-control" maxlength="20" value="<?=(!empty($form['display_name']['value']) ? $form['display_name']['value'] : '')?>">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>User Type</label><br>
                                <select name="user_type" class="form-control">
                                    <option value="0">All</option>
                                    <?php
                                    foreach($groups as $user_groups)
                                    {
                                        echo "\n <option value='{$user_groups['group_id']}' ";
                                        if(!empty($form['group_id']) && $form['group_id']['value'] == $user_groups['group_id']) echo 'selected';
                                        echo ">{$user_groups['group_name']}</option>\n";
                                    }
                                    ?>       
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label>Status</label><br>
                                <select name="user_status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <label><?php if(isset($error)) print_r($error);?></label><br>
                        </div>  
                        <button type="submit" name="submit" value="search" class="btn btn-lg btn-primary">Search</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Display Name</th>
                        <th>User Group</th>
                        <th>Status</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($users) && !empty($users)){
                            $num = (($page-1)*10)+1;
                            foreach($users as $result){
                                switch($result['user_status']){
                                    case "1":
                                        $status = 'Active';
                                    break;
                                    default:
                                        $status = 'Disabled';
                                }
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['user_name']}</td>
                        <td>{$result['display_name']}</td>
                        <td>{$result['group_name']}</td>
                        <td>{$status}</td>
                        <td>
                        <a href=\"admin_edit_user_{$result['user_id']}.html\" id=\"{$result['user_id']}\" class='btn btn-success btn-sm btn-block'>Edit</a>
                        </td>
    
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="6">Result not found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
            
            $pages = ceil($total/$max_result);
            if($pages > 1){
                echo '
            <nav>
                <ul class="pagination pagination-sm" id="pagingBottom">
                ';
                if(($current_page - 4) > 1){
                    echo"
                    <li class='page-item'>
                        <a href='#' class='page-link page-navigate' data-id='1'>1</a>
                    </li>
                    <li>
                        <a href='#'>...</a>
                    </li>";
                }
                $page_start = 1;
                $page_end = $pages;
                if($pages > 9 && $current_page > 9)
                {
                 
                    $page_start = (($current_page-4) < 1 ? 1 : $current_page-4);
                    $page_end = (($current_page+4) > $pages ? $pages : ($current_page+4));
                }
                elseif($pages > 9 && $current_page <= 9)
                {
                    $page_start = (($current_page-4) < 1 ? 1 : $current_page-4);
                    $page_end = (($current_page+4) > 9 ? ($current_page+4) : 9);
                }
                elseif($pages < 9 && $current_page <= 9)
                {
                    $page_start = (($current_page-4) < 1 ? 1 : $current_page-4);
                    $page_end = $pages;
                }
                for($i=$page_start;$i <= $page_end;$i++){
                    echo "<li class='page-item".($i == $current_page ? ' active' : '')."'><a href='admin_user_{$i}.html' class='page-navigate page-link' data-id='{$i}'>{$i}</a></li>";
                }
                echo "
                    <li class='page-item'>
                        <a href=\"#\">...</span></a>
                    </li>
                    <li class='page-item'>
                        <a href=\"#\" class='page-navigate page-link' data-id='{$pages}'>{$pages}</a>
                    </li>
                </ul>
            </nav>";
            }
        ?>
            
            <a href="admin_create_user.html" class="btn btn-primary btn-sm">Create New User</a>
        </div>
        <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus User</h4>
                    </div>
                    <div class="modal-body">
                        <p class="content-padding bg-danger">Apa kamu yakin ingin menghapus user ini?</p>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <a href=""><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script>
        function doconfirm()
        {
            job=confirm("Apakah anda yakin akan menghapus User ini?");
            if (job!=true) 
            {
                return false;
            }
        }
    </script>