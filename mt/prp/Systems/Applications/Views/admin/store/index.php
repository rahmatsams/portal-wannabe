	<div class="container-fluid">
        <h2 class="sub-header">Manage Outlet</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="administrator.html">Administrator Page</a></li>
            <li class="breadcrumb-item active"><a href="admin_store.html">Manage Outlet</a></li>
        </ol>
		<div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Outlet Name</th>
                        <th>Outlet Email</th>
                        <th>Outlet location</th>
                        <th>Status</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($store) && !empty($store)){
                            $num = (($page-1)*10)+1;
                            foreach($store as $result){
                                switch($result['store_status']){
                                    case 1:
                                        $status = 'Enabled';
                                        break;
                                    default:
                                        $status = 'Disabled';
                                }
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['store_name']}</td>
                        <td>{$result['store_email']}</td>
                        <td>{$result['location_name']}</td>
                        <td>{$status}</td>
                        <td><a href=\"admin_edit_store_{$result['store_id']}.html\" class='edit-users btn btn-success btn-sm btn-block'>Edit</a>
                        </td>
                       
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="4">There's no result found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
            $pages = ceil($total/$max_result); #paginationbootstrap
                if($pages > 1){
                    echo '
                <nav>
                    <ul class="pagination pagination-sm">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>';
                    for($i=1;$i <= $pages;$i++){
                        echo "<li ". ($page == $i ? 'class="page-item active"' : '') ."><a class=\"page-link\" href='admin_store_{$i}.html'>{$i}</a></li>";
                    }
                    echo '
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                    </li>
                    </ul>
                </nav>';
                }
            ?>
            <a href="admin_create_store.html" class="btn btn-primary btn-sm">Create New Outlet</a>

        </div>
        <!-- Delete Store Modal -->
        <!-- <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Hapus Store</h4>
                        </div>
                        <div class="modal-body">
                            <p class="content-padding bg-danger">Apa kamu yakin ingin menghapus Store ini?</p>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <a href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
    </div>
    <script>
        function doconfirm()
        {
            job=confirm("Apakah anda yakin akan menghapus Store ini?");
            if (job!=true) 
            {
                return false;
            }
        }
    </script>