					<div class="content-right">
						<h3 class="well">Create New Email</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="admin_email.html">/ Manage Email</a></li>
                            
                        </ol>
						<div class="row">
                            <form id="addEmail" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['pic_name']) ? "<div class=\"alert alert-danger\"><strong>PIC Name </strong>{$error['pic_name']}</div>" : '<label>PIC Name</label>')?>
                                            <input name="pic_name" type="text" placeholder="Name" class="form-control" maxlength="50" required>
                                        </div>
                                        <!-- Email -->
                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['email']) ? "<div class=\"alert alert-danger\"><strong>Store Email </strong>{$error['email']}</div>" : '<label>Email</label>')?>
                                            <input name="email" type="email" placeholder="Email" class="form-control" maxlength="50" required>
                                        </div>

                                        <div class="form-group">
                                            <?=(isset($error) && isset($error['email_status']) ? "<div class=\"alert alert-danger\"><strong>Status </strong>{$error['email_status']}</div>" : '<label>Status </label>')?>
                                            <select name="email_status" class="form-control">
                                                <option value="0">Disabled</option>
                                                <option value="1">Enabled</option>
                                            </select>
                                        </div>
                                        <button type="submit" name="submit" value="create_email" class="btn btn-primary ">Create</button>
                                    </div>
                            </form>
                         </div>
					</div>
					