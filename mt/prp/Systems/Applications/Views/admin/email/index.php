    <div class="container-fluid">
        <h2 class="sub-header">Manage Email</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="administrator.html">Administrator Page</a></li>
            <li class="breadcrumb-item active"><a href="admin_email.html">Manage Email</a></li>
        </ol>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PIC Name</th>
                        <th>Email</th>
                        <th>Email Status</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody id="listTable">
                    <?php
                        if(is_array($email) && !empty($email)){
                            $num = (($page-1)*10)+1;
                            foreach($email as $result){
                                switch($result['email_status']){
                                    case 1:
                                        $status = 'Enabled';
                                        break;
                                    default:
                                        $status = 'Disabled';
                                }
                                echo "
                    <tr>
                        <td>{$num} </td>
                        <td>{$result['pic_name']}</td>
                        <td>{$result['email']}</td>
                        <td>{$status}</td>
                        <td><a href=\"admin_edit_email_{$result['email_id']}.html\" class='edit-users btn btn-success btn-sm btn-block'>Edit</a>
                        </td>
                        <td>
                            <a href=\"#\" class=\"delete_email deleteconfirmationmodal\" data-id=\"{$result['email_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"btn btn-danger btn-sm btn-block\">Hapus</button></a>
                        </td>
                       
                    </tr>";
                                $num++;
                            }
                        } else {
                    ?>
                    <tr>
                        <td colspan="4">There's no result found</td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
            $pages = ceil($total/$max_result); #paginationbootstrap
                if($pages > 1){
                    echo '
                <nav>
                    <ul class="pagination pagination-sm">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>';
                    for($i=1;$i <= $pages;$i++){
                        echo "<li ". ($page == $i ? 'class="page-item active"' : '') ."><a class=\"page-link\" href='admin_email_{$i}.html'>{$i}</a></li>";
                    }
                    echo '
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a>
                    </li>
                    </ul>
                </nav>';
                }
            ?>
            <a href="admin_create_email.html" class="btn btn-primary btn-md btn-outline-info waves-effect"><b>Create New Email</b></a>

        </div>

        <!--  -->
        <!-- DELETE REKOMENDASI -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">HAPUS EMAIL</h5>
                                    </div>
                                    <div class="modal-body">
                                        Apa kamu yakin ingin menghapus Email ini?
                                    </div>
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <!-- id="admin_recom_delete" untuk jquery-->    
                                        <a id="admin_delete_email" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

           