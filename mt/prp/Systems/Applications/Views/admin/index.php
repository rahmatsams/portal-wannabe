	
          
          
        <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4">
                <div class="card-body">
                <h5 class="card-title">Admin Page</h5>
                    <div class="row">
                    <!--
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <i class="fa fa-users float-right icon-grey-big"></i>
                                    </div>
                                    <h4 class="card-title font-weight-normal text-success">1</h4>
                                    <h6 class="card-subtitle mb-4"><a href="admin_user.html">Manage User</a></h6>
                                    <div class="progress progress-slim">
                                        <div class="progress-bar bg-success-gadient" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    -->
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <i class="fa fa-folder-open float-right icon-grey-big"></i>
                                    </div>
                                    <h4 class="card-title font-weight-normal text-success">2</h4>
                                    <h6 class="card-subtitle mb-4"><a href="admin_category.html">Manage PRP</a></h6>
                                    <div class="progress progress-slim">
                                        <div class="progress-bar bg-success-gadient" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!--
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <i class="fa fa-institution float-right icon-grey-big"></i>
                                    </div>
                                    <h4 class="card-title font-weight-normal text-success">3</h4>
                                    <h6 class="card-subtitle mb-4"><a href="admin_store.html">Manage Store</a></h6>
                                    <div class="progress progress-slim">
                                        <div class="progress-bar bg-success-gadient" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    -->
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <i class="fa fa-envelope-open float-right icon-grey-big"></i>
                                    </div>
                                    <h4 class="card-title font-weight-normal text-success">4</h4>
                                    <h6 class="card-subtitle mb-4"><a href="admin_email.html">Manage Email</a></h6>
                                    <div class="progress progress-slim">
                                        <div class="progress-bar bg-success-gadient" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>