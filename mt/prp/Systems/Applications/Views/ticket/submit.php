		  <h3 class="page-heading mb-4">Submit PRP</h3>
          <?php
            echo (isset($result) && isset($result['error']['exists'])) ? '<div class="p-3 mb-2 bg-warning text-dark">This PRP Already Submitted by '.$result['submitted']['creator_name'].'</div>' : '';
          ?>
          <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4">
              <div class="card-body">
                <form id="submitPRP" method="POST" enctype="multipart/form-data" action="submit_prp.html">
					<div class="form-row">
						<div class="form-group col-lg-6">							
							<label for="selectOutlet">Outlet</label>
							<select id="selectOutlet" name="store" class="form-control" required>
								<option value="">--Select Outlet--</option>
								<?php
									
									foreach($store_list as $store){
                                        $selected = (isset($input) && $input['store'] == $store['store_id']) ? 'selected' : '';
										echo "\n
										<option value=\"{$store['store_id']}\" {$selected}>{$store['store_name']}</option>\n";
									}
								?>
							</select>
						
						</div>
						<div class="form-group col-lg-6">
							  <label for="exampleInputEmail1">Month</label>
							  <input type="month" name="date" class="form-control p-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Select Month"<?=(isset($input)) ? "value=\"{$input['date']}\"" : ''?>>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-lg-6">
							<label for="selectCategory">Category</label>
							<select id="selectCategory" name="category" class="form-control" required>
								<option value="">--Select Category--</option>
								<?php
									
									foreach($category_list as $category){
										echo "\n
										<option value=\"{$category['category_id']}\">{$category['category_name']}</option>\n";
									}
								?>
							</select>
						</div>
						<div class="form-group col-lg-6">
							<label for="selectSub">Point</label>
							<select id="selectSub" name="sub" class="form-control" disabled required>
								<option value="">--Select Point--</option>
                                <?php
                                    echo (isset($result) && isset($result['error']['exists'])) ? "<option value=\"{$result['submitted']['category_id']}\" selected>{$result['submitted']['category_name']}</option>" : '';
                                ?>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="selectArea">Area</label>
								<select id="selectArea" name="area" class="form-control" required>
									<option value="">--Select Area--</option>
								<?php
									
									foreach($area as $data){
                                        $selected = (isset($input) && $input['area'] == $data['area_id']) ? 'selected' : '';
										echo "\n
										<option value=\"{$data['area_id']}\" {$selected}>{$data['area_name']}</option>\n";
									}
								?>
								</select>
							</div>
							<div class="form-group"  id="addFile">
								<label>Upload Image (JPEG)</label>
								<input name="image_upload[]" type="file" class="form-control form-control-file" accept="image/*">
							</div>
						</div>
						<div class="form-group col-lg-6">
							<label for="inputNote">Notes</label>
							<textarea id="inputNote" name="notes" class="form-control" rows="7" required><?=(isset($input)) ? $input['notes'] : ''?></textarea>
						</div>
					</div>
                    
					<div class="col-lg-12">
						<button id="submitButton" type="submit" name="submit" value="kirim" class="btn btn-md btn-primary">Submit PRP</button>
					</div>
                </form>
              </div>
            </div>
          </div>