                  
                <?php
                    $ad = '';
                    $sl = '';
                    foreach($area as $data){
                        $selected = (isset($input) && $input['area'] == $data['area_id']) ? 'selected' : '';
                        $ad .= "
                                                <option value=\"{$data['area_id']}\" {$selected}>{$data['area_name']}</option>";
                    }
                    foreach($store_list as $store){
                        $selected = (isset($input) && $input['store'] == $store['store_id']) ? 'selected' : '';
                        $sl .= "
                                                <option value=\"{$store['store_id']}\" {$selected}>{$store['store_name']}</option>";
                    }
                ?>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="view_prp_<?=$group['group_id']?>.html"><?=$group['store_name']?> - <?=strftime("%B %Y", strtotime($group['group_date']))?></a></li>
                </ol>
                <div class="card-deck">
                    <div class="card col-lg-12 px-0 mb-4">  
                        <div class="card-header">
                            <strong class="card-title">Edit Checklist</strong>
                        </div>
                        <?=(isset($result) && isset($result['error']['exists'])) ? '<div class="p-3 mb-2 bg-warning text-dark">This PRP Already Submitted by '.$result['submitted']['creator_name'].'</div>' : '';?>
                    
                        
                        <div class="card-body">
                            <form id="submitPRP" class="forms-sample" method="POST" enctype="multipart/form-data" action="edit_prp_<?=$_GET['id']?>.html">
                                <input type='hidden' name='group' value='<?=$group['group_id']?>'>
                                <div class="form-row">
                                    <div class="form-group col-lg-4">
                                        <label for="selectArea">Area</label>
                                        <select id="selectArea" name="area" class="form-control" required>
                                            <option value="All" selected>-- All --</option><?=$ad?>

                                        </select>
                                    </div>
                                </div>
                                <div class="table-responsive" id="tableForm">
                                    <table class="table center-aligned-table table-striped">
                                        <thead>
                                            <tr class="text-primary text-center">
                                                <th style="min-width: 275px;">Point PRP</th>
                                                <th>Ceklist</th>
                                                <th colspan="2">Upload</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                            <?php
                            
                                #print_r($checklist);
                                $last = '';
                                $num = 0;
                                if(is_array($checklist) && count($checklist) > 0){
                                    foreach($checklist as $cl){
                                        $enable = ($cl['status'] == 1 ? '' : 'disabled');
                                        
                                        if($last != $cl['area']){
                                            echo '
                                                <tr class="bg-secondary text-white table-area" data-area="'.$cl['area_id'].'">
                                                    <td colspan="4"><strong>'.$cl['area'].'</strong></td>
                                                </tr>';
                                            $last = $cl['area'];
                                        }
                                        echo "
                                        <tr class='table-area control-row ".($cl['status'] == 0 ? 'bg-info text-white' : 'bg-danger')."' data-id='{$num}' data-area='{$cl['area_id']}'>
                                            <td>{$cl['category_name']}</td>
                                            <td class='form-check'>
                                                <input type='checkbox' name='status[{$num}]' value='{$cl['status']}' ".($cl['status'] == 0 ? 'checked' : '')." class='check-trigger'>
                                                <input type='hidden' name='ticket[{$num}]' value='{$cl['ticket_id']}' {$enable}>
                                                <input type='hidden' name='last_status[{$num}]' value='{$cl['status']}' {$enable}>
                                            </td>
                                            <td>".(!empty($cl['finding_images']) ? "<a href=\"./Resources/images/prp/{$cl['finding_images']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$cl['finding_images']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/{$cl['finding_images']}\"></a><input type='file' accept='image/*' id='img_{$num}' class='d-none' name='image_upload[{$num}]' {$enable}><label class='btn btn-sm btn-secondary' for='img_{$num}'>Select File</label>" : "<input type='file' accept='image/*' id='img_{$num}' class='d-none' name='image_upload[{$num}]' {$enable}><label class='btn btn-sm btn-secondary' for='img_{$num}'>Select File</label>")."</td>
                                            <td><textarea name='notes[{$num}]' style='padding:5px; margin:5px;' {$enable}>{$cl['notes']}</textarea></td>
                                        </tr>";
                                        $num++;
                                        
                                    }
                                }
                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4"><input type="submit" id="submitButton" class="btn btn-lg btn-primary" name="prp_edit" value="Edit PRP"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-success" style="position: fixed; bottom: 40px; right: 30px; z-index: 99;">Top</button>