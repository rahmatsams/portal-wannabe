                  
                <?php
                    $ad = '';
                    $sl = '';
                    foreach($area as $data){
                        $selected = (isset($input) && $input['area'] == $data['area_id']) ? 'selected' : '';
                        $ad .= "
                                                <option value=\"{$data['area_id']}\" {$selected}>{$data['area_name']}</option>";
                    }
                    foreach($store_list as $store){
                        $selected = (isset($input) && $input['store'] == $store['store_id']) ? 'selected' : '';
                        $sl .= "
                                                <option value=\"{$store['store_id']}\" {$selected}>{$store['store_name']}</option>";
                    }
                ?>
                <div class="card-deck">
                    <div class="card col-lg-12 px-0 mb-4">  
                        <div class="card-header">
                            <strong class="card-title">Submit Checklist</strong>
                        </div>
                        <?=(isset($result) && isset($result['error']['exists'])) ? '<div class="p-3 mb-2 bg-warning text-dark">This PRP Already Submitted by '.$result['submitted']['creator_name'].'</div>' : '';?>
                    
                        
                        <div class="card-body">
                            <form id="submitPRP" class="forms-sample" method="POST" enctype="multipart/form-data" action="submit_prp.html">
                                <div class="form-row">
                                    <div class="form-group col-lg-3">
                                        <label for="selectArea">Area</label>
                                        <select id="selectArea" name="area" class="form-control" required>
                                            <option value="All" selected>-- All --</option><?=$ad?>

                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">							
                                        <label for="selectOutlet">Outlet</label>
                                        <select id="selectOutlet" name="store" class="form-control" required>
                                            <option value="" selected></option><?=$sl?>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="selectMonth">Month</label>
                                        <input type="month" name="date" class="form-control" id="selectMonth" aria-describedby="emailHelp" placeholder="Select Month" <?=(isset($input)) ? "value=\"{$input['date']}\"" : 'value="'.date("Y-m").'"'?> required>
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="inputAudite">Auditee</label>
                                        <input type="text" name="auditee" class="form-control" id="inputAudite" aria-describedby="inputAudite" placeholder="Nama Auditee.." required>
                                    </div>
                                </div>
                                <div class="table-responsive" id="tableForm">
                                    <table class="table center-aligned-table table-striped">
                                        <thead>
                                            <tr class="text-primary text-center">
                                                <th style="min-width: 225px;">Point PRP</th>
                                                <th style="max-width: 50px;">Nilai</th>
                                                <th><input type='checkbox' id="checkAll"></th>
                                                <th colspan="2">Upload</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                            
                            <?php
                                #print_r($category_list);
                                $last = '';
                                $num = 0;
                                if(is_array($category_list) && count($category_list) > 0){
                                    foreach($category_list as $cl){
                                        if($last != $cl['area_id']){
                                            echo '
                                                <tr class="bg-secondary text-white table-area" data-area="'.$cl['area_id'].'">
                                                    <td colspan="5"><strong>'.$cl['area_name'].'</strong></td>
                                                </tr>';
                                            $last = $cl['area_id'];
                                        }
                                        
                                        echo "
                                        <tr class='table-area control-row' data-id='{$num}' data-area='{$cl['area_id']}'>
                                            <td>{$cl['category_name']}</td>
                                            <td>{$cl['point_deduction']}</td>
                                            <td class='form-check'>
                                                <input type='checkbox' name='status[{$num}]' value='{$cl['category_id']}' class='check-trigger'>
                                                <input type='hidden' name='link[{$num}]' value='{$cl['category_id']}'>
                                            </td>
                                            <td><input type='file' accept='image/*' id='img_{$num}' class='d-none file-trigger' name='image_upload[{$num}]'><label class='btn btn-sm btn-secondary' for='img_{$num}'>Select File</label></td>
                                            <td><textarea class='note-trigger' name='notes[{$num}]' style='padding:5px; margin:5px;'></textarea></td>
                                        </tr>";
                                        $num++;
                                        
                                    }
                                }
                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4"><input type="submit" id="submitButton" class="btn btn-lg btn-primary" name="prp_submit" value="Submit"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="statusModal" class="modal fade" tabindex="-1" role="loading" data-backdrop="static" data-keyboard="false">
                    <div class="vertical-alignment-helper">
                        <div class="modal-dialog vertical-align-center">
                            <div class="modal-content">
                                <div class="modal-header bg-success text-white">
                                
                                    <h4 class="modal-title" id="myModalLabel">Already Submitted</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-danger" style="position: fixed; bottom: 40px; right: 30px; z-index: 99;">Top</button>