            <?php
                $hp = $prp['default_point'] - $prp['group_point'];
            ?>
            <div class="card-deck">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title"><?=$prp['store_name']?> - <?=strftime("%B %Y", strtotime($prp['group_date']))?></strong>
                    </div>
                     <div class="card-body"> 
                        <div class="card-deck">
                            <div class="card">
                                <div class="card-header"><strong class="card-title">Status</strong></div>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="indexContent" class="table">
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Nilai</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$hp?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Finding</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=count($sub)?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Auditor</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['group_user']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Auditee</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['group_auditee']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Tanggal</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=date("d-m-Y", strtotime($prp['group_submit_date']))?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Email</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=($prp['group_status'] == 0 ? 'Not Sent' : 'Sent')?></td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"><strong class="card-title">Perintah</strong></div>
                                <div class="card-body">
                                    <?php
                                        if($prp['group_status'] == 0){
                                            echo '<div class="col-sm-12">
                                        <a href="edit_prp_'.$prp['group_id'].'.html"><button class="btn btn-small btn-info btn-block">Edit Checklist</button></a>
                                    </div>
                                    <div class="col-sm-12 mt-1">
                                        <button class="btn btn-small btn-success btn-block" data-toggle="modal" data-target="#confirmationModal">Send Email</button>
                                    </div>';
                                        }
                                    ?>
                                        
                                    <div class="col-sm-12 mt-1">
                                        <a href="report_prp_<?=$prp['group_id']?>.html"><button class="btn btn-small btn-info btn-block">Unduh Report</button></a>
                                    </div>
                                    <div class="col-sm-12 mt-1">
                                        <a href="recommend_prp_<?=$prp['group_id']?>.html"><button class="btn btn-small btn-success btn-block">Lihat Rekomendasi</button></a>
                                    </div>
                                    <?php
                                        if($prp['group_status'] == 0){
                                            echo '
                                    <div class="col-sm-12 mt-1">
                                        <a class="delete_category deleteconfirmationmodal" data-id="'.$prp['group_id'].'" href="#" data-toggle="modal" data-target="#modalconfirmdelete"><button class="btn btn-small btn-danger btn-block">Hapus PRP</button></a>
                                    </div>';
                                        };
                                    ?>
                                </div>
                                
                            </div>
                        </div>
                        <br>
                    <?php if(is_array($sub) && count($sub) > 0){ ?>
                        <div class="table-responsive">
                          <table id="indexContent" class="table center-aligned-table table-striped">
                            <thead>
                              <tr class="text-primary">
                                <th>No</th>
                                <th>Area</th>
                                <th>Kategori</th>
                                <th>Points</th>
                                <th>Gambar</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                            
                            <?php
                                    $i=1;
                                    foreach($sub as $d){
                                        echo "<tr class=''>
                                    <td>{$i}</td>
                                    <td>{$d['area']}</td>
                                    <td>{$d['category_name']}</td>
                                    <td>-{$d['point']}</td>
                                    <td>".(!empty($d['finding_images']) ? "<a href=\"./Resources/images/prp/{$d['finding_images']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$d['finding_images']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/{$d['finding_images']}\"></a>" : "<a href='upload_single_{$d['ticket_id']}.html'>Upload</a>")."</td>
                                    <td>{$d['notes']}</td>
                                    <td></td>
                                </tr>";
                                    $i++;
                                    }
                            ?>
                            </tbody>
                          </table>
                        </div>
                    <?php
                        }else{
                            echo '<div class="p-3 mb-2 bg-success text-white">Tidak ditemukan finding bulan ini</div>';
                        }
                    ?>
                    </div>
                    <div id="confirmationModal" class="modal fade" tabindex="-1" role="confirm" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Confirmation</h4>
                                    </div>
                                    <div class="modal-body">Are you sure you want to Email this PRP result to Outlet?</div>
                                    <div class="modal-footer">
                                        <button id="sendEmail" data-id="<?=$prp['group_id']?>" type="button" class="btn btn-success">Yes</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="successModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Success</h4>
                                    </div>
                                    <div class="modal-body">Email has been sent to Outlet.</div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="failedModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Failed</h4>
                                    </div>
                                    <div class="modal-body">Send Email Failed.</div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-danger" style="position: fixed; bottom: 40px; right: 30px; z-index: 99;">Top</button>
        <!-- DELETE PRP -->
        <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="vertical-alignment-helper">
                
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Hapus PRP</h4>
                        </div>
                        <div class="modal-body">
                            Apa kamu yakin ingin menghapus Ticket ini?
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                
                                <a href="<?= "delete_prp_".$prp['group_id'].".html"; ?>"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                              
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <script type="text/javascript">
        var num = <?=$_GET['n']?>;
    </script>