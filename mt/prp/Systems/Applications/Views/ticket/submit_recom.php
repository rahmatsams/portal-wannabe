        <nav aria-label="breadcrumb">
        </nav>
        <!-- <?php
            if(is_array($fdata) && count($fdata) > 0){
                if($fdata['success'] == 1){
                    echo '<div class="p-3 mb-2 bg-success text-white">Category Updated Successfully !!</div>';
                }else{
                    echo '<div class="p-3 mb-2 bg-danger text-white">Please check your input !!</div>';
                }
            }
        ?> -->
        <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4"> 
                <div class="card-header">
                    <strong class="card-title">Manage Recommendation</strong>
                </div>
                <div class="col-lg-6 card-body">
                    <div class="card-deck">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Add Recommendation</strong>
                            </div>
                            <div class="card-body p-0 pt-3 pb-3">
                                <form id="addsubcategory" method="POST" action="admin_edit_recommendation.html">
                                    <div class="col-md-12 form-group">
                                        <label for="insertCatName">Content</label>
                                        <input id="catid" name="group_id" type="hidden" value="<?=$result['group_id']?>" class="form-control" maxlength="6" required>
                                        <input type="hidden" name="recommendation_id" value="<?=$result['recommendation_id']?>" required>
                                        <textarea name="recommendation_content" id="insertCatName" class="form-control" cols="70" rows="3" required></textarea>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="insertCatName">Feedback</label>
                                        <textarea name="recommendation_feedback" id="insertCatName" class="form-control" cols="70" rows="3" required></textarea>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="insertCatName">Image</label><br>
                                        <input type='file' accept='image/*' id='img_{$num}' class='d-none file-trigger' name='image_upload[{$num}]'><label class='btn btn-sm btn-secondary' for='img_{$num}'>Select File</label>
                                    </div>
                                   
                                   
                                </form>
                            </div>
                            <div class="card-footer">
                                <input type='submit' class='btn btn-sm btn-primary' name="action" value="Add Recommendation">
                            </div>
                        </div>
                    </div>
                </div>
                
        </div>
            <!-- delete sub category -->
             <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Hapus Sub Kategori</h4>
                        </div>
                        <div class="modal-body">
                            <p class="content-padding bg-danger">Apa kamu yakin ingin menghapus rekomendasi ini?</p>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <a href="<?= "admin_recom_delete_".$result['recommendation_id'].".html"; ?>"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        