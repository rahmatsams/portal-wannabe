            <?php
                $hp = $prp['default_point'] - $prp['group_point'];
            ?>
            <div class="card-deck">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title"><?=$prp['store_name']?> - <?=strftime("%B %Y", strtotime($prp['group_date']))?></strong>
                    </div>
                     <div class="card-body"> 
                        <div class="card-deck">
                            <div class="card">
                                <div class="card-header"><strong class="card-title">Status</strong></div>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="indexContent" class="table">
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Nilai</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$hp?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Finding</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=count($sub)?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Auditor</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['group_user']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Auditee</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=$prp['group_auditee']?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Tanggal</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=date("d-m-Y", strtotime($prp['group_submit_date']))?></td>
                                            </tr>
                                            <tr>
                                                <td class="pt-1 pb-1 pl-3 pr-3">Email</td>
                                                <td class="pt-1 pb-1 pl-3 pr-3"><?=($prp['group_status'] == 0 ? 'Not Sent' : 'Sent')?></td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header"><strong class="card-title">Perintah</strong></div>
                                <div class="card-body">
                                    <div class="col-sm-12 mt-1">
                                        <a href="report_prp_<?=$prp['group_id']?>.html"><button class="btn btn-small btn-info btn-block">Unduh Report</button></a>
                                    </div>
                                    <div class="col-sm-12 mt-1">
                                        <a href="recommend_prp_<?=$prp['group_id']?>.html"><button class="btn btn-small btn-success btn-block">Lihat Rekomendasi</button></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <br>
                    <?php if(is_array($sub) && count($sub) > 0){ ?>
                        <div class="table-responsive">
                          <table id="indexContent" class="table center-aligned-table table-striped">
                            <thead>
                              <tr class="text-primary">
                                <th>No</th>
                                <th>Area</th>
                                <th>Kategori</th>
                                <th>Points</th>
                                <th>Gambar</th>
                                <th>Keterangan</th>
                                <th>Balasan</th>
                                <th>Ket.Balasan</th>
                                <th>Perintah</th>
                              </tr>
                            </thead>
                            <tbody>
                            
                            <?php
                                    $i=1;
                                    foreach($sub as $d){
                                        echo "<tr class='finding' data-id='{$d['ticket_id']}'>
                                    <td>{$i}</td>
                                    <td class='area-name'>{$d['area']}</td>
                                    <td class='category-name'>{$d['category_name']}</td>
                                    <td>-{$d['point']}</td>
                                    <td>".(!empty($d['finding_images']) ? "<a href=\"./Resources/images/prp/{$d['finding_images']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$d['finding_images']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/{$d['finding_images']}\"></a>" : "")."</td>
                                    <td>{$d['notes']}</td>
                                    <td>".(!empty($d['resolved_images']) ? "<a href=\"./Resources/images/prp/reply/{$d['resolved_images']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/reply/{$d['resolved_images']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/reply/{$d['resolved_images']}\"></a>" : "")."</td>
                                    <td>{$d['resolved_solution']}</td>
                                    <td><button class='btn btn-block btn-primary update-modal' data-toggle='modal' data-target='#replyModal'>Balas</button></td>
                                </tr>";
                                    $i++;
                                    }
                            ?>
                            </tbody>
                          </table>
                        </div>
                    <?php
                        }else{
                            echo '<div class="p-3 mb-2 bg-success text-white">Tidak ditemukan finding bulan ini</div>';
                        }
                    ?>
                    </div>
                    <div id="confirmationModal" class="modal fade" tabindex="-1" role="confirm" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Konfirmasi</h4>
                                    </div>
                                    <div class="modal-body">Apa anda yakin ingin mengirimkan Email PRP ke outlet?</div>
                                    <div class="modal-footer">
                                        <button id="sendEmail" data-id="<?=$prp['group_id']?>" type="button" class="btn btn-success">Ya</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="successModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Berhasil</h4>
                                    </div>
                                    <div class="modal-body">Email Berhasil Dikirim.</div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="failedModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Gagal</h4>
                                    </div>
                                    <div class="modal-body">Email Gagal Terkirim.</div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="replyModal" class="modal fade" tabindex="-1" role="dialog">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <form id="replyTicket" method="POST" action="index.php?s=ticket&i=reply" enctype="multipart/form-data">
                                        <div class="modal-header">
                                             <h4 class="modal-title">Balas PRP</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row bg-info text-white">
                                                <div class="col-sm-4" id="areaReply">
                                                    Area
                                                </div>
                                                <div class="col-sm-8" id="categoryReply">
                                                    Kategori
                                                </div>
                                            </div>
                                            <div class="row mt-2">
                                                <div class="col-sm-4">
                                                    <img src="./Resources/images/not_found.jpg" style="width: 100%; height: 100%;" id="imageReply"/>
                                                </div>
                                                <div class="col-sm-8 form-group">
                                                    <input type="file" name="image_upload" accept="image/*" class="form-control" id="fileReply"><br/>
                                                    <textarea name="notes" cols="30" rows="3" class="form-control" id="noteReply"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" id="submitReply" name="prp_reply" value="Balas" class="btn btn-primary btn-small">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="successReplyModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Berhasil</h4>
                                    </div>
                                    <div class="modal-body">Balasan PRP berhasil dikirim.</div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="failedReplyModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Gagal</h4>
                                    </div>
                                    <div class="modal-body">Balasan PRP gagal dikirim.</div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button onclick="topFunction()" id="myBtn" class="btn btn-lg btn-danger" style="position: fixed; bottom: 40px; right: 30px; z-index: 99;">Top</button>
                    <!-- DELETE PRP -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="vertical-alignment-helper">
                            
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Hapus PRP</h4>
                                    </div>
                                    <div class="modal-body">
                                        Apa kamu yakin ingin menghapus Ticket ini?
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                            
                                            <a href="<?= "delete_prp_".$prp['group_id'].".html"; ?>"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                              
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>  