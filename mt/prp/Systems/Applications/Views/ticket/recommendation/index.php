        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"><a href="view_prp_<?=$prp['group_id']?>.html">Manage PRP</a></li>
                <li class="breadcrumb-item"><a>Kelola Rekomendasi</a></li>
            </ol>
        </nav>
       
        <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4"> 
                <?php
                        if($admin){
                            
                    ?>
                <div class="col-lg-6 card-body">
                    <div class="card-deck">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Tambah Rekomendasi</strong>
                            </div>
                            <div class="card-body p-0 pt-3 pb-3">
                                <form id="addsubcategory" method="POST" action="recommend_prp_<?=$prp['group_id']?>.html" enctype="multipart/form-data">
                                    <div class="col-md-12 form-group">
                                        <label for="insertCatName">Konten</label>
                                        <input id="catid" name="group_id" type="hidden" value="<?=$prp['group_id']?>" class="form-control" maxlength="6" required>
                                        <textarea name="recommendation_content" id="insertCatName" class="form-control" cols="70" rows="2" required></textarea>
                                    </div>
                                    <div class="col-md-12 form-group" hidden="hidden">
                                        <label for="insertCatName">Feedback</label>
                                        <textarea name="recommendation_feedback" id="insertCatName" class="form-control" cols="70" rows="3"></textarea>
                                    </div>
                                    <div class="col-md-12 form-group" hidden="hidden">
                                        <label for="insertCatName">Status</label>
                                        <input name="recommendation_status" id="insertCatName" class="form-control" value="1" cols="70" rows="3" required>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="insertCatName">Gambar</label>&nbsp;
                                        <input type='file' accept='image/*' id='img_0' class='d-none file-trigger' name='recommendation_image[0]'><label class='btn  btn-secondary btn-sm' for='img_0'>Select File</label>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <button type="submit" name="submit" value="new_recom" class="btn btn-primary btn-sm">Submit</button>     
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> 
                <?php
                        }
                    ?>
                <div class="card-header">
                    <strong class="card-title">Data Rekomendasi</strong>
                </div>
                <div class="card-body">
                    <table class="table table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Content</th> 
                                <th>Feedback</th>
                                <th>Image</th>
                                <th colspan="3"><center>Action</center></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(is_array($sub) && !empty($sub)){
                                $num = (($page-1)*10)+1;
                                
                               if($session['group'] == 'MOD Outlet') {

                                foreach($sub as $prp){
                                    echo "
                                        <tr class='form-group' id=''>
                                            <form method='POST' action=''>
                                                <td>
                                                    {$num} 
                                                </td>
                                                <td>
                                                    <textarea name='recommendation_content' cols='70' rows='3' readonly class='form-control-plaintext' required>{$prp['recommendation_content']}</textarea>
                                                </td>
                                                <td>
                                                    <textarea name='recommendation_feedback' cols='70' rows='3' readonly class='form-control-plaintext' required>{$prp['recommendation_feedback']}</textarea>
                                                </td>
                                                <td>
                                                    ".(!empty($prp['recommendation_image']) ? "<a href=\"./Resources/images/prp/{$prp['recommendation_image']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$prp['recommendation_image']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/{$prp['recommendation_image']}\"></a>" : "Not Uploaded [<a href='upload_single_{$prp['recommendation_id']}.html'>Upload</a>]")."
                                                </td>
                                                
                                                <td>
                                                    <a href=\"admin_feedback_{$prp['recommendation_id']}.html\" class='manage-button' data-id='{$prp['recommendation_id']}'><button type=\"button\" class=\"btn btn-info btn-sm\">Feedback</button></a><input type='submit' name='action' class='btn btn-sm btn-info d-none' value='Edit'>
                                                </td>
                                                
                                                
                                            </form>
                                        </tr>
                                        ";
                                        $num++;
                                        } /*foreach*/
                              }else{
                                foreach($sub as $prp){
                                    echo "
                                        <tr class='form-group' id=''>
                                            <form method='POST' action=''>
                                                <td>
                                                    {$num} 
                                                </td>
                                                <td>
                                                    <textarea name='recommendation_content' cols='70' rows='3' readonly class='form-control-plaintext' required>{$prp['recommendation_content']}</textarea>
                                                </td>
                                                <td>
                                                    <textarea name='recommendation_feedback' cols='70' rows='3' readonly class='form-control-plaintext' required>{$prp['recommendation_feedback']}</textarea>
                                                </td>
                                                <td>
                                                    ".(!empty($prp['recommendation_image']) ? "<a href=\"./Resources/images/prp/{$prp['recommendation_image']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$prp['recommendation_image']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:50px; height:50px;' src=\"./Resources/images/prp/{$prp['recommendation_image']}\"></a>" : "Not Uploaded [<a href='upload_single_{$prp['recommendation_id']}.html'>Upload</a>]")."
                                                </td>
                                                <td>
                                                    <a href=\"admin_edit_recom_{$prp['recommendation_id']}.html\" class='manage-button' data-id='{$prp['recommendation_id']}'><button type=\"button\" class=\"btn btn-success btn-sm\">Edit</button></a><input type='submit' name='action' class='btn btn-sm btn-success d-none' value='Edit'>
                                                </td>
                                                <td>
                                                    <a href=\"#\" class=\"delete_recom deleteconfirmationmodal\" data-id=\"{$prp['recommendation_id']}\" href=\"#\" data-toggle=\"modal\" data-target=\"#modalconfirmdelete\"><button type=\"button\" class=\"btn btn-danger btn-sm\">Hapus</button></a>
                                                </td>
                                                <td>
                                                    <a href=\"admin_feedback_{$prp['recommendation_id']}.html\" class='manage-button' data-id='{$prp['recommendation_id']}'><button type=\"button\" class=\"btn btn-info btn-sm\">Feedback</button></a><input type='submit' name='action' class='btn btn-sm btn-info d-none' value='Edit'>
                                                </td>
                                                
                                            </form>
                                        </tr>
                                        ";
                                        $num++;
                                        }
                                    }
                            } /*if isset*/
                            else{
                                echo '
                            <tr>
                                <td colspan="6" align="center">No data</td>
                            </tr>';
                            }
                        ?>
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            <!-- DELETE REKOMENDASI -->
                    <div class="modal fade" id="modalconfirmdelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="vertical-alignment-helper">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content" id="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myModalLabel">Hapus Rekomendasi</h5>
                                    </div>
                                    <div class="modal-body">
                                        Apa kamu yakin ingin menghapus Rekomendasi ini?
                                    </div>
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                        <!-- id="admin_recom_delete" untuk jquery-->    
                                        <a id="admin_recom_delete" href="#"><button id="deleteconfirm" type="button" class="btn btn-danger">Ya</button></a>                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <!-- JQUERY DELETE REKOMENDASI -->
                