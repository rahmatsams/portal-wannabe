        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active"><a href="recommend_prp_<?=$prp['group_id']?>.html">Halaman Rekomendasi</a></li>
                <li class="breadcrumb-item"><a>Kelola Edit Recommendation</a></li>
            </ol>
        </nav>
        <div class="card-deck">
            <div class="card col-lg-12 px-0 mb-4"> 
                <div class="col-lg-6 card-body">
                    <div class="card-deck">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Edit Rekomendasi</strong>
                            </div>
                            <div class="card-body p-0 pt-3 pb-3">
                                <form id="addsubcategory" method="POST" action="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" enctype="multipart/form-data">
                                    <div class="col-md-12 form-group">
                                        <label for="insertCatName">Konten</label>
                                        <input id="catid" name="group_id" type="hidden" value="<?=$prp['group_id']?>" class="form-control" maxlength="6" required>
                                        <input id="catid" name="recommendation_id" type="hidden" value="<?=$prp['recommendation_id']?>" class="form-control" maxlength="6" required>
                                        <textarea name="recommendation_content" id="insertCatName" class="form-control" cols="70" rows="2" required><?=$prp['recommendation_content']?></textarea>
                                    </div>
                                    <div class="col-md-12 form-group"  hidden="hidden">
                                        <label for="insertCatName">Feedback</label>
                                        <textarea name="recommendation_feedback" id="insertCatName" class="form-control" cols="70" rows="3"><?=$prp['recommendation_feedback']?></textarea>
                                    </div>
                                    <div class="col-md-12 form-group" hidden="hidden">
                                        <label for="insertCatName">Status</label>
                                        <input name="recommendation_status" id="insertCatName" class="form-control" value="1" cols="70" rows="3" required>
                                    </div>
                                    <div class="col-md-12 form-group" >
                                        <label for="insertCatName">
                                            <?=(!empty($prp['recommendation_image']) ? "<a href=\"./Resources/images/prp/{$prp['recommendation_image']}\" target='_blank' onclick=\"window.open('./Resources/images/prp/{$prp['recommendation_image']}', 'newwindow', 'width=300,height=250'); return false;\"><img style='width:80px; height:80px;' src=\"./Resources/images/prp/{$prp['recommendation_image']}\"></a>" : 'Insert Foto Kembali')?> 
                                        </label>&nbsp;
                                        <input type='file' accept='image/*' id='img_0' class='d-none file-trigger' name='recommendation_image[0]' value="<?=$prp['recommendation_image']?>"><label class='btn  btn-secondary btn-sm' for='img_0'>Select File</label>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <button type="submit" name="submit" value="edit_recom" class="btn btn-success btn-sm">Edit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
              </div>              
        </div>
    </div>
        