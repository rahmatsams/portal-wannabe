<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Controller Rekomendasi
*/
class Recommendation extends Controller
{
	public function accessRules()
	{
		return array(
            array('Allow', 
                'actions'=>array('viewRecommend', 'viewCreateRecom', 'viewDeleteRecommend', 'viewEditRecommend'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin QA', 'Admin View'),
            ),
            array('Allow', 
                'actions'=>array('viewRecommend', 'viewAddFeedback'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin QA', 'MOD Sushi Tei', 'Admin View'),
            ),
            array('Deny', 
                'actions'=>array('viewEdit', 'viewSubmit', 'viewSubmitUpload', 'viewTicket', 'viewDeletePending', 'viewProccessSubmit','viewGroup','viewExportReport', 'viewRecommend', 'viewAddFeedback'),
                'groups'=>array('Guest'),
            ),
        );
	}

	public function site()
    {
        $site = array(
            'root' => 'prp'
        );
        return $site;
    }

	#Recommendation Data
    public function viewRecommend()
    { 
        $input = $this->load->lib('Input');
        $m_recom = $this->load->model('Recommendation');
        $tg =  $this->load->model('TicketGroup');
        $data = array(
            'admin' => $this->isAdmin(),
            'site' => $this->Site(),
            'session' => $this->_session,
            'page' => 1,
            'option'        => array(
                    'injs' => array(),
                    'exjs' => array('Resources/js/prpscript.js')
                )
        );
       	#Submit Recommendation view:recommendation.php
        if(isset($_POST['submit'])){
            if($_POST['submit'] == 'new_recom'){ 
                $input->addValidation('parent_max', $_POST['group_id'], 'max=6', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['group_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['recommendation_content'], 'min=1', 'Cek kembali input anda');
                $input->addValidation('category_max', $_POST['recommendation_feedback'], 'min=0', 'Cek kembali input anda');
                if(count($_FILES['recommendation_image']) > 0){
                    $files = $this->reArrayFiles($_FILES['recommendation_image']);
                }
                if($input->validate()){
                    unset($_POST['submit']);
                    $upload = $this->doUpload($files);
                    if(isset($upload[0]) && !empty($upload[0]['upload_name'])){
                        $_POST['recommendation_image'] = $upload[0]['upload_name'];
                    }
                    if($m_recom->createRecommend($_POST)){
                        
                        unset($_POST['recommendation_image']);
                        $data['success'] = 1;
                        $parent = $_POST['group_id'];
                        $tg->editGroup(
                            array(
                                'value' => array(
                                    'group_recommendation' => $tg->getTicketGroup(array('group' => $parent))['group_recommendation']+1
                                ),
                                'where' => array(
                                    'group_id' => $parent
                                )
                            )
                        );
                        header("Location: recommend_prp_{$parent}.html");
                    } else {
                        $data['error'] = 'Unknown Error';
                    }
                }else{
                    $data['error'] = $input->_error;
                }
            }    
        }else if(isset($_GET['n'])){  
            $input = $this->load->lib('Input');
            $input->addValidation('idformat', $_GET['n'], 'numeric', 'Unknown Error');
            $input->addValidation('idlen', $_GET['n'], 'min=1', 'Unknown Error');
            $input->addValidation('idmax', $_GET['n'], 'max=6', 'Unknown Error');
            if($input->validate()){
                $data['prp'] = $tg->getTicketGroup(array('group' => $_GET['n']));
                if(is_array($data['prp'])){
                    $data['sub'] = $m_recom->getRecomByGroup(array('group'=>$_GET['n']));
                }
            }else{
                 $this->showError(2);
            }
        }else{
            $this->showError(2);
        }
        if(isset($data['sub'])) $this->load->template('ticket/recommendation/index', $data);
    }

    #Edit Recommendation
    public function viewEditRecommend()
    {
        $input = $this->load->lib('Input');
        $m_recom = $this->load->model('Recommendation');
        $tg =  $this->load->model('TicketGroup');
        $data = array(
            'site' => $this->Site(),
            'session' => $this->_session,
            'page' => 'Manage Feedback',
            'option'   => array(
                    'admin' => $this->isAdmin(),
                    'injs' => array(),
                    'exjs' => array('Resources/js/prpscript.js')
                )
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_recom'){ 
                $input->addValidation('parent_max', $_POST['group_id'], 'max=6', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['group_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['recommendation_content'], 'min=1', 'Cek kembali input anda');
                $input->addValidation('category_max', $_POST['recommendation_feedback'], 'min=0', 'Cek kembali input anda');
                if(count($_FILES['recommendation_image']) > 0){
                    $files = $this->reArrayFiles($_FILES['recommendation_image']);
                }
                if($input->validate()){
                    unset($_POST['submit']);
                    $upload = $this->doUpload($files);
                    $_POST['recommendation_image'] = $upload[0]['upload_name'];
                    if($m_recom->editRecommend($_POST, array('recommendation_id' => $_GET['id'])))
                    {
                        
                        unset($_POST['recommendation_image']);
                        $data['success'] = 1;
                        $parent = $_POST['group_id'];
                        $data['prp'] = $tg->getTicketGroup(array('group' => $parent));
                        if(is_array($data['prp'])){
                            $data['sub'] = $m_recom->getRecomByGroup(array('group'=>$parent));
                            header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                        }
                    } else {
                        $data['error'] = 'Unknown Error';
                    }
                }else{
                    $data['error'] = $input->_error;
                }
            }  
        else{
            if(isset($_GET['id']))
            {
                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                if($input->validate()){
                    $data['option'] = array(
                        'exjs' => array('./Resources/js/adminprp.js')
                    );
                    $data['prp'] = $m_recom->getFeedbackId(array('cid'=>$_GET['id']));
                    $this->load->template('ticket/recommendation/edit', $data);
                } else {
                    $this->showError(2);
                }
            }else{
                $this->showError(2);
            	}
        	}  
    }

    #Add Feedback
    public function viewAddFeedback()
    {
        $input = $this->load->lib('Input');
        $m_recom = $this->load->model('Recommendation');
        $tg =  $this->load->model('TicketGroup');
        $data = array(
            'site' => $this->Site(),
            'session' => $this->_session,
            'page' => 'Manage Feedback',
            'option'        => array(
                    'injs' => array(),
                    'exjs' => array('Resources/js/prpscript.js')
                )
        );
        if (isset($_POST['submit']) && $_POST['submit'] == 'new_feedback') {
            $input->addValidation('category_max', $_POST['recommendation_feedback'], 'min=0', 'Cek kembali input anda');
            if ($input->validate()) {
                $prp = array(
                    'recommendation_feedback' => $_POST['recommendation_feedback'], 
                );
                if ($m_recom->editAddFeedback($prp, array('recommendation_id' => $_GET['id']))) {
                    echo "<script>alert('Feedback ditambahkan'); window.location.replace('recommend_prp_".$_POST['group_id'].".html');</script>";
                }
            }
            else{
                $data['error'] = $input->_error;
                $this->load->template('ticket/feedback', $data);
            }
        }
        else{
            if(isset($_GET['id']))
	            {
	                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
	                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
	                if($input->validate()){
	                    $data['option'] = array(
	                        'exjs' => array('./Resources/js/adminprp.js')
	                    );
	                    $data['prp'] = $m_recom->getFeedbackId(array('cid'=>$_GET['id']));
	                    $this->load->template('ticket/recommendation/feedback', $data);
	                } else {
	                    $this->showError(2);
	                }
	            }else{
	                $this->showError(2);
	            	}
        	}  
    }


    protected function doUpload($file, $location='Resources/images/prp')
    {
        
        if(count($file) > 0){
            $uploaded = array();
            $num=0;
            $upload = $this->load->lib('Upload', $file);
            for($uid=0;$uid < count($file); $uid++){
                $file[$uid] = new Upload($file[$uid]);
                if($file[$uid]->uploaded) {
                    $file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num;
                    $file[$uid]->image_resize = true;
                    $file[$uid]->image_x = 800;
                     $file[$uid]->image_ratio_y = true;
                    $file[$uid]->allowed = array('image/jpeg', 'image/png');
                    $file[$uid]->image_convert = 'jpg';
                    $file[$uid]->process($location);
                    if ($file[$uid]->processed) {
                        $file[$uid]->clean();
                        $uploaded[$num] = array(
                            'upload_name' => $file[$uid]->file_dst_name,
                            'upload_location' => $file[$uid]->file_dst_path,
                            'upload_time' => date("Y-m-d H:i:s"),
                            'upload_temp' => 0,
                            'upload_user' => $this->_session['userid'],
                        );
                        $num++;
                    }else{
                        $uploaded[$num]['error'] = $file[$uid]->error;
                    }
                }
            }
            return $uploaded;
        }
    }

    #Delete Recommendation
    public function viewDeleteRecommend()
    {
        $id = $_GET['idRecommend'];
        $m_recom = $this->load->model('Recommendation');
        $m_tg = $this->load->model('TicketGroup');
        if (is_array($m_recom->getRecommendationId(array('recommendation_id' => $_GET['idRecommend'] )))) 
        {
            $rc = $m_recom->checkImageRecommendation(array('recommendation_id' => $_GET['idRecommend']));
            foreach ($rc as $img) {
                $file = "Resources/images/prp/".$img['recommendation_image'];
                if (file_exists($file) && is_file($file)) 
                {
                    unlink($file);
                }
            }
            $m_recom->deleteRecommend($id);
            $m_tg->editGroup(
                array(
                    'value' => array(
                        'group_recommendation' => $m_tg->getTicketGroup(array('group' => $_GET['idGroup']))['group_recommendation']-1
                    ),
                    'where' => array(
                        'group_id' => $_GET['idGroup']
                    )
                )
            );
            header("Location: recommend_prp_{$_GET['idGroup']}.html");
        }else{
            exit('No Data');
        }
    }
    
    
    protected function reArrayFiles(&$file_post)
    {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }
        return $file_ary;
    }

}