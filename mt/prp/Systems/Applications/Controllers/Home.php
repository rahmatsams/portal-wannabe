<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Home extends Controller
{
    
    public function site()
    {
        $site = array(
            'template' => getConfig('default_template'),
            'root' => 'home'
        );
        return $site;
    }
    
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewIndex', 'viewAjaxIndex'),
                'groups'=>array('Administrator','Super Admin', 'Admin QA', 'Admin View', 'MOD Sushi Tei'),
            ),
            array('Deny', 
                'actions'=>array('viewIndex'),
                'groups'=>array('Guest'),
            ),
            
        );
    }
    
    public function viewIndex()
    {
        $t = $this->load->model('Ticket');
        $df = array(
            'submit' => date("Y-m-1"),
            'end' => date("Y-m-t")
        );
        $session = $this->getSession();
        $data = array(
            'site'          => $this->Site(),
            'page'          => 'Dashboard',
            'session'       => $session,
            'admin'       => $this->isAdmin(),
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/indexprp.js')
            )
        );
        if($this->isAdmin()){
            $data['result'] = $this->prpListAdmin($df);
            $this->load->template('home/index', $data);
        }else{
            $df['submit'] = date("Y-01-01");
            $df['end'] = date("Y-12-t");
            $df['storeid'] = $session['outlet'];
            $data['result'] = $this->prpListUser($df);
            #print_r($df);
            $this->load->template('home/index_user', $data);
        }
            
    }
    
    public function viewRefresh()
    {
        $this->setSession('last_activity', date("Y-m-d H:i:s"));
    }
    
    public function viewIndexDetail()
    {
        $t = $this->load->model('Ticket');
        $df = array(
            'submit' => date("Y-m-1"),
            'end' => date("Y-m-t")
        );
        $session = $this->getSession();
        $data = array(
            'site'          => $this->Site(),
            'page'          => 'Dashboard',
            'session'       => $session,
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/indexprp.js')
            )
        );

        if($session['group'] == 'Administrator' || $session['group'] == 'Super Admin' || $session['group'] == 'Admin QA'){
            $data['result'] = $this->prpListAdmin($df);
            $this->load->template('home/index', $data);
        }else{
            $data['result'] = $this->prpListUser($df);
            $this->load->template('home/index_user', $data);
        }
            
    }
    
    public function viewIndexSub()
    {
        $t = $this->load->model('Ticket');
        $df = array(
            'submit' => date("Y-m-1"),
            'end' => date("Y-m-t")
        );
        $session = $this->getSession();
        $data = array(
            'site'          => $this->Site(),
            'page'          => 'Dashboard',
            'session'       => $session,
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/indexprp.js')
            )
        );

        if($this->isAdmin()){
            $data['result'] = $this->prpListAdmin($df);
            $this->load->template('home/index', $data);
        }else{
            $data['result'] = $this->prpListUser($df);
            $this->load->template('home/index_user', $data);
        }
    }
    
    public function viewAjaxIndex()
    {
        $session = $this->getSession();
        $df = array(
            'submit' => date("Y-m-1"),
            'end' => date("Y-m-t")
        );
        $data = array();
        if(isset($_POST['date'])){
            $input = $this->load->lib('Input');
            $_POST['date'] .= '-01';
            $input->addValidation('date_req', $_POST['date'], 'min=1', 'Invalid Month Format');
            $input->addValidation('date_format', $_POST['date'], 'date', 'Invalid Month Format');        
            if($input->validate()){
                $df['submit'] = date("Y-m-1", strtotime($_POST['date']));
                $df['end'] = date("Y-m-t", strtotime($_POST['date']));
                if($this->isAdmin()){
                    $data['success'] = 1;
                    $data['data'] = $this->prpListAdmin($df);
                    
                }else{
                    $df['group'] = $session['outlet'];
                    $data['success'] = 1;
                    $data['data'] = $this->prpListUser($df);
                }
            }else{
                $data['success'] = 0;
                $data['error'] = $input->_error;
            }
        }
        echo json_encode($data);
        
    }
    
    protected function prpListAdmin($df)
    {
        $t = $this->load->model('Ticket');

        return $t->getTicketAdmin($df);
    
    }
    protected function prpListUser($df)
    {
        $t = $this->load->model('TicketGroup');
        
        return $t->getTicketGroupByStore($df);
    }
}
/*
* End Home Class
*/