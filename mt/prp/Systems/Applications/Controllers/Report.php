<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Report extends Controller
{
public function accessRules()
{
return array(
    array('Allow', 
        'actions'=>array('viewExportReport'),
        'groups'=>array('*'),
    ),
);
}

public function site()
{
$site = array(
    'root' => 'prp'
);
return $site;
}

public function viewExportReport()
{
$ticket = $this->load->model('Ticket');
$tg = $this->load->model('TicketGroup');
$trec = $this->load->model('Recommendation');
$input = $this->load->lib('Input');
if(isset($_GET['id'])){
    $input->addValidation('group_id', $_GET['id'], 'numeric', 'Must be number');
    $input->addValidation('group_id', $_GET['id'], 'min=1', 'Must be filled');
    $input->addValidation('group_id', $_GET['id'], 'max=11', 'Limit reached');
    if($input->validate()){
        
        $tr = $ticket->getTicketByGroup(array('group' => $_GET['id']));
        $trd = $trec->getRecomByGroup(array('group' => $_GET['id']));
        $gd = $tg->getTicketGroup(array('group' => $_GET['id']));
        if(is_array($gd)){
            $objPHPExcel = $this->load->lib('PHPExcel');
            
            #$objPHPExcel = new Spreadsheet();
            $objPHPExcel->getProperties()->setCreator("Sushitei IT")
                                 ->setLastModifiedBy("Sushitei IT")
                                 ->setTitle("Audit PRP Outlet")
                                 ->setSubject("Audit PRP Outlet")
                                 ->setDescription("In America.")
                                 ->setKeywords("office 2007 openxml php")
                                 ->setCategory("PRP");

            $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells('A1:I3')
                        ->setCellValue('A1', 'PRP AUDIT REPORT')
                        ->setCellValue('A5', 'TANGGAL AUDIT')
                        ->setCellValue('C5', ": {$gd['group_submit_date']}")
                        ->setCellValue('E5', 'TANGGAL VERIFIKASI')
                        ->setCellValue('F5', '-')
                        ->setCellValue('A7', 'OUTLET')
                        ->setCellValue('C7', ": {$gd['store_name']}")
                        ->setCellValue('A9', 'NAMA AUDITOR')
                        ->setCellValue('C9', ": {$gd['group_user']}")
                        ->setCellValue('E9', 'NAMA AUDITEE')
                        ->setCellValue('F9', ": {$gd['group_auditee']}")
                        ->mergeCells('A11:A12')
                        ->setCellValue('B11', 'AREA')
                        ->mergeCells('B11:B12')
                        ->setCellValue('A11', 'NO')
                        ->mergeCells('C11:C12')
                        ->setCellValue('C11', 'HASIL AUDIT')
                        ->mergeCells('D11:D12')
                        ->setCellValue('D11', 'FOTO HASIL AUDIT')
                        ->mergeCells('E11:E12')
                        ->setCellValue('E11', 'TINDAKAN PERBAIKAN')
                        ->mergeCells('F11:F12')
                        ->setCellValue('F11', 'FOTO TINDAKAN PERBAIKAN')
                        ->mergeCells('G11:G12')
                        ->setCellValue('G11', 'POINT CHECKLIST')
                        ->mergeCells('H11:H12')
                        ->setCellValue('H11', 'SCORE')
                        ->mergeCells('I11:I12')
                        ->setCellValue('I11', 'KETERANGAN');
                        
            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setShowGridlines(false);
            $A1 = $sheet->getStyle('A1');
            $A1->getAlignment()->applyFromArray(
                array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            );
            $A1->getFont()->setBold( true );
            $A11 = $sheet->getStyle('A11:I12');
            $A11->getAlignment()->applyFromArray(
                array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $A11->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
            $A11->getFont()->setBold( true );
            $first = 11;
            $last = 13;
            $num = 1;
            if(count($tr) > 0){
                foreach($tr as $data){
                    $sheet->setCellValue("B{$last}", $data['area'])
                            ->setCellValue("A{$last}", $num)
                            ->setCellValue("C{$last}", $data['notes'])
                            ->setCellValue("E{$last}", $data['resolved_solution'])
                            ->setCellValue("G{$last}", $data['category_name'])
                            ->setCellValue("H{$last}", $data['point']);
                            
                    #/** 
                    $location = 'Resources/images/prp/';
                    if(!empty($data['finding_images']) && file_exists($location.$data['finding_images'])){
                        #$gdImage = imagecreatefromjpeg($location.$data['finding_images']);
                        // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                        $objDrawing = new PHPExcel_Worksheet_Drawing();
                        $objDrawing->setName('Finding Image');
                        $objDrawing->setDescription('Finding Image');
                        $objDrawing->setPath($location.$data['finding_images']);
                        $objDrawing->setHeight(150);
                        $objDrawing->setCoordinates("D{$last}");
                        $objDrawing->setOffsetY(10);
                        $objDrawing->setOffsetX(10);
                        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                        $sheet->getRowDimension($last)->setRowHeight(150);
                        $sheet->getColumnDimension('D')->setWidth(40);
                    }
                    $location_rep = 'Resources/images/prp/reply/';
                    if(!empty($data['resolved_images']) && file_exists($location_rep.$data['resolved_images'])){
                        #$gdImage = imagecreatefromjpeg($location.$data['finding_images']);
                        // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                        $objDrawing = new PHPExcel_Worksheet_Drawing();
                        $objDrawing->setName('resolved Image');
                        $objDrawing->setDescription('Finding Image');
                        $objDrawing->setPath($location_rep.$data['resolved_images']);
                        $objDrawing->setHeight(150);
                        $objDrawing->setCoordinates("F{$last}");
                        $objDrawing->setOffsetY(10);
                        $objDrawing->setOffsetX(10);
                        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                        $sheet->getRowDimension($last)->setRowHeight(150);
                        $sheet->getColumnDimension('F')->setWidth(40);
                    }
                    #**/
                    $last++;
                    $num++;
                }
            }else{
                $sheet->mergeCells("A{$last}:I{$last}")
                      ->setCellValue("A{$last}", 'No Data Found');
                $last++;
            }
            
            if(count($trd) > 0){
                $sheet->mergeCells("A{$last}:I{$last}")
                      ->setCellValue("A{$last}", 'Rekomendasi');
                $last++;
                foreach($trd as $data){
                    $sheet->setCellValue("B{$last}", 'Rekomendasi')
                            ->setCellValue("A{$last}", $num)
                            ->setCellValue("C{$last}", $data['recommendation_content'])
                            ->setCellValue("E{$last}", $data['recommendation_feedback']);
                            
                    #/** 
                    $location = 'Resources/images/prp/';
                    if(!empty($data['recommendation_image']) && file_exists($location.$data['recommendation_image'])){
                        #$gdImage = imagecreatefromjpeg($location.$data['finding_images']);
                        // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                        $objDrawing = new PHPExcel_Worksheet_Drawing();
                        $objDrawing->setName('Finding Image');
                        $objDrawing->setDescription('Finding Image');
                        $objDrawing->setPath($location.$data['recommendation_image']);
                        $objDrawing->setHeight(150);
                        $objDrawing->setCoordinates("D{$last}");
                        $objDrawing->setOffsetY(10);
                        $objDrawing->setOffsetX(10);
                        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                        $sheet->getRowDimension($last)->setRowHeight(150);
                        $sheet->getColumnDimension('D')->setWidth(30);
                    }
                    #**/
                    $last++;
                    $num++;
                }
            }
            
            $styleBorder = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $b = $last-1;
            $tc = $sheet->getStyle("A{$first}:I{$b}");
            $sheet->getColumnDimension('C')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(20);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('I')->setWidth(20);
            $tc->applyFromArray($styleBorder);
            
            // Rename worksheet
            $objPHPExcel->getActiveSheet()->setTitle('Category');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);
            
            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename={$gd['store_name']} ".date("F Y",strtotime($gd['group_date'])).".xlsx");
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        }
    }

}
}
}
/*
* End Home Class
*/