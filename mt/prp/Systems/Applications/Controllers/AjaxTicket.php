<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class AjaxTicket extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewEditTicket', 'viewSubmit', 'viewGetTicketData'),
                'groups'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewEditTicket','viewSubmit', 'viewGetTicketData'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
    public function viewSubmit()
    {
        $data = array();
        if(isset($_POST)){
            $recaptcha = 0;
            if(getConfig('recaptcha_submit') == 1){
                $captcha = $_POST['g-recaptcha-response'];
                $secretKey = '6LemwyETAAAAABp7aR2EScUPsLoLwCSHPvGMQTpH';
                $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$captcha}&remoteip={$_SERVER['REMOTE_ADDR']}");
                $obj = json_decode($response);
                if($obj->success == 1) {
                    $recaptcha = 1;
                }
            }
            
            if((getConfig('recaptcha_submit') == 1 && $recaptcha == 1) || (getConfig('recaptcha_submit') == 0 && $recaptcha == 0)){
                $input = $this->load->lib('Input');
                
                if ($input->validate()){
                    
                    $ticket = $this->load->model('Ticket');
                    $user = $this->load->model('User');
                    
                    $_POST = $input->cleanInputArray($_POST);
                    
                    $real_field = array(
                        'ticket_title' => "{$_POST['item_code']} - {$_POST['item_name']}",
                        'item_code' => $_POST['item_code'],
                        'item_name' => $_POST['item_name'],
                        'uom' => $_POST['uom'],
                        'supplier' => $_POST['supplier'],
                        'ticket_type' => $_POST['type'],
                        'category' => $_POST['main_category'],
                        'content' => htmlspecialchars($_POST['etc_note'], ENT_QUOTES),
                        'status' => 1,
                        'rcv_qty' => $_POST['received_quantity'],
                        'order_qty' => $_POST['order_quantity'],
                        'ticket_user' => $user_detail['user_id'],
                        'ticket_creator' => $this->_mySession['user_id'],
                        'submit_date' => date("Y-m-d H:i:s"),
                        'store' => $user_detail['store_id']
                    );
                    
                    $log = array(
                        'log_type' =>  'Submit',
                        'user' => $this->_mySession['user_id'],
                        'log_title' => $this->_mySession['user_name']. " Created a new Ticket",
                        'log_content' => $real_field['content'],
                        'visible' => '1'
                    );
                    
                    if($ticket->createTicket($real_field, $log)){
                        $inserted_data = $ticket->showTicketBy(array('ticket_id' => $ticket->lastInsertID));
                        $mail_option = array(
                            'title' => "SLNC #{$ticket->lastInsertID} (NEW): {$inserted_data['ticket_title']}",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                        
                        $data = array(
                            'success' => 1,
                            'message' => 'Success.',
                            'email' => $this->send_email($mail_option, $inserted_data)
                        );
                    } else {
                        $this->showError(2);
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'error_type' => 'Validation',
                        'message' => $input->error
                    );
                }
            }elseif(getConfig('recaptcha_submit') == 1 && $recaptcha == 0){
                $data = array(
                    'success' => 0,
                    'error_type' => 'recaptcha',
                    'message' => 'Recaptcha error, please check your input'
                );
            }
            
        } else {
            $data = array(
                'success' => 0,
                'error_type' => 'Input',
                'message' => 'No input specified.'
            );
        }
        echo json_encode($data);
    }
    
    public function viewEditTicket()
    {
        $return = array();
        if(isset($_POST['submit_type'])){
            
            $result = 'failed';
            $m_ticket = $this->load->model('Tickets');
            $input = $this->load->lib('input');
            $edit_status = 0;
            
            if($_POST['submit_type'] == 'Take Over'){
               
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input->addValidation('assigned_staff', $_POST['assigned_staff'], 'numeric', 'Unknown Staff');
                $input->addValidation('a_name', $_POST['a_name'], 'alpha_numeric_sc', 'Unknown Target');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );
                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'assigned_staff' => $_POST['assigned_staff'],        
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Take Over',
                        'log_title' => $this->_mySession['user_name']. " take over the item to ". $_POST['a_name'],
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $data['input']['last_update'],
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (ASSIGN): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Comment'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Comment',
                        'log_title' => $this->_mySession['user_name']. " Commenting this ticket.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $data['input']['last_update'],
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (Comment): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Force Close'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 5,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Force-Close',
                        'log_title' => $this->_mySession['user_name']. " Closing this ticket.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $data['input']['last_update'],
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (FORCE CLOSE): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Close'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 5,
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Force-Close',
                        'log_title' => $this->_mySession['user_name']. " Closing this ticket.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $data['input']['last_update'],
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (CLOSE): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Release'){
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 2,
                            'resolved_date' => date("Y-m-d H:i:s"),
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Release',
                        'log_title' => $this->_mySession['user_name']. " Releasing the item.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $data['input']['last_update'],
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (Release): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Return'){
                
                
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 3,
                            'resolved_date' => date("Y-m-d H:i:s"),
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Return',
                        'log_title' => $this->_mySession['user_name']. " Returning the item.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $data['input']['last_update'],
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (RETURN): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }elseif($_POST['submit_type'] == 'Reject'){
                $input->addValidation('t_id', $_POST['t_id'], 'numeric', 'Unknown ID');
                $input_filtered = array(
                    'comment' => $input->cleanInput($_POST['comment']),
                );

                if($input->validate()){
                    $data = array(
                        'input' => array(
                            'status' => 4,
                            'resolved_date' => date("Y-m-d H:i:s"),
                            'last_update' => date("Y-m-d H:i:s")
                        ),
                        'where' => array(
                            'ticket_id' => $_POST['t_id']
                        )
                    );
                    
                    $log = array(
                        'log_type' =>  'Reject',
                        'log_title' => $this->_mySession['user_name']. " Rejecting the item.",
                        'user' => $this->_mySession['user_id'],
                        'ticket' => $_POST['t_id'],
                        'log_content' => $input_filtered['comment'],
                        'log_time' => $data['input']['last_update'],
                        'visible' => '1'
                    );
                    
                    if($m_ticket->editTicket($data, $log)){
                        $edit_status = 1;
                        $mail_option = array(
                            'title' => "SLNC #{$_POST['t_id']} (REJECT): ",
                            'recipient' => $this->_mySession['email'],
                            'recipient_name' => $this->_mySession['user_name'],
                        );
                    
                    }
                } else {
                    $data = array(
                        'success' => 0,
                        'message' => 'Failed.',
                        'error' => $input->_error
                    );
                }
            }
            if($edit_status == 1){
                $inserted_data = $m_ticket->showTicketBy(array('ticket_id' => $_POST['t_id']));
                
                $data = array(
                    'success' => 1,
                    'message' => 'Success.',
                    'email' => $this->send_email($mail_option, $inserted_data)
                );
                #$ticket_data = $m_ticket->showTicketBy(array('ticket_id' => $_POST['t_id']));
                #$mail_option['recipient'] = $ticket_data['email'];
                #$mail_option['recipient_name'] = $ticket_data['display_name'];
                #$mail_option['title'] .= $ticket_data['ticket_title'];
                #if($this->send_email($mail_option, $ticket_data, $m_ticket))
                #echo 'sukses';
            }
        }
        echo json_encode($data);
    
    }
    
    
    public function viewGetTicketData(){
        if(!$this->isGuest()){
            $input = $this->load->lib('Input');
            $input->addValidation('page',$_POST['page'],'numeric', 'Periksa kembali input anda');
            if(isset($_POST['main_category']) && !empty($_POST['main_category'])){
                $input->addValidation('main_category_char',$_POST['main_category'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('main_category_max',$_POST['main_category'], 'max=3', 'Periksa kembali input anda');
            }
            if(isset($_POST['ticket_type']) && !empty($_POST['ticket_type'])){
                $input->addValidation('status_char',$_POST['status'], 'numeric', 'Periksa kembali input anda');
                $input->addValidation('status_max',$_POST['status'], 'max=2', 'Periksa kembali input anda');
            }
            if(isset($_POST['store']) && !empty($_POST['store'])){
                $input->addValidation('store',$_POST['store'], 'alpha_numeric_sp', 'Please check your input');
                $input->addValidation('store_max',$_POST['store'], 'max=25', '25');
            }
            if(isset($_POST['date_start']) && !empty($_POST['date_start'])){
                $input->addValidation('date_start',$_POST['date_start'], 'date', 'Periksa kembali input anda');
            }
            if(isset($_POST['date_end']) && !empty($_POST['date_end'])){
                $input->addValidation('date_end',$_POST['date_end'], 'date', 'Periksa kembali input anda');
            }
            

            if ($input->validate()){
                $model_ticket = $this->load->model('Tickets');
                
                $option = array(
                    'page' => (isset($_POST['page']) ? $_POST['page'] : 1),
                    'result' => 10,
                    'order_by' => 'submit_date',
                    'order' => 'DESC'
                );
                
                $query = array();
                
                if($_POST['sort'] == 'title_asc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'title_desc'){
                    $option['order_by'] = 'ticket_title';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'outlet_asc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'outlet_desc'){
                    $option['order_by'] = 'store_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'submit_asc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'submit_desc'){
                    $option['order_by'] = 'submit_date';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'category_asc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'category_desc'){
                    $option['order_by'] = 'category_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'pic_asc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'pic_desc'){
                    $option['order_by'] = 'staff_name';
                    $option['order'] = 'DESC';
                }elseif($_POST['sort'] == 'status_asc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'ASC';
                }elseif($_POST['sort'] == 'status_desc'){
                    $option['order_by'] = 'status_name';
                    $option['order'] = 'DESC';
                }
                
                if(isset($_POST['ticket_title']) && !empty($_POST['ticket_title'])) $query['ticket_title'] = $_POST['ticket_title'];
                
                if(isset($_POST['main_category']) && !empty($_POST['main_category']) ) $query['category'] = $_POST['main_category'];
                
                if(isset($_POST['status']) && !empty($_POST['status'])) $query['status'] = $_POST['status'];
                
                if(isset($_POST['store']) && !empty($_POST['store'])) $query['store_name'] = $_POST['store'];
                
                if(isset($_POST['date_start']) && !empty($_POST['date_start'])) $query['submit_date'] = $_POST['date_start'];
                
                if(isset($_POST['date_end']) && !empty($_POST['date_end'])) $query['date_end'] = $_POST['date_end'];
                
                
                if($this->_mySession['role'] == 1){
                    $query['ticket_user'] = $this->_mySession['user_id'];
                    
                    $my_info = $model_ticket->myOutlet(array('user_id' => $this->_mySession['user_id']));
                    $query['store_name'] = $my_info['store_name'];
                }
               
                
                $ticket_list = $model_ticket->showAllTicketFiltered($query, $option);
                $this->setSession('last_query', $query);
                $this->setSession('option', $option);
                if(count($ticket_list) > 0){
                    $this->setSession('last_page', ceil($model_ticket->getCountResult()/$option['result']));
                    $this->setSession('current_page', $option['page']);
                    $num = (($option['page']-1)*10) + 1;
                    foreach($ticket_list as $result){
                        $date_check = '';
                        $diff_seconds  = ($result['resolved_date'] != '0000-00-00 00:00:00' ? strtotime($result['resolved_date']) - strtotime($result['submit_date']) : strtotime(date("Y-m-d H:i:s")) - strtotime($result['submit_date']));
                        $stat = floor($diff_seconds/3600);
                        
                        if(($stat >= 24 && $stat < 48) && $result['status'] < 5){
                            
                            $date_check = "warning";
                        }elseif($stat >= 48 && $result['status'] < 5){
                            $date_check = "danger";
                        }elseif($result['status'] >= 5){
                            $date_check = "success";
                        }else {
                            $date_check = 'active';
                        }
						
						$submit_date = date("d-m-Y", strtotime($result['submit_date']));
						$last_update = (!empty($result['last_update']) ? date("d-m-Y", strtotime($result['last_update'])) : '-');
                            
                        if($this->_mySession['role'] == 1){
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$num} </td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_id']}</a></td>
                        <td>{$result['ticket_title']}</td>
                        <td>{$result['supplier']}</td>
                        <td>{$result['category_name']}</td>
                        <td>{$result['staff_name']}</td>
                        <td>{$result['status_name']}</td>
                        <td>{$submit_date}</td>
                        <td>{$last_update}</td>
                    </tr>";    
                        }else{
                            echo "
                    <tr class=\"{$date_check}\">
                        <td>{$num} </td>
                        <td><a href=\"edit_ticket_{$result['ticket_id']}.html\" target=\"_blank\">{$result['ticket_title']}</a></td>
                        <td>{$result['supplier']}</td>
                        <td>{$result['store_name']}</td>
                        <td>{$result['category_name']}</td>
                        <td>{$result['staff_name']}</td>
                        <td>{$result['status_name']}</a></td>
                        <td>{$submit_date}</td>
                        <td>{$last_update}</td>
                    </tr>";
                        }                
                        $num++;
                    }
                } else {
					print_r($query);
                    echo "
                    <tr>
                        <td colspan=\"9\">Tiket Kosong</td>
                    </tr>";
                }           
            }else{
                print_r($input->_error);
            }
		} else {
            echo 'nope';
        }
        
    }
    
    
    function reArrayFiles(&$file_post) 
    {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
    
    private function send_email($option, $data)
    {
        $mail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail = $this->load->lib('STMail');
        $model = $this->load->model('Tickets');
        $log_data = $model->getLog(array('ticket_id' => $data['ticket_id']));
        $stmail->IsSMTP(); // telling the class to use SMTP
        $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
        $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $stmail->SMTPAuth   = true;                  // enable SMTP authentication
        $stmail->Host       = "mail.sushitei.co.id"; // sets the SMTP server
        $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
        $stmail->user_name   = "it1.jkt@sushitei.co.id"; // SMTP account user_name
        $stmail->Password   = "Fangblade_99";        // SMTP account password

        $stmail->SetFrom('it1.jkt@sushitei.co.id', 'SLNC Center');

        $stmail->AddReplyTo('it1.jkt@sushitei.co.id', 'SLNC Center');
        
        $stmail->isHTML('true');

        $stmail->Subject    = $option['title'];
        
        $stmail->MsgHTML($stmail->mailContent($data, $log_data));
        
        $stmail->AddAddress($option['recipient'], $option['recipient_name']);
        
        $category = $model->getCategoryDetail(array('input' => $data['category_id']));
        $recipient = explode(";", $category['description']);
        
        foreach($recipient as $address){
            #echo "addressnya adalah {$address} <br/>";
            $stmail->AddBCC($address, $data['category_name']);
        }
        
        #$stmail->AddBCC('purchteam.jkt@sushitei.co.id', 'Team Purchasing');
        #$stmail->AddBCC('oktavianus@sushitei.co.id', 'Oktavianus');
        #$stmail->AddBCC('m.chandra@sushitei.co.id', 'M. Chandra');
        #$stmail->AddBCC('dadang@sushi-kiosk.com', 'Dadang');
        #$stmail->AddBCC('arief@sushi-kiosk.com', 'Arief');
        
        return ($stmail->Send() ? 1 : 0);
            
        
    }
}
/*
* End Home Class
*/