<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class User extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewIndex', 'viewLogin', 'viewLoginProccess', 'viewLogout', 'viewDeleteUser'),
                'role'=>array('*'),
            ),
            array('Deny', 
                'actions'=>array('viewIndex', 'viewLogout'),
                'groups'=>array('Guest'),
            ),
            
        );
    }
    
    public function viewIndex()
    {
        echo 'Here';
            
    }
    
    public function viewLogin()
    {
        if($this->isGuest()){
            $data = array(
                'session' => $this->_session['login_info'],
                '_template_path' => 'Templates/'.getConfig('default_template')
            ) ;
            $this->load->view('users/login', $data);
            $this->load->render();
        }else{
            header("Location: index.php");
        }
    }
    
    public function viewLoginProccess()
    {
        if($this->isGuest()){
            $current_attempt = $this->_session['login_info']['attempt'];
            $recaptcha = 1;
            if($current_attempt > 5){
                #
                # Verify captcha
                $post_data = http_build_query(
                    array(
                        'secret' => '6LemwyETAAAAABp7aR2EScUPsLoLwCSHPvGMQTpH',
                        'response' => $_POST['g-recaptcha-response'],
                        'remoteip' => $_SERVER['REMOTE_ADDR']
                    )
                );
                $opts = array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $post_data
                    )
                );
                $context  = stream_context_create($opts);
                $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
                $result = json_decode($response);
                if (!$result->success) {
                    $recaptcha = 0;
                }
            }
            if(isset($_POST['ticket_email']) && isset($_POST['ticket_password']) && $recaptcha == 1){
                $input = $this->load->lib('Input');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'min=1', 'User name should be filled');
                $input->addValidation('ticket_email', $_POST['ticket_email'], 'username', 'Wrong User Name format');
                $input->addValidation('ticket_password', $_POST['ticket_password'], 'min=1', 'Password must be filled');
                if ($input->validate()) {
                    $login_data = array(
                        'user_name' => $_POST['ticket_email'],
                        'user_password' => MD5($_POST['ticket_password'].getConfig('salt')),
                    );
                    $model_users = $this->load->model('User');
                    $result = $model_users->doLogin($login_data);
                    if(is_array($result) && count($result) > 0){
                        $s = array(
                            'userid'  => $result['user_id'],
                            'username'     => $result['display_name'],
                            'outlet'   => $result['store_id'],
                            'group'      => $result['group_name'],
                            'role'      => $result['level'],
                            'email'      => $result['email'],
                            'attempt'      => 0
                        );
                        $this->setSession('login_info', $s);
                        
                        $return = array(
                            'success' => 1,
                            'last_url' => $_POST['last_url']
                        );
                    } else {
                        $current_attempt++;
                        $this->setSession('attempt', $current_attempt);
                        $return = array(
                            'login_attempt' => $current_attempt,
                            'message' => "Username or Password does not match.",
                            'success' => 0
                        );
                        
                    }
                } else {
                    $current_attempt++;
                    $this->setSession('attempt', $current_attempt);
                    $return = array(
                        'login_attempt' => $current_attempt,
                        'error' => $input->_error,
                        'success' => 0
                    );
                }
            
            } else {
                $current_attempt++;
                $return = array(
                    'login_attempt' => $current_attempt,
                    'success' => 0,
                    'data' => $_POST
                );
                
            }
        
            echo json_encode($return);
        }else{
            $return = array(
                'success' => 1
            );
             echo json_encode($return);
        }
    }
    
    public function viewLogout()
    {        
        $this->unsetSession('login_info');
        $this->unsetSession('pending');
        $s = array(
            'username'  => 'Guest',
            'group'     => 'Guest',
            'attempt'   => 0,
            'role'      => 1
        );
        $this->setSession('login_info', $s);
        header("Location: login.html");
    }

    #delete User
    public function viewDeleteUser()
    {
        $id = $_GET['idUser'];
        $model_users = $this->load->model('User');
        $model_users->deleteUser($id);
        header('Location: admin_user.html');
    }

}
/*
* End Home Class
*/