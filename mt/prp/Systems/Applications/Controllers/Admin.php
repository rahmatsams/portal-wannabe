<?php
class Admin extends Controller
{
    public function accessRules()
    {
        return array(
            array('Deny', 
                'actions'=>array('index', 'edit_user'),
                'groups'=>array('Guest'),
                
            ),
            array('Allow', 
                'actions'=>array('viewIndex', 'viewStore_index', 'viewNewStore', 'viewEditStore', 'viewUser', 'viewCreateUser', 'viewDropUser', 'viewCategory', 'viewCreateCategory', 'viewManageCategory', 'viewManageSub', 'viewManageStoreArea', 'viewDeleteCategory', 'viewDeleteSubCategory', 'viewDeleteStore', 'viewEditUser', 'viewUserback'),
                'groups'=>array('Super Admin', 'Administrator' , 'Admin QA'),
            )
        );
    }
    
    protected function site()
    {
        $site = array(
            'root' => 'admin'
        );
        return $site;
    }
    
    function viewIndex()
    {
        $session = $this->getSession();
        $data = array(
            'site' => $this->Site(),
            'page' => 'Admin Page',
            'session'   => $session,
            'admin'      => $this->isAdmin(),
        );
        $this->load->template('admin/index', $data);
    }
    
    #CRUD Store
    function viewStore_index()
    {
        
        $model_store = $this->load->model('Store');
        $session = $this->getSession();
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'store_name',
            'order' => 'ASC'
        );
        $data = array(
            'session'       => $session,
            'admin'      => $this->isAdmin(),
            'store' => $model_store->getAllStore($query_option),
            'total' => $model_store->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'],
        );
        $this->load->template('admin/store/index', $data);
    }
    
    #ADD NEW STORE
    function viewNewStore(){
        
        $model_store = $this->load->model('Store');
        $data = array(
            'session' => $this->_session['login_info'],
            'admin' => $this->isAdmin(),
            'location_list' => $model_store->getAllActiveLocation()
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'create_store'){
            $input = $this->load->lib('Input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_email', $_POST['store_email'], 'min=1', 'Must be filled');
            $input->addValidation('store_email',$_POST['store_email'],'email', 'Invalid email format');
            #$input->addValidation('store_email', $_POST['store_email'], 'username', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_email' => $_POST['store_email'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->newStore($store)){
                    header("Location: admin_store.html");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/store/add', $data);
            }
        }else{
            $this->load->template('admin/store/add', $data);
        }
        
    }
    
    function viewEditStore(){
        $model_store = $this->load->model('Store');
        $session = $this->getSession();
        $input = $this->load->lib('Input');
        $data = array(
            'session' => $this->$session,
            'admin'      => $this->isAdmin(),
            'admin' => $this->isAdmin()
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_store'){
            $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
            $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
            $input->addValidation('store_name', $_POST['store_name'], 'alpha_numeric_sp', 'Only accept alpha numeric and space character');
            $input->addValidation('store_name', $_POST['store_name'], 'min=1', 'Must be filled');
            $input->addValidation('store_name', $_POST['store_name'], 'max=50', 'Exceeding 50 characters');
            $input->addValidation('store_location', $_POST['store_location'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_location', $_POST['store_location'], 'min=1', 'Check your input');
            $input->addValidation('store_location', $_POST['store_location'], 'max=3', 'Check your input');
            $input->addValidation('store_email', $_POST['store_email'], 'min=1', 'Must be filled');
            $input->addValidation('store_email',$_POST['store_email'],'email', 'Invalid email format');
            $input->addValidation('store_status', $_POST['store_status'], 'numeric', 'Only numeric characer allowed');
            $input->addValidation('store_status', $_POST['store_status'], 'min=1', 'Check your input');
            $input->addValidation('store_status', $_POST['store_status'], 'max=1', 'Check your input');
            if($input->validate()){
                $store = array(
                    'store_name' => $_POST['store_name'],
                    'store_email' => $_POST['store_email'],
                    'store_status' => $_POST['store_status'],
                    'location_id' => $_POST['store_location']
                );
                if($model_store->editStore($store, array('store_id' => $_GET['id']))){
                    header("Location: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
                }
            }else{
                $data['error'] = $input->_error;
                $this->load->template('admin/store/edit', $data);
            }
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'Please check your input');
                $input->addValidation('id', $_GET['id'], 'max=3', 'Please check your input');
                if($input->validate()){
                    $data['store'] = $model_store->getStoreBy(array('store_id' => $_GET['id']));
                    if(is_array($data['store']) && count($data['store']) > 1){
                        $data['location_list'] = $model_store->getAllActiveLocation();
                        $this->load->template('admin/store/edit', $data);
                    }
                }
                
            }
        }
        
    }
    
    /* CRUD User */
    
    public function viewUserback()
    {
        $m_users = $this->load->model('User');
        $query_option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'user_name',
            'order' => 'ASC'
        );
        $data = array(
            'site' => $this->site(),
            'session' => $this->_session['login_info'],
            'users' => $m_users->getAllUser($query_option),
            'total' => $m_users->getCountResult(),
            'page' => $query_option['page'],
            'max_result' => $query_option['result'], 
            'groups' => $m_users->getAllGroups(),
            'admin' => $this->isAdmin()
        );
        $option = array(
            'personal_js' => 1,
            'scriptload' => '
            <script type="text/javascript">
                $(document).ready(function(){
                    $(".edit-users").click(function(event){
                        event.preventDefault();
                        classid = this.id;
                        getUser(classid);
                    });
                });
               
            </script>
            '
        );
        $this->load->template('admin/user/index', $data, $option);
    }
    
    public function viewUser()
    {
        $m_users = $this->load->model('User');
        $session = $this->getSession();
        $option = array(
            'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
            'result' => 10,
            'order_by' => 'user_name',
            'order' => 'ASC'
        );
        
        $data = array(
            'session' => $session,
            'page' => $option['page'],
            'current_page' => (!empty($_GET['page']) ? $_GET['page'] : 1),
            'groups' => $m_users->getAllGroups(),
            'max_result' => $option['result'],
            'admin' => $this->isAdmin(),
            'form' => false
        );
        
        
        if(isset($_POST['submit']) && $_POST['submit'] == 'search')
        {

            $input = $this->load->lib('Input');
      
            if(isset($_POST['user_name']) && !empty($_POST['user_name'])) $input->addValidation('user_name', $_POST['user_name'] ,'username','Please use only alphanumeric character for user name');
            if(isset($_POST['display_name']) && !empty($_POST['display_name'])) $input->addValidation('display_name', $_POST['display_name'] ,'alpha_numeric','Please use only alphanumeric character for display name');
            if(isset($_POST['user_type']) && !empty($_POST['user_type'])) $input->addValidation('user_type', $_POST['user_type'] ,'numeric','Please input proper user type for user type');
            if(isset($_POST['user_status']) && !empty($_POST['user_status'])) $input->addValidation('user_status', $_POST['user_status'] ,'boolean','Please input proper user type for user status');
            
            if ($input->validate())
            {
                $search_array = array(
                    'page' => array(
                        'viewUser' => array(
                            'user_name' => array(
                                'table' => 'users',
                                'condition' => 'LIKE',
                                'value' => $input->form('user_name')
                            ),
                            'display_name' => array(
                                'table' => 'users',
                                'condition' => 'LIKE',
                                'value' => $input->form('display_name')
                            ),
                            'group_id' => array(
                                'table' => 'users',
                                'condition' => '=',
                                'value' => $input->form('user_type')
                            ),
                            'user_status' => array(
                                'table' => 'users',
                                'condition' => '=',
                                'value' => $input->form('user_status')
                            )
                        )
                    )
                );
                
            
                $this->setSession('search', $search_array);
            }else{
                $data['error'] = $input->showError();
            }
        }
    

        if(!isset($data['error']))
        {
            if(!empty($this->getSession()['search']['page']['viewUser'])) $data['form'] = $this->getSession()['search']['page']['viewUser'];
            
            $data['users'] = $m_users->getAllUser($data['form'], $option);
            $data['total'] = $m_users->getCountResult();
        }else{
            $data['users'] = array();
            $data['total'] = 0;
        }
        #print_r($data['form']);exit;
        $this->load->template('admin/user/index', $data);
        
    }

    public function viewCreateUser()
    {
        $m_users = $this->load->model('User');
        $data = array(
            'session' => $this->_session,
            'admin' => $this->isAdmin(),
            'groups' => $m_users->getAllGroups(),
            'outlet_list' => $m_users->getAllActiveStore(),
            'admin' => $this->isAdmin()
        );
        if(isset($_POST['submit']) && $_POST['submit'] == 'new_user'){
            $input = $this->load->lib('Input');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Please check your input');
            $input->addValidation('user_name',$_POST['user_name'],'min=1', 'cannot be blank');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Please check your input');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Cannot be blank');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('new_password', $_POST['new_password'],'min=6', 'Password lenght should be greater than 6 character');
            $input->addValidation('confirm_password', $_POST['confirm_password'],'like='.$_POST['new_password'], 'Password confirmation is different');
            $input->addValidation('store_id', $_POST['store_id'],'min=1', 'Cannot be blank');
            $input->addValidation('store_id', $_POST['store_id'],'numeric', 'Please check your input');
            $input->addValidation('group_id', $_POST['group_id'],'min=1', 'Cannot be blank');
            $input->addValidation('group_id', $_POST['group_id'],'numeric', 'Please check your input');
            if($input->validate()){
                $insert_value = array(
                    'new_user' => array(
                        'user_name' => $_POST['user_name'],
                        'user_password' => MD5($_POST['new_password'].getConfig('salt')),
                        'display_name' => $_POST['display_name'],
                        'email' => $_POST['email'],
                        'store_id' => $_POST['store_id'],
                        'group_id' => $_POST['group_id'],
                        'user_status' => $_POST['user_status']
                    )
                );
                if($m_users->checkUserName($_POST['user_name'])){
                    if($m_users->newTicketUser($insert_value)){
                        header("Location: admin_user.html");
                        
                    } else {
                        exit('Unexpected Error');
                    }
                }else{
                    $data['error']['user_name'] = 'is already used';
                }
            } else {
                $data['error'] = $input->_error;
            }
            $data['last_input'] = $_POST;
            
        }
        
        $this->load->template('admin/user/add', $data);
        
    }
    
    public function viewEditUser()
    {
        $account = $this->load->model('User');
        $data = array(
            'session' => $this->_session,
            'admin' => $this->isAdmin(),
            'groups' => $account->getAllGroups(),
            'outlet_list' => $account->getAllActiveStore(),
            'admin' => $this->isAdmin()
        );
        
        $input = $this->load->lib('Input');
        if(isset($_POST['submit']) && $_POST['submit'] == 'edit_user' ){
            $input->addValidation('user_id',$_POST['user_id'], 'numeric', 'Cek kembali input anda');
            $input->addValidation('user_id',$_POST['user_id'], 'max=4', 'Cek kembali input anda');
            $input->addValidation('user_name',$_POST['user_name'],'username', 'Please check your input');
            $input->addValidation('user_name',$_POST['user_name'],'min=1', 'cannot be blank');
            $input->addValidation('display_name',$_POST['display_name'],'alpha_numeric_sc', 'Only Accept numeric and t');
            $input->addValidation('display_name',$_POST['display_name'],'min=1', 'Cannot be blank');
            $input->addValidation('email',$_POST['email'],'email', 'Invalid email format');
            $input->addValidation('store_id', $_POST['store_id'],'min=1', 'Cannot be blank');
            $input->addValidation('store_id', $_POST['store_id'],'numeric', 'Please check your input');
            $input->addValidation('group_id', $_POST['group_id'],'min=1', 'Cannot be blank');
            $input->addValidation('group_id', $_POST['group_id'],'numeric', 'Please check your input');
            if(!empty($_POST['new_password'])){
                $input->addValidation('new_password', $_POST['new_password'], 'min=6', 'Password minimal 6 huruf');
                $input->addValidation('confirm_password', $_POST['confirm_password'], 'like=' . $_POST['new_password'], 'Password konfirmasi harus sama dengan password awal');
            }
            $data['user'] = $account->getUserBy(array('user_id' => $_POST['user_id']));
            if($input->validate()){
                $input_data = array(
                    'users' => array(
                        'user_name' => $_POST['user_name'],
                        'display_name' => $_POST['display_name'],
                        'group_id' => $_POST['group_id'],
                        'store_id' => $_POST['store_id'],
                        'email' => $_POST['email'],
                        'user_status' => $_POST['user_status'],
                    ),
                    'where' => array(
                        'user_id' => $_POST['user_id']
                    ),
                );
                if(!empty($_POST['new_password'])){
                    $input_data['users']['user_password'] = md5($_POST['new_password'].getConfig('salt'));
                }
                if($account->editTicketUser($input_data)){
                    $data['user'] = $account->getUserBy(array('user_id' => $_POST['user_id']));
                    $data['success'] = 1;
                }else{
                    $data['success'] = 0;
                }
            }else{
                $data['error'] = $input->_error;
            }
            $this->load->template('admin/user/edit', $data);
                
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id', $_GET['id'], 'numeric', 'User not found');
                $input->addValidation('id', $_GET['id'], 'max=6', 'User not found');
                if($input->validate()){
                    $data['user'] = $account->getUserBy(array('user_id' => $_GET['id']));
                    if($data['user']['level'] > $this->_session['login_info']){
                        exit('you dont have enough privilege');
                    }else{
                        $this->load->template('admin/user/edit', $data);
                    }
                }
            }
            
        }
    }
    
    public function viewDropUser(){
        if(isset($_POST['rem_user'])){
            $input = $this->load->lib('Input');
            $input->addValidation('id',$_POST['id'], 'numeric', 'Periksa kembali input anda');
            if($input->validate()){
                if($this->load_model->deleteUser($_POST)){
                    echo 'sukses';
                }
            }else{
                print_r($input->_error);
            }
        }
    }
    
    // CRUD Category // 
    public function viewCategory()
    {
        $m_storea = $this->load->model('StoreArea');
        #$m_category = $this->load->model('TicketCategory');
        $data = array(
            'site' => $this->Site(),
            'session' => $this->_session,
            'admin' => $this->isAdmin(),
            'page' => 'Manage PRP',
            'store_area' => $m_storea->getAllAreaByPoint(),
            'option' => array(
                'exjs' => array(
                    './Resources/js/admin_category.js'
                )
            ),
            'admin' => $this->isAdmin()
        );
        $this->load->template('admin/category/index', $data);
    }
    
    #Edit Store Area
    public function viewManageStoreArea() 
    {
        $input = $this->load->lib('Input');
        $m_storea = $this->load->model('StoreArea');
        $m_category = $this->load->model('TicketCategory');
        $data = array(
            'site' => $this->site(),
            'session' => $this->_session,
            'admin' => $this->isAdmin(),
            'page' => 'Manage PRP',
            'fdata' => $this->getFlashData(),
            'admin' => $this->isAdmin()
        );
        $opt = array(
            'order_by' => 'area_id', #was category_id
            'order' => 'DESC' 
        );
        if (isset($_POST['action'])) {
            $data = array(
                'success' => 0 
            );
            $parent = $_POST['area_id'];
            if($_POST['action'] == 'Add Point'){
                $input->addValidation('parent_max', $_POST['area_id'], 'max=6', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['area_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('category_name', $_POST['category_name'], 'min=4', 'Cek kembali input anda');
                if($input->validate()){
                    $m_ticketcategory = $this->load->model('TicketCategory');
                    unset($_POST['action']);
                    if($m_ticketcategory->createCategory($_POST)){
                        $data['result'] = $m_storea->getStoreAreaId(array('sid' => $_POST['area_id']));
                        $data['list'] = $m_category->getSubCategory($_POST['category_id'], $opt);
                        $this->load->template('admin/category/manage', $data);
                    } else {
                        echo 'failed';
                    }
                }else{
                    echo 'failed';
                }
            }elseif($_POST['action'] == 'Update'){
                $input->addValidation('area_name', $_POST['area_name'], 'min=3', 'Cek kembali input anda');
                if($input->validate()){
                    $m_storea = $this->load->model('StoreArea');
                    unset($_POST['action']);
                    if($m_storea->editStoreArea($_POST, array('area_id' => $_POST['area_id']))){
                        $data['success'] = 1;
                        $parent = $_POST['area_id'];
                    } else {
                        $data['success'] = 0;
                        $data['error'] = 'Unknown error.';
                    }
                }else{
                    $data['success'] = 0;
                    $data['error'] = $input->_error;
                }
            }
            $this->setFlashData($data);
            header("Location: ".controllerUrl('Admin','manageStoreArea', array('id' => $parent)));
        }else{
            if(isset($_GET['id'])){
                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                if($input->validate()){
                    $data['option'] = array(
                        'exjs' => array('./Resources/js/adminprp.js')
                    );
                    $data['result'] = $m_storea->getStoreAreaId(array('sid'=>$_GET['id']));
                    $data['list'] = $m_category->getSubCategory($_GET['id'], $opt);
                    $this->load->template('admin/category/manage', $data);
                } else {
                    $this->showError(2);
                }
            }else{
                $this->showError(2);
            }
        }
    }

    public function viewCreateCategory()
    {
        if(isset($_POST['submit']) && $_POST['submit'] == 'kirim'){
            $input = $this->load->lib('input');
            $input->addValidation('area_name', $_POST['area_name'], 'min=3', 'Cek kembali input anda');
            if(isset($_POST['sub_category'])){
                $input->addValidation('area_id', $_POST['area_id'], 'numeric', 'Cek kembali input anda');
            }
            $input->addValidation('description_min', $_POST['area_desc'], 'min=1', 'Cek kembali input anda');
            if($input->validate()){
                unset($_POST['submit']);
                if(!isset($_POST['area_id'])){
                    $_POST['area_id'] = 0;
                }
                $m_storea = $this->load->model('StoreArea');
                if($m_storea->createStoreArea($_POST)){
                    header("Location: admin_category.html");
                } else {
                    exit('Failed, press back to return');
                }
            }else{
                echo json_encode($input->_error);
            }
            
        }
    }
    
    
    #add new point
    public function viewManageSub() 
    {
        $input = $this->load->lib('Input');
        $m_category = $this->load->model('TicketCategory');
        $data = array(
            'admin' => $this->isAdmin(),
            'session' => $this->_session
        );
        $opt = array();
        if(isset($_POST['action'])){
            $data = array(
                'success' => 0
            );
            /* Fail over if parent is not found */
            $parent = 1;
            if($_POST['action'] == 'Add Point'){ #value button add new point
                $input->addValidation('parent_max', $_POST['area_id'], 'max=6', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['area_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['description'], 'min=0', 'Cek kembali input anda');
                $input->addValidation('category_max', $_POST['category_name'], 'min=1', 'Cek kembali input anda');
                if($input->validate()){
                    unset($_POST['action']);
                    if($m_category->createCategory($_POST)){
                        $data['success'] = 1;
                        $parent = $_POST['area_id'];
                    } else {
                        $data['error'] = 'Unknown Error';
                    }
                }else{
                    $data['error'] = $input->_error;
                }
            }elseif($_POST['action'] == 'Edit'){
                $input->addValidation('category_max', $_POST['category_name'], 'min=1', 'Cek kembali input anda');
                $input->addValidation('parent_format', $_POST['area_id'], 'numeric', 'Cek kembali input anda');
                $input->addValidation('description', $_POST['description'], 'min=0', 'Cek kembali input anda');
                $input->addValidation('point_deduction', $_POST['point_deduction'], 'min=1', 'Cek kembali input anda');
                if($input->validate()){
                    $m_ticketcategory = $this->load->model('TicketCategory');
                    $parent = $_POST['area_id'];
                    unset($_POST['action']);
                    unset($_POST['area_id']);
                    if($m_ticketcategory->editCategory($_POST, array('category_id' => $_POST['category_id']))){
                        $data['success'] = 1;
                    } else {
                        $data['error'] = 'Unknown Error';
                    }
                }else{
                    $data['error'] = $input->_error;
                }
            }
            $this->setFlashData($data);
            header("Location: ".controllerUrl('Admin','ManageStoreArea', array('id' => $parent)));
        }else{
            
            if(isset($_GET['id'])){
                $input->addValidation('id_length', $_GET['id'], 'max=6', 'Excedding allowed range');
                $input->addValidation('id_format', $_GET['id'], 'numeric', 'Excedding allowed range');
                if($input->validate()){
                    $data['option'] = array(
                        'exjs' => array('./Resources/js/adminprp.js')
                    );
                    $data['result'] = $m_category->getSubCategoryId(array('cid'=>$_GET['id']));
                    #$data['list'] = $m_category->getSubCategory($_GET['id'], $opt);
                    $this->load->template('admin/category/manage_sub', $data);
                } else {
                    $this->showError(2);
                }
            }else{
                $this->showError(2);
            }
        } 
    }

    #deleteCategory
    public function viewDeleteCategory()
    {
        $id = $_GET['idCategory'];
        $m_storea = $this->load->model('StoreArea');
        $m_storea->deleteCategory($id);
        header('Location: admin_category.html');
    }

    #deleteSubCategory
    public function viewDeleteSubCategory()
    {
        $id = $_GET['idSubCat'];
        $m_ticketcategory = $this->load->model('TicketCategory');
        $m_ticketcategory->deleteSubCategory($id);
        header('Location: admin_category.html');
    }

    #deleteStore
    public function viewDeleteStore()
    {
        $id = $_GET['idStore'];
        $model_store = $this->load->model('Store');
        $model_store->deleteStore($id);
        header('Location: admin_store.html');
    }

}
?>