<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Default Controller
*/
class Ticket extends Controller
{
    public function accessRules()
    {
        return array(
            array('Allow', 
                'actions'=>array('viewSubmit', 'viewSubmitUpload', 'viewDeletePending', 'viewProccessSubmit', 'viewEmail', 'viewRecommend', 'viewDeleteRecommend', 'viewAddFeedback', 'viewDeletePRP', 'viewEditRecommend'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin QA'),
            ),
            array('Allow', 
                'actions'=>array('viewEdit', 'viewTicket', 'viewGroup', 'viewRecommend', 'viewEditRecommend', 'viewReply'),
                'groups'=>array('Super Admin', 'Administrator', 'Admin QA', 'MOD Sushi Tei', 'Admin View'),
            ),
            array('Deny', 
                'actions'=>array('viewEdit', 'viewSubmit', 'viewSubmitUpload', 'viewTicket', 'viewDeletePending', 'viewProccessSubmit','viewGroup','viewExportReport'),
                'groups'=>array('Guest'),
            ),
        );
    }
    
	public function site()
    {
        $site = array(
            'root' => 'prp'
        );
        return $site;
    }
	
    public function viewSubmit()
    {
        $ticket = $this->load->model('Ticket');
        $category = $this->load->model('TicketCategory');
        $area = $this->load->model('StoreArea');
        $sess = $this->getSession();
        $data = array(
			'site' 			=> $this->Site(),
            'page' 			=> 'Submit PRP',
            'session' 		=> $sess,
            'admin' 		=> $this->isAdmin(),
            'category_list' => $category->getAllCategory(),
			'area'			=> $area->getAllArea(),
            'category'      => $area->getAllAreaCategory(),
			'store_list'	=> $ticket->getAllActiveStore(),
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/prpscript.js')
            )
        );
        #upload gambar
        if(isset($_POST['prp_submit']) && $_POST['prp_submit'] == "Submit"){
            $form = array();
            foreach($_POST['link'] as $k => $v){
                $form[$k]['category'] = $v;
                $form[$k]['prp_date'] = $_POST['date']."-01";
                $form[$k]['store'] = $_POST['store'];
                $form[$k]['status'] = (isset($_POST['status'][$k]) ? 0 : 1);
                $form[$k]['ticket_creator'] = $sess['user_id'];
                $form[$k]['submit_date'] = date("Y-m-d");
                if(isset($_POST['notes'][$k]) && !empty($_POST['notes'][$k])) $form[$k]['notes'] = $_POST['notes'][$k];
                
                if(isset($_FILES['image_upload']['error'][$k]) && $_FILES['image_upload']['error'][$k] == 0){
                    $form[$k]['file'][0] = array(
                        'name' => $_FILES['image_upload']['name'][$k],
                        'type' => $_FILES['image_upload']['type'][$k],
                        'tmp_name' => $_FILES['image_upload']['tmp_name'][$k],
                        'error' => $_FILES['image_upload']['error'][$k],
                        'size' => $_FILES['image_upload']['size'][$k],
                    );
                }
            }
            #exit(print_r($_POST));
            if(count($form) > 0){
                $gid = $ticket->checkTicketGroup(array('store' => $_POST['store'], 'prp_date' => $_POST['date']."-01", 'uid' => $sess['user_name'], 'auditee' => $_POST['auditee']));
                foreach($form as $post){
                    $data['submit'][$post['category']] = $this->proccessSubmit($post, $gid);
                }
                if(count($form) > 0){
                    $tg = $this->load->model('TicketGroup');
                    $tg->updateGroupByTicket($gid['group_id']);
                    echo "<script>alert('Submit Success'); window.location.replace('index.html');</script>";
                }
            }
        }
        
        $this->load->template('ticket/submit_new', $data);
        
    }
    
    
    public function viewGroup()
    {
        if(isset($_POST['month']) && isset($_POST['outlet'])){
            $input = $this->load->lib('Input');
            $submitted = array(
                'group_store'  => $_POST['outlet'],
                'group_date' => $_POST['month'].'-01'
            );
            $input->addValidation('month_format', $submitted['group_date'], 'date', 'Unknown Format');
            $input->addValidation('store_format', $submitted['group_store'], 'numeric', 'Unknown Format');
            $input->addValidation('store_req', $submitted['group_store'], 'min=1', 'Should be filled');
            $input->addValidation('store_max', $submitted['group_store'], 'max=6', 'Should be filled');
            if($input->validate()){
                $ticket = $this->load->model('Ticket');
                $group = $ticket->verifyTicketGroup($submitted);
                $display = array(
                    'exists' => 0,
                );
                if(is_array($group)){
                    $display = array(
                        'exists' => 1,
                        'url' => getConfig('base_url')."edit_prp_{$group['group_id']}.html",
                        'data' => $group
                    );
                }
                echo json_encode($display);
            }else{
                $display = array(
                    'error' => 1,
                    'message' => $input->_error
                );
                echo json_encode($display);
            }
        }else{
            print_r($_POST);
            exit('Unknown Error');
        }
        
    }
    
    public function viewDeletePending()
    {
        $sess = $this->getSession();
        if(isset($_POST['submit']) && $_POST['submit'] == 'Delete'){
            $input = $this->load->lib('Input');
            $input->addValidation('upload_id', $_POST['upload_id'] ,'min=1','Upload info not found.');
            $input->addValidation('upload_id', $_POST['upload_id'] ,'numeric','Upload info not found.');
            $input->addValidation('upload_id', $_POST['upload_id'] ,'max=6','Upload info not found.');
            if($input->validate()){
                $m = $this->load->model('Upload');
                $mi = $m->getUploadInfo(array('upload_id' => $_POST['upload_id']));
                $dd = array(
                    'upload_id' => $_POST['upload_id'],
                    'upload_temp' => 1
                );
                if(is_array($mi)){
                    if($m->deletePending($dd)){
                        unlink("{$mi['upload_location']}/{$mi['upload_name']}");
                        $pending = $m->getPendingUpload(array('user_id' => $sess['user_id']));
                        $this->setSession('pending', $pending);
                        if(count($pending) > 0){
                            header("Location: submit_upload.html");
                        }else{
                            header("Location: submit_prp.html");
                        }
                    }else{
                        exit('Unexpected Error');
                    }
                }
            }
        }
    }

    #Delete PRP (delete ticket)
    public function viewDeletePRP()
    {
        $id = $_GET['idTicketGroup'];
        $tg = $this->load->model('TicketGroup');
        $gg = $this->load->model('Ticket');
        if(is_array($tg->getTicketGroup(array('group' => $_GET['idTicketGroup']))))
        {
            $ug = $gg->checkFindingImage(array('ticket_group'=> $_GET['idTicketGroup']));
            foreach($ug AS $img){
                $file = "Resources/images/prp/".$img['finding_images'];
                if (file_exists($file) && is_file($file))
                {
                    unlink($file);               
                }
            }
            $tg->deletePRP($id);
            header('Location: index.html');
        }else{
            exit('No data');
        }
    }    
    

    #Ticket
    public function viewTicket()
    {
        if(isset($_GET['n'])){
            $ticket = $this->load->model('Ticket');
            $tg = $this->load->model('TicketGroup');
            $session = $this->_session;
            $data = array(
                'site' 			=> $this->Site(),
                'page' 			=> 'View PRP',
                'session' 		=> $session,
                'admin' 		=> $this->isAdmin(),
                'option' 		=> array(
                    'injs' => array(),
                    'exjs' => array('Resources/js/prplist.js')
                )
            );
            $input = $this->load->lib('Input');
            $input->addValidation('idformat', $_GET['n'], 'numeric', 'Unknown Error');
            $input->addValidation('idlen', $_GET['n'], 'min=1', 'Unknown Error');
            $input->addValidation('idmax', $_GET['n'], 'max=6', 'Unknown Error');
            if($input->validate()){
                $data['prp'] = $tg->getTicketGroup(array('group' => $_GET['n']));
                $data['sub'] = $ticket->getTicketByGroup(array('group'=>$_GET['n']));
                if(is_array($data['prp'])){
                    if($session['group'] == 'Super Admin' || $session['group'] == 'Administator' || $session['group'] == 'Admin QA'){
                        $this->load->template('ticket/view', $data);
                    }else{
                        $this->load->template('ticket/view_user', $data);
                    }
            }
            }else{
                $this->showError(2);
            }
            
        }else{
            $this->showError(2);
        }   
    }
    
    public function viewSubmitUpload()
    {
        $ticket = $this->load->model('Ticket');
        $sess = $this->getSession();
        $area = $this->load->model('StoreArea');
        $data = array(
			'site' 			=> $this->Site(),
            'page' 			=> 'Upload',
            'session' 		=> $sess,
            'category_list' => $ticket->getMainCategory(),
            'type' 			=> $ticket->getTicketType(),
			'area'			=> $area->getAllArea(),
			'store_list'	=> $ticket->getAllActiveStore(),
            'admin' 		=> $this->isAdmin(),
            'pending'       => $sess['pending'],
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/prpscript.js')
            )
        );
        $uploaded = '';
      
        $this->load->template('ticket/pending', $data);
    }
    
    protected function proccessSubmit($POST, $gid)
    {
        $data = array();
        if(1 == 1) {
            $input = $this->load->lib('Input');
            $input->addValidation('store', $POST['store'] ,'min=1','Outlet is must be filled.');
            $input->addValidation('store', $POST['store'] ,'numeric','Invalid Outlet');
            $input->addValidation('date', $POST['prp_date'],'date','Invalid Month Format');
            $input->addValidation('category', $POST['category'],'numeric','Invalid Category');
            if(isset($_POST['sub']) && !empty($POST['sub'])){
                $input->addValidation('sub', $POST['sub'],'numeric','Invalid Point');
            }
            
            if ($input->validate()){

                $ticket = $this->load->model('Ticket');

                $mc = $this->load->model('TicketCategory');
                
                $ci = $mc->getCategoryFromLink(array('cid' => $POST['category']));
                
                $POST['ticket_group'] = $gid['group_id'];
                
                if($POST['status'] == 1 && isset($POST['file'])){
                    $file = $POST['file'];
                    unset($POST['file']);
                    
                    $upload = $this->doUpload($file);
                    if(!isset($upload[0]['error'])){
                        
                        $POST['finding_images'] = $upload[0]['upload_name'];
                        
                    }
                    
                }
                if($ticket->createTicket($POST)){
                    $data['status'] = 1;
                    
                } else {
                    $data['error']['unknown'] = 'Unknown Error';
                }                    
            } else {
                $data['error'] = $input->_error;
            }
        } else {
            $data['error']['recaptcha'] = 1;
        }
        
        return $data;
    }
	
    
    public function viewReply()
    {
        $ticket = $this->load->model('Ticket');
        $category = $this->load->model('TicketCategory');
        $area = $this->load->model('StoreArea');
        $sess = $this->getSession();
        $data = array('success' => 0);
        #upload gambar
        if(!empty($_POST)){
            
            $file[0] = $_FILES['image_upload'];
            $upload = $this->doUpload($file, 'Resources/images/prp/reply');
            if(!empty($upload[0]['upload_name'])){                
                $POST['value']['resolved_images'] = $upload[0]['upload_name'];
                $POST['value']['resolved_date'] = $upload[0]['upload_time'];
                $POST['value']['resolved_solution'] = $_POST['notes'];
            }
            $POST['where'] = array(
                'ticket_id' => $_GET['id']
            );
            
            if(!empty($POST['value']) && $ticket->editTicket($POST)){
                $data['success'] = 1;
            } else {
                $data['error']['unknown'] = $upload;
            }
        }
        echo json_encode($data);
        
    }
    
    /* Edit Start Here */
    
    public function viewEdit()
    {
        $ticket = $this->load->model('Ticket');
        $category = $this->load->model('TicketCategory');
        $area = $this->load->model('StoreArea');
        $tg = $this->load->model('TicketGroup');
        $sess = $this->getSession();
        $data = array(
			'site' 			=> $this->Site(),
            'page' 			=> 'Edit Checklist',
            'session' 		=> $sess,
            'category_list' => $category->getAllCategory(),
			'area'			=> $area->getAllArea(),
			'store_list'	=> $ticket->getAllActiveStore(),
            'option' 		=> array(
                'injs' => array(),
                'exjs' => array('Resources/js/prpedit.js'),
            ),
            'checklist' => $ticket->getTicketFromGroup(array(
                'ticket_group' => $_GET['id'])
            ),
            'admin' 		=> $this->isAdmin(),
            'group' => $tg->getTicketGroup(array('group'=>$_GET['id']))
        );
        if(isset($_POST['prp_edit']) && $_POST['prp_edit'] == "Edit PRP"){
            
            
            $form = array();
            
            foreach($_POST['ticket'] as $k => $v){
                $form[$k]['value']['last_update'] = date("Y-m-d H:i:s");
                $form[$k]['value']['status'] = (isset($_POST['status'][$k]) ? 0 : 1);
                $form[$k]['value']['last_status'] = $_POST['last_status'][$k];
                $form[$k]['value']['notes'] = (isset($_POST['notes'][$k]) ? $_POST['notes'][$k] : '');
                $form[$k]['where']['ticket_id'] = $_POST['ticket'][$k];
                if(isset($_FILES['image_upload']['error'][$k]) && $_FILES['image_upload']['error'][$k] == 0){
                    $form[$k]['file'][0] = array(
                        'name' => $_FILES['image_upload']['name'][$k],
                        'type' => $_FILES['image_upload']['type'][$k],
                        'tmp_name' => $_FILES['image_upload']['tmp_name'][$k],
                        'error' => $_FILES['image_upload']['error'][$k],
                        'size' => $_FILES['image_upload']['size'][$k],
                    );
                }
            }
            #exit(print_r($form));
            if(count($form) > 0){
                $num = 0;
                foreach($form as $post){
                    $t = $ticket->getTicketDetailForUpdate(array('ticket_id' => $post['where']['ticket_id']));
                    if(($post['value']['status'] != $post['value']['last_status']) || (empty($t['finding_images']) && isset($post['file'])) || ($post['value']['status'] == 1 && isset($post['file']))  || ($post['value']['status'] == 1 && isset($post['value']['notes']))){
                        $data['submit'][$post['where']['ticket_id']] = $this->proccessEdit($post, $t);
                        $num++;
                    }
                }
                $tg = $this->load->model('TicketGroup');
                $tg->updateGroupByTicket($_POST['group']);
                if(is_array($data['submit'])){
                    echo "<script>alert('Submit Success'); window.location.replace('edit_prp_{$_GET['id']}.html');</script>";
                }
            }
            exit;
        }
        
        $this->load->template('ticket/edit', $data);
        
    }
    
    
    protected function proccessEdit($POST, $ci)
    {
        $data = array();
        $sess = $this->getSession();
        
        $ticket = $this->load->model('Ticket');

        $mc = $this->load->model('TicketCategory');
        $location = "Resources/images/prp/{$ci['finding_images']}";
        
        if($POST['value']['status'] == 1 && isset($POST['file'])){
            $file = $POST['file'];
            unset($POST['file']);
            if(!empty($ci['finding_images']) && file_exists($location)){
                unlink($location);
            }
            
            $upload = $this->doUpload($file);
            
            if(count($upload) > 0 && !isset($upload[0]['error'])){
                $POST['value']['finding_images'] = $upload[0]['upload_name'];
            }
            
        }
        if($POST['value']['status'] == 0 && file_exists($location)){
            $POST['value']['finding_images'] = '';
            if(is_file($location))unlink($location);
            
        }
        #echo "file: {$ci['finding_images']}";
        #exit(var_dump(file_exists($location)));
        
        unset($POST['value']['last_status']);
        if($ticket->editTicket($POST)){
            
            $data['status'] = 1;
            
        } else {
            $data['error']['unknown'] = 'Unknown Error';
        }
            
        return $data;
    }
    
    
    public function viewEmail()
    {
        $data = array();
        if(isset($_POST['group'])){
            $input = $this->load->lib('Input');
            $input->addValidation('group_id', $_POST['group'], 'min=1', 'Must be Filled');
            $input->addValidation('group_id', $_POST['group'], 'max=11', 'Unknown Error');
            $input->addValidation('group_id', $_POST['group'], 'numeric', 'Unknown Format');
            if($input->validate()){
                $mtg = $this->load->model('TicketGroup');
                $group = $mtg->getTicketGroup(array('group' => $_POST['group']));
                if(count($group) > 0){
                    $option = array(
                        'title' => "PRP Result #{$group['store_name']}",
                        'view' => 'email/send',
                        'email' => $group['store_email'],
                        'store' => $group['store_name'],
                        'data' => array(
                            'detail' => $group,
                            'content' => 'Test Email PRP',
                            'header' => 'Email tentang PRP.'
                        ),
                    );
                    $email = $this->_sendEmail($option);
                    if($email['email_status'] == 1){
                        $mtg->editGroup(array(
                            'value' => array(
                                'group_status' => 2
                            ),
                            'where' => array(
                                'group_id' => $_POST['group']
                            )
                        ));
                        
                        $data['success'] = 1;
                        $data['email'] = 1;
                    }else{
                        $data['success'] = 0;
                        $data['email'] = $email;
                    }
                    
                }else{
                    $data['success'] = 0;
                    $data['error'] = 'No Data';
                }
            }else{
                $data['success'] = 0;
                $data['error'] = $input->_error;
            }
        }else{
            $data['success'] = 0;
            $data['error'] = 'No request submitted';
             
        }
        echo json_encode($data);
    }
    # Protected function
    
    protected function doUpload($file, $location='Resources/images/prp')
    {
        
        if(count($file) > 0){
            $uploaded = array();
            $num=0;
            $upload = $this->load->lib('Upload', $file);
            for($uid=0;$uid < count($file); $uid++){
                $file[$uid] = new Upload($file[$uid]);
                if($file[$uid]->uploaded) {
                    $file[$uid]->file_new_name_body   = strtotime(date("Y-m-d H:i:s")).$num;
                    $file[$uid]->image_resize = true;
                    $file[$uid]->image_x = 800; //ukuran weight
                    $file[$uid]->image_ratio_y = true; //ukuran height mengikuti weight
                    $file[$uid]->allowed = array('image/jpeg', 'image/png');
                    $file[$uid]->image_convert = 'jpg';
                    $file[$uid]->process($location);
                    if ($file[$uid]->processed) {
                        $file[$uid]->clean();
                        $uploaded[$num] = array(
                            'upload_name' => $file[$uid]->file_dst_name,
                            'upload_location' => $file[$uid]->file_dst_path,
                            'upload_time' => date("Y-m-d H:i:s"),
                            'upload_temp' => 0,
                            'upload_user' => $this->_session['user_id'],
                        );
                        $num++;
                    }else{
                        $uploaded[$num]['error'] = $file[$uid]->error;
                    }
                }
            }
            return $uploaded;
        }
    }
    
    protected function reArrayFiles(&$file_post)
    {

        $file_ary = array();
        $file_count = (is_array($file_post['name']) ? count($file_post['name']) : 0);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
    
    protected function _sendEmail($option){
        $html = $this->load->view($option['view'], $option['data']);
        $stmail   = $this->load->lib('PHPMailer');
        $smtp   = $this->load->lib('SMTP');
        $stmail   = new PHPMailer(true);
        #$stmail = $this->load->lib('STMail');
        try
        {
            $stmail->IsSMTP(); // telling the class to use SMTP
            $stmail->Host       = "mail.sushitei.co.id"; // SMTP server
            $stmail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                                                       // 1 = errors and messages
                                                       // 2 = messages only
            $stmail->SMTPAuth   = true;                  // enable SMTP authentication
            $stmail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $stmail->Port       = 587;                    // set the SMTP port for the GMAIL server
            $stmail->Username   = "qa.helpdesk@sushitei.co.id"; // SMTP account username
            $stmail->Password   = "QAHelpdesk123";        // SMTP account password

            $stmail->SetFrom('qa.helpdesk@sushitei.co.id', 'PRP');

            $stmail->AddReplyTo('qa.helpdesk@sushitei.co.id', 'PRP');
            
            
            $stmail->addStringAttachment($this->viewExportReport(), 'Hasil PRP.xlsx');
            
            $stmail->isHTML('true');

            $stmail->Subject    = $option['title'];
            
            $stmail->MsgHTML($html);
            
            $mr = $this->load->model('Email');
            
            $stmail->AddAddress($option['email'], $option['store']);
            #$stmail->AddAddress('it1.jkt@sushitei.co.id', 'Rahmat Samsudin');
            
            foreach($mr->getAllActiveEmail() as $edata){
                $stmail->AddCC($edata['email'], $edata['pic_name']);
            }
            $stmail->AddCC('rizal@sushitei.co.id','Rizal');
            $stmail->AddCC('devi@sushitei.co.id','Devi');
            $stmail->AddCC('wahyuni@sushitei.co.id','Wahyuni');
            $stmail->AddCC('indra@sushitei.co.id','Indra Oktapianus');
            $stmail->AddCC('yulis@sushitei.co.id','Yulis Setialianny Eka Azhari');
            $stmail->AddCC('senior.bartender@sushitei.co.id','Muhammad Sayuti');
            $stmail->AddCC('senior.bartender2@sushitei.co.id', 'Senior Bartender');
            $stmail->AddCC('alam@sushitei.co.id','Alam Suryana');
            $stmail->AddCC('thubagus@sushitei.co.id','Thubagus');
            $stmail->AddCC('ade.gendawi@sushitei.co.id','Ade Gendawi');
            $stmail->AddCC('m.chandra@sushitei.co.id','Michael Chandra');
            $stmail->AddCC('ppfbspv2@sushitei.co.id','Nurvenda Lafian');
            $stmail->AddCC('ppfbspv@sushitei.co.id','Fahmi Wahyudin');
            $stmail->AddCC('adminfbspv@sushitei.co.id','Dede Mardinah');
            $stmail->AddCC('headsk2@sushitei.co.id','Dwi Kristanto');
            $stmail->AddCC('headsk5@sushitei.co.id','Slamet Asroi');
            $stmail->AddCC('headsk@sushitei.co.id','Andhika Kusuma');
            $stmail->AddCC('headsk4@sushitei.co.id','Heri Kiswanto');
            $stmail->AddCC('headsk3@sushitei.co.id','Fadillah Hakim');
            
            $sd = array(
                'user' => 'Rahmat Samsudin',
                'email_status'  => $stmail->Send()
            );
            return $sd;
            
        } catch (phpmailerException $e) {
            $sd = array(
                'user' => 'Rahmat Samsudin',
                'email_status'  => 0,
                'message' => $e->errorMessage()
            );
            
            return $sd;
            
        } catch (Exception $e) {
                $sd = array(
                    'user' => $option['recipient']['address'],
                    'email_status'  => 0,
                    'message' => $e->getMessage(),
                    'data' => $returned
                );
                return $sd;
        }
    }
    
    public function viewExportReport()
    {
        $ticket = $this->load->model('Ticket');
        $tg = $this->load->model('TicketGroup');
        $trec = $this->load->model('Recommendation');
        $input = $this->load->lib('Input');
        if(isset($_GET['id'])){
            $input->addValidation('group_id', $_GET['id'], 'numeric', 'Must be number');
            $input->addValidation('group_id', $_GET['id'], 'min=1', 'Must be filled');
            $input->addValidation('group_id', $_GET['id'], 'max=11', 'Limit reached');
            if($input->validate()){
                
                $tr = $ticket->getTicketByGroup(array('group' => $_GET['id']));
                $trd = $trec->getRecomByGroup(array('group' => $_GET['id']));
                $gd = $tg->getTicketGroup(array('group' => $_GET['id']));
                if(is_array($gd)){
                    $objPHPExcel = $this->load->lib('PHPExcel');
                    
                    #$objPHPExcel = new Spreadsheet();
                    $objPHPExcel->getProperties()->setCreator("Sushitei IT")
                                         ->setLastModifiedBy("Sushitei IT")
                                         ->setTitle("Audit PRP Outlet")
                                         ->setSubject("Audit PRP Outlet")
                                         ->setDescription("In America.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("PRP");

                    $objPHPExcel->setActiveSheetIndex(0)
                                ->mergeCells('A1:I3')
                                ->setCellValue('A1', 'PRP AUDIT REPORT')
                                ->setCellValue('A5', 'TANGGAL AUDIT')
                                ->setCellValue('C5', ": {$gd['group_submit_date']}")
                                ->setCellValue('E5', 'TANGGAL VERIFIKASI')
                                ->setCellValue('F5', '-')
                                ->setCellValue('A7', 'OUTLET')
                                ->setCellValue('C7', ": {$gd['store_name']}")
                                ->setCellValue('A9', 'NAMA AUDITOR')
                                ->setCellValue('C9', ": {$gd['group_user']}")
                                ->setCellValue('E9', 'NAMA AUDITEE')
                                ->setCellValue('F9', ": {$gd['group_auditee']}")
                                ->mergeCells('A11:A12')
                                ->setCellValue('B11', 'AREA')
                                ->mergeCells('B11:B12')
                                ->setCellValue('A11', 'NO')
                                ->mergeCells('C11:C12')
                                ->setCellValue('C11', 'HASIL AUDIT')
                                ->mergeCells('D11:D12')
                                ->setCellValue('D11', 'FOTO HASIL AUDIT')
                                ->mergeCells('E11:E12')
                                ->setCellValue('E11', 'TINDAKAN PERBAIKAN')
                                ->mergeCells('F11:F12')
                                ->setCellValue('F11', 'FOTO TINDAKAN PERBAIKAN')
                                ->mergeCells('G11:G12')
                                ->setCellValue('G11', 'POINT CHECKLIST')
                                ->mergeCells('H11:H12')
                                ->setCellValue('H11', 'SCORE')
                                ->mergeCells('I11:I12')
                                ->setCellValue('I11', 'KETERANGAN');
                                
                    $sheet = $objPHPExcel->getActiveSheet();
                    $sheet->setShowGridlines(false);
                    $A1 = $sheet->getStyle('A1');
                    $A1->getAlignment()->applyFromArray(
                        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                        )
                    );
                    $A1->getFont()->setBold( true );
                    $A11 = $sheet->getStyle('A11:I12');
                    $A11->getAlignment()->applyFromArray(
                        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        )
                    );
                    $A11->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
                    $A11->getFont()->setBold( true );
                    $first = 11;
                    $last = 13;
                    $num = 1;
                    if(count($tr) > 0){
                        foreach($tr as $data){
                            $sheet->setCellValue("B{$last}", $data['area'])
                                    ->setCellValue("A{$last}", $num)
                                    ->setCellValue("C{$last}", $data['notes'])
                                    ->setCellValue("E{$last}", $data['resolved_solution'])
                                    ->setCellValue("G{$last}", $data['category_name'])
                                    ->setCellValue("H{$last}", $data['point']);
                                    
                            #/** 
                            $location = 'Resources/images/prp/';
                            if(!empty($data['finding_images']) && file_exists($location.$data['finding_images'])){
                                #$gdImage = imagecreatefromjpeg($location.$data['finding_images']);
                                // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Finding Image');
                                $objDrawing->setDescription('Finding Image');
                                $objDrawing->setPath($location.$data['finding_images']);
                                $objDrawing->setHeight(150);
                                $objDrawing->setCoordinates("D{$last}");
                                $objDrawing->setOffsetY(10);
                                $objDrawing->setOffsetX(10);
                                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                                $sheet->getRowDimension($last)->setRowHeight(150);
                                $sheet->getColumnDimension('D')->setWidth(40);
                            }
                            $location_rep = 'Resources/images/prp/reply/';
                            if(!empty($data['resolved_images']) && file_exists($location_rep.$data['resolved_images'])){
                                #$gdImage = imagecreatefromjpeg($location.$data['finding_images']);
                                // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('resolved Image');
                                $objDrawing->setDescription('Finding Image');
                                $objDrawing->setPath($location_rep.$data['resolved_images']);
                                $objDrawing->setHeight(150);
                                $objDrawing->setCoordinates("F{$last}");
                                $objDrawing->setOffsetY(10);
                                $objDrawing->setOffsetX(10);
                                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                                $sheet->getRowDimension($last)->setRowHeight(150);
                                $sheet->getColumnDimension('F')->setWidth(40);
                            }
                            #**/
                            $last++;
                            $num++;
                        }
                    }else{
                        $sheet->mergeCells("A{$last}:I{$last}")
                              ->setCellValue("A{$last}", 'No Data Found');
                        $last++;
                    }
                    
                    if(count($trd) > 0){
                        $sheet->mergeCells("A{$last}:I{$last}")
                              ->setCellValue("A{$last}", 'Rekomendasi');
                        $last++;
                        foreach($trd as $data){
                            $sheet->setCellValue("B{$last}", 'Rekomendasi')
                                    ->setCellValue("A{$last}", $num)
                                    ->setCellValue("C{$last}", $data['recommendation_content'])
                                    ->setCellValue("E{$last}", $data['recommendation_feedback']);
                                    
                            #/** 
                            $location = 'Resources/images/prp/';
                            if(!empty($data['recommendation_image']) && file_exists($location.$data['recommendation_image'])){
                                #$gdImage = imagecreatefromjpeg($location.$data['finding_images']);
                                // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                                $objDrawing = new PHPExcel_Worksheet_Drawing();
                                $objDrawing->setName('Finding Image');
                                $objDrawing->setDescription('Finding Image');
                                $objDrawing->setPath($location.$data['recommendation_image']);
                                $objDrawing->setHeight(150);
                                $objDrawing->setCoordinates("D{$last}");
                                $objDrawing->setOffsetY(10);
                                $objDrawing->setOffsetX(10);
                                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                                $sheet->getRowDimension($last)->setRowHeight(150);
                                $sheet->getColumnDimension('D')->setWidth(30);
                            }
                            #**/
                            $last++;
                            $num++;
                        }
                    }
                    
                    $styleBorder = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );
                    $b = $last-1;
                    $tc = $sheet->getStyle("A{$first}:I{$b}");
                    $sheet->getColumnDimension('C')->setWidth(20);
                    $sheet->getColumnDimension('E')->setWidth(20);
                    $sheet->getColumnDimension('G')->setWidth(20);
                    $sheet->getColumnDimension('I')->setWidth(20);
                    $tc->applyFromArray($styleBorder);
                    // Rename worksheet
                    $objPHPExcel->getActiveSheet()->setTitle('Category');


                    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                    $objPHPExcel->setActiveSheetIndex(0);
                    
                    // Redirect output to a clientâ€™s web browser (Excel2007)
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header("Content-Disposition: attachment;filename={$gd['store_name']} ".date("F Y",strtotime($gd['group_date'])).".xlsx");
                    header('Cache-Control: max-age=0');
                    // If you're serving to IE 9, then the following may be needed
                    header('Cache-Control: max-age=1');

                    // If you're serving to IE over SSL, then the following may be needed
                    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                    header ('Pragma: public'); // HTTP/1.0
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                    ob_start();
                    $objWriter->save('php://output');
                    $excelOutput = ob_get_clean();
                    return $excelOutput;
                }
            }
    
        }
    }
}
/*
* End Home Class
*/