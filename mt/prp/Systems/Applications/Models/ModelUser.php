<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

	class ModelUser extends Model
    {
		public $_result = array();
		private $table_field = array('user_id','user_name','user_password','group_id');
        
        function __construct()
        {
            #$config = array('db' => 'MySQL','host' => 'localhost','db_name' => 'sushitei_ticketdesk','user_name' => 'root','password' => '');
            $config = array('db' => 'MySQL','host' => 'localhost','db_name' => 'sushitei_ticketdesk','user_name' => 'h71332_ticket','password' => 'a)_1qjbW!0-5');
            parent::__construct($config);
        }
        
		function doLogin($input)
        {
			$query = "SELECT user_id, display_name, group_name, email, level, store_id FROM users LEFT JOIN user_groups USING (group_id) WHERE user_name=:user_name AND user_password=:user_password";
            $result = $this->fetchSingleQuery($query, $input);
			return $result;
		}
        
        function checkForgotRequirement($input)
        {
			$query = "SELECT * FROM users WHERE user_name=:user_name AND security_question=:security_question AND security_answer=:security_answer";
            $result = $this->fetchSingleQuery($query, $input);
			return $result;
		}
        
        function newUser($input)
        {
            
            try{
                $this->insertQuery('users', $input);
                return 1;
            } catch(Exception $e){
                if(getConfig('development') == 1){
                    echo $e;
                    exit;
                }
                return 0;
            }
        }
        
        function newTicketUser($input)
        {
            $result = $this->insertQuery('users', $input['new_user']);
            return $result;
        }
        
        function editTicketUser($input = array())
        {
            $result = $this->editQuery('users', $input['users'], $input['where']);
            return $result;

        }
		
        
        function checkUserName($input){
			$query = $this->prepare("SELECT user_name FROM users WHERE user_name=:username");
			$query->bindParam(':username',$input);
			$query->execute();
			if($query->rowCount() > 0){
				return 0;
			}else{
				return 1;
			}
        }
        function getAllNotAdmin()
        {
			$query_string = "SELECT user_id,display_name FROM users WHERE group_id=4";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        function getAllUser2()
        {
			$query_string = "SELECT user_id,display_name FROM users";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllNotUser()
        {
			$query_string = "SELECT user_id,display_name FROM users WHERE group_id NOT IN ('4')";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        function getAllEngineer()
        {
			$query_string = "SELECT user_id,display_name FROM users WHERE group_id=3";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
        
        function getAllUser($form, $qo)
        {

            $qs = "SELECT  user_id, user_name, display_name, email, user_groups.group_name, user_status FROM users LEFT JOIN user_groups ON users.group_id=user_groups.group_id ";
            $cq = "SELECT COUNT(*) AS row_total FROM users ";
            
            if(!empty($form['user_name']['value']) || !empty($form['display_name']['value']) || !empty($form['group_id']['value']) || !empty($form['user_status']['value'])){
                $qs .= 'WHERE ';
                $cq .= 'WHERE ';
                $first = true;
                foreach($form as $key=>$criteria){
                    if(!empty($criteria['value']) && $first == false){
                        $qs .= 'AND ';
                        $cq .= 'AND ';
                    }
                    if(!empty($criteria['value'])){
                        if($criteria['condition'] == 'LIKE') $criteria['value'] = "%{$criteria['value']}%";
                        $cq .= "{$key} {$criteria['condition']} '{$criteria['value']}' ";
                        $qs .= (!empty($criteria['table']) ? "{$criteria['table']}." : '')."{$key} {$criteria['condition']} '{$criteria['value']}' ";
                        
                    }
                    if(!empty($criteria['value']) && $first == true){
                        $first = false;
                    }
                }

            }
            #echo $qs; exit;
            $result = $this->pagingQuery($qs, $cq, $qo);
            return $result;
        }
        
        function countAllUser()
        {
            $query_string = "SELECT COUNT(*) AS total FROM users";
            
            $result = $this->doQuery($query_string);
            return $result['total'];
        }
        
		function getUserBy($input)
        {
            $query_string = "SELECT  user_id, user_name, user_password, display_name, store_id, email, user_groups.group_name, users.group_id, user_status, level ";
            $query_string .= "FROM users LEFT JOIN user_groups ON users.group_id=user_groups.group_id ";
            $query_string .= "WHERE user_id=:user_id";            
            $result = $this->fetchSingleQuery($query_string, $input);
            return $result;
        }
        
        function editUser($form = array(), $where = array()){
            $table = 'users';
            try{
                $this->editQuery($table, $form, $where);
                return 1;
            } catch(Exception $e){
                return $e;
            }
                
        }
        
        function getAllGroups(){
			$query_string = "SELECT group_id, group_name FROM user_groups";
			$result = $this->fetchAllQuery($query_string);
            return $result;
		}
        
		function getAllActiveStore()
        {
            $query_string = "SELECT * FROM store WHERE store_status=1";
            
            $result = $this->fetchAllQuery($query_string);
            return $result;
        }

        #deleteUser
        function deleteUser($id)
        {
            $query_string = $this->doQuery("UPDATE users SET user_status=2 WHERE user_id=$id");
        }

        /*function deleteUser($id){
            
            $result = $this->deleteQuery('users' ,$id);
            return $result;
            
        }*/
	}
?>