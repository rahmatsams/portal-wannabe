<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Model for Ticketdesk
*/
class ModelTicket extends Model
{
	public $tableName = "ticket_main";
    public $lastID;
	
	function createTicket($form = array())
    {
		$this->beginTransaction();
		try{
			$this->insertQuery('ticket_main',$form);
            $this->lastID = $this->lastInsertId();
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
            if(getConfig('development') == 1){
                exit($e);
            }
			return 0;
		}
			
	}
    
    function editTicket($form = array())
    {
        $table_ticket = 'ticket_main';
        $table_log = 'ticket_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table_ticket, $form['value'], $form['where']);
			#$this->insertQuery($table_log, $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}
	
    function showAllTicket($query_option)
    {
        $query_string = "SELECT *,SUM(point) AS total_deduction,COUNT(*) AS ticket_num FROM view_ticket  ";
		$count_query = "SELECT COUNT(*) AS row_total FROM ticket_main ";
        
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
    
    function showAllTicketFiltered($filter, $query_option)
    {
        
        $query_string = "SELECT *,SUM(point) AS total_deduction,COUNT(*) AS ticket_num FROM view_ticket ";
        $num = 1;
        $count_query = "SELECT COUNT(*) AS row_total FROM view_ticket ";
        foreach($filter as $key => $value){
            if($num > 1){
                if($key != 'date_end' && !empty($value)){
                    if($key == 'submit_date' && !empty($filter['date_end'])){
                        $query_string .= "AND submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                        $count_query .= "AND submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                    }else{
                        $query_string .= "AND {$key} LIKE '%{$value}%' ";
                        $count_query .= "AND {$key} LIKE '%{$value}%' ";
                    }
                }
            } else {
                if($key != 'date_end' && !empty($value)){
                    if($key == 'submit_date' && !empty($filter['date_end'])){
                        $query_string .= "WHERE submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";
                        $count_query .= "WHERE submit_date BETWEEN '{$value} 00:00:00' AND '{$filter['date_end']} 23:59:00' ";                    
                    }else{
                        $query_string .=  "WHERE {$key} LIKE '%{$value}%' ";
                        $count_query .= "WHERE {$key} LIKE '%{$value}%' ";
                    }
                }
            }
            $num++;
        }
		$result = $this->pagingQuery($query_string, $count_query, $query_option);
        return $result;
    }
	
	function getTicketAdmin($filter)
    {
        $qs = "SELECT group_id,store_id,store_name,default_point,group_date,group_finding, group_recommendation,group_point,group_status FROM sushitei_portal.portal_store a LEFT JOIN (SELECT * FROM ticket_group WHERE group_date BETWEEN :submit AND :end) b ON a.store_id=b.group_store WHERE a.store_status=1 AND a.store_location IN(1) ORDER BY store_id;";
		$result = $this->fetchAllQuery($qs, $filter);
        return $result;
    }
    
    function getTicketByGroup($input)
    {
        $query_string = "SELECT * FROM view_ticket ";
        if(isset($input['my_id'])){
            $query_string .= "WHERE ticket_user=:my_id AND ticket_group=:group AND status=1";
        } else {
            $query_string .= "WHERE ticket_group=:group AND status=1";
        }
        $result = $this->fetchAllQuery($query_string, $input);
        
		return $result;
    }
    
    function getLog($input){
        $query_string = "SELECT log_title, user, sppu.display_name, log_type, log_content, log_time FROM ticket_log ";
        $query_string .= "LEFT JOIN sushitei_portal.portal_user sppu ON ticket_log.user=sppu.user_id ";
        $query_string .= "WHERE ticket = :ticket_id AND visible = 1 ORDER BY log_time DESC";
        $result = $this->fetchAllQuery($query_string, $input);
		return $result;
    }
    
	function getTicketType()
    {
        $query_string = 'SELECT * FROM ticket_type';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getMainCategory()
    {
        $query_string = 'SELECT category_id, category_name, description, point_deduction FROM ticket_category WHERE category_parent_id=0';
        $result = $this->fetchAllQuery($query_string);
		return $result;
    }
	
	function getSubCategory($id)
	{
		$query_string = 'SELECT * FROM ticket_category WHERE category_parent_id=:cat_id';
		$query_value = array(
			'cat_id' => $id
		);
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getUploadedFile($query_value)
	{
		$query_string = 'SELECT * FROM ticket_upload WHERE ticket_id=:ticket_id';
        $result = $this->fetchAllQuery($query_string, $query_value);
		return $result;
	}
    
    function getAllActiveStore()
    {
        $query_string = "SELECT * FROM sushitei_portal.portal_store WHERE store_status=1 AND store_location IN(1)";
        
        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function checkTicketGroup($input)
    {
        $query_string = "SELECT * FROM ticket_group WHERE group_store=:group_store AND group_date=:group_date";
        $a = array(
            'group_store' => $input['store'],
            'group_date' => $input['prp_date']
        );
        $result = $this->fetchSingleQuery($query_string, $a);
        if(empty($result['group_id'])){
            $a['group_status'] = 0;
            $a['group_finding'] = 0;
            $a['group_point'] = 0;
            $a['group_submit_date'] = date("Y-m-d H:i:s");
            $a['group_user'] = $input['uid'];
            $a['group_auditee'] = $input['auditee'];
            $this->insertQuery('ticket_group', $a);
            $a['group_id'] = $this->lastInsertId();
            return $a;
        }else{
            return $result;
        }
    }
    
    function getTicketGroupByID($input)
    {
        $query_string = "SELECT * FROM ticket_group WHERE group_id=:group_id";
        
        $result = $this->fetchSingleQuery($query_string, $input);
        
        return $result;
        
    }
    
    function verifyTicketGroup($input)
    {
        $query_string = "SELECT * FROM ticket_group WHERE group_store=:group_store AND group_date=:group_date";

        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
        
    }
    
    function getTicketGroup($input)
    {
        $query_string = "SELECT store_name,group_date,group_submit_date,group_user,group_id,group_point,b.default_point,group_status FROM ticket_group a LEFT JOIN sushitei_portal.portal_store b ON a.group_store=b.store_id WHERE group_id=:group";

        $result = $this->fetchSingleQuery($query_string, $input);
        
        return $result;
        
    }
    
    function checkTicketCategory($input)
    {
        $query_string = "SELECT * FROM view_ticket WHERE ticket_group=:group AND link_id=:link_id";
        $a = array(
            'group' => $input['ticket_group'],
            'link_id' => $input['link_id'],
        );
        $result = $this->fetchSingleQuery($query_string, $a);
        return $result;
    }
    
    function getTicketFromGroup($input)
    {
        $query_string = "SELECT * FROM view_ticket WHERE ticket_group=:ticket_group ORDER BY area_id ASC";
        $result = $this->fetchAllQuery($query_string, $input);
        return $result;
    }
    
    function getTicketDetailForUpdate($input)
    {
        $query_string = "SELECT a.*,b.group_store,b.group_finding,b.group_status,c.point_deduction FROM ticket_main a LEFT JOIN ticket_group b ON a.ticket_group=b.group_id LEFT JOIN ticket_category c ON a.category=c.category_id WHERE ticket_id=:ticket_id";
        $result = $this->fetchSingleQuery($query_string, $input);
        return $result;
    }

    #untuk cek file image d ticket_main
    function checkFindingImage($input)
    {
        $query_string = "SELECT ticket_group, finding_images FROM ticket_main WHERE ticket_group=:ticket_group AND finding_images IS NOT NULL";
        $result = $this->fetchAllQuery($query_string, $input);
        return $result;
    }

}