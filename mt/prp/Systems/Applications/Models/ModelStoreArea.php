<?php
/*
* Model for Ticketdesk
*/
class ModelStoreArea extends Model
{
	
    function getAllArea()
    {
        $query_string = "SELECT area_id, area_name, area_desc FROM store_area WHERE area_status=1 ORDER BY area_name ASC";
		
		$result = $this->fetchAllQuery($query_string);
        return $result;
    }

    function getAllAreaByPoint()
    {
        $query_string = "SELECT 
                a.area_id, 
                a.area_name, 
                a.area_desc, 
                b.points 
                FROM store_area a 
                LEFT JOIN (SELECT area_id,SUM(point_deduction) AS points 
                FROM ticket_category WHERE deleted=0 GROUP BY area_id) b 
                ON a.area_id=b.area_id 
                WHERE area_status=1 
                ORDER BY area_id ASC";

        $result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function getFromAreaLink($i)
    {
        $qs = "SELECT link_id,category_id,category_name FROM area_category_link a LEFT JOIN ticket_category b ON a.category=b.category_id WHERE area=:area";
        
        $result = $this->fetchAllQuery($qs, $i);
        return $result;
    }
    
    function getAllAreaCategory()
    {
        $qs = "SELECT a.link_id,b.category_id,b.category_name,c.area_id,c.area_name FROM area_category_link a LEFT JOIN ticket_category b ON a.category=b.category_id LEFT JOIN store_area c ON a.area=c.area_id ORDER BY area_name ASC";
        
        $result = $this->fetchAllQuery($qs);
        return $result;
    }

    function getStoreAreaId($input)
    {
        $qs = "SELECT area_id, area_name, area_desc FROM store_area WHERE area_id=:sid ";
        $result = $this->fetchSingleQuery($qs, $input);
        return $result;
    }

    function createStoreArea($form){
        try{
            $result = $this->insertQuery('store_area', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
        } catch(Exception $e){
            if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
        }
    }

    function editStoreArea($form, $where)
    {
        $result = $this->editQuery('store_area', $form, $where);
        return $result;
    }

    function deleteCategory($id)
    {
        $query = $this->doQuery("UPDATE store_area SET area_status=0 WHERE area_id=$id");
    }
    
}