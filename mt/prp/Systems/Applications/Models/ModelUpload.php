<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
	class ModelUpload extends Model{
        
        
        public function newUpload($input)
        {
            
            return $this->insertQuery('ticket_upload', $input);
            
        }
        
        public function multiUpload($input)
        {
            try{
                $this->beginTransaction();
                
                foreach($input as $submit){
                    $this->insertQuery('ticket_upload', $submit);
                }
                
                $this->commit();
                
                return 1;
            } catch(Exception $e){
                $this->rollBack();
                return 0;
            }
            
        }
        
        public function getPendingUpload($uid)
        {
            $q = 'SELECT * FROM ticket_upload WHERE upload_user=:user_id AND upload_temp=1';
            return $this->fetchAllQuery($q, $uid);
        }
        
        public function getUploadInfo($uid)
        {
            $q = 'SELECT * FROM ticket_upload WHERE upload_id=:upload_id';
            return $this->fetchSingleQuery($q, $uid);
        }
        
        public function deletePending($input)
        {
            return $this->deleteQuery('ticket_upload', $input);
        }
	}
?>