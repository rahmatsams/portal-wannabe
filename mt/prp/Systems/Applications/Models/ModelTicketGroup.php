<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');
/*
* Model for Ticketdesk
*/
class ModelTicketGroup extends Model
{
	public $tableName = "ticket_group";
    public $lastID;
	
    function getTicketGroup($input)
    {
        $query_string = "SELECT store_name,store_email,group_date,group_submit_date,group_user,group_auditee,group_id,group_point,group_finding,b.default_point,group_status,group_recommendation FROM ticket_group a LEFT JOIN sushitei_portal.portal_store b ON a.group_store=b.store_id WHERE group_id=:group";

        $result = $this->fetchSingleQuery($query_string, $input);
        
        return $result;
        
    }
    
    function getTicketGroupByStore($input)
    {
        $query_string = "SELECT store_name,group_date,group_submit_date,group_user,group_auditee,group_id,group_point,group_finding,b.default_point,group_status,group_recommendation FROM ticket_group a LEFT JOIN sushitei_portal.portal_store b ON a.group_store=b.store_id WHERE a.group_store=:storeid AND group_date BETWEEN :submit AND :end ORDER BY group_date ASC";

        $result = $this->fetchAllQuery($query_string, $input);
        
        return $result;
    }

    function createRecommendation($form){
        try{
            $result = $this->insertQuery('ticket_group', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
        } catch(Exception $e){
            if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
        }
    }
    
	public function updateGroupByTicket($input)
    {
        $insert = array(
            'group_id' => $input
        );
        $q = "UPDATE ticket_group d,(SELECT SUM(point_deduction) AS total_point,COUNT(ticket_id) AS total_finding FROM ticket_main a LEFT JOIN ticket_category b ON b.category_id=a.category WHERE a.ticket_group=:group_id AND a.status=1) c SET d.group_finding=c.total_finding,d.group_point=c.total_point WHERE d.group_id=:group_id";
        #exit("UPDATE ticket_group d,(SELECT SUM(point_deduction) AS total_point,COUNT(ticket_id) AS total_finding FROM ticket_main a LEFT JOIN ticket_category b ON b.category_id=a.category WHERE a.ticket_group={$input} AND a.status=1) c SET d.group_finding=c.total_finding,d.group_point=c.total_point WHERE d.group_id={$input}");
        return $this->doQuery($q, $insert);
    }
    
    function editGroup($form = array())
    {
        $table = 'ticket_group';
        $table_log = 'ticket_log';
		$this->beginTransaction();
		try{
			$this->editQuery($table, $form['value'], $form['where']);
			#$this->insertQuery($table_log, $log);
			$this->commit();
			return 1;
		} catch(Exception $e){
			$this->rollBack();
			return 0;
		}
			
	}

    function deletePRP($id)
    {
        $query_string = $this->doQuery("DELETE FROM ticket_group WHERE group_id=$id");
    }
    
}