<?php
/*
* Model for Ticketdesk Store Area
*/
class ModelArea extends Model
{
	
    function getAllActiveArea()
    {
        $query_string = "SELECT area_id,area_name FROM store_area ORDER BY area_name ASC";
		
		$result = $this->fetchAllQuery($query_string);
        return $result;
    }
    
    function newArea($input)
    {
        $result = $this->insertQuery('store_area', $input);
        return $result;
    }
    
    function getFromAreaLink($i)
    {
        $qs = "SELECT category_id,category_name FROM area_category_link a LEFT JOIN ticket_category b ON a.category=b.category_id WHERE area=:area";
        
        $result = $this->fetchAllQuery($qs, $i);
        return $result;
    }
    
}