<?php if(!defined('__SYSTEM_DIR')) exit('<b>404</b> Page not found.');

/**
 * Model for Recommendation
 */
class ModelRecommendation extends Model
{
	public $tableName = "ticket_recommendation";
	public $lastID;

	function getAllRecommendByGroupID()
	{
		$query_string = "SELECT * FROM ticket_recommendation WHERE recommendation_status=1 ORDER BY recommendation_id ASC";
		$result = $this->fetchAllQuery($query_string);
		return $result;
	}

	function getRecomByGroup($input)
    {
        $qs = "SELECT * FROM ticket_recommendation WHERE group_id=:group AND recommendation_status=1";
        $result = $this->fetchAllQuery($qs, $input);
        return $result;
    }

    function checkTicketGroup($input)
    {
        $query_string = "SELECT * FROM ticket_group WHERE group_store=:group_store AND group_date=:group_date";
        $a = array(
            'group_store' => $input['store'],
            'group_date' => $input['prp_date']
        );
        $result = $this->fetchSingleQuery($query_string, $a);
        if(empty($result['group_id'])){
            $a['group_status'] = 0;
            $a['group_finding'] = 0;
            $a['group_point'] = 0;
            $a['group_submit_date'] = date("Y-m-d H:i:s");
            $a['group_user'] = $input['uid'];
            $a['group_auditee'] = $input['auditee'];
            $this->insertQuery('ticket_group', $a);
            $a['group_id'] = $this->lastInsertId();
            return $a;
        }else{
            return $result;
        }
    }

    function createRecommend($form){
        try{
            $result = $this->insertQuery('ticket_recommendation', $form);
            $this->lastInsertID = $this->lastInsertId();
            return 1;
        } catch(Exception $e){
            if(getConfig('development') == 1){
                echo $e;
                exit;
            }
            return 0;
        }
    }

    function getFeedbackId($input)
    {
        $query = "SELECT * FROM ticket_recommendation WHERE recommendation_id=:cid";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    function editAddFeedback($form, $where){
            $result = $this->editQuery('ticket_recommendation', $form, $where);
            
            return $result;
    }

    function editRecommend($form, $where)
    {
        $result = $this->editQuery('ticket_recommendation', $form, $where);

        return $result;
    }

    function getRecommendationId($input)
    {
        $query = "SELECT * FROM ticket_recommendation WHERE recommendation_id=:recommendation_id";
        $result = $this->fetchSingleQuery($query, $input);
        return $result;
    }

    #delete rekomendasi
    function deleteRecommend($id)
    {
        $query_string = $this->doQuery("UPDATE ticket_recommendation SET recommendation_status=0 WHERE recommendation_id=$id");
    }

    #cek ada file image d rekomendasi
    function checkImageRecommendation($input)
    {
        $query_string = "SELECT recommendation_image FROM ticket_recommendation WHERE recommendation_id=:recommendation_id AND recommendation_image IS NOT NULL ";
        $result = $this->fetchAllQuery($query_string, $input);
        return $result;
    }

}