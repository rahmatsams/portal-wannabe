<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?=$_option['page_name']?> - <?=getConfig('site_name')?></title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="Resources/assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="Resources/assets/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<?=(isset($_option['style']) ? $_option['style'] : '')?>
        
		<link href="<?=$_template_path?>/css/styles.css" rel="stylesheet">
        
	</head>
	<body>
<!-- header -->
		<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">Room Reservation</a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> <?=$_option['session']['username']?>  <span class="caret"></span></a>
							<ul id="g-account-menu" class="dropdown-menu" role="menu">
                                <?php
                                    if(isset($_option['session']) && ($_option['session']['group'] == 'Administrator' || $_option['session']['group'] == 'Super Admin')){
                                        echo '
                                <li role="presentation"><a href="admin_calendar.html" id="notificationBadge">Book Request (Admin)</li>';
                                    }
                                    echo '
                                <li role="presentation"><a href="history.html" id="notificationBadge">My Request</li>';
                                    
                                ?>
                                <li><a href="account.html">My Profile</a></li>
							</ul>
                            
						</li>
						<?php
                            if(isset($_option['session']) && ($_option['session']['group'] == 'Administrator' || $_option['session']['group'] == 'Super Admin')){
                                echo '
                        <li><a href="admin.html">Administrator</a></li>';
                            }
                        ?>
						<!--<li><a href="book.html">Room Booking</a></li> -->
						<li><a href="logout.html"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
					</ul>
				</div>
			</div>
			<!-- /container -->
		</div>
<!-- /Header -->

<!-- Main -->
		<div class="container-fluid">
			<div class="row">
				<!-- /col-3 -->
				<div class="col-sm-12">
					<?=$this->view($_action, $_passed_var);?>
					
				</div>
				<!--/col-span-9-->
			</div>
		</div>
		<div id="failedModal" class="modal fade" tabindex="-1" role="failed" data-keyboard="false">
			<div class="vertical-alignment-helper">
				<div class="modal-dialog vertical-align-center">
					<div class="modal-content">
						<div class="modal-header bg-danger">
						
							 <h4 class="modal-title">Request Declined</h4>

						</div>
						<div class="modal-body text-warning">
							
						</div>
					</div>
				</div>
			</div>
		</div>
        
        <div id="successModal" class="modal fade" tabindex="-1" role="success" data-keyboard="false">
			<div class="vertical-alignment-helper">
				<div class="modal-dialog vertical-align-center">
					<div class="modal-content">
						<div class="modal-header bg-success">
						
							 <h4 class="modal-title">Request Sent</h4>

						</div>
						<div class="modal-body text-success">
							
						</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onClick="location.reload();">Close</button>
                        </div>
					</div>
				</div>
			</div>
		</div>
        
        <div id="disclaimerModal" class="modal fade" tabindex="-1" role="failed" data-keyboard="false">
			<div class="vertical-alignment-helper">
				<div class="modal-dialog vertical-align-center">
					<div class="modal-content">
						<div class="modal-header bg-danger">
						
							 <h4 class="modal-title">Disclaimer</h4>

						</div>
						<div class="modal-body text-warning">
                            <ol>
                                <li>Keterlambatan melakukan Cancel activity, biaya training akan dibebankan kepada user </li>
                                <li>Ketentuan pemesanan snack & lunch : > 2 jam tanpa snack & lunch, >= 3 jam-4 jam : snack, > 4jam : snack & lunch </li>
                            </ol>
						</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info" id="bookDisclaimer" data-dismiss="modal">Accept Disclaimer</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
					</div>
				</div>
			</div>
		</div>
<!-- /Main -->

<!-- /.modal -->
	<!-- script references -->
		<script src="Resources/assets/jquery/jquery-1.12.3.min.js"></script>
		<script src="Resources/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		
		<?=(isset($_option['scripts']) ? $_option['scripts'] : '')?>
        
        <script>
        var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        if(isChrome == 0){
            var script = document.createElement('script');
            script.onload = function () {
                console.log('not chrome');
                webshim.polyfill();
            };
            script.src = "Resources/assets/polyfiller-shims/polyfiller.js";
            
            document.head.appendChild(script);
        }
        
        getNotification();
        setInterval(function() {
          getNotification();
        }, 10000);
        function getNotification(){
            $.ajax({
                type: "POST",
                url: "notification.html",
                dataType: "json",
                success: function(resp){
                    if(resp.success == 1){
                        if(resp.data > 0){
                            if($(".badge").length > 0) {
                                $(".badge").remove();
                            }
                            var html = "  <span class='badge'>"+resp.data+"</span></a>";
                            $(html).insertBefore('.caret');
                            $("#notificationBadge").append(html);                    
                        }
                    }
                },
            });
            
        }
        </script>
	</body>
</html>