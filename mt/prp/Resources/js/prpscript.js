var submitted = 0;

jQuery(document).ready(function(){
    window.onscroll = function() {scrollFunction()};
    
    jQuery("#checkAll").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        if(me.is(':checked')){
            jQuery('.file-trigger').prop('disabled', true);
            jQuery('.note-trigger').prop('disabled', true);
            jQuery('.check-trigger').prop('checked', true);
            jQuery('.control-row').addClass('bg-success');
            jQuery('.control-row').addClass('text-white');
        }else{
            jQuery('.file-trigger').prop('disabled', false);
            jQuery('.note-trigger').prop('disabled', false);
            jQuery('.check-trigger').prop('checked', false);
            jQuery('.control-row').removeClass('bg-success');
            jQuery('.control-row').removeClass('text-white');
        }
    });
    jQuery(".check-trigger").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        var parent_parent = me.parent().parent();
        if(me.is(':checked')){
            parent_parent.find('input[type="file"], textarea').prop("disabled", true);
            parent_parent.removeClass('bg-warning');
            parent_parent.addClass('bg-success');
            parent_parent.addClass('text-white');
        }else{
            parent_parent.find('input[type="file"], textarea').prop("disabled", false);
            parent_parent.removeClass('bg-success');
            parent_parent.removeClass('text-white');
        }
    });
    
    jQuery(".file-trigger").click(function(event){
		var me = jQuery(this);
        var parent_parent = me.parent().parent();
        console.log(me.val()+ 'value');
        if(me.val()){
            parent_parent.removeClass('bg-success');
            parent_parent.addClass('bg-warning');
            parent_parent.addClass('text-white');
        }else{
            parent_parent.removeClass('bg-warning');
            parent_parent.removeClass('text-white');
        }
    });
    
    jQuery("#addFile").click(function(event){
        event.preventDefault();
		var me = jQuery(this);
        var html = '<div class="input-group">'+
        '               <input name="image_upload[]" type="file" class="form-control form-control-file" accept="image/*">'+
        '           </div><br/>';
        jQuery("#fileForm").append(html);
    });
    jQuery("#selectArea").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        if(me.val() != 'All'){
            jQuery(".table-area:not([data-area='"+me.val()+"'])").addClass('d-none');
            jQuery(".table-area[data-area='"+me.val()+"']").removeClass('d-none');
        }else{
            jQuery('.table-area').removeClass('d-none');
        }
    });

    jQuery("#selectOutlet").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        checkGroupExists(me);
    });
    
    jQuery("#selectMonth").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        checkGroupExists(me);
    });
    jQuery("#submitPRP").submit(function(event){
        if(submitted == 1){
            event.preventDefault();
        }else{
            jQuery('input[type="file"]', jQuery(this)).each(function(){
                if(jQuery(this).val() == ""){
                    jQuery(this).prop("disabled", true);
                }
                return true;
            });
        }
    });
    
    jQuery("#sendEmail").click(function(event){
		var me = jQuery(this);
        sendEmail(me);
    });
    
    jQuery('#submitPRP input[type="file"]').change(function(event){
        if(jQuery(this).val() != ""){
            readURL(this, jQuery(this));
        }else{
            jQuery(this).parent().find('label').html('Select File');
            jQuery(this).parent().find('img').remove();
        }
    });
    
    jQuery(".delete_recom").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        var cat_id = jQuery("#catid").val();
        jQuery("#admin_recom_delete").attr("href", "admin_recom_delete_"+id+"_"+cat_id+".html"); 
    });
});


function readURL(input, input2) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        input2.parent().find('label').html('Change');
        input2.parent().find('img').remove();
        input2.parent().append("<img src='"+e.target.result+"' style='width:50px; height:50px;'>");
    }

    reader.readAsDataURL(input.files[0]);
  }
}
function checkGroupExists(me){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
    var dataid = me.data("id");
    var outlet = jQuery("#selectOutlet").val();
    var month = jQuery("#selectMonth").val();
    jQuery.ajax({
        type: "POST",
        url: "group_data.html",
        data: 'outlet='+outlet+'&month='+month,
		dataType: "json",
        success: function(json){
            if(json.exists==1){
                // window.location.replace(json.url);
                jQuery('#tableForm').addClass('d-none');
                submitted = 1;
                jQuery("#submitButton").attr('disabled','disabled');
                jQuery("#statusModal .modal-body").html('This outlet PRP is already submitted, click <a href="'+json.url+'">Here</a> to edit.');
                jQuery("#statusModal").modal('show');
            }else{
                jQuery('#tableForm').removeClass('d-none');
                submitted = 0;
                jQuery("#submitButton").removeAttr('disabled');
            }
        },
        complete: function() {
			me.data('requestRunning', false);
		},
    });
    return false;
}

function sendEmail(me){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
    var dataid = me.data("id");
    jQuery.ajax({
        type: "POST",
        url: "send_email.html",
        data: 'group='+dataid,
		dataType: "json",
        success: function(json){
            jQuery('#confirmationModal').modal('hide');
            if(json.success==1){
                jQuery('#successModal').modal('show');
            }else if(json.success==0){
                jQuery('#failedModal').modal('show');
            }
        },
        complete: function() {
			me.data('requestRunning', false);
		},
    });
    return false;
}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        jQuery('#myBtn').css('display','block');
    } else {
        jQuery('#myBtn').css('display','none');
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
var refresh_page = function() {
  
    jQuery.ajax({
        type: "POST",
        url: "refresh_user.html",
        data: 'group=1',
		dataType: "json",
        
        complete: function() {
			console.log('page refreshed');
		}
    });
  
};

var interval = 1000 * 60 * 7; // where X is your every X minutes

setInterval(refresh_page, interval);