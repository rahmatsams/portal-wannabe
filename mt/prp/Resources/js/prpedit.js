var submitted = 0;

jQuery(document).ready(function(){
   
    jQuery(".check-trigger").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        var parent_parent = me.parent().parent();
        var last_status = jQuery("input[type='hidden'][name='last_status["+parent_parent.data("id")+"]']").val();
        if(me.is(':checked')){
            if(last_status == 1){
                parent_parent.find('input[type="file"], textarea').prop("disabled", true);
                parent_parent.removeClass('bg-danger');
                parent_parent.addClass('bg-success');
                parent_parent.addClass('text-white');
            }else{
                parent_parent.find('input[type="hidden"],input[type="file"], textarea').prop("disabled", true);
                parent_parent.removeClass('bg-warning');
                parent_parent.removeClass('text-black');
                parent_parent.addClass('bg-info');
                parent_parent.addClass('text-white');
                
            }
            
        }else{
            if(last_status == 0){
                parent_parent.find('input[type="hidden"],input[type="file"], textarea').prop("disabled", false);
                parent_parent.removeClass('bg-info');
                parent_parent.removeClass('text-white');
                parent_parent.addClass('bg-warning');
                parent_parent.addClass('text-black');
            }else{
                parent_parent.addClass('bg-danger');
                parent_parent.removeClass('bg-success');
                parent_parent.removeClass('text-white');
            }
        }
    });
    
    
    
    jQuery("#selectArea").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        if(me.val() != 'All'){
            jQuery(".table-area:not([data-area='"+me.val()+"'])").addClass('d-none');
            jQuery(".table-area[data-area='"+me.val()+"']").removeClass('d-none');
        }else{
            jQuery('.table-area').removeClass('d-none');
        }
    });

    jQuery("#selectOutlet").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        checkGroupExists(me);
    });
    
    jQuery("#selectMonth").change(function(event){
        event.preventDefault();
		var me = jQuery(this);
        checkGroupExists(me);
    });
    
    jQuery("#submitPRP").submit(function(event){
        jQuery('input[type="file"]', jQuery(this)).each(function(){
            if(jQuery(this).val() == ""){
                jQuery(this).prop("disabled", true);
            }
        });
        
        return true;
    });
   
    jQuery('#submitPRP input[type="file"]').change(function(event){
        if(jQuery(this).val() != ""){
            readURL(this, jQuery(this));
        }else{
            jQuery(this).parent().find('label').html('Select File');
            jQuery(this).parent().find('img').remove();
        }
    });
});

window.onscroll = function() {scrollFunction()};

function readURL(input, input2) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        input2.parent().find('label').html('Change');
        input2.parent().find('img').remove();
        input2.parent().append("<img src='"+e.target.result+"' style='width:50px; height:50px;'>");
    }

    reader.readAsDataURL(input.files[0]);
  }
}


function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
