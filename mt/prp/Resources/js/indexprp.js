function doSubmitSearch(){
    jQuery("#searchPRP").submit();
}


function loadData(me){
    if ( me.data('requestRunning') ) {
        return;
    }
    
    me.data('requestRunning', true);
    
    jQuery.ajax({
        type: "POST",
        url: "index_data.html",
        data: me.serialize(),
		dataType: "json",
        success: function(json){
            if(json.success==0){
                jQuery("#selectSub").addClass("sr-only");
				jQuery("#selectSub").addAttr('disabled');
            } else {
                var item = '';
                var l = 1;
                jQuery(json.data).each(function() {
                    var f = 0;
                    if(this.total_deduction != null){
                        this.default_point = this.default_point - this.group_point;
                    }
                    if(this.group_finding != null && this.group_id != null){
                        this.store_name = '<a href="view_prp_'+this.group_id+'.html" class="btn btn-primary">'+this.store_name+'</a>';
                    }
                    if(this.group_finding == null){
                        this.group_finding = 0;
                    }
                    if(this.group_recommendation == null){
                        this.group_recommendation = 0;
                    }
                    if(this.group_point == null){
                        this.group_point = 0;
                    }
                    if(this.group_status == null || this.group_status == 0){
                        this.group_status = '<button class="btn btn-sm btn-danger">Not Sent</button>';
                    }else{
                        this.group_status = '<button class="btn btn-sm btn-success">Sent</button>';
                    }
                    item += '<tr>'+
                    '   <td>'+l+'</td>'+
                    '   <td>'+this.store_name+'</td>'+
                    '   <td>'+this.group_finding+'</td>'+
                    '   <td>'+this.group_recommendation+'</td>'+
                    '   <td>'+this.group_point+'</td>'+
                    '   <td>'+this.default_point+'</td>'+
                    '   <td>'+this.group_status+'</td>'+
                    '<tr>';
                    l++;
                });
                jQuery("table#indexContent tbody").html(item);
            }
        },
        complete: function() {
			me.data('requestRunning', false);
		},
    });
    return false;
}

jQuery(document).ready(function(){
    jQuery("#selectMonth").change(function(event){
        doSubmitSearch();
    });
    jQuery("#searchPRP").submit(function(event){
        event.preventDefault();
        loadData(jQuery(this));
    });
    
});