﻿jQuery(document).ready(function(){
    jQuery(".manage-button").click(function(event){
        event.preventDefault();
		var bid = "#formEdit"+jQuery(this).data('id');
		if(jQuery(bid).length > 0){
			jQuery(bid+" td > input[type!='hidden']" ).removeAttr('readonly');
			jQuery(bid+" td > input[type!='hidden']" ).removeClass('form-control-plaintext');
			jQuery(bid+" td > input[type!='hidden']" ).addClass('form-control');

			jQuery(bid+" td > textarea" ).removeAttr('readonly');
			jQuery(bid+" td > textarea" ).removeClass('form-control-plaintext');
			jQuery(bid+" td > textarea" ).addClass('form-control');
			jQuery(bid+" td a.manage-button").addClass('d-none');
			jQuery(bid+" td input[type='submit']").removeClass('d-none');
            jQuery(bid+" td input[type='submit']").removeClass('form-control');
		}else{
			alert('no '+bid);
		}
    });
    jQuery(".delete-button").click(function(event){
        event.preventDefault();
		var bid = "#formEdit"+jQuery(this).data('id');
		if(jQuery(bid).length > 0){
			jQuery(bid+" td > input[type!='hidden']" ).removeAttr('readonly');
			jQuery(bid+" td > input[type!='hidden']" ).removeClass('form-control-plaintext');
			jQuery(bid+" td > input[type!='hidden']" ).addClass('form-control');

			jQuery(bid+" td > textarea" ).removeAttr('readonly');
			jQuery(bid+" td > textarea" ).removeClass('form-control-plaintext');
			jQuery(bid+" td > textarea" ).addClass('form-control');
			jQuery(bid+" td a.manage-button").addClass('d-none');
			jQuery(bid+" td input[type='submit']").removeClass('d-none');
            jQuery(bid+" td input[type='submit']").removeClass('form-control');
		}else{
			alert('no '+bid);
		}
    });
    
    jQuery(".delete_sub").click(function() {
        var id = jQuery(this).attr('data-id'); /*data-id ada di button Hapus*/
        jQuery("#admin_subcategory_delete").attr("href", "admin_subcategory_delete_"+id+".html"); 
    });
    
});