var submitted = 0;

jQuery(document).ready(function(){
    
    
    jQuery("#sendEmail").click(function(event){
		var me = jQuery(this);
        sendEmail(me);
    });
    
    jQuery(".update-modal").click(function(event){
		var me = jQuery(this);
        var data = jQuery(this).parent().parent().data('id');
        var area_name = jQuery(".finding[data-id='"+data+"']").find('.area-name').html();
        var category_name = jQuery(".finding[data-id='"+data+"']").find('.category-name').html();
        jQuery("#areaReply").html(area_name);
        jQuery("#categoryReply").html(category_name);
        jQuery("#imageReply").prop('src','./Resources/images/not_found.jpg');
        jQuery("#replyTicket")[0].reset();
        jQuery("#replyTicket").prop('action', 'index.php?s=ticket&i=reply&id='+data);
    });
    
    jQuery('#replyTicket input[type="file"]').change(function(event){
        if(jQuery(this).val() != ""){
            readURL(this, jQuery(this));
        }else{
            jQuery("#imageReply").prop('src','./Resources/images/not_found.jpg');
        }
    });
    
    jQuery('#replyTicket').submit(function(event){
        event.preventDefault();
        var me = jQuery(this);
        replyPRP(me, this);
    });
});


function readURL(input, input2) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        jQuery("#imageReply").attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
function replyPRP(me, input2){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
    var loc = me.prop('action');
    var dataid = me.data("id");
    jQuery.ajax({
        type: "POST",
        url: loc,
        data: new FormData(input2),
        contentType: false,
        cache: false,
        processData:false,
        dataType: 'json',
        beforeSend: function(){
            jQuery('#submitReply').prop("disabled","disabled");
            jQuery("#replyTicket").css("opacity",".5");
        },
        success: function(json){
            jQuery('#replyModal').modal('hide');
            if(json.success==1){
                jQuery('#successReplyModal').modal('show');
                location.reload();
            }else if(json.success==0){
                jQuery('#failedReplyModal').modal('show');
            }
        },
        complete: function() {
			me.data('requestRunning', false);
            jQuery('#submitReply').removeProp("disabled");
            jQuery("#replyTicket").css("opacity","1");
		},
    });
    return false;
}
function sendEmail(me){
    if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
    var dataid = me.data("id");
    jQuery.ajax({
        type: "POST",
        url: "send_email_"+num+".html",
        data: 'group='+dataid,
		dataType: "json",
        success: function(json){
            jQuery('#confirmationModal').modal('hide');
            if(json.success==1){
                jQuery('#successModal').modal('show');
                location.reload();
            }else if(json.success==0){
                jQuery('#failedModal').modal('show');
            }
        },
        complete: function() {
			me.data('requestRunning', false);
		},
    });
    return false;
}

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
